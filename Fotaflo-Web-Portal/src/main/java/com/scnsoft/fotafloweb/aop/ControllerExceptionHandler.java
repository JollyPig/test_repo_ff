package com.scnsoft.fotafloweb.aop;

import org.apache.catalina.connector.ClientAbortException;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class ControllerExceptionHandler {
    protected static Logger logger = Logger.getLogger(ControllerExceptionHandler.class);

    @Around("controllers()")
    public Object logException(ProceedingJoinPoint point){
        logger.debug("Before invoking controller " + point.getSignature().toShortString());
        Object value = null;
        try {
            value = point.proceed();
        }catch(ClientAbortException e){
            logger.error("ClientAbortException has been thrown by " + point.getSignature().toShortString());
        }catch (Throwable e) {
            logger.error("Error has been thrown by " + point.getSignature().toShortString(), e);
            throw new RuntimeException(e);
        }
        logger.debug("After invoking controller " + point.getSignature().toShortString());
        return value;
    }

    @Pointcut("execution(public * com.scnsoft.fotafloweb.controller.*.*(..))")
    public void controllers(){}

}
