package com.scnsoft.fotafloweb.dto;

import com.scnsoft.fotafloweb.model.TextPromotion;

public class TextPromotionDto {

    private Integer id;

    private Integer number;

    private String caption;

    private String text;

    public TextPromotionDto() {
    }

    public TextPromotionDto(TextPromotion promotion) {
        this.id = promotion.getId();
        this.number = promotion.getNumber();
        this.caption = promotion.getCaption();
        this.text = promotion.getText();
    }

    public TextPromotion toEntity(){
        TextPromotion promotion = new TextPromotion();
        promotion.setId(this.id);
        promotion.setNumber(this.number);
        promotion.setCaption(this.caption);
        promotion.setText(this.text);
        return promotion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "TextPromotionDto{" +
                "number=" + number +
                ", caption='" + caption + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
