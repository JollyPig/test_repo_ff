package com.scnsoft.fotafloweb.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LocationBean {

    @SerializedName("id")
    private Integer id;

    @SerializedName("locationName")
    private String name;

    @SerializedName("emailFooter")
    private String emailFooter;

    @SerializedName("fromEmail")
    private String fromEmail;

    private String logoUrl;

    private String logoText;

    private String mainLogoUrl;

    @SerializedName("pictures")
    private List<PictureBean> pictures;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailFooter() {
        return emailFooter;
    }

    public void setEmailFooter(String emailFooter) {
        this.emailFooter = emailFooter;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getLogoText() {
        return logoText;
    }

    public void setLogoText(String logoText) {
        this.logoText = logoText;
    }

    public String getMainLogoUrl() {
        return mainLogoUrl;
    }

    public void setMainLogoUrl(String mainLogoUrl) {
        this.mainLogoUrl = mainLogoUrl;
    }

    public List<PictureBean> getPictures() {
        return pictures;
    }

    public void setPictures(List<PictureBean> pictures) {
        this.pictures = pictures;
    }

    @Override
    public String toString() {
        return "LocationBean{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
