package com.scnsoft.fotafloweb.bean;

public class ShareStatusBean {

    private String type;
    private boolean success;
    private String account;
    private Integer friends;
    private Throwable error;

    public ShareStatusBean(String type, String account, Integer friends) {
        this.type = type;
        this.account = account;
        this.friends = friends;
        this.success = true;
    }

    public ShareStatusBean(String type, Throwable error) {
        this.type = type;
        this.error = error;
        this.success = false;
    }

    public String getType() {
        return type;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getAccount() {
        return account;
    }

    public Integer getFriends() {
        return friends;
    }

    public Throwable getError() {
        return error;
    }
}
