package com.scnsoft.fotafloweb.report.model;

public class ColumnHeader {

    private String id;
    private String name;
    private ColumnType type;
    private ColumnDataType dataType;
    private Integer width;

    public ColumnHeader(String id, String name, ColumnDataType dataType, Integer width) {
        this.id = id;
        this.name = name;
        this.type = ColumnType.CAPTION;
        this.dataType = dataType;
        this.width = width;
    }

    public ColumnHeader(String id, String name, ColumnType type, ColumnDataType dataType, Integer width) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.dataType = dataType;
        this.width = width;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ColumnType getType() {
        return type;
    }

    public void setType(ColumnType type) {
        this.type = type;
    }

    public ColumnDataType getDataType() {
        return dataType;
    }

    public void setDataType(ColumnDataType dataType) {
        this.dataType = dataType;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public enum ColumnDataType {
        LABEL, NUMBER;
    }

    public enum ColumnType{
        CAPTION, VALUE
    }

    @Override
    public String toString() {
        return "ColumnHeader{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", dataType=" + dataType +
                ", width=" + width +
                '}';
    }
}
