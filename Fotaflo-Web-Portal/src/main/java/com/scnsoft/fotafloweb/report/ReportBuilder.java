package com.scnsoft.fotafloweb.report;

import com.scnsoft.fotafloweb.report.model.ReportData;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;

public abstract class ReportBuilder {
    protected static final Logger logger = Logger.getLogger(ReportBuilder.class);

    @Value("${analytics.output}")
    protected String filepath;

    public abstract File createReport(ReportData data);

    protected File getOutputFile(String name){
        File out = new File(filepath);
        if (!out.exists()) {
            out.mkdirs();
        }
        out = new File(filepath + name);

        return out;
    }

    protected String getOutputFilename(ReportData data){
        return data.getType() + "_" + data.getRange();
    }
}
