package com.scnsoft.fotafloweb.report.model;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class ReportData {

    private String type;
    private String range;
    private List<ColumnHeader> columns;
    private Map<String, Map<Date, Map<String, Integer>>> data;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public List<ColumnHeader> getColumns() {
        return columns;
    }

    public void setColumns(List<ColumnHeader> columns) {
        this.columns = columns;
    }

    public Map<String, Map<Date, Map<String, Integer>>> getData() {
        return data;
    }

    public void setData(Map<String, Map<Date, Map<String, Integer>>> data) {
        this.data = data;
    }
}
