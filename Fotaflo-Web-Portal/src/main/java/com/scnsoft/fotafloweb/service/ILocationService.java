package com.scnsoft.fotafloweb.service;

import com.scnsoft.fotafloweb.bean.LocationBean;

import java.util.List;

public interface ILocationService {

    List<LocationBean> getLocations();

    LocationBean getLocation(int id);

}
