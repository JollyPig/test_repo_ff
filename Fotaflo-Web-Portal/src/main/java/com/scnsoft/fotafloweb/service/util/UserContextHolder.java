package com.scnsoft.fotafloweb.service.util;

import com.scnsoft.fotafloweb.security.User;
import com.scnsoft.fotafloweb.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class UserContextHolder {
    public final static int SERVER_COUNT = 2;

    public enum Server{
        PROD_1(1), PROD_2(2);

        private int id;
        Server(int id){
            this.id = id;
        }

        public int getId(){
            return id;
        }
    }

    @Value("${fotaflo.rest_path}")
    protected String restPath;

    @Value("${fotaflo.rest_path2}")
    protected String restPath2;

    @Value("${fotaflo.picture_remote_path}")
    private String remotePath;

    @Value("${fotaflo.picture_remote_path2}")
    private String remotePath2;

    public User getPrincipal(){
        User user = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal instanceof User){
            user = (User) principal;
        }

        return user;
    }

    public String getRestPath(int serverId){
        switch(serverId){
            case 1: return restPath;
            case 2: return restPath2;
            default: return restPath;
        }
    }

    public String getRestPath(){
        User user = getPrincipal();
        if(user == null){
            throw new ServiceException("Method call for getRestPath() is not allowed, because user is not set");
        }

        return getRestPath(user.getServer());
    }

    public String getRemotePicturePath(int serverId){
        switch(serverId){
            case 1: return remotePath;
            case 2: return remotePath2;
            default: return remotePath;
        }
    }

    public String getRemotePicturePath(){
        User user = getPrincipal();
        if(user == null){
            throw new ServiceException("Method call for getRemotePicturePath() is not allowed, because user is not set");
        }

        return getRemotePicturePath(user.getServer());
    }

    public int getServerCount(){
        return SERVER_COUNT;
    }

    public Server[] getServers(){
        return Server.values();
    }

}
