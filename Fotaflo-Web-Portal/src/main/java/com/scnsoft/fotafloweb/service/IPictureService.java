package com.scnsoft.fotafloweb.service;

import com.scnsoft.fotafloweb.bean.PictureBean;
import com.scnsoft.fotafloweb.model.Picture;
import com.scnsoft.fotafloweb.model.SystemUser;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 04.04.14
 * Time: 15:12
 * To change this template use File | Settings | File Templates.
 */
public interface IPictureService {

    public List<PictureBean> getPictures(String user);


 /*   public List<PictureBean> getPicturesForCodeUsers(String user, String picturesIds);*/

    public PictureBean getPicture(int id);

    PictureBean getPictureByUrl(String url);

    public List<PictureBean> getPicturesByIds(List<Integer> ids);

    List<PictureBean> getPicturesFromWebService(final SystemUser user);

    public  List<PictureBean> getPictureBeanListFromPicturesSet(List<Picture> pictures);

    List<PictureBean> getPictureBeanListFromRemoteDataStore(SystemUser user);
}
