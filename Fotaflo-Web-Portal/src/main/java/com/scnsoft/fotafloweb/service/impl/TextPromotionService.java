package com.scnsoft.fotafloweb.service.impl;

import com.scnsoft.fotafloweb.dao.ITextPromotionDao;
import com.scnsoft.fotafloweb.dto.TextPromotionDto;
import com.scnsoft.fotafloweb.model.TextPromotion;
import com.scnsoft.fotafloweb.service.ITextPromotionService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(readOnly = false)
public class TextPromotionService implements ITextPromotionService {
    protected static final Logger logger = Logger.getLogger(TextPromotionService.class);

    @Autowired
    private ITextPromotionDao promotionDao;

    @Override
    public TextPromotionDto getPromotion(Integer id) {
        TextPromotion p = promotionDao.get(id);
        return new TextPromotionDto(p);
    }

    @Override
    public List<TextPromotionDto> getPromotions() {
        List<TextPromotion> list = promotionDao.list();
        if(list == null){
            list = new ArrayList<TextPromotion>();
        }
        List<TextPromotionDto> result = new ArrayList<TextPromotionDto>(list.size());
        for(TextPromotion p: list){
            result.add(new TextPromotionDto(p));
        }
        return result;
    }

    @Override
    public Map<Integer, TextPromotionDto> getPromotionMap() {
        List<TextPromotion> list = promotionDao.list();
        if(list == null){
            list = new ArrayList<TextPromotion>();
        }
        Map<Integer, TextPromotionDto> result = new HashMap<Integer, TextPromotionDto>();
        for(TextPromotion p: list){
            if(p.getNumber() != null){
                result.put(p.getNumber(), new TextPromotionDto(p));
            }
        }
        return result;
    }

    @Override
    public Integer createPromotion(TextPromotionDto promotion) {
        if(promotion == null){
            throw new IllegalArgumentException("Field 'promotion' cannot be null");
        }
        return promotionDao.create(promotion.toEntity());
    }

    @Override
    public void editPromotion(TextPromotionDto promotion) {
        if(promotion == null){
            throw new IllegalArgumentException("Field 'promotion' cannot be null");
        }
        promotionDao.update(promotion.toEntity());
    }

    @Override
    public void deletePromotion(TextPromotionDto promotion) {
        if(promotion == null){
            throw new IllegalArgumentException("Field 'promotion' cannot be null");
        }
        promotionDao.delete(promotion.toEntity());
    }

    @Override
    public void savePromotions(List<TextPromotionDto> promotions) {
        TextPromotion promotion;
        for(TextPromotionDto p: promotions){
            if(p.getNumber() == null){
                throw new RuntimeException("Field 'number' must be defined"); // TODO
            }
            promotion = promotionDao.getByNumber(p.getNumber());
            if(promotion == null){
                promotion = new TextPromotion();
            }

            promotion.setNumber(p.getNumber());
            promotion.setCaption(p.getCaption());
            promotion.setText(p.getText());

            promotionDao.update(promotion);
        }
    }
}
