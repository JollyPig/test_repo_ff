package com.scnsoft.fotafloweb.service.impl;

import com.scnsoft.fotafloweb.dao.IPicturesDao;
import com.scnsoft.fotafloweb.dao.ISystemUserDao;
import com.scnsoft.fotafloweb.model.Picture;
import com.scnsoft.fotafloweb.model.SystemUser;
import com.scnsoft.fotafloweb.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 07.04.14
 * Time: 14:31
 * To change this template use File | Settings | File Templates.
 */
@Service
@Transactional(readOnly = false)
public class UserService implements IUserService {

    @Autowired
    private ISystemUserDao systemUserDao;

    @Autowired
    private IPicturesDao picturesDao;

    public SystemUser getUserByLogin(String userlogin) {
        return systemUserDao.getUserByLogin(userlogin);
    }

    public List<String> generateNewUserLogins(String userloginGroup, int n, String pictureIds) {

        userloginGroup = userloginGroup.substring(0,1).toUpperCase()+userloginGroup.substring(1,userloginGroup.length());
        SystemUser old = getUserByLogin(userloginGroup);
        List<String> userlogins = new ArrayList<String>();
        int lastDigit =  getLastLoginDigit(userloginGroup);
        for(int i = 1; i<=n; i++){
           SystemUser systemUser = new SystemUser();
            int num = lastDigit+i;
            String newLogin = userloginGroup+String.valueOf(num);
            if(old.getAccessCode()!=null && old.getAccessCode().equals("B") ){
                newLogin = "P"+userloginGroup.substring(1,userloginGroup.length())+String.valueOf(num);
            }
            systemUser.setPictureIds(pictureIds);
            systemUser.setLoginName(newLogin);
            systemUser.setAccessCode("A");
            systemUser.setLoginGroup(userloginGroup);
            systemUser.setLoginGroupNumber(num);
           // systemUser.setPictures(getPicturesFromString(pictureIds, systemUser));
            systemUser.setCurrency(old.getCurrency());
            systemUser.setPrice(old.getPrice());
            systemUser.setLocationId(old.getLocationId());

            Md5PasswordEncoder md5PasswordEncoder = new Md5PasswordEncoder();
            systemUser.setPassword(md5PasswordEncoder.encodePassword(newLogin, null));
            systemUser.setAccess(old.getAccess());
            systemUser.setServerNumber(old.getServerNumber());
            systemUserDao.create(systemUser);

            updateUserPicturesFromString(pictureIds, systemUser);

            userlogins.add(newLogin);
        }
        return userlogins;
    }

    protected Set<Picture> updateUserPicturesFromString(String pictureIds, SystemUser systemUser) {
        Set<Picture> pictures = new HashSet<Picture>();
        if(pictureIds==null) return pictures;
        String[] idsString = pictureIds.split(",");
        if(idsString.length!=0) {
            for(String idString :idsString){
                if(idString.length()>3 && idString.contains("_")) {
                    idString= idString.substring(idString.indexOf("_")+1,idString.length());
                    Picture picture = picturesDao.get(Integer.parseInt(idString));
                    if(picture!=null){
                        Set<SystemUser> systemUsers = picture.getSystemUsers();
                        systemUsers.add(systemUser);
                        pictures.add(picture);
                    }
                    picturesDao.update(picture);
                }
            }
        }
        return pictures;
    }

    protected int getLastLoginDigit(String userLogin) {
        List<SystemUser> userlogins =  systemUserDao.getUserByLoginGroup(userLogin);
        if(userlogins!=null && !userlogins.isEmpty()){
            Collections.sort(userlogins, new Comparator<SystemUser>() {

                public int compare(SystemUser o1, SystemUser o2) {
                    if (o1.getLoginGroupNumber() != null && o2.getLoginGroupNumber() != null) {
                            try {
                                int last1 = o1.getLoginGroupNumber().intValue();
                                int last2 = o2.getLoginGroupNumber().intValue();
                                return last1 > last2 ? 1 : -1;
                            } catch (NumberFormatException e) {
                                e.printStackTrace();
                            }
                        }
                   else {
                        return 0;
                   }
                    return 0;
                }

            });
            SystemUser lastuser = userlogins.get(userlogins.size() - 1);
            return lastuser.getLoginGroupNumber().intValue();
        }else{
            return 0;
        }
    }

}
