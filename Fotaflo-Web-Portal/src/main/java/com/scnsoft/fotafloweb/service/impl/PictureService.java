package com.scnsoft.fotafloweb.service.impl;

import java.util.*;

import com.scnsoft.fotafloweb.bean.LocationBean;
import com.scnsoft.fotafloweb.bean.PictureBean;
import com.scnsoft.fotafloweb.dao.IPicturesDao;
import com.scnsoft.fotafloweb.dao.remote.IRemoteDao;
import com.scnsoft.fotafloweb.model.Picture;
import com.scnsoft.fotafloweb.model.SystemUser;
import com.scnsoft.fotafloweb.service.IPictureService;
import com.scnsoft.fotafloweb.service.IUserService;
import com.scnsoft.fotafloweb.ws.IBeanWrapper;
import com.scnsoft.fotafloweb.ws.IPictureWebService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * A custom service for retrieving users from a custom datasource, such as a database.
 * <p/>
 * This custom service must implement Spring's {@link UserDetailsService}
 */
@Service
@Transactional(readOnly = false)
public class PictureService implements IPictureService {
    protected static Logger logger = Logger.getLogger(PictureService.class);

    @Autowired
    private IPicturesDao pictureDao;

    @Autowired
    private IUserService userService;

    @Autowired
    private IRemoteDao remoteDao;

    @Autowired
    private IPictureWebService pictureWebService;

    /**
     * Retrieves user pictures.
     */
    public List<PictureBean> getPictures(String user) {
        List<PictureBean> picturesList = new ArrayList<PictureBean>();
        SystemUser systemUser = userService.getUserByLogin(user);

        if(systemUser == null){
            throw new RuntimeException("No user in database with login: " + user);
        }

        List<Picture> picturesLocal = systemUser.getPictures();
        if(picturesLocal==null || picturesLocal.size()==0
                || ((systemUser.getLoginGroup() == null || systemUser.getLoginGroup().isEmpty()) && systemUser.getAccess().equals("8"))){
            picturesList = getPicturesFromWebService(systemUser);
        }else{
            picturesList = getPictureBeanListFromPicturesSet(picturesLocal);
        }

        Collections.sort(picturesList, new Comparator<PictureBean>() {

            public int compare(PictureBean o1, PictureBean o2) {
                Date d1 = o1.getCreationDate(), d2 = o2.getCreationDate();
                if(d1 == null){
                    return 1;
                }else if(d2 == null){
                    return -1;
                }else {
                    return d2.compareTo(d1);
                }
            }
        });

        return picturesList;

    }

    public  List<PictureBean> getPictureBeanListFromPicturesSet(List<Picture> pictures) {
        List<PictureBean> pictureBeans = new ArrayList<PictureBean>();
        if(pictures==null) return pictureBeans;
        for(Picture picture: pictures){

            if(picture!=null){
               pictureBeans.add(new PictureBean(picture));
            }

        }

        return pictureBeans;
    }


    @Override
    public PictureBean getPicture(int id) {
        Picture pic = pictureDao.get(id);
        if(pic != null){
            return new PictureBean(pic);
        }
        return null;
    }

    @Override
    public PictureBean getPictureByUrl(String url) {
        if(url == null){
            return null;
        }
        url = url.trim();
        if(url.startsWith("/")){
            url = url.replaceFirst("/","");
        }
        Picture p = pictureDao.getPictureByUrl(url);
        if(p == null){
            return null;
        }
        return new PictureBean(p);
    }

    @Override
    public List<PictureBean> getPicturesByIds(List<Integer> ids) {
        List<PictureBean> pictures = new ArrayList<PictureBean>();
        PictureBean pic = null;
        for(Integer id: ids){
            pic = this.getPicture(id);
            if(pic != null){
                pictures.add(pic);
            }
        }
        return pictures;
    }

    public List<PictureBean> getPicturesFromWebService(final SystemUser user){
        return pictureWebService.getPictures(user.getLoginName(), new IBeanWrapper() {
            @Override
            public Object prepare(Object bean) {
                savePicture((PictureBean)bean, user);
                return bean;
            }
        });
    }

    public List<PictureBean> getPictureBeanListFromRemoteDataStore(SystemUser user){
        final Integer picDataLength = 7, locDataLength = 5;
        LocationConverter locationConverter = new LocationConverter();
        PictureConverter pictureConverter = new PictureConverter();
        List<PictureBean> result = new ArrayList<PictureBean>();
        if(user == null){
            return result;
        }

        Map<Integer, LocationBean> locations = new HashMap<Integer, LocationBean>();
        LocationBean locationBean;
        List<Object[]> ls = remoteDao.getLocations(user);
        if(ls != null){
            for (Object[] item: ls){
                if(item.length == locDataLength){
                    locationBean = locationConverter.convert(item);
                    if(locationBean.getId() != null){
                        locations.put(locationBean.getId(), locationBean);
                    }
                }
            }
        }

        PictureBean pictureBean;
        List<Object[]> ps = remoteDao.getPicturesByCode(user);
        for (Object[] item: ps){
            if(item.length == picDataLength){
                pictureBean = pictureConverter.convert(item);
                if(pictureBean != null){
                    if(item[picDataLength-1] != null){
                        locationBean = locations.get(item[picDataLength-1]);
                        if(locationBean != null){
                            pictureBean.setLogoUrl(locationBean.getLogoUrl());
                            pictureBean.setLogoText(locationBean.getLogoText());
                            pictureBean.setLogoMainUrl(locationBean.getMainLogoUrl());
                        }
                    }

                    savePicture(pictureBean, user);
                    result.add(pictureBean);
                }
            }
        }

        ps = remoteDao.getLocationPictures(user, user.getLocationId());
        if(ps != null){
            for (Object[] item: ps){
                pictureBean = pictureConverter.convert(item);
                if(pictureBean != null){
                    locationBean = locations.get(user.getLocationId());
                    if(locationBean != null){
                        pictureBean.setLogoUrl(locationBean.getLogoUrl());
                        pictureBean.setLogoText(locationBean.getLogoText());
                        pictureBean.setLogoMainUrl(locationBean.getMainLogoUrl());
                    }
                    savePicture(pictureBean, user);
                    result.add(pictureBean);
                }
            }
        }

        return result;
    }

    protected boolean savePicture(PictureBean pictureBean, SystemUser user){
        if(pictureBean == null || user == null){
            return false;
        }
        Picture picture = pictureDao.getPictureByExternalId(pictureBean.getId(), user.getServerNumber());
        if(picture==null){
            picture = pictureBean.toEntity(user);
            picture.setPictureId(pictureBean.getId());
            Integer newId = pictureDao.create(picture);
            pictureBean.setId(newId);
        } else{
            picture.getSystemUsers().add(user);
            picture.setUrl(pictureBean.getUrl());
            picture.setSmall_url(pictureBean.getBase64code());
            picture.setLogoUrl(pictureBean.getLogoUrl());
            picture.setLogoText(pictureBean.getLogoText());
            picture.setLogoMainUrl(pictureBean.getLogoMainUrl());
            pictureDao.update(picture);
            pictureBean.setId(picture.getId());
        }
        return true;
    }

    interface Converter{
        Object convert(Object[] fields);
    }
    class PictureConverter implements Converter{
        @Override
        public PictureBean convert(Object[] fields) {
            PictureBean bean = null;
            String url;
            if(fields != null && fields.length >= 6){
                bean = new PictureBean();
                if(fields[0] != null) bean.setId((Integer)fields[0]);
                if(fields[1] != null) {
                    bean.setCreationDate((Date)fields[1]);
                }
                if(fields[2] != null) bean.setName(fields[2].toString());
                if(fields[3] != null) {
                    url = fields[3].toString();
                    bean.setUrl(url);
                    bean.setBase64code(bean.getUrl(true));
                }
                if(fields[4] != null) bean.setCameraId((Integer)fields[4]);
                if(fields[5] != null) bean.setRotated(new Boolean(fields[5].toString()));
            }
            return bean;
        }
    }
    class LocationConverter implements Converter{
        @Override
        public LocationBean convert(Object[] fields) {
            LocationBean bean = null;
            if(fields != null && fields.length >= 5){
                bean = new LocationBean();
                if(fields[0] != null) bean.setId(new Integer(fields[0].toString()));
                if(fields[1] != null) bean.setName(fields[1].toString());
                if(fields[2] != null) bean.setLogoUrl(fields[2].toString());
                if(fields[3] != null) bean.setLogoText(fields[3].toString());
                if(fields[4] != null) bean.setMainLogoUrl(fields[4].toString());
            }
            return bean;
        }
    }
}
