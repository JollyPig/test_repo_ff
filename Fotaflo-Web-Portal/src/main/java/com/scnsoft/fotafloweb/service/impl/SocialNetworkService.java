package com.scnsoft.fotafloweb.service.impl;

import com.scnsoft.fotafloweb.dao.ISocialNetworkDao;
import com.scnsoft.fotafloweb.dto.SocialNetworkDto;
import com.scnsoft.fotafloweb.model.SocialNetwork;
import com.scnsoft.fotafloweb.model.SocialNetworkType;
import com.scnsoft.fotafloweb.model.SystemUser;
import com.scnsoft.fotafloweb.service.IShareService;
import com.scnsoft.fotafloweb.service.ISocialNetworkService;
import com.scnsoft.fotafloweb.service.IUserService;
import com.scnsoft.fotafloweb.service.exception.ServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional(readOnly = false)
public class SocialNetworkService implements ISocialNetworkService {
    protected final static Logger logger = Logger.getLogger(SocialNetworkService.class);

    @Autowired
    private ISocialNetworkDao socialNetworkDao;

    @Autowired
    private IUserService userService;

    @Autowired
    private IShareService shareService;

    @Override
    public SocialNetworkDto getSocialNetwork(Integer id) {
        SocialNetwork sn = socialNetworkDao.get(id);
        if(sn == null){
            return null;
        }
        return new SocialNetworkDto(sn);
    }

    @Override
    public SocialNetworkDto findSocialNetworkDto(String name, Integer location) {
        SocialNetwork sn = socialNetworkDao.getByNameAndLocation(name, location);
        if(sn == null){
            return null;
        }
        return new SocialNetworkDto(sn);
    }

    @Override
    public Map<String, SocialNetworkDto> getSocialNetworkMap() {
        List<SocialNetwork> list = socialNetworkDao.list();
        if(list == null){
            list = new ArrayList<SocialNetwork>();
        }
        logger.debug(list);
        Map<String, SocialNetworkDto> result = new HashMap<String, SocialNetworkDto>();
        for(SocialNetwork sn: list){
            if(sn.getName() != null){
                result.put(sn.getName(), new SocialNetworkDto(sn));
            }
        }
        return result;
    }

    @Override
    public void updateSocialNetwork(SocialNetworkDto socialNetwork) {
        if(socialNetwork == null){
            throw new IllegalArgumentException("Field 'socialNetwork' cannot be null");
        }
        SocialNetworkType name = SocialNetworkType.valueOf(socialNetwork.getName());
        if(name != null && socialNetwork.getLocation() != null){
            SocialNetwork entity = socialNetworkDao.getByNameAndLocation(name.name(), socialNetwork.getLocation());
            if(entity == null){
                entity = new SocialNetwork();
                entity.setName(name.name());
                entity.setLocation(socialNetwork.getLocation());
            }
            entity.setTitle(socialNetwork.getTitle());
            entity.setDescription(socialNetwork.getDescription());
            entity.setAddress(socialNetwork.getAddress());
            entity.setTags(socialNetwork.getTags());
            if(name.equals(SocialNetworkType.facebook)){
                List<String> wrongTags = new ArrayList<String>();
                if(entity.getTagList() != null && !entity.getTagList().isEmpty()){
                    for(String tag: entity.getTagList()){
                        if(!shareService.checkFacebookTag(tag)){
                            wrongTags.add('@' + tag);
                        }
                    }
                }
                if(!wrongTags.isEmpty()){
                    ServiceException exception = new ServiceException("These tags are not exist: " + wrongTags.toString());
                    exception.setData(wrongTags);
                    throw exception;
                }
            }

            entity.setHashtags(socialNetwork.getHashtags());

            socialNetworkDao.update(entity);
        }else{
            throw new ServiceException("Invalid parameters.");
        }
    }

    @Override
    public void updateSocialNetworks(List<SocialNetworkDto> socialNetworks) {
        for(SocialNetworkDto dto: socialNetworks){
            this.updateSocialNetwork(dto);
        }
    }

    @Override
    public List<SocialNetworkType> getSocialNetworkNames() {
        return Arrays.asList(SocialNetworkType.values());
    }

    @Override
    public Map<String, SocialNetwork> getSocialNetworkSettings() {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        SystemUser systemUser = userService.getUserByLogin(currentUser);
        Integer locationId = systemUser.getLocationId();

        Map<String, SocialNetwork> result = new HashMap<String, SocialNetwork>();
        for(SocialNetwork sn: socialNetworkDao.findByLocation(locationId)){
            if(sn.getName() != null){
                result.put(sn.getName(), sn);
            }
        }
        return result;
    }
}
