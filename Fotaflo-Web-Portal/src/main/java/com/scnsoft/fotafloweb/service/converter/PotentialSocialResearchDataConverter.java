package com.scnsoft.fotafloweb.service.converter;

import com.scnsoft.fotafloweb.analytics.AnalyticData;
import com.scnsoft.fotafloweb.bean.LocationBean;
import com.scnsoft.fotafloweb.report.model.ColumnHeader;
import com.scnsoft.fotafloweb.report.model.ReportData;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class PotentialSocialResearchDataConverter extends AnalyticDataConverter {
    @Override
    public ReportData getReportData(AnalyticData o) {
        Map result = new HashMap();
        if(o == null || o.getStartDate() == null || o.getEndDate() == null || o.getRange() == null){
            throw new IllegalArgumentException("'analyticData' must be defined");
        }
        Date startDate = o.getStartDate(),
                endDate = o.getEndDate();
        DateRange range = getRange(o.getRange());

        if(range == null){
            throw new IllegalArgumentException("'range' is invalid");
        }

        List<ColumnHeader> columns = new ArrayList<ColumnHeader>();
        int dateFieldCount = addHeaderDateColumns(columns, range);

        columns.add(new ColumnHeader("facebook", "Facebook", ColumnHeader.ColumnType.VALUE, ColumnHeader.ColumnDataType.NUMBER, 15));
        columns.add(new ColumnHeader("twitter", "Twitter", ColumnHeader.ColumnType.VALUE, ColumnHeader.ColumnDataType.NUMBER, 15));
        columns.add(new ColumnHeader("tumblr", "Tumblr", ColumnHeader.ColumnType.VALUE, ColumnHeader.ColumnDataType.NUMBER, 15));

        if(o.getRows() != null){
            Integer locId = null;
            LocationBean loc;
            Map locData = null, dateData = null;
            String locDate = null, ds;
            Date date = null;
            String label, sum, count;
            Integer value;
            for(List<String> list: o.getRows()){
                if(!new Integer(list.get(0)).equals(locId)){
                    locId = new Integer(list.get(0));
                    try{
                        loc = locationService.getLocation(locId);
                    }catch(Exception e){
                        logger.error(e.getMessage(), e);
                        loc = null;
                    }
                    if(loc == null){
                        logger.warn("No location with id: "+locId);
                        continue;
                    }
                    locData = new TreeMap();
                    result.put(loc.getName(), locData);
                    locDate = null;
                }

                ds = list.get(1);
                for(int i=1; i<dateFieldCount; i++){
                    ds += list.get(i+1);
                }

                if(!ds.equals(locDate)){
                    try{
                        date = new SimpleDateFormat(range.getPattern()).parse(ds);
                    }catch(ParseException e){
                        logger.warn(e.getMessage(), e);
                    }
                    if(date == null){
                        continue;
                    }
                    locDate = ds;

                    dateData = new HashMap();
                    locData.put(date, dateData);
                    addDateColumns(dateData, range, date);

                }

                label = list.get(dateFieldCount+1);
                sum = list.get(dateFieldCount+3);
                count = list.get(dateFieldCount+4);

                if(label == null || sum == null || count == null){
                    continue;
                }

                if(dateData.containsKey(label)){
                    value = (Integer)dateData.get(label);
                }else{
                    value = 0;
                }

                if(new Integer(count) > 0){
                    value += new Integer(sum) / new Integer(count);
                }
                dateData.put(label, value);
            }
        }

        ReportData data = new ReportData();
        data.setType("Potential_Social_Research");
        data.setRange(formatter.format(startDate) + "-" + formatter.format(endDate));
        data.setColumns(columns);
        data.setData(result);

        return data;
    }
}
