package com.scnsoft.fotafloweb.service.impl;

import com.scnsoft.fotafloweb.bean.PictureBean;
import com.scnsoft.fotafloweb.service.ILocationService;
import com.scnsoft.fotafloweb.service.IPictureLoadService;
import com.scnsoft.fotafloweb.service.IPictureLogoService;
import com.scnsoft.fotafloweb.service.IPictureService;
import com.scnsoft.fotafloweb.util.IOUtil;
import com.scnsoft.fotafloweb.service.util.ImageWatermark;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class PictureLogoService implements IPictureLogoService {
    protected final static Logger logger = Logger.getLogger(PictureLogoService.class);

    @Autowired
    private IPictureService pictureService;

    @Autowired
    private IPictureLoadService pictureLoadService;

    @Autowired
    private ImageWatermark imageWatermark;

    @Autowired
    private ILocationService locationService;

    @Value("${fotaflo.picture_cache_path}")
    private String cachePath;

    Map<String, BufferedImage> logos = new HashMap<String, BufferedImage>();

    public BufferedImage getMarkedPicture(PictureBean picture, boolean thumb) throws IOException, URISyntaxException{
        if(picture == null){
            throw new IllegalArgumentException("Field 'picture' cannot be null");
        }

        String url = thumb ? picture.getBase64code() : picture.getUrl(),
                mainLogoUrl = picture.getLogoMainUrl(),
                logoUrl = picture.getLogoUrl();

        BufferedImage bufferedImage = getImage(url);

        if(bufferedImage != null){
            return bufferedImage;
        }

        bufferedImage = pictureLoadService.loadPicture(url);

        BufferedImage logo1 = getLogo(logoUrl);
        BufferedImage logo2 = getLogo(mainLogoUrl);

        imageWatermark.addLogosToImage(bufferedImage, logo1, logo2, picture.getLogoText());

        saveImage(bufferedImage, url);

        return bufferedImage;
    }

    private BufferedImage getLogo(String url) throws IOException, URISyntaxException{
        BufferedImage logo = null;
        if(url != null && !url.trim().isEmpty()){
            logo = getLogoImage(url);
            if(logo == null){
                logo = pictureLoadService.loadPicture(url);
                saveLogoImage(logo, url);
            }
        }
        return logo;
    }

    public void writeMarkedPicture(PictureBean picture, OutputStream out, boolean thumb) throws IOException, URISyntaxException{
        String url = thumb ? picture.getBase64code() : picture.getUrl();

        if(isImageCached(url)){
            writeImage(url, out);
            return;
        }

        BufferedImage bufferedImage = getMarkedPicture(picture, thumb);

        imageWatermark.saveImage(bufferedImage, getImageFormat(url), out);
    }

    public File getMarkedPictureFile(PictureBean picture, boolean thumb) throws IOException, URISyntaxException{
        File file = new File(getFilepath(thumb ? picture.getBase64code() : picture.getUrl()));
        if(file.exists()){
            return file;
        }

        BufferedImage bufferedImage = getMarkedPicture(picture, thumb);

        return saveImage(bufferedImage, picture.getUrl());
    }

    protected BufferedImage getImage(String uri) throws IOException, URISyntaxException{
        String filepath = getFilepath(uri);
        if(filepath != null && !filepath.trim().isEmpty()){
            File file = new File(filepath);
            if(file.exists()){
                logger.debug("Get file by path: "+filepath);
                return ImageIO.read(file);
            }
        }
        return null;
    }

    protected BufferedImage getLogoImage(String uri) throws IOException, URISyntaxException{
        String filepath = getLogoFilepath(uri);
        if(filepath != null && !filepath.trim().isEmpty()){
            File file = new File(filepath);
            if(file.exists()){
                logger.debug("Get file by path: "+filepath);
                return ImageIO.read(file);
            }
        }
        return null;
    }

    protected File getImageFile(String uri) throws IOException, URISyntaxException{
        String filepath = getFilepath(uri);
        if(filepath != null && !filepath.trim().isEmpty()){
            File file = new File(filepath);
            if(file.exists()){
                logger.debug("Get file by path: "+filepath);
                return file;
            }
        }
        return null;
    }

    public void writeImage(String uri, OutputStream out) throws IOException, URISyntaxException{
        File file = getImageFile(uri);
        InputStream in = new FileInputStream(file);
        if(file != null){
            IOUtil.writeFromInputToOutput(in, out);
        }

        if(in != null){
            in.close();
        }
    }

    public boolean isImageCached(String uri){
        String filepath = getFilepath(uri);
        if(filepath != null && !filepath.trim().isEmpty()){
            File file = new File(filepath);
            if(file.exists()){
                logger.debug("File exist by following path: "+filepath);
                return true;
            }
        }
        return false;
    }

    protected File saveImage(BufferedImage image, String path){
        String filepath = getFilepath(path);
        logger.debug("Save file by path: "+filepath);

        return imageWatermark.saveImage(image, getImageFormat(filepath), filepath);
    }

    protected File saveLogoImage(BufferedImage image, String path){
        String filepath = getLogoFilepath(path);
        logger.debug("Save file by path: "+filepath);

        return imageWatermark.saveImage(image, getImageFormat(filepath), filepath);
    }

    private String getFilepath(String path){
        String filepath = null;
        if(path != null && !path.trim().isEmpty()){
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            filepath = cachePath + File.separator + username;
            if(!path.startsWith("/")){
                filepath += File.separator;
            }
            filepath += path.replace('/', File.separatorChar);
        }

        return filepath;
    }

    private String getLogoFilepath(String path){
        /*String filepath = null;
        if(path != null && !path.trim().isEmpty()){
            filepath = cachePath + File.separator + "logo_cache";
            if(!path.startsWith("/")){
                filepath += File.separator;
            }
            filepath += path.replace('/', File.separatorChar);
        }

        return filepath;*/
        return getFilepath(path);
    }

    protected String getImageFormat(String path){
        String format = "JPEG";
        path = path.toLowerCase();

        if(path.endsWith(".jpg") || path.endsWith(".jpeg")){
            format = "JPEG";
        }else if(path.endsWith(".png")){
            format = "PNG";
        }else if(path.endsWith(".gif")){
            format = "GIF";
        }

        return format;
    }

}
