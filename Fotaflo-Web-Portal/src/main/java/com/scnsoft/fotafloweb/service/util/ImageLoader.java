package com.scnsoft.fotafloweb.service.util;

import com.scnsoft.fotafloweb.util.IOUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;

@Component
public class ImageLoader {
    protected final static Logger logger = Logger.getLogger(ImageLoader.class);

    @Value("${fotaflo.username}")
    private String username;

    @Value("${fotaflo.password}")
    private String password;

    public void loadImage(URL url, OutputStream destination) throws IOException{
        String credentials = username + ':' + password;
        String encoding = new sun.misc.BASE64Encoder().encode(credentials.getBytes());

        URLConnection uc = url.openConnection();
        uc.setRequestProperty("Authorization", "Basic " + encoding);
        //uc.setConnectTimeout(60000);

        InputStream content = null;
        int count = 0;

        try{
            content = uc.getInputStream();

            count = IOUtil.writeFromInputToOutput(content, destination);
        }finally {
            if(content != null){
                content.close();
            }
        }

        logger.info("File loaded by url " + url + ", file size: "+count+" byte(s)");
    }

    public BufferedImage loadImageAsBufferedImage(URL url) throws IOException{
        String credentials = username + ':' + password;
        String encoding = new sun.misc.BASE64Encoder().encode(credentials.getBytes());

        URLConnection uc = url.openConnection();
        uc.setRequestProperty("Authorization", "Basic " + encoding);
        //uc.setConnectTimeout(60000);

        InputStream content = null;
        BufferedImage image = null;

        try{
            content = uc.getInputStream();

            image = ImageIO.read(content);
        }finally {
            if(content != null){
                content.close();
            }
        }

        logger.info("File loaded by url " + url);

        return image;
    }

}
