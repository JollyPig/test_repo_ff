package com.scnsoft.fotafloweb.service.impl;

import com.scnsoft.fotafloweb.dao.IMerchantDao;
import com.scnsoft.fotafloweb.dao.IPaymentDao;
import com.scnsoft.fotafloweb.model.Merchant;
import com.scnsoft.fotafloweb.model.Payment;
import com.scnsoft.fotafloweb.service.IPayPalService;
import com.scnsoft.fotafloweb.util.PasswordEncryption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Tayna on 01.05.14.
 */
@Service
@Transactional(readOnly = false)
public class PayPalService implements IPayPalService {

    @Autowired
    private IMerchantDao merchantDao;

    @Autowired
    private IPaymentDao payPalDao;


    public void registerPayment(String userlogin, Date today, String currency, Double price,
                                Long invoiceId, String status, String token, String result, String ids, String email) {

        Payment payment = new Payment();
        payment.setUserLogin(userlogin);
        payment.setInvoiceId(invoiceId);
        payment.setPrice(price);
        payment.setCurrency(currency);
        payment.setStatus(status);
        payment.setToken(token);
        payment.setResult(result);
        payment.setPaymentDate(today);
        payment.setPictureids(ids);
        payment.setPayerEmail(email);
        payPalDao.create(payment);
    }

    public void savePayment(Payment payment) {

        payPalDao.update(payment);
    }

    public Payment getRegisteredPayment(String token) {

        if (token == null) {
            return null;
        }
        List<Payment> payments = payPalDao.getPaymentByTransaction(token);
        if (payments != null && payments.size() == 1) {
            return payments.get(0);
        } else {
            return null;
        }

    }

    public Merchant getMerchantAccount() throws Exception {
        Merchant merchant = null;
        List<Merchant> accounts = merchantDao.list();
        if (accounts.size() == 1) {
            merchant = accounts.get(0);
        }
        if (merchant != null) {
            PasswordEncryption passwordEncryption = new PasswordEncryption();
            merchant.setPassword(passwordEncryption.decryptPassword(merchant.getPassCode()));
            merchant.setSignature(passwordEncryption.decryptPassword(merchant.getSignCode()));
        }
        return merchant;
    }

    public Merchant getMerchantAccountEncrypted() throws Exception {
        Merchant merchant = null;
        List<Merchant> accounts = merchantDao.list();
        if (accounts.size() == 1) {
            merchant = accounts.get(0);
        }
        if(merchant != null){
            merchant.setPassword(merchant.getPassCode().toString());
            merchant.setSignature(merchant.getSignCode().toString());
        }
        return merchant;
    }


    public Merchant getMerchant() throws Exception {
        Merchant merchant = null;
        List<Merchant> accounts = merchantDao.list();
        if (accounts.size() == 1) {
            merchant = accounts.get(0);
        }
        return merchant;
    }


    public Merchant saveMerchantAccount(String account,
                                        String password,
                                        String signature,
                                        String environment) throws Exception {
        Merchant merchant = getMerchant();
        if (merchant == null) {
            merchant = new Merchant();
            fillMerchantFields(merchant,account,password,signature,environment);
            merchantDao.create(merchant);
        }else{
            fillMerchantFields( merchant, account,password,signature,environment);
            merchantDao.update(merchant);
        }

        return merchant;
    }


    private Merchant fillMerchantFields(Merchant merchant, String account,
                                       String password,
                                       String signature,
                                       String environment) throws Exception{

        PasswordEncryption passwordEncryption = new PasswordEncryption();
        merchant.setAccount(account);
        merchant.setPassword(password);
        merchant.setPassCode(passwordEncryption.encryptPassword(password));
        merchant.setSignature(signature);
        merchant.setSignCode(passwordEncryption.encryptPassword(signature));
        merchant.setEnvironment(environment);

        return merchant;
    }


   /* public Map<String, Object> getMerchantSettings() {
        Map map = new HashMap<String, String>();
        Merchant merch = null;
        try {
            merch = getMerchantAccount();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (merch != null) {
            map.put("account", merch.getAccount());
            map.put("password", merch.getPassword());
            map.put("signature", merch.getSignature());
            map.put("environment", merch.getEnvironment());
            return map;
        } else {
            return null;
        }
    }*/

}
