package com.scnsoft.fotafloweb.service;

import com.scnsoft.fotafloweb.model.Merchant;
import com.scnsoft.fotafloweb.model.Payment;

import java.util.Date;

/**
 * Created by Tayna on 01.05.14.
 */
public interface IPayPalService {

    public void registerPayment(String userlogin, Date today, String currency, Double price,
                                Long invoiceId, String status, String token, String result, String ids, String email);

    public void savePayment(Payment payment);

    public Payment getRegisteredPayment(String token);

    public Merchant saveMerchantAccount(String account,
                                        String password,
                                        String signature,
                                        String environment) throws Exception;

    public Merchant getMerchantAccount() throws Exception ;

    public Merchant getMerchantAccountEncrypted() throws Exception;


}
