package com.scnsoft.fotafloweb.service.util;

import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.http.client.utils.URIBuilder;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

@Component
public class RestClient {
    protected static Logger logger = Logger.getLogger(RestClient.class);

    public static final int RETRY_COUNT = 3;

    @Value("${fotaflo.username}")
    private String username;

    @Value("${fotaflo.password}")
    private String password;

    public String get(String url){
        return get(url, null);
    }

    public String get(String url, Map<String, Object> params){
        String response = null;
        GetMethod method = null;

        if(url == null){
            throw new IllegalArgumentException("'Url' cannot be null");
        }

        try {
            HttpClient client = new HttpClient();
            Credentials credentials = new UsernamePasswordCredentials(username, password);
            client.getState().setCredentials(AuthScope.ANY, credentials);

            client.getParams().setAuthenticationPreemptive(true);
            client.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, getRetryHandler());

            URIBuilder builder = new URIBuilder(url);

            if(params != null && !params.isEmpty()){
                Object value;
                for(String key: params.keySet()){
                    value = params.get(key);
                    if(value != null){
                        builder.addParameter(key, value.toString());
                    }
                }
            }

            method = new GetMethod(builder.build().toString());
            method.setDoAuthentication(true);

            // Send GET request
            int statusCode = client.executeMethod(method);
            logger.info(method.getURI());

            switch (statusCode) {
                case 400: {
                    logger.error("Bad request. The parameters passed to the service did not match as expected. The Message should tell you what was missing or incorrect.");
                    logger.warn("Change the parameter appcd to appid and this error message will go away.");
                    break;
                }
                case 403: {
                    logger.error("Forbidden. You do not have permission to access this resource, or are over your rate limit.");
                    break;
                }
                case 503: {
                    logger.error("Service unavailable. An internal problem prevented us from returning data to you.");
                    break;
                }
                default: {
                    if(statusCode >= 200 && statusCode < 400){
                        logger.info("Response HTTP status: " + statusCode);
                        response = method.getResponseBodyAsString();
                    }else{
                        logger.error("Your call returned an unexpected HTTP status of: " + statusCode);
                    }
                }

            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            // release any connection resources used by the method
            if(method != null){
                method.releaseConnection();
            }
        }

        return response;
    }

    protected HttpMethodRetryHandler getRetryHandler(){
        HttpMethodRetryHandler handler = new HttpMethodRetryHandler() {
            public boolean retryMethod(final HttpMethod method, final IOException exception, int executionCount) {
                if (executionCount >= RETRY_COUNT) {
                    // Do not retry if over max retry count
                    return false;
                }
                if (exception instanceof NoHttpResponseException) {
                    // Retry if the server dropped connection on us
                    return true;
                }
                if (!method.isRequestSent()) {
                    // Retry if the request has not been sent fully or
                    // if it's OK to retry methods that have been sent
                    return true;
                }
                // otherwise do not retry
                return false;
            }
        };

        return handler;
    }

}
