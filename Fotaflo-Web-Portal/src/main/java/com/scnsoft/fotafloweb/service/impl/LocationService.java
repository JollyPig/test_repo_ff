package com.scnsoft.fotafloweb.service.impl;

import com.scnsoft.fotafloweb.bean.LocationBean;
import com.scnsoft.fotafloweb.service.ILocationService;
import com.scnsoft.fotafloweb.ws.ILocationWebService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class LocationService implements ILocationService {
    protected static Logger logger = Logger.getLogger(LocationService.class);

    @Autowired
    private ILocationWebService locationWebService;

    @Override
    public List<LocationBean> getLocations(){
        List<LocationBean> result = locationWebService.getLocations();

        return result;
    }

    @Override
    public LocationBean getLocation(int id) {
        LocationBean result = locationWebService.getLocation(id);

        return result;
    }
}
