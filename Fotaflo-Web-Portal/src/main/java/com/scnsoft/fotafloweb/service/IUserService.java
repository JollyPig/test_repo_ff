package com.scnsoft.fotafloweb.service;

import com.scnsoft.fotafloweb.model.SystemUser;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 07.04.14
 * Time: 14:31
 * To change this template use File | Settings | File Templates.
 */
public interface IUserService {

    public SystemUser getUserByLogin(String userlogin);

    public List<String> generateNewUserLogins(String userloginGroup, int n, String pictureIds);
}
