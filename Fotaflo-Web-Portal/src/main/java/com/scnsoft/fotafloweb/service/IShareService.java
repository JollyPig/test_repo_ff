package com.scnsoft.fotafloweb.service;

import com.scnsoft.fotafloweb.bean.ShareStatusBean;
import com.scnsoft.fotafloweb.model.SocialNetworkType;
import com.scnsoft.fotafloweb.social.Connection;

import java.util.List;
import java.util.Map;

public interface IShareService {

    List<Connection> availableConnections();

    List<ShareStatusBean> shareImage(List<SocialNetworkType> socialNetTypes, List<Integer> images, String message, Map<String, String> tags);

    void removeAllConnections();

    boolean checkFacebookTag(String tag);

}
