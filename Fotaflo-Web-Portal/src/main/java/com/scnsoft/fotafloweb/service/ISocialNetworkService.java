package com.scnsoft.fotafloweb.service;


import com.scnsoft.fotafloweb.dto.SocialNetworkDto;
import com.scnsoft.fotafloweb.model.SocialNetwork;
import com.scnsoft.fotafloweb.model.SocialNetworkType;

import java.util.List;
import java.util.Map;

public interface ISocialNetworkService {

    SocialNetworkDto getSocialNetwork(Integer id);

    SocialNetworkDto findSocialNetworkDto(String name, Integer location);

    Map<String, SocialNetworkDto> getSocialNetworkMap();

    void updateSocialNetwork(SocialNetworkDto socialNetwork);

    void updateSocialNetworks(List<SocialNetworkDto> socialNetwork);

    List<SocialNetworkType> getSocialNetworkNames();

    Map<String, SocialNetwork> getSocialNetworkSettings();
}
