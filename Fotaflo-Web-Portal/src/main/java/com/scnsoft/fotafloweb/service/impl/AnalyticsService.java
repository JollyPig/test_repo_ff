package com.scnsoft.fotafloweb.service.impl;

import com.scnsoft.fotafloweb.analytics.ga.GaTemplate;
import com.scnsoft.fotafloweb.analytics.AnalyticData;
import com.scnsoft.fotafloweb.analytics.ga.tracking.GaTracker;
import com.scnsoft.fotafloweb.bean.LocationBean;
import com.scnsoft.fotafloweb.model.SystemUser;
import com.scnsoft.fotafloweb.report.model.ColumnHeader;
import com.scnsoft.fotafloweb.report.ReportBuilder;
import com.scnsoft.fotafloweb.report.model.ReportData;
import com.scnsoft.fotafloweb.service.IAnalyticsService;
import com.scnsoft.fotafloweb.service.ILocationService;
import com.scnsoft.fotafloweb.service.IUserService;
import com.scnsoft.fotafloweb.service.converter.DeviceTypeDataConverter;
import com.scnsoft.fotafloweb.service.converter.PotentialSocialResearchDataConverter;
import com.scnsoft.fotafloweb.service.converter.ShareDataConverter;
import com.scnsoft.fotafloweb.service.converter.VisitOverviewDataConverter;
import com.scnsoft.fotafloweb.ws.IAnalyticWebService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class AnalyticsService implements IAnalyticsService {
    protected static final Logger logger = Logger.getLogger(AnalyticsService.class);

    protected static final SimpleDateFormat formatter = new SimpleDateFormat("MM.dd.yyyy");

    @Autowired
    private GaTemplate ga;

    @Autowired
    GaTracker gaTracker;

    @Autowired
    private IUserService userService;

    @Autowired
    private ILocationService locationService;

    @Autowired
    private IAnalyticWebService analyticWebService;

    /* report builders */
    @Resource(name = "excelReportBuilder")
    private ReportBuilder excelReportBuilder;

    /*@Resource(name = "pdfReportBuilder")
    private ReportBuilder pdfReportBuilder;*/

    /* analytic data converters */
    @Resource(name = "shareDataConverter")
    private ShareDataConverter shareDataConverter;

    @Resource(name = "deviceTypeDataConverter")
    private DeviceTypeDataConverter deviceTypeDataConverter;

    @Resource(name = "visitOverviewDataConverter")
    private VisitOverviewDataConverter visitOverviewDataConverter;

    @Resource(name = "potentialSocialResearchDataConverter")
    private PotentialSocialResearchDataConverter potentialSocialResearchDataConverter;

    @Override
    public AnalyticData getVisitsByDeviceType() {
        try{
            AnalyticData o = ga.getDeviceTypeStatistic(new Date(), new Date(), null);
            logger.info(o);
        }catch(IOException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Map<String, Object> getVisitsByDeviceType(Date startDate, Date endDate, Integer location) {
        Map<String, Object> result = new HashMap<String, Object>();

        if(startDate == null || endDate == null){
            throw new IllegalArgumentException("Date range must be set");
        }

        if(location != null){
            LocationBean loc = locationService.getLocation(location);
            if(loc == null){
                throw new RuntimeException("Location not exist");
            }
        }

        Calendar c = Calendar.getInstance();

        c.setTime(startDate);
        c.add(Calendar.MILLISECOND, -(int)(endDate.getTime() - startDate.getTime()));

        AnalyticData o = null;
        Integer count;
        Map<String, Map<String, Integer>> data = new HashMap<String, Map<String, Integer>>();
        try{
            o = ga.getDeviceTypeStatistic(startDate, endDate, location);
            //result.put("records", o.getRows());
            if(o.getRows() == null){
                return result;
            }
            for(final List<String> list: o.getRows()){
                data.put(list.get(0), new HashMap<String, Integer>(){{put("value", new Integer(list.get(1)));}});
            }

            o = ga.getDeviceTypeStatistic(c.getTime(), startDate, location);
            if(o.getRows() != null){
                for(final List<String> list: o.getRows()){
                    if(data.containsKey(list.get(0))){
                        data.get(list.get(0)).put("oldValue", new Integer(list.get(1)));
                    }else{
                        data.put(list.get(0), new HashMap<String, Integer>(){{
                            put("value", 0);
                            put("oldValue", new Integer(list.get(1)));
                        }});
                    }
                }
            }
            result.put("records", data);

            o = ga.getDeviceTypeTotal(startDate, endDate, location);
            if(o.getRows() != null && o.getRows().size() > 0 && o.getRows().get(0).size() > 0){
                count = new Integer(o.getRows().get(0).get(0));
            }else{
                count = 0;
            }
            result.put("total", count);

            o = ga.getDeviceTypeTotal(c.getTime(), startDate, location);
            if(o.getRows() != null && o.getRows().size() > 0 && o.getRows().get(0).size() > 0){
                count = new Integer(o.getRows().get(0).get(0));
            }else{
                count = 0;
            }
            result.put("oldTotal", count);

            if(count > 0){
                result.put("increase", null);
            }
        }catch(IOException e){
            e.printStackTrace();
        }
        logger.info(result);
        return result;
    }

    @Override
    public Map<String, Object> getVisitsByGeography(Date startDate, Date endDate, Integer location) {
        Map<String, Object> result = new HashMap<String, Object>();

        if(startDate == null || endDate == null){
            throw new IllegalArgumentException("Date range must be set");
        }

        if(location != null){
            LocationBean loc = locationService.getLocation(location);
            if(loc == null){
                throw new RuntimeException("Location not exist");
            }
        }

        Calendar c = Calendar.getInstance();

        c.setTime(startDate);
        c.add(Calendar.MILLISECOND, -(int)(endDate.getTime() - startDate.getTime()));

        AnalyticData o = null;
        Integer count;
        try{
            o = ga.getGeographyStatistic(startDate, endDate, location);
            result.put("records", o.getRows());

            o = ga.getGeographyTotal(startDate, endDate, location);
            if(o.getRows() != null && o.getRows().size() > 0 && o.getRows().get(0).size() > 0){
                count = new Integer(o.getRows().get(0).get(0));
            }else{
                count = 0;
            }
            result.put("total", count);

            o = ga.getGeographyTotal(c.getTime(), startDate, location);
            if(o.getRows() != null && o.getRows().size() > 0 && o.getRows().get(0).size() > 0){
                count = new Integer(o.getRows().get(0).get(0));
            }else{
                count = 0;
            }
            result.put("oldTotal", count);

            if(count > 0){
                result.put("increase", null);
            }
        }catch(IOException e){
            e.printStackTrace();
        }
        logger.info(result);
        return result;
    }

    @Override
    public Map<String, Object> getShareCount(Date startDate, Date endDate, Integer location) {
        Map<String, Object> result = new HashMap<String, Object>();

        if(startDate == null || endDate == null){
            throw new IllegalArgumentException("Date range must be set");
        }

        if(location != null){
            LocationBean loc = locationService.getLocation(location);
            if(loc == null){
                throw new RuntimeException("Location not exist");
            }
        }

        Calendar c = Calendar.getInstance();

        c.setTime(startDate);
        c.add(Calendar.MILLISECOND, -(int)(endDate.getTime() - startDate.getTime()));

        AnalyticData o = null;
        Integer count;
        Map<String, Map<String, Integer>> data = new HashMap<String, Map<String, Integer>>();
        try{
            o = ga.getShareStatistic(startDate, endDate, location);
            //result.put("records", o.getRows());
            if(o.getRows() == null){
                return result;
            }
            for(final List<String> list: o.getRows()){
                data.put(list.get(0), new HashMap<String, Integer>(){{put("value", new Integer(list.get(1)));}});
            }

            o = ga.getShareStatistic(c.getTime(), startDate, location);
            if(o.getRows() != null){
                for(final List<String> list: o.getRows()){
                    if(data.containsKey(list.get(0))){
                        data.get(list.get(0)).put("oldValue", new Integer(list.get(1)));
                    }else{
                        data.put(list.get(0), new HashMap<String, Integer>(){{
                            put("value", 0);
                            put("oldValue", new Integer(list.get(1)));
                        }});
                    }
                }
            }
            result.put("records", data);

            o = ga.getShareTotal(startDate, endDate, location);
            if(o.getRows() != null && o.getRows().size() > 0 && o.getRows().get(0).size() > 0){
                count = new Integer(o.getRows().get(0).get(0));
            }else{
                count = 0;
            }
            result.put("total", count);

            o = ga.getShareTotal(c.getTime(), startDate, location);
            if(o.getRows() != null && o.getRows().size() > 0 && o.getRows().get(0).size() > 0){
                count = new Integer(o.getRows().get(0).get(0));
            }else{
                count = 0;
            }
            result.put("oldTotal", count);

            if(count > 0){
                result.put("increase", null);
            }
        }catch(IOException e){
            e.printStackTrace();
        }
        logger.info(result);
        return result;
    }

    public enum DateRange{
        DAY     (Calendar.DATE, "yyyyMMdd"),
        WEEK    (Calendar.WEEK_OF_YEAR, "yyyyww"),
        MONTH   (Calendar.MONTH, "yyyyMM"),
        YEAR    (Calendar.YEAR, "yyyy");

        private int id;
        private String pattern;

        DateRange(int id, String pattern){
            this.id = id;
            this.pattern = pattern;
        }

        int getId() {
            return id;
        }

        String getPattern() {
            return pattern;
        }
    }

    @Override
    public Map<String, Object> getShareCount(Date startDate, Date endDate, Integer locationId, DateRange range) {
        AnalyticData o = null;
        Map result = new HashMap();
        List r = new ArrayList();

        int dateFieldCount;
        switch (range){
            case DAY: dateFieldCount = 1;break;
            case WEEK: dateFieldCount = 2;break;
            case MONTH: dateFieldCount = 2;break;
            case YEAR: dateFieldCount = 1;break;
            default: dateFieldCount = 1;
        }

        try{
            o = ga.getShareStatisticWithoutLocationRanged(startDate, endDate, null, range.getId());
            if(o.getRows() != null){
                Map dateData = null;
                String locDate = null, ds;
                Date date = null;
                for(List<String> list: o.getRows()){

                    ds = list.get(0);
                    for(int i=1; i<dateFieldCount; i++){
                        ds += list.get(i);
                    }

                    if(!ds.equals(locDate)){
                        try{
                            date = new SimpleDateFormat(range.getPattern()).parse(ds);
                        }catch(ParseException e){
                            logger.warn(e.getMessage(), e);
                        }
                        if(date == null){
                            continue;
                        }
                        locDate = ds;

                        dateData = new HashMap();
                        //result.put(date, dateData);
                        r.add(dateData);
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(date);

                        calendar.roll(range.getId(), true);
                        calendar.add(Calendar.DATE, -1);

                        dateData.put("date", formatter.format(calendar.getTime()));
                    }

                    String key = list.get(dateFieldCount);
                    int value = new Integer(list.get(dateFieldCount+1));

                    dateData.put(key, value);
                }
            }
            result.put("data", r);
            logger.info(o);
            logger.info("-------->"+result);

        }catch(IOException e){
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public File getShareReport(Date startDate, Date endDate, Integer locationId, DateRange range) {
        AnalyticData o = null;
        File out = null;

        try{
            o = ga.getShareStatisticRanged(startDate, endDate, locationId, range.getId());

            ReportData data = shareDataConverter.getReportData(o);

            out = excelReportBuilder.createReport(data);
        }catch(IOException e){
            logger.info(e.getMessage(), e);
        }

        return out;
    }

    @Override
    public File getDeviceTypeReport(Date startDate, Date endDate, Integer locationId, DateRange range) {
        AnalyticData o = null;
        File out = null;

        try{
            o = ga.getDeviceTypeStatisticRanged(startDate, endDate, locationId, range.getId());

            ReportData data = deviceTypeDataConverter.getReportData(o);

            out = excelReportBuilder.createReport(data);
        }catch(IOException e){
            logger.info(e.getMessage(), e);
        }

        return out;
    }

    @Override
    public File getVisitorsOverviewReport(Date startDate, Date endDate, Integer locationId, DateRange range) {
        AnalyticData o = null;
        File out = null;

        try{
            o = ga.getUserStatistic(startDate, endDate, locationId, range.getId());

            String response = analyticWebService.getPurchaseEmails(locationId, startDate, endDate);

            logger.info(" ----> response1: "+response);

            response = analyticWebService.getPurchaseEmailCount(locationId, startDate, endDate);

            logger.info(" ----> response2: "+response);

            ReportData data = visitOverviewDataConverter.getReportData(o);

            out = excelReportBuilder.createReport(data);
        }catch(IOException e){
            e.printStackTrace();
        }
        return out;
    }

    @Override
    public File getPotentialSocialResearchReport(Date startDate, Date endDate, Integer locationId, DateRange range) {
        AnalyticData o = null;
        File out = null;

        try{
            o = ga.getPotentialSocialReachStatisticRanged(startDate, endDate, locationId, range.getId());

            ReportData data = potentialSocialResearchDataConverter.getReportData(o);
            logger.info(data.getData());

            out = excelReportBuilder.createReport(data);
        }catch(IOException e){
            e.printStackTrace();
        }
        return out;
    }

    @Override
    public void trackEvent(String clientId, String category, String action, String label, Integer value) {
        trackEvent(clientId, category, action, label, value, null);
    }

    @Override
    public void trackEvent(String clientId, String category, String action, String label, Integer value, String error) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        SystemUser user = userService.getUserByLogin(username);
        Integer locationId = null;
        String locationName = null;
        if(user != null){
            locationId = user.getLocationId();
            if(locationId != null){
                LocationBean locationBean = locationService.getLocation(locationId);
                if(locationBean != null){
                    locationName = locationBean.getName();
                }
            }
        }
        if(clientId != null){
            gaTracker.trackEvent(clientId, username, locationId, locationName, category, action, label, value, error);
        }
    }

    @Override
    public void trackSocial(String clientId, String category, String action, String label, Integer value, String accountName, Integer friendsCount) {
        trackSocial(clientId, category, action, label, value, accountName, friendsCount, null);
    }

    @Override
    public void trackSocial(String clientId, String category, String action, String label, Integer value, String accountName, Integer friendsCount, String error) {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        SystemUser user = userService.getUserByLogin(username);
        Integer locationId = null;
        String locationName = null;
        if(user != null){
            locationId = user.getLocationId();
            if(locationId != null){
                LocationBean locationBean = locationService.getLocation(locationId);
                if(locationBean != null){
                    locationName = locationBean.getName();
                }
            }
        }
        if(clientId != null){
            gaTracker.trackSocial(clientId, username, locationId, locationName, category, action, label, value, accountName, friendsCount, error);
        }
    }
}
