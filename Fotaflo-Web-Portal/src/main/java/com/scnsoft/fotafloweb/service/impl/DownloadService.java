package com.scnsoft.fotafloweb.service.impl;

import com.scnsoft.fotafloweb.bean.PictureBean;
import com.scnsoft.fotafloweb.service.IDownloadService;
import com.scnsoft.fotafloweb.service.IPictureLogoService;
import com.scnsoft.fotafloweb.service.IPictureService;
import com.scnsoft.fotafloweb.service.exception.ServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Service
public class DownloadService implements IDownloadService {
    protected static final Logger logger = Logger.getLogger(DownloadService.class);

    protected static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH-mm");

    @Autowired
    private IPictureService pictureService;

    @Autowired
    private IPictureLogoService pictureLogoService;

    @Override
    public byte[] createZipArchiveByPictureIds(List<Integer> ids, String fileBaseName) {
        List<PictureBean> pictures = pictureService.getPicturesByIds(ids);
        return createZipArchive(pictures, fileBaseName);
    }

    @Override
    public byte[] createZipArchive(List<PictureBean> pictures, String fileBaseName) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        writeZipArchive(pictures, fileBaseName, baos);
        return baos.toByteArray();
    }

    @Override
    public void writeZipArchiveByPictureIds(List<Integer> ids, String fileBaseName, OutputStream out) {
        List<PictureBean> pictures = pictureService.getPicturesByIds(ids);
        writeZipArchive(pictures, fileBaseName, out);
    }

    @Override
    public void writeZipArchive(List<PictureBean> pictures, String fileBaseName, OutputStream out) {
        if(logger.isDebugEnabled()){
            logger.debug(pictures);
        }
        if(pictures == null || pictures.isEmpty()){
            throw new IllegalArgumentException("'pictures' cannot be null or empty");
        }
        if(out == null){
            throw new IllegalArgumentException("out is null");
        }
        String folderName = fileBaseName + " " + sdf.format(new Date()) + File.separator;
        try{
            ZipOutputStream zipArchive = new ZipOutputStream(out);
            int pictureNumber = 0;
            for(PictureBean picture : pictures){
                StringBuffer entryName = new StringBuffer( folderName );
                entryName.append( " " + getFormattedDate(picture.getCreationDate()) );
                entryName.append( " " + pictureNumber );
                entryName.append( ".jpg" );
                logger.info(entryName.toString());

                ZipEntry zipEntry = new ZipEntry( entryName.toString() );
                zipArchive.putNextEntry( zipEntry );
                pictureLogoService.writeMarkedPicture(picture, zipArchive, false);
                pictureNumber++;
            }
            zipArchive.flush();
            zipArchive.close();
        }catch(IOException e){
            logger.error(e.getMessage(), e);
            throw new ServiceException(e.getMessage());
        }catch(URISyntaxException e){
            logger.error(e.getMessage(), e);
            throw new ServiceException(e.getMessage());
        }
        logger.info("DONE");
    }

    protected String getFormattedDate(Date date){
        if(date == null){
            date = new Date();
        }

        return sdf.format(date);
    }
}
