package com.scnsoft.fotafloweb.service;

import com.scnsoft.fotafloweb.bean.PictureBean;

import java.io.OutputStream;
import java.util.List;

public interface IDownloadService {

    byte[] createZipArchiveByPictureIds( List<Integer> pictures, String fileBaseName);

    byte[] createZipArchive(List<PictureBean> pictures, String fileBaseName);

    void writeZipArchiveByPictureIds( List<Integer> pictures, String fileBaseName, OutputStream out);

    void writeZipArchive(List<PictureBean> pictures, String fileBaseName, OutputStream out);

}
