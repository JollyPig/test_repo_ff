package com.scnsoft.fotafloweb.service.impl;

import com.scnsoft.fotafloweb.bean.PictureBean;
import com.scnsoft.fotafloweb.bean.ShareStatusBean;
import com.scnsoft.fotafloweb.model.SocialNetwork;
import com.scnsoft.fotafloweb.model.SocialNetworkType;
import com.scnsoft.fotafloweb.service.*;
import com.scnsoft.fotafloweb.social.Connection;
import com.scnsoft.fotafloweb.social.facebook.FacebookTemplate;
import com.scnsoft.fotafloweb.social.tumblr.TumblrTemplate;
import com.scnsoft.fotafloweb.social.twitter.TwitterTemplate;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.tumblr.api.Tumblr;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import twitter4j.Twitter;

import javax.inject.Inject;
import javax.inject.Provider;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class ShareService implements IShareService {
    private final static Logger logger = Logger.getLogger(ShareService.class);

    private FacebookTemplate facebook;

    private TwitterTemplate twitter;

    private TumblrTemplate tumblr;

    @Inject
    private Provider<ConnectionRepository> connectionRepositoryProvider;

    @Autowired
    private IPictureService pictureService;

    @Autowired
    private IPictureLogoService pictureLogoService;

    @Autowired
    private ISocialNetworkService socialNetworkService;

    @Inject
    public ShareService(Facebook facebook, Twitter twitter, Tumblr tumblr) {
        this.facebook = new FacebookTemplate(facebook);
        this.twitter = new TwitterTemplate(twitter);
        this.tumblr = new TumblrTemplate(tumblr);
    }

    private ConnectionRepository getConnectionRepository() {
        return connectionRepositoryProvider.get();
    }

    @Override
    public List<Connection> availableConnections() {
        MultiValueMap<String, org.springframework.social.connect.Connection<?>> conns = getConnectionRepository().findAllConnections();

        List<Connection> connections = new ArrayList<Connection>(conns.size());
        //Connection conn = null;
        for(String key: conns.keySet()){
            if(conns.get(key).size() > 0){
                connections.add(new Connection(key, conns.get(key).get(0)));
            }
        }
        return connections;
    }

    @Override
    public List<ShareStatusBean> shareImage(List<SocialNetworkType> socialNetTypes, List<Integer> images, String message, Map<String, String> usertags){
        List<ShareStatusBean> result = new ArrayList<ShareStatusBean>();

        List<PictureBean> pictures = pictureService.getPicturesByIds(images);

        List<Resource> files = new ArrayList<Resource>();// = getImages();

        for(PictureBean picture : pictures){
            try{
                files.add(new FileSystemResource(pictureLogoService.getMarkedPictureFile(picture, false)));
            }catch(Exception e){
                logger.warn(e.getMessage(),e);
            }
        }

        if(socialNetTypes == null || socialNetTypes.size() == 0){
            throw new IllegalArgumentException("Field 'socialNetTypes' cannot be empty");
        }

        Map<String, SocialNetwork> settingsMap = socialNetworkService.getSocialNetworkSettings();
        Map<String, Throwable> errors = new HashMap<String, Throwable>(socialNetTypes.size());
        Map<String, String> accounts = new HashMap<String, String>(socialNetTypes.size());
        Map<String, Integer> friends = new HashMap<String, Integer>(socialNetTypes.size());

        SocialNetwork settings;

        if(socialNetTypes.contains(SocialNetworkType.facebook)){
            settings = settingsMap.get(SocialNetworkType.facebook.name());
            String albumCaption = null, albumDescription = null, address = null;
            List<String> tags = null, hashtags = null;
            if(settings != null){
                albumCaption = settings.getTitle();
                albumDescription = settings.getDescription();
                tags = parseTags(usertags.get(SocialNetworkType.facebook.name()), "@");//settings.getTagList();
                hashtags = parseTags(usertags.get(SocialNetworkType.facebook.name()), "#");//settings.getHashtagList();
                address = settings.getAddress();
            }
            logger.info("facebook sharing");
            if(facebook != null && facebook.isAuthorized()){
                try{
                    facebook.shareImages(albumCaption, albumDescription, files, message, tags, hashtags, address);
                    accounts.put(SocialNetworkType.facebook.name(), facebook.getUserName());
                }catch (Exception e){
                    logger.error(e.getMessage(), e);
                    errors.put(SocialNetworkType.facebook.name(), e);
                }
                try{
                    friends.put(SocialNetworkType.facebook.name(), facebook.getFriendsCount());
                }catch (Exception e){
                    logger.warn(e.getMessage(), e);
                }
            }
        }

        if(socialNetTypes.contains(SocialNetworkType.twitter)){
            settings = settingsMap.get(SocialNetworkType.twitter.name());
            String address = null;
            List<String> tags = null, hashtags = null;
            if(settings != null){
                tags = parseTags(usertags.get(SocialNetworkType.twitter.name()), "@");//settings.getTagList();
                hashtags = parseTags(usertags.get(SocialNetworkType.twitter.name()), "#");//settings.getHashtagList();
                address = settings.getAddress();
            }
            logger.info("twitter sharing");
            if(twitter != null && twitter.isAuthorized()){
                int count = 1;
                String twit = message;
                try{
                    for(Resource res: files){
                        if(count > 1){
                            twit = message + " [twit "+count+"]";
                        }
                        twitter.shareImage(twit, res, tags, hashtags, address);
                        count++;
                    }
                    accounts.put(SocialNetworkType.twitter.name(), twitter.getUserName());
                }catch (Exception e){
                    logger.error(e.getMessage(), e);
                    errors.put(SocialNetworkType.twitter.name(), e);
                }
                try{
                    friends.put(SocialNetworkType.twitter.name(), twitter.getFriendAndFollowersCount());
                }catch (Exception e){
                    logger.warn(e.getMessage(), e);
                }
            }
        }

        if(socialNetTypes.contains(SocialNetworkType.tumblr)){
            settings = settingsMap.get(SocialNetworkType.tumblr.name());
            List<String> hashtags = null;
            if(settings != null){
                hashtags = parseTags(usertags.get(SocialNetworkType.tumblr.name()), "#");//settings.getHashtagList();
            }
            logger.info("tumblr sharing");
            if(tumblr != null && tumblr.isAuthorized()){
                try{
                    tumblr.shareImages(message, files, hashtags);
                    accounts.put(SocialNetworkType.tumblr.name(), tumblr.getUserId());
                }catch (Exception e){
                    logger.error(e.getMessage(), e);
                    errors.put(SocialNetworkType.tumblr.name(), e);
                }
                try{
                    friends.put(SocialNetworkType.tumblr.name(), tumblr.getFollowersCount());
                }catch (Exception e){
                    logger.warn(e.getMessage(), e);
                }
            }
        }

        logger.info("DONE");

        //this.deleteTemporalFiles(files);

        logger.info("frieds"+ friends);
        ShareStatusBean status;
        for(SocialNetworkType type: socialNetTypes){
            if(errors.containsKey(type.name())){
                status = new ShareStatusBean(type.name(), errors.get(type.name()));
            }else{
                status = new ShareStatusBean(type.name(), accounts.get(type.name()), friends.get(type.name()));
            }
            result.add(status);
        }

        return result;

        /*if(errors.size() > 0){
            RuntimeException e = new ShareImageException(errors);
            logger.error(e.getMessage(), e);
            throw e;
        }*/
    }

    @Override
    public void removeAllConnections(){
        MultiValueMap<String, org.springframework.social.connect.Connection<?>> conns = getConnectionRepository().findAllConnections();

        for(String key: conns.keySet()){
            getConnectionRepository().removeConnections(key);
        }
    }

    @Override
    public boolean checkFacebookTag(String tag) {
        return facebook.isTag(tag);
    }

    private List parseTags(String tagstr, String prefix){
        List<String> tags = new ArrayList<String>();

        if(tagstr != null){
            String[] tokens = tagstr.split("[, ]");
            for(String t: tokens){
                if(!t.isEmpty()){
                    if(t.startsWith(prefix)){
                        tags.add(t.substring(prefix.length()));
                    }
                }
            }
        }
        return tags;
    }

    protected void deleteTemporalFiles(List<Resource> resources){
        File file;
        boolean success;
        for(Resource res: resources){
            try{
                file = res.getFile();
                if(file != null){
                    success = file.delete();
                    if(!success){
                        throw new IOException("File cannot be deleted");
                    }
                }else{
                    throw new FileNotFoundException("Resource doesn't contain the file");
                }
            }catch(IOException e){
                logger.warn("Error on deleting temporal file",e);
            }
        }
    }

    protected List<Resource> getImages(List<String> images){
        List<Resource> list = new ArrayList<Resource>();
        Resource res = null;
        /*if(images != null){
            for(String image: images){
                try{
                    res = getImage(image);
                    list.add(res);
                }catch(MalformedURLException e){
                    logger.warn("Get image by url exception",e);
                }catch(URISyntaxException e){
                    logger.warn("Get image by url exception",e);
                }catch(IOException e){
                    logger.warn("Save image exception",e);
                }
            }
        }*/
        logger.info(list);
        return list;
    }

    protected Resource getImage(String url) throws IOException, URISyntaxException{
        Resource res = null;
        //File file = loader.loadImage(url);
        /*BufferedImage image = pictureLoadService.loadPicture(url);

        if(file != null){
            res = new FileSystemResource(file);
        }*/
        return res;
    }

}