package com.scnsoft.fotafloweb.service.impl;

import com.scnsoft.fotafloweb.dao.IPromotionDao;
import com.scnsoft.fotafloweb.dto.PromotionDto;
import com.scnsoft.fotafloweb.model.Position;
import com.scnsoft.fotafloweb.model.Promotion;
import com.scnsoft.fotafloweb.service.IPromotionService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(readOnly = false)
public class PromotionService implements IPromotionService {
    protected static final Logger logger = Logger.getLogger(PromotionService.class);

    @Autowired
    private IPromotionDao promotionDao;

    @Override
    public PromotionDto getPromotion(Integer id) {
        Promotion p = promotionDao.get(id);
        if(p == null){
            return null;
        }
        return new PromotionDto(p);
    }

    @Override
    public List<PromotionDto> getPromotions() {
        List<Promotion> list = promotionDao.list();
        if(list == null){
            list = new ArrayList<Promotion>();
        }
        List<PromotionDto> result = new ArrayList<PromotionDto>(list.size());
        for(Promotion p: list){
            result.add(new PromotionDto(p));
        }
        return result;
    }

    @Override
    public Map<Integer, PromotionDto> getPromotionMap() {
        List<Promotion> list = promotionDao.list();
        if(list == null){
            list = new ArrayList<Promotion>();
        }
        Map<Integer, PromotionDto> result = new HashMap<Integer, PromotionDto>();
        for(Promotion p: list){
            if(p.getNumber() != null){
                result.put(p.getNumber(), new PromotionDto(p));
            }
        }
        return result;
    }

    @Override
    public Integer createPromotion(PromotionDto promotion) {
        if(promotion == null){
            throw new IllegalArgumentException("Field 'promotion' cannot be null");
        }
        return promotionDao.create(promotion.toEntity());
    }

    @Override
    public void editPromotion(PromotionDto promotion) {
        if(promotion == null){
            throw new IllegalArgumentException("Field 'promotion' cannot be null");
        }
        promotionDao.update(promotion.toEntity());
    }

    @Override
    public void deletePromotion(PromotionDto promotion) {
        if(promotion == null){
            throw new IllegalArgumentException("Field 'promotion' cannot be null");
        }
        promotionDao.delete(promotion.toEntity());
    }

    @Override
    public Position[] getPromotionPositions() {
        return Position.values();
    }

    @Override
    public void savePromotions(List<PromotionDto> promotions) {
        Promotion promotion;
        for(PromotionDto p: promotions){
            if(p.getNumber() == null){
                throw new RuntimeException("Field 'number' must be defined"); // TODO
            }
            promotion = promotionDao.getByNumber(p.getNumber());
            if(promotion == null){
                promotion = new Promotion();
            }

            promotion.setNumber(p.getNumber());
            promotion.setText(p.getText());
            promotion.setPosition(p.getPosition());
            promotion.setColor(p.getColor());
            promotion.setShadowColor(p.getShadowColor());
            promotion.setPath(p.getPath());

            promotionDao.update(promotion);
        }
    }
}
