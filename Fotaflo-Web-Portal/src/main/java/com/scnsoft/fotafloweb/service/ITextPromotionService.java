package com.scnsoft.fotafloweb.service;

import com.scnsoft.fotafloweb.dto.TextPromotionDto;

import java.util.List;
import java.util.Map;

public interface ITextPromotionService {

    TextPromotionDto getPromotion(Integer id);

    List<TextPromotionDto> getPromotions();

    Map<Integer, TextPromotionDto> getPromotionMap();

    Integer createPromotion(TextPromotionDto promotion);

    void editPromotion(TextPromotionDto promotion);

    void deletePromotion(TextPromotionDto promotion);

    void savePromotions(List<TextPromotionDto> promotions);
}
