package com.scnsoft.fotafloweb.dao.remote;

import com.scnsoft.fotafloweb.model.SystemUser;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Repository
public class RemoteDao extends AbstractRemoteDao implements IRemoteDao {
    protected final static Logger logger = Logger.getLogger(RemoteDao.class);

    enum AccessID {
        ADMIN(1), USER(2), PUBLICUSER(3), PUBLICEMAILUSER(4), PUBLICTAGUSER(5), PUBLICPURCHASEUSER(6), PUBLICTAGCODEUSER(7),PUBLICTAGACODE(8);

        private int value;

        private AccessID(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public String getStringValue() {
            return String.valueOf(this.value);
        }
    }

    /*@Override
    public List<String[]> getPicturesByCode(SystemUser user) {
        List<String[]> result = new ArrayList<String[]>();
        if(user == null || user.getLoginName() == null){
            return result;
        }
        logger.info("Get pictures by "+user.getLoginName()+"(id="+user.getId()+")"+", access: "+user.getAccess());

        Set<String> params = new HashSet<String>(3);
        StringBuffer query = new StringBuffer();
        query.append("SELECT pc.id,pc.creation_date,pc.picture_name,pc.picture_url,pc.camera_id,pc.rotated,pc.pictureSize,c.location_id");

        if(String.valueOf(AccessID.PUBLICPURCHASEUSER.getValue()).equals(user.getAccess())){
            query.append(" FROM purchase p, purchase_pictures pp, picture pc, camera c");
            query.append(" WHERE p.id=pp.purchase_id AND pp.picture_id=pc.id AND pc.camera_id=c.id AND");
            query.append(" p.purchase_code=:code");
            params.add("code");
        }else if(String.valueOf(AccessID.PUBLICEMAILUSER.getValue()).equals(user.getAccess())){
            query.append(" FROM userfilter f, userfilter_picture fp, picture pc, camera c");
            query.append(" WHERE fp.userfilter_id=f.id AND fp.pictures_id=pc.id AND pc.camera_id=c.id AND");
            query.append(" f.system_user_id=:id");
            params.add("id");
        }else if(String.valueOf(AccessID.PUBLICTAGCODEUSER.getValue()).equals(user.getAccess())){
            query.append(" FROM tagpurchase tp, purchase_tags pt, tag t, picture_tags pct, picture pc, camera c");
            query.append(" WHERE tp.id=pt.purchase_tag_id AND pt.tag_id=t.id AND t.id=pct.tag_id AND pct.picture_id=pc.id AND pc.camera_id=c.id AND");
            query.append(" (pc.creation_date > tp.start_date AND pc.creation_date < tp.end_date) AND tp.purchase_code=:code");
            params.add("code");
        }else if(String.valueOf(AccessID.PUBLICTAGACODE.getValue()).equals(user.getAccess())){
            query.append(" FROM tagpurchase tp, purchase_tags pt, tag t, picture_tags pct, picture pc, camera c");
            query.append(" WHERE tp.id=pt.purchase_tag_id AND pt.tag_id=t.id AND t.id=pct.tag_id AND pct.picture_id=pc.id AND pc.camera_id=c.id AND");
            query.append(" (pc.creation_date > tp.start_date AND pc.creation_date < tp.end_date) AND");
            query.append(" pt.purchase_tag_id IN");
            query.append(" (SELECT pt.purchase_tag_id FROM tag t, purchase_tags pt WHERE pt.tag_id=t.id AND t.tag_name=:code AND t.location_id=:location)");
            params.add("code");
            params.add("location");
        }else if(String.valueOf(AccessID.PUBLICTAGUSER.getValue()).equals(user.getAccess())){
            query.append(" FROM tag t, picture_tags pct, picture pc, camera c");
            query.append(" WHERE t.id=pct.tag_id AND pct.picture_id=pc.id AND pc.camera_id=c.id AND");
            query.append(" t.tag_name=:code AND t.location_id=:location");
            params.add("code");
            params.add("location");
        }else{
            return result;
        }

        query.append(" order by pc.creation_date desc");

        logger.info("Query: "+query);

        SQLQuery sqlQuery = getSession().createSQLQuery(query.toString());
        for(String p: params){
            if(p.equals("id")){
                sqlQuery.setInteger("id", user.getId());
            }else if(p.equals("code")){
                sqlQuery.setString("code", user.getLoginName());
            }else if(p.equals("location")){
                sqlQuery.setInteger("location", user.getLocationId());
            }
        }
        result = sqlQuery.list();

        return result;
    }*/

    @Override
    public List<Object[]> getPicturesByCode(SystemUser user) {
        List<Object[]> result = new ArrayList<Object[]>();
        if(user == null || user.getLoginName() == null){
            return result;
        }
        logger.info("Get pictures by "+user.getLoginName()+"(id="+user.getId()+")"+", access: "+user.getAccess());
        String code = user.getLoginName();

        Set<String> params = new HashSet<String>(3);
        StringBuffer query = new StringBuffer();
        query.append("SELECT pc.id,pc.creation_date,pc.picture_name,pc.picture_url,pc.camera_id,pc.rotated,c.location_id");

        if(String.valueOf(AccessID.PUBLICPURCHASEUSER.getValue()).equals(user.getAccess())){
            return getPicturesByPurchase(query, user);
            /*query.append(" FROM purchase p INNER JOIN purchase_pictures pp ON p.id=pp.purchase_id INNER JOIN picture pc ON pp.picture_id=pc.id INNER JOIN camera c ON pc.camera_id=c.id");
            query.append(" WHERE p.purchase_code=:code");
            params.add("code");*/
        }else if(String.valueOf(AccessID.PUBLICEMAILUSER.getValue()).equals(user.getAccess())){
            query.append(" FROM userfilter f INNER JOIN userfilter_picture fp ON fp.userfilter_id=f.id INNER JOIN picture pc ON fp.pictures_id=pc.id INNER JOIN camera c ON pc.camera_id=c.id");
            query.append(" WHERE f.system_user_id=:id");
            params.add("id");
        }else if(String.valueOf(AccessID.PUBLICTAGCODEUSER.getValue()).equals(user.getAccess())){
            if(code.length() == 8){
                code = code.substring(0,7);
            }
            query.append(" FROM tagpurchase tp INNER JOIN purchase_tags pt ON tp.id=pt.purchase_tag_id INNER JOIN tag t ON pt.tag_id=t.id INNER JOIN picture_tags pct ON t.id=pct.tag_id INNER JOIN picture pc ON pct.picture_id=pc.id INNER JOIN camera c ON pc.camera_id=c.id");
            query.append(" WHERE (pc.creation_date > tp.start_date AND pc.creation_date < tp.end_date) AND tp.purchase_code=:code");
            params.add("code");
        }else if(String.valueOf(AccessID.PUBLICTAGACODE.getValue()).equals(user.getAccess())){
            query.append(" FROM tagpurchase tp INNER JOIN purchase_tags pt ON tp.id=pt.purchase_tag_id INNER JOIN tag t ON pt.tag_id=t.id INNER JOIN picture_tags pct ON t.id=pct.tag_id INNER JOIN picture pc ON pct.picture_id=pc.id INNER JOIN camera c ON pc.camera_id=c.id");
            query.append(" WHERE (pc.creation_date > tp.start_date AND pc.creation_date < tp.end_date) AND");
            query.append(" pt.purchase_tag_id IN");
            query.append(" (SELECT pt.purchase_tag_id FROM tag t, purchase_tags pt WHERE pt.tag_id=t.id AND t.tag_name=:code AND t.location_id=:location)");
            params.add("code");
            params.add("location");
        }else if(String.valueOf(AccessID.PUBLICTAGUSER.getValue()).equals(user.getAccess())){
            query.append(" FROM tag t INNER JOIN picture_tags pct ON t.id=pct.tag_id INNER JOIN picture pc ON pct.picture_id=pc.id INNER JOIN camera c ON pc.camera_id=c.id");
            query.append(" WHERE t.tag_name=:code AND t.location_id=:location");
            params.add("code");
            params.add("location");
        }else{
            return result;
        }

        query.append(" order by pc.creation_date desc");

        logger.info("Query: "+query);

        SQLQuery sqlQuery = getSession(user.getServerNumber()).createSQLQuery(query.toString());
        for(String p: params){
            if(p.equals("id")){
                sqlQuery.setInteger("id", user.getId());
            }else if(p.equals("code")){
                sqlQuery.setString("code", code);
            }else if(p.equals("location")){
                sqlQuery.setInteger("location", user.getLocationId());
            }
        }
        result = sqlQuery.list();

        return result;
    }

    private List<Object[]> getPicturesByPurchase(StringBuffer query, SystemUser user){
        List<Object[]> result = new ArrayList<Object[]>();
        StringBuffer query1 = new StringBuffer(),
                pids = new StringBuffer();
        List<Object> purchases;
        String code = user.getLoginName();
        if(code.length() == 8){
            code = code.substring(0,7);
        }
        query1.append("select pp.picture_id from purchase p INNER JOIN purchase_pictures pp ON p.id=pp.purchase_id where purchase_code=:code");
        purchases = getSession(user.getServerNumber()).createSQLQuery(query1.toString())
                .setString("code", code)
                .list();
        if(purchases != null){
            for (Object id: purchases){
                if(id != null){
                    pids.append(id + ",");
                }
            }
        }
        if(pids.length() > 1){
            query.append(" FROM picture pc INNER JOIN camera c ON pc.camera_id=c.id");
            query.append(" WHERE pc.id IN (");
            query.append(pids.substring(0,pids.length()-1));
            query.append(")");
            query.append(" order by pc.creation_date desc");

            result = getSession(user.getServerNumber()).createSQLQuery(query.toString())
                    .list();
        }
        return result;
    }

    @Override
    public List<Object[]> getLocations(SystemUser user) {
        List<Object[]> result = new ArrayList<Object[]>();
        if(user == null || user.getLoginName() == null){
            return result;
        }

        StringBuffer query = new StringBuffer();
        query.append("SELECT l.id AS id,l.location_name AS name,l.location_image_logo AS image_logo,l.location_text_logo AS text_logo,m.location_image_logo as main_logo");
        query.append(" FROM location l, mainlogo m WHERE l.id = m.location_id");

        result = getSession(user.getServerNumber()).createSQLQuery(query.toString())
                .addScalar("id")
                .addScalar("name")
                .addScalar("image_logo", Hibernate.STRING)
                .addScalar("text_logo")
                .addScalar("main_logo", Hibernate.STRING)
                .list();

        return result;
    }

    @Override
    public List<Object[]> getLocationPictures(SystemUser user, Integer location) {
        List<Object[]> result = new ArrayList<Object[]>();
        if(user == null || user.getLoginName() == null){
            return result;
        }
        if(location == null || location <= 0){
            return result;
        }

        StringBuffer query = new StringBuffer();
        query.append("SELECT pc.id,pc.creation_date,pc.picture_name,pc.picture_url,pc.camera_id,pc.rotated,lp.location_id");
        query.append(" FROM picture pc INNER JOIN location_pictures lp ON pc.id=lp.picture_id WHERE lp.location_id=:location");

        result = getSession(user.getServerNumber()).createSQLQuery(query.toString())
                .setInteger("location", location)
                .list();

        return result;
    }
}
