package com.scnsoft.fotafloweb.dao.impl;

import com.scnsoft.fotafloweb.dao.IMerchantDao;
import com.scnsoft.fotafloweb.model.Merchant;
import org.springframework.stereotype.Repository;

@Repository
public class MerchantDao extends AbstractDao<Merchant, Integer> implements IMerchantDao {

    @Override
    protected Class<Merchant> getDomainClass() {
        return Merchant.class;
    }

}
