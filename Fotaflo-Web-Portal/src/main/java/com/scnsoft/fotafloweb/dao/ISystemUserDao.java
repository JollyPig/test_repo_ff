package com.scnsoft.fotafloweb.dao;
import com.scnsoft.fotafloweb.model.SystemUser;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 30.03.2012
 * Time: 12:21:37
 * To change this template use File | Settings | File Templates.
 */
public interface ISystemUserDao extends IDao<SystemUser, Integer> {
    
    public SystemUser getUserByLogin(String login);

    public List<SystemUser> getUserByLoginGroup(String loginGroup);

}
