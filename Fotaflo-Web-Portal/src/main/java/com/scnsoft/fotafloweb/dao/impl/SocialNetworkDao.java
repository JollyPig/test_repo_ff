package com.scnsoft.fotafloweb.dao.impl;

import com.scnsoft.fotafloweb.dao.ISocialNetworkDao;
import com.scnsoft.fotafloweb.model.SocialNetwork;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SocialNetworkDao extends AbstractDao<SocialNetwork, Integer> implements ISocialNetworkDao {

    @Override
    protected Class<SocialNetwork> getDomainClass() {
        return SocialNetwork.class;
    }

    @Override
    public SocialNetwork getByNameAndLocation(String name, Integer location) {
        List<SocialNetwork> list = getSession().createCriteria(getDomainClass())
                .add(Restrictions.eq("name", name))
                .add(Restrictions.eq("location", location))
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .list();
        if(!list.isEmpty()){
            return list.get(0);
        }
        return null;
    }

    @Override
    public List<SocialNetwork> findByLocation(Integer location) {
        List<SocialNetwork> list = getSession().createCriteria(getDomainClass())
                .add(Restrictions.eq("location", location))
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .list();
        return list;
    }
}
