package com.scnsoft.fotafloweb.dao.remote;

import com.scnsoft.fotafloweb.model.SystemUser;

import java.util.List;

public interface IRemoteDao {

    List<Object[]> getPicturesByCode(SystemUser user);

    List<Object[]> getLocations(SystemUser user);

    List<Object[]> getLocationPictures(SystemUser user, Integer locationId);

}
