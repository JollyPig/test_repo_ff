package com.scnsoft.fotafloweb.dao;

import com.scnsoft.fotafloweb.model.Promotion;

import java.util.List;

public interface IPromotionDao extends IDao<Promotion, Integer> {

    Promotion getByNumber(Integer number);

}
