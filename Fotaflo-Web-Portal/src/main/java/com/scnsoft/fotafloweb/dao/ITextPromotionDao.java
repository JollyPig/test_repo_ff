package com.scnsoft.fotafloweb.dao;

import com.scnsoft.fotafloweb.model.TextPromotion;

import java.util.List;

public interface ITextPromotionDao extends IDao<TextPromotion, Integer> {

    TextPromotion getByNumber(Integer number);

}
