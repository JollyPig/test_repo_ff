package com.scnsoft.fotafloweb.dao.impl;

import com.scnsoft.fotafloweb.dao.ITextPromotionDao;
import com.scnsoft.fotafloweb.model.TextPromotion;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TextPromotionDao extends AbstractDao<TextPromotion, Integer> implements ITextPromotionDao {

    @Override
    protected Class<TextPromotion> getDomainClass() {
        return TextPromotion.class;
    }

    @Override
    public TextPromotion getByNumber(Integer number){
        List<TextPromotion> list = getSession().createCriteria(getDomainClass())
                .add(Restrictions.eq("number", number))
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .list();
        if(!list.isEmpty()){
            return list.get(0);
        }
        return null;
    }
}
