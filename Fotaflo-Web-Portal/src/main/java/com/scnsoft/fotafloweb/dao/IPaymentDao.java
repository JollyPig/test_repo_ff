package com.scnsoft.fotafloweb.dao;

import com.scnsoft.fotafloweb.model.Payment;

import java.util.List;

/**
 * Created by Tayna on 01.05.14.
 */
public interface IPaymentDao extends IDao<Payment, Integer> {

    public List<Payment> getPaymentByTransaction(String token);
}
