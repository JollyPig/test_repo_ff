package com.scnsoft.fotafloweb.dao;

import com.scnsoft.fotafloweb.model.IEntity;
import java.util.Collection;
import java.util.List;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */
public interface IBaseDao {

    /**
     * Creates the entity
     *
     * @param entity the entity to be created
     * @return id
     */
    public Integer create(IEntity entity);

    /**
     * Retrieves the entity by the identifier
     *
     * @param entityClass class
     * @param id          the entity identifier
     * @return the entity
     */
    public IEntity retrieve(Class entityClass, Integer id);

    /**
     * Get all existed entities from the database
     *
     * @param entityClass the entity class
     * @return all the entities existed in the database
     */
    public List retrieveAll(Class entityClass);

    /**
     * Generic method used to retreive all objects of the specified type in the specified order.
     *
     * @param entityClass the specified type
     * @param orderBy     order by property
     * @param asc         asc
     * @return the list of retreived objects
     */
    public List retrieveAll(Class entityClass, String orderBy, boolean asc);

    /**
     * Deletes the entity by the identifier
     *
     * @param entityClass class
     * @param id          the entity identifier
     */

    public void delete(Class entityClass, Integer id);

    /**
     * Deletes the entity
     *
     * @param entity The entity to be deleted
     */
    public void delete(IEntity entity);

    /**
     * Deletes all the entitis
     *
     * @param objects The entities to be deleted
     */
    public void deleteAll(Collection objects);

    /**
     * Updates the entity
     *
     * @param entity the entity to be updated
     */
    public void update(IEntity entity);
}
