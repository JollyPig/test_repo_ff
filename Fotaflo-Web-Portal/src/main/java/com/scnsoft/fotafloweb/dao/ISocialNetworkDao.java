package com.scnsoft.fotafloweb.dao;

import com.scnsoft.fotafloweb.model.SocialNetwork;

import java.util.List;

public interface ISocialNetworkDao extends IDao<SocialNetwork, Integer> {

    SocialNetwork getByNameAndLocation(String name, Integer location);

    List<SocialNetwork> findByLocation(Integer location);

}
