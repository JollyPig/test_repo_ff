package com.scnsoft.fotafloweb.dao;

import com.scnsoft.fotafloweb.model.IEntity;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public interface IDao<Entity extends IEntity, Key extends Serializable> {

    /**
     * Creates the entity
     *
     * @param entity the entity to be created
     * @return id
     */
    public Key create(Entity entity);

    /**
     * Retrieves the entity by the identifier
     *
     * @param id          the entity identifier
     * @return the entity
     */
    public Entity get(Key id);

    /**
     * Get all existed entities from the database
     *
     * @return all the entities existed in the database
     */
    public List<Entity> list();

    /**
     * Generic method used to retreive all objects of the specified type in the specified order.
     *
     * @param orderBy     order by property
     * @param asc         asc
     * @return the list of retreived objects
     */
    public List<Entity> getAll(String orderBy, boolean asc);

    /**
     * Deletes the entity by the identifier
     *
     * @param id          the entity identifier
     */

    public void delete(Key id);

    /**
     * Deletes the entity
     *
     * @param entity The entity to be deleted
     */
    public void delete(Entity entity);

    /**
     * Deletes all the entitis
     *
     * @param objects The entities to be deleted
     */
    public void deleteAll(Collection<Entity> objects);

    /**
     * Updates the entity
     *
     * @param entity the entity to be updated
     */
    public void update(Entity entity);
}
