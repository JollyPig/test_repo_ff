package com.scnsoft.fotafloweb.dao;

import com.scnsoft.fotafloweb.model.Merchant;

public interface IMerchantDao extends IDao<Merchant, Integer> {

}
