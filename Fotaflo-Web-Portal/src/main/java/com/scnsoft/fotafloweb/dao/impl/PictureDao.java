package com.scnsoft.fotafloweb.dao.impl;

import com.scnsoft.fotafloweb.dao.IPicturesDao;
import com.scnsoft.fotafloweb.model.Picture;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 08.04.14
 * Time: 17:42
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class PictureDao extends AbstractDao<Picture, Integer> implements IPicturesDao {

    @Override
    protected Class<Picture> getDomainClass() {
        return Picture.class;
    }

    public List<Picture> getUserPictures(String userlogin){

        StringBuffer query = new StringBuffer();
        query.append("select picture from Picture as picture where ");
        query.append("picture.username  = :login");

        List<Picture> result = getSession().createQuery(query.toString())
                .setParameter("login", userlogin)
                .list();
        if (!result.isEmpty()) {
            return result;
        }
        return new ArrayList<Picture>();

    }

    public Picture getPictureByExternalId(int id, int serverId){
        StringBuffer query = new StringBuffer();
        query.append("select picture from Picture as picture where ");
        query.append("picture.pictureId  = :id");
        query.append(" and picture.serverId  = :serverId");

        List<Picture> result = getSession().createQuery(query.toString())
                .setParameter("id", id)
                .setParameter("serverId", serverId)
                .list();
        if (!result.isEmpty()) {
            return result.get(0);
        }
        return null;

    }

    public Picture getLocationPictureByExternalId(int id, int serverId){
        StringBuffer query = new StringBuffer();
        query.append("select picture from Picture as picture where ");
        query.append("picture.locationPictureId  = :id");
        query.append(" and picture.serverId  = :serverId");

        List<Picture> result = getSession().createQuery(query.toString())
                .setParameter("id", id)
                .setParameter("serverId", serverId)
                .list();
        if (!result.isEmpty()) {
            return result.get(0);
        }
        return null;

    }

    public Picture getPictureByUrl(String url){

        StringBuffer query = new StringBuffer();
        query.append("select picture from Picture as picture where ");
        query.append("picture.url  = :url or picture.small_url  = :url");

        List<Picture> result = getSession().createQuery(query.toString())
                .setParameter("url", url)
                .list();
        if (!result.isEmpty()) {
            return result.get(0);
        }
        return null;

    }
}
