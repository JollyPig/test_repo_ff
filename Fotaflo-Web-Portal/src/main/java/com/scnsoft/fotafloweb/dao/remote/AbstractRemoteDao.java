package com.scnsoft.fotafloweb.dao.remote;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.annotation.Resource;
import java.io.Serializable;

abstract public class AbstractRemoteDao {

    @Resource(name = "remoteSessionFactory")
    protected SessionFactory sessionFactory1;

    @Resource(name = "remoteSessionFactory2")
    protected SessionFactory sessionFactory2;

    protected Session getSession(Integer serverId){
        SessionFactory sessionFactory = sessionFactory1;
        if(serverId != null){
            if(serverId == 1){
                sessionFactory = sessionFactory1;
            }else if(serverId == 2){
                sessionFactory = sessionFactory2;
            }
        }
        return sessionFactory.getCurrentSession();
    }

}
