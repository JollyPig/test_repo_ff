package com.scnsoft.fotafloweb.dao.impl;

import com.scnsoft.fotafloweb.dao.IPromotionDao;
import com.scnsoft.fotafloweb.model.Promotion;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PromotionDao extends AbstractDao<Promotion, Integer> implements IPromotionDao {

    @Override
    protected Class<Promotion> getDomainClass() {
        return Promotion.class;
    }

    @Override
    public Promotion getByNumber(Integer number){
        List<Promotion> list = getSession().createCriteria(getDomainClass())
                .add(Restrictions.eq("number", number))
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .list();
        if(!list.isEmpty()){
            return list.get(0);
        }
        return null;
    }
}
