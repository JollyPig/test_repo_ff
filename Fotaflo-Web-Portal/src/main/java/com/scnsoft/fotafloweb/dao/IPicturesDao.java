package com.scnsoft.fotafloweb.dao;

import com.scnsoft.fotafloweb.model.Picture;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 08.04.14
 * Time: 17:54
 * To change this template use File | Settings | File Templates.
 */
public interface IPicturesDao extends IDao<Picture, Integer> {

    List<Picture> getUserPictures(String userlogin);

    Picture getPictureByExternalId(int id, int serverId);

    Picture getLocationPictureByExternalId(int id, int serverId);

    Picture getPictureByUrl(String url);

}
