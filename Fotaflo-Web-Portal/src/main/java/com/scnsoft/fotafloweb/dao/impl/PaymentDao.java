package com.scnsoft.fotafloweb.dao.impl;

import com.scnsoft.fotafloweb.dao.IPaymentDao;
import com.scnsoft.fotafloweb.model.Payment;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Created by Tayna on 01.05.14.
 */
@Repository
public class PaymentDao extends AbstractDao<Payment, Integer> implements IPaymentDao {

    @Override
    protected Class<Payment> getDomainClass() {
        return Payment.class;
    }

    public List<Payment> getPaymentByTransaction(String token) {
        List<Payment> payments = getSession()
                .createQuery("from Payment payment where payment.token = :token")
                .setString("token", token)
                .list();

        return payments;
    }
}
