package com.scnsoft.fotafloweb.social;

public class Connection {

    private String service;

    private boolean connected;

    private String name;

    public Connection(String service, org.springframework.social.connect.Connection connection) {
        if(service == null || service.trim().isEmpty()){
            throw new IllegalArgumentException("Field 'service' cannot be null");
        }
        this.service = service;

        if(connection != null && !connection.hasExpired() && connection.test()){
            this.name = connection.getDisplayName();
            this.connected = true;
        }else{
            this.connected = false;
        }
    }

    public String getService() {
        return service;
    }

    public boolean isConnected() {
        return connected;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Connection{" +
                "service='" + service + '\'' +
                ", connected=" + connected +
                ", name='" + name + '\'' +
                '}';
    }
}
