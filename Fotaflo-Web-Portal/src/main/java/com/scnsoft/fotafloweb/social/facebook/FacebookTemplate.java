package com.scnsoft.fotafloweb.social.facebook;

import org.apache.log4j.Logger;
import org.springframework.core.io.Resource;
import org.springframework.social.ResourceNotFoundException;
import org.springframework.social.facebook.api.*;

import java.util.ArrayList;
import java.util.List;

public class FacebookTemplate {
    protected static final Logger logger = Logger.getLogger(FacebookTemplate.class);

    private Facebook facebook;

    public FacebookTemplate(Facebook facebook) {
        if(facebook == null){
            throw new IllegalArgumentException("Facebook service cannot be null");
        }
        this.facebook = facebook;
    }

    public Facebook getApi(){
        return this.facebook;
    }

    public boolean isAuthorized(){
        return this.facebook.isAuthorized();
    }

    public void shareImages(String albumCaption, String albumDescription, List<Resource> images, String message, List<String> tags, List<String> hashtags, String address){
        String defaultAlbumCaption = "Fotaflo Photo Album";   // move to properties file
        String user = this.getUserId();

        if(albumCaption == null || albumCaption.trim().isEmpty()){
            albumCaption = defaultAlbumCaption;
        }
        String albumId = this.getAlbumByName(user, albumCaption);
        if(albumId == null){
            albumId = this.createAlbum(albumCaption, albumDescription);
        }

        Album album = this.getAlbum(albumId);

        this.postPhotos(albumId, images, convertHashtagsToString(hashtags));

        this.postFeed(user, message, album.getLink(), tags, hashtags, this.getPlace(address), /*Post.Privacy.SELF*/null);
    }

    public void shareImage(String caption, Resource image, String message){
        String photoId = this.postPhoto(image, caption);

        Photo photo = this.getPhoto(photoId);

        String user = this.getUserId();

        System.out.println("photolink: " + photo.getLink());

        this.postFeed(user, message, photo.getLink(), null, null, null, Post.Privacy.SELF);
    }

    public int getFriendsCount(){
        String user = this.getUserId();
        logger.info(user);
        List<String> list = facebook.friendOperations().getFriendIds(user);
        logger.info(list);
        if(list != null){
            return list.size();
        }
        return 0;
    }

    public String getUserId(){
        return facebook.userOperations().getUserProfile().getId();
    }

    public String getUserName(){
        return facebook.userOperations().getUserProfile().getUsername();
    }

    public String createAlbum(String name, String description){
        // TODO setup privilegy
        return facebook.mediaOperations().createAlbum(name, description);
    }

    public String getAlbumByName(String user, String name){
        if(name != null){
            List<Album> albums = facebook.mediaOperations().getAlbums(user);
            for(Album a: albums){
                System.out.println(a.getName());
                if(name.equals(a.getName())){
                    return a.getId();
                }
            }
        }
        return null;
    }

    public Album getAlbum(String id){
        return facebook.mediaOperations().getAlbum(id);
    }

    public Photo getPhoto(String id){
        return facebook.mediaOperations().getPhoto(id);
    }

    public String postPhoto(String albumId, Resource image){
        return facebook.mediaOperations().postPhoto(albumId, image);
    }

    public String postPhoto(Resource image, String caption){
        return facebook.mediaOperations().postPhoto(image, caption);
    }

    public String postPhoto(String albumId, Resource image, String caption){
        return facebook.mediaOperations().postPhoto(albumId, image, caption);
    }

    public List<String> postPhotos(String albumId, List<Resource> images){
        if(images != null && images.size() > 0){
            List<String> ids = new ArrayList<String>();
            String id = null;
            for(Resource image: images){
                id = postPhoto(albumId, image);
                ids.add(id);
            }
            return ids;
        }
        return null;
    }

    public List<String> postPhotos(String albumId, List<Resource> images, String caption){
        if(images != null && images.size() > 0){
            List<String> ids = new ArrayList<String>();
            String id = null;
            for(Resource image: images){
                id = postPhoto(albumId, image, caption);
                ids.add(id);
            }
            return ids;
        }
        return null;
    }

    public String postFeed(String userId, String message, String link, List<String> tags, List<String> hashtags, String placeId, Post.Privacy privacy){
        PostData data = new PostData(userId);

        if(link != null && !link.trim().isEmpty()){
            data.link(link);
        }

        if(hashtags != null && !hashtags.isEmpty()){
            if(message == null){
                message = "";
            }
            message += convertHashtagsToString(hashtags);
        }

        if(message != null && !message.trim().isEmpty()){
            data.message(message);
        }

        tags = getCheckedTags(tags);

        if(tags != null && !tags.isEmpty()){
            String[] t = new String[tags.size()];
            int i = 0;
            for(String tag: tags){
                t[i++] = tag;
            }
            data.tags(t);
        }

        if(placeId != null && !placeId.trim().isEmpty()){
            data.place(placeId);
        }

        if(privacy != null){
            data.privacy(privacy);
        }

        return facebook.feedOperations().post(data);
    }

    protected String convertHashtagsToString(List<String> hashtags){
        String result = "";
        if(hashtags != null && !hashtags.isEmpty()){
            for(String h: hashtags){
                result += " #" + h;
            }
        }
        return result;
    }

    protected List<String> getCheckedTags(List<String> tags){
        List<String> list = new ArrayList<String>();
        if(tags != null && !tags.isEmpty()){
            for(String tag: tags){
                if(getPageId(tag) != null){
                    list.add(tag);
                }
            }
        }

        return list;
    }

    public boolean isTag(String tag){
       if(tag == null){
           return false;
       }
        return getPageId(tag) != null;
    }

    public String getPageId(String name){
        String id = null;
        try{
            Page page = facebook.pageOperations().getPage(name);
            id = page.getId();
        }catch(ResourceNotFoundException e){
            logger.warn(e.getMessage());
        }
        return id;
    }

    public String getPlace(String place){
        if(place == null){
            return null;
        }
        if(place.matches("\\d+")){
            return place;
        }

        // TODO
        /*place = "Canada";

        List<Page> pages = facebook.placesOperations().search(place, 49.652, -109.521, 12000);
        System.out.println(pages);
        String o = "";
        for(Page p: pages){
            o += "======================"+'\n';
            o += p.getId() +'\n';
            o += p.getName() +'\n';
            o += p.getLocation().getCountry() +'\n';
            o += p.getLocation().getCity() +'\n';
            o += p.getLocation().getStreet() +'\n';
            o += p.getLocation().getDescription() +'\n';
        }
        System.out.println(o);
        if(pages != null && !pages.isEmpty()){
            return pages.get(0).getId();
        }*/
        return null;
    }
}
