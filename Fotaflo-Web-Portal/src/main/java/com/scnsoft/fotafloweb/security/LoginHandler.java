/*
 * Copyright by JSC Belgazprombank 2013
 */
package com.scnsoft.fotafloweb.security;

import com.scnsoft.fotafloweb.model.SystemUser;
import com.scnsoft.fotafloweb.service.IUserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.io.Writer;
import java.util.Locale;

public class LoginHandler implements AuthenticationSuccessHandler, AuthenticationFailureHandler {
    protected final static Logger logger = Logger.getLogger(LoginHandler.class);

    private String defaultTargetUrl;
    private String authenificationFailureUrl;

    @Autowired
    private MessageSource messageSource;

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {

		HttpServletResponseWrapper responseWrapper = new HttpServletResponseWrapper(response);

        boolean mobile = Boolean.parseBoolean(request.getParameter("mobile"));
        request.getSession().setAttribute("mobile", mobile);
        logger.info("Client is mobile: "+mobile);

        boolean ajax = Boolean.parseBoolean(request.getParameter("ajax"));
        String redirectUrl = request.getContextPath() + (defaultTargetUrl != null ? defaultTargetUrl : "");

        request.getSession().setAttribute("newSession", true);

        if(ajax){
            Writer out = responseWrapper.getWriter();

            out.write(new LoginStatus(true, redirectUrl, null).toString());
            out.close();
        }else{
		    response.sendRedirect(redirectUrl);
        }
	}

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {
		HttpServletResponseWrapper responseWrapper = new HttpServletResponseWrapper(response);

        boolean ajax = Boolean.parseBoolean(request.getParameter("ajax"));
        String redirectUrl = request.getContextPath() + (authenificationFailureUrl != null ? authenificationFailureUrl : "");

        if(ajax){
            Writer out = responseWrapper.getWriter();

            out.write(new LoginStatus(false, redirectUrl, getErrorMessage(request.getParameter("type"))).toString());
            out.close();
        }else{
            response.sendRedirect(redirectUrl);
        }
	}

    public void setDefaultTargetUrl(String defaultTargetUrl) {
        this.defaultTargetUrl = defaultTargetUrl;
    }

    public void setAuthenificationFailureUrl(String authenificationFailureUrl) {
        this.authenificationFailureUrl = authenificationFailureUrl;
    }

    protected String getErrorMessage(String type){
        String source = "error.login";
        if(type != null && type.toLowerCase().trim().equals("code")){
            source = "error.code";
        }
        return messageSource.getMessage(source, null, Locale.ROOT);
    }

    protected class LoginStatus{
        private boolean success;
        private String errorMessage;
        private String redirectUrl;

        public LoginStatus(boolean success, String redirectUrl, String errorMessage) {
            this.success = success;
            this.redirectUrl = redirectUrl;
            this.errorMessage = errorMessage;
        }

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public String getErrorMessage() {
            return errorMessage;
        }

        public void setErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        public String getRedirectUrl() {
            return redirectUrl;
        }

        public void setRedirectUrl(String redirectUrl) {
            this.redirectUrl = redirectUrl;
        }

        @Override
        public String toString() {
            return "{\"success\":" + success + ", \"redirectUrl\":\"" + redirectUrl + "\"" +
                    (errorMessage != null ? ", \"errorMessage\":\"" + errorMessage + "\"" : "") +
                    "}";
        }
    }

}
