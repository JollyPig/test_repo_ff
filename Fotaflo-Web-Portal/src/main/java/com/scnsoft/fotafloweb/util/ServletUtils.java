package com.scnsoft.fotafloweb.util;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.net.URL;

public class ServletUtils {
    protected final static Logger logger = Logger.getLogger(ServletUtils.class);

    public static String getContextPath(HttpServletRequest request){
        try{
            URL url = new URL(request.getScheme(), request.getServerName(), request.getServerPort(), request.getContextPath());
            return url.toString();
        }catch(MalformedURLException e){
            logger.error(e.getMessage(), e);
        }
        return null;
    }

}
