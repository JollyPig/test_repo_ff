package com.scnsoft.fotafloweb.util;

/**
 * Created by Tayna on 02.05.14.
 */

import org.springframework.security.authentication.encoding.Md5PasswordEncoder;

import javax.crypto.*;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 09.04.2012
 * Time: 5:36:05
 * To change this template use File | Settings | File Templates.
 */
public class PasswordEncryption {

    // Salt
    //private static byte[] salt = { (byte) 0xc7, (byte) 0x73, (byte) 0x21,
    //		(byte) 0x8c, (byte) 0x7e, (byte) 0xc8, (byte) 0xee, (byte) 0x99 };
    private static byte[] salt = { (byte) 0xc7, (byte) 0x73, (byte) 0x21,
            (byte) 0x8c, (byte) 0x7e, (byte) 0xc8, (byte) 0xee, (byte) 0x99 };

    // Iteration count
    private static int count = 30;

    private static byte[] clearText = "Secret".getBytes();

    public static void main(String[] args) {
        try {
            PasswordEncryption test = new PasswordEncryption();
            // byte[] passwd = test.encryptPassword("ArOcGAx8uUH0Jon43nj995.VeDCVAekzqr3OOnSYx326WGUe2lM9.prG");byte[] passwd = test.encryptPassword("ArOcGAx8uUH0Jon43nj995.VeDCVAekzqr3OOnSYx326WGUe2lM9.prG");
            Map newMap = new HashMap<String,String>();
            int k =0;
            for(int i =0;i<300000;i++){
                byte[] passwd = test.encryptPassword(new BigInteger(20, new SecureRandom()).toString(32)+new Date().getTime());
                // System.out.println("My encrypted: " + passwd);
                String pw= passwd.toString();
                pw = pw.substring(pw.length()-7,pw.length());
                // System.out.println("pwCut: " + pw);
                if(newMap.containsKey(pw)){
                    //System.out.println("pw!: " + pw);
                    k++;
                }else{
                    newMap.put(pw,pw);
                }
                //String passwddec  = test.decryptPassword(passwd);
                // System.out.println("My decrypted: " + passwddec);
            }
            System.out.println("Ales gut: " + k);

            Md5PasswordEncoder md5PasswordEncoder = new Md5PasswordEncoder();
            //String paswd = md5PasswordEncoder.encodePassword("admin", "mysalt");
            //System.out.println("MD5 " + paswd);

            Map newMap2 = new HashMap<String,String>();
            int k2 =0;
            for(int i =0;i<1000000;i++){
                //   String paswdS = md5PasswordEncoder.encodePassword(new BigInteger(100, new SecureRandom()).toString(32)+new Date().getTime(), "mysaltUHKJHKHJI267767787878");
                // System.out.println("My encrypted: " + passwd);
                String paswdS = new BigInteger(35, new SecureRandom()).toString(32);
                //paswdS = paswdS.substring(paswdS.length()-7,paswdS.length());
                // System.out.println("pwCut: " + pw);
                if(newMap2.containsKey(paswdS)){
                    System.out.println("pw!: " + paswdS);
                    k2++;
                }else{
                    newMap2.put(paswdS,paswdS);
                }
                //String passwddec  = test.decryptPassword(passwd);
                // System.out.println("My decrypted: " + passwddec);
            }
            System.out.println("Ales gut: " + k2);




            /*test.sample();*/
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private Cipher pbeCipher;
    private PBEParameterSpec pbeParamSpec;

    public PasswordEncryption() throws NoSuchAlgorithmException,
            NoSuchPaddingException {
        // Create PBE parameter set
        pbeParamSpec = new PBEParameterSpec(salt, count);

        // Create PBE Cipher
        pbeCipher = Cipher.getInstance("PBEWithMD5AndDES");
    }

    public byte[] encryptPassword(String password) throws Exception {
        PBEKeySpec pbeKeySpec;
        SecretKeyFactory keyFac;

        String pass = "Password";
        pbeKeySpec = new PBEKeySpec( pass.toCharArray());
        keyFac = SecretKeyFactory.getInstance("PBEWithMD5AndDES");
        SecretKey pbeKey = keyFac.generateSecret(pbeKeySpec);

        byte[] cipherText = encrypt(pbeKey, password.getBytes());

        return cipherText;

    }

    public String decryptPassword(byte[] password) throws Exception {
        PBEKeySpec pbeKeySpec;
        SecretKeyFactory keyFac;

        String pass = "Password";
        pbeKeySpec = new PBEKeySpec( pass.toCharArray());
        keyFac = SecretKeyFactory.getInstance("PBEWithMD5AndDES");
        SecretKey pbeKey = keyFac.generateSecret(pbeKeySpec);

        byte[] cipherText = decrypt(pbeKey, password);

        return new String(cipherText) ;

    }

    private byte[] encrypt(SecretKey pbeKey, byte[] data)
            throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, InvalidAlgorithmParameterException,
            IllegalBlockSizeException, BadPaddingException {
        // Initialize PBE Cipher with key and parameters
        pbeCipher.init(Cipher.ENCRYPT_MODE, pbeKey, pbeParamSpec);

        // Our cleartext

        // Encrypt the cleartext
        byte[] ciphertext = pbeCipher.doFinal(data);

        return ciphertext;
    }

    private byte[] decrypt(SecretKey pbeKey, byte[] data)
            throws InvalidKeyException, InvalidAlgorithmParameterException,
            IllegalBlockSizeException, BadPaddingException {
        // Initialize PBE Cipher with key and parameters
        pbeCipher.init(Cipher.DECRYPT_MODE, pbeKey, pbeParamSpec);

        // Our cleartext

        // Decrypt the ciphertext
        byte[] clearText = pbeCipher.doFinal(data);

        return clearText;

    }


}
