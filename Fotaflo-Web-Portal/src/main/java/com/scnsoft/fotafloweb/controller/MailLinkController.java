package com.scnsoft.fotafloweb.controller;

import com.scnsoft.fotafloweb.analytics.util.CookieManager;
import com.scnsoft.fotafloweb.bean.LocationBean;
import com.scnsoft.fotafloweb.model.SystemUser;
import com.scnsoft.fotafloweb.service.IAnalyticsService;
import com.scnsoft.fotafloweb.service.ILocationService;
import com.scnsoft.fotafloweb.service.IMailService;
import com.scnsoft.fotafloweb.service.IUserService;
import com.scnsoft.fotafloweb.util.ServletUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Controller
@RequestMapping("/main")
public class MailLinkController {
    protected static Logger logger = Logger.getLogger( MailLinkController.class );

    public static final String REDIRECT_URL_PART = "/pictures/auth/autoLogin?username=";

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private IMailService mailService;

    @Autowired
    private IUserService userService;

    @Autowired
    private ILocationService locationService;

    @Autowired
    private IAnalyticsService analyticsService;

    @RequestMapping(value = "/mail", method = RequestMethod.POST)
    public
    @ResponseBody
    ResponseEntity<?> shareImage(@RequestParam("message") String message,
                                 @RequestParam("sharemails") String sharemails,
                                 @RequestParam(value = "imageIds", required = false) List<String> ids,
                                 ModelMap model, HttpServletRequest request) {
        logger.info("Message: " + message + ", mails: " + sharemails + ", count of images: " + (ids != null ? ids.size() : 0));
        Map<String, Object> result = new HashMap<String, Object>();

        String contextPath = ServletUtils.getContextPath(request);

        //final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getCredentials().toString();
        logger.info("Current User: " + currentUser);
        String userBase = currentUser;
        SystemUser user = userService.getUserByLogin(currentUser);
        if (user != null && user.getLoginGroup() != null && !user.getLoginGroup().isEmpty()) {
            userBase = user.getLoginGroup();
        }

        LocationBean location = locationService.getLocation(user.getLocationId());

        String locationName = "", locationFooter = "", fromEmail ="";

        if(location != null){
            locationName = location.getName() != null ? location.getName() : "";
            locationFooter = location.getEmailFooter() != null ? location.getEmailFooter() : "";
            fromEmail= location.getFromEmail()!=null?location.getFromEmail(): "";
        }

        String link, login, subject, messageBody;
        if (!sharemails.isEmpty()) {
            String[] emails = sharemails.split(",");
            List<String> userLogins = userService.generateNewUserLogins(userBase, emails.length, getImageListString(ids));
            int j = 0;
            for (String email : emails) {
                login = userLogins.get(j);
                link = contextPath + REDIRECT_URL_PART + login;
                subject = messageSource.getMessage("mail.subject", new Object[]{locationName}, Locale.ROOT);
                messageBody = messageSource.getMessage("mail.template", new Object[]{message, link, login, locationFooter}, Locale.ROOT);

                mailService.sendMail(fromEmail,subject, messageBody, email);
                logger.info("Sending to  " + email + "  " + messageBody + " from" + fromEmail );
                j++;
            }
        }

        result.put("success", true);

        analyticsService.trackEvent(CookieManager.getClientId(request.getCookies()), "share", "success", "mail", ids.size());
        return new ResponseEntity<Map>(result, HttpStatus.OK);
    }

    private String getImageListString(List<String> images) {
        String imageString = "";
        for (String image : images) {
            imageString = imageString.length() == 0 ? image : imageString + ", " + image;
        }
        return imageString;
    }

}
