package com.scnsoft.fotafloweb.controller;

import com.scnsoft.fotafloweb.bean.LocationBean;
import com.scnsoft.fotafloweb.bean.PictureBean;
import com.scnsoft.fotafloweb.model.SystemUser;
import com.scnsoft.fotafloweb.service.ILocationService;
import com.scnsoft.fotafloweb.service.IPictureService;
import com.scnsoft.fotafloweb.service.IShareService;
import com.scnsoft.fotafloweb.service.IUserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */

@Controller
@RequestMapping("/main")
public class MainController
{
    protected static Logger logger = Logger.getLogger( MainController.class );

    @Autowired
    private IPictureService pictureService;

    @Autowired
    private IUserService userService;

    @Autowired
    private ILocationService locationService;

    @Autowired
    private IShareService shareService;

    /**
     * Handles and retrieves the common JSP page that everyone can see
     *
     * @return the name of the JSP page
     */

    @RequestMapping(value = "/common", method = RequestMethod.GET)
    public String getCommonPage( ModelMap model, HttpServletRequest request, HttpServletResponse response )
    {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        SystemUser systemUser = userService.getUserByLogin(currentUser);

        if(systemUser == null){
            return "index";
        }
        logger.info("systemUser - "+systemUser.getLoginName());

        clearCookies(request, response);

        if(request.getSession().getAttribute("username") == null){
            request.getSession().setAttribute("username", currentUser);
        }
        if(request.getSession().getAttribute("location") == null){
            request.getSession().setAttribute("location", systemUser.getLocationId());
            if(systemUser.getLocationId() != null){
                LocationBean loc = locationService.getLocation(systemUser.getLocationId());
                if(loc != null){
                    request.getSession().setAttribute("locationName", loc.getName());
                }
            }
        }

        List<PictureBean> pictures = new ArrayList<PictureBean>();
        if(request.isUserInRole("ROLE_ADMIN")){
            return "admin";
        }else if(request.isUserInRole("ROLE_USER")){
            return "analytics";
        }else{
         //   if(systemUser!=null && systemUser.getLoginGroup()!=null && !systemUser.getLoginGroup().isEmpty()){

            if(systemUser.getAccessCode()!=null && systemUser.getAccessCode().equals("B")){
                String price = systemUser.getPrice()==null?"0":systemUser.getPrice();
                model.put(PayPalController.payedProperty, false);
                model.put(PayPalController.successProperty, true);
                model.put(PayPalController.msgProperty, "");
                model.put("price",price);

                /*
                String userForPictures = systemUser.getLoginGroup();
              //  String pictureListString = systemUser.getPictureIds();
              //  pictures =  pictureService.getPicturesForCodeUsers(userForPictures, pictureListString);
                */

                //Set<Picture> pictureSet = systemUser.getPictures();
                pictures = pictureService.getPictures(currentUser);//getPictureBeanListFromPicturesSet(pictureSet);

                model.addAttribute("pictures", pictures);
                return "catalogNotLogin";
            }

            pictures =  pictureService.getPictures(currentUser);
            model.addAttribute("pictures", pictures);
            return "catalog";
        }


    }

    @RequestMapping(value = "/single", method = RequestMethod.GET)
    public String getSlideshowPage( ModelMap model, HttpServletRequest request, HttpServletResponse response )
    {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        SystemUser systemUser = userService.getUserByLogin(currentUser);

        if(systemUser == null){
            return "index";
        }

        clearCookies(request, response);

        String userForPictures = currentUser;
        List<PictureBean> pictures = new ArrayList<PictureBean>();
        if(request.isUserInRole("ROLE_ADMIN")){
            return "admin";
        }else if(request.isUserInRole("ROLE_USER")){
            return "client";
        }else{
            pictures =  pictureService.getPictures(userForPictures);
            model.addAttribute("pictures", pictures);
            return "singleFotorama";
        }


    }

    protected void clearCookies(HttpServletRequest request, HttpServletResponse response){
        Boolean isNewSession = (Boolean)request.getSession().getAttribute("newSession");
        String sessionId = null;
        if(request.getCookies() != null){
            for(Cookie cookie: request.getCookies()){
                if(cookie.getName().equals("last_sessionid")){
                    sessionId = cookie.getValue();
                    break;
                }
            }
        }

        Cookie cookie;
        if((isNewSession != null && isNewSession) || (sessionId != null && !sessionId.equals(request.getRequestedSessionId()))){
            cookie = new Cookie("selected_photos", "");
            cookie.setMaxAge(0);
            //cookie.setPath("/");
            response.addCookie(cookie);

            //clearSocialNetworkConnections();
            request.getSession().setAttribute("newSession", false);
        }

        cookie = new Cookie("last_sessionid", request.getRequestedSessionId());
        response.addCookie(cookie);
    }

    protected void clearSocialNetworkConnections(){
        shareService.removeAllConnections();
    }

    public void setUserService(IUserService userService) {
        this.userService = userService;
    }
}
