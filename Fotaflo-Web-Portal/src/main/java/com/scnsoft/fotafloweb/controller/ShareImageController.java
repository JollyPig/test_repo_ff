package com.scnsoft.fotafloweb.controller;

import com.scnsoft.fotafloweb.analytics.util.CookieManager;
import com.scnsoft.fotafloweb.bean.ShareStatusBean;
import com.scnsoft.fotafloweb.model.SocialNetwork;
import com.scnsoft.fotafloweb.model.SocialNetworkType;
import com.scnsoft.fotafloweb.service.IAnalyticsService;
import com.scnsoft.fotafloweb.service.IShareService;
import com.scnsoft.fotafloweb.service.ISocialNetworkService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/main")
public class ShareImageController {
    protected static final Logger logger = Logger.getLogger(ShareImageController.class);

    @Inject
    private Provider<ConnectionRepository> connectionRepositoryProvider;

    @Autowired
    private IShareService shareService;

    @Autowired
    private ISocialNetworkService socialNetworkService;

    @Autowired
    private IAnalyticsService analyticsService;

    @Value("${facebook.clientId}")
    private String facebookClientId;

    @RequestMapping(value = "/connections", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<?> availableConnections(){
        List<com.scnsoft.fotafloweb.social.Connection> list = shareService.availableConnections();

        return new ResponseEntity<List>(list, HttpStatus.OK);
    }

    @RequestMapping(value = "/share", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<?> shareImage(@RequestParam("message") String message,
                                 @RequestParam(value = "services") List<SocialNetworkType> serviceTypes,
                                 @RequestParam(value = "images") List<String> images,
                                 @RequestParam(value = "tags", required = false) List<String> tags,
                                 ModelMap model, HttpServletRequest request ){
        MultiValueMap<String, Connection<?>> conns = getConnectionRepository().findAllConnections();

        List<Integer> imgIds = new ArrayList<Integer>();
        Integer id;
        for(String img: images){
            id = new Integer(img);
            if(id != null){
                imgIds.add(id);
            }
        }

        Map<String, String> tagMap = new HashMap<String, String>();
        String tag;
        int i;
        for(String t: tags){
            i = t.indexOf(':');
            tag = t.substring(0, (i < 0) ? 0 : i).trim();
            if(!tag.isEmpty() && (++i < t.length())){
                tagMap.put(tag, t.substring(i));
            }
        }

        logger.info("SocialNetworks: "+serviceTypes+", Message: "+message+", Images: "+imgIds+", Tags: "+tagMap);
        Map<String, Object> result = new HashMap<String, Object>();

        List<ShareStatusBean> statuses;

        statuses = shareService.shareImage(serviceTypes, imgIds, message, tagMap);

        boolean success = true;
        String errorMessage = "";
        for(ShareStatusBean status: statuses){
            if(status.isSuccess()){
                analyticsService.trackSocial(CookieManager.getClientId(request.getCookies()), "share", "success", status.getType(), imgIds.size(), status.getAccount(), status.getFriends());
            }else{
                success = false;
                errorMessage += status.getType() + ": " + status.getError().getMessage() + ", ";
                analyticsService.trackSocial(CookieManager.getClientId(request.getCookies()), "share", "error", status.getType(), 0, null, 0, status.getError().getMessage());
            }
        }
        result.put("success", success);
        result.put("message", errorMessage);

        /*try{
        }catch (ShareImageException e){
            result.put("success", false);
            result.put("message", e.getMessage());
            result.put("data", e.getErrors());

            for(SocialNetworkType sn: serviceTypes){
                if(e.getErrors().containsKey(sn.name())){
                    analyticsService.trackEvent(CookieManager.getClientId(request.getCookies()), "share", "error", sn.name(), 0, e.getErrors().get(sn.name()).getMessage());
                }else{
                    analyticsService.trackEvent(CookieManager.getClientId(request.getCookies()), "share", "success", sn.name(), imgIds.size());
                }
            }
            return new ResponseEntity<Map>(result, HttpStatus.OK);
        }*/

//        result.put("success", true);

        /*for(SocialNetworkType sn: serviceTypes){
            analyticsService.trackEvent(CookieManager.getClientId(request.getCookies()), "share", "success", sn.name(), imgIds.size());
        }*/
        return new ResponseEntity<Map>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/share/settings", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<?> shareImage(ModelMap model, HttpServletRequest httpServletRequest ){
        Map<String, SocialNetwork> settings = socialNetworkService.getSocialNetworkSettings();

        if(logger.isDebugEnabled()){
            logger.debug("Settings: "+settings);
        }

        return new ResponseEntity<Map<String, SocialNetwork>>(settings, HttpStatus.OK);
    }

    @RequestMapping(value = "/share/facebook", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<?> getFacebook(ModelMap model, HttpServletRequest httpServletRequest ){
        Map<String, Object> response = new HashMap<String, Object>();

        response.put("appId", facebookClientId);

        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
    }

    private ConnectionRepository getConnectionRepository() {
        return connectionRepositoryProvider.get();
    }

}
