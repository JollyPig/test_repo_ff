package com.scnsoft.fotafloweb.controller;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 22.04.14
 * Time: 17:09
 * To change this template use File | Settings | File Templates.
 */

import com.scnsoft.fotafloweb.bean.LocationBean;
import com.scnsoft.fotafloweb.bean.PictureBean;
import com.scnsoft.fotafloweb.model.Merchant;
import com.scnsoft.fotafloweb.model.Payment;
import com.scnsoft.fotafloweb.model.SystemUser;
import com.scnsoft.fotafloweb.service.*;
import com.scnsoft.fotafloweb.util.ServletUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import urn.ebay.api.PayPalAPI.PayPalAPIInterfaceServiceService;
import urn.ebay.api.PayPalAPI.SetExpressCheckoutReq;
import urn.ebay.api.PayPalAPI.SetExpressCheckoutRequestType;
import urn.ebay.api.PayPalAPI.SetExpressCheckoutResponseType;
import urn.ebay.apis.CoreComponentTypes.BasicAmountType;
import urn.ebay.apis.eBLBaseComponents.AddressType;
import urn.ebay.apis.eBLBaseComponents.CountryCodeType;
import urn.ebay.apis.eBLBaseComponents.CurrencyCodeType;
import urn.ebay.apis.eBLBaseComponents.ErrorType;
import urn.ebay.apis.eBLBaseComponents.PaymentActionCodeType;
import urn.ebay.apis.eBLBaseComponents.PaymentDetailsType;
import urn.ebay.apis.eBLBaseComponents.SellerDetailsType;
import urn.ebay.apis.eBLBaseComponents.SetExpressCheckoutRequestDetailsType;
import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentReq;
import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentRequestType;
import urn.ebay.api.PayPalAPI.DoExpressCheckoutPaymentResponseType;
import urn.ebay.apis.eBLBaseComponents.DoExpressCheckoutPaymentRequestDetailsType;
import urn.ebay.apis.eBLBaseComponents.PaymentInfoType;

import javax.servlet.http.HttpServletRequest;

import java.util.*;

@Controller
public class PayPalController{

    protected static Logger logger = Logger.getLogger( PayPalController.class );

    @Autowired
    private IUserService userService;

    @Autowired
    private IPayPalService payPalService;

    @Autowired
    private IPictureService pictureService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private IMailService mailService;

    @Autowired
    private ILocationService locationService;

    public final static String successProperty = "success";
    public final static String msgProperty = "msg";
    public final static String payedProperty = "payed";


    private final static String statusRegistered = "REGISTERED";
    private final static String statusComplete = "COMPLETE";
    private final static String statusErrorRegistered = "ERROR_REGISTERED";
    private final static String statusErrorComplete = "ERROR_COMPLETE";

    private static String CONFIRM_REDIRECT_URL = "";
    private static String CANCEL_REDIRECT_URL = "";

    private static String PAYPAL_REDIRECT_URL = "";

    @Value("${http.ConnectionTimeOut}")
    private String connectionTimeOut;

    @Value("${http.Retry}")
    private String retry;

    @Value("${http.ReadTimeOut}")
    private String readTimeOut;

    @Value("${http.MaxConnection}")
    private String maxConnection;

    @Value("${http.IPAddress}")
    private String iPAddress;

    @Value("${http.UseProxy}")
    private String useProxy;

    @Value("${http.ProxyPort}")
    private String proxyPort;

    @Value("${http.ProxyHost}")
    private String proxyHost;

    @Value("${http.ProxyUserName}")
    private String proxyUserName;

    @Value("${http.ProxyPassword}")
    private String proxyPassword;

    @Value("${http.GoogleAppEngine}")
    private String googleAppEngine;

    @Value("${sandbox.service.RedirectURL}")
    private String sandboxServiceRedirectURL;

    @Value("${sandbox.service.EndPoint.PayPalAPI}")
    private String sandboxServiceEndPointPayPalAPI;

    @Value("${sandbox.service.EndPoint.PayPalAPIAA}")
    private String sandboxServiceEndPointPayPalAPIAA;

    @Value("${sandbox.service.EndPoint.Permissions}")
    private String sandboxServiceEndPointPermissions;

    @Value("${sandbox.fotaflo.paypal.url}")
    private String sandboxFotafloPaypalUrl;

    @Value("${live.service.RedirectURL}")
    private String liveServiceRedirectURL;

    @Value("${live.service.EndPoint.PayPalAPI}")
    private String liveServiceEndPointPayPalAPI;

    @Value("${live.service.EndPoint.PayPalAPIAA}")
    private String liveServiceEndPointPayPalAPIAA;

    @Value("${live.service.EndPoint.Permissions}")
    private String liveServiceEndPointPermissions;

    @Value("${live.fotaflo.paypal.url}")
    private String liveFotafloPaypalUrl;

    /*static private String currency = "USD";
    static private String picturePrice = "5";*/


    public boolean setRedirectUrls(String context) // fixme
    {
        CONFIRM_REDIRECT_URL = context + "/pictures/main/commonPayed";
        CANCEL_REDIRECT_URL = context + "/pictures/main/commonPayed";
        return true;
    }

    public void setPayPalRedirectUrls(String payPalRedirectUrl)
    {
        PAYPAL_REDIRECT_URL = payPalRedirectUrl;
    }

    @RequestMapping(value = "registerPayment.json")
    public
    @ResponseBody
    Map<String, ? extends Object> getRegisterPayment( HttpServletRequest request,
                                                      @RequestParam(value = "totalPrice", required = false) String totalPrice,
                                                      @RequestParam(value = "pictureIds", required = false) String pictureIds,
                                                      @RequestParam(value = "pictureEmail", required = false) String pictureEmail,
                                                      @RequestParam(value = "picturesSize", required = false) String picturesSize)
            throws Exception
    {
        String contextPath = ServletUtils.getContextPath(request);

        setRedirectUrls(contextPath);
        logger.info("totalPrice " + totalPrice);
        HashMap<String, Object> modelMap = new HashMap<String, Object>();
        if ( totalPrice == null || totalPrice.equals( "0" ) )
        {
            modelMap.put( successProperty, false );
            modelMap.put( msgProperty, "Total price error. Please contact administrator." );
            logger.warn( "Total price error. Please contact administrator." );
            return modelMap;
        }


        if ( pictureEmail == null || pictureEmail.equals( "" ) )
        {
            modelMap.put( successProperty, false );
            modelMap.put( msgProperty, "The email was entered incorrectly." );
            logger.warn( "The email was entered incorrectly.. Please contact administrator." );
            return modelMap;
        }



        Double total = null;
        try
        {
            total = Double.parseDouble( totalPrice );
        }
        catch ( NumberFormatException e )
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        if ( total == null )
        {
            modelMap.put( successProperty, false );
            modelMap.put( msgProperty, "Total price is incorrect. Please contact administrator." );
            logger.warn( "Total price is incorrect. Please contact administrator." );
            return modelMap;
        }
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        SystemUser user = userService.getUserByLogin( currentUser );
        if ( user == null )
        {
            modelMap.put( successProperty, false );
            modelMap.put( msgProperty, "User error. Please contact administrator." );
            logger.warn( "User error. Please contact administrator." );
            return modelMap;
        }
        String currency =user.getCurrency();
        String picturePrice =user.getPrice();

        if ( currency == null || picturePrice==null || picturePrice.trim().equals( "" ) )
        {
            modelMap.put( successProperty, false );
            modelMap.put( msgProperty, "Currency of the location error. Please contact administrator." );
            logger.warn( "Currency of the location error. Please contact administrator." );
            return modelMap;
        }

        if ( Integer.valueOf(picturesSize)*Integer.valueOf(picturePrice)!=total)
        {
            logger.info("total " + total);
            logger.info("picturePrice " + picturePrice);
            logger.info("picturesSize " + picturesSize);
            modelMap.put( successProperty, false );
            modelMap.put( msgProperty, "The total amont of money does not correspond" );
            logger.warn( "Currency of the location error. Please contact administrator." );
            return modelMap;
        }

        Long invoiceID = new Long( System.currentTimeMillis() );

        SetExpressCheckoutResponseType setExpressCheckoutResponseType  = setExpressCheckout2(currency, total, invoiceID, pictureIds, pictureEmail);
        if(setExpressCheckoutResponseType!=null && setExpressCheckoutResponseType.getToken()!=null){
            modelMap.put(msgProperty, PAYPAL_REDIRECT_URL + setExpressCheckoutResponseType.getToken()+"&useraction=commit");
            modelMap.put( successProperty, true );
            logger.info("RESPONSE: " +  PAYPAL_REDIRECT_URL+ setExpressCheckoutResponseType.getToken()+"&useraction=commit");
        }else{
            modelMap.put(msgProperty, "Error during payment token getting. Please contact the administrator");
            modelMap.put(successProperty, false );
        }

        return modelMap;
    }


    @RequestMapping(value = "/main/commonPayed")
    public String getPurchasePaymentPage( ModelMap model, String token, String PayerID, HttpServletRequest request )
    {
        String contextPath = ServletUtils.getContextPath(request);

        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        SystemUser systemUser = userService.getUserByLogin(currentUser);
        String price = systemUser.getPrice()==null?"0":systemUser.getPrice();
        model.put("price",price);

        logger.info("User [ " + currentUser + " ] opened a PAYED catalog page");
        logger.info("TOKEN:" + token);
        logger.info("PayerID:" + PayerID);

        String userForPictures = currentUser;
        List<PictureBean> pictures = new ArrayList<PictureBean>();
      //  String pictureListString = systemUser.getPictureIds();
        pictures =  pictureService.getPictures(currentUser);//getPictureBeanListFromPicturesSet(systemUser.getPictures());
        model.addAttribute("pictures", pictures);

        Payment payment = payPalService.getRegisteredPayment( token );
        if(payment.getTransactionID()!=null && payment.getStatus()!=null && payment.getStatus().equals(statusComplete)){
            model.put(payedProperty, false);
            model.put(successProperty, true);
            return "catalogNotLogin";
        }
        if ( payment == null )
        {
            logger.warn( "Payment was not registered. Please contact administrator." );
            model.put( msgProperty, "Payment was not registered. (Payment is null) Please contact administrator." );
            model.put( payedProperty, false );
            model.put( successProperty, false );
            return "catalogNotLogin";
        }

        if ( StringUtils.isEmpty( PayerID ) || StringUtils.isEmpty( token ) )
        {
            logger.warn( "Payment was not registered. Please contact administrator." );
            model.put( msgProperty, "Payment was not registered.(PayerId or token problems) Please contact administrator." );
            model.put( payedProperty, false );
            model.put( successProperty, false );
            return "catalogNotLogin";
        }

         if(payment.getCurrency() ==null || payment.getPrice()==null || payment.getInvoiceId()==null){
             logger.warn( "Payment was not registered. Please contact administrator." );
             model.put( msgProperty, "Payment was not registered. (Price, invoiceId or currency problems) Please contact administrator." );
             model.put( payedProperty, false );
             model.put( successProperty, false );
             return "catalogNotLogin";
         }

        logger.info("token:" +token);
        logger.info("PayerID:" +PayerID);

        String newLogin  =  doExpressCheckout( payment, token, PayerID);
        if(newLogin==null){
            logger.warn( "Payment was not registered. Please contact administrator." );
            model.put(msgProperty,"Something went wrong. Please contact the administrator. Thank you.");
            model.put(successProperty, false );
            model.put(payedProperty, false );
        }else{
            LocationBean location = locationService.getLocation(systemUser.getLocationId());

            String locationName = "", locationFooter = "";

            if(location != null){
                locationName = location.getName() != null ? location.getName() : "";
                locationFooter = location.getEmailFooter() != null ? location.getEmailFooter() : "";
            }

            model.put(msgProperty,newLogin);
            model.put(successProperty, true );
            model.put(payedProperty, true );
            String email = payment.getPayerEmail(),
                    link, subject, message;
            if(email!=null && newLogin!=null){
                link = contextPath + "/pictures/auth/autoLogin?username=" + newLogin;
                subject = messageSource.getMessage("mail.subject", new Object[]{locationName}, Locale.ROOT);
                message = messageSource.getMessage("mail.template", new Object[]{"", link, newLogin, locationFooter}, Locale.ROOT);

                mailService.sendMail(subject, message, email);
            }

        }

        return "catalogNotLogin";
    }


    public  List<PaymentDetailsType> getPaymentDetailsList(String currency, Double total, String invoiceID){
        // ### Payment Information
        // list of information about the payment
        List<PaymentDetailsType> paymentDetailsList = new ArrayList<PaymentDetailsType>();

        // information about the first payment
        PaymentDetailsType paymentDetails1 = new PaymentDetailsType();

        CurrencyCodeType currencyCodeType = currency.equals("CAD")?CurrencyCodeType.CAD:CurrencyCodeType.USD;
        // * `Amount`
        BasicAmountType orderTotal1 = new BasicAmountType(currencyCodeType,String.valueOf(total));
        paymentDetails1.setOrderTotal(orderTotal1);
        paymentDetails1.setPaymentAction(PaymentActionCodeType.ORDER);

        // Unique identifier for the merchant. For parallel payments, this field
        // is required and must contain the Payer Id or the email address of the
        // merchant.
        SellerDetailsType sellerDetails1 = new SellerDetailsType();
      //  sellerDetails1.setPayPalAccountID("mytain_1328882426_per@yandex.ru");
        paymentDetails1.setSellerDetails(sellerDetails1);

        // A unique identifier of the specific payment request, which is
        // required for parallel payments.
       // paymentDetails1.setPaymentRequestID("mytain_1328780854_biz@yandex.ru");
        paymentDetails1.setPaymentRequestID(invoiceID);

/*      AddressType shipToAddress1 = new AddressType();
        shipToAddress1.setStreet1("Ape Way");
        shipToAddress1.setCityName("Austin");
        shipToAddress1.setStateOrProvince("TX");
        shipToAddress1.setCountry(CountryCodeType.US);
        shipToAddress1.setPostalCode("78750");

        paymentDetails1.setNotifyURL("http://localhost/ipn");

        paymentDetails1.setShipToAddress(shipToAddress1);*/

        paymentDetailsList.add(paymentDetails1);
        return paymentDetailsList;
    }

    public SetExpressCheckoutRequestDetailsType setExpressCheckoutDetails(String currency, Double total, String invoiceID) {

        // ## SetExpressCheckoutReq
        SetExpressCheckoutRequestDetailsType setExpressCheckoutRequestDetails = new SetExpressCheckoutRequestDetailsType();


        setExpressCheckoutRequestDetails
                .setReturnURL(CONFIRM_REDIRECT_URL);

        setExpressCheckoutRequestDetails
                .setCancelURL(CANCEL_REDIRECT_URL);

        List<PaymentDetailsType> paymentDetailsList= getPaymentDetailsList(currency, total, invoiceID);



        setExpressCheckoutRequestDetails.setPaymentDetails(paymentDetailsList);
         return setExpressCheckoutRequestDetails;

    }


    private PayPalAPIInterfaceServiceService setPayPalAPIInterfaceServiceService() {
        PayPalAPIInterfaceServiceService service = null;

        Properties properties = new Properties();
        try {
            Merchant merchantAccount = payPalService.getMerchantAccount();

            properties.setProperty("acct1.UserName",merchantAccount.getAccount());
            properties.setProperty("acct1.Password",merchantAccount.getPassword());
            properties.setProperty("acct1.Signature",merchantAccount.getSignature());
            properties.setProperty("acct1.AppId",new Long( System.currentTimeMillis() )+"");

            if(merchantAccount.getEnvironment()!=null && merchantAccount.getEnvironment().equals("live")){

                properties.setProperty("service.RedirectURL", liveServiceRedirectURL);
                properties.setProperty("service.EndPoint.PayPalAPI", liveServiceEndPointPayPalAPI);
                properties.setProperty("service.EndPoint.PayPalAPIAA",liveServiceEndPointPayPalAPIAA);
                properties.setProperty("service.EndPoint.Permissions",liveServiceEndPointPermissions);
                setPayPalRedirectUrls(liveFotafloPaypalUrl);
            }else{
                properties.setProperty("service.RedirectURL", sandboxServiceRedirectURL);
                properties.setProperty("service.EndPoint.PayPalAPI", sandboxServiceEndPointPayPalAPI);
                properties.setProperty("service.EndPoint.PayPalAPIAA",sandboxServiceEndPointPayPalAPIAA);
                properties.setProperty("service.EndPoint.Permissions",sandboxServiceEndPointPermissions);
                setPayPalRedirectUrls(sandboxFotafloPaypalUrl);
            }

            properties.setProperty("http.ConnectionTimeOut", connectionTimeOut);
            properties.setProperty("http.Retry", retry);
            properties.setProperty("http.ReadTimeOut", readTimeOut);
            properties.setProperty("http.MaxConnection", maxConnection);
            properties.setProperty("http.IPAddress", iPAddress);

            properties.setProperty("http.UseProxy", useProxy);
            properties.setProperty("http.ProxyPort", proxyPort);
            properties.setProperty("http.ProxyHost", proxyHost);
            properties.setProperty("http.ProxyUserName", proxyUserName);
            properties.setProperty("http.ProxyPassword", proxyPassword);
            properties.setProperty("http.GoogleAppEngine", googleAppEngine);

            service = new PayPalAPIInterfaceServiceService(properties);

        } catch (Exception e) {
            logger.error("Error during retrieving merchant account settings " +e.getMessage(), e);
            return null;
        }

        return service;

    }

    public SetExpressCheckoutResponseType setExpressCheckout2( String currency, Double totalPrice, Long invoiceID, String pictureIds, String email) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        Logger logger = Logger.getLogger(this.getClass().toString());

        SetExpressCheckoutRequestDetailsType setExpressCheckoutRequestDetails =  setExpressCheckoutDetails( currency, totalPrice, String.valueOf(invoiceID));

        SetExpressCheckoutReq setExpressCheckoutReq = new SetExpressCheckoutReq();
        SetExpressCheckoutRequestType setExpressCheckoutRequest = new SetExpressCheckoutRequestType(
                setExpressCheckoutRequestDetails);

        setExpressCheckoutReq.setSetExpressCheckoutRequest(setExpressCheckoutRequest);

        // ## Creating service wrapper object
        // Creating service wrapper object to make API call and loading
        // configuration file for your credentials and endpoint


        PayPalAPIInterfaceServiceService service =  setPayPalAPIInterfaceServiceService();
        if(service==null){
            logger.error("Error during merchat account setting retrieving");
            return null;
        }

        SetExpressCheckoutResponseType setExpressCheckoutResponse = null;
        try {
            // ## Making API call
            // Invoke the appropriate method corresponding to API in service
            // wrapper object
            setExpressCheckoutResponse = service
                    .setExpressCheckout(setExpressCheckoutReq);
        } catch (Exception e) {
            logger.error("Error Message : " + e.getMessage());
        }

        if (setExpressCheckoutResponse.getAck().getValue()
                .equalsIgnoreCase("success")) {


            payPalService.registerPayment(currentUser, new Date(),  currency, totalPrice,
                    invoiceID, statusRegistered, setExpressCheckoutResponse.getToken(), "success", pictureIds, email);
            logger.info("EC Token:" + setExpressCheckoutResponse.getToken());
        }
        // ### Error Values
        // Access error values from error list using getter methods
        else {
            List<ErrorType> errorList = setExpressCheckoutResponse.getErrors();
            logger.error("API Error Message : "
                    + errorList.get(0).getLongMessage());
            payPalService.registerPayment(currentUser, new Date(),  null, null,
                    invoiceID, statusErrorRegistered, setExpressCheckoutResponse.getToken(), errorList.get(0).getLongMessage(), pictureIds,email);
        }
        return setExpressCheckoutResponse;
    }


    public String doExpressCheckout(Payment payment, String token, String PayerId) {

        String userNewLogin = null;
        Logger logger = Logger.getLogger(this.getClass().toString());

        // ## DoExpressCheckoutPaymentReq
        DoExpressCheckoutPaymentReq doExpressCheckoutPaymentReq = new DoExpressCheckoutPaymentReq();

        DoExpressCheckoutPaymentRequestDetailsType doExpressCheckoutPaymentRequestDetails = new DoExpressCheckoutPaymentRequestDetailsType();

        doExpressCheckoutPaymentRequestDetails.setToken(token);

        // Unique paypal buyer account identification number as returned in
        // `GetExpressCheckoutDetails` Response
        doExpressCheckoutPaymentRequestDetails.setPayerID(PayerId);

        List<PaymentDetailsType> paymentDetailsList= getPaymentDetailsList(payment.getCurrency(), payment.getPrice(), String.valueOf(payment.getInvoiceId()));

        doExpressCheckoutPaymentRequestDetails
                .setPaymentDetails(paymentDetailsList);
        DoExpressCheckoutPaymentRequestType doExpressCheckoutPaymentRequest = new DoExpressCheckoutPaymentRequestType(
                doExpressCheckoutPaymentRequestDetails);
        doExpressCheckoutPaymentReq
                .setDoExpressCheckoutPaymentRequest(doExpressCheckoutPaymentRequest);

        PayPalAPIInterfaceServiceService service =  setPayPalAPIInterfaceServiceService();
        if(service==null){
            logger.error("Error during merchat account setting retrieving");
            return null;
        }

        DoExpressCheckoutPaymentResponseType doExpressCheckoutPaymentResponse = null;
        try {
            // ## Making API call
            // Invoke the appropriate method corresponding to API in service
            // wrapper object
            doExpressCheckoutPaymentResponse = service
                    .doExpressCheckoutPayment(doExpressCheckoutPaymentReq);
        } catch (Exception e) {
            logger.error("Error Message : " + e.getMessage());
        }

        // ## Accessing response parameters
        // You can access the response parameters using getter methods in
        // response object as shown below
        // ### Success values
        if (doExpressCheckoutPaymentResponse.getAck().getValue()
                .equalsIgnoreCase("success")) {

            // Transaction identification number of the transaction that was
            // created.
            // This field is only returned after a successful transaction
            // for DoExpressCheckout has occurred.
            if (doExpressCheckoutPaymentResponse
                    .getDoExpressCheckoutPaymentResponseDetails()
                    .getPaymentInfo() != null) {
                Iterator<PaymentInfoType> paymentInfoIterator = doExpressCheckoutPaymentResponse
                        .getDoExpressCheckoutPaymentResponseDetails()
                        .getPaymentInfo().iterator();
                while (paymentInfoIterator.hasNext()) {
                    PaymentInfoType paymentInfo = paymentInfoIterator
                            .next();
                    logger.info("Transaction ID : "
                            + paymentInfo.getTransactionID());

                    payment.setTransactionID(paymentInfo.getTransactionID());
                    payment.setPayerID(PayerId);

                    /// NEW LOGIN GENERATION
                    final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
                    SystemUser user = userService.getUserByLogin(currentUser);
                    String userBase = currentUser;
                    if (user != null && user.getLoginGroup() != null && !user.getLoginGroup().isEmpty()) {
                        userBase = user.getLoginGroup();
                    }
                    List<String> userLogins = userService.generateNewUserLogins(userBase, 1, payment.getPictureids());
                    userNewLogin= userLogins.get(0);
                    payment.setNewLogin(userNewLogin);
                    payment.setStatus(statusComplete);
                    payPalService.savePayment(payment);

                }
            }
        }
        // ### Error Values
        // Access error values from error list using getter methods
        else {
            List<ErrorType> errorList = doExpressCheckoutPaymentResponse
                    .getErrors();
            logger.error("API Error Message : "
                    + errorList.get(0).getLongMessage());

            payment.setStatus(statusErrorComplete);
            payment.setResult(errorList.get(0).getLongMessage());
            payPalService.savePayment(payment);

        }

       // doExpressCheckoutPaymentResponse.getDoExpressCheckoutPaymentResponseDetails().getPaymentInfo().get(0).setTransactionID();
        return userNewLogin;
    }
}
