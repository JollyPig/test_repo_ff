package com.scnsoft.fotafloweb.controller;

import com.scnsoft.fotafloweb.bean.PictureBean;
import com.scnsoft.fotafloweb.service.IPictureLoadService;
import com.scnsoft.fotafloweb.service.IPictureLogoService;
import com.scnsoft.fotafloweb.service.IPictureService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
//@RequestMapping()
public class ImageController {
    protected final static Logger logger = Logger.getLogger(ImageController.class);

    @Autowired
    private IPictureLoadService pictureLoadService;

    @Autowired
    private IPictureLogoService pictureLogoService;

    @Autowired
    private IPictureService pictureService;

    @RequestMapping(value = "/img_small/**", method = RequestMethod.GET)
    public void getImage(HttpServletRequest req, HttpServletResponse resp) throws Exception {

        /*resp.addHeader("Content-Type", "image/jpg");

        Boolean mobile = (Boolean)req.getSession().getAttribute("mobile");

        *//*if(pictureLogoService.isImageCached(req.getPathInfo())){
            pictureLogoService.writeImage(req.getPathInfo(), resp.getOutputStream());
        }else{
            PictureBean picture = pictureService.getPictureByUrl(req.getPathInfo());
            if(picture != null){
                pictureLogoService.writeMarkedPicture(picture, resp.getOutputStream());
            }else{
                pictureLoadService.loadPicture(req.getPathInfo(), resp.getOutputStream());
            }
        }*//*

        if(mobile != null && mobile){
            BufferedImage img = pictureLoadService.loadPicture(req.getPathInfo());
            Thumbnails.of(img).size(200, 150).outputFormat("JPEG").toOutputStream(resp.getOutputStream());
        }else{
            pictureLoadService.loadPicture(req.getPathInfo(), resp.getOutputStream());
        }

        resp.getOutputStream().close();*/
        resp.addHeader("Content-Type", "image/jpg");

        if(pictureLogoService.isImageCached(req.getPathInfo())){
            pictureLogoService.writeImage(req.getPathInfo(), resp.getOutputStream());
        }else{
            PictureBean picture = pictureService.getPictureByUrl(req.getPathInfo());
            if(picture != null){
                pictureLogoService.writeMarkedPicture(picture, resp.getOutputStream(), true);
            }else{
                pictureLoadService.loadPicture(req.getPathInfo(), resp.getOutputStream());
            }
        }
        resp.getOutputStream().close();
    }

    @Async
    @RequestMapping(value = "/img/**", method = RequestMethod.GET)
    public void getFullsizeImage(final HttpServletRequest req, final HttpServletResponse resp) throws Exception {
        resp.addHeader("Content-Type", "image/jpg");

        if(pictureLogoService.isImageCached(req.getPathInfo())){
            pictureLogoService.writeImage(req.getPathInfo(), resp.getOutputStream());
        }else{
            PictureBean picture = pictureService.getPictureByUrl(req.getPathInfo());
            if(picture != null){
                pictureLogoService.writeMarkedPicture(picture, resp.getOutputStream(), true);
            }else{
                pictureLoadService.loadPicture(req.getPathInfo(), resp.getOutputStream());
            }
        }
        resp.getOutputStream().close();
    }

}
