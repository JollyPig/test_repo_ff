package com.scnsoft.fotafloweb.controller;

import com.scnsoft.fotafloweb.dto.PromotionDto;
import com.scnsoft.fotafloweb.dto.TextPromotionDto;
import com.scnsoft.fotafloweb.model.Merchant;
import com.scnsoft.fotafloweb.model.Position;
import com.scnsoft.fotafloweb.model.SystemUser;
import com.scnsoft.fotafloweb.service.IPayPalService;
import com.scnsoft.fotafloweb.service.IUserService;
import com.scnsoft.fotafloweb.service.impl.PayPalService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 01.07.14
 * Time: 17:23
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/admin/paypal")
public class PayPayAdminController {

    protected static Logger logger = Logger.getLogger( PayPayAdminController.class );

    @Autowired
    private IPayPalService payPalService;

    @Autowired
    private IUserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public String list(ModelMap model){

        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        SystemUser systemUser = userService.getUserByLogin(currentUser);
        boolean isAdmin = (systemUser!=null&& systemUser.getAccess()!=null && systemUser.getAccess().equals("1"))?true:false;

        String account = "";
        String password = "";
        String signature = "";
        String environment = "";
        try {
            Merchant merchantAccount = payPalService.getMerchantAccountEncrypted();
            if(merchantAccount!=null){
                account = merchantAccount.getAccount();
                password = merchantAccount.getPassword();
                signature = merchantAccount.getSignature();
                environment = merchantAccount.getEnvironment();
            }


        } catch (Exception e) {
            logger.error("Error during retrieving merchant account settings " +e.getMessage(), e);
            return null;
        }


        model.addAttribute("account", account);
        model.addAttribute("password", password);
        model.addAttribute("signature", signature);
        model.addAttribute("environment", environment);
        model.addAttribute("isadmin", isAdmin);


        return "paypal";
    }


    @RequestMapping(value = "/savemerchant", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<?> save(@RequestParam("account") String account,@RequestParam("password") String password,
                           @RequestParam("signature") String signature,@RequestParam("environment") String environment){

        try {
            payPalService.saveMerchantAccount(account,password,signature,environment);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return new ResponseEntity<String>("OK", HttpStatus.OK);
    }
}
