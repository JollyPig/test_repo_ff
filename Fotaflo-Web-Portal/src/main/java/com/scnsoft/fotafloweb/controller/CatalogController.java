package com.scnsoft.fotafloweb.controller;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 01.04.14
 * Time: 11:55
 * To change this template use File | Settings | File Templates.
 */

import com.scnsoft.fotafloweb.bean.PictureBean;
import com.scnsoft.fotafloweb.model.SystemUser;
import com.scnsoft.fotafloweb.service.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */
@Controller
public class CatalogController {
    private final static String successProperty = "success";

    protected static Logger logger = Logger.getLogger(CatalogController.class);


    @Autowired
    private IPictureService pictureService;

    @Autowired
    private IUserService userService;


    @RequestMapping(value = "getPictures.json")
    public
    @ResponseBody
    Map<String, ? extends Object> loadPictures(HttpServletRequest request) {

        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("user " + currentUser + "loads pictures");

        SystemUser curUser = userService.getUserByLogin(currentUser);

        String userForPictures = currentUser;
        List<PictureBean> pictures = new ArrayList<PictureBean>();
        if(curUser!=null && curUser.getLoginGroup()!=null && !curUser.getLoginGroup().isEmpty()){
            userForPictures = curUser.getLoginGroup();
            String pictureListString = curUser.getPictureIds();
           /* pictures =  pictureService.getPicturesForCodeUsers(userForPictures, pictureListString);*/
            pictures =  pictureService.getPictureBeanListFromPicturesSet(curUser.getPictures());
        }else{
            pictures =  pictureService.getPictures(userForPictures);
        }



        for(PictureBean picture :pictures){
            System.out.println(picture.getUrl());
        }

        Map modelMap = new HashMap();
        modelMap.put("pictures", pictures);

        return modelMap;
    }


    @RequestMapping(value = "/main/selected")
    public @ResponseBody ResponseEntity<List<PictureBean>> getPicturesById(HttpServletRequest request){
        String[] ids = request.getQueryString().split(",");
        List<Integer> list = new ArrayList<Integer>();
        for(String id: ids){
            if(!id.trim().isEmpty() && id.matches("[\\d]+")){
                list.add(new Integer(id));
            }
        }
        logger.info("Selected pictures ids: " + list);
        List<PictureBean> result = pictureService.getPicturesByIds(list);
        return new ResponseEntity<List<PictureBean>>(result, HttpStatus.OK);
    }
}

