package com.scnsoft.fotafloweb.model;

import java.io.Serializable;

/**
 * An interface used to represent the generic
 * methods for each possible entity
 */
public interface IEntity extends Serializable {

    /**
     * Returns the entity identifier.
     *
     * @return entity identifier
     */
    int getId();
}
