package com.scnsoft.fotafloweb.analytics.ga.tracking;

public class SocialEventPayload extends EventPayload {

    protected static final String usernameParam         = "cd2";
    protected static final String locationIdParam       = "cd1";
    protected static final String locationNameParam     = "cd3";

    protected static final String eventErrorParam       = "cd5";
    protected static final String socialAccountParam    = "cd6";
    protected static final String friendsCountParam     = "cm1";

    public SocialEventPayload(String trackingId, String clientId) {
        super(trackingId, clientId);
    }

    public SocialEventPayload(String protocolVersion, String trackingId, String clientId) {
        super(protocolVersion, trackingId, clientId);
    }

    public String getUsername() {
        return getParameter(usernameParam);
    }

    public void setUsername(String username) {
        addParameter(usernameParam, username);
    }

    public Integer getLocationId() {
        return (Integer)getRawParameter(locationIdParam);
    }

    public void setLocationId(Integer locationId) {
        addParameter(locationIdParam, locationId);
    }

    public String getLocationName() {
        return getParameter(locationNameParam);
    }

    public void setLocationName(String locationName) {
        addParameter(locationNameParam, locationName);
    }

    public String getEventError() {
        return getParameter(eventErrorParam);
    }

    public void setEventError(String eventError) {
        addParameter(eventErrorParam, eventError);
    }

    public String getSocialAccount() {
        return getParameter(socialAccountParam);
    }

    public void setSocialAccount(String socialAccount) {
        addParameter(socialAccountParam, socialAccount);
    }

    public Integer getFriendsCount() {
        return (Integer)getRawParameter(friendsCountParam);
    }

    public void setFriendsCount(Integer friendsCount) {
        addParameter(friendsCountParam, friendsCount);
    }
}
