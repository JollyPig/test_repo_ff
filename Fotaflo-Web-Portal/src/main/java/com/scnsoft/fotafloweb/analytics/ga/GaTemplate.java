package com.scnsoft.fotafloweb.analytics.ga;

import com.google.api.services.analytics.Analytics;
import com.google.api.services.analytics.model.GaData;
import com.scnsoft.fotafloweb.analytics.AnalyticData;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class GaTemplate {
    protected static final Logger logger = Logger.getLogger(GaTemplate.class);

    protected static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private Analytics analytics;

    @Value("${profileId}")
    private String profileId;

    @Inject
    public GaTemplate(Analytics analytics) {
        if(analytics == null){
            throw new IllegalArgumentException("Analytics service cannot be null");
        }
        this.analytics = analytics;
    }

    public Analytics getApi(){
        return analytics;
    }

    public AnalyticData getVisits(Date startDate, Date endDate, Integer locationId) throws IOException{
        String filter = getFields(GaDimension.location) + "!=null";
        GaData data = getData(dateFormat.format(startDate), dateFormat.format(endDate),
                getFields(GaMetric.session), null, filter, null, null);

        return createAnalyticDataFromGaData(data, startDate, endDate);
    }

    public AnalyticData getDeviceTypeStatistic(Date startDate, Date endDate, Integer locationId) throws IOException{
        String filter = getFields(GaDimension.location) + "!=null",
                sort = getFields(GaDimension.deviceType);

        if(locationId != null){
            filter = getFields(GaDimension.location) + "==" + locationId;
        }
        GaData data = getData(dateFormat.format(startDate), dateFormat.format(endDate),
                getFields(GaMetric.session), getFields(GaDimension.deviceType), filter, sort, null);

        return createAnalyticDataFromGaData(data, startDate, endDate);
    }

    public AnalyticData getDeviceTypeTotal(Date startDate, Date endDate, Integer locationId) throws IOException{
        String filter = getFields(GaDimension.location) + "!=null";

        if(locationId != null){
            filter = getFields(GaDimension.location) + "==" + locationId;
        }
        GaData data = getData(dateFormat.format(startDate), dateFormat.format(endDate),
                getFields(GaMetric.session), null, filter, null, null);

        return createAnalyticDataFromGaData(data, startDate, endDate);
    }

    public AnalyticData getDeviceTypeStatisticRanged(Date startDate, Date endDate, Integer locationId, Integer range) throws IOException{
        List<GaDimension> dims = new ArrayList<GaDimension>();
        GaDimension sortDateDim;
        dims.add(GaDimension.location);
        dims.addAll(Arrays.asList(getDateParametersByRange(range)));
        sortDateDim = dims.get(dims.size() - 1);

        dims.add(GaDimension.deviceType);

        String filter = getFields(GaDimension.location) + "!=null",
                sort = getFields(GaDimension.location) + ",-" + getFields(sortDateDim) + ",-" + getFields(GaMetric.session);

        if(locationId != null){
            filter = getFields(GaDimension.location) + "==" + locationId;
        }

        GaData data = getData(dateFormat.format(startDate), dateFormat.format(endDate),
                getFields(GaMetric.session),
                getFields(dims.toArray(new GaDimension[]{})),
                filter, sort, null);

        return createAnalyticDataFromGaData(data, startDate, endDate, range);
    }

    public AnalyticData getGeographyStatistic(Date startDate, Date endDate, Integer locationId) throws IOException{
        String filter = getFields(GaDimension.location) + "!=" + null,
                sort = "-" + getFields(GaMetric.session);

        if(locationId != null){
            filter = getFields(GaDimension.location) + "==" + locationId;
        }
        GaData data = getData(dateFormat.format(startDate), dateFormat.format(endDate),
                getFields(GaMetric.session), getFields(GaDimension.city), filter, sort, null);

        return createAnalyticDataFromGaData(data, startDate, endDate);
    }

    public AnalyticData getGeographyTotal(Date startDate, Date endDate, Integer locationId) throws IOException{
        String filter = getFields(GaDimension.location) + "!=" + null;

        if(locationId != null){
            filter = getFields(GaDimension.location) + "==" + locationId;
        }
        GaData data = getData(dateFormat.format(startDate), dateFormat.format(endDate),
                getFields(GaMetric.session), null, filter, null, null);

        return createAnalyticDataFromGaData(data, startDate, endDate);
    }

    public AnalyticData getShareStatistic(Date startDate, Date endDate, Integer locationId) throws IOException{
        String filter = getFields(GaDimension.location) + "!=null",
                sort = "-" + getFields(GaMetric.socialAction);

        GaData data = getData(dateFormat.format(startDate), dateFormat.format(endDate),
                getFields(GaMetric.socialAction), getFields(GaDimension.socialNetwork), filter, sort, null);

        return createAnalyticDataFromGaData(data, startDate, endDate);
    }

    public AnalyticData getShareTotal(Date startDate, Date endDate, Integer locationId) throws IOException{
        String filter = getFields(GaDimension.location) + "!=null";

        GaData data = getData(dateFormat.format(startDate), dateFormat.format(endDate),
                getFields(GaMetric.socialAction), null, filter, null, null);

        return createAnalyticDataFromGaData(data, startDate, endDate);
    }

    public AnalyticData getShareStatisticRanged(Date startDate, Date endDate, Integer locationId, Integer range) throws IOException{
        List<GaDimension> dims = new ArrayList<GaDimension>();
        GaDimension sortDateDim;
        dims.add(GaDimension.location);
        dims.addAll(Arrays.asList(getDateParametersByRange(range)));
        sortDateDim = dims.get(dims.size() - 1);

        dims.add(GaDimension.socialNetwork);

        String filter = getFields(GaDimension.location) + "!=null",
                sort = getFields(GaDimension.location) + ",-" + getFields(sortDateDim) + ",-" + getFields(GaMetric.socialAction);

        GaData data = getData(dateFormat.format(startDate), dateFormat.format(endDate),
                getFields(GaMetric.socialAction),
                getFields(dims.toArray(new GaDimension[]{})),
                filter, sort, null);

        return createAnalyticDataFromGaData(data, startDate, endDate, range);
    }

    public AnalyticData getShareStatisticWithoutLocationRanged(Date startDate, Date endDate, Integer locationId, Integer range) throws IOException{
        List<GaDimension> dims = new ArrayList<GaDimension>();
        GaDimension sortDateDim;
        dims.addAll(Arrays.asList(getDateParametersByRange(range)));
        sortDateDim = dims.get(dims.size() - 1);

        dims.add(GaDimension.socialNetwork);

        String filter = getFields(GaDimension.location) + "!=null",
                sort = getFields(sortDateDim) + ",-" + getFields(GaMetric.socialAction);

        GaData data = getData(dateFormat.format(startDate), dateFormat.format(endDate),
                getFields(GaMetric.socialAction),
                getFields(dims.toArray(new GaDimension[]{})),
                filter, sort, null);

        return createAnalyticDataFromGaData(data, startDate, endDate, range);
    }

    public AnalyticData getUserStatistic(Date startDate, Date endDate, Integer locationId, Integer range) throws IOException{
        String filter = getFields(GaDimension.location) + "!=" + null;

        if(locationId != null){
            filter = getFields(GaDimension.location) + "==" + locationId;
        }
        List<GaDimension> dims = new ArrayList<GaDimension>();
        GaDimension sortDateDim;
        dims.add(GaDimension.location);
        dims.addAll(Arrays.asList(getDateParametersByRange(range)));
        sortDateDim = dims.get(dims.size() - 1);

        dims.add(GaDimension.username);

        String sort = getFields(GaDimension.location) + ",-" + getFields(sortDateDim);

        GaData data = getData(dateFormat.format(startDate), dateFormat.format(endDate),
                getFields(GaMetric.newUsers,GaMetric.session),
                getFields(dims.toArray(new GaDimension[]{})),
                filter, sort, null);

        return createAnalyticDataFromGaData(data, startDate, endDate, range);
    }

    public AnalyticData getPotentialSocialReachStatisticRanged(Date startDate, Date endDate, Integer locationId, Integer range) throws IOException{
        String filter = getFields(GaDimension.location) + "!=" + null;

        if(locationId != null){
            filter = getFields(GaDimension.location) + "==" + locationId;
        }
        List<GaDimension> dims = new ArrayList<GaDimension>();
        GaDimension sortDateDim;
        dims.add(GaDimension.location);
        dims.addAll(Arrays.asList(getDateParametersByRange(range)));
        sortDateDim = dims.get(dims.size() - 1);

        dims.add(GaDimension.eventLabel);
        dims.add(GaDimension.socialAccount);

        String sort = getFields(GaDimension.location) + ",-" + getFields(sortDateDim);

        GaData data = getData(dateFormat.format(startDate), dateFormat.format(endDate),
                getFields(GaMetric.potentialSocial,GaMetric.totalEvents),
                getFields(dims.toArray(new GaDimension[]{})),
                filter, sort, null);

        return createAnalyticDataFromGaData(data, startDate, endDate, range);
    }

    protected GaDimension[] getDateParametersByRange(Integer range){
        switch (range){
            case Calendar.DATE: return new GaDimension[]{GaDimension.date};
            case Calendar.WEEK_OF_YEAR: return new GaDimension[]{GaDimension.year, GaDimension.week};
            case Calendar.MONTH: return new GaDimension[]{GaDimension.year, GaDimension.month};
            case Calendar.YEAR: return new GaDimension[]{GaDimension.year};
            default: return new GaDimension[]{GaDimension.date};
        }
    }

    protected GaDimension[] getDateSortParametersByRange(Integer range){
        switch (range){
            case Calendar.DATE: return new GaDimension[]{GaDimension.date};
            case Calendar.WEEK_OF_YEAR: return new GaDimension[]{GaDimension.year, GaDimension.week};
            case Calendar.MONTH: return new GaDimension[]{GaDimension.year, GaDimension.month};
            case Calendar.YEAR: return new GaDimension[]{GaDimension.year};
            default: return new GaDimension[]{GaDimension.date};
        }
    }

    private AnalyticData createAnalyticDataFromGaData(GaData data, Date startDate, Date endDate){
        return createAnalyticDataFromGaData(data, startDate, endDate, null);
    }

    private AnalyticData createAnalyticDataFromGaData(GaData data, Date startDate, Date endDate, Integer range){
        List<String> columns = new ArrayList<String>();
        if(data.getColumnHeaders() != null){
            for (GaData.ColumnHeaders col: data.getColumnHeaders()){
                columns.add(getFieldLabelById(col.getName()));
            }
        }

        AnalyticData analyticData = new AnalyticData(columns, data.getRows(), startDate, endDate, range);
        return analyticData;
    }

    protected String getFieldLabelById(String id){
        for (GaDimension c : GaDimension.values()) {
            if (c.getId().equals(id)) {
                return c.getLabel();
            }
        }
        for (GaMetric c : GaMetric.values()) {
            if (c.getId().equals(id)) {
                return c.getLabel();
            }
        }
        return "";
    }

    protected String getFields(IGaField... fields){
        StringBuffer s = new StringBuffer("");
        String d = ",";
        if(fields != null && fields.length > 0){
            for(IGaField field: fields){
                if(field != null){
                    s.append(field.getId());
                    s.append(d);
                }
            }
            return s.substring(0, s.length()-1);
        }
        return null;
    }

    protected GaData getData(String startDate, String endDate, String metrics, String dimensions,
                             String filters, String sort, Integer maxResults)
                                                                                throws IOException{
        String ids = "ga:"+profileId;

        logger.info("query: "+ids+"|"+startDate+"|"+endDate+"|"+metrics+"|"+dimensions+"|"+filters+"|"+sort+"|"+maxResults+"|");
        Analytics.Data.Ga.Get get = analytics.data().ga().get(ids, startDate, endDate, metrics);

        if(dimensions != null){
            get.setDimensions(dimensions);
        }
        if(filters != null){
            get.setFilters(filters);
        }
        if(sort != null){
            get.setSort(sort);
        }
        if(maxResults != null){
            get.setMaxResults(maxResults);
        }

        return get.execute();
    }
}
