package com.scnsoft.fotafloweb.analytics.ga.tracking;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

@Component
public class GaTracker {
    protected static final Logger logger = Logger.getLogger(GaTracker.class);

    @Value("${trackingCode}")
    private String trackingId;

    public void trackEvent(String clientId, String username, Integer locationId, String locationName, String category, String action, String label, Integer value, String error) {
        SocialEventPayload data = createPayloadData(clientId, username, locationId, locationName, category, action, label, value);

        data.setEventError(error);

        makeRequest(data);
    }

    public void trackEvent(String clientId, String category, String action, String label, Integer value) {
        trackEvent(clientId, null, null, null, category,action,label,value, null);
    }

    public void trackSocial(String clientId, String username, Integer locationId, String locationName, String category, String action, String label, Integer value, String account, Integer friends, String error) {
        SocialEventPayload data = createPayloadData(clientId, username, locationId, locationName, category, action, label, value);

        data.setEventError(error);
        data.setSocialAccount(account);
        data.setFriendsCount(friends);

        makeRequest(data);
    }

    private SocialEventPayload createPayloadData(String clientId, String username, Integer locationId, String locationName, String category, String action, String label, Integer value) {
        SocialEventPayload data = new SocialEventPayload(trackingId, clientId);
        data.setEvent(category, action, label, value);

        data.setUsername(username);
        data.setLocationId(locationId);
        data.setLocationName(locationName);

        return data;
    }

    public void makeRequest(Payload data){
        if(data == null){
            throw new IllegalArgumentException("'data' cannot be null");
        }
        List<String> urls = data.buildUrls();

        for (String url: urls){
            dispatchRequest(url);
        }
    }

    protected void dispatchRequest(String argURL) {
        try {
            URL url = new URL(argURL);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("GET");
            connection.setInstanceFollowRedirects(true);
            connection.connect();
            int responseCode = connection.getResponseCode();
            if (responseCode != HttpURLConnection.HTTP_OK) {
                logger.error("Error requesting url '{" + argURL + "}', received response code {" + responseCode + "}");
            } else {
                logger.info("Tracking success for url '{" + argURL + "}'");
            }
        } catch (Exception e) {
            logger.error("Error making tracking request", e);
        }
    }

}
