package com.scnsoft.fotafloweb.analytics.ga;

public interface IGaField {
    String getId();
    String getLabel();
}
