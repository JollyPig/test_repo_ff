package com.scnsoft.fotafloweb.analytics.ga.tracking;

import com.scnsoft.fotafloweb.util.URIEncoder;

import java.util.*;

public class Payload {
    public static final String URL_ENDPOINT = "http://www.google-analytics.com/collect";

    protected static final String versionParam  = "v";
    protected static final String trackIdParam  = "tid";
    protected static final String clientIdParam = "cid";
    protected static final String hitTypeParam  = "t";

    private String protocolVersion;
    private String trackingId;
    private String clientId;
    private String hitType;

    private Map<String, Object> parameters = new HashMap<String, Object>();

    public Payload(String trackingId, String clientId, String hitType) {
        this("1", trackingId, clientId, hitType);
    }

    public Payload(String protocolVersion, String trackingId, String clientId, String hitType) {
        if(protocolVersion == null || protocolVersion.isEmpty()){
            throw new IllegalArgumentException("'protocolVersion' cannot be null or empty");
        }
        if(trackingId == null || trackingId.isEmpty()){
            throw new IllegalArgumentException("'trackingId' cannot be null or empty");
        }
        if(clientId == null || clientId.isEmpty()){
            throw new IllegalArgumentException("'clientId' cannot be null or empty");
        }
        if(hitType == null || hitType.isEmpty()){
            throw new IllegalArgumentException("'hitType' cannot be null or empty");
        }
        this.protocolVersion = protocolVersion;
        this.trackingId = trackingId;
        this.clientId = clientId;
        this.hitType = hitType;
    }

    public List<String> buildUrls(){
        String[] trackingIds = trackingId.split(",");
        List<String> urls = new ArrayList<String>();
        StringBuilder sb;
        for(String tid: trackingIds){
            sb = new StringBuilder(URL_ENDPOINT);
            sb.append("?");
            sb.append(versionParam + "=" + getURIString(protocolVersion));
            sb.append("&");
            sb.append(trackIdParam + "=" + getURIString(tid));
            sb.append("&");
            sb.append(clientIdParam + "=" + getURIString(clientId));
            sb.append("&");
            sb.append(hitTypeParam + "=" + getURIString(hitType));

            for(String p: parameters.keySet()){
                if(parameters.get(p) != null){
                    sb.append("&");
                    sb.append(getURIString(p) + "=" + getURIString(parameters.get(p).toString()));
                }
            }

            sb.append("&");
            sb.append("z=" + new Date().getTime());

            urls.add(sb.toString());
        }

        return urls;
    }

    protected void addParameter(String key, Object value){
        if(key == null && key.isEmpty()){
            throw new IllegalArgumentException("'key' cannot be null or empty");
        }
        parameters.put(key, value);
    }

    protected String getParameter(String key){
        Object param = getRawParameter(key);
        if(param != null){
            return param.toString();
        }
        return null;
    }

    protected Object getRawParameter(String key){
        if(key != null && !key.isEmpty()){
            return parameters.get(key);
        }
        return null;
    }

    protected String removeParameter(String key){
        if(key != null && !key.isEmpty() && parameters.get(key) != null){
            return parameters.remove(key).toString();
        }
        return null;
    }

    public String getProtocolVersion() {
        return protocolVersion;
    }

    public String getTrackingId() {
        return trackingId;
    }

    public String getClientId() {
        return clientId;
    }

    public String getHitType() {
        return hitType;
    }

    protected Map<String, Object> getParameters() {
        return parameters;
    }

    private String getURIString(String argString){
        if(argString == null){
            return "";
        }
        return URIEncoder.encodeURI(argString);
    }
}
