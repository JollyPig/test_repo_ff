package com.scnsoft.fotafloweb.analytics;

import java.util.Date;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;

public class AnalyticData {

    private List<String> columns;
    private List<List<String>> rows;
    private Date startDate;
    private Date endDate;
    private Integer range;

    public AnalyticData(List<String> columns, List<List<String>> rows) {
        this.columns = columns;
        this.rows = rows;
    }

    public AnalyticData(List<String> columns, List<List<String>> rows, Date startDate, Date endDate) {
        this.columns = columns;
        this.rows = rows;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public AnalyticData(List<String> columns, List<List<String>> rows, Date startDate, Date endDate, Integer range) {
        this.columns = columns;
        this.rows = rows;
        this.startDate = startDate;
        this.endDate = endDate;
        this.range = range;
    }

    public List<String> getColumns() {
        return columns;
    }

    public List<List<String>> getRows() {
        return rows;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public Integer getRange() {
        return range;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
//        Formatter f = new Formatter(sb, Locale.ROOT);

        sb.append(columns);
        sb.append('\n');

        if(rows != null){
            for(List<String> row: rows){
                sb.append(row);
                sb.append('\n');
            }
        }

        return sb.substring(0,sb.length());
    }
}
