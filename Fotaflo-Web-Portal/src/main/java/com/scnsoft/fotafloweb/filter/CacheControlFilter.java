package com.scnsoft.fotafloweb.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Calendar;

public class CacheControlFilter implements Filter {

    public static final int CACHE_AGE = 2592000; // 1 month

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        Calendar expiry = Calendar.getInstance();
        expiry.add(Calendar.SECOND, CACHE_AGE);

        HttpServletResponse httpResponse = (HttpServletResponse)response;
        httpResponse.setDateHeader("Expires", expiry.getTimeInMillis());
        httpResponse.setHeader("Cache-Control", "max-age="+ CACHE_AGE);

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }
}
