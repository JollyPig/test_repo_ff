package com.scnsoft.fotafloweb.ws;

import com.scnsoft.fotafloweb.bean.LocationBean;

import java.util.List;

public interface ILocationWebService {

    String RESOURCE = "/web/locations";

    public List<LocationBean> getLocations();

    public LocationBean getLocation(Integer id);

}
