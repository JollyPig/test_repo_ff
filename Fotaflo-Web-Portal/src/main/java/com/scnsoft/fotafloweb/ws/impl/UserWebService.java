package com.scnsoft.fotafloweb.ws.impl;

import com.scnsoft.fotafloweb.bean.UserBean;
import com.scnsoft.fotafloweb.ws.IUserWebService;
import org.springframework.stereotype.Service;

@Service
public class UserWebService extends AbstractWebService implements IUserWebService {
    @Override
    public String getResourcePath() {
        return null;
    }

    @Override
    public UserBean getUser(String login) {
        UserBean userBean = null;
        if(login != null){
            int serverId = 1,
                serverCount = userContextHolder.getServerCount();
            String path;

            while((userBean == null) && (serverId <= serverCount)){
                path = userContextHolder.getRestPath(serverId) + RESOURCE + '/' + login.trim();
                logger.info("Path by " + serverId + ": "+path);
                userBean = (UserBean)getJson(client.get(path), UserBean.class);
                if(userBean != null){
                    userBean.setServerNumber(serverId);
                }

                serverId++;
            }
        }

        return userBean;
    }
}
