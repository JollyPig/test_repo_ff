package com.scnsoft.fotafloweb.ws.impl;

import com.scnsoft.fotafloweb.bean.PictureBean;
import com.scnsoft.fotafloweb.ws.IBeanWrapper;
import com.scnsoft.fotafloweb.ws.IPictureWebService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PictureWebService extends AbstractWebService implements IPictureWebService {
    @Override
    public String getResourcePath() {
        return getRestPath() + RESOURCE;
    }

    @Override
    public List<PictureBean> getPictures(String login) {
        return getPictures(login, null);
    }

    @Override
    public List<PictureBean> getPictures(String login, IBeanWrapper preparator) {
        if(login == null){
            return new ArrayList<PictureBean>();
        }
        String url = getResourcePath() + '/' + login;
        return getJsonList(client.get(url), PictureBean.class, preparator);
    }
}
