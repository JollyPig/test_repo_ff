package com.scnsoft.fotafloweb.ws.impl;

import com.scnsoft.fotafloweb.ws.IAnalyticWebService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class AnalyticWebService extends AbstractWebService implements IAnalyticWebService {
    public static final String RESOURCE_COUNT = "/count";

    @Override
    public String getPurchaseEmailCount(Integer locationId, Date startDate, Date endDate) {
        String url = getResourcePath();

        url += RESOURCE_COUNT;

        Map<String, Object> params = new HashMap<String, Object>();
        if(startDate != null){
            params.put("start", startDate.getTime()+"");
        }
        if(endDate != null){
            params.put("end", endDate.getTime()+"");
        }
        if(locationId != null){
            params.put("id", locationId+"");
        }

        String response = client.get(url, params);

        return response;
    }

    @Override
    public String getPurchaseEmails(Integer locationId, Date startDate, Date endDate) {
        String url = getResourcePath();

        Map<String, Object> params = new HashMap<String, Object>();
        if(startDate != null){
            params.put("start", startDate.getTime()+"");
        }
        if(endDate != null){
            params.put("end", endDate.getTime()+"");
        }
        if(locationId != null){
            params.put("id", locationId+"");
        }

        String response = client.get(url, params);

        return response;
    }

    @Override
    public String getResourcePath() {
        return getRestPath() + RESOURCE;
    }
}
