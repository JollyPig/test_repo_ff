package com.scnsoft.fotafloweb.ws;

public interface IBeanWrapper {
    Object prepare(Object bean);
}
