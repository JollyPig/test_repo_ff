package com.scnsoft.fotafloweb.ws;

import com.scnsoft.fotafloweb.bean.UserBean;

public interface IUserWebService {

    String RESOURCE = "/web/users";

    UserBean getUser(String login);

}
