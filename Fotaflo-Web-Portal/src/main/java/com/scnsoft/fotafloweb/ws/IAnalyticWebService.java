package com.scnsoft.fotafloweb.ws;

import java.util.Date;

public interface IAnalyticWebService {

    String RESOURCE = "/web/purchases";

    String getPurchaseEmailCount(Integer locationId, Date startDate, Date endDate);

    String getPurchaseEmails(Integer locationId, Date startDate, Date endDate);

}
