package com.scnsoft.fotafloweb.ws.impl;

import com.scnsoft.fotafloweb.bean.LocationBean;
import com.scnsoft.fotafloweb.ws.ILocationWebService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocationWebService extends AbstractWebService implements ILocationWebService {
    @Override
    public String getResourcePath() {
        return getRestPath() + RESOURCE;
    }

    @Override
    public List<LocationBean> getLocations() {
        String url = getResourcePath() + '/' + "all";
        List<LocationBean> result = getJsonList(client.get(url), LocationBean.class);

        return result;
    }

    @Override
    public LocationBean getLocation(Integer id) {
        if(id == null){
            return null;
        }
        String url = getResourcePath() + '/' + id;
        LocationBean result = (LocationBean)getJson(client.get(url), LocationBean.class);

        return result;
    }
}
