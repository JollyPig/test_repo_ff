package com.scnsoft.fotafloweb.ws.impl;

import com.google.gson.*;
import com.scnsoft.fotafloweb.service.util.UserContextHolder;
import com.scnsoft.fotafloweb.service.util.RestClient;
import com.scnsoft.fotafloweb.ws.IBeanWrapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

abstract public class AbstractWebService {
    protected Logger logger = Logger.getLogger(getClass());

    protected final static Gson gson = new GsonBuilder()
            .setDateFormat("dd/MM/yyyy hh:mm a")
            .create();

    @Autowired
    protected RestClient client;

    @Autowired
    protected UserContextHolder userContextHolder;

    public String getRestPath(){
        return userContextHolder.getRestPath();
    }

    abstract public String getResourcePath();

    /**
     * get JSON.
     */
    protected List getJsonList(Object data, Class type) {
        return getJsonList(data, type, null);
    }

    /**
     * get JSON.
     */
    protected List getJsonList(Object data, Class type, IBeanWrapper preparator) {
        if(type == null){
            throw new IllegalArgumentException("'type' cannot be null");
        }
        List details = new ArrayList();

        if(data != null){
            String jsons = data.toString();
            try{
                JsonElement json = new JsonParser().parse(jsons);
                if(json.isJsonArray()){
                    JsonArray array = json.getAsJsonArray();
                    Iterator<JsonElement> iterator = array.iterator();
                    while (iterator.hasNext()) {
                        JsonElement json2 = iterator.next();

                        Object bean = gson.fromJson(json2, type);

                        if(bean != null){
                            if(preparator != null){
                                preparator.prepare(bean);
                            }
                            details.add(bean);
                        }
                    }
                }
            }catch(JsonParseException e){
                logger.error(e.getMessage(), e);
            }
        }
        return details;
    }

    /**
     * get LocationBean from JSON.
     */
    protected Object getJson(Object data, Class type) {
        if(type == null){
            throw new IllegalArgumentException("'type' cannot be null");
        }
        Object bean = null;

        if(data != null){
            String jsons = data.toString();
            try{
                JsonElement json = new JsonParser().parse(jsons);

                if(json.isJsonObject()){
                    bean = gson.fromJson(json, type);
                }
            }catch(JsonParseException e){
                logger.error(e.getMessage(), e);
            }
        }

        return bean;
    }
}
