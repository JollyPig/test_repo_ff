package com.scnsoft.fotafloweb.ws;

import com.scnsoft.fotafloweb.bean.PictureBean;

import java.util.List;

public interface IPictureWebService {

    String RESOURCE = "/web/pictures";

    List<PictureBean> getPictures(String login);

    List<PictureBean> getPictures(String login, IBeanWrapper preparator);

}
