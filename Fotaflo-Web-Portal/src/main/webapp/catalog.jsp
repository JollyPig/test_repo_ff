<%@ page session="true" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
	
<!doctype html>
<!--[if lt IE 8]> <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9 ie8"> <![endif]-->
<!--[if IE 9]>    <html class="ie9""> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->

<head>
    <jsp:include page="layout/head.jsp" />
</head>
<body>

<jsp:include page="layout/analytics.jsp" />

<div class="page">
    <header class="head head_page_inner">
        <div class="wrapper">
            <nav class="head__nav head__nav_page_inner">
                <ul class="head__list head__list_page_inner">
                    <%--<li><a class="head__link" href="<%= request.getContextPath()%>/pictures/main/common">Home</a></li>--%>
                    <li><a class="head__link js-contactlink" href="">Contact us</a></li>
                    <li><a class="head__link" href="<%= request.getContextPath()%>/pictures/auth/logout">Sign out</a></li>
                </ul>
                <div class="head__nav-ico"></div>
            </nav>

        </div>
    </header>

<menu class="menu js-sticky">
        <div class="menu__wrapper">
        <ul class="menu__list menu__list_position_left">
            <li><a href="" class="js-select-all">Select all</a></li>
            <li><a href="" class="js-deselect-all">Reset all</a></li>
        </ul>
        <div class="menu__counter">0</div>
        <ul class="menu__list menu__list_position_right">
            <li><span>Selected photos</span></li>
        </ul>

        <ul class="menu__list menu__list_position_center">
            <li><a class="menu__item menu__item_ico_share js-sharelink" href="">Share</a></li>
            <li><a class="menu__item menu__item_ico_download js-download" href="">Download</a></li>
            <li><a class="menu__item menu__item_ico_mail js-maillink" href="">Mail a link</a></li>
            <li><a class="menu__item menu__item_ico_slide js-slideshow-cat" id="js-slideshow" href="">Slideshow</a></li>

        </ul>
        </div>
    </menu>

    <section class="catalog js-image-list">
        <ul id="catalog_list_id" class="catalog__list">
            <c:set var="v" value="16" />
            <c:forEach var="picture" items="${pictures}">
                <c:set var="i" value="${i + 1}" />
                <li class="catalog__item  js-image-item">
                    <div class="catalog__overlay" data-hash="${i}"></div>
                    <div id="${picture.id}" class="catalog__add"></div>
                <c:choose>
                    <c:when test="${v > 0}">
                        <c:set var="v" value="${v - 1}" />
                        <img class="catalog__image" src="../${picture.base64code}" alt=""/>
                    </c:when>
                    <c:otherwise>
                        <img class="catalog__image" data-original="../${picture.base64code}" alt=""/>
                        <noscript><img class="catalog__image" src="../${picture.base64code}" alt=""/></noscript>
                    </c:otherwise>
                </c:choose>
                </li>
            </c:forEach>
        </ul>
        <div class="catalog__line">

        </div>
    </section>

    <div class="bottom">
        <div class="bottom__logo">
            <img src="<%= request.getContextPath()%>/images/helmet__logo.jpg" alt=""/>
        </div>
    </div>

</div>

<footer class="footer">
    <span>Copyright � 2014 Fotaflo. All rights reserved.</span>
</footer>

<div id="fotoramaHide" class="fotoramaHide"
     data-auto="false"
     data-allowfullscreen="native"
     data-nav="false"

        >

</div>
<!--MODAL WINDOWS-->

<jsp:include page="forms/share.jsp" />

<jsp:include page="forms/maillink.jsp" />

<jsp:include page="forms/contactus.jsp" />



<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.form.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.cookie.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/caroufredsell.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.easyModal.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.lazyload.js"></script>

<script type="text/javascript" src="<%= request.getContextPath()%>/js/jquery.kyco.preloader.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.loadmask.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/fotorama/fotorama.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/service.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/main.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        if($('.js-image-list .js-image-item').size()>0){
            $('.js-image-list').kycoPreload({
                loaderText: 'Loading your pictures, please wait...',
                debugMode: false,
                showInContainer: true,
                truePercentage: false,
                useOpacity: true,
                progressiveReveal: true,
                forceSequentialLoad: false
            });
        }

        function isiPhone() {
            return (
                    (navigator.platform.indexOf("iPhone") != -1) ||
                            (navigator.platform.indexOf("iPod") != -1)
                    );
        }

        if (isiPhone()) {
            $('.catalog__overlay').remove();
            $('.catalog__image').addClass('grayscale')
        }
    })
</script>

</body>
</html>
