/**
 *  Analytics
 */

var fotaflo = fotaflo || {};

fotaflo.analytics = (function(){
    var me = {};

    me.createGeographyDataTable = function(){
        var $table = $('.geo__table'),
            $body = $table.find('tbody');

        $body.remove();
    }

    me.parametersChangeHandler = function(e){
        var date = $('[name=date]').val(),
            location = $('[name=location]').val(),
            startDate , endDate,
            params = {
                'location': location != "null" ? location : null,
                'date': date
            },
            url = '?';

        console.log('params', params)
        if(location != "null"){
            url += 'location='+location+'&';
        }

        if(date){
            date = date.split(',');
            if(date && date.length == 2){
                startDate = date[0];
                endDate = date[1];
            }
        }
        if(startDate && endDate){
            url += 'startDate='+startDate;
            url += '&';
            url += 'endDate='+endDate;
            url += '&';
        }
        url += 'date='+date;

        document.location = url;
    }

    // calendar
    var initLayout = function() {

        $('#js-calendar').DatePicker({
            format:'m/d/Y',
            date: $('#js-calendar').val(),
            current: $('#js-calendar').val(),
            calendars: 2,
            mode: 'range',
            position: 'bottom',
            starts: 1,

            onBeforeShow: function(){
                $('#js-calendar').DatePickerSetDate($('#js-calendar').val(), true);
            },
            onChange: function(formated, dates){
                $('#js-calendar').val(formated);
            },
            onHide: function(){
                $('#js-calendar').trigger('change');
            }

        });

    };

    EYE.register(initLayout, 'init');

    $('#js-location').on('change', me.parametersChangeHandler);
    $('#js-calendar').on('change', me.parametersChangeHandler);

})()