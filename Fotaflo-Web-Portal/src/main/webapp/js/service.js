/**
 *  Remote functions
 */

var fotaflo = fotaflo || {};

fotaflo.service = (function(){
    var me = {};

    me.connectToSocialNetwork = function(socialNet, handler){
        me.logoutFromSocialNetwork(socialNet, function(){
            me.createSocialNetAuthWindow(socialNet, handler);
        });
    }

    me.logoutFromSocialNetwork = function(socialNet, handler){
        if(socialNet == 'facebook'){
            me.facebookLogout(handler);
        }else if(socialNet == 'tumblr'){
            me.tumblrLogout(handler);
        }else{
            handler.call(me);
        }

    }

    me.loadFacebookApi = function(handler){
        window.fbAsyncInit = function(){
            $.get('share/facebook', function(response){
                FB.init({appId: response.appId, status:true, cookie:true, xfbml:true});

                handler.call(me);
            })
        };
        if($('#fb-root').length == 0){ $(document.body).append('<div id="fb-root"></div>');}
        var js, id = 'facebook-jssdk'; if (document.getElementById(id)) {return;}
        js = document.createElement('script'); js.id = id; js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        document.getElementsByTagName('head')[0].appendChild(js);
    }

    me.facebookLogout = function(handler){
        var logoutFn = function(){
            FB.getLoginStatus(function(response){
                if(response.status == 'connected'){
                    FB.logout(function(response){
                        console.log('facebook logout: DONE')
                        handler.call(me, true);
                    });
                }else{
                    handler.call(me, false);
                }
            });
        }
        if(me.facebookLoaded){
            logoutFn();
        }else{
            me.loadFacebookApi(function(){
                me.facebookLoaded = true;
                logoutFn();
            });
        }
    }

    me.tumblrLogout = function(handler){
        var type = 'sociallogoutsuccess';

        var callback = function(event){
            console.log('tumblr logout: DONE')
            handler.call(this, event);
        }

        $(window).off(type);
        $(window).one(type, callback);

        var win = window.open("../../connect/tumblrLogout.jsp", "tumblrLogoutWindow", "width=10,height=10");
    }

    // show social network authenification popup
    me.createSocialNetAuthWindow = function(socialNet, handler){
        var win = window.open("", "connectWindow", "width=600,height=400");
        var cTP = $('<form action="../connect/'+ socialNet +'" method="POST"></form>')/*$(event.currentTarget).parent()*/;
        if(socialNet == 'facebook'){
            cTP.append('<input type="hidden" name="scope" value="publish_stream,user_photos,user_friends,offline_access" />');
        }
        cTP[0].setAttribute("target", "connectWindow");
        $(document.body).append(cTP)

        var type = 'socialauthsuccess';

        var callback = function(event){
            if(handler){
                handler.call(this, event);
            }
            cTP.remove();
        }

        $(window).off(type);
        $(window).one(type, callback);

        cTP[0].submit();
        win.focus();
    }

    // get all available social networks to which user is connected
    me.getSocialNetworkConnections = function(handler){
        var nets = [];
        $.get("connections",function(data,status){
            if(data != null){
                for(var i=0; i<data.length; i++){
                    if(data[i].service && data[i].connected){
                        nets[data[i].service] = data[i].name;
                    }
                }
            }

            handler.call(this, nets);
        });
    }

    return me;
})()
