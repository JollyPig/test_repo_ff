<%@ page session="true" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<!doctype html>
<!--[if lt IE 8]> <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9 ie8"> <![endif]-->
<!--[if IE 9]>    <html class="ie9""> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->

<head>
    <jsp:include page="layout/head.jsp" />

</head>
<body>

<div class="page">
    <header class="head head_page_inner">
        <div class="wrapper">
            <nav class="head__nav head__nav_page_inner">
                <ul class="head__list head__list_page_inner">
                    <li><a class="head__link" href="">Home</a></li>
                    <li><a class="head__link js-contactlink" href="">Contact us</a></li>
                    <li><a class="head__link" href="<%= request.getContextPath()%>/pictures/auth/logout">Sign out</a></li>
                </ul>
                <div class="head__nav-ico"></div>
            </nav>

        </div>
    </header>


    <menu class="menu js-sticky">
        <div class="menu__wrapper">
            <ul class="menu__list menu__list_position_left">
                <li><a href="common" class="js-manage-socialnet">Manage Social Network</a></li>
                <li><a class="menu__item js-manage-promotions" href="promotions">Manage Promotions</a></li>
                <c:if test="${isadmin}">
                    <li><a class="menu__item js-manage-paypal" href="paypal">Manage PayPal Settings</a></li>
                </c:if>
            </ul>

            <ul class="menu__list menu__list_position_center">
            </ul>
        </div>
    </menu>

    <section class="manage">
        <h2 class="manage__title">Manage Social Network</h2>
        <div class="manage__form">
            <form class="mform" action="">
                <div class="mform__row">
                    <div class="mform__cell">
                        <label class="mform__label">Social Network</label>
                        <select class="mform__select js-manage-sn-name" name="name">
                            <c:forEach var="name" items="${socialNetworkNames}">
                                <option value="${name}">${name.label}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="mform__cell">
                        <label class="mform__label">Location</label>
                        <select class="mform__select js-manage-sn-location" name="location">
                            <c:forEach var="location" items="${locations}">
                                <c:choose>
                                    <c:when test="${userLocation == location.id}">

                                        <option value="${location.id}" selected>${location.name}</option>
                                    </c:when>

                                    <c:otherwise>
                                        <option value="${location.id}">${location.name}</option>
                                    </c:otherwise>
                                 </c:choose>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="mform__row">
                    <label class="mform__label">Tags</label>
                    <input class="mform__input" type="text" name="tags" placeholder="@"/>
                </div>
                <div class="mform__row">
                    <label class="mform__label">Hashtags</label>
                    <input class="mform__input" type="text" name="hashtags" placeholder="#"/>
                </div>
                <div class="mform__row">
                    <label class="mform__label">Address</label>
                    <input class="mform__input" type="text" name="address"/>
                </div>
                <div class="mform__row">
                    <label class="mform__label">Album title</label>
                    <input class="mform__input" type="text" name="title"/>
                </div>
                <div class="mform__row">
                    <label class="mform__label">Album description</label>
                    <textarea class="mform__text" type="text" name="description"/></textarea>
                </div>
                <div class="mform__row mform__row_align_center">
                    <input class="mform__submit" type="submit" value="Save"/>
                </div>
            </form>
        </div>
    </section>

    <div class="bottom">
        <div class="bottom__logo">
            <img src="<%= request.getContextPath()%>/images/helmet__logo.jpg" alt=""/>
        </div>
    </div>

</div>

<footer class="footer">
    <span>Copyright � 2014 Fotaflo. All rights reserved.</span>
</footer>

<!--MODAL WINDOWS-->


<jsp:include page="forms/contactus.jsp" />


<script type="text/javascript" src="<%= request.getContextPath()%>/js/jquery.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.form.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.cookie.js"></script>
<script type="text/javascript" src="<%= request.getContextPath()%>/js/caroufredsell.js"></script>

<script type="text/javascript" src="<%= request.getContextPath()%>/js/jquery.easyModal.js"></script>
<script type="text/javascript" src="<%= request.getContextPath()%>/js/jquery.lazyload.js"></script>


<script type="text/javascript" src="<%= request.getContextPath()%>/js/main.js"></script>
<script>

</script>
</body>
</html>
