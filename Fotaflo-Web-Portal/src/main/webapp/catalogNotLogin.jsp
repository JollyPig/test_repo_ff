<%@ page import="org.springframework.social.facebook.api.Page" %>
<%@ page session="true" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<!doctype html>
<!--[if lt IE 8]> <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9 ie8"> <![endif]-->
<!--[if IE 9]>    <html class="ie9""> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->

<head>
    <jsp:include page="layout/head.jsp" />

    <%--<style>


        .catalog__logo {
            position: absolute;
            width: 30px;
            /*height: 30px;*/
            bottom: 10px;
            left: 10px;
            z-index: 2;
            background-size: contain;
        }

        .catalog__mainlogo {
            position: absolute;
            width: 30px;
            /*height: 30px;*/
            right: 10px;
            bottom: 10px;
            z-index: 2;
            background-size: cover;
        }

        .catalog__logotext {
            position: absolute;
            right: 50px;
            left: 50px;
            bottom: 10px;
            z-index: 2;
            color: #ffffff;
            text-shadow: #000000;
            font-size: 12px;
        }
    </style>--%>

</head>
<body>

<jsp:include page="layout/analytics.jsp" />

<div class="page">
    <header class="head head_page_inner">
        <div class="wrapper">
            <nav class="head__nav head__nav_page_inner">
                <ul class="head__list head__list_page_inner">
                    <%--<li><a class="head__link" href="<%= request.getContextPath()%>/pictures/main/common">Home</a></li>--%>
                    <li><a class="head__link js-contactlink" href="">Contact us</a></li>
                    <li><a class="head__link" href="<%= request.getContextPath()%>/pictures/auth/logout">Sign out</a></li>
                </ul>
                <div class="head__nav-ico"></div>
            </nav>

        </div>
    </header>

    <menu class="menu js-sticky">
        <div class="menu__wrapper">
        <ul class="menu__list menu__list_position_left">
            <li><a href="" class="js-select-all">Select all</a></li>
            <li><a href="" class="js-deselect-all">Reset all</a></li>
        </ul>
        <div class="menu__counter">0</div>
        <ul class="menu__list menu__list_position_right">
            <li><span>Selected photos</span></li>
        </ul>

        <ul class="menu__list menu__list_position_center">
            <li><a href="" id = "buy_pictures" class="menu__item menu__item_ico_share menu__item_bay_now" >Buy Now</a></li>
             <li class="menu__money"><span>$</span><span class="js-money">0</span></li>
           <%-- <li class="menu__price"></li>--%>
        </ul>
            <input type=hidden name=menuprice class="menu__price" value="">
        </div>
    </menu>

    <section class="catalog catalog_login_none">
        <ul id="catalog_list_id" class="catalog__list">
            <c:forEach var="picture" items="${pictures}">
                <li class="catalog__item  js-image-item">
                    <%--<div class="catalog__overlay"></div>--%>
                    <div id="${picture.id}" class="catalog__add"></div>
                    <%--<c:if test="${sessionScope.mobile == false}">
                        <c:if test="${not empty picture.logoUrl}">
                            <img class="catalog__logo" src="../${picture.logoUrl}" title = "" alt="">
                        </c:if>
                        <div class="catalog__logotext">${picture.logoText}</div>
                        <c:if test="${not empty picture.logoMainUrl}">
                            <img class="catalog__mainlogo" src="../${picture.logoMainUrl}" title = "" alt="">
                        </c:if>
                    </c:if>--%>
                    <div class="catalog__watermark"></div>
                    <img class="catalog__image" src="../${picture.base64code}" alt=""/></li>
                </li>
            </c:forEach>
        </ul>
        <div class="catalog__line">

        </div>
    </section>

    <div class="bottom">
        <div class="bottom__logo">
            <img src="<%=request.getContextPath()%>/images/helmet__logo.jpg" alt=""/>
        </div>
    </div>

</div>

<footer class="footer">
    <span>Copyright � 2014 Fotaflo. All rights reserved.</span>
</footer>


<div id="fotoramaHide" class="fotoramaHide"
     data-auto="false"
     data-allowfullscreen="native"
     data-nav="false"

        >

</div>

<!--MODAL WINDOWS-->
<jsp:include page="forms/contactus.jsp" />
<jsp:include page="forms/buyemail.jsp" />


<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/caroufredsell.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.form.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.cookie.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.easyModal.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.lazyload.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/fotorama/fotorama.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/service.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/main.js"></script>



<script>

     $(function(){
         afterLoad();

         $('#buy_pictures').click(function(e){
             $('#buyemailForm').trigger('openModal');
             e.preventDefault();
         });
     })

    function afterLoad(){
         var payed = ${payed};
         var success= ${success};
         var messs = "${msg}";
         var price = "${price}";


        $('.menu__price').val(price);
        if(payed==true && success==true){
            alert("The payment was successful. Your new code is " + messs + " and it was sent you via email");
        }else{
            if(payed==false && success==false){
                alert("Error: " + messs);
            }
        }

        var $selected = $('#catalog_list_id .catalog__item_state_seleted'),
                count = $selected.length;

        if($('.js-money').length!=0){
            $('.js-money').html(count*price);
        }
    }

    function initPayPalPicture(){
        var $selected = $('#catalog_list_id .catalog__item_state_seleted'),
                id
        if($selected.length == 0){
           return false;
           /* $selected = $('#catalog_list_id .catalog__item');*/
        }
        var stringIds = "";
        $selected.each(function(i, item){
            id = $(item).find('div.catalog__add').attr('id');
            if(stringIds==""){
                stringIds= "id_"+id;
            }else{
                stringIds=stringIds+"," +  "id_"+id;
            }

        });

        return stringIds;
    }

    $('#continue_with_paypal').click(function(e) {
        $('#buyemailForm').trigger('closeModal');

        e.preventDefault();
        payWithPayPal();
    });


    function payWithPayPal(){

        var picturesForPayPalIds = initPayPalPicture();
        var pictureEmail = $('#buyer_email_input').val() ;
        var money = $('.js-money')[0].innerHTML;
        if(picturesForPayPalIds!=false){
            $.ajax({
                url: "../../registerPayment.json",

                data: {
                    'totalPrice':  money,
                    'pictureIds':  picturesForPayPalIds,
                    'pictureEmail':  pictureEmail,
                    'picturesSize': $('.catalog__item_state_seleted').size()
                }

            }).done(function( data ) {
                if(data.success==false){
                    alert("Error: " + data.msg);
                }else{
                    fotaflo_ga.send('event', 'purchase', 'click', 'click buy now');
                    window.location = data.msg;
                }

            })
            .fail(function() {
                alert( "Error!" );
            })
        }else{
            alert("Please select pictures.");
        }
    }


</script>

</body>
</html>
