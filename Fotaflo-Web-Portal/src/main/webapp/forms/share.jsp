
<div class="share js-modal-window" id="shareForm">
    <div class="share__wrapper">
        <div class="modal-close"></div>
        <form action="<%=request.getContextPath()%>/pictures/main/share" method="post">
            <ul class="share__sochial">
                <li data-link="twitter" class="share__sochial-item share__sochial-item_net_tw"></li>
                <li data-link="facebook" class="share__sochial-item share__sochial-item_net_fb"></li>
                <li data-link="tumblr" class="share__sochial-item share__sochial-item_net_tb"></li>
            </ul>

            <textarea class="share_text" name="message"></textarea>

            <ul class="share__small">
                <li data-link="twitter" class="share__small-item share__small-item_net_tw"><i></i><input class="share__small-input" type="text" /></li>
                <li data-link="facebook" class="share__small-item share__small-item_net_fb"><i></i><input class="share__small-input" type="text" /></li>
                <li data-link="tumblr" class="share__small-item share__small-item_net_tb"><i></i><input class="share__small-input" type="text" /></li>
            </ul>

            <div class="share__clear">
                <div class="share__counter">0</div>
                <ul class="share__select">
                    <li>Selected photos</li>
                </ul>
            </div>

            <div class="share__slider">
                <ul class="share__list">
                </ul>
                <div class="share__slider-prev"></div>
                <div class="share__slider-next"></div>
            </div>

            <input class="share__submit" type="submit"  value="Share"/>
            <div class="share__alert" style="display: none;"></div>
        </form>
    </div>
</div>