<%--
  Goal form
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="share share_type_goal" id="goalForm">
    <div class="share__wrapper">
        <div class="modal-close"></div>

        <h3 class="share__title">Set new goal</h3>
        <form action="">

            <label class="go__label">Subject:</label>
            <select class="go__select">
                <option value="">first</option>
                <option value="">second</option>
                <option value="">third</option>
            </select>

            <label class="go__label">Date:</label>
            <input class="go__input" type="text"/>

            <label class="go__label">Goal Metric:</label>
            <select class="go__select">
                <option value="">first</option>
                <option value="">second</option>
                <option value="">third</option>
            </select>

            <label class="go__label">Value:</label>
            <input class="go__input go__input_type_short" type="text"/>


            <input class="share__submit" type="submit" value="Send"/>
        </form>
    </div>
</div>