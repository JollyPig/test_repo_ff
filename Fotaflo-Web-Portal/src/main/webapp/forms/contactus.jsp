<div class="share js-modal-window" id="contactForm">
    <div class="share__wrapper">
        <div class="modal-close"></div>

        <h3 class="share__title">Contact us</h3>
        <form action="<%=request.getContextPath()%>/pictures/main/contactus" method="post">

            <label class="share__label">Email</label>
            <input class="share__input" name="email" type="email" required="true"/>
            <label class="share__label">Subject</label>
            <input class="share__input" name="subject" type="text" required="true"/>
            <label class="share__label">Message</label>
            <textarea class="share_text" name="message" maxlength="4000" required="true"></textarea>


            <input class="share__submit" type="submit" value="Send"/>
        </form>
    </div>
</div>