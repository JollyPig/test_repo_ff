<%@ page session="true" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<!doctype html>
<!--[if lt IE 8]> <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9 ie8"> <![endif]-->
<!--[if IE 9]>    <html class="ie9""> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->

<head>
    <jsp:include page="layout/head.jsp" />

    <link rel="stylesheet" href="<%= request.getContextPath()%>/css/graps.css">

</head>
<body>

<div class="page">
    <header class="head head_page_inner">
        <div class="wrapper">
            <jsp:include page="layout/headNavigation.jsp" />

        </div>
    </header>



    <section class="an">
        <jsp:include page="layout/analyticsNavigation.jsp" />
        
        
        <div class="tabs">
          <div class="tabs__top">
              <jsp:include page="layout/analyticsParameterTab.jsp" />

          </div>
            <div class="geo">
                <div id="geoGraph">

                </div>
                
                <table class="geo__table">
                    <thead>
                    <tr>
                        <th>City</th>
                        <th>Visits</th>
                        <th>Percent</th>
                    </tr>
                    </thead>
                    
                    <tbody>
                    <c:forEach var="item" items="${data.records}">
                        <tr>
                            <td><div class="geo__pad geo__pad_font_small">${item.get(0)}</div></td>
                            <td><div class="geo__pad">${item.get(1)}</div></td>
                            <td><div class="geo__pad"><fmt:formatNumber type="PERCENT" maxFractionDigits="2">${item.get(1)/data.total}</fmt:formatNumber></div></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>

            </div>

        </div>
        <div class="an__footer">
        <div class="an__icons">
            <div class="an__icon an__icon_type_print"></div>
            <div class="an__icon an__icon_type_download"></div>
            <div class="an__icon an__icon_type_mail"></div>
            <div class="an__icon an__icon_type_blank"></div>
        </div>
        </div>

        <div class="an__total">
            total: <span>${data.total}</span>
            <c:if test="${data.oldTotal > 0}">
            <div class="adm__cell-per adm__cell-per_type_p"><fmt:formatNumber type="PERCENT" maxFractionDigits="2">${(data.total-data.oldTotal)/data.oldTotal}</fmt:formatNumber></div>
            </c:if>
        </div>
    </section>

    <div class="bottom">
        <div class="bottom__logo">
            <img src="<%= request.getContextPath()%>/images/helmet__logo.jpg" alt=""/>
        </div>
    </div>

</div>

<footer class="footer">
    <span>Copyright � 2014 Fotaflo. All rights reserved.</span>
</footer>

<!--MODAL WINDOWS-->

<jsp:include page="forms/contactus.jsp" />

<jsp:include page="layout/scripts.jsp" />

<script>

//    calendar
    (function($){

        var initChart = function(){
            var $table = $('.geo__table'),
                $rows = $table.find('tbody tr'),
                    $data, data=[];
            console.log($rows)
            for(var i=0; i<10 && i<$rows.length; i++){
                console.log($rows[i])
                $data = $($rows[i]).find('.geo__pad');
                data.push({
                    'country': $($data[0]).text(),
                    'visits': $($data[1]).text(),
                    "color": "#0000FF"
                });
            }
            var chart;

            AmCharts.ready(function () {
                console.log(data)
                // SERIAL CHART
                chart = new AmCharts.AmSerialChart();
                chart.dataProvider = data;
                chart.categoryField = "country";
                chart.startDuration = 1;

                // AXES
                // category
                var categoryAxis = chart.categoryAxis;
                categoryAxis.labelRotation = 0;
                categoryAxis.labelColor = "#b0b0b0";
                categoryAxis.gridAlpha = 0;
                categoryAxis.fillAlpha = 0;
                categoryAxis.fontSize = 13;
                categoryAxis.fillColor = "#b0b0b0";
                categoryAxis.gridPosition = "start";

                // value
                var valueAxis = new AmCharts.ValueAxis();
                valueAxis.dashLength = 0;
                valueAxis.axisAlpha = 0;
                chart.addValueAxis(valueAxis);

                // GRAPH
                var graph = new AmCharts.AmGraph();
                graph.valueField = "visits";
                graph.colorField = "color";
                graph.balloonText = "<b>[[category]]: [[value]]</b>";
                graph.type = "column";
                graph.lineAlpha = 0;
                graph.fillAlphas = 1;
                chart.addGraph(graph);

                // CURSOR
                var chartCursor = new AmCharts.ChartCursor();
                chartCursor.cursorAlpha = 0;
                chartCursor.zoomable = false;
                chartCursor.categoryBalloonEnabled = false;
                chart.addChartCursor(chartCursor);

                chart.creditsPosition = "top-right";

                // WRITE
                chart.write("geoGraph");
            });
        }

        initChart();



        /*var $table = $('.geo__table'),
                $body = $table.find('tbody');

        $body.remove();*/
    })(jQuery)
</script>
<script type="text/javascript">
    var chart;

    /*var chartData = [
        {
            "country": "USA",
            "visits": 400,
            "color": "#FF0F00"
        },
        {
            "country": "China",
            "visits": 358,
            "color": "#FF6600"
        },
        {
            "country": "Japan",
            "visits": 220,
            "color": "#FF9E01"
        },
        {
            "country": "Germany",
            "visits": 420,
            "color": "#FCD202"
        },
        {
            "country": "UK",
            "visits": 560,
            "color": "#F8FF01"
        },
        {
            "country": "France",
            "visits": 385,
            "color": "#B0DE09"
        },
        {
            "country": "India",
            "visits": 125,
            "color": "#04D215"
        },
        {
            "country": "Spain",
            "visits": 200,
            "color": "#0D8ECF"
        },
        {
            "country": "Netherlands",
            "visits": 240,
            "color": "#0D52D1"
        },
        {
            "country": "Russia",
            "visits": 580,
            "color": "#2A0CD0"
        },
        {
            "country": "South Korea",
            "visits": 400,
            "color": "#8A0CCF"
        },
        {
            "country": "Canada",
            "visits": 441,
            "color": "#CD0D74"
        }
    ];


    AmCharts.ready(function () {
        // SERIAL CHART
        chart = new AmCharts.AmSerialChart();
        chart.dataProvider = chartData;
        chart.categoryField = "country";
        chart.startDuration = 1;

        // AXES
        // category
        var categoryAxis = chart.categoryAxis;
        categoryAxis.labelRotation = 0;
        categoryAxis.labelColor = "#b0b0b0";
        categoryAxis.gridAlpha = 0;
        categoryAxis.fillAlpha = 0;
        categoryAxis.fontSize = 13;
        categoryAxis.fillColor = "#b0b0b0";
        categoryAxis.gridPosition = "start";

        // value
        var valueAxis = new AmCharts.ValueAxis();
        valueAxis.dashLength = 0;
        valueAxis.axisAlpha = 0;
        chart.addValueAxis(valueAxis);

        // GRAPH
        var graph = new AmCharts.AmGraph();
        graph.valueField = "visits";
        graph.colorField = "color";
        graph.balloonText = "<b>[[category]]: [[value]]</b>";
        graph.dataType = "column";
        graph.lineAlpha = 0;
        graph.fillAlphas = 1;
        chart.addGraph(graph);

        // CURSOR
        var chartCursor = new AmCharts.ChartCursor();
        chartCursor.cursorAlpha = 0;
        chartCursor.zoomable = false;
        chartCursor.categoryBalloonEnabled = false;
        chart.addChartCursor(chartCursor);

        chart.creditsPosition = "top-right";

        // WRITE
        chart.write("geoGraph");
    });*/
</script>
</body>
</html>
