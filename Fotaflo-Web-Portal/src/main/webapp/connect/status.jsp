<%--
  Spring Social connect status page. Open when success authentification is proceeded.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
<script>
    $(function(){
        if(window.opener) {
            var type = 'socialauthsuccess';
            //window.opener.location.reload(false);

            // notify parent window that auth is successful
            window.opener.$(window.opener).trigger(type);
            window.close();
        }
    });
</script>

</body>
</html>