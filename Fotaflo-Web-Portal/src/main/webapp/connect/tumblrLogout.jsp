<!DOCTYPE html>
<html>
<head>
    <title>Tumblr Logout</title>
    <script>
        function loadHandler(){
            var type = 'sociallogoutsuccess';

            // notify parent window that auth is successful
            window.opener.$(window.opener).trigger(type);
            window.close();
        }
    </script>
</head>
<body>

    <iframe src="https://www.tumblr.com/logout" onload="loadHandler()" width="100%" height="100%">
        Ваш браузер не поддерживает плавающие фреймы!
    </iframe>

</body>
</html>