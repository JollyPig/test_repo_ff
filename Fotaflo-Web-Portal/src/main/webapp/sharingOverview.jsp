<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@ page session="true" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<!doctype html>
<!--[if lt IE 8]> <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9 ie8"> <![endif]-->
<!--[if IE 9]>    <html class="ie9""> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->

<head>
    <jsp:include page="layout/head.jsp" />

    <link rel="stylesheet" href="<%= request.getContextPath()%>/css/graps.css">

</head>
<body>

<div class="page">
    <header class="head head_page_inner">
        <div class="wrapper">
            <jsp:include page="layout/headNavigation.jsp" />

        </div>
    </header>



    <section class="an">

        <jsp:include page="layout/analyticsNavigation.jsp" />
        
        <div class="tabs">
            <div class="tabs__top">
                <jsp:include page="layout/analyticsParameterTab.jsp" />

                <ul class="tabs__head">
                  <li class="thead active">Day</li>
                  <li class="thead">Week</li>
                  <li class="thead">Month</li>
                </ul>
            </div>


            <div class="loy">
                <div id="loyGraph" class="loygraph">

                </div>
            </div>

            <%
                Map settings = new HashMap(){{
                    put("facebook", new String[]{"Facebook", "adm-type-facebook.png", "or"});
                    put("twitter",  new String[]{"Twitter", "adm-type-twitter.png", "lm"});
                    put("mail",     new String[]{"Mails", "adm-type-mail.png", "gr"});
                    put("tumblr",   new String[]{"Tumblr", "adm-type-tumblr.png", "rd"});
                }};

                request.setAttribute("settings", settings);
            %>

            <div class="adm">
                <c:forEach var="entry" items="${data.records}" >
                <div class="adm__block">
                    <div class="adm__cell adm__cell_type_w adm__cell_bg_${settings.get(entry.key)[2]}">
                        <img class="adm__type-img" src="<%= request.getContextPath()%>/images/${settings.get(entry.key)[1]}" alt=""/>
                        <span class="adm__type-text">${settings.get(entry.key)[0]}</span>
                    </div>
                    <div class="adm__cell adm__cell_type_p ">
                        <c:if test="${entry.value.get('oldValue') > 0}">
                        <div class="adm__cell-per adm__cell-per_type_p">
                            <fmt:formatNumber type="PERCENT" maxFractionDigits="2">${(entry.value.get("value")-entry.value.get("oldValue"))/entry.value.get("oldValue")}</fmt:formatNumber>
                        </div>
                        </c:if>
                        <div class="adm__cel-numbers">${entry.value.get("value")}</div>
                    </div>
                    <div class="adm__cell adm__cell_type_b">
                        <span class="adm__pers">
                            <fmt:formatNumber type="PERCENT" maxFractionDigits="2">${entry.value.get("value")/data.total}</fmt:formatNumber>
                        </span>
                    </div>
                    <div class="adm__cell adm__cell_type_g">
                        <div id="graphFacebook" class="graph">

                        </div>
                    </div>

                </div>
                </c:forEach>


                <%--
                <div class="adm__block">
                    <div class="adm__cell adm__cell_type_w adm__cell_bg_or">
                        <img class="adm__type-img" src="<%= request.getContextPath()%>/images/adm-type-fb.png" alt=""/>
                        <span class="adm__type-text">Facebook</span>
                    </div>
                    <div class="adm__cell adm__cell_type_p ">
                        <div class="adm__cell-per adm__cell-per_type_p">15%</div>
                        <div class="adm__cel-numbers">1002</div>
                    </div>
                    <div class="adm__cell adm__cell_type_b">
                        <span class="adm__pers">
                            60%
                        </span>
                    </div>
                    <div class="adm__cell adm__cell_type_g">
                        <div id="graphFacebook" class="graph">

                        </div>
                    </div>

                </div>

                <div class="adm__block">
                    <div class="adm__cell adm__cell_type_w adm__cell_bg_lm">
                        <img class="adm__type-img" src="<%= request.getContextPath()%>/images/adm-type-tw.png" alt=""/>
                        <span class="adm__type-text">Twitter</span>
                    </div>
                    <div class="adm__cell adm__cell_type_p">
                        <div class="adm__cell-per adm__cell-per_type_p">15%</div>
                        <div class="adm__cel-numbers">1002</div>
                    </div>
                    <div class="adm__cell adm__cell_type_b ">
                        <span class="adm__pers adm__pers_color_lm">
                            60%
                        </span>
                    </div>
                    <div class="adm__cell adm__cell_type_b ">
                        <span class="adm__pers adm__pers_color_lm">
                            60%
                        </span>
                    </div>
                    <div class="adm__cell adm__cell_type_b ">
                        <span class="adm__pers adm__pers_color_lm">
                            60%
                        </span>
                    </div>
                    <div class="adm__cell adm__cell_type_b">
                        <div class="adm__cell-per adm__cell-per_type_p">15%</div>
                        <div class="adm__cel-numbers">1002</div>
                    </div>

                </div>


                <div class="adm__block">
                    <div class="adm__cell adm__cell_type_w adm__cell_bg_gr">
                        <img class="adm__type-img" src="<%= request.getContextPath()%>/images/adm-type-mailr.png" alt=""/>
                        <span class="adm__type-text">Mails</span>
                    </div>
                    <div class="adm__cell adm__cell_type_p">
                        <div class="adm__cell-per adm__cell-per_type_p">15%</div>
                        <div class="adm__cel-numbers">1002</div>
                    </div>
                    <div class="adm__cell adm__cell_type_b">
                        <span class="adm__pers adm__pers_color_gr">
                            60%
                        </span>
                    </div>
                    <div class="adm__cell adm__cell_type_b">
                        <span class="adm__pers adm__pers_color_gr">
                            60%
                        </span>
                    </div>
                    <div class="adm__cell adm__cell_type_b">
                        <span class="adm__pers adm__pers_color_gr">
                            60%
                        </span>
                    </div>
                    <div class="adm__cell adm__cell_type_b">
                        <div class="adm__cell-per adm__cell-per_type_p">15%</div>
                        <div class="adm__cel-numbers">1002</div>
                    </div>

                </div>


                <div class="adm__block">
                    <div class="adm__cell adm__cell_type_w adm__cell_bg_rd">
                        <img class="adm__type-img" src="<%= request.getContextPath()%>/images/adm-type-tmb.png" alt=""/>
                        <span class="adm__type-text">Tumbler</span>
                    </div>
                    <div class="adm__cell adm__cell_type_p">
                        <div class="adm__cell-per adm__cell-per_type_p">15%</div>
                        <div class="adm__cel-numbers">1002</div>
                    </div>
                    <div class="adm__cell adm__cell_type_b">
                        <span class="adm__pers adm__pers_color_rd">
                            60%
                        </span>
                    </div>
                    <div class="adm__cell adm__cell_type_b">
                        <span class="adm__pers adm__pers_color_rd">
                            60%
                        </span>
                    </div>
                    <div class="adm__cell adm__cell_type_b">
                        <span class="adm__pers adm__pers_color_rd">
                            60%
                        </span>
                    </div>
                    <div class="adm__cell adm__cell_type_b">
                        <div class="adm__cell-per adm__cell-per_type_p">15%</div>
                        <div class="adm__cel-numbers">1002</div>
                    </div>

                </div>--%>

            </div>

        </div>
        <div class="an__footer">
        <div class="an__icons">
            <div class="an__icon an__icon_type_print"></div>
            <div class="an__icon an__icon_type_download"></div>
            <div class="an__icon an__icon_type_mail"></div>
            <div class="an__icon an__icon_type_blank"></div>
        </div>
        </div>

        <div class="an__total">
            total: <span>${data.total}</span>
        </div>
    </section>

    <div class="bottom">
        <div class="bottom__logo">
            <img src="<%= request.getContextPath()%>/images/helmet__logo.jpg" alt=""/>
        </div>
    </div>

</div>

<footer class="footer">
    <span>Copyright � 2014 Fotaflo. All rights reserved.</span>
</footer>

<!--MODAL WINDOWS-->

<jsp:include page="forms/contactus.jsp" />

<jsp:include page="layout/scripts.jsp" />

<script>

//    calendar
    (function($){


        $('.an__icon_type_blank').click(function(){
            var $form = $('<form hidden="hidden"></form>'),
                    $location = $('<input type="hidden" name="location">'),
                    $date = $('<input type="hidden" name="date">'),
                    $range = $('<input type="hidden" name="range">');
            $form.attr('method', 'POST');
            $form.attr('action', 'download');

            $location.val($('[name=location]').val());
            $form.append($location);

            $date.val($('[name=date]').val());
            $form.append($date);

            $range.val(1);
            $form.append($range);

            $(document.body).append($form);

            $form.trigger('submit');
        })


        var date = $('[name=date]').val(),
                location = $('[name=location]').val(),
                params = {
                    'date': date
                };
        if(location != "null"){
            params.location = location;
        }

        $.get("share/graph",params,function(response){
            console.log("share/graph",response)
            /*chart = new AmCharts.AmSerialChart();
            chart.dataProvider = response.data;
            chart.categoryField = "date";
            chart.startDuration = 0;
            chart.dataDateFormat = "MM.DD.YYYY";
            chart.balloon.color = "#000000";

            // AXES
            // category
            var categoryAxis = chart.categoryAxis;
            categoryAxis.fillAlpha = 0;
            categoryAxis.fillColor = "#FAFAFA";
            categoryAxis.gridAlpha = 0;
            categoryAxis.axisAlpha = 0;
            categoryAxis.gridPosition = "start";
            categoryAxis.position = "left";

            // value
            var valueAxis = new AmCharts.ValueAxis();
            valueAxis.dashLength = 0;
            valueAxis.axisAlpha = 0;

            valueAxis.integersOnly = true;
            valueAxis.gridCount = 0;
            chart.addValueAxis(valueAxis);


            var graph = new AmCharts.AmGraph();
            graph.title = "Facebook";
            graph.valueField = "facebook";
            graph.balloonText = "[[category]]: <br /> [[value]]";
            graph.bullet = "round";
            chart.addGraph(graph);

            var graph = new AmCharts.AmGraph();
            graph.title = "Twitter";
            graph.valueField = "twitter";
            graph.labelPosition = "left";
            graph.balloonText = "[[category]]: <br /> [[value]]";
            graph.bullet = "round";
            chart.addGraph(graph);

            var graph = new AmCharts.AmGraph();
            graph.title = "Mails";
            graph.valueField = "mail";
            graph.labelPosition = "left";
            graph.balloonText = "[[category]]: <br /> [[value]]";
            graph.bullet = "round";
            chart.addGraph(graph);

            var graph = new AmCharts.AmGraph();
            graph.title = "Tumblr";
            graph.valueField = "tumblr";
            graph.labelPosition = "left";
            graph.balloonText = "[[category]]: <br /> [[value]]";
            graph.bullet = "round";
            chart.addGraph(graph);

            // WRITE
            chart.write("loyGraph");*/
        });

    })(jQuery)

var chart;

var chartData = [
    {
        "year":  "01/03/2014",
        "newV": 100,
        "retV": 0,
        "anotherV": 500
    },
    {
        "year": "01/04/2014",
        "newV": 200,
        "retV": 160,
        "anotherV": 400
    },
    {
        "year": "01/05/2014",
        "newV": 300,
        "retV": 300,
        "anotherV": 300
    },
    {
        "year": "01/06/2014",
        "newV": 400,
        "retV": 800,
        "anotherV": 200
    },
    {
        "year": "01/07/2014",
        "newV": 500,
        "retV": 300,
        "anotherV": 100
    }
];


AmCharts.ready(function () {
    // SERIAL CHART
    chart = new AmCharts.AmSerialChart();
    chart.dataProvider = chartData;
    chart.categoryField = "year";
    chart.startDuration = 0;
    chart.dataDateFormat = "DD/MM/YYYY";
    chart.balloon.color = "#000000";

    // AXES
    // category
    var categoryAxis = chart.categoryAxis;
    categoryAxis.fillAlpha = 0;
    categoryAxis.fillColor = "#FAFAFA";
    categoryAxis.gridAlpha = 0;
    categoryAxis.axisAlpha = 0;
    categoryAxis.gridPosition = "start";
    categoryAxis.position = "left";

    // value
    var valueAxis = new AmCharts.ValueAxis();
    valueAxis.dashLength = 0;
    valueAxis.axisAlpha = 0;

    valueAxis.integersOnly = true;
    valueAxis.gridCount = 0;
    chart.addValueAxis(valueAxis);


    var graph = new AmCharts.AmGraph();
    graph.title = "New Visitors";
    graph.valueField = "newV";
    graph.balloonText = "[[category]]: <br /> [[value]]";
    graph.bullet = "round";
    chart.addGraph(graph);


    var graph = new AmCharts.AmGraph();
    graph.title = "Returning Visitors";
    graph.valueField = "retV";
    graph.labelPosition = "left";
    graph.balloonText = "[[category]]: <br /> [[value]]";
    graph.bullet = "round";
    chart.addGraph(graph);


    var graph = new AmCharts.AmGraph();
    graph.title = "Returning Visitors";
    graph.valueField = "anotherV";
    graph.labelPosition = "left";
    graph.balloonText = "[[category]]: <br /> [[value]]";
    graph.bullet = "round";
    chart.addGraph(graph);


     chart.balloon.adjustBorderColor = true;
     chart.balloon.color = "#ffffff";
     chart.balloon.borderThickness = 0;
     chart.balloon.cornerRadius = 0;
     chart.balloon.borderColor = "#ff8600";
     chart.balloon.fillColor = "#ff8600";
     chart.balloon.pointerWidth = 8;
     chart.balloon.horizontalPadding = 10;
     chart.balloon.varticalPadding = 10;
     chart.balloon.textAlign = "middle";

    // WRITE
    chart.write("loyGraph");
});

var chartD = AmCharts.makeChart("graphFacebook", {
    "type": "serial",
    "theme": "light",
    "dataDateFormat": "DD/MM/YYYY",
    "pathToImages": "../amcharts/images/",
    "dataProvider": [{
        "date": "01/03/2014",
        "value": 0
    }, {
        "date": "01/04/2014",
        "value": 80
    }, {
        "date": "01/05/2014",
        "value": 70
    }, {
        "date": "01/06/2014",
        "value": 85
    }],
    "valueAxes": [{
        "maximum": 100,
        "minimum": 0,
        "axisAlpha": 0,
        "guides": [{
            "fillAlpha": 1,
            "fillColor": "#FFFFFF",
            "lineAlpha": 0,
            "toValue": 120,
            "value": 0,
            inside: true
        }]
    }],
    "graphs": [{
        "bullet": "round",
        "valueField": "value",
        balloonText: "<span><b>[[value]] Visitors </b> <br />[[date]]</span>"
    }],
    "chartCursor": {
        "cursorAlpha": 0
    },
    "categoryField": "date",
    "categoryAxis": {
        "parseDates": true
    }
});
</script>
</body>
</html>
