<!doctype html>
<!--[if lt IE 8]> <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9 ie8"> <![endif]-->
<!--[if IE 9]>    <html class="ie9""> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->

<head>
    <jsp:include page="layout/head.jsp" />

    <link rel="stylesheet" href="<%= request.getContextPath()%>/css/graps.css">

</head>
<body>

<div class="page">
    <header class="head head_page_inner">
        <div class="wrapper">
            <jsp:include page="layout/headNavigation.jsp" />

        </div>
    </header>



    <section class="an">
        <jsp:include page="layout/analyticsNavigation.jsp" />
        
        <div class="tabs">
          <div class="tabs__top">
              <jsp:include page="layout/analyticsParameterTab.jsp" />

              <ul class="tabs__head">
                  <li class="thead active">Day</li>
                  <li class="thead">Week</li>
                  <li class="thead">Month</li>
              </ul>
          </div>

            <div class="loy">
                <div id="loyGraph" class="loygraph">

                </div>
            </div>


            <div class="adm adm_margin_tb">
                <div class="adm__block">

                    <div class="adm__cell adm__cell_type_sm adm__cell_bg_or">

                            <span class="adm__type-heading"><i>Participants</i></span>


                    </div>
                    <div class="adm__cell adm__cell_type_p">
                        <div class="adm__cell-per adm__cell-per_type_p">15%</div>
                        <div class="adm__cel-numbers">1002</div>
                    </div>
                    <div class="adm__cell adm__cell_type_g">
                        <div id="graphDesktop" class="graph">

                        </div>
                    </div>
                </div>

                <div class="adm__block">
                    <div class="adm__cell adm__cell_type_sm adm__cell_bg_lm">
                        <span class="adm__type-heading"><i>Twitter</i></span>

                    </div>
                    <div class="adm__cell adm__cell_type_p">
                        <div class="adm__cell-per adm__cell-per_type_m">15%</div>
                        <div class="adm__cel-numbers">1002</div>
                    </div>
                    <div class="adm__cell adm__cell_type_g">
                        <div id="graphMobile" class="graph">

                        </div>
                    </div>

                </div>

                <div class="adm__block">
                    <div class="adm__cell adm__cell_type_sm adm__cell_bg_gr">
                        <span class="adm__type-heading"><i>Twitter</i> <i>Twitter</i></span>


                    </div>
                    <div class="adm__cell adm__cell_type_p">
                        <div class="adm__cell-per adm__cell-per_type_p">15%</div>
                        <div class="adm__cel-numbers">1002</div>
                    </div>
                    <div class="adm__cell adm__cell_type_g">
                        <div id="" class="graph">

                        </div>
                    </div>

                </div>

                <div class="adm__block">
                    <div class="adm__cell adm__cell_type_sm adm__cell_bg_rd">
                        <span class="adm__type-heading"><i>Twitter</i> <i>Twitter</i></span>


                    </div>
                    <div class="adm__cell adm__cell_type_p">
                        <div class="adm__cell-per adm__cell-per_type_m">15%</div>
                        <div class="adm__cel-numbers">1002</div>
                    </div>
                    <div class="adm__cell adm__cell_type_g">
                        <div id="" class="graph">

                        </div>
                    </div>

                </div>
            </div>



        </div>
        <div class="an__footer">
            <div class="an__icons">
                <div class="an__icon an__icon_type_print"></div>
                <div class="an__icon an__icon_type_download"></div>
                <div class="an__icon an__icon_type_mail"></div>
                <div class="an__icon an__icon_type_blank"></div>
            </div>
        </div>

        <div class="an__total">
            total: <span>5000</span>
        </div>




    </section>

    <div class="bottom">
        <div class="bottom__logo">
            <img src="<%= request.getContextPath()%>/images/helmet__logo.jpg" alt=""/>
        </div>
    </div>

</div>

<footer class="footer">
    <span>Copyright © 2014 Fotaflo. All rights reserved.</span>
</footer>

<!--MODAL WINDOWS-->

<jsp:include page="forms/contactus.jsp" />


<jsp:include page="layout/scripts.jsp" />

<script>

//    calendar
    (function($){

    })(jQuery)


var chart;

var chartData = [
    {
        "year":  "01/03/2014",
        "Participants": 100,
        "Emails": 50,
        "Codes": 200,
        "Address": 250
    },
    {
        "year": "01/04/2014",
        "Participants": 150,
        "Emails": 80,
        "Codes": 180,
        "Address": 240
    },
    {
        "year": "01/05/2014",
        "Participants": 200,
        "Emails": 180,
        "Codes": 10,
        "Address": 40
    },
    {
        "year": "01/06/2014",
        "Participants": 850,
        "Emails": 500,
        "Codes": 900,
        "Address": 540
    },
    {
        "year": "01/07/2014",
        "Participants": 650,
        "Emails": 380,
        "Codes": 480,
        "Address": 440
    }
];


AmCharts.ready(function () {
    // SERIAL CHART
    chart = new AmCharts.AmSerialChart();
    chart.dataProvider = chartData;
    chart.categoryField = "year";
    chart.startDuration = 0;
    chart.dataDateFormat = "DD/MM/YYYY";
    chart.balloon.color = "#000000";

    // AXES
    // category
    var categoryAxis = chart.categoryAxis;
    categoryAxis.fillAlpha = 0;
    categoryAxis.fillColor = "#FAFAFA";
    categoryAxis.gridAlpha = 0;
    categoryAxis.axisAlpha = 0;
    categoryAxis.gridPosition = "start";
    categoryAxis.position = "left";

    // value
    var valueAxis = new AmCharts.ValueAxis();
    valueAxis.dashLength = 0;
    valueAxis.axisAlpha = 1;

    valueAxis.integersOnly = true;
    valueAxis.gridCount = 0;
    chart.addValueAxis(valueAxis);


    var graph = new AmCharts.AmGraph();
    graph.title = "Participants";
    graph.valueField = "Participants";
    graph.balloonText = "[[category]]: <br /> [[value]]";
    graph.bullet = "round";
    chart.addGraph(graph);


    var graph = new AmCharts.AmGraph();
    graph.title = "Emails";
    graph.valueField = "Emails";
    graph.labelPosition = "left";
    graph.balloonText = "[[category]]: <br /> [[value]]";
    graph.bullet = "round";
    chart.addGraph(graph);

    var graph = new AmCharts.AmGraph();
    graph.title = "Codes";
    graph.valueField = "Codes";
    graph.labelPosition = "left";
    graph.balloonText = "[[category]]: <br /> [[value]]";
    graph.bullet = "round";
    chart.addGraph(graph);

    var graph = new AmCharts.AmGraph();
    graph.title = "Address";
    graph.valueField = "Address";
    graph.labelPosition = "left";
    graph.balloonText = "[[category]]: <br /> [[value]]";
    graph.bullet = "round";
    chart.addGraph(graph);


   /* chart.balloon.adjustBorderColor = true;
    chart.balloon.color = "#ffffff";
    chart.balloon.borderThickness = 0;
    chart.balloon.cornerRadius = 0;
    chart.balloon.borderColor = "#ff8600";
    chart.balloon.fillColor = "#ff8600";
    chart.balloon.pointerWidth = 8;
    chart.balloon.horizontalPadding = 10;
    chart.balloon.varticalPadding = 10;
    chart.balloon.textAlign = "middle";*/

    // WRITE
    chart.write("loyGraph");
});


var chartD = AmCharts.makeChart("graphDesktop", {
    "type": "serial",
    "theme": "light",
    "dataDateFormat": "DD/MM/YYYY",
    "pathToImages": "../amcharts/images/",
    "dataProvider": [{
        "date": "01/03/2014",
        "value": 0
    }, {
        "date": "01/04/2014",
        "value": 80
    }, {
        "date": "01/05/2014",
        "value": 70
    }, {
        "date": "01/06/2014",
        "value": 85
    }],
    "valueAxes": [{
        "maximum": 100,
        "minimum": 0,
        "axisAlpha": 0,
        "guides": [{
            "fillAlpha": 1,
            "fillColor": "#FFFFFF",
            "lineAlpha": 0,
            "toValue": 120,
            "value": 0,
            inside: true
        }]
    }],
    "graphs": [{
        "bullet": "round",
        "valueField": "value",
        balloonText: "<span><b>[[value]] Visitors </b> <br />[[date]]</span>"
    }],
    "chartCursor": {
        "cursorAlpha": 0
    },
    "categoryField": "date",
    "categoryAxis": {
        "parseDates": true
    }
});

</script>
</body>
</html>
