<%@ page session="true" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<!doctype html>
<!--[if lt IE 8]> <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9 ie8"> <![endif]-->
<!--[if IE 9]>    <html class="ie9""> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->

<head>
    <jsp:include page="layout/head.jsp" />

</head>
<body>

<div class="page">
    <header class="head head_page_inner">
        <div class="wrapper">
            <nav class="head__nav head__nav_page_inner">
                <ul class="head__list head__list_page_inner">
                    <li><a class="head__link" href="">Home</a></li>
                    <li><a class="head__link js-contactlink" href="">Contact us</a></li>
                    <li><a class="head__link" href="<%= request.getContextPath()%>/pictures/auth/logout">Sign out</a></li>
                </ul>
                <div class="head__nav-ico"></div>
            </nav>

        </div>
    </header>


    <menu class="menu js-sticky">
        <div class="menu__wrapper">
            <ul class="menu__list menu__list_position_left">
                <li><a href="common" class="js-manage-socialnet">Manage Social Network</a></li>
                <li><a class="menu__item js-manage-promotions" href="promotions">Manage Promotions</a></li>
                <c:if test="${isadmin}">
                    <li><a class="menu__item js-manage-paypal" href="paypal">Manage PayPal Settings</a></li>
                </c:if>
            </ul>

            <ul class="menu__list menu__list_position_center">
            </ul>
        </div>
    </menu>

    <section class="manage">
        <h2 class="manage__title">Manage PayPal Settings</h2>
        <div class="manage__form_paypal">
            <form class="mform" action="">
                <div class="mform__row">
                    <label class="mform__label">Account</label>
                    <input class="mform__input" type="text" name="account" value="<c:out value="${account}"/>" />
                </div>
                <div class="mform__row">
                    <label class="mform__label">Password</label>
                    <input class="mform__input" type="password" name="password" value="<c:out value="${password}"/>"/>
                </div>
                <div class="mform__row">
                    <label class="mform__label">Signature</label>
                    <input class="mform__input" type="password" name="signature" value="<c:out value="${signature}"/>"/>
                </div>
                <div class="mform__row">
                    <label class="mform__label">Environment</label>
                    <input class="mform__input" type="text" name="environment" value="<c:out value="${environment}"/>"/>
                </div>
                <div class="mform__row mform__row_align_center">
                    <input class="mform__submit" type="submit" value="Save"/>
                </div>
            </form>
        </div>
    </section>

    <div class="bottom">
        <div class="bottom__logo">
            <img src="<%= request.getContextPath()%>/images/helmet__logo.jpg" alt=""/>
        </div>
    </div>

</div>

<footer class="footer">
    <span>Copyright � 2014 Fotaflo. All rights reserved.</span>
</footer>

<!--MODAL WINDOWS-->


<jsp:include page="forms/contactus.jsp" />


<script type="text/javascript" src="<%= request.getContextPath()%>/js/jquery.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.form.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.cookie.js"></script>
<script type="text/javascript" src="<%= request.getContextPath()%>/js/caroufredsell.js"></script>

<script type="text/javascript" src="<%= request.getContextPath()%>/js/jquery.easyModal.js"></script>
<script type="text/javascript" src="<%= request.getContextPath()%>/js/jquery.lazyload.js"></script>


<script type="text/javascript" src="<%= request.getContextPath()%>/js/main.js"></script>
<script>

</script>
</body>
</html>
