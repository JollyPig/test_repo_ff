<%--
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Social Network Login Status Detector Demo</title>

    <meta http-equiv="refresh" content="5; url=<%=request.getContextPath()%>/" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type='text/javascript' src='http://platform.twitter.com/widgets.js?ver=1.1'></script>

    <script type="text/javascript">

        function show_login_status(network, status)
        {
            if (status)
            {
                $("#" + network + "Status").html("You are currently logged in to <span class='green' style='color: green;'>" + network + "</span>");
            }else{
                $("#" + network + "Status").html("You are not currently logged in to <span class='red' style='color: red;'>" + network + "</span>");
            }
        }
    </script>


</head>
<body>


<div class="container">
    <section>
        <div class="row">
            <div class="span16">
                <div class="page-header">
                    <h1>Social Network Login Status Detector Demo</h1>
                </div>

                <p>(Firefox, Chrome, IE 7+, Safari and Opera).</p>

                <div id="FacebookStatus" class="outputStatus"></div>
                <div id="TwitterStatus" class="outputStatus"></div>
                <div id="TumblrStatus" class="outputStatus"></div>

            </div>
        </div>
    </section>
</div>

<img style="display:none;" src="https://twitter.com/login?redirect_after_login=%2Fimages%2Fspinner.gif" onload="show_login_status('Twitter', true)" onerror="show_login_status('Twitter', false)" />

<img style="display:none;" src="https://www.tumblr.com/login?redirect_to=%2Fimages%2Ffollower_wall%2Fspinner.png" onload="show_login_status('Tumblr', true)" onerror="show_login_status('Tumblr', false)" />

<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function(){
        FB.init({ appId:'1388333571439399', status:true,  cookie:true, xfbml:true});
        FB.getLoginStatus(function(response){
            if (response.status != "unknown")
            {
                show_login_status("Facebook", true);
            }else{
                show_login_status("Facebook", false);
            }
        });
    };
    // Load the SDK Asynchronously
    (function(d){
        var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
        js = d.createElement('script'); js.id = id; js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        d.getElementsByTagName('head')[0].appendChild(js);
    }(document));
</script>



</body>
</html>