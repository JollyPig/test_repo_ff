<%@ page session="true" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!doctype html>
<!--[if lt IE 8]> <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9 ie8"> <![endif]-->
<!--[if IE 9]>    <html class="ie9""> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->

<head>
    <jsp:include page="layout/head.jsp" />

</head>
<body>

<div class="page">
    <header class="head">
        <div class="wrapper">
            <div class="head__logo">
                <a href="">
                    <img src="<%= request.getContextPath()%>/images/logo.png" alt="FotalFlo"/>
                </a>
            </div>
            <nav class="head__nav head__nav_page_index">
                <ul class="head__list">
                    <li><a id="open-loginForm" class="head__link  <c:out value="${preview?'':'js-loginlink'}"/>" href="#">Log in</a></li>
                    <li><a  class="head__link <c:out value="${preview?'':'js-contactlink'}"/>" href="#">Contact us</a></li>
                </ul>
            </nav>



        </div>
    </header>

    <section class="code">
        <h2 class="code__title">
            Just enter your code. That&apos;s all.
        </h2>

        <form class="code__form" action="../../j_spring_security_check" method="POST" onsubmit="copyLogin()">
            <div style="text-align: center;">
                <div id="login-error" class="share__alert">${error}</div>
            </div>
            <div class="code__holder">
                <label class="code__label">Code</label>
                <input id="j_username" name="j_username" class="code__text" type="text" <c:out value="${preview?'readonly':''}"/>/>
				<input type="hidden" id="j_password" name="j_password" class="code__text" type="text"/>
                <input type="hidden" name="type" type="text" value="code"/>

            </div>
            <input class="code__submit" type="submit" value="Ok" <c:out value="${preview?'disabled':''}"/>/>
            <div id="loadmask" style="padding: 10px 50px; display: none;">
                <img src="<%=request.getContextPath()%>/images/ajax-loader.gif" /> Loading, please wait
            </div>
        </form>

    </section>


        <c:forEach var="i" begin="0" end="4">
            <c:set var="promotion" scope="session" value="${promotions.get(i)}"/>
            <c:set var="textPromotion" value="${textPromotions.get(i)}"/>
            <c:if test="${i==0}">
        <div id="layerslider" >
            </c:if>
            <c:if test="${(i >= 0 && i <= 2)}">
                <div class="slider__item ls-layer" >
                    <img class="ls-bg" src="<%=request.getContextPath()%><c:out value="${promotion.path}"/>" alt="layer1-sublayer1">
                    <h5 class="ls-s1 text slider__text slider__text_pos_<c:out value="${promotion.position}"/>" style="
                      slidedirection : fade;
                      slideoutdirection : fade;
                      durationout : 750;
                      easingin : easeOutQuint;
                      delayin : 300;
                      scalein : .8;
                      scaleout : .8;
                      color: <c:out value="${promotion.color}"/>;
                      text-shadow: 0px 0px 10px <c:out value="${promotion.shadowColor}"/>;"><c:out value="${promotion.text}" escapeXml="false"/></h5>
                </div>
            </c:if>

            <c:if test="${i==2}">
        </div>
            </c:if>

            <c:if test="${i>=3}">
                <section class="description">
                    <h2 class="description__title">
                        <c:out value="${textPromotion.caption}"/>
                    </h2>
                    <div class="description__text">
                        <c:out value="${textPromotion.text}" escapeXml="false"/>
                    </div>
                </section>
                <section class="promo <c:out value="${(i == 3)? 'promo_position_first' : 'promo_position_second'}"/>"
                         style="background-image: url(<%=request.getContextPath()%><c:out value="${promotion.path}"/>)">
                    <h3 class="promo__title promo__title_pos_<c:out value="${promotion.position}"/>"
                        style="color: <c:out value="${promotion.color}"/>; text-shadow: 0px 0px 10px <c:out value="${promotion.shadowColor}"/>">
                        <c:out value="${promotion.text}" escapeXml="false"/>
                    </h3>
                </section>
            </c:if>

        </c:forEach>

    <section class="helmet">
        <img class="helmet__image" src="<%=request.getContextPath()%>/images/helmet.jpg" alt=""/>
        <div class="helmet__logo">
            <img src="<%=request.getContextPath()%>/images/helmet__logo.jpg" alt=""/>
        </div>
    </section>


</div>

<footer class="footer">
    <span>Copyright � 2014 Fotaflo. All rights reserved.</span>
</footer>

<!--MODAL WINDOWS-->

<div class="login js-modal-window" id="loginForm">
    <div class="login__wrapper">
        <div class="modal-close"></div>
        <div class="login__logo">
            <img src="<%=request.getContextPath()%>/images/modal__logo.png" alt=""/>
        </div>
        <form class="login__form" action="<%=request.getContextPath()%>/j_spring_security_check" method="POST">
            <input class="login__text" name="j_username" type="text" placeholder="Username" />
            <input class="login__text" name="j_password" type="password" placeholder="Password" />
            <input class="login__submit" type="submit" value="Log in"/>
            <div class="share__alert" style="display: none;"></div>
        </form>
    </div>
</div>



<jsp:include page="forms/contactus.jsp" />


<script type="text/javascript">

    function copyLogin(){
        document.getElementById('j_password').value = document.getElementById('j_username').value;
        }

</script>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.form.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.cookie.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-easing-1.3.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-transit-modified.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/detectmobilebrowsers.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/layerslider.transitions.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/layerslider.kreaturamedia.jquery.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.loadmask.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.easyModal.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/main.js"></script>


<script type="text/javascript">

    $(function(){

        $('#loginForm,.code__form').ajaxForm({
            data: {
                ajax: true,
                mobile: isMobile()
            },
            beforeSubmit: function(data, $form){
                $form.find('.share__alert').hide();
                $form.find('#loadmask').show();
                $form.find('input').prop('disabled', true);
            },
            success: function(responseText, status, xhr, $form){
                var response = JSON.parse(responseText);
                if(response.success === true){
                    location.href = response.redirectUrl;
                }else{
                    $form.find('#loadmask').hide();
                    $form.find('input').prop('disabled', false);
                    $form.find('.share__alert').html(response.errorMessage).show();
                }
            },
            error: function(responseText, status, xhr, $form){
                $form.find('#loadmask').hide();
                $form.find('input').prop('disabled', false);
                $form.find('.share__alert').html('There is a problem to sending request. Check your connection.').show();
            }
        });
    })

</script>

</body>
</html>
