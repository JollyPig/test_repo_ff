<!doctype html>
<!--[if lt IE 8]> <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9 ie8"> <![endif]-->
<!--[if IE 9]>    <html class="ie9""> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->

<head>
    <jsp:include page="layout/head.jsp" />

    <link rel="stylesheet" href="<%= request.getContextPath()%>/css/graps.css">

</head>
<body>

<div class="page">
    <header class="head head_page_inner">
        <div class="wrapper">
            <jsp:include page="layout/headNavigation.jsp" />

        </div>
    </header>



    <section class="an">
        <jsp:include page="layout/analyticsNavigation.jsp" />
        
        <div class="tabs">
          <div class="tabs__top">
              <div class="tabs__nav">
                          <div class="tabs__nav-cell">
                              <select class="tabs__select" name="">
                                  <option value="0">Twitter</option>
                                  <option value="1">Facebook</option>
                                  <option value="2">Vtentakle</option>
                              </select>

                              <input id="js-calendar" class="tabs__input" type="text" value="03/03/2014 - 03/31/2014"/>
                          </div>
                          <div class="tabs__nav-cell"></div>
                      </div>

          </div>
            <div class="loc">
                <div id="locGraph">

                </div>
                
                <table class="loc__table">

                    <tr>
                        <td class="loc__th"><div class="loc__heading"><span></span>City</div></td>
                        <td class="loc__th"><div class="loc__heading"><span></span>Visits</div></td>
                        <td class="loc__th"><div class="loc__heading"><span></span>Percent</div></td>
                        <td class="loc__th"><div class="loc__heading"><span></span>Percent</div></td>
                        <td class="loc__th"><div class="loc__heading"><span></span>Percent</div></td>
                        <td class="loc__th"><div class="loc__heading"><span></span>Percent</div></td>
                        <td class="loc__th"><div class="loc__heading"><span></span>Percent</div></td>
                        <td class="loc__th"><div class="loc__heading"><span></span>Percent</div></td>
                        <td class="loc__th"><div class="loc__heading"><span></span>Percent</div></td>
                        <td class="loc__th"><div class="loc__heading"><span></span>Percent</div></td>
                    </tr>

                    

                    <tr>
                        <td><div class="loc__pad loc__pad_font_small">Toronto</div></td>
                        <td><div class="loc__pad">1002</div></td>
                        <td><div class="loc__pad">70%</div></td>
                        <td><div class="loc__pad">70%</div></td>
                        <td><div class="loc__pad">70%</div></td>
                        <td><div class="loc__pad">70%</div></td>
                        <td><div class="loc__pad">70%</div></td>
                        <td><div class="loc__pad">70%</div></td>
                        <td><div class="loc__pad">70%</div></td>
                        <td><div class="loc__pad">70%</div></td>
                    </tr>
                    <tr>
                        <td><div class="loc__pad loc__pad_font_small">Toronto</div></td>
                        <td><div class="loc__pad">1002</div></td>
                        <td><div class="loc__pad">70%</div></td>
                        <td><div class="loc__pad">70%</div></td>
                        <td><div class="loc__pad">70%</div></td>
                        <td><div class="loc__pad">70%</div></td>
                        <td><div class="loc__pad">70%</div></td>
                        <td><div class="loc__pad">70%</div></td>
                        <td><div class="loc__pad">70%</div></td>
                        <td><div class="loc__pad">70%</div></td>
                    </tr>
                    <tr>
                        <td><div class="loc__pad loc__pad_font_small">Toronto</div></td>
                        <td><div class="loc__pad">1002</div></td>
                        <td><div class="loc__pad">70%</div></td>
                        <td><div class="loc__pad">70%</div></td>
                        <td><div class="loc__pad">70%</div></td>
                        <td><div class="loc__pad">70%</div></td>
                        <td><div class="loc__pad">70%</div></td>
                        <td><div class="loc__pad">70%</div></td>
                        <td><div class="loc__pad">70%</div></td>
                        <td><div class="loc__pad">70%</div></td>
                    </tr>
                    <tr class="loc__last">
                        <td><div class="loc__pad loc__pad_font_small">Toronto</div></td>
                        <td><div class="loc__pad">1002</div></td>
                        <td><div class="loc__pad">70%</div></td>
                        <td><div class="loc__pad">70%</div></td>
                        <td><div class="loc__pad">70%</div></td>
                        <td><div class="loc__pad">70%</div></td>
                        <td><div class="loc__pad">70%</div></td>
                        <td><div class="loc__pad">70%</div></td>
                        <td><div class="loc__pad">70%</div></td>
                        <td><div class="loc__pad">70%</div></td>
                    </tr>


                </table>

            </div>

        </div>
        <div class="an__footer">
        <div class="an__icons">
            <div class="an__icon an__icon_type_print"></div>
            <div class="an__icon an__icon_type_download"></div>
            <div class="an__icon an__icon_type_mail"></div>
            <div class="an__icon an__icon_type_blank"></div>
        </div>
        </div>

        <div class="an__total">
            total: <span>5000</span>
        </div>
    </section>

    <div class="bottom">
        <div class="bottom__logo">
            <img src="<%= request.getContextPath()%>/images/helmet__logo.jpg" alt=""/>
        </div>
    </div>

</div>

<footer class="footer">
    <span>Copyright © 2014 Fotaflo. All rights reserved.</span>
</footer>

<!--MODAL WINDOWS-->

<jsp:include page="forms/contactus.jsp" />

<jsp:include page="layout/scripts.jsp" />

<script>

//    calendar
    (function($){
        var initLayout = function() {

            $('#js-calendar').DatePicker({
                format:'m/d/Y',
                date: $('#js-calendar').val(),
                current: $('#js-calendar').val(),
                calendars: 2,
                mode: 'range',
                position: 'bottom',
                starts: 1,

                onBeforeShow: function(){
                    $('#js-calendar').DatePickerSetDate($('#js-calendar').val(), true);
                },
                onChange: function(formated, dates){
                    $('#js-calendar').val(formated);
                }

            });

        };

        EYE.register(initLayout, 'init');
    })(jQuery)

    var chart;

    var chartData = [
        {
            "country": "USA",
            "visits": 600,
            "color": "#FF0F00"
        },
        {
            "country": "China",
            "visits": 580,
            "color": "#FF6600"
        },
        {
            "country": "Japan",
            "visits": 560,
            "color": "#FF9E01"
        },
        {
            "country": "Germany",
            "visits": 500,
            "color": "#FCD202"
        },
        {
            "country": "UK",
            "visits": 475,
            "color": "#F8FF01"
        },
        {
            "country": "France",
            "visits": 450,
            "color": "#B0DE09"
        },
        {
            "country": "India",
            "visits": 440,
            "color": "#04D215"
        },
        {
            "country": "Spain",
            "visits": 400,
            "color": "#0D8ECF"
        }
    ];


    AmCharts.ready(function () {
        // SERIAL CHART
        chart = new AmCharts.AmSerialChart();
        chart.dataProvider = chartData;
        chart.categoryField = "country";
        chart.startDuration = 1;

        // AXES
        // category
        var categoryAxis = chart.categoryAxis;
        categoryAxis.labelRotation = 0;
        categoryAxis.labelColor = "#b0b0b0";
        categoryAxis.gridAlpha = 0;
        categoryAxis.fillAlpha = 0;
        categoryAxis.fontSize = 13;
        categoryAxis.fillColor = "#b0b0b0";
        categoryAxis.gridPosition = "start";

        // value
        var valueAxis = new AmCharts.ValueAxis();
        valueAxis.dashLength = 0;
        valueAxis.axisAlpha = 0;
        chart.addValueAxis(valueAxis);

        // GRAPH
        var graph = new AmCharts.AmGraph();
        graph.valueField = "visits";
        graph.colorField = "color";
        graph.balloonText = "<b>[[category]]: [[value]]</b>";
        graph.type = "column";
        graph.lineAlpha = 0;
        graph.fillAlphas = 1;
        chart.addGraph(graph);

        // CURSOR
        var chartCursor = new AmCharts.ChartCursor();
        chartCursor.cursorAlpha = 0;
        chartCursor.zoomable = false;
        chartCursor.categoryBalloonEnabled = false;
        chart.addChartCursor(chartCursor);

        chart.creditsPosition = "top-right";

        // WRITE
        chart.write("locGraph");
    });


$('td').hover(function() {
            var t = parseInt($(this).index()) + 1;
            $('td:nth-child(' + t + ')').addClass('hover');
        },
        function() {
            var t = parseInt($(this).index()) + 1;
            $('td:nth-child(' + t + ')').removeClass('hover');

        });


</script>
</body>
</html>
