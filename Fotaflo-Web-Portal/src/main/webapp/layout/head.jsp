<%--
  page head content
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<meta charset="UTF-8"/>

<meta name="viewport"  content="width=device-width, initial-scale=1.0"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<!--<meta http-equiv="Cache-Control" content="no-cache"/>-->

<!-- Blackberry and etc. -->
<meta http-equiv="cleartype" content="on"/>
<meta name="HandheldFriendly" content="True"/>

<!-- IE -->
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<!--[if IE]>
<meta http-equiv="imagetoolbar" content="no"/>
<meta http-equiv="MSThemeCompatible" content="no"/>
<![endif]-->

<!-- SEO -->
<title>Sharing Memories</title>
<meta name="description" content=""/>
<meta name="keywords" content=""/>
<meta name="author" content=""/>
<meta name="copyright" content=""/>
<meta http-equiv="Reply-to" content=""/>

<link href="" rel="shortcut icon" type="image/x-icon"/>

<link rel="stylesheet" href="<%= request.getContextPath()%>/layerslider/css/layerslider.css" type="text/css">
<link rel="stylesheet" href="<%= request.getContextPath()%>/css/main.css">
<link rel="stylesheet" href="<%= request.getContextPath()%>/css/jquery.loadmask.css">
<link rel="stylesheet" href="<%= request.getContextPath()%>/css/jquery.kyco.preloader.css">
<!--[if IE]><script src="<%= request.getContextPath()%>/js/html5.js"></script><![endif]-->