<%--
  Analytics Parameters
--%>
<%@ page session="true" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<div class="tabs__nav">
    <div class="tabs__nav-cell">
        <select id="js-location" class="tabs__select" name="location">
            <option value="null" selected>All Locations</option>
            <c:forEach var="loc" items="${locations}" >
                <option value="${loc.id}" <c:if test="${loc.id == sessionScope.location}" >selected</c:if> >${loc.name}</option>
            </c:forEach>
        </select>

        <input id="js-calendar" class="tabs__input" name="date" type="text" value="${sessionScope.date}"/>   <%--MM/DD/YYYY,MM/DD/YYYY--%>
    </div>
    <div class="tabs__nav-cell"></div>
</div>