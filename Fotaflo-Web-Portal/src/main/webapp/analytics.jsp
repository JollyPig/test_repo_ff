<%@ page session="true" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<!doctype html>
<!--[if lt IE 8]> <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9 ie8"> <![endif]-->
<!--[if IE 9]>    <html class="ie9""> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->

<head>
    <%--<% response.sendRedirect( request.getContextPath() + "/pictures/analytics/common" ); %>--%>
    <jsp:include page="layout/head.jsp" />

</head>
<body>

<div class="page">
    <header class="head head_page_inner">
        <div class="wrapper">
            <nav class="head__nav head__nav_page_inner">
                <ul class="head__list head__list_page_inner">
                    <li><a class="head__link" href="<%= request.getContextPath()%>/pictures/main/common">Home</a></li>
                    <li><a class="head__link js-contactlink" href="">Contact us</a></li>
                    <li><a class="head__link" href="<%= request.getContextPath()%>/pictures/auth/logout">Sign out</a></li>
                </ul>
                <div class="head__nav-ico"></div>
            </nav>

        </div>
    </header>



    <section class="an">
        <p style="margin: 20%;">Analytic functionality is under development</p>

    </section>

    <div class="bottom">
        <div class="bottom__logo">
            <img src="<%= request.getContextPath()%>/images/helmet__logo.jpg" alt=""/>
        </div>
    </div>

</div>

<footer class="footer">
    <span>Copyright � 2014 Fotaflo. All rights reserved.</span>
</footer>

<!--MODAL WINDOWS-->

<jsp:include page="forms/contactus.jsp" />

<jsp:include page="layout/scripts.jsp" />

</body>
</html>
