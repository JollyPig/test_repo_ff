package com.scnsoft.fotafloweb.service;

import com.scnsoft.fotafloweb.util.IOUtil;
import com.scnsoft.fotafloweb.service.util.ImageLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;

public class ImageLoaderTest extends AbstractServiceTest {

    @Autowired
    ImageLoader loader;

    @Test(enabled = false)
    public void getImageTest() throws Exception{
        URL path = new URL("http://199.58.116.35:8081/fotaflo/pictures/img_small/ACCT%20Conference/2014/04/02/Rappel/20140402_184736.jpg");

        File file = IOUtil.createFile("c:\\temp\\asdf.jpg");

        loader.loadImage(path, new FileOutputStream(file));
    }

}
