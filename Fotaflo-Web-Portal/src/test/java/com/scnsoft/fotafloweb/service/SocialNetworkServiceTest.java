package com.scnsoft.fotafloweb.service;

import com.scnsoft.fotafloweb.dto.SocialNetworkDto;
import com.scnsoft.fotafloweb.model.SocialNetwork;
import com.scnsoft.fotafloweb.model.SocialNetworkType;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;

public class SocialNetworkServiceTest extends AbstractServiceTest {

    @Autowired
    ISocialNetworkService socialNetworkService;

    @DataProvider
    public Object[][] getLocalData() {
        return new Object[][]{
                {new SocialNetwork(SocialNetworkType.facebook.name(), 1, "title 1", "description 1", "address 1", "tag1,tag2,tag3", "hashtag1,hashtag2"), "facebook"},
                {new SocialNetwork(SocialNetworkType.twitter.name(), 1, "title 2", "description 2", "address 2", "tag1,tag2", "hashtag1"), "twitter"},
                {new SocialNetwork(SocialNetworkType.tumblr.name(), 1, "title 3", "description 3", "address 3", "tag1,tag3", "hashtag2"), "tumblr"},
        };
    }

    @Test
    public void names(){
        List<SocialNetworkType> list = socialNetworkService.getSocialNetworkNames();
        logger.info(list);
        Assert.assertNotNull(list);
        Assert.assertEquals(list.size(), SocialNetworkType.values().length);
    }

    @Test(priority = 0, dataProvider = "getLocalData", enabled = false)
    public void create(SocialNetwork sn, String name){
        logger.info(sn);
        socialNetworkService.updateSocialNetwork(new SocialNetworkDto(sn));
    }

    @Test(priority = 1, enabled = false)
    public void map(){
        Map<String, SocialNetworkDto> map = socialNetworkService.getSocialNetworkMap();
        logger.info(map);
        Assert.assertNotNull(map);
        Assert.assertEquals(map.size(), SocialNetworkType.values().length);
    }
}
