package com.scnsoft.fotafloweb.service;

import com.scnsoft.fotafloweb.dto.PromotionDto;
import com.scnsoft.fotafloweb.model.Promotion;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

public class PromotionServiceTest extends AbstractServiceTest {

    @Autowired
    private IPromotionService promotionService;

    @DataProvider
    public Object[][] getLocalData() {
        return new Object[][]{
                {new Promotion(1, "promotion 1", null, "345L", "445L", "path/to/image"), 1},
                {new Promotion(2, "promotion 2", null, "345L", "445L", "path/to/image"), 2},
                {new Promotion(3, "promotion 3", null, "345L", "445L", "path/to/image"), 3},
                {new Promotion(4, "promotion 4", null, "345L", "445L", "path/to/image"), 4},
                {new Promotion(5, "promotion 5", null, "345L", "445L", "path/to/image"), 5},
        };
    }

    @Test(enabled = false, priority = 0, dataProvider = "getLocalData")
    public void create(Promotion p, Integer eid){
        PromotionDto dto = new PromotionDto(p);
        Integer id = promotionService.createPromotion(dto);
        logger.info("Created promotion "+dto+" with id "+id);
        Assert.assertNotNull(id);
        Assert.assertEquals(id, eid);
    }

    @Test(enabled = false, priority = 2)
    public void update(){
        Assert.assertTrue(true);
    }

    @Test(enabled = false, priority = 1)
    public void get(){
        Promotion p = (Promotion)getLocalData()[0][0];
        Integer id = (Integer)getLocalData()[0][1];
        PromotionDto dto = promotionService.getPromotion(id);
        logger.info("Get promotion "+dto+" with id "+id);
        Assert.assertEquals(p.getColor(), dto.getColor());
        Assert.assertEquals(p.getShadowColor(), dto.getShadowColor());
        Assert.assertEquals(p.getPath(), dto.getPath());
        Assert.assertEquals(p.getText(), dto.getText());
    }

    @Test(enabled = false, priority = 1)
    public void list(){
        int len = getLocalData().length;
        logger.info("Array length: " + len);
        List<PromotionDto> list = promotionService.getPromotions();
        Assert.assertNotNull(list);
        logger.info("Promotions count: " + list.size());
        logger.info("Promotions: " + list);
        Assert.assertEquals(len, list.size());
        logger.info("Promotions: " + list);
    }

    @Test(enabled = false, priority = 3)
    public void delete(){
        Assert.assertTrue(true);
    }

}
