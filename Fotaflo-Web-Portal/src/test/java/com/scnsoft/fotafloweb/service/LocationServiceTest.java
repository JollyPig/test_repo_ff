package com.scnsoft.fotafloweb.service;

import com.scnsoft.fotafloweb.bean.LocationBean;
import junit.framework.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class LocationServiceTest extends AbstractServiceTest {

    @Autowired
    ILocationService service;

    List<LocationBean> list = new ArrayList<LocationBean>();

    @Test(enabled = false)
    public void list(){
        List<LocationBean> list = service.getLocations();
        Assert.assertNotNull(list);
        logger.info("Clients list size: "+list.size());
        Assert.assertTrue(list.size() > 0);
        this.list = list;
    }

    @Test(enabled = false, dependsOnMethods = "list")
    public void get(){
        LocationBean rc;
        for(LocationBean c: this.list){
            rc = service.getLocation(c.getId());
            Assert.assertNotNull(rc);
            logger.info("Client: "+ rc);
        }
    }

}
