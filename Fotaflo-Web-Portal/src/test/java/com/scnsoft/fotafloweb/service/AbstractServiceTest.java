package com.scnsoft.fotafloweb.service;

import com.scnsoft.fotafloweb.AbstractTest;
import org.apache.log4j.Logger;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.testng.annotations.BeforeTest;

@TransactionConfiguration(defaultRollback = true)
@Transactional
public abstract class AbstractServiceTest extends AbstractTest {
}
