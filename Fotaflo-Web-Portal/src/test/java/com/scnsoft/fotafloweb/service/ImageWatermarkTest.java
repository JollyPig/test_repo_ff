package com.scnsoft.fotafloweb.service;

import com.scnsoft.fotafloweb.service.util.ImageWatermark;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.testng.annotations.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Date;

public class ImageWatermarkTest extends AbstractServiceTest {

    @Autowired
    private ImageWatermark imageWatermark;

    @Autowired
    IPictureService pictureService;

    @Test
    public void watermarkFullsizeImageTest() throws Exception{
        long st = new Date().getTime();
        Date sd = new Date();
        Resource resource2 = new ClassPathResource("images/IMG_20140515_124848.jpg");
        Resource resource3 = new ClassPathResource("images/Untitled.jpg");
        Resource resource4 = new ClassPathResource("images/Untitled_1.jpg");

        log("Before read fullsize image", sd);
        BufferedImage image = ImageIO.read(resource2.getFile());
        BufferedImage logo1 = ImageIO.read(resource3.getFile());
        BufferedImage logo2 = ImageIO.read(resource4.getFile());
        log("After read fullsize image", sd);

        BufferedImage bi = imageWatermark.addLogosToImage(image, logo1, logo2, "simple text");
        log("After mark fullsize image", sd);

        ImageIO.write(bi, "JPEG", new File("c:/temp/1.jpg"));
        log("After write fullsize image", sd);
    }

    @Test
    public void watermarkThumbImageTest() throws Exception{
        long st = new Date().getTime();
        Date sd = new Date();
        Resource resource2 = new ClassPathResource("images/IMG_20140626_130727.jpg");
        Resource resource3 = new ClassPathResource("images/Untitled.jpg");
        Resource resource4 = new ClassPathResource("images/Untitled_1.jpg");

        log("Before read thumb", sd);
        BufferedImage image = ImageIO.read(resource2.getFile());
        BufferedImage logo1 = ImageIO.read(resource3.getFile());
        BufferedImage logo2 = ImageIO.read(resource4.getFile());
        log("After read thumb", sd);

        BufferedImage bi = imageWatermark.addLogosToImage(image, logo1, logo2, "simple text");
        log("After mark thumb", sd);

        ImageIO.write(bi, "JPEG", new File("c:/temp/1.jpg"));
        log("After write thumb", sd);
    }

    private void log(String message, Date date){
        logger.info(message + ", " + getDateDiff(date) + "ms");
    }

    private long getDateDiff(Date date){
        return (new Date().getTime() - date.getTime());
    }
}
