package com.scnsoft.fotafloweb.service;

import com.scnsoft.fotafloweb.bean.PictureBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.channels.Channels;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class DownloadServiceTest extends AbstractServiceTest {

    @Autowired
    private IDownloadService service;

    @Autowired
    IPictureService pictureService;

    @Test(enabled = false)
    public void downloadPictures() throws Exception{
        String user = "Aoi4t5sa";
        List<PictureBean> list = /*pictureService.getPictures(user);*/new ArrayList<PictureBean>();
        PictureBean bean = new PictureBean(430, "pic1","http://23.21.116.219:8080/fotaflo/pictures/img/ACCT%20Conference/2014/04/02/Rappel/20140402_184736.jpg",null,null,null,new Date(),false, null, null, null);
        list.add(bean);

        logger.info(list);

        List<Integer> ids = new ArrayList<Integer>();
        /*for(PictureBean p: list){
            ids.add(p.getId());
        }*/
        /*ids.add(429);
        ids.add(430);*/

        logger.info(ids);

        byte[] a = service.createZipArchive(list, "fotaflo");
        Assert.assertNotNull(a);
        logger.info(a.length);

        ByteArrayInputStream bais = new ByteArrayInputStream(a);
        FileOutputStream fos = new FileOutputStream(new File("c://uploaddir//fotaflo.zip"));
        fos.getChannel().transferFrom(Channels.newChannel(bais), 0, Long.MAX_VALUE);
    }

}
