package com.scnsoft.fotafloweb.dao;

import com.scnsoft.fotafloweb.model.Picture;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.List;

public class PictureDaoTest  extends AbstractDaoTest<Picture> {

    @Autowired
    IPicturesDao picturesDao;

    @Override
    protected Object[][] getTestData() {
        return new Object[][]{
                {getEntity(false)},
                {getEntity(false)},
                {getEntity(true)},
                {getEntity(false)},
                {getEntity(true)}
        };
    }

    private Picture getEntity(boolean lp){
        Picture entity = new Picture();
        String code = generateCode(8);
        entity.setName("name$" + code);
        entity.setUsername("username$" + code);
        entity.setUrl(code);
        entity.setServerId(0);
        if(lp){
            entity.setLocationPictureId(new SecureRandom().nextInt());
        }else{
            entity.setPictureId(new SecureRandom().nextInt());
        }
        logger.info("Picture: "+entity.getName()+", "+entity.getUrl()+", "+entity.getUsername());
        return entity;
    }

    public String generateCode(int length) {
        String seq = new BigInteger(60, new SecureRandom()).toString(32);
        return seq.substring(0,length);
    }

    @Override
    protected IPicturesDao getDao() {
        return picturesDao;
    }

    @Override
    protected void modifyEntity(Picture entity) {
        entity.setUrl(entity.getUrl()+" modifyed");
    }

    @Override
    protected boolean isEntitiesEqual(Picture entity1, Picture entity2) {
        return (entity1.getName() != null && entity1.getName().equals(entity2.getName()))
                && (entity1.getUrl() != null && entity1.getUrl().equals(entity2.getUrl()));
    }

    @Test(priority = 1, enabled = true)
    public void getUserPictures(){
        List<Picture> list;
        for(Picture e: persistentData.keySet()){
            logger.info("Get by username - "+e.getUsername());
            list = getDao().getUserPictures(e.getUsername());
            logger.info(list);
            Assert.assertNotNull(list);
        }
    }

    @Test(priority = 1, enabled = true)
    public void getPictureByExternalId(){
        Picture result;
        for(Picture e: persistentData.keySet()){
            if(e.getPictureId() != null){
                logger.info("Get pic by external id - "+e.getPictureId());
                result = getDao().getPictureByExternalId(e.getPictureId(), e.getServerId());
            }else{
                logger.info("Get location pic by external id - "+e.getLocationPictureId());
                result = getDao().getLocationPictureByExternalId(e.getLocationPictureId(), e.getServerId());
            }
            logger.info(result);
            Assert.assertNotNull(result);
        }
    }

    @Test(priority = 1, enabled = true)
    public void getPictureByUrl(){
        Picture result;
        for(Picture e: persistentData.keySet()){
            logger.info("Get by url - "+e.getUrl());
            result = getDao().getPictureByUrl(e.getUrl());
            logger.info(result);
            Assert.assertNotNull(result);
        }
    }

}
