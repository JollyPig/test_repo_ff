package com.scnsoft.fotafloweb.dao;

import com.scnsoft.fotafloweb.AbstractTest;
import com.scnsoft.fotafloweb.model.IEntity;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@TransactionConfiguration(defaultRollback = false)
@Transactional
abstract public class AbstractDaoTest<Entity extends IEntity> extends AbstractTest {

    Map<Entity, Integer> persistentData = new HashMap<Entity, Integer>();

    @DataProvider
    public Object[][] getData() {
        return getTestData();
    }

    abstract protected Object[][] getTestData();

    abstract protected IDao<Entity, Integer> getDao();

    abstract protected void modifyEntity(Entity entity);

    abstract protected boolean isEntitiesEqual(Entity entity1, Entity entity2);

    @Test(priority = 0, dataProvider = "getData", enabled = true)
    public void create(Entity entity){
        logger.info("Save: " + entity);
        Integer id = getDao().create(entity);
        Assert.assertNotNull(id);
        logger.info("Saved: " + entity.getId());
        persistentData.put(entity, id);
    }

    @Test(priority = 1, enabled = true)
    public void get(){
        Object[][] data = getTestData();
        logger.info("Data: "+data);
        List<Entity> list = getDao().list();
        logger.info("Got: "+list);
        Assert.assertNotNull(list);
        Assert.assertTrue(list.size() > 0);

        Entity entity;
        for (Entity e: persistentData.keySet()){
            entity = getDao().get(persistentData.get(e));
            Assert.assertNotNull(entity);
            Assert.assertTrue(isEntitiesEqual(entity, e));
            logger.info("Got: " + entity);
        }
    }

    @Test(priority = 2, enabled = true)
    public void modify(){
        Entity e = persistentData.keySet().iterator().next();
        Integer id = persistentData.get(e);
        logger.info("Modify: " + e);
        Entity entity  = getDao().get(id);
        Assert.assertNotNull(entity);

        modifyEntity(entity);
        getDao().update(entity);

        entity = getDao().get(id);
        Assert.assertNotNull(entity);
        Assert.assertFalse(isEntitiesEqual(entity, e));

        getDao().delete(entity);

        List<Entity> list = getDao().list();
        for(Entity de: list){
            Assert.assertNotEquals(de.getId(), id);
        }

        id = list.get(0).getId();
        getDao().delete(id);
        list = getDao().list();
        for(Entity de: list){
            Assert.assertNotEquals(de.getId(), id);
        }
    }

    @Test(priority = 10, enabled = true)
    public void deleteAll(){
        List<Entity> list = getDao().list();
        logger.info("Got: "+list);
        getDao().deleteAll(list);

        list = getDao().list();
        logger.info("Got: "+list);
        Assert.assertNotNull(list);
        Assert.assertEquals(list.size(), 0);
    }

}
