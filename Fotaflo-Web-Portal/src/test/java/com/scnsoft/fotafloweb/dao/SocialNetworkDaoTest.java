package com.scnsoft.fotafloweb.dao;

import com.scnsoft.fotafloweb.model.SocialNetwork;
import com.scnsoft.fotafloweb.model.SocialNetworkType;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Random;

public class SocialNetworkDaoTest extends AbstractDaoTest<SocialNetwork> {

    @Autowired
    ISocialNetworkDao socialNetworkDao;

    @Override
    protected Object[][] getTestData() {
        return new Object[][]{
                {getEntity()},
                {getEntity()},
                {getEntity()},
                {getEntity()},
                {getEntity()}
        };
    }

    private SocialNetwork getEntity(){
        SocialNetwork entity = new SocialNetwork();
        entity.setName(SocialNetworkType.names().get(new Random().nextInt(3)));
        entity.setLocation(new Random().nextInt());
        entity.setDescription("settings by "+entity.getName()+" and "+entity.getLocation());
        return entity;
    }

    @Override
    protected ISocialNetworkDao getDao() {
        return socialNetworkDao;
    }

    @Override
    protected void modifyEntity(SocialNetwork entity) {
        entity.setDescription(entity.getDescription()+" modifyed");
    }

    @Override
    protected boolean isEntitiesEqual(SocialNetwork entity1, SocialNetwork entity2) {
        return (entity1.getName() != null && entity1.getName().equals(entity2.getName()))
                && (entity1.getLocation() != null && entity1.getLocation().equals(entity2.getLocation()))
                && (entity1.getDescription() != null && entity1.getDescription().equals(entity2.getDescription()));
    }

    @Test(priority = 1, enabled = true)
    public void getByNameAndLocation(){
        SocialNetwork entity;
        for(SocialNetwork e: persistentData.keySet()){
            logger.info("Get by name - "+e.getName()+" and location - "+e.getLocation());
            entity = getDao().getByNameAndLocation(e.getName(), e.getLocation());
            Assert.assertNotNull(entity);
            Assert.assertEquals(entity.getId(), (int)persistentData.get(e));
        }
    }

    @Test(priority = 1, enabled = true)
    public void findByLocation(){
        List<SocialNetwork> list;
        for(SocialNetwork e: persistentData.keySet()){
            logger.info("Get by location - "+e.getLocation());
            list = getDao().findByLocation(e.getLocation());
            Assert.assertNotNull(list);
        }
    }
}
