package com.scnsoft.fotafloweb.dao;

import com.scnsoft.fotafloweb.AbstractTest;
import com.scnsoft.fotafloweb.dao.remote.RemoteDao;
import com.scnsoft.fotafloweb.model.SystemUser;
import com.scnsoft.fotafloweb.service.IPictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@TransactionConfiguration(defaultRollback = false)
@Transactional
public class RemoteDaoTest extends AbstractTest {

    @Autowired
    RemoteDao pictureDao;

    @Autowired
    IPictureService pictureService;

    @DataProvider
    public Object[][] getData(){
        return new Object[][]{
                {getEntity(3258, "1f8vle2", 6, 205)},
                {getEntity(3258, "1f8vle2", 4, 205)},
                {getEntity(17802, "Tpk3tjl", 7, 168)},
                {getEntity(8334, "0E00334", 8, 168)},
                {getEntity(61360, "1A00020", 5, 252)}
        };
    }

    private SystemUser getEntity(Integer id, String login, Integer access, Integer locationId){
        SystemUser user = new SystemUser();
        user.setId(id);
        user.setLoginName(login);
        user.setAccess(access+"");
        user.setLocationId(locationId);
        user.setServerNumber(0);
        return user;
    }

    @Test(enabled = false, dataProvider = "getData")
    public void getPicturesByCode(SystemUser user){
        long start = new Date().getTime();
        List<Object[]> result = pictureDao.getPicturesByCode(user);
        long end = new Date().getTime() - start;
        logger.info("Result: "+result.size());
        logger.info("Time: "+end/1000+"s");
        String rs;
        Set<String> ids = new HashSet<String>();
        for(Object[] row: result){
            rs = "";
            if(ids.contains(row[0].toString())){
                logger.info("!!!!!ALERT!!!!! dublicate");
            }else{
                ids.add(row[0].toString());
            }
            for(Object d: row){
                rs += d + " ";
            }
            logger.info(rs);
        }
    }

    @Test(enabled = false)
    public void getLocations(){
        long start = new Date().getTime();
        SystemUser user = (SystemUser)getData()[1][0];
        List<Object[]> result = pictureDao.getLocations(user);
        long end = new Date().getTime() - start;
        logger.info("Result: "+result+", size: "+result.size());
        logger.info("Time: "+end/1000+"s");
        String rs;
        for(Object[] row: result){
            rs = "";
            for(Object d: row){
                rs += d + " ";
            }
            logger.info(rs);
        }
    }

}
