package com.scnsoft.fotafloweb.dao;

import com.scnsoft.fotafloweb.model.SystemUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.List;

@TransactionConfiguration(defaultRollback = false)
@Transactional
public class SystemUserDaoTest extends AbstractDaoTest<SystemUser> {

    @Autowired
    ISystemUserDao systemUserDao;

    @Override
    protected Object[][] getTestData() {
        return new Object[][]{
                {getSystemUser(1)},
                {getSystemUser(2)},
                {getSystemUser(4)},
                {getSystemUser(8)},
        };
    }

    @Override
    protected ISystemUserDao getDao() {
        return systemUserDao;
    }

    @Override
    protected void modifyEntity(SystemUser entity) {
        entity.setAccess(entity.getAccess()+"0");
    }

    @Override
    protected boolean isEntitiesEqual(SystemUser entity1, SystemUser entity2) {
        return entity1.getLoginName() != null && entity1.getLoginName().equals(entity2.getLoginName())
                && entity1.getAccess() != null && entity1.getAccess().equals(entity2.getAccess());
    }

    private SystemUser getSystemUser(int access){
        SystemUser user = new SystemUser();
        user.setLoginName(generateCode(8));
        user.setAccess("" + access);
        user.setLoginGroup("group"+access);
        user.setServerNumber(0);
        return user;
    }

    public String generateCode(int length) {
        String seq = new BigInteger(60, new SecureRandom()).toString(32);
        return seq.substring(0,length);
    }

    @Test(priority = 1, enabled = true)
    public void getUserByLogin(){
        SystemUser entity;
        for(SystemUser user: persistentData.keySet()){
            logger.info("Get user by login: "+user.getLoginName());
            entity = getDao().getUserByLogin(user.getLoginName());
            Assert.assertNotNull(entity);
            Assert.assertEquals(entity.getId(), (int)persistentData.get(user));
        }
    }

    @Test(priority = 1, enabled = true)
    public void getUsersByLoginGroup(){
        List<SystemUser> list;
        for(SystemUser user: persistentData.keySet()){
            logger.info("Get user by loginGroup: "+user.getLoginGroup());
            list = getDao().getUserByLoginGroup(user.getLoginGroup());
            Assert.assertNotNull(list);
        }
    }

}
