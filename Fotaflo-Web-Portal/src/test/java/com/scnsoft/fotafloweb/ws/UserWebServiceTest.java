package com.scnsoft.fotafloweb.ws;

import com.scnsoft.fotafloweb.AbstractTest;
import com.scnsoft.fotafloweb.bean.UserBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

public class UserWebServiceTest extends AbstractTest {

    @Autowired
    IUserWebService service;

    @Test
    public void test(){
        UserBean u = service.getUser("Aeopgf6");
        logger.info(u.getId() + " - " + u.getLoginName());

        u = service.getUser("tayna");
        logger.info(u);
    }

}
