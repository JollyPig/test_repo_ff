package com.scnsoft.fotafloweb.ws;

import com.scnsoft.fotafloweb.bean.LocationBean;
import com.scnsoft.fotafloweb.service.AbstractServiceTest;
import junit.framework.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class LocationWebServiceTest extends AbstractServiceTest {

    @Autowired
    ILocationWebService service;

    List<LocationBean> list = new ArrayList<LocationBean>();

    @Test(enabled = true)
    public void list(){
        List<LocationBean> list = service.getLocations();
        Assert.assertNotNull(list);
        logger.info("Clients list size: "+list.size());
        Assert.assertTrue(list.size() > 0);
        this.list = list;
    }

    @Test(enabled = true, dependsOnMethods = "list")
    public void get(){
        LocationBean rc;
        for(LocationBean c: this.list){
            rc = service.getLocation(c.getId());
            Assert.assertNotNull(rc);
            logger.info("Client: "+ rc);
        }
    }

}
