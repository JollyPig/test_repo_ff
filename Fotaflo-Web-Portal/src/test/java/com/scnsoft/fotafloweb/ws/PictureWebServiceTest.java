package com.scnsoft.fotafloweb.ws;

import com.scnsoft.fotafloweb.AbstractTest;
import com.scnsoft.fotafloweb.bean.PictureBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import java.util.List;

public class PictureWebServiceTest extends AbstractTest {

    @Autowired
    IPictureWebService service;

    @Test
    public void test(){
        List<PictureBean> list = service.getPictures("Aeopgf6");
        logger.info(list);
        for(PictureBean bean: list){
            logger.info("Picture: "+bean.getName()+", url: "+bean.getUrl()+", small: "+bean.getBase64code()+'\n'
                    +"date: "+bean.getCreationDate()+", logos: "+bean.getLogoUrl()+" - "+bean.getLogoText()+" - "+bean.getLogoMainUrl());
        }
    }

}
