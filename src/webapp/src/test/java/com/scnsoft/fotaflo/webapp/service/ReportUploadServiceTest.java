package com.scnsoft.fotaflo.webapp.service;

import com.scnsoft.fotaflo.webapp.reportmodule.service.ReportUploadService;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Scanner;

public class ReportUploadServiceTest {
    public static final Logger logger = Logger.getLogger(ReportUploadServiceTest.class);

    public static void main(String[] args) throws Exception{
        new ReportUploadServiceTest().execute();
    }

    public void execute() throws Exception{
        ApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext.xml","applicationContext-dao.xml");

        ReportUploadService reportUploadService = context.getBean(ReportUploadService.class);
        reportUploadService.setFullPath("C:/temp/");

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -4);

        Date date = calendar.getTime();
        Map<String, Object> reportMap = reportUploadService.fileReportCreate(date);

        logger.info(reportMap);

        String content = new Scanner(new File(reportMap.get("report") + "")).useDelimiter("\\Z").next();

        logger.info("\n" + content);
    }

}
