package com.scnsoft.fotaflo.webapp.util;

import org.apache.log4j.Logger;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

public class PictureResizerTest {
    protected static Logger logger = Logger.getLogger(PictureResizerTest.class);

    public static int pictureWidth = DefaultValues.pictureWidth;
    public static int pictureHeight = DefaultValues.pictureHeight;

    public static BufferedImage resizePictureFixed(String srcImg, String pictureSize) {
        if (pictureSize.equals("medium")) {
            pictureWidth = 1024;
            pictureHeight = 768;
        }
        if (pictureSize.equals("small")) {
            pictureWidth = DefaultValues.pictureWidth / 8;
            pictureHeight = DefaultValues.pictureHeight / 8;
        }
        try {
            boolean rotated = false;
            File _file = new File(srcImg);
            if (!_file.exists()) return null;

            int pictureWidthLocal = pictureWidth;
            int pictureHeightLocal = pictureHeight;

            BufferedImage readImage = ImageIO.read(_file);
            int h = readImage.getHeight();
            int w = readImage.getWidth();
            double ratio = (double) w / (double) h;
            if (h > w) {
                rotated = true;
                ratio = (double) h / (double) w;
            }
            if (ratio == (double) 16 / (double) 9) {
                logger.info("SPECIAL PICTURE FORMAT!");
                pictureWidthLocal = DefaultValues.pictureWidthFormat2;
                pictureHeightLocal = DefaultValues.pictureHeightFormat2;
            }

            logger.info("ROTATED when resize " + rotated);
            if (rotated) {
                int temp = pictureWidthLocal;
                pictureWidthLocal = pictureHeightLocal;
                pictureHeightLocal = temp;
            }
            Image src = readImage;
            BufferedImage image = new BufferedImage(pictureWidthLocal, pictureHeightLocal, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = image.createGraphics();
            g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g.drawImage(src, 0, 0, pictureWidthLocal, pictureHeightLocal, 0, 0, w, h, null);
            g.dispose();
            return image;
        } catch (Exception e) {
            logger.info("UPLOAD ERROR: The reseizing was UNSUCCESSFULL" + e);
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) throws Exception{
        String path1 = "C:/temp/testImages/1.jpg";
        String path2 = "C:/temp/testImages/2.jpg";

        String out = "C:/temp/testImages/";

        String size = "medium";

        long time = 0;
        float q = 0;
        BufferedImage image1, image2;

        time = System.currentTimeMillis();
        image1 = resizePictureFixed(path1, size);
        logger.info("Picture one processing time: " + (System.currentTimeMillis() - time));
        time = System.currentTimeMillis();
        image2 = resizePictureFixed(path2, size);
        logger.info("Picture two processing time: " + (System.currentTimeMillis() - time));

        ImageIO.write(image1, "jpg", new File(out+"1_thumb"+".jpg"));
        ImageIO.write(image2, "jpg", new File(out+"2_thumb"+".jpg"));

        q = 0.3f;
        logger.info("compressed: "+q*100+"%");
        time = System.currentTimeMillis();
        image1 = resizePictureFixed(path1, size);
        saveCompressedImage(image1, new File(out + "1_thumb_compressed_quality_" + (int)(q * 100) + "%.jpg"), q);
        logger.info("Picture one processing time: " + (System.currentTimeMillis() - time));
        time = System.currentTimeMillis();
        image2 = resizePictureFixed(path2, size);
        saveCompressedImage(image2, new File(out + "2_thumb_compressed_quality_" + (int)(q * 100) + "%.jpg"), q);
        logger.info("Picture two processing time: " + (System.currentTimeMillis() - time));

        q = 0.2f;
        logger.info("compressed: "+q*100+"%");
        time = System.currentTimeMillis();
        image1 = resizePictureFixed(path1, size);
        saveCompressedImage(image1, new File(out + "1_thumb_compressed_quality_" + (int)(q * 100) + "%.jpg"), q);
        logger.info("Picture one processing time: " + (System.currentTimeMillis() - time));
        time = System.currentTimeMillis();
        image2 = resizePictureFixed(path2, size);
        saveCompressedImage(image2, new File(out + "2_thumb_compressed_quality_" + (int)(q * 100) + "%.jpg"), q);
        logger.info("Picture two processing time: " + (System.currentTimeMillis() - time));

        q = 0.5f;
        logger.info("compressed: "+q*100+"%");
        time = System.currentTimeMillis();
        image1 = resizePictureFixed(path1, size);
        saveCompressedImage(image1, new File(out + "1_thumb_compressed_quality_" + (int)(q * 100) + "%.jpg"), q);
        logger.info("Picture one processing time: " + (System.currentTimeMillis() - time));
        time = System.currentTimeMillis();
        image2 = resizePictureFixed(path2, size);
        saveCompressedImage(image2, new File(out + "2_thumb_compressed_quality_" + (int)(q * 100) + "%.jpg"), q);
        logger.info("Picture two processing time: " + (System.currentTimeMillis() - time));

    }

    public static void saveCompressedImage(BufferedImage image, File out, float quality) throws IOException{
        Iterator iter = ImageIO.getImageWritersByFormatName("jpeg");
        ImageWriter writer = (ImageWriter) iter.next();

        ImageWriteParam iwp = writer.getDefaultWriteParam();
        iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);

        // reduced quality.
        iwp.setCompressionQuality(quality);

        FileImageOutputStream output = new FileImageOutputStream(out);

        writer.setOutput(output);

        IIOImage img = new IIOImage(image, null, null);
        writer.write(null, img, iwp);

        writer.dispose();
    }

}
