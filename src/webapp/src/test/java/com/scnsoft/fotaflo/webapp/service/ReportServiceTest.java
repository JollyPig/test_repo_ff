package com.scnsoft.fotaflo.webapp.service;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.util.Calendar;
import java.util.Date;

public class ReportServiceTest {
    static Logger logger = Logger.getLogger(ReportServiceTest.class);

    public static void main(String[] arg){
        ApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext.xml","applicationContext-dao.xml");

        ReportService reportService = context.getBean(ReportService.class);

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 1);

        logger.info("Date: " + calendar.getTime());
        boolean result = reportService.generateAndSendMonthlyStatisticV2(calendar.getTime(), ReportService.ReportNumber.LAST);
        logger.info(result);
    }

}
