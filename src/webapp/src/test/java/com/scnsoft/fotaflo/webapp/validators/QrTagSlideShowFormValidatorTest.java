package com.scnsoft.fotaflo.webapp.validators;

import com.scnsoft.fotaflo.webapp.exceptions.FormValidationException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

public class QrTagSlideShowFormValidatorTest {

    @Test
    public void testValidate () {

        final List<String> listFailureTag = Arrays.asList ("15-04-07_07:32:12_3946", "sss", "ddsQsds~123", "1234_753", "Miaw3", "Hello hello");

        Assert.assertEquals (process (listFailureTag), listFailureTag.size ());

        final List<String> listSuccessTag = Arrays.asList ("123", "AAFFF123", "123AAFFF123");

        Assert.assertEquals (process (listSuccessTag), 0);
    }

    protected int process (final List<String> list) {
        int quantity = 0;

        for (final String qrTag : list) {
            try {
                new QrTagSlideShowFormValidator (qrTag).validate ();
            }
            catch (FormValidationException ex) {
                quantity++;
            }
        }

        return quantity;
    }

}
