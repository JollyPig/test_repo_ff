package com.scnsoft.fotaflo.webapp.controller;

import com.scnsoft.fotaflo.common.util.IOUtil;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;

public class ImageUploaderTest {

    static final String username = "tayna";
    static final String password = "tayna";

    static final String path = "http://localhost:8080/fotaflo/pictures/upload";

    public static void main(String[] args) throws Exception{
        String credentials = username + ':' + password;
        String encoding = new sun.misc.BASE64Encoder().encode(credentials.getBytes());

        URL url = new URL(path);

        long date = new Date().getTime();

        HttpURLConnection uc = (HttpURLConnection)url.openConnection();
        uc.setRequestProperty("Authorization", "Basic " + encoding);
        uc.setRequestProperty("location","MINSK-Testwww");
        uc.setRequestProperty("filename","20150525_053733_PM.jpg");
        uc.setRequestProperty("FileDate",date+"");
//        uc.setRequestProperty("Timezone", "Asia/Shanghai");
        uc.setRequestProperty("Timezone", "Europe/Minsk");
        uc.setRequestProperty("deviceId","New Camera");
        uc.setRequestMethod("POST");
        System.out.println(date);

        uc.setDoInput(true);
        uc.setDoOutput(true);

        OutputStream content = null;
        InputStream source = null;
        File file = null;

        try{
            file = new File("C:\\temp\\testImages\\3.jpg");
            System.out.println(file.exists());
            source = new FileInputStream(file);
            content = uc.getOutputStream();

            IOUtil.writeFromInputToOutput(source, content);
            content.flush();
            content.close();

            //Get Response
            InputStream is = uc.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            System.out.println(response.toString());
            uc.disconnect();
        }finally {
            if(content != null){
                content.close();
            }
            if(source != null){
                source.close();
            }
        }
    }

}
