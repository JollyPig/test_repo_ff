package com.scnsoft.fotaflo.webapp.service;

import com.scnsoft.fotaflo.webapp.dao.*;
import com.scnsoft.fotaflo.webapp.model.*;
import com.scnsoft.fotaflo.webapp.util.AccessID;
import com.scnsoft.fotaflo.webapp.util.DateConvertationUtils;
import com.scnsoft.fotaflo.webapp.util.Filter;
import com.scnsoft.fotaflo.webapp.utils.CreationUtils;
import junit.framework.Assert;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.*;

import static org.easymock.EasyMock.*;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 30.03.2012
 * Time: 11:54:16
 * To change this template use File | Settings | File Templates.
 */


public class PictureServiceMock extends TestCase {  // todo recreate test
    IFilterDao filterDao = createMock(IFilterDao.class);
    ISystemUserDao userDao = createMock(ISystemUserDao.class);
    IMainLogoDao mainLogoDao = createMock(IMainLogoDao.class);
    IDao baseDao = createMock(IDao.class);
    ICameraDao cameraDao = createMock(ICameraDao.class);
    PictureService pictureService = new PictureService();


    @Override
    public void setUp() {
        /*pictureService = new PictureService();
        pictureService.setFilterDao(filterDao);
        pictureService.setSystemUserDao(userDao);
        pictureService.setBaseDao(baseDao);
        pictureService.setMainLogoDao(mainLogoDao);
        pictureService.setCameraDao(cameraDao);*/
    }

    @Test
    public void testGetUserFilter() {
        SystemUser systemUser = new SystemUser();
        UserFilter userFilter = new UserFilter();

        expect(userDao.getUserByLogin("testLogin")).andReturn(systemUser);
        expect(filterDao.getFilterByUser(systemUser)).andReturn(userFilter);
        replay(userDao);
        replay(filterDao);

        UserFilter userFilter1 = pictureService.getUserFilter("testLogin");
        Assert.assertNotNull(userFilter1);
        verify(userDao);
        verify(filterDao);
    }

    @Test
    public void testLoadPictureToDB() {
        String location = "testLocation1";
        String deviceId = "TestCamera2";
        String fileName = "testFileName";
        Date date = new Date();

        Location location1 = CreationUtils.createTestLocation(1, "testLocation1");
        Location location2 = CreationUtils.createTestLocation(2, "testLocation2");

        List<Camera> cameraList = new ArrayList<Camera>();
        cameraList.add(CreationUtils.createTestCamera(location1, 1, "TestCamera1"));
        cameraList.add(CreationUtils.createTestCamera(location1, 2, "TestCamera2"));
        cameraList.add(CreationUtils.createTestCamera(location2, 3, "TestCamera1"));
        cameraList.add(CreationUtils.createTestCamera(location2, 4, "TestCamera2"));

        expect(baseDao.retrieveAll(Camera.class)).andReturn(cameraList);
        expect(baseDao.create(isA(Picture.class))).andReturn(null);
        replay(baseDao);
        //pictureService.loadPictureToDB(location, deviceId, fileName, date);
        //verify(baseDao);
    }

    @Test
    public void testLoadPictureToDBNullCamera() {
        String location = "testLocation1";
        String deviceId = "TestCameraUnknown";
        String fileName = "testFileName";
        Date date = new Date();

        Location location1 = CreationUtils.createTestLocation(1, "testLocation1");
        Location location2 = CreationUtils.createTestLocation(2, "testLocation2");

        List<Camera> cameraList = new ArrayList<Camera>();
        cameraList.add(CreationUtils.createTestCamera(location1, 1, "TestCamera1"));
        cameraList.add(CreationUtils.createTestCamera(location1, 2, "TestCamera2"));
        cameraList.add(CreationUtils.createTestCamera(location2, 3, "TestCamera1"));
        cameraList.add(CreationUtils.createTestCamera(location2, 4, "TestCamera2"));

        expect(baseDao.retrieveAll(Camera.class)).andReturn(cameraList);
        replay(baseDao);
        //pictureService.loadPictureToDB(location, deviceId, fileName, date);
        //verify(baseDao);
    }

    @Test
    public void testLoadPictureToDBNullLocation() {
        String location = "testLocationUnknown";
        String deviceId = "TestCamera1";
        String fileName = "testFileName";
        Date date = new Date();

        Location location1 = CreationUtils.createTestLocation(1, "testLocation1");
        Location location2 = CreationUtils.createTestLocation(2, "testLocation2");

        List<Camera> cameraList = new ArrayList<Camera>();
        cameraList.add(CreationUtils.createTestCamera(location1, 1, "TestCamera1"));
        cameraList.add(CreationUtils.createTestCamera(location1, 2, "TestCamera2"));
        cameraList.add(CreationUtils.createTestCamera(location2, 3, "TestCamera1"));
        cameraList.add(CreationUtils.createTestCamera(location2, 4, "TestCamera2"));

        expect(baseDao.retrieveAll(Camera.class)).andReturn(cameraList);
        replay(baseDao);
        //pictureService.loadPictureToDB(location, deviceId, fileName, date);
        //verify(baseDao);
    }

    @Test
    public void testLoadPictureToDBNullLocationName() {
        String location = null;
        String deviceId = "TestCamera1";
        String fileName = "testFileName";
        Date date = new Date();

        Location location1 = CreationUtils.createTestLocation(1, null);
        List<Camera> cameraList = new ArrayList<Camera>();
        cameraList.add(CreationUtils.createTestCamera(location1, 1, "TestCamera1"));

        expect(baseDao.retrieveAll(Camera.class)).andReturn(cameraList);
        replay(baseDao);
        //pictureService.loadPictureToDB(location, deviceId, fileName, date);
        //verify(baseDao);
    }

    @Test
    public void testLoadPictureToDBNullCameraName() {
        String location = "testLocation1";
        String deviceId = null;
        String fileName = "testFileName";
        Date date = new Date();

        Location location1 = CreationUtils.createTestLocation(1, "testLocation1");

        List<Camera> cameraList = new ArrayList<Camera>();
        cameraList.add(CreationUtils.createTestCamera(location1, 1, null));

        expect(baseDao.retrieveAll(Camera.class)).andReturn(cameraList);
        replay(baseDao);
        //pictureService.loadPictureToDB(location, deviceId, fileName, date);
        //verify(baseDao);
    }


    @Test
    public void testGetAllPicturesSelected() {
        Date date = new Date();
        Date startDate = DateConvertationUtils.getStartDate(10, date);
        Date endDate = DateConvertationUtils.getStartDate(-10, date);

        Date date1 = DateConvertationUtils.getStartDate(0, date);
        Date date2 = DateConvertationUtils.getStartDate(-20, date);
        Date date3 = DateConvertationUtils.getStartDate(-5, date);

        Location location1 = CreationUtils.createTestLocation(1, "testLocation1");
        Camera camera = CreationUtils.createTestCamera(location1, 1, "TestCamera1");

        SystemUser user = CreationUtils.createTestSystemUser(1, "testLogin", AccessID.ADMIN.getStringValue());

        Set<SystemUser> systemUsers = new HashSet<SystemUser>();
        systemUsers.add(user);
        Picture picture1 = CreationUtils.createTestPicture(1, "testPictureName1", camera, date1, systemUsers, null, null, null, "testUrl1");
        Picture picture2 = CreationUtils.createTestPicture(2, "testPictureName2", camera, date2, systemUsers, null, null, null, "testUrl2");
        Picture picture3 = CreationUtils.createTestPicture(3, "testPictureName3", camera, date3, systemUsers, null, null, null, "testUrl3");

        Set<Picture> pictures = new HashSet<Picture>();
        pictures.add(picture1);
        pictures.add(picture2);
        pictures.add(picture3);
        user.setUserPictures(pictures);

        expect(userDao.getUserByLogin("testLogin")).andReturn(user);

        Filter filter = CreationUtils.createTestFilter("TestCamera1", startDate, endDate, "testLocation1");

        MainLogo mainLogo = CreationUtils.createTestMainLogo(1, location1, "Logo text url");
        expect(mainLogoDao.getMainLogoByLocation(location1)).andReturn(mainLogo).times(2);

        replay(userDao);
        replay(mainLogoDao);

        /*List<PictureBean> pictureBeans = pictureService.getAllPicturesSelected("testLogin", filter);
        Assert.assertEquals(pictureBeans.size(), 2);
         Assert.assertEquals(pictureBeans.get(0).getCameraId(), "1");
        Assert.assertEquals(pictureBeans.get(0).getLogoMainUrl(), "Logo text url");
        verify(userDao);
        verify(mainLogoDao);*/

    }

    @Test
    public void testGetAllPicturesSelected2() {
        Date date = new Date();
        Date startDate = DateConvertationUtils.getStartDate(10, date);
        Date endDate = DateConvertationUtils.getStartDate(-10, date);

        Date date1 = DateConvertationUtils.getStartDate(0, date);
        Date date2 = DateConvertationUtils.getStartDate(-20, date);
        Date date3 = DateConvertationUtils.getStartDate(-5, date);

        Location location1 = CreationUtils.createTestLocation(1, "testLocation1");
        Camera camera = CreationUtils.createTestCamera(null, 1, "TestCamera1");

        SystemUser user = CreationUtils.createTestSystemUser(1, "testLogin", AccessID.ADMIN.getStringValue());

        Set<SystemUser> systemUsers = new HashSet<SystemUser>();
        systemUsers.add(user);
        Picture picture1 = CreationUtils.createTestPicture(1, "testPictureName1", camera, date1, systemUsers, null, null, null, "testUrl1");
        Picture picture2 = CreationUtils.createTestPicture(2, "testPictureName2", camera, date2, systemUsers, null, null, null, "testUrl2");
        Picture picture3 = CreationUtils.createTestPicture(3, "testPictureName3", camera, date3, systemUsers, null, null, null, "testUrl3");

        Set<Picture> pictures = new HashSet<Picture>();
        pictures.add(picture1);
        pictures.add(picture2);
        pictures.add(picture3);
        user.setUserPictures(pictures);

        expect(userDao.getUserByLogin("testLogin")).andReturn(user);

        Filter filter = CreationUtils.createTestFilter("TestCamera1", startDate, endDate, "testLocation1");

        MainLogo mainLogo = CreationUtils.createTestMainLogo(1, location1, "Logo text url");
        //  expect(mainLogoDao.getMainLogoByLocation(location1)).andReturn(mainLogo).times(2);

        replay(userDao);
        replay(mainLogoDao);

        /*List<PictureBean> pictureBeans = pictureService.getAllPicturesSelected("testLogin", filter);
        Assert.assertEquals(pictureBeans.size(), 0);
        Assert.assertEquals( AccessID.ADMIN.getStringValue(),"1");

        verify(userDao);
        verify(mainLogoDao);*/

    }


    @Test
    public void testGetCameraList() {
        SystemUser user = CreationUtils.createTestSystemUser(1, "testLogin", AccessID.ADMIN.getStringValue());
        SystemUser user2 = CreationUtils.createTestSystemUser(2, "testLogin2", AccessID.USER.getStringValue());
        Location location1 = CreationUtils.createTestLocation(1, "testLocation1");
        user2.setLocation(location1);
        Camera camera = CreationUtils.createTestCamera(location1, 1, "TestCamera1");
        Camera camera2 = CreationUtils.createTestCamera(location1, 2, "TestCamera2");
        Camera camera3 = CreationUtils.createTestCamera(location1, 3, "TestCamera3");
        List<Camera> cameras = new ArrayList<Camera>();
        cameras.add(camera);
        cameras.add(camera2);
        cameras.add(camera3);

        expect(cameraDao.getCamerasByLocation(isA(Location.class))).andReturn(cameras);
        replay(cameraDao);
        List<Camera> camerasAll = pictureService.getCameraList("", "0", user2);
        Assert.assertEquals(camerasAll.size(), 3);
        verify(cameraDao);

    }

    @Test
    public void testGetCameraList2() {
        SystemUser user = CreationUtils.createTestSystemUser(1, "testLogin", AccessID.ADMIN.getStringValue());
        SystemUser user2 = CreationUtils.createTestSystemUser(2, "testLogin2", AccessID.USER.getStringValue());
        Location location1 = CreationUtils.createTestLocation(1, "testLocation1");
        user2.setLocation(location1);
        Camera camera = CreationUtils.createTestCamera(location1, 1, "TestCamera1");
        Camera camera2 = CreationUtils.createTestCamera(location1, 2, "TestCamera2");
        Camera camera3 = CreationUtils.createTestCamera(location1, 3, "TestCamera3");
        List<Camera> cameras = new ArrayList<Camera>();
        cameras.add(camera);
        cameras.add(camera2);
        cameras.add(camera3);

        expect((Camera) baseDao.retrieve(Camera.class, 1)).andReturn(camera);
        expect((Camera) baseDao.retrieve(Camera.class, 3)).andReturn(camera3);
        replay(baseDao);
        List<Camera> camerasAll = pictureService.getCameraList("", "1,3", user2);
        Assert.assertEquals(camerasAll.size(), 2);
        verify(baseDao);

    }


    @Test
    public void testGetCameraList3() {
        SystemUser user = CreationUtils.createTestSystemUser(1, "testLogin", AccessID.ADMIN.getStringValue());
        SystemUser user2 = CreationUtils.createTestSystemUser(2, "testLogin2", AccessID.USER.getStringValue());
        Location location1 = CreationUtils.createTestLocation(1, "testLocation1");
        user2.setLocation(location1);
        Camera camera = CreationUtils.createTestCamera(location1, 1, "TestCamera1");
        Camera camera2 = CreationUtils.createTestCamera(location1, 2, "TestCamera2");
        Camera camera3 = CreationUtils.createTestCamera(location1, 3, "TestCamera3");
        List<Camera> cameras = new ArrayList<Camera>();
        cameras.add(camera);
        cameras.add(camera2);
        cameras.add(camera3);

        expect(baseDao.retrieveAll(Camera.class)).andReturn(cameras);
        replay(baseDao);
        List<Camera> camerasAll = pictureService.getCameraList("0", "0", user);
        Assert.assertEquals(camerasAll.size(), 3);
        verify(baseDao);

    }

    @Test
    public void testGetCameraList4() {
        SystemUser user = CreationUtils.createTestSystemUser(1, "testLogin", AccessID.ADMIN.getStringValue());
        SystemUser user2 = CreationUtils.createTestSystemUser(2, "testLogin2", AccessID.USER.getStringValue());
        Location location1 = CreationUtils.createTestLocation(1, "testLocation1");
        user2.setLocation(location1);
        Camera camera = CreationUtils.createTestCamera(location1, 1, "TestCamera1");
        Camera camera2 = CreationUtils.createTestCamera(location1, 2, "TestCamera2");
        Camera camera3 = CreationUtils.createTestCamera(location1, 3, "TestCamera3");
        List<Camera> cameras = new ArrayList<Camera>();
        cameras.add(camera);
        cameras.add(camera2);
        cameras.add(camera3);

        expect((Camera) baseDao.retrieve(Camera.class, 1)).andReturn(camera);
        expect((Camera) baseDao.retrieve(Camera.class, 2)).andReturn(camera2);
        replay(baseDao);
        List<Camera> camerasAll = pictureService.getCameraList("0", "1,2", user);
        Assert.assertEquals(camerasAll.size(), 2);
        verify(baseDao);

    }

    @Test
    public void testGetCameraList5() {
        SystemUser user = CreationUtils.createTestSystemUser(1, "testLogin", AccessID.ADMIN.getStringValue());
        SystemUser user2 = CreationUtils.createTestSystemUser(2, "testLogin2", AccessID.USER.getStringValue());
        Location location1 = CreationUtils.createTestLocation(1, "testLocation1");
        user2.setLocation(location1);
        Camera camera = CreationUtils.createTestCamera(location1, 1, "TestCamera1");
        Camera camera2 = CreationUtils.createTestCamera(location1, 2, "TestCamera2");
        Camera camera3 = CreationUtils.createTestCamera(location1, 3, "TestCamera3");
        List<Camera> cameras = new ArrayList<Camera>();
        cameras.add(camera);
        cameras.add(camera2);
        cameras.add(camera3);

        expect((Location) baseDao.retrieve(Location.class, 1)).andReturn(location1);
        expect(cameraDao.getCamerasByLocation(location1)).andReturn(cameras);
        replay(cameraDao);
        replay(baseDao);
        List<Camera> camerasAll = pictureService.getCameraList("1", "0", user);
        Assert.assertEquals(camerasAll.size(), 3);
        verify(baseDao);
        verify(cameraDao);
    }

    @Test
    public void testGetCameraList6() {
        SystemUser user = CreationUtils.createTestSystemUser(1, "testLogin", AccessID.ADMIN.getStringValue());
        SystemUser user2 = CreationUtils.createTestSystemUser(2, "testLogin2", AccessID.USER.getStringValue());
        Location location1 = CreationUtils.createTestLocation(1, "testLocation1");
        user2.setLocation(location1);
        Camera camera = CreationUtils.createTestCamera(location1, 1, "TestCamera1");
        Camera camera2 = CreationUtils.createTestCamera(location1, 2, "TestCamera2");
        Camera camera3 = CreationUtils.createTestCamera(location1, 3, "TestCamera3");
        List<Camera> cameras = new ArrayList<Camera>();
        cameras.add(camera);
        cameras.add(camera2);
        cameras.add(camera3);

        expect((Location) baseDao.retrieve(Location.class, 1)).andReturn(location1);
        expect((Camera) baseDao.retrieve(Camera.class, 3)).andReturn(camera3);
        replay(cameraDao);
        replay(baseDao);
        List<Camera> camerasAll = pictureService.getCameraList("1", "3", user);
        Assert.assertEquals(camerasAll.size(), 1);
        verify(baseDao);
        verify(cameraDao);
    }

    @Test
    public void testGetCameraList7() {
        SystemUser user = CreationUtils.createTestSystemUser(1, "testLogin", AccessID.ADMIN.getStringValue());
        SystemUser user2 = CreationUtils.createTestSystemUser(2, "testLogin2", AccessID.USER.getStringValue());
        Location location1 = CreationUtils.createTestLocation(1, "testLocation1");
        user2.setLocation(location1);
        Camera camera = CreationUtils.createTestCamera(location1, 1, "TestCamera1");
        Camera camera2 = CreationUtils.createTestCamera(location1, 2, "TestCamera2");
        Camera camera3 = CreationUtils.createTestCamera(location1, 3, "TestCamera3");
        List<Camera> cameras = new ArrayList<Camera>();
        cameras.add(camera);
        cameras.add(camera2);
        cameras.add(camera3);

        replay(cameraDao);
        replay(baseDao);
        List<Camera> camerasAll = pictureService.getCameraList("", "", user);
        Assert.assertEquals(camerasAll.size(), 0);
        verify(baseDao);
        verify(cameraDao);
    }

    @Test
    public void testGetCameraList8() {
        SystemUser user = CreationUtils.createTestSystemUser(1, "testLogin", AccessID.ADMIN.getStringValue());
        SystemUser user2 = CreationUtils.createTestSystemUser(2, "testLogin2", AccessID.USER.getStringValue());
        Location location1 = CreationUtils.createTestLocation(1, "testLocation1");
        user2.setLocation(location1);
        Camera camera = CreationUtils.createTestCamera(location1, 1, "TestCamera1");
        Camera camera2 = CreationUtils.createTestCamera(location1, 2, "TestCamera2");
        Camera camera3 = CreationUtils.createTestCamera(location1, 3, "TestCamera3");
        List<Camera> cameras = new ArrayList<Camera>();
        cameras.add(camera);
        cameras.add(camera2);
        cameras.add(camera3);

        replay(cameraDao);
        replay(baseDao);
        List<Camera> camerasAll = pictureService.getCameraList("", "1,2,3,0", user);
        Assert.assertEquals(camerasAll.size(), 0);
        verify(baseDao);
        verify(cameraDao);
    }


}
