package com.scnsoft.fotaflo.webapp.service;

import com.scnsoft.fotaflo.webapp.bean.TagSavedPurchaseBean;
import com.scnsoft.fotaflo.webapp.util.Filter;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class PurchaseServiceTest {
    static Logger logger = Logger.getLogger(ReportServiceTest.class);

    public static void main(String[] arg){
        ApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext.xml","applicationContext-dao.xml");

        PurchaseService purchaseService = context.getBean(PurchaseService.class);
        TagPurchaseService tagPurchaseService = context.getBean(TagPurchaseService.class);

        Date start, end;
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, 3);
        calendar.set(Calendar.DATE, 1);
        start = calendar.getTime();

        calendar.add(Calendar.DATE, 30);
        end = calendar.getTime();

        logger.info("Date: " + start + " - " + end);
        Filter filter = new Filter(start, end, null, null, null);
        String email = null, code = null, targetTag = null;

        long t1 = System.currentTimeMillis();
        List<TagSavedPurchaseBean> result = purchaseService.getHistoryPurchasesByParameters(filter, email, code, targetTag);
        long t2 = System.currentTimeMillis();
        List<TagSavedPurchaseBean> result1 = tagPurchaseService.getHistoryTagPurchasesByParameters(filter, email, code, targetTag);
        long t3 = System.currentTimeMillis();
        result.addAll(result1);
        long t4 = System.currentTimeMillis();
        logger.info(result.size());
        logger.info(result1.size());
        logger.info("Time: " + Arrays.asList((t2-t1),(t3-t2),(t4-t3)));
    }

}
