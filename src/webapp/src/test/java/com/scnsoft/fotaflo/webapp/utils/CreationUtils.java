package com.scnsoft.fotaflo.webapp.utils;

import com.scnsoft.fotaflo.webapp.model.*;
import com.scnsoft.fotaflo.webapp.util.Filter;

import java.util.Date;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 31.03.2012
 * Time: 3:40:10
 * To change this template use File | Settings | File Templates.
 */
public class CreationUtils {

    public static Camera createTestCamera(Location location, int id, String cameraName) {
        Camera camera = new Camera();
        camera.setCameraName(cameraName);
        camera.setId(id);
        camera.setLocation(location);
        return camera;
    }

    public static Location createTestLocation(int id, String locationName, boolean reduced, Integer locationSmtpServerPort, String locationFromEmail,
                                              String locationImageLogoUrl, String locationSmtpServer,
                                              String locationSmtpServerPassword, String sslCheck,
                                              String locationTextLogo, String locationToFacebookEmail, String locationToFlickrEmail, LocationSettings locationSettings,
                                              PayPalLocationSettings paypallocationSettings) {
        Location location = new Location();
        location.setId(id);
        location.setLocationName(locationName);
        location.setReduced(reduced);
        location.setLocationFromEmail(locationFromEmail);
        location.setLocationImageLogoUrl(locationImageLogoUrl);
        location.setLocationSettings(locationSettings);
        location.setLocationSmtpServer(locationSmtpServer);
        location.setLocationSmtpServerPassword(locationSmtpServerPassword);
        location.setLocationSmtpServerPort(locationSmtpServerPort);
        location.setLocationSslCheck(sslCheck);
        location.setLocationTextLogo(locationTextLogo);
        location.setLocationToFacebookEmail(locationToFacebookEmail);
        location.setLocationToFlickrEmail(locationToFlickrEmail);
        location.setPaypallocationSettings(paypallocationSettings);
        return location;
    }

    public static Location createTestLocation(int id, String locationName) {
        return createTestLocation(id, locationName, true, null, null, null, null, null, null, null, null, null, null, null);

    }

    public static SystemUser createTestSystemUser(int id, String login, String access, String firstName, String lastName, String password,
                                                  Date creationDate, Date expirationDate, String email,
                                                  String emailbody, Location location, Set<Requests> requests, String stringedFileName,
                                                  Set<PurchaseSelected> selectedPictures, Set<Picture> userPictures, UserSettings userSettings) {
        SystemUser systemUser = new SystemUser();
        systemUser.setId(id);
        systemUser.setAccess(access);
        systemUser.setCreationDate(creationDate);
        systemUser.setEmail(email);
        systemUser.setEmailBody(emailbody);
        systemUser.setExpirationDate(expirationDate);
        systemUser.setFirstName(firstName);
        systemUser.setLastName(lastName);
        systemUser.setLocation(location);
        systemUser.setLoginName(login);
        systemUser.setPassword(password);
        systemUser.setRequests(requests);
        systemUser.setSelectedPictures(selectedPictures);
        systemUser.setStringedFileName(stringedFileName);
        systemUser.setUserPictures(userPictures);
        systemUser.setUserSettings(userSettings);
        return systemUser;
    }

    public static SystemUser createTestSystemUser(int id, String login, String access) {
        return createTestSystemUser(id, login, access, null, null, null,
                null, null, null, null, null, null, null, null, null, null);

    }

    public static SystemUser createTestSystemUser(int id, String login, String access, Location location, Set<Picture> userPictures) {
        return createTestSystemUser(id, login, access, null, null, null,
                null, null, null, null, location, null, null, null, userPictures, null);

    }

    public static Picture createTestPicture(int id, String name, Camera camera, Date creationDate, Set<SystemUser> systemUsers,
                                            Set<HelpYourselfSelected> helpYourselfSelected, Set<RequestsSelected> requestSelected,
                                            Set<PurchaseSelected> selectedUsers, String url) {
        Picture picture = new Picture();
        picture.setCamera(camera);
        picture.setCreationDate(creationDate);
        picture.setHelpYourselfSelected(helpYourselfSelected);
        picture.setId(id);
        picture.setName(name);
        picture.setRequestSelected(requestSelected);
        picture.setSelectedUsers(selectedUsers);
        picture.setUrl(url);
        picture.setSystemUsers(systemUsers);
        return picture;
    }

    public static Filter createTestFilter(String camera, Date startDate, Date endDate, String location) {
        Filter filter = null;//new Filter(startDate, endDate, location, camera);
        return filter;
    }

    public static MainLogo createTestMainLogo(int id, Location location, String imageMainLogo) {
        MainLogo mainLogo = new MainLogo();
        mainLogo.setId(id);
        mainLogo.setLocation(location);
        mainLogo.setImageMainLogo(imageMainLogo);
        return mainLogo;
    }

}
