package com.scnsoft.fotaflo.webapp.service;

import com.scnsoft.fotaflo.webapp.bean.LocationBean;
import com.scnsoft.fotaflo.webapp.dao.ISystemUserDao;
import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.model.SystemUser;
import com.scnsoft.fotaflo.webapp.util.AccessID;
import com.scnsoft.fotaflo.webapp.utils.CreationUtils;
import junit.framework.Assert;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.easymock.EasyMock.*;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 31.03.2012
 * Time: 11:09:17
 * To change this template use File | Settings | File Templates.
 */
public class LocationServiceMock extends TestCase {// todo recreate test

    /*  IDao baseDao = createMock(IDao.class);*/
    ISystemUserDao userDao = createMock(ISystemUserDao.class);
    LocationService locationService = new LocationService();


    @Override
    public void setUp() {
        locationService = new LocationService();
       /* locationService.setBaseDao(baseDao);*/
        //locationService.setSystemUserDao(userDao);

    }

 /*   @Test
    public void testUserLocationsAdmin() {
         SystemUser user = CreationUtils.createTestSystemUser(1, "testLogin", AccessID.ADMIN.getStringValue());
         SystemUser user2 = CreationUtils.createTestSystemUser(2, "testLogin2", AccessID.USER.getStringValue());
         Location location1 = CreationUtils.createTestLocation(1, "testLocation1");
         Location location2 = CreationUtils.createTestLocation(2, "testLocation2");
         user.setLocation(location1);
         user2.setLocation(location2);
         List<Location> locations = new ArrayList<Location>();
         locations.add(location1);
         locations.add(location2);
         expect(userDao.getUserByLogin("testLogin")).andReturn(user);
         expect(baseDao.retrieveAll(Location.class)).andReturn(locations);
         replay(userDao);
         replay(baseDao);
         List<Location> locs = locationService.getUserLocations("testLogin");
         Assert.assertEquals(locs.size(), 3);
         verify(userDao);
         verify(baseDao);
    }*/

    @Test
    public void testUserLocationsUser() {
        SystemUser user = CreationUtils.createTestSystemUser(1, "testLogin", AccessID.ADMIN.getStringValue());
        SystemUser user2 = CreationUtils.createTestSystemUser(2, "testLogin2", AccessID.USER.getStringValue());
        Location location1 = CreationUtils.createTestLocation(1, "testLocation1");
        Location location2 = CreationUtils.createTestLocation(2, "testLocation2");
        user.setLocation(location1);
        user2.setLocation(location2);
        List<Location> locations = new ArrayList<Location>();
        locations.add(location1);
        locations.add(location2);
        expect(userDao.getUserByLogin("testLogin2")).andReturn(user2);
        replay(userDao);

        List<LocationBean> locs = locationService.getUserLocations("testLogin2");
        Assert.assertEquals(locs.size(), 1);
        Assert.assertEquals(locs.get(0).getLocationName(), "testLocation2");
        verify(userDao);

    }

    /* @Test
    public void testUserRealLocations() {
         SystemUser user = CreationUtils.createTestSystemUser(1, "testLogin", AccessID.ADMIN.getStringValue());
         SystemUser user2 = CreationUtils.createTestSystemUser(2, "testLogin2", AccessID.USER.getStringValue());
         Location location1 = CreationUtils.createTestLocation(1, "testLocation1");
         Location location2 = CreationUtils.createTestLocation(2, "testLocation2");
         user.setLocation(location1);
         user2.setLocation(location2);
         List<Location> locations = new ArrayList<Location>();
         locations.add(location1);
         locations.add(location2);
         expect(userDao.getUserByLogin("testLogin")).andReturn(user);
         expect(baseDao.retrieveAll(Location.class)).andReturn(locations);
         replay(userDao);
         replay(baseDao);
         List<Location> locs = locationService.getUserRealLocations("testLogin",0,10);
         Assert.assertEquals(locs.size(), 2);
         verify(userDao);
         verify(baseDao);
    }*/


    @Test
    public void testUserRealLocationsUser() {
        SystemUser user = CreationUtils.createTestSystemUser(1, "testLogin", AccessID.ADMIN.getStringValue());
        SystemUser user2 = CreationUtils.createTestSystemUser(2, "testLogin2", AccessID.USER.getStringValue());
        Location location1 = CreationUtils.createTestLocation(1, "testLocation1");
        Location location2 = CreationUtils.createTestLocation(2, "testLocation2");
        user.setLocation(location1);
        user2.setLocation(location2);
        List<Location> locations = new ArrayList<Location>();
        locations.add(location1);
        locations.add(location2);
        expect(userDao.getUserByLogin("testLogin2")).andReturn(user2);
        replay(userDao);

        List<Location> locs = locationService.getUserRealLocations("testLogin2", 0, 10);
        Assert.assertEquals(locs.size(), 1);
        verify(userDao);

    }

}
