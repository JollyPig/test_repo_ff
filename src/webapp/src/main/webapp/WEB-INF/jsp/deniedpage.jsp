<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <link href="<%= request.getContextPath()%>/css/style.css" rel="stylesheet" type="text/css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Fotaflo Denied</title>
     <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/ext-3.2.1/resources/css/ext-all.css"/>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/ext-3.2.1/resources/css/xtheme-gray.css"/>
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/style.css"/>

    <script type="text/javascript" src="<%= request.getContextPath()%>/js/handleTimeout.js"></script>
    <script src="<%= request.getContextPath()%>/ext-3.2.1/adapter/ext/ext-base.js" type="text/javascript"></script>
    <script src="<%= request.getContextPath()%>/ext-3.2.1/ext-all.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css"
              href="<%= request.getContextPath()%>/js/ux/data-view.css"/>

    <script type="text/javascript">
        var GLOBAL = {
            userId: ${userId},
            contextPath: "<%= request.getContextPath()%>",
            tagApp: '${tagApp}'
        };
    </script>  
</head>

<body>

<div id="wrapper" class="wrapper">

    <div id="header" class="headerpage">
        <%--<div class="logo-img"></div>
        <div class="logo-login"> Hello, ${current}!
            <a id="logout" href="<%= request.getContextPath()%>/pictures/auth/logout"
               class="logout_link">Logout <img src="<%= request.getContextPath()%>/css/images/logout.png" alt=""></a>
        </div>--%>
        <div class="logo-img"></div>
        <jsp:include page="menu.jsp"/>
        </div>

    <!-- #header-->

    <div id="content" class="content">


        <%--<div id="login-error">${error}</div>--%>
        <h1 id="main">Access Denied</h1>

    </div>
    <!-- #content-->

</div>
<!-- #wrapper -->
<div class="footer"> <jsp:include page="copyright.jsp"/></div>
<!-- #footer -->

</body>
</html>
