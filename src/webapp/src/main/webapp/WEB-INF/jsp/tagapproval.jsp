<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml">

<head>
    <link href="<%= request.getContextPath()%>/css/style.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css"
          href="<%= request.getContextPath()%>/ext-3.2.1/resources/css/ext-all.css"/>
    <link rel="stylesheet" type="text/css"
          href="<%= request.getContextPath()%>/js/ux/data-view.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Fotaflo Tag Approval</title>

     <style type="text/css">


        #mainbody{
            background: white;
            width:100%;
            display:none;
        }
        #v{
            width:320px;
            height:240px;
        }
        #qr-canvas{
            display:none;
        }
        #outdiv
        {
            width:320px;
            height:240px;
            border: solid;
            border-width: 3px 3px 3px 3px;
            margin: 10px 0 0;
        }
        #result{
            border: solid;
            border-color: gray;
            border-width: 1px 1px 1px 1px;
            padding:0px;
            width:70%;
        }

        .tsel{
            padding:0;
            width:20%;
        }
        .tagAppr{
            margin-left: 15px;
            margin-top: 25px;
            color: #666666;
            font-family: Arial,Tahoma,Verdana,sans-serif;
            font-size: 16px;
            font-weight: bold;
        }

        #tot_scan{
            margin: 10;
        }
    </style>



    <script type="text/javascript" src="<%= request.getContextPath()%>/js/handleTimeout.js"></script>


    <script type="text/javascript">
        var GLOBAL = {
            userId: ${userId},
            location: "${location}",
            tagApp: '${tagApp}',
            giveaway: ${giveaway},
            emails: [],
            fileName: "${fileName}",
            staff: "${staff}",
            contextPath: "<%= request.getContextPath()%>"
        };
    </script>
    <!-- ExtJS js -->
    <script type="text/javascript" src="<%= request.getContextPath()%>/ext-3.2.1/adapter/ext/ext-base.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/ext-3.2.1/ext-all.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/Ext.ux.util.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/lovcombo.js"></script>

    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/grid.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/version.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/detector.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/formatinf.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/errorlevel.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/bitmat.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/datablock.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/bmparser.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/datamask.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/rsdecoder.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/gf256poly.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/gf256.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/decoder.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/qrcode.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/findpat.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/alignpat.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/databr.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/webqr.js"></script>

<%--    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/llqrcode.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/webqr.js"></script>--%>

</head>

<body>


<div id="wrapper" class="wrapper">

    <div id="header" class="headerpage">
        <div class="logo-img"></div>
        <jsp:include page="menu.jsp"/>
    </div>
    <div>

        <span  class="tagAppr">Tag Approval</span>

    </div>

   <div id="mainbody">
        <table class="tsel" border="0">
            <tr>
                <td valign="top" align="center" width="50%">
                    <table class="tsel" border="0">

                        <tr><td colspan="2" align="center">
                            <div id="outdiv">
                            </div></td></tr>
                    </table>
                </td>
            </tr>

            <tr><td colspan="3" align="center">
                <div id="result"></div>
            </td></tr>

        </table>
    </div>
   <%-- <br/><br/>
   --%>
    <div id="content" class="content">
     <%--   <br/>fT
        <div id="tot_scan"><label id="total_scanned_id" class=" p_labels_total">Total number of scanned pictures: </label> </div>--%>
        <script src="<%= request.getContextPath()%>/js/ux/tagapproval.js"></script>

    </div>

    <audio id="audiotag1" src="<%= request.getContextPath()%>/js/audio/button1.wav" preload="auto"></audio>
    <audio id="audiotag2" src="<%= request.getContextPath()%>/js/audio/button2.wav" preload="auto"></audio>
    <!-- #content-->

</div>
<!-- #wrapper -->

<div class="footer"> <jsp:include page="copyright.jsp"/></div>
<!-- #footer -->
<canvas id="qr-canvas" width="800" height="600"></canvas>
<script type="text/javascript">
    var total = 0;
    function tagScanned(scannedCode){
        var value = scannedCode,
                panel = Ext.getCmp('tagCheckbox'),
                hf,hv,contains,
                cb,
                body;

        if(!value){
            return;
        }

        body = panel.getEl().child('.x-panel-body');

        Ext.each(body.query('.x-form-checkbox'), function(item){
            var cb = Ext.getCmp(Ext.get(item).getAttribute('id')),
                    lb;
            lb = cb.boxLabel;
            if(value == lb){
                contains = true;
            }
        })
        if(contains){
            document.getElementById('audiotag1').play();
            return;
        }

        document.getElementById('audiotag2').play();
        cb = new Ext.form.Checkbox({
            boxLabel: value,
            name: 'tags',
            ctCls: 'p_labels',
            style: {
                verticalAlign: 'middle'
            },
            listeners: {
                check: function(checkbox, checked) {
                    if(checked){
                        total = total+1;
                    }else{
                        total= total-1;
                    }
                    var combo  = Ext.getCmp('participantCombo');
                    var store = combo.getStore(),
                            field = combo.valueField,
                            value,
                            index;
                    if (!(total > 0)){
                        combo.setValue(1);
                    }else{
                        combo.setValue(total);
                    }

                    value = combo.getValue();
                    index = store.find(field, value);
                    combo.fireEvent('select', combo, store.getAt(index), index);
                    document.getElementById('totalLabel').textContent =total;
                }
            }

        })
        cb.render(body, 0);

        cb.setValue(true);
    }

    load(function(code){
        if (code != null && /^[A-Z0-9]*$/.test (code)) {
            tagScanned(code);
        }
        else {
            Ext.Msg.alert('', 'QR tag \"' + code + '\" should contain only capital letters and numbers.');
        }
    });
</script>
</body>
</html>
