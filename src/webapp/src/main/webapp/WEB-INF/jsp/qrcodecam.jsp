<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml">

<head>
    <link href="<%= request.getContextPath()%>/css/style.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css"
          href="<%= request.getContextPath()%>/ext-3.2.1/resources/css/ext-all.css"/>
    <link rel="stylesheet" type="text/css"
          href="<%= request.getContextPath()%>/js/ux/data-view.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Fotaflo QR Code Scanner</title>

    <style type="text/css">

        body{
            background-color: rgb(206, 206, 206);
        }

        #mainbody{
            background: white;
            width:100%;
            display:none;
        }
        #v{
            width:320px;
            height:240px;
        }
        #qr-canvas{
            display:none;
        }
        #outdiv
        {
            width:320px;
            height:240px;
            border: solid;
            border-width: 3px 3px 3px 3px;
            margin: 10px 0 0;
        }
        #result{
            border: solid;
            border-color: gray;
            border-width: 1px 1px 1px 1px;
            padding:0px;
            width:70%;
        }

        .tsel{
            padding:0;
            width:20%;
        }

        #wrapper{
            width: 350px;
            height: 100%;
            min-width: 300px;
            padding: 10px;
            padding-left: 20px;
            overflow: hidden;
            margin: auto;
        }

        #loadMask{
            margin: auto;
            text-align: center;
            width: 150px;
            display: none;
        }

        #loadMask div{
            background-size: 24px;
        }
    </style>



    <script type="text/javascript" src="<%= request.getContextPath()%>/js/handleTimeout.js"></script>


    <script type="text/javascript">
        var GLOBAL = {
            userId: ${userId},
            currentUser: "${current}",
            location: "${location}",
            contextPath: "<%= request.getContextPath()%>"
        };
    </script>
    <!-- ExtJS js -->
    <script type="text/javascript" src="<%= request.getContextPath()%>/ext-3.2.1/adapter/ext/ext-base.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/ext-3.2.1/ext-all.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/Ext.ux.util.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/lovcombo.js"></script>

    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/grid.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/version.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/detector.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/formatinf.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/errorlevel.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/bitmat.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/datablock.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/bmparser.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/datamask.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/rsdecoder.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/gf256poly.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/gf256.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/decoder.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/qrcode.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/findpat.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/alignpat.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/databr.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/webqr.js"></script>

    <%--    <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/llqrcode.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath()%>/js/ws/webqr.js"></script>--%>

</head>

<body onload="onloadHandler();">


<div id="wrapper" class="wrapper">

    <div id="mainbody">
        <table class="tsel" border="0">
            <tr>
                <td valign="top" align="center" width="50%">
                    <table class="tsel" border="0">

                        <tr><td colspan="2" align="center">
                            <div id="outdiv">
                            </div></td></tr>
                    </table>
                </td>
            </tr>

            <tr><td colspan="3" align="center">
                <div id="result"></div>
            </td></tr>

        </table>
    </div>

    <audio id="audiotag1" src="<%= request.getContextPath()%>/js/audio/button1.wav" preload="auto"></audio>
    <audio id="audiotag2" src="<%= request.getContextPath()%>/js/audio/button2.wav" preload="auto"></audio>
    <!-- #content-->

</div>
<!-- #wrapper -->

<div id="loadMask" class="x-mask-loading">
    <div>Please wait...</div>
</div>

<!-- #footer -->
<canvas id="qr-canvas" width="400" height="300"></canvas>

<script type="text/javascript">

    function tagScanned(code){
        var url = '../uploadtag/' + GLOBAL.currentUser;
        console.log('Scanned: ' + code);
        document.getElementById('audiotag2').play();

        Ext.Ajax.request({
            url: url,
            method: 'GET',
            success: function(response){
                console.log('success',response)
                if(response.responseText == "true"){
                    showMask();
                }
            },
            failure: function(response){
                console.log('failure',response)
                Ext.Msg.alert('','Error occured on tag registration');
                hideMask();
            },
            params: { tag: code }
        });
    }

    function onloadHandler(){
        load(function(code){
            if (code != null && /^[A-Z0-9]*$/.test (code)) {
                tagScanned(code);
            } else {
                Ext.Msg.alert('', 'QR tag \"' + code + '\" should contain only capital letters and numbers.');
            }
        });
    }

    function showMask(){
        var mask = Ext.fly('loadMask');
        mask.show();
    }

    function hideMask(){
        var mask = Ext.fly('loadMask');
        mask.hide();
    }

</script>

</body>
</html>
