<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml">

<head>
    <link href="<%= request.getContextPath()%>/css/style.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css"
              href="<%= request.getContextPath()%>/ext-3.2.1/resources/css/ext-all.css"/>
        <link rel="stylesheet" type="text/css"
              href="<%= request.getContextPath()%>/js/ux/data-view.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Fotaflo Purchase</title>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/handleTimeout.js"></script>
    <script type="text/javascript">
        var GLOBAL = {
            pageSize: ${pageSize},
            startDate: ${startDate},
            endDate: ${endDate},
            startDay: "${startDay}",
            endDay: "${endDay}",
            startTime: "${startTime}",
            endTime: "${endTime}",
            userId: ${userId},
            contextPath: "<%= request.getContextPath()%>",
            requestId: ${requestId},
            emailCount: ${emailCount},
            emails: "${emails}",
            isPurchase: true,
            fileName: "${fileName}",
            payed: ${payed},
            selectedids: "${selectedids}",
            email:"${email}",
            price:${price},
            redirectpaypal:"${redirectpaypal}",
            facebookPage:"${facebookPage}",
            facebookAppId:"${facebookAppId}",
            purchId:${purchId},
            purchIdCat: ${purchIdCat},
            emailOtherCount: ${emailOtherCount},
            emailsOther: "${emailsOther}",
            location: "${location}",
            packages: "${packages}",
            staff: "${staff}",
            subject: "${subject}",
            tagApp: '${tagApp}'
        };
    </script>
    <!-- ExtJS js -->
    <script type="text/javascript" src="<%= request.getContextPath()%>/ext-3.2.1/adapter/ext/ext-base.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/ext-3.2.1/ext-all.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/Ext.ux.util.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/lovcombo.js"></script>
</head>

<body>

<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function() {
        // init the FB JS SDK
        FB.init({
            appId      : "${facebookAppId}",
            status     : true,                                 // Check Facebook Login status
            xfbml      : true                                  // Look for social plugins on the page
        });
    };
    // Load the SDK asynchronously
    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

</script>

<div id="wrapper" class="wrapper">

    <div id="header" class="headerpage">
        <div class="logo-img"></div>
        <jsp:include page="menu.jsp"/>
    </div>

    <!--
      Below we include the Login Button social plugin. This button uses the JavaScript SDK to
      present a graphical Login button that triggers the FB.login() function when clicked.

      Learn more about options for the login button plugin:
      /docs/reference/plugins/login/ -->

    <div id="content" class="content">

        <fb:login-button autologoutlink="true" perms="publish_actions,user_photos"></fb:login-button>
        <!-- App js -->
        <script src="<%= request.getContextPath()%>/js/ux/purchase.js"></script>

    </div>
    <!-- #content-->

</div>
<!-- #wrapper -->

<div class="footer"> <jsp:include page="copyright.jsp"/></div>
<!-- #footer -->

</body>
</html>
