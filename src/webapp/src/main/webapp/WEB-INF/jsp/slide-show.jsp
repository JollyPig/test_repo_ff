<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
<title>Fotaflo Slideshow</title>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/touch-slide-show.css"/>
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/js/ux/data-view.css"/>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/ext-3.2.1/resources/css/ext-all.css"/>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/ext-3.2.1/resources/css/xtheme-gray.css"/>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/handleTimeout.js"></script>
    <script src="<%= request.getContextPath()%>/ext-3.2.1/adapter/ext/ext-base.js" type="text/javascript"></script>
    <script src="<%= request.getContextPath()%>/ext-3.2.1/ext-all.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../js/jquery/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="../../js/jquery/slideshow.js"></script>
    <script type="text/javascript" src="../../js/touchSlideShow.js"></script>
    <script type="text/javascript">
    var GLOBAL = {
                userId:"${userId}",
                contextPath:"<%= request.getContextPath()%>",
                slideSpeed:"${slidespeed}",
                slideCount:"${slidecount}",
                leftUrl:"${leftUrl}",
                rightUrl:"${rightUrl}"
        },
        PARAM = {
            startdate:"${startdate}",
            starttime:"${starttime}",
            enddate:"${enddate}",
            endtime:"${endtime}",
            location:"${location}",
            camera:"${camera}",
            tag:"${tag}"
        },
        currentSave=-1;
        selected = false,
        selectedIds = [],
        total = 0,
        nextIndexForUpload = 0,
        positions = [],
        isPlay = false,
        PlayLoader = {
            start:function () {
                uploadPicturesThread();
            },

            stop:function () {
                isPlay = false;
            }
        },
        slideShow = null;

    function uploadPicturesThread() {
        var delay = GLOBAL.slideCount == 1 ? getSlideSpeed() + 1000 : getSlideSpeed() * 3 + 500;
        if (GLOBAL.slideSpeed == 20) {
            delay -= 500;
        }
        setInterval(function () {
            if (isPlay) {
                var current = $('.slideshow').data('slides').current;
                progress(current, positions);
             //   uploadPicturesThread();
            }
        }, delay);
    }


    $.ajax({
        url:'../../getBanner.json',
        type:'POST',
        dataType:'json',
        data:{
            'type':'right'
        },
        success:function (result) {
            var rightBanner = $('.banner-right').attr('src', '..' + GLOBAL.rightUrl);
            if (result.avaliable == "true") {
                $(rightBanner).show();
            } else {
                $(rightBanner).hide();
            }
        }
    });

    $.ajax({
        url:'../../getBanner.json',
        type:'POST',
        dataType:'json',
        data:{
            'type':'left'
        },
        success:function (result) {
            var leftBanner = $('.banner-left').attr('src', '..' + GLOBAL.leftUrl);
            if (result.avaliable == "true") {
                $(leftBanner).show();
            } else {
                $(leftBanner).hide();
            }
        }
    });

    function progress(current, positions) {

        if (current < total) {
            var previous;
            for(var i in positions) {
                if (positions[i] < current) {
                    previous = positions[i];
                } else {
                    break;
                }
            }

            console.log("positions: " + positions);
            console.log("current:" + current);
            console.log("previous:" + previous);
            console.log("$.getUploaded():" + $.getUploaded());
            console.log("mextUploaded:" + nextIndexForUpload);

            var position = $.inArray(previous, $.getUploaded()), flag = (position != -1);
            if ($.inArray(current, positions) != -1) {
                console.log("flag -- " + flag);
                console.log($('li.slide').size());

                if(previous==undefined){
                    flag=true;
                }

                if($('li.slide').size()==total){

                    $.addUploaded(current);
                }

                if(!flag){
                   console.log("ALERT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                }
                var startIndex = flag && current != positions[0] ?(GLOBAL.slideCount == 1 ? nextIndexForUpload + 8 : nextIndexForUpload + 24):nextIndexForUpload;


                console.log("startIndex:" + startIndex);
                if (startIndex < total) {
                    $.uploadPictures(
                            $.extend(PARAM, GLOBAL, {
                                start:startIndex,
                                uploaded : flag ? current : previous
                            })
                    );
                }
                if (GLOBAL.slideCount == 1) {
                    nextIndexForUpload += 8;
                } else {
                    nextIndexForUpload += 24;
                }
            }
        }

    }

    function uploadFirstPartPictures(slideShow, isUpdate) {
        $.ajax({
            url: '../../getPicturesForSlideShow.json',
            data: PARAM,
            dataType: 'json',
            success: function(result, status) {
                if(result!= undefined || result != null) {
                    var pictures = result.pictures;
                    total = result.total;
                    $.populate({
                        pictures: pictures,
                        selector: ".carousel",
                        filters: {
                            slideCount : GLOBAL.slideCount,
                            userId: GLOBAL.userId
                        }
                    });

                    var query = 'img[class="picture' + (GLOBAL.slideCount == 1 ? '-single' : '') + '"]';
                    $.each($('li.slide').find(query), function(index, el) {
                        if ($(this).attr('selectedid') == "true") {
                            $(this).css('background', 'none repeat scroll 0 0 #F28C29');
                        }
                    });

                    var i;
                    if (total > 8) {
                        nextIndexForUpload = 0;
                        if (GLOBAL.slideCount == 1) {
                            for(i = 3; i < total; i += 8) {
                                positions.push(i);
                            }
                        } else {
                            for(i = 4; i < Math.ceil(total/4); i += 6) {
                                positions.push(i);
                            }
                        }
                    }

                    slideShow = $('.slideshow').slides().data('slides');

                    if (isUpdate) {
                        slideShow.upsert();
                    }

                    $.initSlider(slideShow);

                    slideShow.stop();

                    $.unbindAndBindPictureClick();

                    $('.slides-prev').text('');
                    $('.slides-next').text('');

                    $('li.slide').bind('touchend', function() {
                        var slider = $('.slideshow').data('slides'),
                            current = slider.current,
                            last = GLOBAL.slideCount == 1 ? total - 1 : Math.ceil(total/4) - 1;
                        progress(current, positions);
                        if (current == last) {
                            restart(slider);
                        }
                    });

                    $('.slides-next').unbind('click');
                    $('.slides-next').click(function() {
                        if ($('.btn-play').text() == "Pause") {
                            $('.btn-play').text("Play");
                            isPlay=false;
                        }
                        var slider = $('.slideshow').data('slides'),
                            current = slider.current,
                            last = GLOBAL.slideCount == 1 ? total - 1 : Math.ceil(total/4) - 1;
                        progress(current, positions);
                        if (current == last) {
                            restart(slider);
                        }
                    });

                    $('.slides-prev').click(function() {
                        if ($('.btn-play').text() == "Pause") {
                            $('.btn-play').text("Play");
                            isPlay=false;
                        }
                    });

                }
            }
        });
    }

    function getSlideSpeed() {
        return GLOBAL.slideSpeed == null ? 12000 : (60 / GLOBAL.slideSpeed * 1000);
    }

    function restart(slider) {
        slider.stop();
        $('.slideshow').find('li.slide').remove();
        nextIndexForUpload = 0;
        positions = [];
        $.emptyUploaded();
        slider.current = 0;
        slider.future = 0;
        PARAM = $.extend(PARAM, {start: 0});
        uploadFirstPartPictures(slider, true);
        setTimeout(function() {
            slider.stop();
            slider.play();
            setTimeout(function(){
                if ($('.btn-play').text() == "Play") {
                    slider.stop();
                } else {
                    slider.play();
                }
            }, getSlideSpeed());
        }, 1100);
    }

    $(document).ready(function () {

        $('#field_for_play').click(function() {
            var slider = $('.slideshow').data('slides'),
                current = slider.current,
                last = GLOBAL.slideCount == 1 ? total - 1 : Math.ceil(total/4) - 1;
            progress(current, positions);
            if (current == undefined) {
                restart(slider);
            }
            if (current == last) {
                slider._init(true);
                restart(slider);
            }
        });

        $.setSelectedIds(selectedIds);

        $.initParams(PARAM);

        $('.slideshow').attr('data-auto', getSlideSpeed());

        uploadFirstPartPictures(slideShow);

        $('.btn-refresh').click(function () {
            var slider = $('.slideshow').data('slides');
            if (slider == undefined || slider == null) {
                return;
            }
            restart(slider);
        });

        $('.btn-select-deselect').click(function () {
            selected = !selected;
            $.ajax({
                url:'../../selectAllSlideshow.json',
                dataType:'json',
                data:PARAM,
                success:function (result) {
                    selectedIds = result.ids;
                    $.unselectedPictures();
                }
            });

        });

        $('.btn-purchase').click(function () {
            var query = 'img[class="picture' + (GLOBAL.slideCount == 1 ? '-single' : '') + '"]',
                    imageIds = [];
            $.each($('li.slide').find(query), function (index, el) {
                if ($(this).attr("selectedid") == "true") {
                    imageIds.push($(this).attr('imageid'));
                }
            });
            var lo = Ext.urlAppend(GLOBAL.contextPath + '/pictures/main/helpyourself', Ext.urlEncode({ids:imageIds} || {}));
            if (imageIds.length > 0) {
                var win = window.open(
                        lo,
                        'helpYourSelf' + new Date().getMilliseconds(),
                        'scrollbars=yes,menubar=no,height=850,width=1200,resizable=yes,toolbar=no,location=no,status=no'
                );
                win.moveTo(10, 10);
            } else {
                Ext.MessageBox.alert('Notice', 'Select pictures to purchase');
            }
        });

        $.unselectedPictures = function () {
            var query = 'img[class="picture' + (GLOBAL.slideCount == 1 ? '-single' : '') + '"]';
            if (selected) {
                $.each($('li.slide').find(query), function (index, el) {
                    $(this).attr("selectedid", "true");
                    $(this).css('background', 'none repeat scroll 0 0 #F28C29');
                });
            } else {
                $.each($('li.slide').find(query), function (index, el) {
                    $(this).attr("selectedid", "false");
                    $(this).css('background', 'none repeat scroll 0 0 #FFFFFF');
                });
                selectedIds = [];
            }
        };

        $('.btn-play').click(function () {
            var me = $(this);
            if (me.text() == "Play") {
                isPlay = true;
                me.text("Pause");
                $('.slideshow').data('slides').play();
                PlayLoader.start();
            } else {
                isPlay = false;
                me.text("Play");
                $('.slideshow').data('slides').pause();
                PlayLoader.stop();
            }
        });

    });

    </script>

</head>
<body>

<input type="hidden" id="field_for_play">

<div id="container">

    <div class="banner-left-side">
        <img class="banner-left"/>
    </div>

    <div class="banner-right-side">
        <img class="banner-right"/>
    </div>

    <div class="slideshow" style="position:relative;overflow:hidden;" data-visible="1" data-pagination="false"
         data-offset="1" data-transition="scroll" data-gestures="true">
        <ul class="carousel">

        </ul>
    </div>
    <div class="clear"></div>

</div>

<div class="navigation">
    <button class="btn-purchase">Purchase</button>
    <button class="btn-play">Play</button>
    <button class="btn-refresh">Refresh</button>
    <button class="btn-select-deselect">Select/Deselect</button>
</div>

</body>
</html>