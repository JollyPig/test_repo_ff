<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Fotaflo RFID Slideshow</title>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/handleTimeout.js"></script>
    <script type="text/javascript">
        var GLOBAL = {
            userId: ${userId},
            contextPath: "<%= request.getContextPath()%>",
            slideSpeed:${slidespeed},
            slideCount:${slidecount},
            leftUrl:"${leftUrl}",
            rightUrl:"${rightUrl}"
        };

        GLOBAL.slideshowProxyConfig = {
            api: {
                read :    '../rfid/getPictures.json',
                create :  '../rfid/getPicturesCreate.json',  // Server MUST return idProperty of new record
                update :  '../rfid/getPicturesUpdate.json',
                destroy : '../rfid/getPicturesDelete.json'
            },
            listeners: {
                'beforeload': function(proxy, params){
                    console.log(proxy, params);
                    proxy.onRead = function(action, o, response) {
                        var result;
                        try {
                            result = o.reader.read(response);
                        }catch(e){
                            this.fireEvent('loadexception', this, o, response, e);

                            this.fireEvent('exception', this, 'response', action, o, response, e);
                            o.request.callback.call(o.request.scope, null, o.request.arg, false);
                            return;
                        }
                        if (result.success === false) {
                            this.fireEvent('loadexception', this, o, response);

                            // Get DataReader read-back a response-object to pass along to exception event
                            var res = o.reader.readResponse(action, response);
                            this.fireEvent('exception', this, 'remote', action, o, res, null);
                        }
                        else {
                            this.fireEvent('load', this, o, o.request.arg);
                        }

                        var proceedRequest = function(records, o){
                            var rs = records,
                                    result = {
                                        records: [],
                                        totalRecords: 0,
                                        success: true
                                    },
                                    res = [],
                                    start = o.params.start,
                                    limit = o.params.limit;
                            if(start >= 0 && limit > 0 && rs){
                                result.totalRecords = rs.length;
                                for(var i = start; i < start + limit && i < rs.length; i++){
                                    res.push(rs[i]);
                                }
                                result.records = res;
                            }

                            o.request.callback.call(o.request.scope, result, o.request.arg, result.success);
                        }

                        // ovirriden part
                        if(!this.localData){
                            this.localData = result.records;
                            this.doRequest = function(action, rs, params, reader, cb, scope, arg) {
                                var  o = {
                                    method: (this.api[action]) ? this.api[action]['method'] : undefined,
                                    request: {
                                        callback : cb,
                                        scope : scope,
                                        arg : arg
                                    },
                                    reader: reader,
                                    callback : this.createCallback(action, rs),
                                    scope: this
                                };

                                if (params.jsonData) {
                                    o.jsonData = params.jsonData;
                                } else if (params.xmlData) {
                                    o.xmlData = params.xmlData;
                                } else {
                                    o.params = params || {};
                                }

                                proceedRequest.call(this, this.localData, o);
                            }
                        }

                        proceedRequest.call(this, this.localData, o);
                    }

                }
            }
        }
    </script>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/ext-3.2.1/resources/css/ext-all.css"/>
    <script src="<%= request.getContextPath()%>/ext-3.2.1/adapter/ext/ext-base.js" type="text/javascript"></script>
    <script src="<%= request.getContextPath()%>/ext-3.2.1/ext-all.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/js/ux/data-view.css"/>
    <script src="<%= request.getContextPath()%>/js/slideshow.js"></script>
</head>

<body>
<div id="content" align="center"></div>
<script src="<%= request.getContextPath()%>/js/slideshowlayout.js"></script>

</body>
</html>