<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Fotaflo Preview</title>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/ext-3.2.1/resources/css/ext-all.css"/>
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/js/ux/data-view.css"/>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/handleTimeout.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/ext-3.2.1/adapter/ext/ext-base.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/ext-3.2.1/ext-all.js"></script>
</head>
<body>

<div id="wrapper" class="wrapper_preview">
 <script type="text/javascript">

    var pictures = ${picturesToPrint};
    var sizeL = ${logoLocation};
    var sizeMainL = ${mainLogoLocation};

    Ext.onReady(function() {
        var html = '<div class ="preview_picture" id="watermark_box">' +
                '<img class="preview_picture_img" src="../{url}">' +
                '<div class="preview_picture_watermark_text">{logotext}</div>' +
                '<tpl if="logoUrl!=\'\'">' +
                '<img src="..{logoUrl}" width="{sizeLogo}" class="preview_picture_watermark_logo" alt="" title=""/></tpl>' +
                '<tpl if="logoMainUrl!=\'\'">' +
                '<img src="..{logoMainUrl}" width ="{sizeMainLogo}" class="preview_picture_main_watermark_logo" alt="" title=""/></tpl>' +
                "</div>";


        if (pictures != null)
        {
            var tpl = new Ext.XTemplate(html);
            var start = 0;
            for (start; start < pictures.length; start++) {
                var url1 = pictures[start].url;
                var logotext1 = pictures[start].logoText;
                if (logotext1 == null || logotext1=="null"){
                    logotext1 = "";
                }
                var logoUrl1 = pictures[start].logoUrl;
                var logoMainUrl1 = pictures[start].logoMainUrl;
                    var toPrintNumber = pictures[start].numbersToPrint;
                    var i = 1;
                    for (i; i <= toPrintNumber; i++)
                tpl.append('wrapper', {
                    url: url1,
                    logotext: logotext1,
                    logoUrl: logoUrl1,
                    logoMainUrl:logoMainUrl1,
                    sizeLogo:sizeL,
                    sizeMainLogo:sizeMainL
                });


            }
        }
    });

</script>
</div>

</body>
</html>
