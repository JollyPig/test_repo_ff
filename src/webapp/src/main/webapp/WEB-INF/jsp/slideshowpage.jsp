<%--
  Created by MilkevichP
  Date: 19.04.11
  Time: 15:30
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Fotaflo Slideshow</title>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/handleTimeout.js"></script>
    <script type="text/javascript">
        var GLOBAL = {
            userId: ${userId},
            contextPath: "<%= request.getContextPath()%>",
            slideSpeed:${slidespeed},
            slideCount:${slidecount},
            leftUrl:"${leftUrl}",
            rightUrl:"${rightUrl}",
            slideShowInNewWindow: ${slideShowInNewWindow}
        };

        GLOBAL.slideshowProxyConfig = {
            api: {
                read :    '../../getPictures.json',
                create :  '../../getPicturesCreate.json',  // Server MUST return idProperty of new record
                update :  '../../getPicturesUpdate.json',
                destroy : '../../getPicturesDelete.json'
            }
        }
    </script>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/ext-3.2.1/resources/css/ext-all.css"/>
    <script src="<%= request.getContextPath()%>/ext-3.2.1/adapter/ext/ext-base.js" type="text/javascript"></script>
    <script src="<%= request.getContextPath()%>/ext-3.2.1/ext-all.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/js/ux/data-view.css"/>
    <script src="<%= request.getContextPath()%>/js/slideshow.js"></script>
</head>

<body>
<div id="content" align="center"></div>
<script src="<%= request.getContextPath()%>/js/slideshowlayout.js"></script>

</body>
</html>
