<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <script src="<%= request.getContextPath()%>/js/menu.js"></script>
</head>
<body>

<div class="logo-login"> Hello, ${current}!
    <a id="logout" href="<%= request.getContextPath()%>/pictures/auth/logout"
       class="logout_link">Logout <img src="<%= request.getContextPath()%>/css/images/logout.png" alt=""></a>
</div>

<div id="menu" class="menu">

</div>
</body>
</html>