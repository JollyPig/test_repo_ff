<%--
  Created by MilkevichP
  Date: 22.04.11
  Time: 14:14
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>Fotaflo Locations</title>

    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/ext-3.2.1/resources/css/ext-all.css"/>
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/js/ux/data-view.css"/>

    <script type="text/javascript" src="<%= request.getContextPath()%>/js/handleTimeout.js"></script>
    <script src="<%= request.getContextPath()%>/ext-3.2.1/adapter/ext/ext-base.js" type="text/javascript"></script>
    <script src="<%= request.getContextPath()%>/ext-3.2.1/ext-all.js" type="text/javascript"></script>

    <script type="text/javascript">
        var GLOBAL = {
            userId: ${userId},
            contextPath:"<%= request.getContextPath()%>",
            tagApp: '${tagApp}'
        };
    </script>
</head>
<body>

<div id="wrapper" class="wrapper">

    <div id="header" class="headerpage">
        <div class="logo-img"></div>
        <jsp:include page="menu.jsp"/>
    </div>
    <!-- #header-->

    <div id="content" class="content"></div>
    <!-- #content-->

</div>
<!-- #wrapper -->

<div class="footer">
    <jsp:include page="copyright.jsp"/>
</div>
<!-- #footer -->

</body>

<script type="text/javascript">

Ext.onReady(function () {

    // shorthand alias
    var fm = Ext.form;

    // the column model has information about grid columns
    // dataIndex maps the column to the specific data field in
    // the data store (created below)
    var numberComboStore = new Ext.data.ArrayStore({
        id: 0,
        fields: [
            'idN',
            'countN'
        ],
        data: [
            [0, 'reduced(1200x900)'],
            [1, 'normal(1600x1200)'],
            [2, '3.2MP(2048x1536)'],
            [3, '5.0MP(2560x1920)']
        ]
    });


 /*   Ext.util.Format.comboRenderer = function(combo) {
        return function(value) {
            var record = combo.findRecord(combo.valueField, value);
            return record ? record.get(combo.displayField) : '0';
        }
    }*/
    var numberCombo = new Ext.form.ComboBox({
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
        store: numberComboStore,
        valueField: 'idN',
        displayField: 'countN',
        width: 40,
        maxHeight: 100,
        shadow : false
    });

    var selModel = new Ext.grid.CellSelectionModel({
        listeners:{
            selectionchange:function (selectionModel, selection) {
                var flag = Ext.isEmpty(selection);
                grid.deleteButton.setDisabled(flag);
                grid.editButton.setDisabled(flag);
            }
        }
    });

    var checkboxColumnRenderer = function (value, metaData, record, rowIndex, colIndex, store) {
        if (value) {
            return '<input class="x-row-checkbox" style="height: 12px;" type="checkbox" checked="checked" />';
        }
        else {
            return '<input class="x-row-checkbox" style="height: 12px;" type="checkbox" />';
        }
    };


    var cm = new Ext.grid.ColumnModel({
        // specify any defaults for each column
        defaults:{
            menuDisabled: true,
            sortable: false // columns are not sortable by default
        },
        columns:[
            {
                header:'Location Name',
                dataIndex:'locationName',
                width: 270,
                // use shorthand alias defined above
                editor:new fm.TextField({
                    allowBlank:false
                })
            },
            {
                header:'Reduced public sale images',
                dataIndex:'resolution',
                width: 120,
                editor: numberCombo,
                renderer:function (value, metaData, record, rowIndex, colIndex, store) {
                    if(value=="" && value!=0){
                        numberCombo.setValue(1);
                    } else{
                        numberCombo.setValue(value);
                    }
                    return numberCombo.lastSelectionText;
                }
            },
            {
                header:'Enable tag',
                dataIndex:'tagged',
                align: 'center',
                width: 50,
                renderer: checkboxColumnRenderer
            },
            {
                header:'Bcc only',
                dataIndex:'bcconly',
                align: 'center',
                width: 50,
                renderer: checkboxColumnRenderer
            },
            {
                header:'Give away tags',
                dataIndex:'giveaway',
                align: 'center',
                width: 70,
                renderer: checkboxColumnRenderer
            },
            {
                header:'FWP enable',
                dataIndex:'fwpenable',
                align: 'center',
                width: 60,
                renderer: checkboxColumnRenderer
            },
            {
                header:'Enable Tags for slideshow',
                dataIndex:'rfid',
                align: 'center',
                width: 110,
                renderer: checkboxColumnRenderer
            },
            {
                header:'Enable Web Camera',
                dataIndex:'camera',
                align: 'center',
                width: 90,
                renderer: checkboxColumnRenderer
            },
            {
                header:'Photo Book',
                dataIndex:'photobook',
                align: 'center',
                width: 60,
                renderer: checkboxColumnRenderer
            },
            {
                header:'Staff save',
                dataIndex:'staffsave',
                align: 'center',
                width: 65,
                renderer: checkboxColumnRenderer
            },
            {
                header: 'Open slide show in a new window',
                dataIndex: 'slideShowInNewWindow',
                align: 'center',
                width: 150,
                renderer: checkboxColumnRenderer
            }
        ]
    });

    // create the Data Store
    var store = new Ext.data.Store({
        autoDestroy:true,
        autoSave:false,
        autoLoad:false,

        proxy:new Ext.data.HttpProxy({
            api:{
                read:'../../locations/getRealLocations.json',
                create:'../../locations/saveLocations.json',
                update:'../../locations/saveLocations.json',
                destroy:'../../locations/deleteLocations.json'
            }
        }),


        reader:new Ext.data.JsonReader({
            root:'locations',
            idProperty:'id',
            totalProperty:'total',
            fields:[
                {name:'id', type:'string'},
                {name:'locationName', type:'string'},
                {name:'reduced', type:'boolean'},
                {name:'resolution', type:'string'},
                {name:'tagged', type:'boolean'},
                {name:'bcconly', type:'boolean'},
                {name:'giveaway', type:'boolean'},
                {name:'fwpenable', type:'boolean'},
                {name:'rfid', type:'boolean'},
                {name:'camera', type:'boolean'},
                {name:'photobook', type:'boolean'},
                {name:'staffsave', type:'boolean'},
                {name: 'slideShowInNewWindow', type: 'boolean'}
            ]
        }),

        writer:new Ext.data.JsonWriter({
            encode:true,
            writeAllFields:true
        }),

        listeners:{
            save:function (store) {
                pagingToolBar.doRefresh();
            }
        },

        sortInfo:{field:'locationName', direction:'ASC'}
    });

    var pagingToolBar = new Ext.PagingToolbar({
        pageSize:25,
        store:store,
        displayInfo:true,
        displayMsg:'Displaying locations {0} - {1} of {2}',
        emptyMsg:"No locations to display"
    });
    pagingToolBar.refresh.hide();

    // create the editor grid
    var grid = new Ext.grid.EditorGridPanel({
        id:'locationGrid',
        store:store,
        cm:cm,
        width:1350,
        height:600,
        style:'margin: 0 auto;',
        autoExpandColumn:'locationName',
        title:'Locations',
        frame:true,
        selModel:selModel,
        viewConfig:{
            forceFit:true,
            getRowClass:function (record, index) {
                var a = index % 2;
                if (a > 0) {
                    return 'selected-class';
                }
            }
        },
        tbar:[
            {
                text:'Add Location',
                ref:'../addButton',
                handler:function () {
                    // access the Record constructor through the grid's store
                    grid.saveButton.setDisabled(false);
                    grid.cancelButton.setDisabled(false);
                    var Location = grid.getStore().recordType;
                    var l = new Location({
                        locationName:'New Location'
                    });
                    grid.stopEditing();
                    grid.getStore().insert(0, l);
                    grid.startEditing(0, 0);
                    grid.addButton.setDisabled(true);
                    grid.deleteButton.setDisabled(true);
                    grid.editButton.setDisabled(true);
                }
            },
            {
                text:'Edit Location',
                disabled:true,
                ref:'../editButton',
                handler:function () {
                    grid.saveButton.setDisabled(false);
                    grid.cancelButton.setDisabled(false);
                    grid.stopEditing();
                    var sc = grid.getSelectionModel().getSelectedCell();
                    if (sc)
                        grid.startEditing(sc[0], sc[1]);
                    grid.addButton.setDisabled(true);
                    grid.deleteButton.setDisabled(true);
                    grid.editButton.setDisabled(true);
                }
            },
            {
                text:'Delete Location',
                disabled:true,
                ref:'../deleteButton',
                handler:function () {
                    var msgWarn = "Please make sure all the users, packages, cameras and staff is deleted for this location. If you delete the location all public packages and related information, public statistic, report emails, requests, subscribing emails will also be deleted for this location. Do you really want to delete this location?";
                    Ext.Msg.confirm("Warning", msgWarn, function (btn) {
                        if (btn == 'yes') {
                            grid.stopEditing();
                            var store = grid.getStore();
                            var sc = grid.getSelectionModel().getSelectedCell();
                            if (sc) {
                                store.removeAt(sc[0]);
                                store.save();
                                grid.getView().refresh();
                            }
                        }
                    });
                }
            }

        ],
        bbar:pagingToolBar,
        fbar:[
            {
                text:'Save',
                disabled:true,
                ref:'../saveButton',
                handler:function () {
                    store.save();
                    grid.getView().refresh();
                    grid.addButton.setDisabled(false);
                    grid.saveButton.setDisabled(true);
                    grid.cancelButton.setDisabled(true);
                }
            },
            {
                text:'Cancel',
                disabled:true,
                ref:'../cancelButton',
                handler:function () {
                    store.rejectChanges();
                    pagingToolBar.doRefresh();
                    grid.getView().refresh();
                    grid.addButton.setDisabled(false);
                    grid.saveButton.setDisabled(true);
                    grid.cancelButton.setDisabled(true);
                }
            }
        ],
        listeners:{
            keydown:function (e) {
                grid.stopEditing();
                store.rejectChanges();
                store.reload();
                grid.addButton.setDisabled(false);
                grid.saveButton.setDisabled(true);
                grid.cancelButton.setDisabled(true);
            },
            keypress:function (e) {
                grid.stopEditing();
                store.rejectChanges();
                store.reload();
                grid.addButton.setDisabled(false);
                grid.saveButton.setDisabled(true);
                grid.cancelButton.setDisabled(true);
            },
            cellclick:function (it, rowIndex, columnIndex, e) {
                if (columnIndex > 0) {
                    var store = it.getStore(),
                        colModel = it.getColumnModel();
                    var record = store.getAt(rowIndex),
                        field = colModel.getDataIndex(columnIndex);

                    if(field){
                        var value = record.get(field);
                        if(Ext.isBoolean(value)){
                            record.set(field, !value);
                        }
                    }else{
                        throw "'dataIndex' is empty"
                    }

                    grid.saveButton.setDisabled(false);
                }
            }
        }
    });
    grid.on({
        'celldblclick':function (grid, rowIndex, columnIndex, e) {
            if (columnIndex == 0) {
                grid.stopEditing();
                store.rejectChanges();
            }
        },
        scope:this
    });


    store.load({
        params:{
            start:0,
            limit:25
        }
    });
    grid.render('content');
});

</script>

</html>
