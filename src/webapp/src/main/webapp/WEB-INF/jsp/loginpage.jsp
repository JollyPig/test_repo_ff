<%@ page session="true" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <link href="<%= request.getContextPath()%>/css/style.css" rel="stylesheet" type="text/css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Fotaflo Login</title>

</head>

<body>

<div id="wrapper" class="wrapper">

    <div id="header" class="headerpage">
        <div class="logo-img"></div>
    </div>
    <!-- #header-->

    <%--<div id="content" class="content">--%>
    <div id="content" class="content-login-form">
        <div class="loginform">

            <div style="text-align: center;">
                <div id="login-error" class="errormessage">${error}</div>
            </div>

            <div>


               <%-- <div style="text-align: center;" >
                    <p style="font-size: 55px;font-family:Times New Roman, Times, serif;">You are using the old Fotaflo system</p>
                    <p style="font-size: 35px;font-family:Times New Roman, Times, serif">Please contact us at <a href=""> info@fotaflo.com</a> if you haven't switched to the new system</p>
                </div>--%>


                <div class="loginform-note">
                    <label>Please enter username and password or use tag as username and password</label>
                </div>

                <form action="../../j_spring_security_check" method="post">

                    <div style="padding: 5px; text-align: center;">
                        <label for="j_username">Username</label>
                        <input id="j_username" name="j_username" type="text" class="logininput"/>
                    </div>

                    <div style="padding: 5px; text-align: center; margin-left: 1px;">
                        <label for="j_password">Password</label>
                        <input id="j_password" name="j_password" type="password" class="logininput" style="margin-left: 1px;"/>
                    </div>

                    <div style="padding: 5px; text-align: center; margin-left: -32px;">
                        <input type="submit" value="Log In" class="mainbutton"/>
                    </div>

                </form>

                <div class="loginform-note">
                    <label>If you are experiencing difficulties logging in please contact info@fotaflo.com</label>
                </div>

            </div>

        </div>

    </div>
    <!-- #content-->

</div>
<!-- #wrapper -->

<div class="footer">
    <jsp:include page="copyright.jsp"/>
</div>
<!-- #footer -->
</body>
</html>
