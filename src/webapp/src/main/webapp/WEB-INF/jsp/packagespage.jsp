<%--
  Created by MilkevichP
  Date: 22.04.11
  Time: 14:14
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>Fotaflo Packages</title>
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/ext-3.2.1/resources/css/ext-all.css"/>  
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath()%>/js/ux/data-view.css"/>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/handleTimeout.js"></script>
    <script src="<%= request.getContextPath()%>/ext-3.2.1/adapter/ext/ext-base.js" type="text/javascript"></script>
    <script src="<%= request.getContextPath()%>/ext-3.2.1/ext-all.js" type="text/javascript"></script>
    <script type="text/javascript">
        var GLOBAL = {
            userId: ${userId},
            contextPath: "<%= request.getContextPath()%>",
            tagApp: '${tagApp}'
        };
    </script>
</head>
<body>

<div id="wrapper" class="wrapper">

<div id="header" class="headerpage">
    <div class="logo-img"></div>
    <jsp:include page="menu.jsp"/>
</div>
<!-- #header-->

<div id="content" class="content"></div>
<!-- #content-->
<script type="text/javascript">

Ext.onReady(function() {

    // shorthand alias
    var fm = Ext.form;

    var comboStore = new Ext.data.Store({

        autoDestroy: true,
        autoLoad: true,
        autoSave: false,

        proxy: new Ext.data.HttpProxy({
            url: '../../locations/getRealLocations.json'
        }),


        reader: new Ext.data.JsonReader({
            root: 'locations',
            idProperty: 'id',
            fields: [
                {name: 'id', type: 'string'},
                {name: 'locationName', type: 'string'}
            ]
        }),

        sortInfo: {field:'locationName', direction:'ASC'}
    });

    var locationsCombo = new fm.ComboBox({
        xtype: 'combo',
        mode: 'remote',
        store: comboStore,
        fieldLabel: 'Location',
        displayField: 'locationName',
        valueField: 'id',
        typeAhead: true,
        forceSelection: true,
        triggerAction: 'all',
        selectOnFocus: true,
        editable: true,
        emptyText: 'Select location...',
        listeners: {
            select: function(combo, record, index) {
                var data = record.get('id');
                grid.getStore().load({
                    params:{
                        locationId: data,
                        start: 0,
                        limit: 25
                    }
                });
                grid.addButton.setDisabled(false);
            }
        }
    });

    // the column model has information about grid columns
    // dataIndex maps the column to the specific data field in
    // the data store (created below)
    var cm = new Ext.grid.ColumnModel({
        // specify any defaults for each column
        defaults: {
            sortable: true // columns are not sortable by default
        },
        columns: [
            {
                id: 'packageName',
                header: 'Package Name',
                dataIndex: 'packageName',
                menuDisabled: true,
                width: 220,
                // use shorthand alias defined above
                editor: new fm.TextField({
                    allowBlank: false
                })
            }
        ]
    });

    // create the Data Store
    var store = new Ext.data.Store({

        autoDestroy: true,
        autoLoad: false,
        autoSave: false,

        proxy: new Ext.data.HttpProxy({
            api: {
                read :    '../../packages/getPackages.json',
                create :  '../../packages/savePackages.json',
                update :  '../../packages/savePackages.json',
                destroy : '../../packages/deletePackages.json'
            },
            listeners: {
                beforeload: function(dataProxy, params) {
                    params = Ext.apply(params || {}, {
                        locationId: locationsCombo.getValue()
                    });
                },
                beforewrite: function(dataProxy, action, rs, params) {
                    params = Ext.apply(params || {}, {
                        locationId: locationsCombo.getValue()
                    });
                }
            }
        }),


        reader: new Ext.data.JsonReader({
            root: 'packages',
            idProperty: 'id',
            totalProperty: 'total',
            fields: [
                {name: 'id', type: 'string'},
                {name: 'packageName', type: 'string'}
            ]
        }),

        writer: new Ext.data.JsonWriter({
            encode: true,
            writeAllFields: true
        }),
        listeners: {
            save: function(store) {
                pagingToolBar.doRefresh();
            }
        },

        sortInfo: {field:'packageName', direction:'ASC'}
    });

    var pagingToolBar = new Ext.PagingToolbar({
        pageSize: 25,
        store: store,
        displayInfo: true,
        displayMsg: 'Displaying packages {0} - {1} of {2}',
        emptyMsg: "No packages to display"
    });

    pagingToolBar.refresh.hide();

    // create the editor grid
    var grid = new Ext.grid.EditorGridPanel({
        store: store,
        id: 'packagesGrid',
        cm: cm,
        width: 600,
        height: 600,
        style: 'margin: 0 auto;',
        autoExpandColumn: 'packageName',
        title: 'Packages',
        frame: true,
        sm: new Ext.grid.CellSelectionModel({
            listeners: {
                selectionchange : function(selectionModel, selection) {
                    var flag = Ext.isEmpty(selection);
                    grid.deleteButton.setDisabled(flag);
                    grid.editButton.setDisabled(flag);
                }
            }
        }),
        viewConfig: {
            forceFit: true,
            getRowClass: function(record, index) {
                var a = index % 2;
                if (a > 0) {
                    return 'selected-class';
                }
            }
        },
        tbar: [
            locationsCombo,
            {
                text: 'Add Package',
                disabled: true,
                ref: '../addButton',
                handler : function() {
                    grid.saveButton.setDisabled(false);
                    grid.cancelButton.setDisabled(false);
                    // access the Record constructor through the grid's store
                    var Package = grid.getStore().recordType;
                    var l = new Package({
                        packageName: 'New Package'
                    });
                    grid.stopEditing();
                    grid.getStore().insert(0, l);
                    grid.startEditing(0, 0);
                    grid.addButton.setDisabled(true);
                    grid.deleteButton.setDisabled(true);
                    grid.editButton.setDisabled(true);
                }
            },
            {
                text: 'Edit Package',
                disabled: true,
                ref: '../editButton',
                handler : function() {
                    grid.saveButton.setDisabled(false);
                    grid.cancelButton.setDisabled(false);
                    grid.stopEditing();
                    var sc = grid.getSelectionModel().getSelectedCell();
                    if (sc)
                        grid.startEditing(sc[0], sc[1]);
                    grid.addButton.setDisabled(true);
                    grid.deleteButton.setDisabled(true);
                    grid.editButton.setDisabled(true);
                }
            },
            {
                text: 'Delete Package',
                disabled: true,
                ref: '../deleteButton',
                handler : function() {
                    Ext.Msg.confirm("Warning", "The statistic and public statistic will also be deleted for this package. Do you really want to delete this package?", function(btn) {
                        if (btn == 'yes') {
                            grid.stopEditing();
                            var store = grid.getStore();
                            var sc = grid.getSelectionModel().getSelectedCell();
                            if (sc) {
                                store.removeAt(sc[0]);
                                store.save();
                                grid.getView().refresh();
                            }
                        }
                    });

                }
            }

        ],
        bbar: pagingToolBar,
        fbar: [
            {
                text: 'Save',
                disabled: true,
                ref: '../saveButton',
                handler: function() {
                    store.save();
                    grid.getView().refresh();
                    grid.addButton.setDisabled(false);
                    grid.saveButton.setDisabled(true);
                    grid.cancelButton.setDisabled(true);
                }
            },
            {
                text: 'Cancel',
                disabled: true,
                ref: '../cancelButton',
                handler: function() {
                    store.rejectChanges();
                    pagingToolBar.doRefresh();
                    grid.getView().refresh();
                    grid.addButton.setDisabled(false);
                    grid.saveButton.setDisabled(true);
                    grid.cancelButton.setDisabled(true);
                }
            }
        ],
        listeners: {
            keydown : function(e) {
                grid.stopEditing();
                store.rejectChanges();
                store.reload();
                grid.addButton.setDisabled(false);
                grid.saveButton.setDisabled(true);
                grid.cancelButton.setDisabled(true);
            },
            keypress : function(e) {
                grid.stopEditing();
                store.rejectChanges();
                store.reload();
                grid.addButton.setDisabled(false);
                grid.saveButton.setDisabled(true);
                grid.cancelButton.setDisabled(true);
            }
        }
    });
    grid.on({
        'celldblclick' : function(grid, rowIndex, columnIndex, e) {
            if (columnIndex == 0)
            {
                grid.stopEditing();
                store.rejectChanges();
            }
        },
        scope: this
    });

    grid.render('content');

});

</script>
</div>
<!-- #wrapper -->

<div class="footer"> <jsp:include page="copyright.jsp"/></div>
<!-- #footer -->
</body>
</html>
