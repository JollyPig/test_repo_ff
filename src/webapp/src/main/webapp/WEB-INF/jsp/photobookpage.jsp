<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <link href="<%= request.getContextPath()%>/css/style.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css"
          href="<%= request.getContextPath()%>/ext-3.2.1/resources/css/ext-all.css"/>
    <link rel="stylesheet" type="text/css"
          href="<%= request.getContextPath()%>/js/ux/data-view.css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Fotaflo Purchase Photo Book</title>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/handleTimeout.js"></script>
    <script type="text/javascript">
        var GLOBAL = {
            pageSize: ${pageSize},
            userId: ${userId},
            contextPath: "<%= request.getContextPath()%>",
            location: "${location}"
        };
    </script>
    <!-- ExtJS js -->
    <script type="text/javascript" src="<%= request.getContextPath()%>/ext-3.2.1/adapter/ext/ext-base.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/ext-3.2.1/ext-all.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/Ext.ux.util.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/lovcombo.js"></script>
</head>

<body>

<div id="wrapper" class="wrapper">

    <div id="header" class="headerpage">
        <div class="logo-img"></div>
        <jsp:include page="menu.jsp"/>
    </div>

    <div id="content" class="content">
        <!-- ExtJS css -->
        <!-- App js -->
        <script src="<%= request.getContextPath()%>/js/ux/photobook.js"></script>


    </div>
    <!-- #content-->

</div>
<!-- #wrapper -->

<div class="footer"> <jsp:include page="copyright.jsp"/></div>
<!-- #footer -->

</body>
</html>
