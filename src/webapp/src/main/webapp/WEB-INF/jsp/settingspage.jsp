<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <link href="<%= request.getContextPath()%>/css/style.css" rel="stylesheet" type="text/css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script type="text/javascript" src="<%= request.getContextPath()%>/ext-3.2.1/adapter/ext/ext-base.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath()%>/ext-3.2.1/ext-all.js"></script>
    <title>Fotaflo General Settings</title>
    <script type="text/javascript" src="<%= request.getContextPath()%>/js/handleTimeout.js"></script>
    <script type="text/javascript">
        var GLOBAL = {
            userId: ${userId},
            locationId: ${location},
            speed: ${speed != null ? speed : 5},
            picCount: ${picCount != null ? picCount : 1},
            startTime: ${startTime != null ? startTime : 'null'},
            delay: ${delay != null ? delay : 0},
            locationLogo: '${locationLogo}',
            contextPath: '<%= request.getContextPath()%>',
            reduced: '${reduced}',
            tagApp: '${tagApp}'
        };
    </script>
</head>

<body>

<div id="wrapper" class="wrapper">

    <div id="header" class="headerpage">
        <div class="logo-img"></div>
        <jsp:include page="menu.jsp"/>
    </div>
    <!-- #header-->

    <div id="content" class="content">
        <link rel="stylesheet" type="text/css"
              href="<%= request.getContextPath()%>/ext-3.2.1/resources/css/ext-all.css"/>
        <link rel="stylesheet" type="text/css"
              href="<%= request.getContextPath()%>/js/ux/data-view.css"/>

        <%--<div id="login-error">${error}</div>--%>
        <script src="<%= request.getContextPath()%>/js/ux/settings.js"></script>
    </div>
    <!-- #content-->

</div>
<!-- #wrapper -->
<div class="footer"> <jsp:include page="copyright.jsp"/></div>
<!-- #footer -->

</body>
</html>

