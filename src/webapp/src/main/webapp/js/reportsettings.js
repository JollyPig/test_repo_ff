Ext.onReady(function() {

    // whizzywig('bold italic image bullet link fullscreen');

    var reportSettingsPanel = new Ext.Panel({
        layout: 'table',
        id:'mainLogo-view',
        layoutConfig: {
            columns: 2
        },
        items:
                [
                    {
                        xtype: 'field',
                        id: 'reportPath',
                        allowBlank: true,
                        inputType: 'text'
                    },
                    {
                        xtype: 'button',
                        text: 'Save Path',
                        handler: function() {
                            form.getForm().findField('reportPath').allowBlank = true;
                            form.getForm().submit({
                                clientValidation: true,
                                url: '../../reportPathUpdate.json',
                                success: function(form, action) {
                                    Ext.Msg.alert('Success', action.result.msg);
                                },
                                failure: function(form, action) {
                                    form.fileUpload = false;
                                    switch (action.failureType) {
                                        case Ext.form.Action.CLIENT_INVALID:
                                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                            break;
                                        case Ext.form.Action.CONNECT_FAILURE:
                                            Ext.Msg.alert('Failure', 'Server connection problems. Please, try to reload page.');
                                            break;
                                        case Ext.form.Action.SERVER_INVALID:
                                            Ext.Msg.alert('Failure', action.result.msg);
                                    }
                                }
                            });
                        }

                    }

                ],
        listeners: {
            beforerender: function(pn) {
                Ext.Ajax.request({
                    url: '../../getReportPath.json',
                    callback: function(options, success, response) {
                        var path = Ext.util.JSON.decode(response.responseText).reportPath;
                        form.getForm().findField('reportPath').setValue(path);
                    }
                });
            }

        }
    });


    var form = new Ext.form.FormPanel({
        title: 'Settings',

        id: 'banner-view',
        layout: 'table',
        width: '100%',
        layoutConfig: {
            columns: 2
        },
        items: [
            {
                xtype: 'label',
                cls: 'p_labels_bold',
                text: 'Administration Setting Name'
            },
            {
                xtype: 'label',
                cls: 'p_labels_bold',
                text: 'Administration Setting Value'
            },
            {
                xtype: 'panel',
                height: 40,
                padding: '10px 0px 0px 0px',
                items:[
                    {
                        xtype: 'label',
                        cls: 'p_labels',
                        text: 'Report Path'
                    }
                ]
            },
            reportSettingsPanel
        ]
    });


    var fromEmailTextfield = new Ext.form.TextField({
        name: 'fromEmail',
        id: 'fromEmail',
        width: 240,
        allowBlank:false
    });

    var smtpServerTextfield = new Ext.form.TextField({
        xtype: 'textfield',
        name: 'smtpServer',
        id: 'smtpServer',
        width: 240,
        allowBlank:false
    });

    var smtpServerFromPasswordTextfield = new Ext.form.TextField({
        inputType: 'password',
        name: 'smtpServerFromPassword',
        id: 'smtpServerFromPassword',
        width: 240,
        allowBlank:true
    });

    var smtpServerPortTextfield = new Ext.form.TextField({
        xtype: 'textfield',
        name: 'smtpServerPort',
        id: 'smtpServerPort',
        width: 100,
        allowBlank:false
    });

    var subjectTextfield = new Ext.form.TextField({
        xtype: 'textfield',
        name: 'subjectText',
        id: 'subjectText',
        width: 450,
        allowBlank:false
    });

    var sslCheck = new Ext.form.Checkbox(
    {
        name : 'sslcheck',
        id : 'sslcheck',
        checked : false,
        disabled : false
    });
    /**
     * Email Settings
     */
    var serverEmailPanel = new Ext.Panel({
        id: 'serverEmailPanel',
        height   : 'auto',
        layout: 'table',
        layoutConfig: {
            columns: 4
        },
        items: [
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'SMTP Server'
            },
            smtpServerTextfield,
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'SMTP Server Port'
            },
            smtpServerPortTextfield,
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'SMTP Account(From Email)'
            },
            fromEmailTextfield,
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'SMTP Account Password'
            },
            smtpServerFromPasswordTextfield,
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'Subject'
            },
            subjectTextfield,
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'SSL'
            },
            sslCheck,
            /*{
             xtype : 'box',
             width : 20
             },
             {
             xtype : 'box',
             width : 20
             },*/
            {
                xtype: 'button',
                text: 'Save Settings',
                handler: function() {
                    form3.getForm().submit({
                        clientValidation: true,
                        url: '../../reportEmailSettingsSave.json',
                        success: function(form, action) {
                            Ext.Msg.alert('Success', action.result.msg);
                        },
                        failure: function(form, action) {
                            form.fileUpload = false;
                            switch (action.failureType) {
                                case Ext.form.Action.CLIENT_INVALID:
                                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                    break;
                                case Ext.form.Action.CONNECT_FAILURE:
                                    Ext.Msg.alert('Failure', 'Server connection problems. Please, try to reload page.');
                                    break;
                                case Ext.form.Action.SERVER_INVALID:
                                    Ext.Msg.alert('Failure', action.result.msg);
                            }
                        }
                    });
                }

            },
            {
                xtype: 'button',
                text: 'Delete Settings',
                handler: function() {
                    Ext.Msg.confirm('Confirm Delete', 'Are you sure you want to delete settings?', function(btnText) {
                        if (btnText == 'yes') {
                            Ext.Ajax.request({
                                url: '../../reportEmailSettingsDelete.json',
                                method: 'POST',

                                success: function (f, a) {
                                    Ext.Msg.alert('Success:', "The settings were successfully deleted");
                                    form3.getForm().findField('smtpServer').setValue("");
                                    form3.getForm().findField('smtpServerPort').setValue("");
                                    form3.getForm().findField('fromEmail').setValue("");
                                    form3.getForm().findField('smtpServerFromPassword').setValue("");
                                    form3.getForm().findField('subjectText').setValue("");
                                    document.getElementById('sslcheck').checked = false;
                                },
                                failure:  function (f, a) {
                                    switch (a.failureType) {
                                        case Ext.form.Action.CLIENT_INVALID:
                                            Ext.Msg.alert('Failure:', 'Form fields may not be submitted with invalid values');
                                            break;
                                        case Ext.form.Action.CONNECT_FAILURE:
                                            Ext.Msg.alert('Failure:', 'Server connection problems. Please, try to reload page.');
                                            break;
                                        case Ext.form.Action.SERVER_INVALID:
                                            Ext.Msg.alert('Failure:', "Failed to delete the settings");
                                    }
                                }
                            });
                        }
                    }, this);
                    return false; // Stop the delete request
                }

            }
        ]
        ,
        listeners: {
            beforerender: function(pn) {
                Ext.Ajax.request({
                    url: '../../getEmailReportSettings.json',
                    callback: function(options, success, response) {
                        var smtpServer = Ext.util.JSON.decode(response.responseText).smtpServer;
                        form3.getForm().findField('smtpServer').setValue(smtpServer);
                        var smtpServerPort = Ext.util.JSON.decode(response.responseText).smtpServerPort;
                        form3.getForm().findField('smtpServerPort').setValue(smtpServerPort);
                        var fromEmail = Ext.util.JSON.decode(response.responseText).fromEmail;
                        form3.getForm().findField('fromEmail').setValue(fromEmail);
                        var smtpServerFromPassword = Ext.util.JSON.decode(response.responseText).smtpServerFromPassword;
                        form3.getForm().findField('smtpServerFromPassword').setValue(smtpServerFromPassword);
                        var subjectText = Ext.util.JSON.decode(response.responseText).subjectText;
                        form3.getForm().findField('subjectText').setValue(subjectText);
                        var ssl = Ext.util.JSON.decode(response.responseText).sslcheck;
                        if (ssl == "off") {
                            document.getElementById('sslcheck').checked = false;
                        } else if (ssl == "on") {
                            document.getElementById('sslcheck').checked = true;
                        }
                    }
                });
            }

        }
    });


    var form3 = new Ext.form.FormPanel({
        title: 'Report Email Settings',
        id: 'banner-view',
        layout: 'table',
        width: '100%',
        layoutConfig: {
            columns: 1
        },
        items: [
            /*   {
             xtype: 'label',
             cls: 'p_labels_bold',
             text: 'Report Email Setting Name'
             },
             {
             xtype: 'label',
             cls: 'p_labels_bold',
             text: 'Report Email Setting Value'
             },*/
            serverEmailPanel
        ]
    });


    var locationStore = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            url: '../../locations/getRealLocations.json'
        }),
        reader: new Ext.data.JsonReader({
            root:'locations'
        },
                [
                    {
                        name: 'id'
                    },
                    {
                        name: 'locationName'
                    }
                ])
    });

    var locationCombo = new Ext.form.ComboBox({
        id: 'locationsCombo',
        width    : 150,
        store: locationStore,
        displayField: 'locationName',
        valueField: 'id',
        hiddenName : 'id',
        cls: 'location-combo',
        typeAhead: true,
        mode: 'local',
        anchor: '100%',
        forceSelection: true,
        triggerAction: 'all',
        emptyText:'Select a location...',
        selectOnFocus:true,
        autoSelect: true,
        hidden: false,
        shadow : false		
    });


    var panelLocation = new Ext.Panel({
        id: 'settingLocation',
        bodyStyle: 'padding: 7px 0px 0px 0px',
        layout: 'table',
        layoutConfig: {
            columns: 2
        },
        items    : [
            {
                xtype: 'label',
                cls: 'p_labels_bold',
                text: 'Location'
            },
            locationCombo
        ]
    });


    var panelEmails = new Ext.Panel({
        id: 'settingEmail',
        layout: 'table',
        layoutConfig: {
            columns: 6
        },
        items    : [
            {
                xtype: 'label',
                cls: 'p_labels_bold',
                text: 'Email 1'
            },
            {
                xtype: 'field',
                id: 'reportEmail1',
                allowBlank: true,
                inputType: 'text',
                width: '200'
            },
            {
                xtype: 'label',
                cls: 'p_labels_bold',
                text: 'Email 2'
            },
            {
                xtype: 'field',
                id: 'reportEmail2',
                allowBlank: true,
                inputType: 'text',
                width: '200'
            },
            {
                xtype: 'label',
                cls: 'p_labels_bold',
                text: 'Email 3'
            },
            {
                xtype: 'field',
                id: 'reportEmail3',
                allowBlank: true,
                inputType: 'text',
                width: '200'
            }
        ]
    });

    var panelStrings = new Ext.Panel({
        id: 'settingString',
        layout: 'table',
        layoutConfig: {
            columns: 2
        },
        items    : [
            {
                xtype: 'label',
                cls: 'p_labels_bold',
                text: 'Email string on the first day of Month'
            },
            {
                xtype: 'textarea',
                name: 'reportString1',
                id: 'reportString1',
                fieldLabel: 'Enter some text',
                width: 700,
                height: 50,
                anchor: '90%',
                multiline: true
            },
            /*{
             xtype: 'field',
             id: 'reportString1',
             allowBlank: true,
             inputType: 'text',
             width: '400'
             },*/
            {
                xtype: 'label',
                cls: 'p_labels_bold',
                text: 'Email string on the middle of the Month'
            },
            {
                xtype: 'textarea',
                name: 'reportString2',
                id: 'reportString2',
                fieldLabel: 'Enter some text',
                width: 700,
                height: 50,
                anchor: '90%',
                multiline: true
            },
            /*{
             xtype: 'field',
             id: 'reportString2',
             allowBlank: true,
             inputType: 'text',
             width: '400'
             },*/
            {
                xtype: 'label',
                cls: 'p_labels_bold',
                text: 'Email string on the last day of Month'
            },
            {
                xtype: 'textarea',
                name: 'reportString3',
                id: 'reportString3',
                fieldLabel: 'Enter some text',
                width: 700,
                height: 50,
                anchor: '90%',
                multiline: true
            }/*,
             {
             xtype: 'field',
             id: 'reportString3',
             allowBlank: true,
             inputType: 'text',
             width: '400'
             }*/
        ]
    });

    var panelStringsAndEmails = new Ext.Panel({
        id: 'settingStringAndEmail',
        layout: 'table',
        layoutConfig: {
            columns: 1
        },
        disabled: true,
        items    : [
            panelEmails,
            panelStrings,
            {
                xtype: 'button',
                text: 'Save Settings',
                handler: function() {
                    // form2.getForm().findField('reportEmail1').allowBlank = true;
                    form2.getForm().submit({
                        clientValidation: true,
                        url: '../../reportSettingsSave.json',
                        success: function(form, action) {
                            Ext.Msg.alert('Success', action.result.msg);
                        },
                        failure: function(form, action) {
                            form.fileUpload = false;
                            switch (action.failureType) {
                                case Ext.form.Action.CLIENT_INVALID:
                                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                    break;
                                case Ext.form.Action.CONNECT_FAILURE:
                                    Ext.Msg.alert('Failure', 'Server connection problems. Please, try to reload page.');
                                    break;
                                case Ext.form.Action.SERVER_INVALID:
                                    Ext.Msg.alert('Failure', action.result.msg);
                            }
                        }
                    });
                }

            }
        ]
    });


    var form2 = new Ext.form.FormPanel({
        title: 'Report Settings',
        id: 'banner-view',
        layout: 'table',
        width: '100%',
        layoutConfig: {
            columns: 2
        },
        items: [
            {
                xtype: 'label',
                cls: 'p_labels_bold',
                text: 'Location'
            },
            {
                xtype: 'label',
                cls: 'p_labels_bold',
                text: 'Location Settings'
            },
            panelLocation,
            panelStringsAndEmails
        ]
    });

    locationCombo.on('select', function(box, record, index) {
        var data = record.get('id');
        panelStringsAndEmails.setDisabled(false);
        Ext.Ajax.request({
            url: '../../getReportEmails.json',
            method: 'POST',
            params:
            {
                'locationid':data
            },
            callback: function(options, success, response) {
                var responseText = response.responseText;
                form2.getForm().findField('reportEmail1').setValue("");
                form2.getForm().findField('reportEmail2').setValue("");
                form2.getForm().findField('reportEmail3').setValue("");
                form2.getForm().findField('reportString1').setValue("");
                form2.getForm().findField('reportString2').setValue("");
                form2.getForm().findField('reportString3').setValue("");
                form2.getForm().findField('reportEmail1').setValue(Ext.util.JSON.decode(responseText).email1);
                form2.getForm().findField('reportEmail2').setValue(Ext.util.JSON.decode(responseText).email2);
                form2.getForm().findField('reportEmail3').setValue(Ext.util.JSON.decode(responseText).email3);
                form2.getForm().findField('reportString1').setValue(Ext.util.JSON.decode(responseText).emailString1);
                form2.getForm().findField('reportString2').setValue(Ext.util.JSON.decode(responseText).emailString2);
                form2.getForm().findField('reportString3').setValue(Ext.util.JSON.decode(responseText).emailString3);
            }
        });

    });

    var locationStore1 = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            url: '../../locations/getRealLocations.json'
        }),
        reader: new Ext.data.JsonReader({
            root:'locations'
        },
                [
                    {
                        name: 'id'
                    },
                    {
                        name: 'locationName'
                    }
                ])
    });

    var locationCombo1 = new Ext.form.ComboBox({
            id: 'locationsCombo1',
            width    : 150,
            store: locationStore1,
            displayField: 'locationName',
            valueField: 'id',
            hiddenName : 'id',
            cls: 'location-combo',
            typeAhead: true,
            mode: 'local',
            anchor: '100%',
            forceSelection: true,
            triggerAction: 'all',
            emptyText:'Select a location...',
            selectOnFocus:true,
            autoSelect: true,
            hidden: false,
            shadow : false
        });




     var panelLocation1 = new Ext.Panel({
        id: 'settingLocation',
        bodyStyle: 'padding: 7px 0px 0px 0px',
        layout: 'table',
        layoutConfig: {
            columns: 2
        },
        items    : [
            {
                xtype: 'label',
                cls: 'p_labels_bold',
                text: 'Location'
            },
            locationCombo1
        ]
    });


    locationCombo1.on('select', function(box, record, index) {
        var data = record.get('id');
        panelStringsAndEmails1.setDisabled(false);
        Ext.Ajax.request({
            url: '../../getDailyReportEmails.json',
            method: 'POST',
            params:
            {
                'locationid':data
            },
            callback: function(options, success, response) {
                var responseText = response.responseText;
                form4.getForm().findField('reportDailyEmail1').setValue("");
                form4.getForm().findField('reportDailyEmail2').setValue("");
                form4.getForm().findField('reportDailyEmail3').setValue("");
                form4.getForm().findField('reportDailyEmail1').setValue(Ext.util.JSON.decode(responseText).email1);
                form4.getForm().findField('reportDailyEmail2').setValue(Ext.util.JSON.decode(responseText).email2);
                form4.getForm().findField('reportDailyEmail3').setValue(Ext.util.JSON.decode(responseText).email3);
            }
        });

    });
    var panelEmails1 = new Ext.Panel({
           id: 'settingEmail',
           layout: 'table',
           layoutConfig: {
               columns: 6
           },
           items    : [
               {
                   xtype: 'label',
                   cls: 'p_labels_bold',
                   text: 'Email 1'
               },
               {
                   xtype: 'field',
                   id: 'reportDailyEmail1',
                   allowBlank: true,
                   inputType: 'text',
                   width: '200'
               },
               {
                   xtype: 'label',
                   cls: 'p_labels_bold',
                   text: 'Email 2'
               },
               {
                   xtype: 'field',
                   id: 'reportDailyEmail2',
                   allowBlank: true,
                   inputType: 'text',
                   width: '200'
               },
               {
                   xtype: 'label',
                   cls: 'p_labels_bold',
                   text: 'Email 3'
               },
               {
                   xtype: 'field',
                   id: 'reportDailyEmail3',
                   allowBlank: true,
                   inputType: 'text',
                   width: '200'
               }
           ]
       });


     var panelStringsAndEmails1 = new Ext.Panel({
        id: 'settingStringAndEmail1',
        layout: 'table',
        layoutConfig: {
            columns: 1
        },
        disabled: true,
        items    : [
            panelEmails1,
            {
                xtype: 'button',
                text: 'Save Daily Report Settings',
                handler: function() {
                    // form2.getForm().findField('reportEmail1').allowBlank = true;
                    form4.getForm().submit({
                        clientValidation: true,
                        url: '../../reportDailySettingsSave.json',
                        success: function(form, action) {
                            Ext.Msg.alert('Success', action.result.msg);
                        },
                        failure: function(form, action) {
                            form.fileUpload = false;
                            switch (action.failureType) {
                                case Ext.form.Action.CLIENT_INVALID:
                                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                    break;
                                case Ext.form.Action.CONNECT_FAILURE:
                                    Ext.Msg.alert('Failure', 'Server connection problems. Please, try to reload page.');
                                    break;
                                case Ext.form.Action.SERVER_INVALID:
                                    Ext.Msg.alert('Failure', action.result.msg);
                            }
                        }
                    });
                }

            }
        ]
    });

    var form4 = new Ext.form.FormPanel({
        title: 'Daily Report Settings',
        id: 'banner-view',
        layout: 'table',
        width: '100%',
        layoutConfig: {
            columns: 2
        },
        items: [
            {
                xtype: 'label',
                cls: 'p_labels_bold',
                text: 'Location'
            },
            {
                xtype: 'label',
                cls: 'p_labels_bold',
                text: 'Location Daily Report Settings'
            },
            panelLocation1,
            panelStringsAndEmails1
        ]
    });

    locationStore.load();
    locationStore1.load();

    form.render('content');
    form3.render('content');
    form4.render('content');
    form2.render('content');

});

