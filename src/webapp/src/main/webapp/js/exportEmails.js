Ext.onReady(function() {

    var storeLocation = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            api: {
                read: '../../locations/getRealLocations.json'
            }
        }),

        reader: new Ext.data.JsonReader(
        {
            root:'locations'
        },
                [
                    {
                        name: 'id'
                    },
                    {
                        name: 'locationName'
                    }
                ])
    });


    var locationComboExp = new Ext.form.ComboBox({
        id: 'locationComboExp',
        displayField: 'locationName',
        valueField: 'id',
        hiddenName : 'locationId',
        mode: 'local',
        width: 112,
        store: storeLocation,
        forceSelection: true,
        triggerAction: 'all',
        emptyText:'Select a location...',
        selectOnFocus:true,
        editable: false,
        allowBlank: false,
        shadow : false
    });

    var startDate = new Ext.form.DateField({
        name: 'dateFrom',
        id: 'dateFrom',
        cls: 'date-field',
        showToday: false,
        width:120,
        format: 'Y-m-d'
    });

    var endDate = new Ext.form.DateField({
        name: 'dateTo',
        id: 'dateTo',
        cls: 'date-field',
        showToday: false,
        width:120,
        format: 'Y-m-d'
    });

    var exportPanel = new Ext.form.FormPanel({
        title: 'Export emails from database',
        /*  standardSubmit: true,*/
        standardSubmit: true,
        url: '../../exportEmails.json',
        layout: 'table',
        standardSubmit: true,
        layoutConfig:{
            columns:2,
            tableAttrs:{
                style:{width: '600px'}
            }
        },
        items: [
            {
                xtype: 'label',
                cls: 'p_labels bold',
                text: 'Location:',
                cellCls: 'labelCell'
            },
            locationComboExp,
            {
                xtype: 'label',
                cls: 'p_labels bold',
                text: 'from Date',
                cellCls: 'labelCell'
            },
            startDate,
            {
                xtype: 'label',
                cls: 'p_labels bold',
                text: 'to Date',
                cellCls: 'labelCell'
            },
            endDate,
            {
                xtype: 'button',
                text: 'Get Emails',
                colspan: '2',
                handler: function() {
                    var form = exportPanel.getForm();
                    if (form.isValid()) {
                        form.submit();
                    } else {
                        Ext.Msg.alert('Alert!', 'All fields are required! Please, fill them in');
                    }
                }

            }
        ]

    });

    var items = [ exportPanel ];


    var forms = new Ext.Panel({
        title: 'Administration - Export purchase emails',
        id: 'purch-view',
        layout: 'table',
        width: '100%',
        layoutConfig: {
            columns: 1,
            tableAttrs: {
                style: {width: '100%'}
            }

        },
        items: items
    });


    storeLocation.load();
    forms.render('content');
});

