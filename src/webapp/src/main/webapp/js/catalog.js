Ext.onReady(function () {

    function isTablet() {
        return /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent);
    }

    function isVisibleImage(url1, url2, rotated,pictureSize) {
        var stri = "";
        var watermarkprev = "watermarkprev "+rotated+" "+pictureSize;
        var watermarkmainprev = "watermarkmainprev "+rotated + " "+pictureSize;

        if (url1.length == 0) {
            stri = stri + "<img src=\".." + url1 + "\" class=\"" + watermarkprev + "\" style='visibility:hidden;' title = '' alt=''/>";
        } else {
            stri = stri + "<img src=\".." + url1 + "\" class=\"" + watermarkprev + "\" title = '' alt=''/>";
        }
        if (url2.length == 0) {
            stri = stri + "<img src=\".." + url2 + "\" class=\"" + watermarkmainprev + "\"  style='visibility:hidden;' title = '' alt=''/>";
        } else {
            stri = stri + "<img src=\".." + url2 + "\" class=\"" + watermarkmainprev + "\"  title = '' alt=''/>";
        }

        return  stri;
    }

    function isPublicImage(rotated) {
        var stri = "";
        var picturewatermarkopaqueprev = "picture-watermark-opaque-prev "+rotated;

        if (isPublicUser()) {
            stri = stri + "<img src='../../css/images/logo.png'" + "' class=\"" + picturewatermarkopaqueprev + "\"  title = '' alt=''/>";
        }

        return  stri;
    }

    var limit = 50, page = 1;

    ImageViewer = Ext.extend(Ext.Window, {
        height: 800,
        iterationSize: 5,
        cls: 'image_viewer',
        initComponent: function () {
            var me = this;
            this.src = [].concat(this.src);
            var children = [];
            for (var i = 0; i < this.src.length; i++) {
                children.push({
                    tag: 'div',
                    cls: 'preview-item' + (i < this.iterationSize ? '' : ' x-hidden'),
                    children: [
                        {
                            tag: 'div',
                            html: "<div class ='main_preview_template_string'><span class='camera'>" + this.src[i].cameraName +
                                  "</span><span class='date'>" + this.src[i].creationDate + "</span>"
                        },
                        {
                            tag: 'div',
                            width: 640,
                            html: '<div class ="main_preview" id="watermark_box">' +
                                  '<div class="main_prev_img" style="background: url(\'../'+ this.src[i].base64code +'\') center center no-repeat; background-size: contain;" title="' + this.src[i].name + '"></div>' +
                                  '<div class="watermarktext2prev '+this.src[i].rotated+' '+this.src[i].pictureSize+'">' + this.src[i].logoText + '</div>' +
                                  isVisibleImage(this.src[i].logoUrl, this.src[i].logoMainUrl, this.src[i].rotated,this.src[i].pictureSize) + isPublicImage(this.src[i].rotated) +
                                  '</div>'
                        }
                    ]
                });
            }
            this.bodyCfg = {
                tag: 'div',
                cls: 'preview-container',
                style: 'overflow-y: auto; overflow-x: hidden;',
                children: children
            };
            if (this.src.length > 1) {
                this.buttons = [
                    {
                        xtype: 'button',
                        text: 'Show more',
                        cls: 'show-more-btn',
                        handler: function () {
                            var items = me.body.select('.preview-item.x-hidden');
                            items.each(function (item, all, index) {
                                if (index < me.iterationSize) {
                                    item.removeClass('x-hidden');
                                } else {
                                    return false;
                                }
                            });
                        }
                    }
                ]
            } else {
                this.setHeight('auto')
            }

            ImageViewer.superclass.initComponent.apply(this, arguments);
        },

        onRender: function () {
            ImageViewer.superclass.onRender.apply(this, arguments);
            this.body.on('load', this.onImageLoad, this, {single: true});
        },

        onImageLoad: function () {
            this.setWidth(640);
            this.setHeight('auto');
            var h = this.getFrameHeight();
            w = this.getFrameWidth();
            this.setSize(this.body.dom.offsetWidth + w, this.body.dom.offsetHeight + h);
            if (Ext.isIE) {
                this.center();
            }
        },

        setSrc: function (src) {
            this.body.on('load', this.onImageLoad, this, {single: true});
            this.body.dom.style.width = this.body.dom.style.width = 'auto';
            this.body.dom.src = src;
        },

        initEvents: function () {
            ImageViewer.superclass.initEvents.apply(this, arguments);
            if (this.resizer) {
                this.resizer.preserveRatio = true;
            }
        }
    });

    /*SEARCH PANEL ELEMENTS */
    var locationStore = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            url: '../../getLocations.json'
        }),
        reader: new Ext.data.JsonReader({
                root: 'locations'
            },
            [
                {
                    name: 'id'
                },
                {
                    name: 'locationName'
                }
            ])
    });

    var cameraStore = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            url: '../../getCameras.json'
        }),
        reader: new Ext.data.JsonReader({
                root: 'cameras',
                idProperty: 'id'
            },
            [
                {
                    name: 'id'
                },
                {
                    name: 'cameraName'
                },
                {
                    name: 'location'
                }

            ])
    });

    Ext.override(Ext.ux.form.LovCombo, {
        beforeBlur: Ext.emptyFn
    });
    var cameraCombo = new Ext.ux.form.LovCombo({
        id: 'camerasCombo',
        width: 200,
        store: cameraStore,
        displayField: 'cameraName',
        valueField: 'id',
        hiddenName: 'cid',
        cls: 'camera-combo',
        typeAhead: true,
        mode: 'local',
        anchor: '100%',
        forceSelection: true,
        triggerAction: 'all',
        emptyText: 'Select a camera...',
        selectOnFocus: true,
        disabled: false,
        autoScroll: true,
        editable: false,
        allowBlank: true,
        maxHeight: 100,
        shadow: false
    });

    var locationCombo = new Ext.form.ComboBox({
        id: 'locationsCombo',
        width: 200,
        store: locationStore,
        displayField: 'locationName',
        valueField: 'id',
        hiddenName: 'id',
        cls: 'location-combo',
        typeAhead: true,
        mode: 'local',
        anchor: '100%',
        forceSelection: true,
        triggerAction: 'all',
        emptyText: 'Select a location...',
        selectOnFocus: true,
        autoSelect: true,
        hidden: isUser(),
        shadow: false
    });

    var tagStore = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            url: '../../getTags.json'
        }),
        reader: new Ext.data.JsonReader({
                root: 'tags',
                idProperty: 'id'
            },
            [
                {
                    name: 'id'
                },
                {
                    name: 'tagIdName'
                },
                {
                    name: 'location'
                }
            ])
    });

    Ext.override(Ext.ux.form.LovCombo, {
        beforeBlur: Ext.emptyFn
    });

    function updateTagCombo(tacom) {
        var newComboValue = tacom;
        if ((tacom.substring(0, 1) == "0") && tacom.length > 1) {
            newComboValue = tacom.substring(2, tacom.length);
        } else {
            if (tacom.length == 0) {
                newComboValue = "0";
            }
        }
        tagCombo.setValue(newComboValue)
    }

    var tagCombo = new Ext.ux.form.LovCombo({
        id: 'camerasCombo1',
        width: 200,
        store: tagStore,
        displayField: 'tagIdName',
        valueField: 'id',
        hiddenName: 'tcid',
        cls: 'camera-combo',
        typeAhead: true,
        mode: 'local',
        anchor: '100%',
        forceSelection: true,
        triggerAction: 'all',
        emptyText: 'Select a tag...',
        selectOnFocus: true,
        autoScroll: true,
        editable: true,
        queryAnyMatch: true,
        allowBlank: true,
        maxHeight: 100,
        shadow: false,
        disabled: GLOBAL.userId != 1 && !GLOBAL.showTags,
        hidden: GLOBAL.userId != 1 && !GLOBAL.showTags,
        listeners: {
            select: function (combo, record, index) {
                // updateTagCombo(tagCombo.getValue());
            }
        }
    });

    function isCodeUser() {
        if (GLOBAL.userId == 6) {
            return true;
        }
    }

    function isUser() {
        if (GLOBAL.userId != 1) {
            return true;
        }
    }

    function isPublicUser() {
        if (GLOBAL.userId == 3 || GLOBAL.userId == 4 || GLOBAL.userId == 5 || GLOBAL.userId == 6) {
            return true;
        }
    }

    function isPublicOnlyUser() {
        if (GLOBAL.userId == 3) {
            return true;
        }
    }

    function isPublicTagEmailUser() {
        if (GLOBAL.userId == 4 || GLOBAL.userId == 5 || GLOBAL.userId == 6) {
            return true;
        }
    }

    locationCombo.on('select', function (box, record, index) {
        var data = record.get('id');
        cameraCombo.setDisabled(false);
        cameraStore.load(
            {
                params: {
                    'locationid': data
                },
                callback: function () {
                    cameraCombo.setValue('0');
                }
            });
        reloadTagStore();
    });

    var myStartDate = GLOBAL.startDay==""?new Date(GLOBAL.startDate).format('Y-m-d'):GLOBAL.startDay;
    var myEndDate = GLOBAL.endDay==""?new Date(GLOBAL.endDate).format('Y-m-d'):GLOBAL.endDay;
    var time = GLOBAL.startTime==""?new Date(GLOBAL.startDate).format('g:i A'):GLOBAL.startTime;
    var timeEnd = GLOBAL.endTime==""?new Date(GLOBAL.endDate).format('g:i A'):GLOBAL.endTime;

    var getTagStoreParams = function(){
        var tacom = tagCombo.getValue();
        return {
            'startdate': getValueFromSFByFieldName('startdt'),
            'starttime': getValueFromSFByFieldName('starttm'),
            'enddate': getValueFromSFByFieldName('enddt'),
            'endtime': getValueFromSFByFieldName('endtm'),
            'location': getValueFromSFByFieldName('id'),
            'start': (page-1)*limit,
            'limit': limit,
            'tag': tacom,
            'tagcombo': tagCombo.getValue(),
            'mandatoryTags': GLOBAL.tag
        };
    }

    var loadTagStore = function(handler){
        var tacom = tagCombo.getValue();
        var params = getTagStoreParams();
        tagStore.load({
            params: params,
            callback: handler
        });
    }

    function reloadTagStoreWithCheck() {
        var tacom = tagCombo.getValue();
        loadTagStore(function () {
            tagCombo.setValue(tacom);
            checkTagsGrouping();
        });
    }

    /*SEARCH PANEL ELEMENTS */
    function checkTagsGrouping() {
        var tacom = tagCombo.getValue();

        Ext.Ajax.request({
            url: '../../checkTagsGrouping.json',
            params: {
                'startdate': getValueFromSFByFieldName('startdt'),
                'starttime': getValueFromSFByFieldName('starttm'),
                'enddate': getValueFromSFByFieldName('enddt'),
                'endtime': getValueFromSFByFieldName('endtm'),
                'location': getValueFromSFByFieldName('id'),
                'tag': tacom
            },
            method: 'POST',
            callback: function (options, success, response) {
                var tagsToSet = Ext.util.JSON.decode(response.responseText).tagsToSet;

                tagCombo.setValue(tagsToSet);
                getSearchedPictures();
            }
        });
    }

    function reloadTagStore() {
        var tacom = tagCombo.getValue();
        var params = getTagStoreParams();
        tagStore.load(
            {
                params: params,
                callback: function () {
                    tagCombo.setValue(tacom);
                }
            });
    }

    var startdt = new Ext.form.DateField({
        fieldLabel: 'Start Date',
        name: 'startdt',
        id: 'startdt',
        cls: 'date-field',
        showToday: false,
        width: 120,
        format: 'Y-m-d',
        disabled: isPublicOnlyUser(),
        value: myStartDate,
        listeners: {
            select: function (startdt, newValue, oldValue) {
                reloadTagStore();
            }
        }
    });

    var enddt = new Ext.form.DateField({
        fieldLabel: 'End Date',
        name: 'enddt',
        id: 'enddt',
        cls: 'date-field',
        showToday: false,
        width: 120,
        format: 'Y-m-d',
        disabled: isPublicOnlyUser(),
        value: myEndDate,
        listeners: {
            select: function (startdt, newValue, oldValue) {
                reloadTagStore()
            }
        }
    });

    var starttm = new Ext.form.TimeField({
        minValue: '0:00 AM',
        maxValue: '23:00 PM',
        id: 'starttm',
        cls: 'time-field',
        increment: 30,
        value: time,
        emptyText: time,
        width: 75,
        autoScroll: true,
        shadow: false,
        style: {
            marginLeft: isTablet() ? '5px' : '0'
        },
        listeners: {
            select: function (startdt, newValue, oldValue) {
                reloadTagStore()
            }
        }
    });

    var endtm = new Ext.form.TimeField({
        minValue: '0:00 AM',
        maxValue: '23:00 PM',
        cls: 'time-field',
        id: 'endtm',
        value: timeEnd,
        increment: 30,
        emptyText: timeEnd,
        width: 75,
        shadow: false,
        style: {
            marginLeft: isTablet() ? '5px' : '0'
        },
        listeners: {
            select: function (startdt, newValue, oldValue) {
                reloadTagStore();
            }
        }
    });

    function getSearchedPictures() {
        store4.load(
            {
                params: {
                    'startdate': getValueFromSFByFieldName('startdt'),
                    'starttime': getValueFromSFByFieldName('starttm'),
                    'enddate': getValueFromSFByFieldName('enddt'),
                    'endtime': getValueFromSFByFieldName('endtm'),
                    'location': getValueFromSFByFieldName('id'),
                    'camera': getValueFromSFByFieldName('cid'),
                    'tag': getValueFromSFByFieldName('tcid'),
                    'getFilter': 'save'
                }
            });
    }

    var submitHandler = function () {
        page = 1;
        reloadTagStoreWithCheck();
    };

    var slideshowHandler = function (btn, evt) {
        var lo = dataview.store.lastOptions || {};
        var url = Ext.urlAppend(GLOBAL.contextPath + '/pictures/main/slideshow', Ext.urlEncode(lo.params || {})),
            name = 'slideshowwindow' + new Date().getMilliseconds();
        var mywindow = window.open(
            url,
            name,
            'scrollbars=yes,menubar=no,height=900,width=1600,resizable=yes,toolbar=no,location=no,status=no'
        );
        mywindow.moveTo(10, 10);
        mywindow.focus();
    }

    var buttons = {
        xtype: 'button',
        text: 'Search',
        id: 'saveBtn',
        cls: 'search-btn',
        width: 100,
        handler: submitHandler
    };

    var startlabel = new Ext.form.Label({
        initialConfig: {
            text: 'Start Date',
            cls: 'search-label'
        }

    });
    var endlabel = new Ext.form.Label({
        initialConfig: {
            text: 'End Date',
            cls: 'search-label'

        }

    });
    var locationlabel = new Ext.form.Label({
        initialConfig: {
            text: 'Location',
            hidden: isUser(),
            cls: 'search-label'
        }

    });

    var camralabel = new Ext.form.Label({
        initialConfig: {
            text: 'Camera',
            cls: 'search-label'

        }

    });

    var tagLabel = new Ext.form.Label({
        initialConfig: {
            text: 'Tag',
            hidden: GLOBAL.userId != 1 && !GLOBAL.showTags,
            cls: 'search-label'
        }
    });


    var tagGeneralStore = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            url: '../../getTagsQuery.json'
        }),
        reader: new Ext.data.JsonReader({
                root: 'tagsQuery',
                idProperty: 'id'
            },
            [
                {
                    name: 'id'
                },
                {
                    name: 'tagIdName'
                },
                {
                    name: 'location'
                }

            ])
    });

    var searchCombo1 = new Ext.form.ComboBox({
        id: 'searchTagCombo1',
        store: tagGeneralStore,
        displayField: 'tagIdName',
        valueField: 'id',
        hiddenName: 'tsid',
        cls: 'camera-combo',
        typeAhead: true,
        hideTrigger: true,
        width: 300,
        mode: 'local',
        anchor: '100%',
        forceSelection: true,
        triggerAction: 'all',
        emptyText: 'Select a tag...',
        selectOnFocus: true,
        disabled: false,
        autoScroll: true,
        editable: true,
        allowBlank: true,
        maxHeight: 100,
        shadow: false
    });

    function getValueFromSFByFieldName(fieldName) {
        var value;
        try {
            value = searchForm.getForm().findField(fieldName).getValue();
        } catch(err) {
            console.log(err);
        }
        return value;
    }

    var submitAddHandler1 = function () {
        var selectedTags = tagCombo.getValue();
        var newLyAddedTag = searchCombo1.getValue();
        var tagToSet = selectedTags + "," + newLyAddedTag;
        panel.setDisabled(false);
        searchForm.setDisabled(false);
        var form = searchForm.getForm();
        Ext.Ajax.request({
            url: '../../getChangedTime.json',
            params: {
                'startdate': getValueFromSFByFieldName('startdt'),
                'starttime': getValueFromSFByFieldName('starttm'),
                'enddate': getValueFromSFByFieldName('enddt'),
                'endtime': getValueFromSFByFieldName('endtm'),
                'location': getValueFromSFByFieldName('id'),
                'tag': searchCombo1.getValue()
            },
            method: 'POST',
            callback: function (options, success, response) {
                var start = Ext.util.JSON.decode(response.responseText).startDateNew;
                var end = Ext.util.JSON.decode(response.responseText).endDateNew;
                if (start != null) {
                    form.findField('startdt').setValue(new Date(start).format('Y-m-d'));
                    form.findField('starttm').setValue(new Date(start).format('g:i A'));
                }
                if (end != null) {
                    form.findField('enddt').setValue(new Date(end).format('Y-m-d'));
                    form.findField('endtm').setValue(new Date(end).format('g:i A'));
                }
                var params = getTagStoreParams();
                tagStore.load(
                    {
                        params: params,
                        callback: function (options, success, response) {
                            tagCombo.setValue(tagToSet);
                        }
                    });

                searchCombo1.setValue("");
                tagWin.hide();
            }
        });
    };

    var addButtonSearch1 = {
        xtype: 'button',
        text: 'Add',
        id: 'addBtnSearch1',
        width: 50,
        handler: submitAddHandler1
    };

    var windowForm = new Ext.Panel({
        frame: true,
        cls: 'search-panel',
        id: 'windowPanel',
        layout: 'table',
        layoutConfig: {
            columns: 2,
            tableAttrs: {
                style: {width: '400px'}
            }
        },
        items: [
            {
                xtype: 'label',
                cls: 'p_labels bold',
                text: 'Please enter first  digits of the Tag your are looking for and select the tag from the drop down menu. Then press Add button to add the selected tag to the search tag field',
                cellCls: 'labelCell',
                colspan: 2
            },
            searchCombo1,
            addButtonSearch1
        ]
    });

    var tagWin = new Ext.Window({
        id: 'myWin',
        height: 150,
        width: 415,
        closeAction: 'hide',
        items: [
            windowForm
        ],
        onHide: function () {
            panel.setDisabled(false);
            searchForm.setDisabled(false);
        }
    });
    tagWin.hide();

    var myButton = new Ext.Button({
        id: 'addBtn',
        split: true,
        hidden: isPublicOnlyUser() || (GLOBAL.userId != 1 && !GLOBAL.showTags),
        width: 50,
        iconCls: 'addButton',
        handler: function () {
            tagWin.show();
            panel.setDisabled(true);
            searchForm.setDisabled(true);
            tagGeneralStore.load(
                {
                    params: {
                        'location': searchForm.getForm().findField('id').getValue()
                    }
                }
            );
        }
    });

    var resultTpl = new Ext.XTemplate(
        '<tpl for="."><div class="search-item">',
        '<h3><span>{id} by {tagIdName}</span></h3>',
        '{excerpt}',
        '</div></tpl>'
    );

    /*SEARCH PANELs */
    var searchForm = new Ext.FormPanel({
        frame: true,
        height: isTablet() ? 90 : 70,
        hidden: isPublicTagEmailUser(),
        cls: 'search-panel',
        bodyStyle: 'padding: 10px 0px 10px 5px',
        id: 'searchPanel',
        layout: 'table',
        layoutConfig: {
            columns: isTablet() ? 6 : 14
        },
        items: isTablet() ? [
            startlabel,
            {
                xtype: 'panel',
                layout: 'hbox',
                items: [
                    startdt,
                    starttm
                ]
            },
            endlabel,
            {
                xtype: 'panel',
                layout: 'hbox',
                items: [
                    enddt,
                    endtm
                ]
            },
            locationlabel,
            locationCombo,
            camralabel,
            cameraCombo,
            tagLabel,
            tagCombo,
            myButton,
            buttons
        ] : [
            startlabel,
            startdt,
            starttm,
            endlabel,
            enddt,
            endtm,
            locationlabel,
            locationCombo,
            camralabel,
            cameraCombo,
            tagLabel,
            tagCombo,
            myButton,
            buttons
        ]
    });

    var writer = new Ext.data.JsonWriter({
        encode: true,
        writeAllFields: true
    });

    var store4 = new Ext.data.Store({
        autoSave: false,
        proxy: new Ext.data.HttpProxy({
            api: {
                read: '../../getPictures.json',
                create: '../../getPicturesCreate.json', // Server MUST return idProperty of new record
                update: '../../getPicturesUpdate.json',
                destroy: '../../getPicturesDelete.json'
            }
        }),
        reader: new Ext.data.JsonReader({
                root: 'pictures',
                totalProperty: 'total',
                idProperty: 'id'
            },
            ['id', 'url', 'base64code', 'name', 'cameraId', 'cameraName', 'tag',
                {
                    name: 'selected',
                    type: 'boolean'
                },
                {
                    name: 'creationDate'
                },
                {
                    name: 'logoMainUrl'
                },
                {
                    name: 'logoUrl'
                },
                {
                    name: 'logoText'
                },
                {
                    name: 'rotated',
                    type: 'boolean'
                },
                {
                    name: 'pictureSize'
                }
            ]),
        writer: writer,
        params: {
            startdate: getValueFromSFByFieldName('startdt'),
            starttime: getValueFromSFByFieldName('starttm'),
            enddate: getValueFromSFByFieldName('enddt'),
            endtime: getValueFromSFByFieldName('endtm'),
            location: getValueFromSFByFieldName('id'),
            camera: getValueFromSFByFieldName('cid'),
            tag: getValueFromSFByFieldName('tcid')
        }
    });

    var myMask = new Ext.LoadMask(Ext.getBody(), { msg: " Please wait...", store: store4});

    /*PICTURE TEMPLATE */
    var tpl = new Ext.XTemplate(
        '<tpl for=".">',
        '<div class="thumb-wrap" id="{id}">',
        '<div class="template_string"><span class="camera">{cameraName}</span><span class="date">{dateString}</span></div>',
        '<div class = "watermark_box" id="watermark_box">',
        '<div style="background: url(\'../{base64code}\') center center no-repeat; background-size: contain;" title="{name}" class="thumb"></div>',
        '<div id="watermarktext2{rotated} {pictureSize}" class="watermarktext2{rotated} {pictureSize}">{logoText}</div>',
        /*'<img src="{[this.toLowerCaseExtension(values.base64code)]}" title="{name}" class="thumb"><div id="watermarktext2">{logoText}</div>',*/
        '<tpl if="logoUrl.length == 0">',
        '<img src="..{logoUrl}" style="visibility:hidden;"/>',
        '</tpl>',
        '<tpl if="logoUrl.length != 0 && rotated==false">',
        '<img src="..{logoUrl}" class="watermark {pictureSize}" title = "" alt=""/>',
        '</tpl>',
        '<tpl if="logoUrl.length != 0 && rotated==true">',
        '<img src="..{logoUrl}" class="watermarkrotated {pictureSize}" title = "" alt=""/>',
        '</tpl>',
        '<tpl if="logoMainUrl.length != 0 && rotated==false">',
        '<img src="..{logoMainUrl}" class="watermarkmain {pictureSize}" title = "" alt=""/>',
        '</tpl>',
        '<tpl if="logoMainUrl.length != 0 && rotated==true">',
        '<img src="..{logoMainUrl}" class="watermarkmainrotated {pictureSize}" title = "" alt=""/>',
        '</tpl>',
        '<tpl if="this.isPublicUser()">',
        '<img src="../../css/images/logo.png" class="watermarkopaque {rotated}" title = "" alt=""/>',
        '</tpl>',
        '</div>',
        '<div class="template_string"><span class="camera">{shortTag}</span><span class="date">{shortName}</span></div>',

        '<div class="picturetoolbar" style="clear: both;">',

        '<a href="javascript:;" class="rotateright">',
        '<img class="thumb-image" src="../../js/ux/images/custom/catalogactions/rotate_left.png" title="rotate left"></a>',
        '<a href="javascript:;" class="rotateleft">',
        '<img class="thumb-image" src="../../js/ux/images/custom/catalogactions/rotate_right.png" title="rotate right"></a>',


        '<a href="javascript:;" class="view">',
        '<img class="thumb-image" src="../../js/ux/images/custom/catalogactions/preview_picture.png" title="preview"></a>',
        '<tpl if="!this.isPublicUser()">',
        '<a href="javascript:;" class="edit">',
        '<img src="../../js/ux/images/custom/catalogactions/edit_picture.png" title="delete"></a></div>',
        '</tpl>',
        '<tpl if="this.isPublicUser()">',
        '</div>',
        '</tpl>',
        '</div>',
        '</tpl>',
        {
            isPublicUser: function () {
                return (GLOBAL.userId == 3 || GLOBAL.userId == 4);
            }
        }
    );

    Ext.namespace('Ext.ux');

    Ext.ux.PageSizePlugin = function () {
        Ext.ux.PageSizePlugin.superclass.constructor.call(this, {
            store: new Ext.data.SimpleStore({
                fields: ['text', 'value'],
                data: [
                    ['5', 5],
                    ['10', 10],
                    ['20', 20],
                    ['25', 25],
                    ['30', 30],
                    ['50', 50]
                ]
            }),
            mode: 'local',
            displayField: 'text',
            valueField: 'value',
            editable: false,
            allowBlank: false,
            triggerAction: 'all',
            width: 40
        });
    };

    /*PAGING TOOLBAR PLUGIN*/
    Ext.extend(Ext.ux.PageSizePlugin, Ext.form.ComboBox, {
        init: function (paging) {
            paging.on('render', this.onInitView, this);
            paging.addEvents('pagesizechanged');
        },
        onInitView: function (paging) {
            paging.add('Page size',
                this,
                '-'
            );
            this.setValue(paging.pageSize);
            this.on('select', this.onPageSizeChanged, paging);
            store4.on('beforeload', function (s) {
                s.setBaseParam('startdate', getValueFromSFByFieldName('startdt'));
                s.setBaseParam('starttime', getValueFromSFByFieldName('starttm'));
                s.setBaseParam('enddate', getValueFromSFByFieldName('enddt'));
                s.setBaseParam('endtime', getValueFromSFByFieldName('endtm'));
                s.setBaseParam('location', getValueFromSFByFieldName('id'));
                s.setBaseParam('camera', getValueFromSFByFieldName('cid'));
                s.setBaseParam('tag', getValueFromSFByFieldName('tcid'));
            });
        },
        onPageSizeChanged: function (combo) {
            this.pageSize = parseInt(combo.getValue());
            this.fireEvent('pagesizechanged', this, this.pageSize);
            this.doLoad(0);
        },
        listWidth: 40,
        shadow: false,
        autoScroll: false
    });

    function isPurchasePublic()
    {
        if (GLOBAL.userId == 6) {
            return true;
        }
    }

    /*PAGING TOOLBAR*/
    var pagingbar = new Ext.PagingToolbar({
        store: store4,
        pageSize: GLOBAL.pageSize,
        prependButtons: true,
        beforePageText: '',
        hideLabel: true,
        listeners: {
            change:function(that,pageData){
                limit = that.pageSize;
                page = pageData.activePage;

                reloadTagStore();
            },
            pagesizechanged: function (paging, pageSize) {
                store4.on('beforeload', function (s) {
                    s.setBaseParam('startdate', getValueFromSFByFieldName('startdt'));
                    s.setBaseParam('starttime', getValueFromSFByFieldName('starttm'));
                    s.setBaseParam('enddate', getValueFromSFByFieldName('enddt'));
                    s.setBaseParam('endtime', getValueFromSFByFieldName('endtm'));
                    s.setBaseParam('location', getValueFromSFByFieldName('id'));
                    s.setBaseParam('camera', getValueFromSFByFieldName('cid'));
                    s.setBaseParam('tag', getValueFromSFByFieldName('tcid'));
                });

                Ext.Ajax.request({
                    url: '../../getPicturesPageSizeUpdate.json',
                    method: 'POST',
                    params: {
                        id: 'id',
                        pageSize: pageSize
                    },
                    callback: function (options, success, response) {

                    }

                });
            }
        },
        items: [
            {
                xtype: "box",
                width: 17
            },
            {
                text: isPurchasePublic()?'Download':'Purchase',
                handler: function (btn, evt) {
                    Ext.Ajax.request({
                        url: '../../firstSavePurchase.json',
                        method: 'POST',
                        params: {
                            startdate: getValueFromSFByFieldName('startdt'),
                            starttime: getValueFromSFByFieldName('starttm'),
                            enddate: getValueFromSFByFieldName('enddt'),
                            endtime: getValueFromSFByFieldName('endtm'),
                            location: getValueFromSFByFieldName('id'),
                            camera: getValueFromSFByFieldName('cid'),
                            tag: getValueFromSFByFieldName('tcid'),
                            purchaseId: GLOBAL.purchaseId
                        },
                        callback: function (options, success, response) {
                            var genId = Ext.util.JSON.decode(response.responseText).purchaseId;

                            if(genId==null){
                                var genIdCat = Ext.util.JSON.decode(response.responseText).purchaseIdCatalog;
                                var lo = Ext.urlAppend(GLOBAL.contextPath + '/pictures/main/purchase', Ext.urlEncode({genCat: genIdCat}));
                                window.location = lo;
                            }else{
                                var lo = Ext.urlAppend(GLOBAL.contextPath + '/pictures/main/purchase', Ext.urlEncode({gen: genId}));
                                window.location = lo;
                            }
                        }
                    });
                }
            },
            {
                xtype: "box",
                width: 50
            },
            {
                text: 'Refresh',
                handler: function (btn, evt) {
                    store4.reload();
                    reloadTagStore();
                }
            },
            {
                xtype: "box",
                width: 50
            },
            {
                text: 'Select / Deselect',
                handler: function (btn, evt) {
                    Ext.Ajax.request({
                        url: '../../getPicturesSelectedDiselected.json',
                        method: 'POST',
                        params: {
                            data: store4.collect('id')
                        },
                        callback: function (options, success, response) {
                            store4.reload({
                                callback: function () {
                                    var l = dataview.selected.elements.length;
                                    var s = l != 1 ? 's' : '';
                                }
                            });

                        }
                    });
                }
            },
            {
                xtype: "box",
                width: 50
            },
            {
                text: 'Clear Selections',
                handler: function (btn, evt) {
                    Ext.Ajax.request({
                        url: '../../getPicturesClearSelections.json',
                        method: 'GET',
                        callback: function (options, success, response) {
                            store4.reload();
                            var l = 0;
                        }
                    });
                }
            },
            {
                xtype: "box",
                width: 50
            },
            {
                text: 'Trash',
                hidden: isPublicUser(),
                handler: function (btn, evt) {
                    Ext.Msg.confirm('Confirm Delete', 'Are you sure you want to delete pictures?', function (btnText) {
                        if (btnText == 'yes') {
                            Ext.Ajax.request({
                                url: '../../getPicturesDeleted.json',
                                method: 'POST',
                                params: {
                                    data: store4.collect('id')
                                },
                                callback: function (options, success, response) {
                                    store4.reload();
                                }
                            });
                        }
                    }, this);
                    return false; // Stop the delete request
                }
            },
            {
                xtype: "box",
                width: isPublicUser() ? 0 : 50
            },
            {
                text: 'Preview',
                handler: function (btn, evt) {
                    var lo = dataview.store.lastOptions || {};
                    var url = Ext.urlAppend(GLOBAL.contextPath + '/pictures/main/preview', Ext.urlEncode(lo.params || {})),
                        name = 'selpicslideshowwindow' + new Date().getMilliseconds();
                    var mywindow = window.open(
                        url,
                        name,
                        'scrollbars=yes,menubar=no,height=900,width=1600,resizable=yes,toolbar=no,location=no,status=no'
                    );
                    mywindow.moveTo(10, 10);
                    mywindow.focus();
                }
            },
            {
                xtype: "box",
                width: 50
            },
            {
                text: 'Slide Show',
                hidden: isPublicUser(),
                handler: slideshowHandler
            },
            {
                xtype: "box",
                hidden: !GLOBAL.photobook,
                width: 50
            },
            {
                xtype: 'box',
                width: 100,
                hidden: !GLOBAL.photobook,
                style: {
                    cursor: 'pointer'
                },
                autoEl: {
                    tag: 'img',
                    src: '../../js/ux/images/custom/purchaseactions/edge-imaging-logo.jpg'
                },
                listeners: {
                    afterrender: function(box){
                        box.el.on('click', function(){
                            var selected = [];

                            store4.each(function(record){
                                if(record.get('selected')){
                                    selected.push(record.get('id'));
                                }
                            })

                            if(selected.length){
                                var lo = Ext.urlAppend(GLOBAL.contextPath + '/pictures/main/photobook', Ext.urlEncode({
                                    startdate: getValueFromSFByFieldName('startdt'),
                                    starttime: getValueFromSFByFieldName('starttm'),
                                    enddate: getValueFromSFByFieldName('enddt'),
                                    endtime: getValueFromSFByFieldName('endtm'),
                                    location: getValueFromSFByFieldName('id'),
                                    camera: getValueFromSFByFieldName('cid'),
                                    tag: getValueFromSFByFieldName('tcid')
                                }));
                                window.location = lo;
                            }else{
                                Ext.MessageBox.alert('', "Please select photos for photo book");
                            }
                        });
                    }
                }
            },
            {
                xtype: "box",
                width: 100
            }
        ],
        plugins: [new Ext.ux.PageSizePlugin()]
    });
    pagingbar.refresh.hide();

    /*DATAVIEW*/
    var dataview = new Ext.DataView({
        store: store4,
        tpl: tpl,
        multiSelect: true,
        simpleSelect: true,
        overClass: 'x-view-over',
        itemSelector: 'div.thumb-wrap',
        emptyText: 'No images to display',
        style: 'border-top-width: 0',
        plugins: [
        ],
        prepareData: function (data) {
            data.shortName = Ext.util.Format.ellipsis(data.name, 15);
            data.shortTag = Ext.util.Format.ellipsis(data.tag, 15);
            data.dateString = data.creationDate;
            return data;
        },
        onContainerClick: function (e) {
        },
        listeners: {
            afterrender: function (dv) {
                dv.el.on('click', function (e, t) {
                    e.stopEvent();
                    var r = dv.getRecord(Ext.fly(t).findParent(dv.itemSelector));
                    var imageWindow = new ImageViewer({
                        title: r.get("name"),
                        src: r.data,
                        hideAction: 'close'
                    });
                    imageWindow.setPosition(580, e.xy[1] - 480);
                    imageWindow.show();
                    return false;
                }, this, {
                    delegate: 'a.view'
                });
                dv.el.on('click', function (e, t) {
                    e.stopEvent();
                    var r = dv.getRecord(Ext.fly(t).findParent(dv.itemSelector));
                    Ext.Msg.confirm('Confirm Delete', 'Are you sure you want to delete this picture', function (btnText) {
                        if (btnText == 'yes') {
                            Ext.Ajax.request({
                                url: '../../getPicturesDeleted.json',
                                method: 'POST',
                                params: {
                                    data: r.get('id'),
                                    singledeletion: true
                                },
                                callback: function (options, success, response) {
                                    store4.reload();
                                }
                            });
                        }
                    });
                }, this, {
                    delegate: 'a.edit'
                });
                dv.el.on('click', function (e, t) {
                    e.stopEvent();
                    var r = dv.getRecord(Ext.fly(t).findParent(dv.itemSelector));
                    //Ext.Msg.confirm('Confirm Rotate', 'Are you sure you want to rotate left this picture', function (btnText) {
                      //  if (btnText == 'yes') {
                            Ext.Ajax.request({
                                url: '../../getPicturesRotate.json',
                                method: 'POST',
                                params: {
                                    data: r.get('id'),
                                    direction: "left"
                                },
                                callback: function (options, success, response) {
                                    store4.reload();
                                }
                            });
                       // }
                   // });
                }, this, {
                    delegate: 'a.rotateleft'
                });
                dv.el.on('click', function (e, t) {
                    e.stopEvent();
                    var r = dv.getRecord(Ext.fly(t).findParent(dv.itemSelector));
                   // Ext.Msg.confirm('Confirm Rotate', 'Are you sure you want to rotate right this picture', function (btnText) {
                    //    if (btnText == 'yes') {
                            Ext.Ajax.request({
                                url: '../../getPicturesRotate.json',
                                method: 'POST',
                                params: {
                                    data: r.get('id'),
                                    direction: "right"
                                },
                                callback: function (options, success, response) {
                                    store4.reload();
                                }
                            });
                     //   }
                    //});
                }, this, {
                    delegate: 'a.rotateright'
                });
                dv.getStore().on('load', function (store, records, options) {
                    for (var i = 0; i < records.length; i++) {
                        if (records[i].get('selected')) {
                            dv.select(records[i], true);
                        }
                    }
                });
            },
            click: function (dv, index, node, e) {
                var l = dv.selected.elements.length;
                var s = l != 1 ? 's' : '';
                var r = dv.getRecord(node);
                r.set('selected', !r.get('selected'));
                dv.getStore().save();
            }
        }
    });

    var panel = new Ext.Panel({
        id: 'images-view',
        frame: true,
        title: 'Photo Catalog',

        items: [pagingbar, dataview]
    });

    /*BOTTOM PANEL ELEMENTS*/
    var purchaseBtn = new Ext.Button({
        id: 'purchBtn',
        text: isPurchasePublic()?'Download':'Purchase',
        handler: function () {
            Ext.Ajax.request({
                url: '../../firstSavePurchase.json',
                method: 'POST',
                params: {
                    startdate: getValueFromSFByFieldName('startdt'),
                    starttime: getValueFromSFByFieldName('starttm'),
                    enddate: getValueFromSFByFieldName('enddt'),
                    endtime: getValueFromSFByFieldName('endtm'),
                    location: getValueFromSFByFieldName('id'),
                    camera: getValueFromSFByFieldName('cid'),
                    tag: getValueFromSFByFieldName('tcid'),
                    purchaseId: GLOBAL.purchaseId
                },
                callback: function (options, success, response) {
                    var genId = Ext.util.JSON.decode(response.responseText).purchaseId;

                    if(genId==null){
                        var genIdCat = Ext.util.JSON.decode(response.responseText).purchaseIdCatalog;
                        var lo = Ext.urlAppend(GLOBAL.contextPath + '/pictures/main/purchase', Ext.urlEncode({genCat: genIdCat}));
                        window.location = lo;
                    }else{
                        var lo = Ext.urlAppend(GLOBAL.contextPath + '/pictures/main/purchase', Ext.urlEncode({gen: genId}));
                        window.location = lo;
                    }
                }
            });
        }
    });
    var refreshBtn = new Ext.Button({
        text: 'Refresh',
        handler: function () {
            store4.reload();
            reloadTagStore();
        }
    });

    var forwardBtn = new Ext.Button({
        cls: 'forward-active',
        height: 35,
        width: 30,
        handler: function () {
            if (pagingbar.cursor + pagingbar.pageSize < store4.getTotalCount()) {
                pagingbar.moveNext();
            }
        }
    });
    var backBtn = new Ext.Button({
        cls: 'back',
        height: 35,
        width: 30,
        handler: function () {
            if (pagingbar.cursor != 0) {
                pagingbar.movePrevious();
            }
        }
    });

    /*BOTTOM PANELs*/
    var panelBottom1 = new Ext.Panel({
        id: 'bottombuttons',
        height: 150,
        layout: 'column',
        layoutConfig: {
            columns: 2
        },
        items: [ purchaseBtn , refreshBtn]
    });

    var panelBottom2 = new Ext.Panel({
        id: 'bottombuttons-b-f',
        height: 150,
        layout: 'column',
        layoutConfig: {
            columns: 2
        },
        items: [backBtn, forwardBtn]
    });

    var panelBottom = new Ext.Panel({
        id: 'bottom',
        width: '93%',
        height: 150,
        layout: 'column',
        layoutConfig: {
            columns: 2
        },
        items: [
            {
                columnWidth: .90,
                items: panelBottom1
            },
            {
                columnWidth: .10,
                items: panelBottom2,
                style: {'float': 'right'}
            }
        ]
    });

    locationStore.load({
        callback: function () {
            locationCombo.setValue(GLOBAL.location);
            cameraCombo.setDisabled(false);
            cameraStore.load(
                {
                    params: {
                        'locationid': GLOBAL.location
                    },
                    callback: function () {
                        cameraCombo.setValue(GLOBAL.camera);
                        var params = getTagStoreParams();

                        tagStore.load({
                            params: params,
                            callback: function () {
                                if (GLOBAL.tag != "") {
                                    tagCombo.setValue(GLOBAL.tag);
                                }
                                checkTagsGrouping();
                            }
                        });
                    }
                });
        }
    });

    var htmlCataloText =!isCodeUser()?'Thank you for choosing fotaflo, to select your pictures follow these instructions:<ul>' +
        '<li>Find - your pictures by simply scrolling through the pictures on this page or other pages by clicking on the arrows at the bottom of this page.</li>' +
        '<li>Select - your pictures by clicking on the image</li>' +
        '<li>Review - the pictures by clicking on the preview button or the little magnifying glass under the picture</li>' +
        '<li>to purchase page by clicking on the download button after you have selected the pictures you would like to purchase.</li></ul>'
        :
        'Thank you for choosing Fotaflo, to select your pictures follow these instructions:<ul>' +
        '<li>Find - your pictures by simply scrolling through the pictures on this page or other pages by clicking on the arrows at the bottom of this page.</li>' +
        '<li>Select - your pictures by clicking on the image</li>' +
        '<li>Review - the pictures by clicking on the preview button or the little magnifying glass under the picture</li>' +
        '<li>Continue - continue to download page by clicking on the download button after you have selected the pictures you would like to download</li>' +
        '<li>FYI - If you change your mind you can cancel your download once you are on the download page </li></ul>';

    var explanationPanel = new Ext.Panel({
        id: 'explanation_panel',
        frame: true,
        hidden: !isPublicUser(),
        width: '100%',
        height: 100,
        items: [
            {
                html: htmlCataloText
            }
        ]
    });

    var filterCatalog = function(record){
        if(!GLOBAL.slideshowStartTime){
            return;
        }
        var startDate = new Date().add(Date.SECOND, - GLOBAL.slideshowStartTime),
            endDate = new Date(),
            tagId = record.get('id');

        loadTagStore(function(){
            searchForm.getForm().setValues({
                'cid': '0',
                'tcid': tagId,
                'startdt': startDate,
                'starttm': startDate,
                'enddt': endDate,
                'endtm': endDate
            });

            submitHandler();
        });
    }

    var rfidTagStore = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            url: '../rfid/tags'
        }),
        reader: new Ext.data.JsonReader({
            root: 'data',
            idProperty: 'id'
        },[
            {
                name: 'id'
            },
            {
                name: 'tagIdName'
            },
            {
                name: 'location'
            },
            {
                name: 'date', convert: function(v, r){
                    var value = r['creationDate'];
                    return Ext.isDate(value)?value:new Date(value);
                }
            },
            {
                name: 'creationDate'
            }
        ])
    });

    var rfid = new Ext.grid.GridPanel({
        id:'rfidGrid',
        title: 'Tags for slideshow',
        baseCls: 'grid-view',
        collapsible: true,
        collapsed: true,
        hidden: !GLOBAL.rfidEnabled,
        frame: true,
        width: 500,
        height: 180,
        viewConfig: {
            forceFit: true
        },
        cm: new Ext.grid.ColumnModel({
            defaults: {
                menuDisabled:true
            },
            columns: [{
                header: 'Catalog',
                dataIndex: 'id',
                align: 'center',
                width: 50,
                resizable: false,
                renderer: function (v, p, r)
                {
                    return '<div class="request_action column_action" style="margin: auto; padding: 4px">' +
                        '<img class="column_action" src="../../js/ux/images/custom/requestactions/add.png">' +
                        '</div>';
                }
            },{
                header: 'Tag',
                dataIndex: 'tagIdName'
            },{
                header: 'Date',
                xtype: 'datecolumn',
                format: 'm/d/Y',
                dataIndex: 'date',
                width: 90
            },{
                header: 'Time',
                xtype: 'datecolumn',
                format: 'h:i a',
                dataIndex: 'date',
                width: 70
            }]
        }),
        sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
        store: rfidTagStore,
        tools:[{
            id: 'camera',
            hidden: !GLOBAL.cameraEnabled,
            qtip: 'QR code scanning',
            handler: function(event, toolEl, panel){
                var url = GLOBAL.contextPath + '/pictures/main/qrcode',
                    name = 'qrcodecam';

                var mywindow = window.open(
                    url,
                    name,
                    'scrollbars=no,menubar=no,height=400,width=400,resizable=no,toolbar=no,location=no,status=no'
                );
                mywindow.moveTo(10, 10);
                mywindow.focus();

                Ext.get(window.document).addListener("showslideshow", function(){
                    mywindow.hideMask();
                });
            }
        }]
    });
    rfid.on({
        'cellclick' : function(grid, rowIndex, columnIndex, e) {
            var target = Ext.fly(e.target);
            if(target.hasClass('column_action')){
                var record = grid.getStore().getAt(rowIndex);
                if (columnIndex == 0){
                    filterCatalog(record);
                }
            }
        },
        scope: this
    });

    rfid.render('content')
    explanationPanel.render('content');
    searchForm.render('content');
    panel.render('content');
    panelBottom.render('content');

    if(GLOBAL.rfidEnabled){
        var slideshowUpdater,
            timeoutId,
            myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Please wait..."});

        var getSlideshowUpdateParams = function(){
            return rfid.getStore().getAt(0) != null ? rfid.getStore().getAt(0).data : {};
        }

        var fireEvent = function(event){
            if (document.createEventObject) {
                var eventObj = document.createEventObject();
                document.fireEvent('on' + event, eventObj);
            } else {
                var eventObj = document.createEvent('MouseEvents');
                eventObj.initMouseEvent(event, true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
                document.dispatchEvent(eventObj);
            }
        };

        var showSlideshow = function(){
            var showInNewWindow = false;
            if (GLOBAL.hasOwnProperty('slideShowInNewWindow')) {
                showInNewWindow = GLOBAL['slideShowInNewWindow'];
            }
            var lo = dataview.store.lastOptions || {};
            var url = Ext.urlAppend(GLOBAL.contextPath + '/pictures/rfid/slideshow', Ext.urlEncode(lo.params || {})),
                name = 'rfidslideshowwindow' + (showInNewWindow ? new Date ().getMilliseconds () : '');

            var mywindow = window.open(
                url,
                name,
                'scrollbars=yes,menubar=no,height=900,width=1600,resizable=yes,toolbar=no,location=no,status=no'
            );
            mywindow.moveTo(10, 10);
            mywindow.focus();

            fireEvent("showslideshow");

            setTimeout(function(){
                var element = mywindow.Ext.getCmp(mywindow.play.id);
                var s = mywindow.slideshow.togglePlayback() ? 'Pause' : 'Play';
                element.setText(s);
            }, 3000);
        }

        var slideshowUpdateHandler = function(result){
            var startDate = getSlideshowUpdateParams().date,
                date,
                current = new Date(),
                delay;

            if(startDate && !Ext.isDate(startDate)){
                startDate = new Date(startDate);
            }

            if(result.data && result.data.length){
                var tag;
                for(var i=result.data.length-1; i>=0; i--){
                    tag = result.data[i];
                    if(tag && tag.creationDate){
                        date = new Date(tag.creationDate);
                        if(date > startDate){
                            break;
                        }
                    }
                }
            }

            startDate = new Date(date.getTime() + GLOBAL.slideshowDelay*1000)
            delay = startDate.getTime() - current.getTime();

            if(delay < 0){
                delay = 0;
            }
            rfidTagStore.loadData(result);

            if(timeoutId){
                clearTimeout(timeoutId);
            }

            timeoutId = setTimeout(function(){
                showSlideshow();
            }, delay);
        }

        rfidTagStore.load({callback: function(){
            slideshowUpdater = new UpdateListener(GLOBAL.slideshowDelay, GLOBAL.contextPath + '/pictures/rfid/update',
                slideshowUpdateHandler, getSlideshowUpdateParams);
        }});

        var now = new Date(),
            clearTime = new Date();
        if(clearTime.getHours() >= 5){
            clearTime = clearTime.add(Date.DAY, 1);
        }
        clearTime.clearTime();
        clearTime = clearTime.add(Date.HOUR, 5);

        setTimeout(function(){
            rfidTagStore.load();
        }, clearTime.getTime() - now.getTime() + 1000);
    }
});
