var lastEventTime = new Date().getTime();

window.onload = function() {
    document.addEventListener("click", function() {
        if ((new Date().getTime() - lastEventTime) > (14 * 60 * 60 * 1000)) {
            var path = window.location.pathname;
            window.location = path.substring(0, path.indexOf('/pictures'));
        }
        lastEventTime = new Date().getTime();
    })
};