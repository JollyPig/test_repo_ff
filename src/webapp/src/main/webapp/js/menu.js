
Ext.onReady(function () {
    var menuSettings = new Ext.menu.Menu({
        id: 'settingsMenu',
        cls: 'dropmenuitem',
        disabled: isPurchase(),
        style: {
            overflow: 'visible'     // For the Combo popup
        },
        items: [
            {
                text: 'General settings',
                href: GLOBAL.contextPath + '/pictures/main/settings'
            },
            {
                text: 'Import/Export',
                href: GLOBAL.contextPath + '/pictures/main/importexport'
            },
            {
                text: 'Public login generation',
                href: GLOBAL.contextPath + '/pictures/main/publicgenerate'
            },
            {
                text: 'Generate QR tag',
                href: GLOBAL.contextPath + '/pictures/main/qrtaggeneration'
            },
            {
                text: 'Image Metadata Settings',
                href: GLOBAL.contextPath + '/pictures/main/imagemetadata'
            }

        ]
    });

    var menuSaved = new Ext.menu.Menu({
        id: 'saveMenu',
        cls: 'dropmenuitem',
        disabled: isPurchase(),
        style: {
            overflow: 'visible'     // For the Combo popup
        },
        items: [
            {
                text: 'Saved Purchases',
                href: GLOBAL.contextPath + '/pictures/main/savedPurchases'
            },
            {
                text: 'Purchase History',
                href: GLOBAL.contextPath + '/pictures/main/purchaseHistory'
            },
            {
                text: 'Refunded Purchases',
                href: GLOBAL.contextPath + '/pictures/main/refundedPurchases',
                hidden: !isTagApprovalEnabled()
            }
        ]
    });


    function isUser() {
        if (GLOBAL.userId != 1) {
            return true;
        }
    }

    function isPublicUser() {
        if (GLOBAL.userId == 3 || GLOBAL.userId == 4 || GLOBAL.userId == 5 || GLOBAL.userId == 6) {
            return true;
        }
    }


    function isTagApprovalEnabled() {
           if(GLOBAL.tagApp=="true" && !isPublicUser() ){
               return true;
           }else{
               return false;
           }

    }

    function isPurchase() {
        if (GLOBAL.isPurchase != null) {
            return GLOBAL.isPurchase;
        }
    }

    var menuAdministration = new Ext.menu.Menu({
        id: 'administrationMenu',
        cls: 'dropmenuitem',
        style: {
            overflow: 'visible'     // For the Combo popup
        },
        items: [
            {
                text: 'User management',
                href: GLOBAL.contextPath + '/pictures/main/usermanagement'
            },
            {
                text: 'Locations',
                href: GLOBAL.contextPath + '/pictures/main/locations'
            },
            {
                text: 'Packages',
                href: GLOBAL.contextPath + '/pictures/main/packages'
            },
            {
                text: 'Cameras',
                href: GLOBAL.contextPath + '/pictures/main/cameras'
            },
            {
                text: 'Report Settings',
                href: GLOBAL.contextPath + '/pictures/main/reports'
            },
            {
                text: 'Email Footer Settings',
                href: GLOBAL.contextPath + '/pictures/main/emailfooter'
            },
            {
                text: 'Public Email Content Settings',
                href: GLOBAL.contextPath + '/pictures/main/publicemailcontent'
            },
            {
                text: 'Social Network and Email Management',
                href: GLOBAL.contextPath + '/pictures/main/emailsettings'
            },
            {
                text: 'Main Logo Settings',
                href: GLOBAL.contextPath + '/pictures/main/mainlogomanagement'
            },
            {
                text: 'Banners Settings',
                href: GLOBAL.contextPath + '/pictures/main/banners'
            },
            {
                text: 'PayPal Settings',
                href: GLOBAL.contextPath + '/pictures/main/paypalsettings'
            },
            {
                text: 'PayPal Merchant Account Settings',
                href: GLOBAL.contextPath + '/pictures/main/merchantsettings'
            },
            {
                text: 'Export Emails',
                href: GLOBAL.contextPath + '/pictures/main/exportemails'
            },
            {
                text: 'Public Reports',
                href: GLOBAL.contextPath + '/pictures/main/exportpublicreport'
            },
            {
                text: 'Attachments Settings',
                href: GLOBAL.contextPath + '/pictures/main/attachmentsettings'
            }

        ]
    });

    var menuContact = new Ext.menu.Menu({
        id: 'contactMenu',
        cls: 'dropmenuitem',
        disabled: isPurchase(),
        style: {
            overflow: 'visible'     // For the Combo popup
        },
        items: [
            {
                text: 'Contact',
                href: GLOBAL.contextPath + '/pictures/main/contact'
            },
            {
                text: 'Contact Administration',
                href: GLOBAL.contextPath + '/pictures/main/contactadmin',
                hidden: isUser()
            }

        ]
    });

    var tb = new Ext.Toolbar();
    tb.cls = 'menuitem';
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
        tb.addClass('toolbar-tablet');
    }
    tb.add({
                xtype: 'button',
                text: 'Catalog',
                disabled: isPurchase(),
                listeners: {
                    click: function (b, e) {
                        var url = Ext.urlAppend(GLOBAL.contextPath + '/pictures/main/common', Ext.urlEncode({getFilter: "load"}));
                        window.location = url;
                    }
                }
            },
            {
                xtype: 'button',
                text: 'Purchase',
                listeners: {
                    click: function (b, e) {
                        var btn = document.getElementById('purchBtn');
                        btn.click();
                    }
                }
            }/*,
             {
             xtype: 'button',
             text: 'Request',
             disabled: isPurchase(),
             listeners:{
             click : function(b, e) {
             var url = Ext.urlAppend(GLOBAL.contextPath + '/pictures/main/requests', Ext.urlEncode({}));
             window.location = url;
             }
             }
             }*/
            /*{
             text:'Settings',
             disabled: isPurchase(),
             menu: menuSettings  // assign menu by instance
             }*/
        );

    if (!isPublicUser() && isTagApprovalEnabled()) {
        tb.add({
            xtype: 'button',
            text: 'Tag Approval',
            disabled: isPurchase(),
            listeners: {
                click: function (b, e) {
                    var url = Ext.urlAppend(GLOBAL.contextPath + '/pictures/main/tagapproval', Ext.urlEncode({}));
                    window.location = url;
                }
            }
        });
    }


    if (!isPublicUser()) {
        tb.add({
            xtype: 'button',
            text: 'Request',
            disabled: isPurchase(),
            listeners: {
                click: function (b, e) {
                    var url = Ext.urlAppend(GLOBAL.contextPath + '/pictures/main/requests', Ext.urlEncode({}));
                    window.location = url;
                }
            }
        });
    }

    if (!isPublicUser()) {
        tb.add({
            text: 'Saved Items',
            disabled: isPurchase(),
            menu: menuSaved  // assign menu by instance
        });
    }

    if (!isPublicUser()) {
        tb.add({
            text: 'Settings',
            disabled: isPurchase(),
            menu: menuSettings  // assign menu by instance
        });
    }


    if (!isUser()) {
        tb.add({
            text: 'Administration',
            menu: menuAdministration,
            disabled: isPurchase()
        });

    }
    tb.add({
        text: 'Contact',
        menu: menuContact,
        disabled: isPurchase()
    });

    tb.render('menu');

});

