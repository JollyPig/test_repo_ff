addWindow = Ext.extend(Ext.Window, {


height: 334,
width: 540,
layout: {
    type: 'border'
},
title: 'Run report',

initComponent: function() {
    var me = this;

    Ext.applyIf(me, {
        items: [
            {
                xtype: 'form',
                bodyPadding: 10,
                region: 'center'
            }
        ]
    });

   /* me.callParent(arguments);*/
}});
