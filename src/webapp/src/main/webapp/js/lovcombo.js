// Check RegExp.escape dependency
    if ('function' !== typeof RegExp.escape) {
        throw('RegExp.escape function is missing. Include Ext.ux.util.js file.');
    }

    // create namespace
    Ext.ns('Ext.ux.form');

    /**
     * Creates new LovCombo
     * @constructor
     * @param {Object} config A config object
     */
    Ext.ux.form.LovCombo = Ext.extend(Ext.form.ComboBox, {
        /**
         * @cfg {String} checkField Name of field used to store checked state.
         * It is automatically added to existing fields.
         * (defaults to "checked" - change it only if it collides with your normal field)
         */
        checkField:'checked',
        shadow : false

        /* on filtering combobox values, set filter by any match. method Ext.form.ComboBox.doQuery must be overridden */
        ,queryAnyMatch: false

        /**
         * @cfg {String} separator Separator to use between values and texts (defaults to "," (comma))
         */
        ,separator:','

        /**
         * @cfg {String/Array} tpl Template for items.
         * Change it only if you know what you are doing.
         */
        ,constructor:function(config) {
            config = config || {};
            config.listeners = config.listeners || {};
            Ext.applyIf(config.listeners, {
                scope:this
                ,beforequery:this.onBeforeQuery
                ,blur:this.onRealBlur
            });
            Ext.ux.form.LovCombo.superclass.constructor.call(this, config);
        }
        ,initComponent:function() {

            // template with checkbox
            if (!this.tpl) {
                this.tpl =
                        '<tpl for=".">'
                                + '<div class="x-combo-list-item">'
                                + '<img src="' + Ext.BLANK_IMAGE_URL + '" '
                                + 'class="ux-lovcombo-icon ux-lovcombo-icon-'
                                + '{[values.' + this.checkField + '?"checked":"unchecked"' + ']}">'
                                + '<div class="ux-lovcombo-item-text">{' + (this.displayField || 'text' ) + ':htmlEncode}</div>'
                                + '</div>'
                                + '</tpl>'
                        ;
            }
            // call parent
            Ext.ux.form.LovCombo.superclass.initComponent.apply(this, arguments);
            // remove selection from input field
            this.onLoad = this.onLoad.createSequence(function() {
                if (this.el) {
                    var v = this.el.dom.value;
                    this.el.dom.value = '';
                    this.el.dom.value = v;
                }
            });
        } // eo function initComponent
        /**
         * Disables default tab key bahavior
         * @private
         */
        ,initEvents:function() {
            Ext.ux.form.LovCombo.superclass.initEvents.apply(this, arguments);

            // disable default tab handling - does no good
            this.keyNav.tab = false;

        } // eo function initEvents
        /**
         * Clears value
         */
        ,clearValue:function() {
            this.value = '';
            this.setRawValue(this.value);
            this.store.clearFilter();
            this.store.each(function(r) {
                r.set(this.checkField, false);
            }, this);
            if (this.hiddenField) {
                this.hiddenField.value = '';
            }
            this.applyEmptyText();
        } // eo function clearValue
        /**
         * @return {String} separator (plus space) separated list of selected displayFields
         * @private
         */
        ,getCheckedDisplay:function() {
            var re = new RegExp(this.separator, "g");
            return this.getCheckedValue(this.displayField).replace(re, this.separator + ' ');
        } // eo function getCheckedDisplay

        /**
         * @return {String} separator separated list of selected valueFields
         * @private
         */
        ,getCheckedValue:function(field) {
            field = field || this.valueField;
            var c = [];

            // store may be filtered so get all records
            var snapshot = this.store.snapshot || this.store.data;

            snapshot.each(function(r) {
                if (r.get(this.checkField)) {
                    c.push(r.get(field));
                }
            }, this);

            return c.join(this.separator);
        } // eo function getCheckedValue
        // }}}
        // {{{
        /**
         * <override>
         *
         * beforequery event handler - handles multiple selections
         * @param {Object} qe query event
         * @private
         */
        ,doQuery: function(q, forceAll){
            q = Ext.isEmpty(q) ? '' : q;
            var qe = {
                query: q,
                forceAll: forceAll,
                combo: this,
                cancel:false
            };
            if(this.fireEvent('beforequery', qe)===false || qe.cancel){
                return false;
            }
            q = qe.query;
            forceAll = qe.forceAll;
            if(forceAll === true || (q.length >= this.minChars)){
                if(this.lastQuery !== q){
                    this.lastQuery = q;
                    if(this.mode == 'local'){
                        this.selectedIndex = -1;
                        if(forceAll){
                            this.store.clearFilter();
                        }else{
                            this.store.filter(this.displayField, q, this.queryAnyMatch);
                        }
                        this.onLoad();
                    }else{
                        this.store.baseParams[this.queryParam] = q;
                        this.store.load({
                            params: this.getParams(q)
                        });
                        this.expand();
                    }
                }else{
                    this.selectedIndex = -1;
                    this.onLoad();
                }
            }
        }
        ,onBeforeQuery:function(qe) {
            qe.query = qe.query.replace(new RegExp(RegExp.escape(this.getCheckedDisplay()) + '[ ' + this.separator + ']*'), '');
        } // eo function onBeforeQuery
        // }}}
        // {{{
        /**
         * blur event handler - runs only when real blur event is fired
         * @private
         */
        ,onRealBlur:function() {
            this.list.hide();
            var rv = this.getRawValue();
            var rva = rv.split(new RegExp(RegExp.escape(this.separator) + ' *'));
            var va = [];
            var snapshot = this.store.snapshot || this.store.data;

            // iterate through raw values and records and check/uncheck items
            Ext.each(rva, function(v) {
                snapshot.each(function(r) {
                    if (v === r.get(this.displayField)) {
                        va.push(r.get(this.valueField));
                    }
                }, this);
            }, this);
            this.setValue(va.join(this.separator));
            this.store.clearFilter();
        } // eo function onRealBlur

        /**
         * Combo's onSelect override
         * @private
         * @param {Ext.data.Record} record record that has been selected in the list
         * @param {Number} index index of selected (clicked) record
         */
        ,onSelect:function(record, index) {
            if (this.fireEvent('beforeselect', this, record, index) !== false) {

                // toggle checked field
                record.set(this.checkField, !record.get(this.checkField));

                // display full list
                if (this.store.isFiltered()) {
                    this.doQuery(this.allQuery);
                }

                // set (update) value and fire event
                this.setValue(this.getCheckedValue());
                this.fireEvent('select', this, record, index);
            }
        } // eo function onSelect

        /**
         * Sets the value of the LovCombo. The passed value can by a falsie (null, false, empty string), in
         * which case the combo value is cleared or separator separated string of values, e.g. '3,5,6'.
         * @param {Mixed} v value
         */
        ,setValue:function(v) {
            if (v) {
                v = '' + v;
                if (this.valueField) {
                    this.store.clearFilter();
                    this.store.each(function(r) {
                        var checked = !(!v.match(
                                '(^|' + this.separator + ')' + RegExp.escape(r.get(this.valueField))
                                        + '(' + this.separator + '|$)'))
                                ;

                        r.set(this.checkField, checked);
                    }, this);
                    this.value = this.getCheckedValue();
                    this.setRawValue(this.getCheckedDisplay());
                    if (this.hiddenField) {
                        this.hiddenField.value = this.value;
                    }
                }
                else {
                    this.value = v;
                    this.setRawValue(v);
                    if (this.hiddenField) {
                        this.hiddenField.value = v;
                    }
                }
                if (this.el) {
                    this.el.removeClass(this.emptyClass);
                }
            }
            else {
                this.clearValue();
            }
        } // eo function setValue

        /**
         * Selects all items
         */
        ,selectAll:function() {
            this.store.each(function(record) {
                // toggle checked field
                record.set(this.checkField, true);
            }, this);

            //display full list
            this.doQuery(this.allQuery);
            this.setValue(this.getCheckedValue());
        } // eo full selectAll

        /**
         * Deselects all items. Synonym for clearValue
         */
        ,deselectAll:function() {
            this.clearValue();
        } // eo full deselectAll


    }); // eo extend

    // register xtype
    Ext.reg('lovcombo', Ext.ux.form.LovCombo);