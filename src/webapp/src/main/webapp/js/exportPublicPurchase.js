Ext.onReady(function() {

    var storeLocation = new Ext.data.Store({
        autoLoad: true,
        proxy: new Ext.data.HttpProxy({
            api: {
                read: '../../locations/getActualLocations.json'
            }
        }),
        reader: new Ext.data.JsonReader({
            root:'locations'
        },[{name: 'id'},{name: 'locationName'}])
    });

    var startYear = 2011,
        currentDate = new Date(),
        dateFormat = 'Y-m-d';

    var getWeekMap = function(date){
        var m = [],
            f = dateFormat,
            firstDateOfMonth = date.getFirstDateOfMonth(),
            firstDay = firstDateOfMonth.getDay(),
            firstDate = firstDateOfMonth.getDate(),
            lastDateOfMonth = date.getLastDateOfMonth(),
            lastDate = lastDateOfMonth.getDate(),
            cd,ds,de,wo;

        cd = firstDate;
        wo = 7 - (firstDay + 1);
        ds = firstDateOfMonth.clearTime(true);
        ds = ds.add(Date.DAY, wo - 6);  //
        de = ds.add(Date.DAY, 6);
        m.push({ value: ds, name: ds.format(f) +"/" + de.format(f)});
        cd += wo;
        while(cd <= lastDate){
            ds = de.add(Date.DAY, 1);
            cd += 7;
            de = ds.add(Date.DAY, 6);
            /*if(cd <= lastDate){
                de = ds.add(Date.DAY, 6);
            }else{
                de = lastDateOfMonth;
            }*/
            m.push({ value: ds, name: ds.format(f) +"/" + de.format(f)});
        }

        return m;
    }

    var updateWeek = function(form){
        var f = dateFormat,
            year = form.findField('year').getValue(),
            month = form.findField('month').getValue(),
            weekField = form.findField('week'),
            date, map;
        date = Date.parseDate(year+'-'+(month < 10 ? '0'+month : month)+'-01', f);
        map = getWeekMap(date);

        weekField.getStore().loadData(map);
        weekField.setValue(map[0].value);
    }

    var updateFullDate = function(form){
        var f = dateFormat,
            type = form.getValues().type,
            year = form.findField('year').getValue(),
            month = form.findField('month').getValue(),
            week = form.findField('week').getValue(),
            date = form.findField('date').getValue(),
            fullDateField = form.findField('fullDate'),
            fd;

        if(type == 'daily'){
            fd = date;
        }else if(type == 'weekly'){
            fd = week;
        }else if(type == 'monthly'){
            fd = Date.parseDate(year+'-'+(month < 10 ? '0'+month : month)+'-01', f);
        }else{
            return;
        }

        fullDateField.setValue(fd.toISOString());
    }

    var generateHandler = function(){
        var form = exportPanel.getForm(),
            values;
        updateFullDate(form);
        values = form.getValues();
        console.log(values)
        if(form.isValid()){
            form.submit();
        }else{
            Ext.Msg.alert('Alert!', 'All fields are required! Please, fill them in');
        }
    };

    var exportPanel = new Ext.form.FormPanel({
        standardSubmit: true,
        url: '../../generateReport.json',
        method: 'GET',
        layout: 'form',
        width: 450,
        defaults: {
            border: false,
            style: {
                marginBottom: 0
            },
            padding: 2
        },
        items: [{
            xtype: 'fieldset',
            anchor: '100%',
            items: [{
                xtype: 'radiogroup',
                fieldLabel: 'Report type',
                items: [
                    {boxLabel: 'Public', name: 'type', inputValue: 'public', checked: true},
                    {boxLabel: 'Daily', name: 'type', inputValue: 'daily'},
                    {boxLabel: 'Weekly', name: 'type', inputValue: 'weekly'},
                    {boxLabel: 'Monthly', name: 'type', inputValue: 'monthly'}
                ],
                listeners:{
                    change: function(item, checked){
                        var form = item.findParentByType('form').getForm(),
                            value = checked.getRawValue();

                        var displayField = function(field, display){
                            if(field.rendered){
//                                field.setVisible(display);
                                field.setDisabled(!display);
                                field.getEl().up('.x-form-item').setDisplayed(display);
                            }else{
                                field.on('afterrender', function(){
                                    displayField(field, display);
                                });
                            }
                        }

                        var filterFields = function(df,dt,y,m,w,d){
                            displayField(form.findField('dateFrom'), df);
                            displayField(form.findField('dateTo'), dt);
                            displayField(form.findField('year'), y);
                            displayField(form.findField('month'), m);
                            displayField(form.findField('week'), w);
                            displayField(form.findField('date'), d);
                            form.findField('fullDate').setDisabled(!(y||m||w||d));
                        }

                        switch (value){
                            case 'public': filterFields(true,true,false,false,false,false); break;
                            case 'daily': filterFields(false,false,false,false,false,true); break;
                            case 'weekly': filterFields(false,false,true,true,true,false); break;
                            case 'monthly': filterFields(false,false,true,true,false,false); break;
                            default: break;
                        }
                    },
                    afterrender: function(item){
                        var form = item.findParentByType('form');
                        item.fireEvent('change', item, item.getValue());
                    }
                }
            }]
        },{
            xtype: 'fieldset',
            anchor: '70%',
            defaults: {
                anchor: '100%',
                allowBlank: false,
                format: dateFormat
            },
            items: [{
                xtype: 'combo',
                fieldLabel: 'Location',
                displayField: 'locationName',
                valueField: 'id',
                hiddenName : 'locationId',
                mode: 'local',
                store: storeLocation,
                forceSelection: true,
                triggerAction: 'all',
                emptyText:'Select a location...',
                selectOnFocus:true,
                editable: false,
                shadow : false
            },{
                xtype: 'datefield',
                cls: 'date-field',
                fieldLabel: 'from Date',
                name: 'dateFrom',
                value: currentDate.add(Date.MONTH, -1),
                showToday: false
            },{
                xtype: 'datefield',
                cls: 'date-field',
                fieldLabel: 'to Date',
                name: 'dateTo',
                value: currentDate,
                showToday: false
            },{
                xtype:          'combo',
                mode:           'local',
                value:          currentDate.getFullYear(),
                triggerAction:  'all',
                forceSelection: true,
                editable:       false,
                fieldLabel:     'Year',
                name:           'year',
                displayField:   'name',
                valueField:     'value',
                store:          new Ext.data.JsonStore({
                    fields : ['name', 'value'],
                    data   : function(start, end){
                        var a = [];

                        if(start <= end){
                            for(var i = start; i <= end; i++){
                                a.push({name: i, value: i});
                            }
                        }

                        return a;
                    }(startYear, currentDate.getFullYear())
                }),
                listeners:{
                    select: function(item, value){
                        var form = item.findParentByType('form').getForm();
                        if(form.getValues().type == 'weekly'){
                            updateWeek(form);
                        }
                    }
                }
            },{
                xtype:          'combo',
                mode:           'local',
                value:          currentDate.getMonth() + 1,
                triggerAction:  'all',
                forceSelection: true,
                editable:       false,
                fieldLabel:     'Month',
                name:           'month',
                displayField:   'name',
                valueField:     'value',
                store:          new Ext.data.JsonStore({
                    fields : ['name', 'value'],
                    data   : function(){
                        var a = [];

                        for(var i = 0; i <= 11; i++){
                            a.push({name: Date.monthNames[i], value: (i + 1)});
                        }

                        return a;
                    }()
                }),
                listeners:{
                    select: function(item, value){
                        var form = item.findParentByType('form').getForm();
                        if(form.getValues().type == 'weekly'){
                            updateWeek(form);
                        }
                    }
                }
            },{
                xtype:          'combo',
                mode:           'local',
                triggerAction:  'all',
                forceSelection: true,
                editable:       false,
                fieldLabel:     'Week',
                name:           'week',
                displayField:   'name',
                valueField:     'value',
                store:          new Ext.data.JsonStore({
                    fields : ['name', 'value']
                }),
                listeners:{
                    afterrender: function(item){
                        var form = item.findParentByType('form').getForm();
                        updateWeek(form);
                    }
                }
            },{
                xtype: 'datefield',
                cls: 'date-field',
                fieldLabel: 'Date',
                name: 'date',
                value: currentDate,
                showToday: false
            },{
                xtype: 'hidden',
                name: 'fullDate'
            }]
        },{
            xtype: 'button',
            text: 'Generate',
            handler: generateHandler
        }]
    });

    var forms = new Ext.Panel({
        title: 'Administration - Public Purchase Report',
        id: 'purch-view',
        layout: 'table',
        width: '100%',
        layoutConfig: {
            columns: 1,
            tableAttrs: {
                style: {width: '100%'}
            }
        },
        items: [ exportPanel ]
    });

    forms.render('content');

});

