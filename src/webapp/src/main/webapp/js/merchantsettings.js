Ext.onReady(function() {

    /**
     * Merchant Settings
     */

    var accountTextfield = new Ext.form.TextField({
        name: 'account',
        id: 'account',
        width: 240,
        allowBlank:false
    });

    var passwordTextfield = new Ext.form.TextField({
        inputType: 'password',
        name: 'password',
        id: 'password',
        width: 240,
        allowBlank:false
    });

    var signatureTextfield = new Ext.form.TextField({
        inputType: 'password',
        xtype: 'textfield',
        name: 'signature',
        id: 'signature',
        width: 400,
        allowBlank:false
    });

    var environmentTextfield = new Ext.form.TextField({
        xtype: 'textfield',
        name: 'environment',
        id: 'environment',
        width: 100,
        allowBlank:false
    });

   


    /*locationCombo.on('select', function(box, record, index) {
        var data = record.get('id');
        Ext.Ajax.request({
            url: '../../getPayPalMerchantSettings.json',
            params:{
                'locationId': data
            },
            success: function (result, request) {
                var ids = result.responseText;
                var array = Ext.util.JSON.decode(ids);
                var account = array.account;
                var password = array.password;
                var signature = array.signature;
                var environment = array.environment;
                accountTextfield.setValue(account);
                passwordTextfield.setValue(password);
                signatureTextfield.setValue(signature);
                environmentTextfield.setValue(environment);
            },
            failure: function (result, request) {
                Ext.MessageBox.alert('Failed', result.responseText);
            }
        });

    });*/


    var merchantPanel = new Ext.Panel({
        title: 'Merchant account',
        id: 'merchantPanel',
        height   : 'auto',
        layout: 'table',
        layoutConfig: {
            columns: 2
        },
        items: [
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'Account'
            },
            accountTextfield,
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'password'
            },
            passwordTextfield,
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'signature'
            },
            signatureTextfield,
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'environment'
            },
            environmentTextfield           
        ],
         listeners: {
            beforerender: function(pn) {
                Ext.Ajax.request({
                    url: '../../getMerchantSettings.json',
                    callback: function(options, success, response) {
                        var account = Ext.util.JSON.decode(response.responseText).account;
                        mainPanel.getForm().findField('account').setValue(account);
                        var password = Ext.util.JSON.decode(response.responseText).password;
                        mainPanel.getForm().findField('password').setValue(password);
                        var signature = Ext.util.JSON.decode(response.responseText).signature;
                        mainPanel.getForm().findField('signature').setValue(signature);
                        var environment = Ext.util.JSON.decode(response.responseText).environment;
                        mainPanel.getForm().findField('environment').setValue(environment);
                    }
                });
            }

        }
    });

    /**
     * Button Panel
     */
    var saveButton = new Ext.Button({
        text: 'Save',
        handler: function() {
            var errorMassage  = "";
            var elem1 = document.getElementById("accountTextfield");
            var elem2 = document.getElementById("passwordTextfield");
            var elem3 = document.getElementById("signatureTextfield");
            var elem4 = document.getElementById("environmentTextfield");

            if (elem1 != null) {
                var val1 = elem1.value;
                if (val1.length == 0) {
                    errorMassage += "account,";
                }
            }
            if (elem2 != null) {
                var val2 = elem2.value;
                if (val2.length == 0) {
                    errorMassage += "password,";
                }
            }
            if (elem3 != null) {
                var val3 = elem3.value;
                if (val3.length == 0) {
                    errorMassage += "signature,";
                }
            }
            if (elem4 != null) {
                var val4 = elem4.value;
                if (val4.length == 0) {
                    errorMassage += "environment,";
                }
            }

            if (errorMassage.length > 0) {
                errorMassage = 'Invalid Fields: ' + errorMassage;
                Ext.Msg.alert('Failure:', errorMsg);
                return;
            }

            mainPanel.getForm().submit({
                url: '../../saveMerchantAccountSettings.json',
                scope:this,
                clientValidation: false,
                success: function(f, a) {
                    Ext.Msg.alert('Success:', a.result.msg);
                },
                failure: function(f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure:', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure:', 'Server connection problems. Please, try to reload page.');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure:', a.result.msg);
                    }

                }
            });
        }
    });

    var cancelButton = new Ext.Button({
        text: 'Cancel',
        handler: function() {
           var lo = Ext.urlAppend(GLOBAL.contextPath + '/pictures/main/common', Ext.urlEncode({getFilter: "load"}));
           window.location = lo;
        }        
    });


    var buttonPanel = new Ext.Panel({
        id: 'buttonPanel',
        width: 'auto',
        height: 50,
        layout: 'table',
        layoutConfig: {
            columns: 2
        },
        items: [saveButton, cancelButton]
    });
    /**
     * Main Panel
     */
    var mainPanel = new Ext.FormPanel({
        title: 'Administration - Merchant Account Settings',
        id: 'emailSettingsPanel',
        height   : 600,
        layout: 'table',
        layoutConfig: {
            columns: 1
        },
        items: [merchantPanel,buttonPanel]
    });


    mainPanel.render('content');
});