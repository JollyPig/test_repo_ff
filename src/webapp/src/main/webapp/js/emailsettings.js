Ext.onReady(function () {

    /**
     * Social Network Settings
     */
    var hid = false;

    var storeLocation = new Ext.data.Store({
        proxy:new Ext.data.HttpProxy({
            api:{
                read:'../../locations/getRealLocations.json'
            }
        }),

        reader:new Ext.data.JsonReader(
            {
                root:'locations'
            },
            [
                {
                    name:'id'
                },
                {
                    name:'locationName'
                }
            ])
    });


    var locationCombo = new Ext.form.ComboBox({
        id:'locationComboText',
        displayField:'locationName',
        valueField:'id',
        hiddenName:'locationId',
        mode:'local',
        width:150,
        store:storeLocation,
        forceSelection:true,
        triggerAction:'all',
        emptyText:'Select a location...',
        selectOnFocus:true,
        editable:false,
        allowBlank:true,
        shadow:false
    });

    var toFlickrEmailTextfield = new Ext.form.TextField({
        name:'toFlickrEmail',
        id:'toFlickrEmail',
        width:240,
        allowBlank:false
    });

    var toFacebookEmailTextfield = new Ext.form.TextField({
        name:'toFacebookEmail',
        id:'toFacebookEmail',
        width:240,
        allowBlank:false
    });

    var fromEmailTextfield = new Ext.form.TextField({
        name:'fromEmail',
        id:'fromEmail',
        width:240,
        allowBlank:false
    });

    var toEmailTextfield = new Ext.form.TextField({
        name:'toEmail',
        id:'toEmail',
        width:240,
        hidden:hid,
        allowBlank:false
    });

    var smtpServerTextfield = new Ext.form.TextField({
        xtype:'textfield',
        name:'smtpServer',
        id:'smtpServer',
        width:240,
        allowBlank:false
    });

    var smtpServerFromPasswordTextfield = new Ext.form.TextField({
        inputType:'password',
        name:'smtpServerFromPassword',
        id:'smtpServerFromPassword',
        width:240,
        allowBlank:false
    });

    var smtpServerPortTextfield = new Ext.form.TextField({
        xtype:'textfield',
        name:'smtpServerPort',
        id:'smtpServerPort',
        width:100,
        allowBlank:false
    });

    var sslCheck = new Ext.form.Checkbox(
        {
            name:'sslcheck',
            id:'sslcheck',
            checked:false,
            disabled:false
        });

    locationCombo.on('select', function (box, record, index) {
        var data = record.get('id');
        Ext.Ajax.request({
            url:'../../selectSettingsLocation.json',
            params:{
                'locationId':data
            },
            success:function (result, request) {
                var ids = result.responseText;
                var array = Ext.util.JSON.decode(ids);

                mainPanel.getForm().setValues(array);

                hid=array.isHiddenTo;
                if(hid){
                    toEmailTextfield.setVisible(false);
                    Ext.getCmp("toText").setVisible(false);
                }else{
                    toEmailTextfield.setVisible(true);
                    Ext.getCmp("toText").setVisible(true);
                }
            },
            failure:function (result, request) {
                Ext.MessageBox.alert('Failed', result.responseText);
            }
        });
    });


    var socialNetworkPanel = new Ext.Panel({
        title:'Social Network Management',
        id:'socialNetworkPanel',
        height:'auto',
        layout:'table',
        layoutConfig:{
            columns:2
        },
        items:[
            {
                xtype:'label',
                cls:'p_labels',
                text:'Location'
            },
            locationCombo,
            {
                xtype:'label',
                cls:'p_labels',
                text:'Flickr Email'
            },
            toFlickrEmailTextfield,
            {
                xtype:'label',
                cls:'p_labels',
                text:'Facebook Page Name'
            },
            toFacebookEmailTextfield
        ]
    });

    /**
     * Email Settings
     */
    var serverEmailPanel = new Ext.Panel({
        title:'Email Management',
        id:'serverEmailPanel',
        height:'auto',
        layout:'table',
        layoutConfig:{
            columns:2
        },
        items:[
            {
                xtype:'label',
                cls:'p_labels',
                text:'SMTP Server'
            },
            smtpServerTextfield,
            {
                xtype:'label',
                cls:'p_labels',
                text:'SMTP Server Port'
            },
            smtpServerPortTextfield,
            {
                xtype:'label',
                cls:'p_labels',
                text:'SMTP Account(From Email)'
            },
            fromEmailTextfield,
            {
                xtype:'label',
                cls:'p_labels',
                text:'SMTP Account Password'
            },
            smtpServerFromPasswordTextfield,
            {
                 xtype:'label',
                 cls:'p_labels',
                 id:'toText',
                 hidden:hid,
                 text:'SMTP Account(To Email)'
            },
            toEmailTextfield,
            {
                xtype:'label',
                cls:'p_labels',
                text:'SSL'
            },
            sslCheck

        ]
    });

    /**
     * Email Settings
     */
    var photobookPanel = new Ext.Panel({
        title:'Photo Book Management',
        height:'auto',
        width: 'auto',
        padding: 5,
        layout:'form',
        defaults: {
            labelStyle: 'width: 140px;',
            labelSeparator: ''
        },
        items:[{
            xtype: 'textfield',
            name: 'photobook_email',
            fieldLabel: "Edge Imaging Email",
            width: 240,
            itemCls: 'p_labels'
        }]
    });
    /**
     * Button Panel
     */
    var saveButton = new Ext.Button({
        text:'Save',
        handler:function () {
            if (locationCombo.getValue().length == 0) {
                Ext.Msg.alert('Failure:', 'Choose the Location');
                return;
            }
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            var errorMsg = "";
            var errorEmails = "";
            var i = 1;
            var elem1 = document.getElementById("toFlickrEmail");
           // var elem2 = document.getElementById("toFacebookEmail");
            var elem3 = document.getElementById("fromEmail");
            var elem4 = document.getElementById("toEmail");

            var photobookEmail = mainPanel.getForm().findField('photobook_email');

            if (elem1 != null) {
                var address1 = elem1.value;
                if (address1.length > 0 && !reg.test(address1)) {
                    errorEmails += "toFlickrEmail,";
                }
                else if (address1.length == 0) {
                    errorEmails += "toFlickrEmail,";
                }
            }
           /* if (elem2 != null) {
                var address2 = elem2.value;
                if (address2.length > 0 && !reg.test(address2)) {
                    errorEmails += "toFacebookEmail,";
                }
                else if (address2.length == 0) {
                    errorEmails += "toFacebookEmail,";
                }
            }*/

            if (elem3 != null) {
                var address3 = elem3.value;
                if (address3.length > 0 && !reg.test(address3)) {
                    errorEmails += "fromEmail,";
                }
                else if (address3.length == 0) {
                    errorEmails += "fromEmail,";
                }
            }

            if (!hid && elem4 != null) {
                var address4 = elem4.value;
                if (address4.length > 0 && !reg.test(address4)) {
                    errorEmails += "toEmail,";
                }
            }

            if(photobookEmail){
                var address5 = photobookEmail.getValue();
                if(address5.length > 0 && !reg.test(address5)){
                    errorEmails += "toPhotobookEmail";
                }
            }

            if (errorEmails.length > 0) {
                errorEmails = 'Invalid Email Addresses: ' + errorEmails;
                errorMsg = errorEmails.substring(0, errorEmails.length - 1);
                Ext.Msg.alert('Failure:', errorMsg);
                return;
            }

            mainPanel.getForm().submit({
                url:'../../saveEmailSettings.json',
                scope:this,
                clientValidation:false,
                success:function (f, a) {
                    Ext.Msg.alert('Success:', a.result.msg);
                },
                failure:function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure:', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure:', 'Server connection problems. Please, try to reload page.');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure:', a.result.msg);
                    }

                }
            });
        }
    });

    var cancelButton = new Ext.Button({
        text:'Cancel',
        handler:function () {
            var lo = Ext.urlAppend(GLOBAL.contextPath + '/pictures/main/common', Ext.urlEncode({getFilter:"load"}));
            window.location = lo;
        }
    });


    var buttonPanel = new Ext.Panel({
        id:'buttonPanel',
        width:'auto',
        height:50,
        layout:'table',
        layoutConfig:{
            columns:2
        },
        items:[saveButton, cancelButton]
    });
    /**
     * Main Panel
     */
    var mainPanel = new Ext.FormPanel({
        title:'Administration - Social Network and Email Management',
        id:'emailSettingsPanel',
        height:600,
        layout:'table',
        layoutConfig:{
            columns:1
        },
        items:[socialNetworkPanel, serverEmailPanel, photobookPanel, buttonPanel]
    });
    storeLocation.load();
    mainPanel.render('content');
});