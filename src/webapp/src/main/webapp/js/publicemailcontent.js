Ext.onReady(function() {

    //whizzywig('bold italic image bullet link fullscreen');

    var locationStore = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            url: '../../locations/getRealLocations.json'
        }),
        reader: new Ext.data.JsonReader({
            root:'locations'
        },
                [
                    {
                        name: 'id'
                    },
                    {
                        name: 'locationName'
                    }
                ])
    });

    var locationCombo = new Ext.form.ComboBox({
        id: 'locationsCombo',
        width    : 150,
        store: locationStore,
        displayField: 'locationName',
        valueField: 'id',
        hiddenName : 'id',
        cls: 'location-combo',
        typeAhead: true,
        mode: 'local',
        anchor: '100%',
        forceSelection: true,
        triggerAction: 'all',
        emptyText:'Select a location...',
        selectOnFocus:true,
        autoSelect: true,
        hidden: false,
        shadow : false
    });
/*

    locationCombo.store.on('load', function(store) {
        locationCombo.setValue(store.getAt(0).get('id'));
    });
*/

    var panelLocation = new Ext.Panel({
        id: 'settingLocation',
        bodyStyle: 'padding: 7px 0px 0px 0px',
        layout: 'table',
        layoutConfig: {
            columns: 2
        },
        items    : [
            {
                xtype: 'label',
                cls: 'p_labels_bold',
                text: 'Location'
            },
            locationCombo
        ]
    });

  var panelTextArea =new Ext.form.TextArea({
        name: 'area',
        id: 'area',
        fieldLabel: 'Enter some text',
        width: 500,
        height: 200,
        anchor: '90%',
        multiline: true
  });

    var panelArea = new Ext.Panel({
        id: 'settingStringAndEmail',
        layout: 'table',
        layoutConfig: {
            columns: 1
        },
       // hidden: true,
        disabled: true,
        items    : [
               panelTextArea,
            /*{
                xtype: 'textarea',
                name: 'area',
                id: 'area',
                fieldLabel: 'Enter some text',
                width: 300,
                height: 100,
                anchor: '90%',
                multiline: true,
                disabled: true
            },*/
            {
                xtype: 'button',
                text: 'Save Settings',
                handler: function() {
                    syncTextarea();
                    form2.getForm().submit({
                        clientValidation: true,
                        url: '../../publicEmailContentSave.json',
                        success: function(form, action) {
                            Ext.Msg.alert('Success', action.result.msg);
                        },
                        failure: function(form, action) {
                            form.fileUpload = false;
                            switch (action.failureType) {
                                case Ext.form.Action.CLIENT_INVALID:
                                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                    break;
                                case Ext.form.Action.CONNECT_FAILURE:
                                    Ext.Msg.alert('Failure', 'Server connection problems. Please, try to reload page.');
                                    break;
                                case Ext.form.Action.SERVER_INVALID:
                                    Ext.Msg.alert('Failure', action.result.msg);
                            }
                        }
                    });
                }

            }
        ]
    });


    var form2 = new Ext.form.FormPanel({
        title: 'Public Email Content Settings',
        id: 'banner-view',
        layout: 'table',
        width: '100%',
        layoutConfig: {
            columns: 2
        },
        items: [
            {
                xtype: 'label',
                cls: 'p_labels_bold',
                text: 'Location'
            },
            {
                xtype: 'label',
                cls: 'p_labels_bold',
                text: 'Location Email footer Settings'
            },
            panelLocation,
            panelArea
        ]
    });

    locationCombo.on('select', function(box, record, index) {
        var data = record.get('id');
        panelArea.setDisabled(false);
        Ext.Ajax.request({
            url: '../../getPublicLoginEmailContent.json',
            method: 'POST',
            params:
            {
                'locationid':data
            },
            callback: function(options, success, response) {

                var responseText = response.responseText;
                oW.document.body.innerHTML = "";
                if (Ext.util.JSON.decode(responseText).area != null && Ext.util.JSON.decode(responseText).area.length != 0) {
                    insHTML(Ext.util.JSON.decode(responseText).area)
                }
            }
        });

    });

    locationStore.load();
    form2.render('content');
    whizzywig();
});

