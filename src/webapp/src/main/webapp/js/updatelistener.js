/**
 *  UpdateListener
 *
 *  Observe changes from server
 *
 *  @param timeout set time between ajax requests
 *  @param url request url
 *  @param handler callback function invokes when response contain parameter success=true
 *  @param params additional parameters of the request
 *  @param method request method (GET or POST only)
 */
function UpdateListener(timeout, url, handler, params, method){
    var me = this;

    if(!url){
        throw 'Error. Define URL';
    }

    me.url = url;

    if(method){
        method = (method+"").toUpperCase();
        // only GET and POST methods are supported
        if(method != 'GET' || method != 'POST'){
            method = null;
        }
    }
    if(method){
        me.method = method;
    }

    if(typeof timeout === 'string'){
        timeout = parseInt(timeout);
    }

    if(timeout && typeof timeout === 'number' && isFinite(timeout)){
        me.timeout = timeout;
    }

    if(params){
        me.params = params;
    }

    me.startUpdateListener(handler);
}

// default request interval timeout
UpdateListener.prototype.timeout = 5;

// default request method
UpdateListener.prototype.method = 'GET';

// start listener
UpdateListener.prototype.startUpdateListener = function(handler){
    var me = this;

    me.interval = setInterval(function(){
        me.checkUpdate(function(result){
            clearInterval(me.interval);

            if(handler){
                handler.call(me, result);
            }

            if(!me.skip){
                me.startUpdateListener(handler);
            }
        });
    }, me.timeout * 1000);
}

// stop listener
UpdateListener.prototype.stopUpdateListener = function(){
    var me = this;

    me.skip = true;
    clearInterval(me.interval);
}

// ajax request
UpdateListener.prototype.checkUpdate = function(success, failure){
    var me = this,
        params;
    if(me.params){
        if(typeof me.params === 'function'){
            params = me.params();
        }else{
            params = me.params;
        }
    }

    Ext.Ajax.request({
        url: me.url,
        method: me.method,
        params: params,
        success: function(response, opts) {
            var result = JSON.parse(response.responseText);

            if(result.success === true){
                success.call(me, result);
            }else if(failure){
                failure.call(me, result);
            }
        },
        failure: function(response, opts) {
            if(failure){
                failure.call(me, response.responseText);
            }
        }
    });
}