Ext.onReady(function() {


    // sample static data for the store

    var user = Ext.data.Record.create(
            [
                'id','loginName', 'password', 'firstName', 'lastName', 'locationId','location','access'
            ]
            );

    var writer = new Ext.data.JsonWriter({
        encode: true,
        writeAllFields: true
    });

    var userStore = new Ext.data.Store({
        autoSave: false,
        proxy: new Ext.data.HttpProxy({
            api: {
                read :    '../../getUsers.json',
                create :  '../../getUsersCreate.json',  // Server MUST return idProperty of new record
                update :    '../../getUsersUpdate.json',
                destroy : '../../getUsersDelete.json'
            }
        }),
        reader: new Ext.data.JsonReader({
            root:'users',
            totalProperty: 'total',
            idProperty: 'id'
        },
                user),
        writer: writer,

        listeners: {
            save: function(store) {
                pagingToolBar.doRefresh();
            }
        }
    });


    var locationStore = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            url: '../../getLocationsClean.json'
        }),
        reader: new Ext.data.JsonReader({
            root:'locations'
        },
                [
                    {
                        name: 'id'
                    },
                    {
                        name: 'locationName'
                    }
                ])

    });


    var locationCombo = new Ext.form.ComboBox({
        id: 'locations2Combo',
        width    : 150,
        store: locationStore,
        displayField: 'locationName',
        valueField: 'id',
        hiddenName : 'id',
        cls: 'location-combo',
        typeAhead: true,
        mode: 'local',
        anchor: '100%',
        forceSelection: true,
        triggerAction: 'all',
        //  value: 'All locations',
        emptyText:'Select a location...',
        selectOnFocus:true,
        autoSelect: true,
        shadow : false
    });

    var roleStore = new Ext.data.ArrayStore({
        id: 0,
        fields: [
            'idN',
            'role'
        ],
        data: [
            ['1', 'admin'],
            ['2', 'user']
        ]
    });

    var roleCombo = new Ext.form.ComboBox({
        typeAhead: true,
        forceSelection: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
        store: roleStore,
        valueField: 'idN',
        displayField: 'role',
        width: 100,
        maxHeight: 100,
        shadow : false		
    });

    var textFieldLast = new Ext.form.TextField({
        id: 'textFieldLast',
        width    : 200
    });
    var textFieldFirst = new Ext.form.TextField({
        id: 'textFieldFirst',
        width    : 200
    });
    var textFieldLogin = new Ext.form.TextField({
        id: 'textFieldLogin',
        width    : 200
    });
    var textFieldPassword = new Ext.form.TextField({
        id: 'textFieldPassword',
        width    : 200,
        inputType: 'password'
    });


    var textFieldPasswordConfirm = new Ext.form.TextField({
        id: 'textFieldPasswordConform',
        width    : 200,
        inputType: 'password'
    });
    var rowIndexField = new Ext.form.TextField({
        id: 'rowIndex',
        width    : 200,
        hidden: true
    });


    var userForm = new Ext.FormPanel({
        title: 'General',
        frame:true,
        height: 300,
        bodyStyle:'padding:25px 50px 0',
        id: 'userPanel',
        border : true,
        labelAlign: 'top',
        hidden:true,
        layout: 'table',
        layoutConfig: {
            columns: 2
        },
        items: [
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'Last name'
            },
            textFieldLast
            ,
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'First name'
            },
            textFieldFirst,
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'Login'
            },
            textFieldLogin,
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'Password'
            },
            textFieldPassword,
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'Confirm Password'

            },
            textFieldPasswordConfirm,
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'Location'
            },
            locationCombo,
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'Role'
            },
            roleCombo,
            rowIndexField


        ],
        buttons: [
            {
                text: 'Save',
                handler: function() {
                    if (rowIndexField.getValue() == -1) {
                        if (textFieldLogin.getValue().length == 0) {
                            alert("The login could not be empty");
                            return
                        }
                        Ext.Ajax.request({
                            url:    '../../checkLogin.json',
                            method: 'POST',
                            params:{
                                login: textFieldLogin.getValue()
                            },
                            callback: function(options, success, response) {
                                if(response.responseText == '{"loginCharacter":false}'){
                                   alert("The following Special characters can not be used for login: '*', ';', '--', ':', '/', 'xp_' ");
                                   return
                                }
                                if (response.responseText == '{"login":false}') {
                                    alert("The current login is already exist. Please select another one");
                                    return
                                } else {
                                    if (textFieldPassword.getValue() == textFieldPasswordConfirm.getValue() &&
                                        textFieldFirst.getValue().length != 0 && textFieldLast.getValue().length != 0 &&
                                        locationCombo.lastSelectionText.length != 0 && roleCombo.getValue() != '') {
                                        var e = new user({
                                            loginName:  textFieldLogin.getValue(),
                                            password: textFieldPassword.getValue(),
                                            firstName:textFieldFirst.getValue(),
                                            lastName: textFieldLast.getValue(),
                                            locationId: locationCombo.getValue(),
                                            location: locationCombo.lastSelectionText,
                                            access: roleCombo.getValue()
                                        });
                                        userStore.insert(0, e);
                                        userStore.save();
                                        userForm.setVisible(false);
                                        userForm.getForm().reset();
                                        grid.getView().refresh();

                                    } else {
                                        alert("Fields should be filled in and the passwords should match");
                                        return;
                                    }

                                }
                            }
                        });
                    } else {
                        var record = grid.getStore().getAt(rowIndexField.getValue());
                        if (record.get('password') != textFieldPassword.getValue()) {
                            if (textFieldPassword.getValue() == textFieldPasswordConfirm.getValue()) {
                                record.set('password', textFieldPassword.getValue());
                            } else {
                                alert("The passwords should be equal");
                                return;
                            }
                        } else {
                            record.set('password', '');
                        }
                        if (record.get('loginName') == textFieldLogin.getValue()) {
                            if (textFieldPassword.getValue() == textFieldPasswordConfirm.getValue() && textFieldFirst.getValue().length != 0 && textFieldLast.getValue().length != 0 && locationCombo.lastSelectionText.length != 0 && roleCombo.getValue() != '') {
                                record.set('lastName', textFieldLast.getValue());
                                record.set('firstName', textFieldFirst.getValue());
                                record.set('access', roleCombo.getValue());
                                record.set('location', locationCombo.lastSelectionText);
                                record.set('locationId', locationCombo.getValue());
                                userStore.save();
                                grid.getView().refresh();
                                userForm.getForm().reset();
                                userForm.setVisible(false);
                                rowIndexField.setValue(-1);
                            } else {
                                alert("Fields should be filled in and the passwords should match");
                                return;
                            }
                        } else {

                            Ext.Ajax.request({
                                url:    '../../checkLogin.json',
                                method: 'POST',
                                params:{
                                    login: textFieldLogin.getValue()
                                },
                                callback: function(options, success, response) {
                                    if(response.responseText == '{"loginCharacter":false}'){
                                       alert("The following Special characters can not be used for login: '*', ';', '--', ':', '/', 'xp_' ");
                                       return
                                    }
                                    if (response.responseText == '{"login":false}') {
                                        alert("The current login is already exist. Please select another one");
                                        return
                                    } else {
                                        if (textFieldPassword.getValue() == textFieldPasswordConfirm.getValue() && textFieldFirst.getValue().length != 0 && textFieldLast.getValue().length != 0 && locationCombo.lastSelectionText.length != 0 && roleCombo.getValue() != '') {
                                            record.set('lastName', textFieldLast.getValue());
                                            record.set('firstName', textFieldFirst.getValue());
                                            record.set('loginName', textFieldLogin.getValue());
                                            record.set('access', roleCombo.getValue());
                                            record.set('location', locationCombo.lastSelectionText);
                                            record.set('locationId', locationCombo.getValue());
                                            userStore.save();
                                            grid.getView().refresh();
                                            userForm.getForm().reset();
                                            userForm.setVisible(false);
                                            rowIndexField.setValue(-1);
                                        } else {
                                            alert("Fields should be filled in and the passwords should match");
                                            return;
                                        }

                                    }
                                }
                            });
                        }

                    }


                } // end of Main Button handler
            },
            {
                text: 'Cancel',
                handler: function() {
                    userForm.getForm().reset();
                    rowIndexField.setValue(-1);
                    userForm.setVisible(false);
                }
            }
        ]

    });
    var pagingToolBar = new Ext.PagingToolbar({
        pageSize: 10,
        width: 800,
        store: userStore,
        displayInfo: true,
        displayMsg: 'Displaying users {0} - {1} of {2}',
        emptyMsg: "No users to display",
        hideBorders: true
    });
    pagingToolBar.refresh.hide();

    function performSearch(value){
        userStore.setBaseParam("searchName", value);
        pagingToolBar.changePage(1);
    }

    // create the Grid
    var grid = new Ext.grid.GridPanel({
        store: userStore,
        columns: [
            {
                id       :'user',
                header   : 'User',
                width    : 100,
                dataIndex: 'loginName',
                menuDisabled: true
            },
            {
                id       :'location',
                header   : 'Location',
                width    : 160,
                dataIndex: 'location',
                menuDisabled: true
            },
            {
                id       :'firstName',
                header   : 'First Name',
                width    : 120,
                dataIndex: 'firstName',
                menuDisabled: true
            },
            {
                id       :'lastName',
                header   : 'Last Name',
                width    : 120,
                dataIndex: 'lastName',
                menuDisabled: true
            },
            {
                id       :'access',
                header   : 'User Role',
                width    : 100,
                dataIndex: 'access',
                menuDisabled: true,
                renderer: function (v, p, r)
                {
                    if (r.data.access == 1) {
                        return 'admin';
                    } else {
                        if(r.data.access==2){
                            return 'user';
                        }else{
                            return 'public user';
                        }
                    }
                }
            },
            {
                header   : 'Edit',
                width    : 50,
                sortable : false,
                dataIndex: 'edit',
                menuDisabled: true,
                renderer: function (v, p, r)
                {
                    if(r.data.access > 2){
                        return '<div class ="user-edit" ></div>';
                    }else{
                        return '<div class ="user-edit" >edit</div>';
                    }
                }
            },
            {
                header   : 'Delete',
                width    : 50,
                sortable : false,
                dataIndex: 'delete',
                menuDisabled: true,
                renderer: function (v, p, r)
                {
                    return '<div class ="user-edit">delete</div>';
                }
            }


        ],
        height: 350,
        title: 'User List',
        viewConfig: {
            forceFit: true,
            getRowClass: function(record, index) {
                var a = index % 2;
                if (a > 0) {
                    return 'selected-class';
                }
            }
        },
        tbar: [{
            xtype: 'textfield',
            name: "searchName",
            width: '400',
            listeners: {
                'specialkey': function(field,e){
                    if(e.getKey() == e.ENTER){
                        performSearch(field.getValue());
                    }
                }
            }
        },{
            text: 'Search',
            handler: function(btn, evt) {
                var value = btn.findParentByType('toolbar').find('name',"searchName")[0].getValue();
                console.log('search', value)

                performSearch(value);
            }
        }],
        bbar: pagingToolBar,
        fbar :[
            {
                text: 'Add new User',
                handler: function(btn, evt) {
                    userForm.getForm().reset();
                    userForm.setVisible(true);
                    rowIndexField.setValue(-1);
                    Ext.Ajax.request({


                    });
                }
            }
        ]
    });
    grid.on({
        'cellclick' : function(grid, rowIndex, columnIndex, e) {
            var record = grid.getStore().getAt(rowIndex);
            if (columnIndex == 5) {
                // userStore.reload();
                if(record.get('access')<3){
                    userForm.getForm().reset();
                    userForm.setVisible(true);
                    textFieldLast.setValue(record.get('lastName'));
                    textFieldFirst.setValue(record.get('firstName'));
                    textFieldLogin.setValue(record.get('loginName'));
                    textFieldPassword.setValue(record.get('password'));
                    textFieldPasswordConfirm.setValue(record.get('password'));
                    locationCombo.setValue(record.get('locationId'));
                    roleCombo.setValue(record.get('access'));
                    rowIndexField.setValue(rowIndex);
                }
            }
            if (columnIndex == 6) {

                Ext.Msg.confirm('Confirm Delete', 'Are you sure you want to delete this user', function(btnText) {
                    if (btnText == 'yes') {
                        var record = grid.getStore().getAt(rowIndex);
                        userStore.removeAt(rowIndex);
                        userStore.save();

                        rowIndexField.setValue(-1);
                        grid.getView().refresh();

                    }
                }, this);
            }

        },
        scope: this
    });

    var mainPanel = new Ext.Panel({
        title: 'Administration - User Management',
        id: 'userManagementPanel',
        height   : 'auto',
        layout: 'column',
        layoutConfig: {
            columns: 2
        },
        items    : [
            {
                columnWidth: .65,
                items: grid
            },
            {
                columnWidth: .35,
                items: userForm
            }
        ]
    });

    locationStore.load();
    userStore.load();
    mainPanel.render('content');


});
