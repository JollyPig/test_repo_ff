;
$(function($) {

    var selectedIds = [],
        params = {},
        slider,
        uploaded = [];

    $.setSelectedIds = function(value) {
        selectedIds = value;
    };

    $.initParams = function(value) {
        params = value;
    };

    $.initSlider = function(value) {
        slider = value;
    };

    $.getUploaded = function () {

        return uploaded;
    };

    $.emptyUploaded = function () {

         uploaded =[];
    };

    $.addUploaded = function (value) {

        uploaded.push(value);
    };

    var populateTemplate = function (image, options) {
            var showclasspref = options.slideCount > 1 ? '' : '-single',
                pictureUrl = (options.slideCount == null || options.slideCount == 1) ? image.url : image.base64code,
                content = '<div class ="picture-wrapper' + showclasspref + '">' +
                    '<div class ="picture-wrapper-watermark-div' + showclasspref + '">' +
                    '<img class="picture' + showclasspref + '" src="../' + pictureUrl + '" title="' + image.name + '" selectedid="' + image.selected + '" imageid="' + image.id +  '">' +
                    '<span class="picture-wrapper-watermark' + showclasspref + '">' + image.logoText + '</span>' +
                    '<span class="picture-index' + showclasspref + '">' + formatDate(image.creationDate) + '</span>';
            if (image.logoUrl.length != 0) {
                content += '<img src="..' + image.logoUrl + '" class="picture-wrapper-logo-watermark' + showclasspref + '"  title = "" alt=""/>';
            }
            if (image.logoMainUrl.length != 0) {
                content += '<img src="..' + image.logoMainUrl + '" class="picture-wrapper-logoMain-watermark' + showclasspref + '" title = "" alt=""/>';
            }
            if (options.userId == 3) {
                content += '<img src="../../css/images/logo.png" class="picture-watermark-opaque' + showclasspref + '"  title = "" alt=""/>';
            }
            content += '</div></div>';
            return content;
        },

        formatDate = function(date) {
            return date.substring(date.indexOf(" ") + 1, date.length);
        },

        removeFromSelectedIds = function(removeId) {
            selectedIds = $.grep(selectedIds, function(value) {
                return value != removeId;
            });
        },

        pushInSelectedIds = function (id) {
            if (id != undefined && id != null) {
                selectedIds.push(id);
            }
        };

    $.uploadPictures = function(parameters) { //parameters
        //var parameters = arguments[0];
        console.log("Uploading start from position : " + parameters['start']);
        console.log(parameters);

        $.ajax({
            url: '../../getPicturesForSlideShow.json',
            data: parameters,
            dataType: 'json',
            success: function(result) {
                uploaded.push(parameters['uploaded']);
              //  uploaded.push(parameters['start']);

                if(result!= undefined || result != null) {

                    var pictures = result.pictures;
                    $.populate({
                        pictures: pictures,
                        selector: ".carousel",
                        filters: {
                            slideCount : parameters.slideCount,
                            userId: parameters.userId
                        }
                    });

                    var query = 'img[class="picture' + (GLOBAL.slideCount == 1 ? '-single' : '') + '"]';
                    $.each($('li.slide').find(query), function(index, el) {
                        if ($(this).attr('selectedid') == "true") {
                            $(this).css('background', 'none repeat scroll 0 0 #F28C29');
                        }
                    });

                    $($('.slideshow').children()[0]).find(query).mouseenter(function(){
                        var me = $(this);
                        if ($(me).attr('selectedid') != "true") {
                            $(me).css('background', 'none repeat scroll 0 0 #F28C29');
                        }
                    }).mouseleave(function(){
                        var me = $(this);
                        if ($(me).attr('selectedid') != "true") {
                            $(me).css('background', 'none repeat scroll 0 0 #FFFFFF');
                        }
                    });

                    slider.upsert();
                    $.unbindAndBindPictureClick();

                }
            }
        });
    };

    $.unbindAndBindPictureClick = function() {
        var pictures = $("img[class*='picture']");
        pictures.unbind('click');
        pictures.click(function() {
            var me = $(this);
            if (me.attr("selectedid") == "true") {
                $(me).attr("selectedid", "false");
                $(me).css("background-color", "#ffffff");
                removeFromSelectedIds($(me).attr("imageid"));
            } else {
                $(me).attr("selectedid", "true");
                $(me).css("background-color", "#f28c29");
                pushInSelectedIds($(me).attr("imageid"));
            }
        });
    };

    $.populate = function(settings) {
        var options = settings || {
                selector: ".carousel",
                pictures: [],
                filters: {
                    slideCount : 1,
                    userId : null
                }
            },
            slideCount = parseInt(options.filters.slideCount);

        if (slideCount == 1) {
            $.each(options.pictures, function(index, image) {
                $(options.selector).append(
                    '<li class="slide"><div style="height: 791px; width: 1060px;">' +
                        populateTemplate(image, options.filters) +
                        '</div></li>');
            });
        } else {
            var count = 0,
                temp = '<li class="slide"><div style="height: 791px; width: 1060px;">',
                content = '';
            $.each(options.pictures, function(index, image) {
                if (count < 4) {
                    if (count == 0) {
                        content += temp;
                    }
                    content += populateTemplate(image, options.filters);
                    count++;
                    if (count == 4) {
                        content += '</div></li>';
                        count = 0;
                    }
                } else {
                    count = 0;
                }
            });
            $(options.selector).append(content);
        }

    };

});