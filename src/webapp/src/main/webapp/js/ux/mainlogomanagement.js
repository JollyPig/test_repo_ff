Ext.onReady(function() {
    var pageLoading = true;
    var storeLocation = new Ext.data.Store({
        proxy : new Ext.data.HttpProxy({
            api : {
                read : '../../locations/getRealLocations.json'
            }
        }),

        reader : new Ext.data.JsonReader({
            root : 'locations'
        }, [
            {
                name : 'id'
            },
            {
                name : 'locationName'
            }
        ]), listeners : {
            load : function() {
                if (pageLoading) {
                    locationComboImg.setValue(GLOBAL.locationId);
                    pageLoading = false;
                }
            }
        }
    });

    var locationComboImg = new Ext.form.ComboBox({
        id : 'locationComboImg', displayField : 'locationName', valueField : 'id',
        hiddenName : 'locationIdImg', mode : 'local', width : 112, store : storeLocation,
        forceSelection : true, triggerAction : 'all', emptyText : 'Select a location...',
        selectOnFocus : true, editable : false, allowBlank : true ,shadow : false		
    });

    if (GLOBAL.userId != 1) {
        locationComboImg.setDisabled(true);
    }

    var secondLogoPanel = new Ext.Panel({
        layout: 'table',
        id:'mainLogo-view',
        layoutConfig: {
            columns: 6
        },
        items:
                [
                    {
                        xtype: 'label',
                        cls: 'p_labels',
                        text : 'Location:'
                    },
                    locationComboImg,
                    {
                        xtype: 'label',
                        cls: 'p_labels',
                        text: 'Path:'
                    },
                    {
                        xtype: 'field',
                        id: 'imageLogo',
                        allowBlank: true,
                        inputType: 'file'
                    },
                    {
                        xtype: 'button',
                        text: 'Upload',
                        handler: function() {
                            if (GLOBAL.userId != 1) {
                                locationComboImg.setDisabled(false);
                            }
                            form.getForm().findField('imageLogo').allowBlank = false;
                            form.getForm().fileUpload = true;
                            if (form.getForm().findField('imageLogo').getValue() != "" && locationComboImg.getValue()!="" ) {
                            form.getForm().submit({
                                clientValidation: true,
                                url: '../../updateMainImageLogo.json',
                                success: function(form, action) {
                                    form.fileUpload = false;
                                    Ext.Msg.alert('Success', action.result.msg);
                                        if (GLOBAL.userId != 1) {
                                            locationComboImg.setDisabled(true);
                                        }
                                },
                                failure: function(form, action) {
                                        if (GLOBAL.userId != 1) {
                                            locationComboImg.setDisabled(true);
                                        }
                                    form.fileUpload = false;
                                    switch (action.failureType) {
                                        case Ext.form.Action.CLIENT_INVALID:
                                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                            break;
                                        case Ext.form.Action.CONNECT_FAILURE:
                                            Ext.Msg.alert('Failure', 'Server connection problems. Please, try to reload page.');
                                            break;
                                        case Ext.form.Action.SERVER_INVALID:
                                            Ext.Msg.alert('Failure', action.result.msg);
                                    }
                                }
                            });
                            } else {
                                alert("Please indicate the logo and location");
                            }
                        }

                    },
                    {
                        xtype : 'button',
                        cls : 'uploadButton',
                        text : 'Delete',
                        handler : function() {
                            form.getForm().findField('imageLogo').allowBlank = true;
                            if (locationComboImg.getValue()!="" ) {
                            form.getForm().submit(
                            {
                                clientValidation : true,
                                url : '../../deleteMainImageLogo.json',
                                success : function(form, action) {
                                    Ext.Msg.alert('Success', action.result.msg);

                                },
                                failure : function(form, action) {
                                    switch (action.failureType) {
                                        case Ext.form.Action.CLIENT_INVALID:
                                            Ext.Msg.alert('Failure',
                                                    'Form fields may not be submitted with invalid values');
                                            break;
                                        case Ext.form.Action.CONNECT_FAILURE:
                                            Ext.Msg.alert('Failure', 'Server connection problems. Please, try to reload page.');
                                            break;
                                        case Ext.form.Action.SERVER_INVALID:
                                            Ext.Msg.alert('Failure', action.result.msg);
                                    }
                                }
                            });
                            }else{
                                alert("Please select a location");
                            }
                        }
                    }

                ]
    });


    var form = new Ext.form.FormPanel({
        title: 'Settings',
        id: 'purch-view',
        layout: 'table',
        width: '100%',
        layoutConfig: {
            columns: 2
        },
        items: [
            {
                xtype: 'label',
                cls: 'p_labels_bold',
                text: 'Administration Setting Name'
            },
            {
                xtype: 'label',
                cls: 'p_labels_bold',
                text: 'Administration Setting Value'
            },
            {
                xtype: 'panel',
                height: 40,
                padding: '10px 0px 0px 0px',
                items:[
                    {
                        xtype: 'label',
                        cls: 'p_labels',
                        text: 'Main Logo'
                    }
                ]
            },
            secondLogoPanel
        ]
    });

    storeLocation.load();
    form.render('content');
})


