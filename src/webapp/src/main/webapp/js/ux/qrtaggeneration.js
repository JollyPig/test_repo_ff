Ext.onReady(function() {
    var form = new Ext.form.FormPanel({
        id : 'settings-view',
        title: "QR Tag Generation",
        layout : 'table',
        width : '100%',
        layoutConfig : {
            columns : 2
        },
        items : [
            {
                xtype : 'label',
                class: 'p_labels',
                text : 'QR Tag'
            },
            {
                xtype : 'textfield',
                itemId: 'QRTag',
                readOnly: true,
                padding: 10,
                text : 'QR Tag',
                listeners: {
                    focus: function(item) {
                        item.selectText();
                    }
                }
            },
            {
                xtype: 'button',
                text: 'Generate QR Tag',
                handler: function () {
                    form.getForm().submit({
                        url: GLOBAL.contextPath + "/qrtag/generate.json",
                        params: {
                            date: new Date().format("U")
                        },
                        success: function (form, action)
                        {
                            var qrTagField = form.findField('QRTag');
                            if (qrTagField)
                            {
                                var qrTag = Ext.util.JSON.decode(action.response.responseText).qrtag;
                                qrTagField.setValue(qrTag);
                            }
                        }
                    })
                }
            }
        ]
    });

    form.render('content');
});