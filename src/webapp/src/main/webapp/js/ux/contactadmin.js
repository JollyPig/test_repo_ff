Ext.onReady(function() {


    /**
     * Admin Panel
     */
    var toEmailTextfield = new Ext.form.TextField({
        name: 'toEmail',
        id: 'toEmail',
        width: 300,
        allowBlank:true
    });
    var saveButton = new Ext.Button({
        text: 'Save Email',
        handler: function() {
            var reg = /^(([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4}),*\s*)*$/;
            var address1 = toEmailTextfield.getValue();
            var errorMsg = "";
            var validateMessage = "";
            if (address1.length != 0 && !reg.test(address1)) {
                validateMessage = " (Example: test@test.test, test@test.test),";
            }
            if (validateMessage.length > 0) {
                errorMsg = 'The Email Address field should be correct. The emails should be separated by comma, see the example: ' + validateMessage;
                Ext.Msg.alert('Failure:', errorMsg);
                return;
            }
            Ext.Ajax.request({
                url: '../../saveInfoEmail.json',
                method: 'POST',
                params:
                {
                    'email':toEmailTextfield.getValue()
                },
                success: function (f, a) {
                    Ext.Msg.alert('Success:', "The emails were successfully saved");
                },
                failure:  function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure:', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure:', 'Server connection problems. Please, try to reload page.');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure:', "Failed to save the emails");
                    }
                }
            });
        }
    });

    var adminPanel = new Ext.Panel({
        id: 'adminPanel',
        width: 600,
        height: 'auto',
        layout: 'table',
        layoutConfig: {
            columns:5
        },
        items: [
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'Please enter contact admin email addressee'
            },
            {
                xtype : 'box',
                width : 5
            },
            toEmailTextfield,
            {
                xtype : 'box',
                width : 5
            },
            saveButton
        ],
        listeners: {
            beforerender: function(pn) {
                Ext.Ajax.request({
                    url: '../../getInfoEmail.json',
                    method: 'POST',
                    callback: function(options, success, response) {
                        var email = Ext.util.JSON.decode(response.responseText).email;
                        adminForm.getForm().findField('toEmail').setValue(email);
                    }
                });
            }
        }
    });


    var fromEmailTextfield = new Ext.form.TextField({
        name: 'fromEmail',
        id: 'fromEmail',
        width: 240,
        allowBlank:false

    });

    var smtpServerTextfield = new Ext.form.TextField({
        xtype: 'textfield',
        name: 'smtpServer',
        id: 'smtpServer',
        width: 240,
        allowBlank:false
    });

    var smtpServerFromPasswordTextfield = new Ext.form.TextField({
        inputType: 'password',
        name: 'smtpServerFromPassword',
        id: 'smtpServerFromPassword',
        width: 240,
        allowBlank:false
    });

    var smtpServerPortTextfield = new Ext.form.TextField({
        xtype: 'textfield',
        name: 'smtpServerPort',
        id: 'smtpServerPort',
        width: 100,
        allowBlank:false
    });

    var subjectTextfield = new Ext.form.TextField({
        xtype: 'textfield',
        name: 'subjectText',
        id: 'subjectText',
        width: 450,
        allowBlank:false
    });

    var sslCheck = new Ext.form.Checkbox(
    {
        name : 'sslcheck',
        id : 'sslcheck',
        checked : true,
        disabled : false
    });
    /**
     * Email Settings
     */
    var serverEmailPanel = new Ext.Panel({
        id: 'serverEmailPanel',
        height   : 'auto',
        layout: 'table',
        layoutConfig: {
            columns: 2
        },
        items: [
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'SMTP Server'
            },
            smtpServerTextfield,
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'SMTP Server Port'
            },
            smtpServerPortTextfield,
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'SMTP Account(From Email)'
            },
            fromEmailTextfield,
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'SMTP Account Password'
            },
            smtpServerFromPasswordTextfield,
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'Subject'
            },
            subjectTextfield,
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'SSL'
            },
            sslCheck,    
            /*  {
             xtype : 'box',
             width : 20
             },*/
            {
                xtype: 'button',
                text: 'Save Settings',
                handler: function() {
                    adminForm.getForm().submit({
                        clientValidation: true,
                        url: '../../contactEmailSettingsSave.json',
                        success: function(form, action) {
                            Ext.Msg.alert('Success', action.result.msg);
                        },
                        failure: function(form, action) {
                            form.fileUpload = false;
                            switch (action.failureType) {
                                case Ext.form.Action.CLIENT_INVALID:
                                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                    break;
                                case Ext.form.Action.CONNECT_FAILURE:
                                    Ext.Msg.alert('Failure', 'Server connection problems. Please, try to reload page.');
                                    break;
                                case Ext.form.Action.SERVER_INVALID:
                                    Ext.Msg.alert('Failure', action.result.msg);
                            }
                        }
                    });
                }

            },
            {
                xtype: 'button',
                text: 'Delete Settings',
                handler: function() {
                    Ext.Msg.confirm('Confirm Delete', 'Are you sure you want to delete settings?', function(btnText) {
                        if (btnText == 'yes') {
                            Ext.Ajax.request({
                                url: '../../contactEmailSettingsDelete.json',
                                method: 'POST',

                                success: function (f, a) {
                                    Ext.Msg.alert('Success:', "The settings were successfully deleted");
                                    adminForm.getForm().findField('smtpServer').setValue("");
                                    adminForm.getForm().findField('smtpServerPort').setValue("");
                                    adminForm.getForm().findField('fromEmail').setValue("");
                                    adminForm.getForm().findField('smtpServerFromPassword').setValue("");
                                    adminForm.getForm().findField('subjectText').setValue("");
                                 	document.getElementById('sslcheck').checked = false;
                                },
                                failure:  function (f, a) {
                                    switch (a.failureType) {
                                        case Ext.form.Action.CLIENT_INVALID:
                                            Ext.Msg.alert('Failure:', 'Form fields may not be submitted with invalid values');
                                            break;
                                        case Ext.form.Action.CONNECT_FAILURE:
                                            Ext.Msg.alert('Failure:', 'Server connection problems. Please, try to reload page.');
                                            break;
                                        case Ext.form.Action.SERVER_INVALID:
                                            Ext.Msg.alert('Failure:', "Failed to delete the settings");
                                    }
                                }
                            });


                        }
                    }, this);
                    return false; // Stop the delete request
                }

            }
        ]
        ,
        listeners: {
            beforerender: function(pn) {
                Ext.Ajax.request({
                    url: '../../getEmailContactSettings.json',
                    method: 'POST',
                    callback: function(options, success, response) {
                        var smtpServer = Ext.util.JSON.decode(response.responseText).smtpServer;
                        adminForm.getForm().findField('smtpServer').setValue(smtpServer);
                        var smtpServerPort = Ext.util.JSON.decode(response.responseText).smtpServerPort;
                        adminForm.getForm().findField('smtpServerPort').setValue(smtpServerPort);
                        var fromEmail = Ext.util.JSON.decode(response.responseText).fromEmail;
                        adminForm.getForm().findField('fromEmail').setValue(fromEmail);
                        var smtpServerFromPassword = Ext.util.JSON.decode(response.responseText).smtpServerFromPassword;
                        adminForm.getForm().findField('smtpServerFromPassword').setValue(smtpServerFromPassword);
                        var subjectText = Ext.util.JSON.decode(response.responseText).subjectText;
                        adminForm.getForm().findField('subjectText').setValue(subjectText);
                        var ssl = Ext.util.JSON.decode(response.responseText).sslcheck;
                        if(ssl=="off"){
                            document.getElementById('sslcheck').checked = false;
                        }else if(ssl=="on"){
                            document.getElementById('sslcheck').checked = true;
                        }
                    }
                });
            }

        }
    });


    var adminForm = new Ext.form.FormPanel({
        title: 'Contact Settings',

        id: 'purch-view',
        layout: 'table',
        width: '100%',
        layoutConfig: {
            columns: 1
        },
        items: [

            serverEmailPanel,
            adminPanel
        ]
    });


    adminForm.render('content');

});