Ext.onReady(function() {

    Ext.Ajax.timeout = 300000;
    var selectedPictures = [];
    var countSelected = 0;
    /**
     * Start top panel elements
     */

    var storeLocation = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            api: {
                read: '../../locations/getRealLocations.json'
            }
        }),

        reader: new Ext.data.JsonReader(
        {
            root:'locations'
        },
                [
                    {
                        name: 'id'
                    },
                    {
                        name: 'locationName'
                    }
                ])
    });

    var storePackage = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            api: {
                read: '../../packages/getPackagesPurchase.json'
            }
        }),

        reader: new Ext.data.JsonReader(
        {
            root:'packages'
        },
                [
                    {
                        name: 'id'
                    },
                    {
                        name: 'packageName'
                    }
                ])
    });


    var storeStaff = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            api: {
                read: '../../getStaff.json'
            }
        }),

        reader: new Ext.data.JsonReader(
        {
            root:'staff'
        },
                [
                    {
                        name: 'id',
                        type: 'int'
                    },
                    {
                        name: 'staffName'
                    }
                ])
    });

    var participantStaff = new Ext.data.ArrayStore({
        id: 0,
        fields: [
            'idE',
            'countE'
        ],
        data: [
            [0, '0'],
            [1, '1'],
            [2, '2'],
            [3, '3'],
            [4, '4'],
            [5, '5'],
            [6, '6'],
            [7, '7'],
            [8, '8'],
            [9, '9'],
            [10, '10'],
            [11, '11'],
            [12, '12'],
            [13, '13'],
            [14, '14'],
            [15, '15'],
            [16, '16'],
            [17, '17'],
            [18, '18'],
            [19, '19'],
            [20, '20']
        ]
    });

    var participantOtherStaff = new Ext.data.ArrayStore({
        id: 0,
        fields: [
            'idE',
            'countE'
        ],
        data: [
            [0, '0'],
            [1, '1'],
            [2, '2'],
            [3, '3'],
            [4, '4'],
            [5, '5'],
            [6, '6'],
            [7, '7'],
            [8, '8'],
            [9, '9'],
            [10, '10'],
            [11, '11'],
            [12, '12'],
            [13, '13'],
            [14, '14'],
            [15, '15'],
            [16, '16'],
            [17, '17'],
            [18, '18'],
            [19, '19'],
            [20, '20']
        ]
    });


    var locationCombo = new Ext.form.ComboBox({
        id: 'locationCombo',
        displayField: 'locationName',
        valueField: 'id',
        hiddenName : 'iDlocationCombo',
        mode: 'local',
        width: 150,
        store: storeLocation,
        forceSelection: true,
        triggerAction: 'all',
        emptyText:'Select a location...',
        selectOnFocus:true,
        editable: false,
        allowBlank: true,
        hidden: isUser(),
        shadow : false,
        tabIndex :1
       /* listeners:{
             afterrender: function(combo) {
                 if(GLOBAL.location!=null && GLOBAL.location!=""){
                    combo.setValue(GLOBAL.location);
                 }
             }
        }  */
    });


    function isUser()
    {
        if (GLOBAL.userId != 1) {
            return true;
        }
    }

    function isPublicUser()
        {
            if (GLOBAL.userId == 3 || GLOBAL.userId == 4 ||  GLOBAL.userId == 5 || GLOBAL.userId == 6) {
                return true;
            }
        }

    function isTotalShowUser()
    {
        if (GLOBAL.userId == 3 || GLOBAL.userId == 4 ||  GLOBAL.userId == 5) {
            return true;
        }
    }

    function isNullPriceAndPublic()
        {
            if (GLOBAL.userId == 1 || GLOBAL.userId == 2 || GLOBAL.price == 0 || GLOBAL.price == "" || GLOBAL.userId == 6) {
                           return true;
                       }

        }

    function isJustNullPriceAndPublic()
    {
        if (GLOBAL.price == 0 || GLOBAL.price == "" || GLOBAL.userId == 6) {
            return true;
        }

    }


    function isPublicOnlyUser()
    {
        if (GLOBAL.userId == 3) {
            return true;
        }
    }

      function isPublicEmailUser()
    {
        if (GLOBAL.userId == 4) {
            return true;
        }
    }

    var packageCombo = new Ext.form.ComboBox({
        id: 'packageCombo',
        displayField: 'packageName',
        valueField: 'id',
        hiddenName : 'iDpackageCombo',
        mode: 'local',
        width: 150,
        store: storePackage,
        forceSelection: true,
        triggerAction: 'all',
        emptyText:'Select a package...',
        selectOnFocus:true,
        editable: false,
        allowBlank: false,
        lazyRender:true,
        disabled: !isUser(),
        hidden: isPublicUser(),
        maxHeight: 100,
        shadow : false,
        tabIndex :2
    });

    storePackage.on('load', function(ds, records, o) {
        if (isPublicUser()) {
            packageCombo.setValue(records[0].data.id);
        }
    });

    Ext.override(Ext.ux.form.LovCombo, {
        beforeBlur: Ext.emptyFn
    });
    var staffCombo = new Ext.ux.form.LovCombo({
        id: 'staffCombo',
        displayField: 'staffName',
        valueField: 'id',
        hiddenName : 'iDstaffCombo',
        mode: 'local',
        hideOnSelect:false,
        forceSelection: true,
        width: 150,
        store: storeStaff,
        triggerAction: 'all',
        emptyText:'Select a person...',
        selectOnFocus:true,
        editable: false,
        allowBlank: true,
        disabled: !isUser(),
        hidden: isPublicUser(),
        maxHeight: 100,
        tabIndex :5

    });

     staffCombo.on('render', function(ds, records, o) {
        if (isPublicEmailUser()) {
               Ext.Ajax.request({
                    url: '../../getPurchaseStaffForPublicUser.json',
                    method: 'GET',
                success: function (result, request) {
                        myMask.hide();
                        var resText = Ext.util.JSON.decode(result.responseText);
                        var st = resText.staff;
                        if(st!=null && st.length!=0){
                            staffCombo.setValue(st);
                        }
                },
                failure: function (result, request) {
                    myMask.hide();
                    Ext.MessageBox.alert('Failed', 'Failed to load staff');
                }
                });
        }
    });

    var participantCombo = new Ext.form.ComboBox({
        id: 'participantCombo',
        valueField: 'idE',
        displayField: 'countE',
        hiddenName : 'iDparticipantCombo',
        mode: 'local',
        width: 40,
        store: participantStaff,
        forceSelection: true,
        triggerAction: 'all',
        selectOnFocus:true,
        editable: false,
        allowBlank: false,
        lazyRender:true,
        maxHeight: 100,
        hidden: isPublicUser(),
        shadow : false,
        tabIndex :4,
        listeners:{
            afterrender: function(combo) {
                if (!(GLOBAL.emailCount > 0))
                    combo.setValue(0);
                if (GLOBAL.emailCount > 0) {
                    participantCombo.setValue(GLOBAL.emailCount);
                    if (GLOBAL.emails.length > 0) {
                        var emails = GLOBAL.emails.substring(1, GLOBAL.emails.length - 1);
                        var array = emails.split(", ");
                        var extPanel = new Ext.Panel({
                            id: "extPanel",
                            title: 'Purchasing emails',
                            width: 'auto',
                            layout: 'table',
                            layoutConfig: {
                                columns: 8
                            }
                        });
                        var i = 0;
                        for (i; i < array.length; i++) {
                            var inpL = new Ext.form.Label({
                                cls: 'p_labels',
                                text: 'Email' + (i + 1)
                            });
                            var inp = new Ext.form.TextField({
                                name: 'email' + (i + 1),
                                id: 'email' + (i + 1),
                                width: 180,
                                allowBlank:false
                            });
                            inp.setValue(array[i]);
                            extPanel.add(inpL);
                            extPanel.add(inp);
                        }
                        extPanel.render('fields');
                    }
                }
                else {
                    if (GLOBAL.emailCount == -1) {
                        participantCombo.setValue(1);
                        /*packageCombo.select(0,true);*/
                        var extPanel = new Ext.Panel({
                            id: "extPanel",
                            width: 'auto',
                            layout: 'table',
                            layoutConfig: {
                                columns: 8
                            }
                        });

                        var inpL = new Ext.form.Label({
                            cls: 'p_labels',
                            text: 'Email' + (1)
                        });
                        var inp = new Ext.form.TextField({
                            name: 'email' + (1),
                            id: 'email' + (1),
                            width: 180,
                            allowBlank:false
                        });

                        inp.setValue(GLOBAL.email);
                        extPanel.add(inpL);
                        extPanel.add(inp);
                        extPanel.render('fields');
                    }
                }
            }
        }
    });

    var participantOtherCombo = new Ext.form.ComboBox({
        id: 'participantOtherCombo',
        valueField: 'idE',
        displayField: 'countE',
        hiddenName : 'iDparticipantOtherCombo',
        mode: 'local',
        width: 40,
        store: participantOtherStaff,
        forceSelection: true,
        triggerAction: 'all',
        selectOnFocus:true,
        editable: false,
        allowBlank: false,
        lazyRender:true,
        maxHeight: 100,
        hidden: isPublicUser(),
        shadow : false,
        tabIndex :5,
        listeners:{
            afterrender: function(combo) {
                if (!(GLOBAL.emailOtherCount > 0)){
                    combo.setValue(0);
                }
                if (GLOBAL.emailOtherCount > 0) {
                    participantOtherCombo.setValue(GLOBAL.emailOtherCount);
                    if (GLOBAL.emailsOther.length > 0) {
                        var emailsOther = GLOBAL.emailsOther.substring(1, GLOBAL.emailsOther.length - 1);
                        var arrayOther = emailsOther.split(", ");
                        var extPanelOther = new Ext.Panel({
                            id: "extPanelOther",
                            title: 'Not Purchasing emails ',
                            width: 'auto',
                            layout: 'table',
                            layoutConfig: {
                                columns: 8
                            }
                        });
                        var i = 0;
                        for (i; i < arrayOther.length; i++) {
                            var inpL = new Ext.form.Label({
                                cls: 'p_labels',
                                text: 'Email' + (i + 1)
                            });
                            var inp = new Ext.form.TextField({
                                name: 'email' + (i + 1),
                                id: 'emailOther' + (i + 1),
                                width: 180,
                                allowBlank:false
                            });
                            inp.setValue(arrayOther[i]);
                            extPanelOther.add(inpL);
                            extPanelOther.add(inp);
                        }
                        extPanelOther.render('fieldsOther');
                    }
                }

            }
        }

        /*listeners:{
            afterrender: function(combo) {
                combo.setValue(0);
            }
        }   */
    });


    locationCombo.on('select', function(box, record, index) {
        var data = record.get('id');
        packageCombo.clearValue();
        staffCombo.clearValue();
        storePackage.load(
        {
            params:
            {
                'locationId': data
            }
        });
        packageCombo.setDisabled(false);

        storeStaff.load(
            {
                params: {
                    'locationId': data
                },
                callback: function () {
                    if (GLOBAL.staff != null && GLOBAL.staff != "") {
                        staffCombo.setValue(GLOBAL.staff);
                    }
                }
            });

        staffCombo.setDisabled(false);
    });

    var indexx = 6;
    participantCombo.on('select', function(box, record, index) {
        var startF = 1;
        var existingArray = [];
        for (startF; startF <= 20; startF++) {
            if (document.getElementById('email' + startF) != null) {
                existingArray[startF] = document.getElementById('email' + startF).value;
            }
        }
        document.getElementById('fields').innerHTML = "";
        var data = record.get('idE');
        var start = 1;
        var inpLAll = new Ext.form.Label({
            cls: 'p_labels_purch_bold',
            /*TO ENABLE SUBSCRIBING FUNCTIONALITY SET hidden false*/
            hidden:true,
            text: 'Subscribe All / Unsubscribe All'
        });

        var checkAll = new Ext.form.Checkbox({
            name: 'checkemailAll',
            id: 'checkemailAll',
            checked:false,
            /*TO ENABLE SUBSCRIBING FUNCTIONALITY SET hidden false*/
            hidden:true,
            tabIndex :indexx,
            listeners : {
                check : function(checkbox, checked) {
                    var istart = 1;
                    for (istart; istart <= 20; istart++) {
                        if (document.getElementById('email' + istart) != null) {
                            if (checked) {
                                document.getElementById('checkemail' + istart).checked = true;
                            } else {
                                document.getElementById('checkemail' + istart).checked = false;
                            }

                        }
                    }
                }
            }

        });
        if (data > 0) {
            var extPanel = new Ext.Panel({
                id: "extPanel",
                title: 'Purchasing emails',
                width: 'auto',
                layout: 'table',
                layoutConfig: {
                    columns: 16
                }
            });
            extPanel.add({
                xtype : 'box',
                width : 10
            });
            extPanel.add(checkAll);
            extPanel.add(inpLAll);
            for (var i = 1; i <= 13; i++) {
                extPanel.add({
                    xtype : 'box',
                    width : 10
                });
            }
            var array = new Array();
            if (GLOBAL.emails.length > 0) {
                var emails = GLOBAL.emails.substring(1, GLOBAL.emails.length - 1);
                array = emails.split(", ");
            }
            for (start; start <= data; start++) {
                var startLbl = start;
                if (start <= 9) {
                    startLbl = '0' + startLbl;
                }
                var inpL = new Ext.form.Label({
                    cls: 'p_labels_purch',
                    text: 'Email' + startLbl
                });
                var inp = new Ext.form.TextField({
                    name: 'email' + start,
                    id: 'email' + start,
                    width: 180,
                    allowBlank:true,
                    tabIndex :indexx++
                });
                var check = new Ext.form.Checkbox({
                    name: 'checkemail' + start,
                    id: 'checkemail' + start,
                   /*TO ENABLE SUBSCRIBING FUNCTIONALITY SET hidden false*/
                    hidden:true,
                    checked:false

                });
                if (array.length > 0 && start <= array.length) {
                    inp.setValue(array[start]);
                }
                if (existingArray.length > 0 && start <= existingArray.length) {
                    inp.setValue(existingArray[start]);
                } else{
                    inp.setValue("");
                }
                extPanel.add(inpL);
                extPanel.add(check);
                extPanel.add(inp);
                extPanel.add({
                    xtype : 'box',
                    width : 40
                });
            }


            extPanel.render('fields');
        }
    });

    var indexxOther = 30;
    participantOtherCombo.on('select', function(box, record, index) {
        var startFOther = 1;
        var existingArrayOther = [];
        for (startFOther; startFOther <= 20; startFOther++) {
            if (document.getElementById('emailOther' + startFOther) != null) {
                existingArrayOther[startFOther] = document.getElementById('emailOther' + startFOther).value;
            }
        }
        document.getElementById('fieldsOther').innerHTML = "";
        var data = record.get('idE');
        var startOther = 1;


        var inpLOtherAll = new Ext.form.Label({
            cls: 'p_labels_purch_bold',
            /*TO ENABLE SUBSCRIBING FUNCTIONALITY SET hidden false*/
            hidden:true,
            text: 'Subscribe All / Unsubscribe All'
        });

        var checkOtherAll = new Ext.form.Checkbox({
            name: 'checkOtherEmailAll',
            id: 'checkOtherEmailAll',
            checked:false,
            /*TO ENABLE SUBSCRIBING FUNCTIONALITY SET hidden false*/
            hidden:true,
            tabIndex: indexxOther,
            listeners : {
                check : function(checkbox, checked) {
                    var jstart = 1;
                    for (jstart; jstart <= 20; jstart++) {
                        if (document.getElementById('emailOther' + jstart) != null /*&&  document.getElementById('emailOther' + jstart).value.length!=0*/) {
                            if (checked) {
                                document.getElementById('checkemailOther' + jstart).checked = true;
                            } else {
                                document.getElementById('checkemailOther' + jstart).checked = false;
                            }

                        }
                    }
                }
            }

        });

        if (data > 0) {
            var extPanelOther = new Ext.Panel({
                id: "extPanelOther",
                title: 'Not Purchasing emails',
                width: 'auto',
                layout: 'table',
                layoutConfig: {
                    columns: 16
                }
            });
            extPanelOther.add({
                xtype : 'box',
                width : 10
            });
            extPanelOther.add(checkOtherAll);
            extPanelOther.add(inpLOtherAll);
            for (var i = 1; i <= 13; i++) {
                extPanelOther.add({
                    xtype : 'box',
                    width : 10
                });
            }

            var array = new Array();
            if (GLOBAL.emailsOther.length > 0) {
                var emails = GLOBAL.emailsOther.substring(1, GLOBAL.emailsOther.length - 1);
                array = emails.split(", ");
            }
            for (startOther; startOther <= data; startOther++) {
                var startOtherLbl = startOther;
                if (startOther <= 9) {
                    startOtherLbl = '0' + startOtherLbl;
                }
                var inpLOther = new Ext.form.Label({
                    cls: 'p_labels_purch',
                    text: 'Email' + startOtherLbl
                });
                var inpOther = new Ext.form.TextField({
                    name: 'emailOther' + startOther,
                    id: 'emailOther' + startOther,
                    width: 180,
                    allowBlank:true,
                    tabIndex: indexxOther++
                });
                var checkOther = new Ext.form.Checkbox({
                    name: 'checkemailOther' + startOther,
                    id: 'checkemailOther' + startOther,
                     /*TO ENABLE SUBSCRIBING FUNCTIONALITY SET hidden false*/
                    hidden:true,
                    checked:false
                });
                if (array.length > 0 && startOther <= array.length) {
                    inpOther.setValue(array[startOther]);
                }
                if (existingArrayOther.length > 0 && startOther <= existingArrayOther.length) {
                    inpOther.setValue(existingArrayOther[startOther]);
                } else{
                    inpOther.setValue("");
                }
                extPanelOther.add(inpLOther);
                extPanelOther.add(checkOther);
                extPanelOther.add(inpOther);
                extPanelOther.add({
                    xtype : 'box',
                    width : 40
                });

            }
            extPanelOther.render('fieldsOther');
        }
    });

    var tabIndexRest = 55;
    var strfilename = new Ext.form.TextField({
        name: 'strfilename',
        id: 'strfilename',
        width: 235,
        allowBlank:true,
        hidden: isPublicUser(),
        tabIndex :56
    });

    var bodyMessagePanel = new Ext.Panel({
        id: "bodyMessagePanel",
        width: 'auto',
        layout: 'table',
        layoutConfig: {
            columns: 4
        },
        items: [
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'Subject',
                hidden: isPublicUser()
            },
            {
                xtype: 'textfield',
                name: 'subject',
                id: 'subject',
                width: 200,
                allowBlank:true,
                hidden: isPublicUser(),
                tabIndex :55
            },
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'Stringed Filename',
                hidden: isPublicUser()
            },
            strfilename,
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'Email body',
                style:{verticalAlign: 'top'},
                hidden: isPublicUser()
            },
            {
                xtype: 'textarea',
                name: 'emailbody',
                id: 'emailbody',
                width: 300,
                height: 100,
                allowBlank:true,
                hidden: isPublicUser(),
                tabIndex :57
            },
            {
                xtype: 'label',
                cls: 'p_labels',
                textAlign: 'right',
                colspan: 2
            }
        ]
    });

    var emailPanel = new Ext.Panel({
        title: 'Email Images',
        width: 'auto',
        layout: 'table',
        layoutConfig: {
            columns: 1
        },
        items: [
            {
                width: 'auto',
                layout: 'table',
                layoutConfig: {
                    columns: 10
                },
                items: [
                    {
                        xtype: 'label',
                        cls: 'p_labels_purch_menu',
                        text: 'Location',
                        hidden: isUser()
                    },
                    locationCombo ,
                    {
                        xtype: 'label',
                        cls: 'p_labels_purch_menu',
                        text: 'Package',
                        hidden: isPublicUser()
                    },
                    packageCombo,
                    {
                        xtype: 'label',
                        cls: 'p_labels_purch_menu',
                        text: '# of participants',
                        hidden: isPublicUser()
                    },
                    participantCombo,
                    {
                        xtype: 'label',
                        cls: 'p_labels_purch_menu',
                        text: 'Preview From Home',
                        hidden: isPublicUser()
                    },
                    participantOtherCombo,
                    {
                        xtype: 'label',
                        cls: 'p_labels_purch_menu',
                        text: 'Staff',
                        hidden: isPublicUser()
                    },
                    staffCombo
                ],
                colspan: 8
            },
            {
                id: "fields",
                width: 'auto'
            },
            {
                id: "fieldsOther",
                width: 'auto'
            },
            bodyMessagePanel
        ]

    });
    Ext.namespace('Ext.ux');

    /**
     * Start bottom panel elements
     */
    var writer = new Ext.data.JsonWriter({
        encode: true,
        writeAllFields: true
    });

    var storeCatalogSelected = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            api: {
                read :    '../../getPicturesByUser.json',
                update :    '../../getPicturesByUserUpdate.json',
                destroy : '../../getPicturesByUserDelete.json'
            }

        }),
        reader: new Ext.data.JsonReader({
            root:'children',
            totalProperty: 'total',
            idProperty: 'id'
        },
                ['id','url', 'base64code', 'name', 'cameraId','cameraName',
                    {
                        name: 'selected',
                        type: 'boolean'
                    },
                    {
                        name: 'creationDate'
                    },
                    {
                        name: 'logoMainUrl'
                    },
                    {
                        name: 'logoUrl'
                    },
                    {
                        name: 'pictureSize'
                    },
                    {
                        name: 'logoText'
                    },
                    {
                        name: 'selectedPurchase',
                        type: 'boolean'
                    },
                    {
                        name:"toPrintNumber"
                    },
                    {
                        name: 'addToFacebook',
                        type: 'boolean'
                    },
                    {
                        name: 'addToFlickr',
                        type: 'boolean'
                    },
                    {
                        name: 'rotated',
                        type: 'boolean'
                    }

                ]),
        /*baseParams: {
            requestId: GLOBAL.requestId,
            purchaseId: GLOBAL.purchId,
            purchaseIdCat: GLOBAL.purchIdCat,
            deleted:1
        },*/

        writer: writer
    });
    var total = storeCatalogSelected.getTotalCount();

    Ext.ux.PageSizePlugin = function() {
        Ext.ux.PageSizePlugin.superclass.constructor.call(this, {
            store: new Ext.data.SimpleStore({
                fields: ['text', 'value'],
                data: [
                    ['5', 5],
                    ['10', 10],
                    ['20', 20],
                    ['25', 25],
                    ['30', 30],
                    ['50', 50]
                ]
            }),
            mode: 'local',
            displayField: 'text',
            valueField: 'value',
            editable: false,
            allowBlank: false,
            triggerAction: 'all',
            width: 40
        });
    };

    Ext.extend(Ext.ux.PageSizePlugin, Ext.form.ComboBox, {
        init: function(paging) {
            paging.on('render', this.onInitView, this);
            paging.addEvents('pagesizechanged');
        },

        onInitView: function(paging) {
            paging.add('Page size',
                    this,
                    '-'
                    );
            this.setValue(paging.pageSize);
            this.on('select', this.onPageSizeChanged, paging);
        },

        onPageSizeChanged: function(combo) {
            this.pageSize = parseInt(combo.getValue());
            this.fireEvent('pagesizechanged', this, this.pageSize);
            this.doLoad(0);
        },
        listWidth: 40,
        shadow: false,
        autoScroll: false

    });

    var pagingbar = new Ext.PagingToolbar({
        store: storeCatalogSelected,
        pageSize: GLOBAL.pageSize,
        prependButtons: true,
        beforePageText: '',
        style:{float: 'right'},
        hideLabel: true,
        listeners: {
            pagesizechanged: function(paging, pageSize) {
                Ext.Ajax.request({
                    url: '../../getPurchasePicturesPageSizeUpdate.json',
                    method: 'GET',
                    params: {
                        id: 'id',
                        pageSize: pageSize
                    },
                    callback: function(options, success, response) {
                        storeCatalogSelected.reload({
                            /*params:{
                               // deleted: 1,
                                requestId: GLOBAL.requestId,
                                purchaseId: GLOBAL.purchId,
                                purchaseIdCat: GLOBAL.purchIdCat
                            }  */
                        }   );
                    }
                });
            }
        },
        plugins: [new Ext.ux.PageSizePlugin()]
    });

    pagingbar.refresh.hide();

    var numberComboStore = new Ext.data.ArrayStore({
        id: 0,
        fields: [
            'idN',
            'countN'
        ],
        data: [
            [0, '0'],
            [1, '1'],
            [2, '2'],
            [3, '3'],
            [4, '4'],
            [5, '5'],
            [6, '6'],
            [7, '7'],
            [8, '8'],
            [9, '9'],
            [10, '10'],
            [11, '11'],
            [12, '12'],
            [13, '13'],
            [14, '14'],
            [15, '15'],
            [16, '16'],
            [17, '17'],
            [18, '18'],
            [19, '19'],
            [20, '20'],
            [21, '21'],
            [22, '22'],
            [23, '23'],
            [24, '24'],
            [25, '25'],
            [26, '26'],
            [27, '27'],
            [28, '28'],
            [29, '29'],
            [30, '30']
        ]
    });


    Ext.util.Format.comboRenderer = function(combo) {
        return function(value) {
            var record = combo.findRecord(combo.valueField, value);
            return record ? record.get(combo.displayField) : '0';
        }
    }
    var numberCombo = new Ext.form.ComboBox({
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
        store: numberComboStore,
        valueField: 'idN',
        displayField: 'countN',
        width: 40,
        maxHeight: 100,
        shadow : false
    });
                            
    var toFacebook =0;
    var gridColumns = [
        {
            dataIndex: 'id',
            header:'Action',
            width: 100,
            menuDisabled: true,
            renderer: function (v, p, r)
            {
                if (r.data.selected || storeCatalogSelected.requestId != -1)
                    return '<div class="purchase_action">' +
                           '<img src="../../js/ux/images/custom/purchaseactions/delete.png">' +
                           '</div>';
            }
        },
        {
            dataIndex: 'base64code',
            header:'Pictures to Email',
            width: 250,
            menuDisabled: true,
            renderer: function (v, p, r)
            {
                if (r.data.selected || storeCatalogSelected.requestId != -1) {
                    /*  var temp = false;
                     if(GLOBAL.selectedids!= null && GLOBAL.selectedids!=""){
                     var dd = GLOBAL.selectedids.split(",");
                     for(var j=0; j<dd.length; j++){
                     if(dd[j]==r.data.id){
                     temp=true;
                     }
                     }
                     if(!temp){
                     //r.data.selected=false;
                     r.data.selectedPurchase=false;
                     //r.set('selectedPurchase', false);
                     storeCatalogSelected.save();
                     }
                     }else{
                     temp=true;
                     }
                     */
                    var log1 =  (r.data.logoUrl.length==0)?'<img src="" style="visibility:hidden;"/>':'<img src="..' + r.data.logoUrl+ '" class="purchase_watermark_logoimg '+r.data.rotated+' '+r.data.pictureSize+'" title = "" alt=""/>';
                    var log2 =  (r.data.logoMainUrl.length==0)?'<img src="" style="visibility:hidden;"/>':'<img src="..' + r.data.logoMainUrl+ '" class="purchase_watermark_logoimg_second '+r.data.rotated+' '+r.data.pictureSize+'" title = "" alt=""/>';
                    if (r.data.selectedPurchase){
                        return '<div class="purchase_pict_selected">' +
                               '<div style="background: url(\'../'+ r.data.base64code +'\') center center no-repeat; background-size: contain;" class="purchase_watermark_box_img"></div>' +
                               '<div class="purchase_watermark_text '+r.data.rotated+'">' + r.data.logoText + '</div>' +
                                 log1+ log2+
                                '</div>';
                             /*  '<img src="../..' + r.data.logoUrl + '" class="purchase_watermark_logoimg"/>' +
                               '<img src="../..' + r.data.logoMainUrl + '" class="purchase_watermark_logoimg_second"/>' +*/
                    }
                    else
                        return '<div class="purchase_pict">' +
                               '<div style="background: url(\'../'+ r.data.base64code +'\') center center no-repeat; background-size: contain;" class="purchase_watermark_box_img"></div>' +
                               '<div class="purchase_watermark_text '+r.data.rotated+'">' + r.data.logoText + '</div>' +
                               log1+log2+
                               '</div>';
                               /*'<img src="../..' + r.data.logoUrl + '" class="purchase_watermark_logoimg"/>' +
                               '<img src="../..' + r.data.logoMainUrl + '" class="purchase_watermark_logoimg_second"/>' +*/

                }
            }
        },
        {
            dataIndex: 'toPrintNumber',
            header:'Pictures to Print',
            width: 150,
            menuDisabled: true,
            editor: numberCombo,
            hidden: isPublicUser(),
            // specify reference to combo instance
            renderer: function (v, p, r)
            {
                if (r.data.selected || storeCatalogSelected.requestId != -1) {
                    return r.data.toPrintNumber;
                }
            }
        },
        {
            dataIndex:'creationDate',
            header:'Date',
            width:150,
            menuDisabled: true,
            renderer: function (v, p, r) {
                if (r.data.selected || storeCatalogSelected.requestId != -1)
                    return v;
            }
        },
        {
            dataIndex: 'exportToFacebook',
            header:'Export To',
            hidden: isPublicUser(),
            width: 70,
            menuDisabled: true,
            renderer: function (v, p, r)
            {
                if (r.data.selected || storeCatalogSelected.requestId != -1)
                if (r.data.addToFacebook){
                    toFacebook=1;
                    return '<div class="purchase_export_action_selected">' +
                           '<img src="../../js/ux/images/custom/purchaseactions/tofacebook.png"></div>';
                }
                else{
                    return '<div class="purchase_export_action">' +
                            '<img src="../../js/ux/images/custom/purchaseactions/tofacebook.png"></div>';

                }
            }

        },
        {
            dataIndex: 'exportToFlickr',
            width: 70,
            menuDisabled: true,
            hidden: isPublicUser(),
            renderer: function (v, p, r)
            {
                if (r.data.selected || storeCatalogSelected.requestId != -1)
                if (r.data.addToFlickr)
                    return '<div class="purchase_export_action_flick_selected">' +
                           '<img src="../../js/ux/images/custom/purchaseactions/toflickr.png"></div>';
                else return '<div class="purchase_export_action_flick">' +
                            '<img src="../../js/ux/images/custom/purchaseactions/toflickr.png"></div>';
            }
        }

    ];

    var gridColModel = new Ext.grid.ColumnModel(gridColumns);


    var selectedImagesGrid = new Ext.grid.EditorGridPanel({
        id: 'selectedImageGrid',
        store: storeCatalogSelected,
        height: 380,
        width: 950,
        cm: gridColModel
    });

    selectedImagesGrid.on({
        'cellclick' : function(grid, rowIndex, columnIndex, e) {
            var record = grid.getStore().getAt(rowIndex);
            if (!isPayed()) {
                if (columnIndex == 0)
                {
                    Ext.Ajax.request({
                        url: '../../getPicturesByUserDelete.json',
                        params:{
                            'pictures': record.get("id"),
                            'requestId': GLOBAL.requestId,
                            'purchaseId': GLOBAL.purchId
                        },
                        callback: function(options, success, response) {
                            storeCatalogSelected.reload({
                                /*params:{
                                   // deleted: -1,
                                    requestId: GLOBAL.requestId,
                                    purchaseId: GLOBAL.purchId,
                                    purchaseIdCat: GLOBAL.purchIdCat
                                }*/
                            });
                        }
                    });
                    if (record.get('selectedPurchase')){
                        total = total - 1;
                        totalLabel.setText(total);
                        totalPriceLabel.setText(total * GLOBAL.price + ' $');
                    }
                }
                if (columnIndex == 1)
                {
                    if (record.get('selectedPurchase')) {
                        total = total - 1;
                    } else {
                        total = total + 1;
                    }
                    totalLabel.setText(total);
                    totalPriceLabel.setText(total * GLOBAL.price + ' $');
                    record.set('selectedPurchase', !record.get('selectedPurchase'));

                    grid.getStore().save(
                        {
                            /*params:{
                                deleted: 1,
                                requestId: GLOBAL.requestId,
                                purchaseId: GLOBAL.purchId,
                                purchaseIdCat: GLOBAL.purchIdCat
                            }*/
                        }

                    );
                }
                if (columnIndex == 2)
                {
                    grid.startEditing(rowIndex, columnIndex);
                }
                if (columnIndex == 4) {
                    record.set('addToFacebook', !record.get('addToFacebook'));
                    grid.getStore().save({
                        /*params:{
                            //deleted: 1,
                            requestId: GLOBAL.requestId,
                            purchaseId: GLOBAL.purchId,
                            purchaseIdCat: GLOBAL.purchIdCat
                        }   */
                    }   );
                }
                if (columnIndex == 5) {
                    record.set('addToFlickr', !record.get('addToFlickr'));
                    grid.getStore().save({
                        /*params:{
                           // deleted: 1,
                            requestId: GLOBAL.requestId,
                            purchaseId: GLOBAL.purchId,
                            purchaseIdCat: GLOBAL.purchIdCat
                        }   */
                    }   );
                }
            } else {
                Ext.Msg.alert('Failure:', 'Please complete sale before selecting different pictures');
            }
        },
        scope: this
    });

    selectedImagesGrid.setAutoScroll(true);


    /**
     * Select/DeselectAll Panel
     */
    var selectBox = new Ext.Panel({
        defaultType:'label',
        items: [
            {
                text:'Select',
                cls: 'selDeselPanel_label'
            }
        ],
        listeners: {render: function(c) {
            c.body.on('click', function() {
                var countrecords = storeCatalogSelected.getCount();
                if (countrecords > 0) {
                    for (var i = 0; i < countrecords; i++) {
                        var record = storeCatalogSelected.getAt(i);
                        if(!record.get('selectedPurchase')){
                            total = total + 1;
                        }
                        record.set('selectedPurchase', true);
                    }
                    storeCatalogSelected.save({
                       /* params:{
                            //deleted: 1,
                            requestId: GLOBAL.requestId,
                            purchaseId: GLOBAL.purchId,
                            purchaseIdCat: GLOBAL.purchIdCat
                        }     */
                    }   );
                    totalLabel.setText(total);
                    totalPriceLabel.setText(total * GLOBAL.price + ' $');
                }
            });
        },
            scope: this
        }
    });

    var deselectBox = new Ext.Panel({
        defaultType:'label',
        items: [
            {
                text:'Deselect All',
                cls: 'selDeselPanel_label'
            }
        ],
        listeners: {render: function(c) {
            c.body.on('click', function() {
                var countrecords = storeCatalogSelected.getCount();
                if (countrecords > 0) {
                    for (var i = 0; i < countrecords; i++) {
                        var record = storeCatalogSelected.getAt(i);
                        if(record.get('selectedPurchase')){
                            total = total - 1;
                        }
                        record.set('selectedPurchase', false);
                    }
                    storeCatalogSelected.save({
                       /* params:{
                           // deleted: 1,
                            requestId: GLOBAL.requestId,
                            purchaseId: GLOBAL.purchId,
                            purchaseIdCat: GLOBAL.purchIdCat
                        }   */
                    }   );
                    totalLabel.setText(total);
                    totalPriceLabel.setText(total * GLOBAL.price + ' $');
                }
            });
        },
            scope: this
        }
    });

    var selDeselPanel = new Ext.Panel({
        id: 'selDeselPanel',
        cls:'selDeselPanel',
        layout: 'table',
        layoutConfig: {
            columns: 3
        },
        items:[selectBox,
            {
                xtype: 'label',
                cls: 'label_deselLinks',
                text: '/'
            },deselectBox
        ]
    });

    var selectedImagesPanel = new Ext.Panel({
        id: 'selectedImage',
        width: 'auto',
        height: 500,
        title:'Selected Images',
        layout: 'table',
        layoutConfig: {
            columns: 1
        },
        items: [pagingbar,selDeselPanel, selectedImagesGrid]
    });

    /**
     *  Start button Panel
     */
    function isPayed() {
        return GLOBAL.payed;
    }

    function isPayedIsPublic() {
        if (GLOBAL.payed) {
            return false;
        } else {
            if(isNullPriceAndPublic()){
                return false;
            }else{
                return true
            }
           /* if (GLOBAL.userId == 3 || GLOBAL.userId == 4 || GLOBAL.userId == 5) {
                return true
            } else {
                return false;
            }*/
        }
    }

    var myMask = new Ext.LoadMask(Ext.getBody(), { msg:" Please wait..."});

    function purchaseHandler() {
        myMask.show();
        if ((locationCombo.getValue() == null || locationCombo.getValue().length == 0) && !isUser()) {
            myMask.hide();
            Ext.Msg.alert('Failure:', 'Define the Location');
            return;
        }
        if ((packageCombo.getValue() == null || packageCombo.getValue().length == 0) && !isPublicUser()) {
            myMask.hide();
            Ext.Msg.alert('Failure:', 'Define the Package');
            return;
        }
        if (participantCombo.getValue() == null || participantCombo.getValue().length == 0) {
            participantCombo.setValue("0");
        }
        if (participantCombo.getValue() != 0) {
            var subj = document.getElementById("subject");
            var strfile = document.getElementById("strfilename");
            var bodyE = document.getElementById("emailbody");
            if (!isPublicUser()) {
                if (subj != null && (subj.value == null || subj.value.length == 0)) {
                    myMask.hide();
                    Ext.Msg.alert('Failure:', 'Define the Subject');
                    return;
                }
                if (strfile != null && (strfile.value == null || strfile.value.length == 0)) {
                    myMask.hide();
                    Ext.Msg.alert('Failure:', 'Define the File Name');
                    return;
                }
                if (bodyE != null && (bodyE.value == null || bodyE.value.length == 0)) {
                    myMask.hide();
                    Ext.Msg.alert('Failure:', 'Define the Body');
                    return;
                }
            }
        }else{
            myMask.hide();
            Ext.Msg.alert('Failure:', 'Please, set the # of participants');
            return;
        }
        if (isPublicUser()) {
            var id = "email1";
            var elem = document.getElementById(id);
            if (elem != null) {
                var address = elem.value;
                if (address == null || address == "") {
                    myMask.hide();
                    Ext.Msg.alert('Failure:', 'Please enter email Address');
                    return;
                }
            }
        }
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var errorMsg = "";
        var errorEmails = "";
        var i = 1;
        for (i; i <= 20; i++) {
            var id = "email" + i;
            var elem = document.getElementById(id);
            if (elem != null) {
                var address = elem.value;
                if (address != null && address != "" && !reg.test(address)) {
                    errorEmails += id + ","
                }
            }
        }
        if (errorEmails.length > 0) {
            errorEmails = 'Invalid Email Addresses: ' + errorEmails;
            errorMsg = errorEmails.substring(0, errorEmails.length - 1);
            myMask.hide();
            Ext.Msg.alert('Failure:', errorMsg);
            return;
        }

        var p_accessToken = "";
        if(toFacebook==1){
            FB.login(function(response){
                    var p_accessToken = '', userId, auth,
                        fb_cred;
                    if(response){
                        auth = response.authResponse;
                        if(auth){
                            p_accessToken = auth.accessToken;
                            userId = auth.userID;
                            fb_cred = {
                                'accessToken': p_accessToken,
                                'userId': userId
                            }
                        }
                    }

                    submitPurchaseForm(p_accessToken);
                }
                ,{scope:"publish_actions,user_photos"});
        }else{
            submitPurchaseForm("");
        }
    }

    function submitPurchaseForm(p_accessToken){
        if(!isPublicUser() && GLOBAL.requestId==-1){
            saveBeforePurchase(p_accessToken);
        }else{
            submitPurchaseForm1(p_accessToken, '');
        }
    }

    function submitPurchaseForm1(p_accessToken, messageCode){

        mainPanel.getForm().submit({
            url: '../../submitPurchaseForm.json',
            timeout: 300000,
            scope:this,
            params: {
                'requestId': GLOBAL.requestId,
                'purchaseId': GLOBAL.purchId,
                'accessToken': p_accessToken
            },
            success: function(f, a) {
                var result = a.result;
                var array = Ext.util.JSON.encode(result.picturesToPrint);
                var logoLocat = Ext.util.JSON.encode(result.logoLocation);
                var mainlogoLocat = Ext.util.JSON.encode(result.mainLogoLocation);

                if (result.picturesToPrint.length > 0) {
                    var lo1 = Ext.urlAppend(GLOBAL.contextPath + '/pictures/main/previewprint', Ext.urlEncode({picturesToPrint:array,logoLocation: logoLocat,mainLogoLocation: mainlogoLocat}));
                    window.open(
                        lo1,
                        'Fotaflo Preview' + new Date().getMilliseconds(),
                        'scrollbars=yes,menubar=yes,height=800,width=800,resizable=yes,toolbar=yes,location=no,status=no'
                    );
                }
                if (result.exportUrl)
                {

                    var iframe = new Ext.Component({
                        renderTo: Ext.getBody(),
                        autoEl:{
                            tag: 'iframe',
                            src: result.exportUrl,
                            cls: "hidden-iframe"
                        }});
                }
                myMask.hide();
                var confMessage =  isPublicUser()?'Don\'t press \"OK\" until download dialog window opens.':'';
                var codemsg = '';
                if(messageCode!=''){
                    codemsg= '</br><div style="position: absolute;left: 60px; font-size: 18pt; font-weight: bold;">'+ 'Code: ' + messageCode + '</div>'
                }
              //  Ext.MessageBox.buttonText.ok = 'Code';
                Ext.Msg.alert('Success:', 'The request was successfully processed. '+confMessage+codemsg, function() {
                    Ext.Ajax.request({
                        url: '../../getPicturesClearSelections.json',
                        success: function (result, request) {
                            var lo = Ext.urlAppend(GLOBAL.contextPath + '/pictures/main/common', Ext.urlEncode({getFilter: "load"}));
                            window.location = lo;
                        },
                        failure: function (result, request) {
                            Ext.MessageBox.alert('Failed', result.responseText);
                        }
                    });

                });
            },
            failure: function(f, a) {
                myMask.hide();
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure:', 'Form fields may not be submitted with empty or invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure:', 'Server connection problems. Please, try to reload page.');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure:', a.result.msg);
                }

            }
        });
    }

    function getFormValues(){
        var values,
            records = storeCatalogSelected.getRange(),
            recordsData = [],
            id, el;
        for (var i = 0; i < records.length; i++) {
            recordsData.push(records[i].data);
        }
        var emails = [];
        for (var j = 1; j <= participantCombo.getValue(); j++) {
            id = 'email'+j;
            el = Ext.getCmp(id);
            if(el){
                if(el.getValue()){
                    emails.push( el.getValue() );
                }
            }
        }
        var emailsOther = [];
        for (var j = 1; j <= participantOtherCombo.getValue(); j++) {
            id = 'emailOther'+j;
            el = Ext.getCmp(id);
            if(el){
                if(el.getValue()){
                    emailsOther.push( el.getValue() );
                }
            }
        }

        values = {
            'location'          : locationCombo.getValue(),
            'package'           : packageCombo.getValue(),
            'participant'       : participantCombo.getValue(),
            'participantOther'  : participantOtherCombo.getValue(),
            'staff'             : staffCombo.getValue(),
            'subject'           : Ext.getCmp("subject").getValue(),
            'strfilename'       : Ext.getCmp("strfilename").getValue(),
            'emailbody'         : Ext.getCmp("emailbody").getValue(),
            'purchaseId'        : GLOBAL.purchId,
            'purchaseIdCat'     : GLOBAL.purchIdCat,
            'startDate'         : GLOBAL.startDate,
            'endDate'           : GLOBAL.endDate,
            'startDay'          : GLOBAL.startDay,
            'endDay'            : GLOBAL.endDay,
            'startTime'         : GLOBAL.startTime,
            'endTime'           : GLOBAL.endTime,
            'children'          : Ext.encode(recordsData),
            'emails'            : Ext.encode(emails),
            'emailsOther'       : Ext.encode(emailsOther)
        };

        return values;
    }

    function savePurchaseForm(){
        var records = storeCatalogSelected.getRange();
        var recordsData = [];
        for (var i = 0; i < records.length; i++) {
            recordsData.push(records[i].data);
        }
        var emails = [];
        for (var j = 1; j <= participantCombo.getValue(); j++) {
            var em = 'email'+j;
            emails.push( document.getElementById(em).value);
        }
        var emailsOther = [];
        for (var j = 1; j <= participantOtherCombo.getValue(); j++) {
            var em = 'emailOther'+j;
            emailsOther.push( document.getElementById(em).value);
        }

        Ext.Ajax.request({
            url: '../../savePurchaseForm.json',
            params: {
                'location': locationCombo.getValue(),
                'package': packageCombo.getValue(),
                'participant': participantCombo.getValue(),
                'participantOther': participantOtherCombo.getValue(),
                'staff': staffCombo.getValue(),
                'subject': document.getElementById("subject").value,
                'strfilename': document.getElementById("strfilename").value,
                'emailbody': document.getElementById("emailbody").value,
                'purchaseId' :GLOBAL.purchId,
                'purchaseIdCat' :GLOBAL.purchIdCat,
                'startDate': GLOBAL.startDate,
                'endDate': GLOBAL.endDate,
                'startDay': GLOBAL.startDay,
                'endDay': GLOBAL.endDay,
                'startTime': GLOBAL.startTime,
                'endTime': GLOBAL.endTime,
                'children': Ext.encode(recordsData),
                'emails': Ext.encode(emails),
                'emailsOther':  Ext.encode(emailsOther)
            },
            success: function (result, request) {
                var res = Ext.util.JSON.decode(result.responseText).code;
                var codemsg = '</br><div style="position: absolute;left: 60px; font-size: 18pt; font-weight: bold;">'+ res + '</div>'

                Ext.Msg.alert('Code:', 'Code was successfully generated:'+codemsg, function()


                {
                    var lo = Ext.urlAppend(GLOBAL.contextPath + '/pictures/main/common', Ext.urlEncode({getFilter: "load"}));
                    window.location = lo;
                });

            },
            failure: function (result, request) {
                Ext.MessageBox.alert('Failed', result.responseText);
            }
        });
    }

    function generatePurchaseForm(){
        var records = storeCatalogSelected.getRange();
        var recordsData = [];
        for (var i = 0; i < records.length; i++) {
            recordsData.push(records[i].data);
        }
        var emails = [];
        for (var j = 1; j <= participantCombo.getValue(); j++) {
            var em = 'email'+j;
            emails.push( document.getElementById(em).value);
        }
        var emailsOther = [];
        for (var j = 1; j <= participantOtherCombo.getValue(); j++) {
            var em = 'emailOther'+j;
            emailsOther.push( document.getElementById(em).value);
        }
        Ext.Ajax.request({
            url: '../../savePurchaseForm.json',
            params: {
                'location': locationCombo.getValue(),
                'package': packageCombo.getValue(),
                'participant': participantCombo.getValue(),
                'participantOther': participantOtherCombo.getValue(),
                'staff': staffCombo.getValue(),
                'subject': document.getElementById("subject").value,
                'strfilename': document.getElementById("strfilename").value,
                'emailbody': document.getElementById("emailbody").value,
                'purchaseId' :GLOBAL.purchId,
                'purchaseIdCat' :GLOBAL.purchIdCat,
                'startDate': GLOBAL.startDate,
                'endDate': GLOBAL.endDate,
                'startDay': GLOBAL.startDay,
                'endDay': GLOBAL.endDay,
                'startTime': GLOBAL.startTime,
                'endTime': GLOBAL.endTime,
                'children': Ext.encode(recordsData),
                'emails': Ext.encode(emails),
                'emailsOther':  Ext.encode(emailsOther)
            },
            success: function (result, request) {
                var res = Ext.util.JSON.decode(result.responseText).code;
                var codemsg = '</br><div style="position: absolute;left: 60px; font-size: 18pt; font-weight: bold;">'+ res + '</div>'

                var reload =Ext.util.JSON.decode(result.responseText).reload;


                if(reload!=-1){
                    Ext.Msg.alert('Code:', 'Code was successfully generated:'+codemsg, function()
                    {
                        var lo = Ext.urlAppend(GLOBAL.contextPath + '/pictures/main/purchase', Ext.urlEncode({gen: reload}));
                        window.location = lo;
                    });


                }else{
                    Ext.Msg.alert('Code:', 'Code was successfully generated:'+codemsg);
                }

            },
            failure: function (result, request) {
                Ext.MessageBox.alert('Failed', result.responseText);
            }
        });
    }

    function saveBeforePurchase(p_accessToken){
        var records = storeCatalogSelected.getRange();
        var recordsData = [];
        for (var i = 0; i < records.length; i++) {
            recordsData.push(records[i].data);
        }
        var emails = [];
        for (var j = 1; j <= participantCombo.getValue(); j++) {
            var em = 'email'+j;
            emails.push( document.getElementById(em).value);
        }
        var emailsOther = [];
        for (var j = 1; j <= participantOtherCombo.getValue(); j++) {
            var em = 'emailOther'+j;
            emailsOther.push( document.getElementById(em).value);
        }
        Ext.Ajax.request({
            url: '../../saveBeforePurchaseForm.json',
            params: {
                'location': locationCombo.getValue(),
                'package': packageCombo.getValue(),
                'participant': participantCombo.getValue(),
                'participantOther': participantOtherCombo.getValue(),
                'staff': staffCombo.getValue(),
                'subject': document.getElementById("subject").value,
                'strfilename': document.getElementById("strfilename").value,
                'emailbody': document.getElementById("emailbody").value,
                'purchaseId' :GLOBAL.purchId,
                'purchaseIdCat' :GLOBAL.purchIdCat,
                'startDate': GLOBAL.startDate,
                'endDate': GLOBAL.endDate,
                'startDay': GLOBAL.startDay,
                'endDay': GLOBAL.endDay,
                'startTime': GLOBAL.startTime,
                'endTime': GLOBAL.endTime,
                'children': Ext.encode(recordsData),
                'emails': Ext.encode(emails),
                'emailsOther':  Ext.encode(emailsOther)
            },
            success: function (result, request) {
                var res = Ext.util.JSON.decode(result.responseText).code;
                if(!isPublicUser()){
                    submitPurchaseForm1(p_accessToken,res);
                }

            },
            failure: function (result, request) {
                Ext.MessageBox.alert('Failed', result.responseText);
            }
        });
    }

    var cancelHandler = function() {
        Ext.Ajax.request({
            url: '../../cancelPurchaseForm.json',
            success: function (result, request) {
                var lo = Ext.urlAppend(GLOBAL.contextPath + '/pictures/main/common', Ext.urlEncode({getFilter: "load"}));
                window.location = lo;
            },
            failure: function (result, request) {
                Ext.MessageBox.alert('Failed', result.responseText);
            }
        });
    }

    var purchaseButton = new Ext.Button({
        text: isJustNullPriceAndPublic()?'Complete':'Purchase',
        clientValidation: true,
        disabled: isPayedIsPublic(),
        handler: purchaseHandler
    });

    var cancelButton = new Ext.Button({
        text: 'Cancel',
        disabled: isPayed(),
        handler: cancelHandler
    });

    var generateCodeButton = new Ext.Button({
        text:'Generate Code',
        hidden: (isPublicUser() || GLOBAL.requestId!=-1),
        handler: function() {
           // myMask.show();
            generatePurchaseForm();
           // myMask.hide();
        }
    });


    var savePurchaseButton = new Ext.Button({
        text:'Save',
        hidden: (isPublicUser() || GLOBAL.requestId!=-1),
        handler: function() {
            // myMask.show();
            savePurchaseForm();
            // myMask.hide();
        }
    });

    var generateCodeButton2 = new Ext.Button({
        text:'Generate Code',
        hidden: (isPublicUser() || GLOBAL.requestId!=-1),
        handler: function() {
            // myMask.show();
            generatePurchaseForm();
            // myMask.hide();
        }
    });


    var savePurchaseButton2 = new Ext.Button({
        text:'Save',
        hidden: (isPublicUser() || GLOBAL.requestId!=-1),
        handler: function() {
            // myMask.show();
            savePurchaseForm();
            // myMask.hide();
        }
    });


    var purchaseButton2 = new Ext.Button({
        text: isJustNullPriceAndPublic()?'Complete':'Purchase',
        clientValidation: true,
        disabled: isPayedIsPublic(),
        tabIndex: 71,
        handler: purchaseHandler
    });
    var myMask = new Ext.LoadMask(Ext.getBody(), { msg:" Please wait..."});
    var payPalBtn = new Ext.Button(
    {
        iconCls : 'payPalBtn',
        name : 'payPalButton',
        disabled: isPayed(),
        hidden: isNullPriceAndPublic(),
        handler : function() {
            var totalPayment = total * GLOBAL.price;
            myMask.show();
            Ext.Ajax.request({
                url: '../../registerPayment.json',
                params:{
                    totalPrice: totalPayment,
                    email: document.getElementById("email1").value/*,
                     ids: ids*/
                },
                success: function (result, request) {
                    myMask.hide();
                    var resText = Ext.util.JSON.decode(result.responseText);
                    var res = resText.success;
                    var message = resText.msg
                    if (res == true) {
                        window.location = GLOBAL.redirectpaypal + message;
                    } else {
                        Ext.MessageBox.alert('Failure', message);
                    }
                },
                failure: function (result, request) {
                    myMask.hide();
                    var res = Ext.util.JSON.decode(result.responseText).message
                    Ext.MessageBox.alert('Failed', res);
                }
            });
        }
    });

    var payPalBtn2 = new Ext.Button(
    {
        id: 'paypal-btn',
        iconCls : 'payPalBtn',
        name : 'payPalButton2',
        disabled: isPayed(),
        hidden: isNullPriceAndPublic(),
        tabIndex: 70,
        handler : function() {
            myMask.show();
            var totalPayment = total * GLOBAL.price;
            Ext.Ajax.request({
                url: '../../registerPayment.json',
                params:{
                    totalPrice: totalPayment,
                    email: document.getElementById("email1").value/*,
                     ids: ids*/
                },
                success: function (result, request) {
                    myMask.hide();
                    var resText = Ext.util.JSON.decode(result.responseText);
                    var res = resText.success;
                    var message = resText.msg
                    if (res == true) {
                        window.location = GLOBAL.redirectpaypal + message;
                    } else {
                        Ext.MessageBox.alert('Failure', message);
                    }
                },
                failure: function (result, request) {
                    myMask.hide();
                    var res = Ext.util.JSON.decode(result.responseText).message
                    Ext.MessageBox.alert('Failed', res);
                }
            });
        }
    });


    var cancelButton2 = new Ext.Button({
        text: 'Cancel',
        disabled: isPayed(),
        tabIndex: 72,
        handler: cancelHandler
    });

    var buttonPanel2 = new Ext.Panel({
        id: 'buttonPanel2',
        width: 'auto',
        height: 50,
        layout: 'table',
        layoutConfig: {
            columns: 5
        },
        items: [payPalBtn2,purchaseButton2, cancelButton2, savePurchaseButton2, generateCodeButton2]
    });

    var buttonPanel = new Ext.Panel({
        id: 'buttonPanel',
        width: 'auto',
        height: 50,
        layout: 'table',
        layoutConfig: {
            columns: 5
        },
        items: [payPalBtn,purchaseButton, cancelButton,savePurchaseButton,generateCodeButton]
    });

    var totalLabel = new Ext.form.Label({
        cls: 'p_labels_total',
        id: 'totalLabel',
        name: 'totalLabel',
        text: total
    });

    var totalText = new Ext.form.TextField({
        name: 'totalText',
        id: 'totalText',
        width: 240,
        allowBlank:false,
        value: total,
        disabled: true
    });

    var totalPriceLabel = new Ext.form.Label({
        cls: 'p_labels_total_price',
        id: 'totalPriceLabel',
        name: 'totalPriceLabel',
        hidden: !isTotalShowUser(),
        text: total
    });

    var totalPrice = new Ext.form.TextField({
        name: 'totalprice',
        id: 'totalprice',
        width: 240,
        hidden: !isPublicUser(),
        disabled: true,
        allowBlank:false
    });

    var totalPanel = new Ext.Panel({
        id: 'totalPanel',
        width: 'auto',
        height: 50,
        layout: 'table',
        layoutConfig: {
            columns: 5
        },
        items: [
            {
                xtype: 'label',
                cls: 'p_labels_total',
                text: 'Total number of pictures:'
            },
            totalLabel,
            {
                xtype : 'box',
                width : 50
            },
            {
                xtype: 'label',
                cls: 'p_labels_total',
                hidden: !isTotalShowUser(),
                text: 'Total price:'
            },
            totalPriceLabel
        ]
    });

    /**
     * Start main panel
     */
    var mainPanel = new Ext.FormPanel({
        xtype: 'form',
        id: 'purch-view',
        title:isCodeUser()?'Download page':'Purchase page',
        layout: 'table',
        layoutConfig: {
            columns: 1
        },
        items: [emailPanel,totalPanel, buttonPanel2,selectedImagesPanel, buttonPanel],
        listeners: {
            beforerender: function(pn) {
                Ext.Ajax.request({
                    url: '../../getEmailBody.json',
                    method: 'POST',
                    params:{
                        purchaseId: GLOBAL.purchId
                    },
                    success: function (result, request) {
                        var ids = result.responseText;
                        var array = Ext.util.JSON.decode(ids);
                        var emailbody = array.emailbody;
                        mainPanel.getForm().findField('emailbody').setValue(emailbody);
                    },
                    failure: function (result, request) {
                        Ext.MessageBox.alert('Failed', result.responseText);
                    }
                });
                if(GLOBAL.subject!=null && GLOBAL.subject!=""){
                    mainPanel.getForm().findField('subject').setValue(GLOBAL.subject);
                }
            }
        }/*,
         listeners: {
         afterrender: function(panel) {
         var text = mainPanel.getForm().findField('totalText');
         text.setValue(total);
         }
         }     */
    });

    storePackage.load();
    storeStaff.load();
    storeCatalogSelected.load({
      /*  params:{
            deleted: -1,
            requestId: GLOBAL.requestId,
            purchaseId: GLOBAL.purchId,
            purchaseIdCat: GLOBAL.purchIdCat
        }, */
        callback: function(store, records, options) {
            if (GLOBAL.selectedids != null && GLOBAL.selectedids != "") {
                if(GLOBAL.selectedids==" "){
                    total=0;
                }else{
                    total = GLOBAL.selectedids.split(",").length;
                }
            } else {
                    total = this.getTotalCount();
            }
            totalLabel.setText(total);           
            totalPriceLabel.setText(total * GLOBAL.price + ' $');

            var countrecords = storeCatalogSelected.getTotalCount();
            if (countrecords > 0) {
                for (var i = 0; i < countrecords; i++) {
                    var record = storeCatalogSelected.getAt(i);
                    var temp = false;
                    if (GLOBAL.selectedids != null && GLOBAL.selectedids != "") {
                        var dd = GLOBAL.selectedids.split(",");
                        for (var j = 0; j < dd.length; j++) {
                            if (dd[j] == record.get('id')) {
                                temp = true;
                            }
                        }
                        if (!temp) {
                            record.set('selectedPurchase', false);
                        }
                    }
                }
            }
            storeCatalogSelected.save({
                /*params:{
                    //deleted: 1,
                    requestId: GLOBAL.requestId,
                    purchaseId: GLOBAL.purchId,
                    purchaseIdCat: GLOBAL.purchIdCat
                } */
            }   );

            /*selectedPictures = storeCatalogSelected.data.keys;
             countSelected = selectedPictures.length;*/
        }
    });

    function isCodeUser() {
        if (GLOBAL.userId == 6) {
            return true;
        }
    }

    var htmlPurchText =isCodeUser()?'<br/>' +
        'Thank you for choosing Fotaflo, to Download your pictures follow these instructions::' +
        '<ul>' +
        '<li>Enter - your email address so that we can send you the pictures</li>' +
        '<li>Review - the pictures in the scroll down window</li>' +
        '<li>Complete - the transaction and receive the pictures by selecting the complete button</li>' +
        '<li>Download - the images from the download dialog pop-up window</li>' +
        '<li>FYI - hit cancel to return to the catalog page, your pictures will still be selected</li>' +
        '</ul>'
        :
        '<br/>' +
            'Thank you for choosing Fotaflo, to purchase pictures follow these instructions:' +
            '<ul>' +
            '<li>Enter - your email address so that we can send you the pictures</li>' +
            '<li>Review - the pictures in the scroll down window</li>' +
            '<li>Purchase - the pictures by selecting the Paypal button</li>' +
            '<li>Complete - the transaction and receive the pictures by selecting the complete button</li>' +
            '<li>Download - the images with the pop-up window</li>' +
            '<li>FYI - hit cancel to return to the catalog page, your pictures will still be selected</li>' +
            '</ul>';

    var explanationPanel = new Ext.Panel({
        id:'explanation_panel',
        frame:true,
        hidden: !isPublicUser(),
        width:'100%',
        height: 150,

        items: [
            {
                html: htmlPurchText
            }
        ]
    });

    explanationPanel.render('content');

    storeLocation.load({
        callback: function () {


            if(GLOBAL.location!=null && GLOBAL.location!=""){
                locationCombo.setValue(GLOBAL.location);
                packageCombo.setDisabled(false);
                staffCombo.setDisabled(false);
                storePackage.load(
                    {
                        params: {
                            'locationid': GLOBAL.location
                        },
                        callback: function () {
                            if(GLOBAL.packages!=null && GLOBAL.packages!=""){
                                packageCombo.setValue(GLOBAL.packages);
                            }
                        }
                    });
                storeStaff.load(
                    {
                        params: {
                            'locationId': GLOBAL.location
                        },
                        callback: function () {
                            if(GLOBAL.staff!=null && GLOBAL.staff!=""){
                                staffCombo.setValue(GLOBAL.staff);
                            }
                        }
                    });

            }
        }
    });

    mainPanel.render('content');

    if (GLOBAL.fileName.length > 0) {
        strfilename.setValue(GLOBAL.fileName);
    }
});
