Ext.onReady(function() {

    var storeLocation = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            api: {
                read: '../../locations/getActualLocations.json'
            }
        }),
        reader: new Ext.data.JsonReader({
            root:'locations'
        },
        [{name: 'id'},{name: 'locationName'}]),
        listeners: {
            load : function() {
                if (GLOBAL.userId != 1) {
                    locationComboImp.setValue(GLOBAL.locationId);
                    cameraCombo.clearValue();
                    storeCamera.load(
                    { params: {'locationId': GLOBAL.locationId}});
                    cameraCombo.setDisabled(false);
                }
            }
        }
    });

    var storeCamera = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            api: {
                read: '../../cameras/getRealCameras.json'
            }
        }),

        reader: new Ext.data.JsonReader(
        {
            root:'cameras'
        },
                [
                    {
                        name: 'id'
                    },
                    {
                        name: 'cameraName'
                    }
                ])
    });

    var cameraCombo = new Ext.form.ComboBox({
        id: 'cameraCombo',
        displayField: 'cameraName',
        valueField: 'id',
        hiddenName : 'iDcameraCombo',
        mode: 'local',
        width: 112,
        store: storeCamera,
        forceSelection: true,
        triggerAction: 'all',
        emptyText:'Select a camera...',
        selectOnFocus:true,
        editable: false,
        allowBlank: false,
        lazyRender:true,
        disabled:true,
        maxHeight: 95,
        shadow : false
    });

    var locationComboExp = new Ext.form.ComboBox({
        id: 'locationComboExp',
        displayField: 'locationName',
        valueField: 'id',
        hiddenName : 'locationId',
        mode: 'local',
        width: 112,
        store: storeLocation,
        forceSelection: true,
        triggerAction: 'all',
        emptyText:'Select a location...',
        selectOnFocus:true,
        editable: false,
        allowBlank: false,
        shadow : false
    });

    var locationComboImp = new Ext.form.ComboBox({
        id: 'locationComboImp',
        displayField: 'locationName',
        valueField: 'id',
        hiddenName : 'iDlocationCombo',
        mode: 'local',
        width: 112,
        store: storeLocation,
        forceSelection: true,
        triggerAction: 'all',
        emptyText:'Select a location...',
        selectOnFocus:true,
        editable: false,
        allowBlank: false,
        shadow : false		
    });

    if (GLOBAL.userId != 1) {
        locationComboImp.setDisabled(true);
    }
    locationComboImp.on('select', function(box, record, index) {
        var data = record.get('id');
        cameraCombo.clearValue();
        storeCamera.load(
        {
            params:
            {
                'locationId': data
            }
        });
        cameraCombo.setDisabled(false);
    });

    var myMask = new Ext.LoadMask(Ext.getBody(), { msg:" Please wait..."});
    var importPanel = new Ext.form.FormPanel({
        title: 'Upload pictures to database',
        layout: 'table',
        fileUpload: true,
        layoutConfig: {
            columns: 2,
            tableAttrs: {
                style: {width: '600px'}
            }
        },
        items: [
            {
                xtype: 'label',
                cls: 'p_labels bold',
                text: 'Path:',
                cellCls: 'labelCell'
            },
            {
                xtype : 'field',
                name: 'pictures',
                inputType : 'file',
                listeners:{
                    afterrender:function(cmp){
                        cmp.el.set({
                            multiple:'multiple'
                        });
                    }
                }
            },
            {
                xtype: 'label',
                cls: 'p_labels bold',
                text: 'Location:',
                cellCls: 'labelCell'
            },
            locationComboImp,
            {
                xtype: 'label',
                cls: 'p_labels bold',
                text: 'Camera:',
                cellCls: 'labelCell'
            },
            cameraCombo,
            {
                xtype: 'label',
                cls: 'p_labels bold',
                text: 'Time:',
                cellCls: 'labelCell'
            },
            {
                xtype: 'panel',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'datefield',
                        id: "uploadDate"
                    },
                    {
                        xtype: 'timefield',
                        width: 60,
                        id: "uploadTime",
                        cls: "timeEl",
                        editable: false,
                        minValue: '0:00 AM',
                        maxValue: '11:00 PM',
                        increment: 30,
                        shadow : false,
                        allowBlank : false
                    }

                ]
            },
            {
                xtype: 'button',
                colspan: 2,
                text: "Upload",
                handler: function() {
                    var date = importPanel.getForm().findField('uploadDate');
                    var hours = importPanel.getForm().findField('uploadTime');
                    var time = date.getRawValue() + " " + hours.getRawValue();
                    var form = importPanel.getForm();
                    var fileInput = form.findField('pictures');
                    if (GLOBAL.userId != 1) {
                        locationComboImp.setDisabled(false);
                    }
                    var errorMessage ="";
                    if (form.findField('uploadDate').getValue() == ""){
                        errorMessage =  errorMessage==""?"date":errorMessage+" ,date";
                    }
                    if (form.findField('uploadTime').getValue() == ""){
                        errorMessage =  errorMessage==""?"time":errorMessage+" ,time";
                    }
                    if (locationComboImp.getValue()=="" ){
                        errorMessage =  errorMessage==""?"location":errorMessage+" ,location";
                    }
                    if (cameraCombo.getValue()=="" ){
                        errorMessage =  errorMessage==""?"camera":errorMessage+" ,camera";
                    }
                    if (fileInput.getValue() == ""){
                        errorMessage =  errorMessage==""?"pictures to import ":errorMessage+" ,pictures to import ";
                    }
                    if (errorMessage!="") {
                       alert("Please fill in the fields: "+errorMessage);
                       return;
                    }
                    
                    myMask.show();
                    form.submit({
                        clientValidation: true,
                        url: '../../importPictures.json',
                        params: {
                            uploadHours: time
                        },
                        success: function(form, action) {
                            if (GLOBAL.userId != 1) {
                                locationComboImp.setDisabled(true);
                            }
                            myMask.hide();
                            var result = action.result;
                            if(result){
                                if(result.success){
                                    Ext.Msg.alert('Success', action.result.msg);
                                    form.reset();
                                }else{
                                    Ext.Msg.alert('Error', action.result.msg);
                                }
                            }
                        }
                    });
                }
            }
        ]
    });  
    var exportPanel = new Ext.form.FormPanel({
        title: 'Export all pictures from database',
        standardSubmit: true,
        url: '../../exportPictures.json',
        layout: 'table',
        standardSubmit: true,
        layoutConfig: {
            columns: 2,
            tableAttrs: {
                style: {width: '600px'}
            }
        },
        items: [
            {
                xtype: 'label',
                cls: 'p_labels bold',
                text: 'Location:',
                cellCls: 'labelCell'
            },
            locationComboExp,
            {
                xtype: 'label',
                cls: 'p_labels bold',
                text: 'Base file name:',
                cellCls: 'labelCell'
            },
            {
                xtype: 'field',
                id: 'baseFileName',
                allowBlank: false
            },
            {
                xtype: 'label',
                cls: 'p_labels bold',
                text: 'From:',
                cellCls: 'labelCell'
            },
            {
                xtype: 'panel',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'datefield',
                        id: 'startDate',
                        editable: false,
                        allowBlank: false

                    },
                    {
                        xtype: 'timefield',
                        width: 60,
                        id: "startTime",
                        cls: "timeEl",
                        editable: false,
                        allowBlank: false,
                        minValue: '0:00 AM',
                        maxValue: '11:00 PM',
                        increment: 30,
                        shadow : false
                    }
                ]
            },
            {
                xtype: 'label',
                cls: 'p_labels bold',
                text: 'To:',
                cellCls: 'labelCell'
            },
            {
                xtype: 'panel',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'datefield',
                        id: 'endDate',
                        editable: false,
                        allowBlank: false
                    },
                    {
                        xtype: 'timefield',
                        width: 60,
                        id: "endTime",
                        cls: "timeEl",
                        editable: false,
                        allowBlank: false,
                        minValue: '0:00 AM',
                        maxValue: '11:00 PM',
                        increment: 30,
                        margin: 10
                    }
                ]
            },
            {
                xtype: 'button',
                text: 'Export',
                colspan: '2',
                handler: function() {
                    var form = exportPanel.getForm();
                    if (form.isValid()) {
                        var startDate = form.findField('startDate');
                        var startHours = form.findField('startTime');
                        var startTime = startDate.getRawValue() + " " + startHours.getValue();
                        var endDate = form.findField('endDate');
                        var endHours = form.findField('endTime');
                        var endTime = endDate.getRawValue() + " " + endHours.getValue();

                        if(form.findField('start') == null) {
                            exportPanel.add({
                                xtype: 'hidden',
                                name: 'start',
                                value: startTime
                            });
                        } else {
                            form.findField('start').setValue(startTime);
                        }
                        if(form.findField('end') == null) {
                            exportPanel.add({
                                xtype: 'hidden',
                                name: 'end',
                                value: endTime
                            });
                        } else {
                            form.findField('end').setValue(endTime);
                        }
                        exportPanel.doLayout();
                        Ext.Ajax.request({
                            url: '../../getPicturesToExport.json',
                            method: 'POST',
                            params: {
                                start:startTime,
                                end: endTime,
                                locationId: form.findField('locationComboExp').value
                            },
                            callback: function(options, success, response) {
                                var size = Ext.util.JSON.decode(response.responseText).size;
                                if(size==0){
                                    Ext.Msg.alert('Alert!', 'No pictures to export for the period');
                                    return;
                                }else{
                                    form.submit();
                                }
                            }});

                    } else {
                        Ext.Msg.alert('Alert!', 'All fields are required! Please fill them in.');
                    }
                }
            }
        ]
    });

    var items
    if (GLOBAL.userId != 1) {
        items = [ importPanel ];
    } else {
        items = [ importPanel, exportPanel ]
    }

    var form = new Ext.Panel({
        title: 'Settings - Export/Import pictures',
        id: 'purch-view',
        layout: 'table',
        width: '100%',
        layoutConfig: {
            columns: 1,
            tableAttrs: {
                style: {width: '100%'}
            }

        },
        items: items
    });
    storeLocation.load();
    form.render('content');
})
