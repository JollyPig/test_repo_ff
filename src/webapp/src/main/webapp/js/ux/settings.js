Ext.onReady(function() {
    var pageLoading = true;

    var storeLocation = new Ext.data.Store({
        proxy : new Ext.data.HttpProxy({
            api : {
                read : '../../locations/getRealLocations.json'
            }
        }),

        reader : new Ext.data.JsonReader({
            root : 'locations'
        }, [
            {
                name : 'id'
            },
            {
                name : 'locationName'
            }
        ]), listeners : {
            load : function() {
                if (pageLoading) {
                    locationComboText.setValue(GLOBAL.locationId);
                    locationComboImg.setValue(GLOBAL.locationId);
                    locationComboStaff.setValue(GLOBAL.locationId);
                    pageLoading = false;
                }
            }
        }
    });

    var locationComboStaff = new Ext.form.ComboBox({
        id : 'locationComboStaff', displayField : 'locationName', valueField : 'id',
        hiddenName : 'locationIdStaff', mode : 'local', width : 112, store : storeLocation,
        forceSelection : true, triggerAction : 'all', emptyText : 'Select a location...',
        selectOnFocus : true, editable : false, allowBlank : true,shadow : false
    });

    var locationComboText = new Ext.form.ComboBox({
        id : 'locationComboText', displayField : 'locationName', valueField : 'id',
        hiddenName : 'locationIdText', mode : 'local', width : 112, store : storeLocation,
        forceSelection : true, triggerAction : 'all', emptyText : 'Select a location...',
        selectOnFocus : true, editable : false, allowBlank : true,shadow : false
    });

    locationComboText.on('select', function(box, record, index) {
        form.getForm().findField('textLogo').setValue(record.json['locationTextLogo']);
    });

    var locationComboImg = new Ext.form.ComboBox({
        id : 'locationComboImg', displayField : 'locationName', valueField : 'id',
        hiddenName : 'locationIdImg', mode : 'local', width : 112, store : storeLocation,
        forceSelection : true, triggerAction : 'all', emptyText : 'Select a location...',
        selectOnFocus : true, editable : false, allowBlank : true,shadow : false		
    });

    if (GLOBAL.userId != 1) {
        locationComboText.setDisabled(true);
        locationComboImg.setDisabled(true);
        locationComboStaff.setDisabled(true);
    }
    var speedPanel = new Ext.Panel({
        layout : 'hbox',
        items : [
            {
                xtype : 'combo',
                id : 'speed',
                editable : false,
                emptyText : '...',
                width : 50,
                allowBlank : false,
                store : [
                    [ 5, '5' ],
                    [ 10, '10' ],
                    [ 15, '15' ],
                    [ 20, '20' ]
                ],
                value : GLOBAL.speed,
                triggerAction : 'all',
                shadow : false
            },
            {
                xtype : 'label',
                cls : 'p_labels',
                text : 'pages per minute'
            }
        ]
    });


    var slideStartPanel = new Ext.Panel({
        layout : 'hbox',
        items : [
            {
                xtype : 'combo',
                editable : false,
                width : 100,
                //emptyText : '-',
                mode : 'local',
                hiddenName : 'startTime',
                allowBlank : true,
                store : [
                    [ 7 * 24 * 60 * 60, '1 week' ],
                    [     72 * 60 * 60, '72 hours' ],
                    [     48 * 60 * 60, '48 hours' ],
                    [     24 * 60 * 60, '24 hours' ],
                    [     12 * 60 * 60, '12 hours' ],
                    [      6 * 60 * 60, '6 hours' ],
                    [      4 * 60 * 60, '4 hours' ],
                    [      3 * 60 * 60, '3 hours' ],
                    [      2 * 60 * 60, '2 hours' ],
                    [      1 * 60 * 60, '1 hours' ],
                    [          30 * 60, '30 minutes' ]
                ],
                value : GLOBAL.startTime==""?7 * 24 * 60 * 60:GLOBAL.startTime,
                triggerAction : 'all',
                shadow : false
            },
            {
                xtype : 'label',
                cls : 'p_labels',
                text : 'back'
            }
        ]
    });



    var textFieldPanel = new Ext.Panel({
        layout : 'table',
        id: 'textLogoPaneL',
        layoutConfig: {
            columns: 4
        },
        items : [
            {
                xtype : 'label',
                cls : 'p_labels',
                text : 'Location:'
            },
            locationComboText,
            {
                xtype : 'field',
                id : 'textLogo',
                cls : 'timeEl',
                allowBlank : true,
                text : 'Text sample',
                value : GLOBAL.locationLogo
            },
            {
                xtype : 'button',
                text : 'Update',
                handler : function() {
                    setAllowBlank(generalFields, true);
                    setAllowBlank(textLogoFields, false);
                    if (GLOBAL.userId != 1) {
                        locationComboText.setDisabled(false);
                    }
                    form.getForm().submit(
                    {
                        clientValidation : true,
                        url : '../../settings/updateTextLogo.json',
                        success : function(form, action) {
                            if (GLOBAL.userId != 1) {
                                locationComboText.setDisabled(true);
                            }
                            setAllowBlank(generalFields, false);
                            setAllowBlank(textLogoFields, true);
                            storeLocation.reload();
                            Ext.Msg.alert('Success', action.result.msg);
                        },
                        failure : function(form, action) {
                            if (GLOBAL.userId != 1) {
                                locationComboText.setDisabled(true);
                            }
                            setAllowBlank(generalFields, false);
                            setAllowBlank(textLogoFields, true);
                            switch (action.failureType) {
                                case Ext.form.Action.CLIENT_INVALID:
                                    Ext.Msg.alert('Failure',
                                            'Form fields may not be submitted with invalid values');
                                    break;
                                case Ext.form.Action.CONNECT_FAILURE:
                                    Ext.Msg.alert('Failure', 'Server connection problems. Please, try to reload page.');
                                    break;
                                case Ext.form.Action.SERVER_INVALID:
                                    Ext.Msg.alert('Failure', action.result.msg);
                            }
                        }

                    });
                }
            }
        ]
    });

    var secondLogoPanel = new Ext.Panel({
        layout : 'table',
        id: 'secondLogoPanel',
        layoutConfig: {
            columns: 6
        },
        items : [
            {
                xtype : 'label',
                cls : 'p_labels',
                text : 'Location:'
            },
            locationComboImg,
            {
                xtype : 'label',
                cls : 'p_labels',
                text : 'Path:'
            },
            {
                xtype : 'field',
                id : 'imageLogo',
                allowBlank : true,
                inputType : 'file'
            },
            {
                xtype : 'button',
                text : 'Upload',
                handler : function() {
                    if (GLOBAL.userId != 1) {
                        locationComboImg.setDisabled(false);
                    }
                    setAllowBlank(generalFields, true);
                    setAllowBlank(imageLogoFields, false);
                    form.getForm().fileUpload = true;
                    form.getForm().submit(
                    {
                        clientValidation : true,
                        url : '../../settings/updateImageLogo.json',
                        success : function(form, action) {
                            if (GLOBAL.userId != 1) {
                                locationComboImg.setDisabled(true);
                            }
                            setAllowBlank(generalFields, false);
                            setAllowBlank(imageLogoFields, true);
                            form.fileUpload = false;
                            Ext.Msg.alert('Success', action.result.msg);

                        },
                        failure : function(form, action) {
                            if (GLOBAL.userId != 1) {
                                locationComboImg.setDisabled(true);
                            }
                            setAllowBlank(generalFields, false);
                            setAllowBlank(imageLogoFields, true);
                            form.fileUpload = false;
                            switch (action.failureType) {
                                case Ext.form.Action.CLIENT_INVALID:
                                    Ext.Msg.alert('Failure',
                                            'Form fields may not be submitted with invalid values');
                                    break;
                                case Ext.form.Action.CONNECT_FAILURE:
                                    Ext.Msg.alert('Failure', 'Server connection problems. Please, try to reload page.');
                                    break;
                                case Ext.form.Action.SERVER_INVALID:
                                    Ext.Msg.alert('Failure', action.result.msg);
                            }
                        }
                    });
                }

            },
            {
                xtype : 'button',
                cls : 'uploadButton',
                text : 'Delete',
                handler : function() {
                    if (GLOBAL.userId != 1) {
                        locationComboImg.setDisabled(false);
                    }
                    setAllowBlank(generalFields, true);
                    setAllowBlank(imageLogoFields, false);
                    form.getForm().submit(
                    {
                        clientValidation : true,
                        url : '../../settings/deleteImageLogo.json',
                        success : function(form, action) {
                            if (GLOBAL.userId != 1) {
                                locationComboImg.setDisabled(true);
                            }
                            setAllowBlank(generalFields, false);
                            setAllowBlank(imageLogoFields, true);
                            Ext.Msg.alert('Success', action.result.msg);

                        },
                        failure : function(form, action) {
                            if (GLOBAL.userId != 1) {
                                locationComboImg.setDisabled(true);
                            }
                            setAllowBlank(generalFields, false);
                            setAllowBlank(imageLogoFields, true);
                            switch (action.failureType) {
                                case Ext.form.Action.CLIENT_INVALID:
                                    Ext.Msg.alert('Failure',
                                            'Form fields may not be submitted with invalid values');
                                    break;
                                case Ext.form.Action.CONNECT_FAILURE:
                                    Ext.Msg.alert('Failure', 'Server connection problems. Please, try to reload page.');
                                    break;
                                case Ext.form.Action.SERVER_INVALID:
                                    Ext.Msg.alert('Failure', action.result.msg);
                            }
                        }
                    });
                }
            }
        ]
    });
    // ---------------------------

    var selectedRecord;

    var staff = Ext.data.Record.create([ 'staffName', 'locationId' ]);

    var storeStaff = new Ext.data.Store({
        paramNames : {
            start : 'start', limit : 'limit', locationId : 'locationId'
        },
        proxy : new Ext.data.HttpProxy({
            api : {
                read : '../../getPagingStaff.json', create : '../../createStaff.json',
                update : '../../updateStaff.json', destroy : '../../deleteStaff.json'
            }
        }),

        reader : new Ext.data.JsonReader({
            root : 'staff'
        }, [
            {
                name : 'id',
                type : 'int'
            },
            {
                name : 'staffName'
            },
            {
                name : 'locationId',
                type : 'int'
            },
            {
                name : 'locationName'
            }
        ]), writer : new Ext.data.JsonWriter({
            encode : true, writeAllFields : true
        }),
        listeners: {
            write: function() {
                storeStaff.reload();
            }
        }
    });

    var titleLabel = new Ext.form.Label({
        colspan : 2, cls : 'p_labels_bold'
    });

    var staffNameField = new Ext.form.Field({
        width : 112
    });

    var staffForm = new Ext.form.FormPanel({
        layout : 'table',
        id: 'staffPanel',
        layoutConfig : {
            columns : 2
        },
        items : [ titleLabel, {
            xtype : 'label',
            text : 'Name',
            cls: "p_labels"
        }, staffNameField, {
            xtype : 'label',
            text : 'Location',
            cls: "p_labels"
        }, locationComboStaff ], buttons : [
            {
                xtype : 'button',
                text : 'Save',
                handler : function() {
                    if (selectedRecord != -1) {
                        selectedRecord.set('staffName', staffNameField.getValue());
                        selectedRecord.set('locationId', locationComboStaff.getValue());
                        storeStaff.save();
                    } else {
                        selectedRecord = new staff({
                            staffName : staffNameField.getValue(), locationId : locationComboStaff.getValue()
                        });
                        storeStaff.insert(0, selectedRecord);

                    }
                    //storeStaff.reload();
                    staffWindow.hide();
                    selectedRecord = -1;
                }
            },
            {
                xtype : 'button',
                text : 'Cancel',
                handler : function() {
                    staffWindow.hide();
                }
            }
        ]
    });

    var staffWindow = new Ext.Window({
        layout : 'fit', width : 250, height : 200, resizable : false, draggable : false, modal : true,id:'staffWindow',
        closable : false, items : [ staffForm ]
    });

    var pagingToolBar = new Ext.PagingToolbar({
        pageSize : 10, width : 620, store : storeStaff, displayInfo : true,
        displayMsg : '{0} - {1} of {2}', emptyMsg : "No staff to display", hideBorders : true,
        listeners : {
            beforechange : function(toolbar, params) {
                params = Ext.apply(params, {
                    locationId : GLOBAL.userId != 1 ? GLOBAL.locationId : 0
                })
            }
        }
    });
    pagingToolBar.refresh.hide();

    var staffGrid = new Ext.grid.GridPanel({
        id : 'userManagementPanel',
        store : storeStaff,
        width : 420,
        height : 350,
        bbar : pagingToolBar,
        viewConfig: {
            forceFit: true,
            getRowClass: function(record, index) {
                var a = index % 2;
                if (a > 0) {
                    return 'selected-class';
                }
            }
        },
        fbar : [
            {
                text : 'Add new Staff',
                handler : function(btn, evt) {
                    titleLabel.text = 'Create new Staff';
                    selectedRecord = -1;
                    staffForm.getForm().reset();
                    locationComboStaff.setValue(GLOBAL.locationId);
                    staffWindow.show();

                }
            }
        ],
        columns : [
            {
                id : 'user',
                header : 'User',
                width : 120,
                dataIndex : 'staffName',
                menuDisabled : true
            },
            {
                id : 'location',
                header : 'Location',
                width : 100,
                dataIndex : 'locationName',
                menuDisabled : true
            },
            {
                header : 'Edit',
                width : 100,
                menuDisabled : true,
                renderer : function() {
                    return '<div class ="user-edit">Edit</div>';
                }
            },
            {
                header : 'Delete',
                width : 100,
                menuDisabled : true,
                renderer : function() {
                    return '<div class ="user-edit">Delete</div>';
                }
            }
        ]
    });

    staffGrid.on({
        'cellclick' : function(grid, rowIndex, columnIndex, e) {
            selectedRecord = grid.getStore().getAt(rowIndex);
            if (columnIndex == 2) {
                staffForm.getForm().reset();
                titleLabel.text = 'Edit Staff';
                staffWindow.show();
                staffNameField.setValue(selectedRecord.get('staffName'));
                locationComboStaff.setValue(selectedRecord.get('locationId'));

            }
            if (columnIndex == 3) {

                Ext.Msg.confirm('Confirm Delete', 'Are you sure you want to delete this staff', function(
                        btnText) {
                    if (btnText == 'yes') {
                        storeStaff.remove(selectedRecord);
                        storeStaff.save();
                        //storeStaff.reload();
                        selectedRecord = -1;
                    }
                }, this);
            }

        }, scope : this
    });

    storeStaff.load({
        params : {
            'start' : 0, 'limit' : 10, 'locationId' : GLOBAL.userId != 1 ? GLOBAL.locationId : 0
        }
    });
    // -----------------------------------------------
    var reducedCheck = new Ext.form.Checkbox(
            {
                name : 'reducedcheck',
                id : 'reducedcheck',
                checked : GLOBAL.reduced=='true'?true:false,
                disabled : false
            });

    var form = new Ext.form.FormPanel({
        title : 'Settings',
        id : 'settings-view',
        layout : 'table',
        width : '100%',
        layoutConfig : {
            columns : 2
        },
        items : [
            {
                xtype : 'label',
                cls : 'p_labels_bold',
                text : 'Setting Name'
            },
            {
                xtype : 'label',
                cls : 'p_labels_bold',
                text : 'Setting Value'
            },
            {
                xtype : 'label',
                cls : 'p_labels',
                text : 'Slide show speed'
            },
            speedPanel,
            {
                xtype : 'label',
                cls : 'p_labels',
                text : 'Number of pictures displayed during the slide show'
            },
            {
                xtype : 'combo',
                editable : false,
                width : 100,
                emptyText : '...',
                mode : 'local',
                id : 'pictureNumber',
                allowBlank : false,
                store : [
                    [ 1, 'Single' ],
                    [ 4, 'Four' ]
                ],
                value : GLOBAL.picCount,
                triggerAction : 'all',
                shadow : false

            },
            {
                xtype : 'label',
                cls : 'p_labels',
                text : 'Slideshow start time'
            },
            slideStartPanel,
            {
                xtype : 'label',
                cls : 'p_labels',
                text : 'Delay before slideshow restart'
            },
            {
                xtype : 'combo',
                editable : false,
                width : 100,
                emptyText : '...',
                mode : 'local',
                hiddenName : 'delay',
                allowBlank : false,
                store : [
                    [       0, '0' ],
                    [      15, '15 seconds' ],
                    [      30, '30 seconds' ],
                    [      45, '45 seconds' ],
                    [  1 * 60, '1 minute' ],
                    [  2 * 60, '2 minutes' ],
                    [  3 * 60, '3 minutes' ],
                    [  5 * 60, '5 minutes' ],
                    [ 10 * 60, '10 minutes' ],
                    [ 30 * 60, '30 minutes' ]
                ],
                value : GLOBAL.delay,
                triggerAction : 'all',
                shadow : false

            },
            {
                xtype : 'label',
                cls : 'p_labels',
                text : 'Reduced pictures'
            },
            reducedCheck,
            {
                xtype : 'label',
                cls : 'p_labels',
                text : 'Text Field'
            },
            textFieldPanel,
            {
                xtype : 'label',
                cls : 'p_labels',
                text : 'Second Logo'
            },
            secondLogoPanel,
            {
                xtype : 'label',
                cls : 'p_labels',
                text : 'Staff List'
            },
            staffGrid,
            {
                xtype : 'panel',
                layout : 'hbox',
                colspan : 2,
                items : [
                    {
                        xtype : 'button',
                        text : 'Save',
                        handler : function() {
                            console.log(form.getForm().getValues())
                            form.getForm().submit(
                            {
                                clientValidation : true,
                                url : '../../settings/save.json',
                                success : function(form, action) {
                                    Ext.Msg.alert('Success', action.result.msg);
                                },
                                failure : function(form, action) {
                                    switch (action.failureType) {
                                        case Ext.form.Action.CLIENT_INVALID:
                                            Ext.Msg.alert('Failure',
                                                    'Form fields may not be submitted with invalid values');
                                            break;
                                        case Ext.form.Action.CONNECT_FAILURE:
                                            Ext.Msg.alert('Failure', 'Server connection problems. Please, try to reload page.');
                                            break;
                                        case Ext.form.Action.SERVER_INVALID:
                                            Ext.Msg.alert('Failure', action.result.msg);
                                    }
                                }
                            });
                        }
                    },
                    {
                        xtype : 'button',
                        text : 'Reset',
                        handler: function(){
                            form.getForm().reset();
                        }
                    }
                ]
            }
        ]
    });
    storeLocation.load();
    form.render('content');
    var generalFields = [ form.getForm().findField('speed'),
        form.getForm().findField('pictureNumber') ];
    var textLogoFields = [ form.getForm().findField('textLogo'), locationComboText ];
    var imageLogoFields = [ locationComboImg, form.getForm().findField('imageLogo') ];
})

function setAllowBlank(items, allowBlank) {
    Ext.each(items, function(item) {
        item.allowBlank = allowBlank;
    });
}
