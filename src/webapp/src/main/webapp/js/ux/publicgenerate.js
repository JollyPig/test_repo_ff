Ext.onReady(function() {

    var storeLocation = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            api: {
                read: '../../locations/getRealLocations.json'
            }
        }),

        reader: new Ext.data.JsonReader(
        {
            root:'locations'
        },
                [
                    {
                        name: 'id'
                    },
                    {
                        name: 'locationName'
                    }
                ]),
        listeners: {
            load : function() {
                if (GLOBAL.userId == 2) {
                    locationComboImp.setValue(GLOBAL.locationId);
                }
            }
        }
    });

    var locationComboImp = new Ext.form.ComboBox({
        id: 'locationComboImp',
        displayField: 'locationName',
        valueField: 'id',
        hiddenName : 'iDlocationCombo',
        mode: 'local',
        width: 112,
        store: storeLocation,
        forceSelection: true,
        triggerAction: 'all',
        emptyText:'Select a location...',
        selectOnFocus:true,
        editable: false,
        allowBlank: false,
        shadow : false
    });

    if (GLOBAL.userId != 1) {
        locationComboImp.setDisabled(true);
    }

    var login = new Ext.form.TextField({
        name: 'login',
        id: 'login',
        width: 180,
        allowBlank:true
    });

    var password = new Ext.form.TextField({
        name: 'password',
        id: 'password',
        width: 180,
        allowBlank:true
    });
    //var expirationdate = new Ext.form.TextField({
    var expirationdate = new Ext.form.DateField({
        name: 'expirationdate',
        id: 'expirationdate',
        itemId: 'expirationdate',
        width: 120,//180,
        format: 'Y-m-d',
        allowBlank:true
    });

    var expectedDate = GLOBAL.expectedDate;

    var expDate = new Ext.form.DateField({
        fieldLabel: 'Expiration Date',
        name: 'expDate',
        id: 'expDate',
        cls: 'date-field',
        showToday: false,
        width:120,
        format: 'Y-m-d',
        value: new Date(expectedDate).format('Y-m-d')
    });
     var expDateLabel = new Ext.form.Label({
        initialConfig: {
            text: 'Expiration Date',
            cls: 'p_labels bold'
        }});


    var myMask = new Ext.LoadMask(Ext.getBody(), { msg:" Please wait..."});
    var importPanel = new Ext.form.FormPanel({
        title: 'Generate public login',
        layout: 'table',
        layoutConfig: {
            columns: 2,
            tableAttrs: {
                style: {width: '600px'}
            }
        },
        items: [
            {
                xtype: 'label',
                cls: 'p_labels bold',
                text: 'Location:',
                cellCls: 'labelCell'
            },
            locationComboImp,
            expDateLabel,
            expDate,
            {
                xtype: 'button',
                colspan: 2,
                text: "Generate",
                handler: function() {
                    if (GLOBAL.userId != 1) {
                        locationComboImp.setDisabled(false);
                    }
                    if(locationComboImp.getValue()==null || locationComboImp.getValue().length==0){
                      Ext.Msg.alert('Warning', "Please select the location");
                      return;
                    }
                    myMask.show();
                    importPanel.getForm().submit({
                        clientValidation: false,
                        url: '../../generatePublic.json',
                        /*params: {
                         uploadHours: time
                         },*/
                        success: function (result, request) {
                            var result = request.result;
                            var loginStr = result.login;
                            var passwordStr = result.password;
                            var expirationDateStr = result.expirationDate;
                            login.setValue(loginStr);
                            password.setValue(passwordStr);
                            expirationdate.setValue(new Date(expirationDateStr).format('Y-m-d'));
                            if (GLOBAL.userId != 1) {
                                locationComboImp.setDisabled(true);
                            }
                            myMask.hide();
                            Ext.Msg.alert('Success', "The public login was successfully created");
                            saveButton.setDisabled(false);
                            hiddenLogin.setValue(loginStr);
                        },
                        failure: function (result, request) {
                            myMask.hide();
                            Ext.MessageBox.alert('Failed', "The public login was not created, please contact administrator");
                        }
                    });
                }
            }
        ]
    });


    var startDate = new Ext.form.DateField({
            name:'startDate',
            id:'startDate',
            itemId: 'startDate',
            cls:'date-field',
            width:120,
            format:'Y-m-d',
            value:new Date().format('Y-m-d')
        }),
        endDate = new Ext.form.DateField({
            name:'endDate',
            id:'endDate',
            itemId: 'endDate',
            cls:'date-field',
            width:120,
            format:'Y-m-d',
            value:new Date(expectedDate).format('Y-m-d')
        }),
        hiddenLogin = new Ext.form.Hidden({
            colspan: 2,
            name: 'generatedLogin'
        }),
        saveButton = new Ext.Button({
            colspan:2,
            text:'Save',
            disabled: true,
            handler:function () {
                if(startDate.getValue() <= endDate.getValue() && (expirationdate.getValue().format('Y-m-d') >= new Date().format('Y-m-d'))) {
                    saveForm.getForm().submit({
                        url:'../../savePublicLogin.json',
                        success:function (form, action) {
                            var response = action.result.children;
                            hiddenLogin.setValue(response.login);
                            myMask.hide();
                            Ext.Msg.alert('Success', "The public login was successfully saved");
                        },
                        failure:function (form, action) {
                            myMask.hide();
                            Ext.MessageBox.alert('Failed', "The public login was not saved, please contact administrator");
                        }
                    });
                } else {
                    Ext.MessageBox.alert('Error', "Start date can't be later end date or expiration date can't be in the past tense");
                }
            }
        });



    var saveForm = new Ext.form.FormPanel({
        layout:'table',
        layoutConfig:{
            columns:2,
            tableAttrs:{
                style:{width:'600px'}
            }
        },
        items:[
            hiddenLogin,
            {
                xtype:'label',
                cls:'p_labels bold',
                text:'Login:',
                cellCls:'labelCell'
            },
            login,
            {
                xtype:'label',
                cls:'p_labels bold',
                text:'Password:',
                cellCls:'labelCell'
            },
            password,
            {
                xtype:'label',
                cls:'p_labels bold',
                text:'Expiration Date:',
                cellCls:'labelCell'
            },
            expirationdate,
            {
                xtype:'label',
                cls:'p_labels bold',
                text:'Start Date'
            },
            startDate,
            {
                xtype:'label',
                cls:'p_labels bold',
                text:'End Date'
            },
            endDate,
            saveButton
        ]
    });


    var items;
    if (GLOBAL.userId != 3) {
        items = [ importPanel, saveForm ];
    }

    var form = new Ext.Panel({
        title: 'Public login generation',
        id: 'purch-view',
        layout: 'table',
        layoutConfig: {
            columns: 2,
            tableAttrs: {
                style: {width: '100%'}
            }

        },
        items: items
    });
    storeLocation.load();
    form.render('content');
});
