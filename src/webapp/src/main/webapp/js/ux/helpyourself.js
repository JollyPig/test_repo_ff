Ext.onReady(function() {

    var numberInGroupStore = new Ext.data.ArrayStore({
        id: 0,
        fields: [
            'idE',
            'countE'
        ],
        data: [
            [0, '0'],
            [1, '1'],
            [2, '2'],
            [3, '3'],
            [4, '4'],
            [5, '5'],
            [6, '6'],
            [7, '7'],
            [8, '8'],
            [9, '9'],
            [10, '10'],
            [11, '11'],
            [12, '12'],
            [13, '13'],
            [14, '14'],
            [15, '15'],
            [16, '16'],
            [17, '17'],
            [18, '18'],
            [19, '19'],
            [20, '20']
        ]
    });

    var numberInGroupCombo = new Ext.form.ComboBox({
        id: 'numberInGroup',
        valueField: 'idE',
        displayField: 'countE',
        hiddenName : 'inumberInGroup',
        mode: 'local',
        width: 40,
        store: numberInGroupStore,
        forceSelection: true,
        triggerAction: 'all',
        selectOnFocus:true,
        editable: false,
        allowBlank: false,
        lazyRender:true,
        maxHeight: 100,
        shadow : false,
        listeners:{
            afterrender: function(combo) {
                combo.setValue(0);
            }
        }
    });

    numberInGroupCombo.on('select', function(box, record, index) {
        var data = record.get('idE');
        var start = 1;
        var startF = 1;
        var existingArray = [];
        for (startF; startF <= 20; startF++) {
            if (document.getElementById('email' + startF) != null) {
                   existingArray[startF] = document.getElementById('email' + startF).value;
            }
        }
        document.getElementById('fields').innerHTML = "";
        if (data > 0) {
            var extPanel = new Ext.Panel({
                id: "extEmailesPanel",
                width: 'auto',
                layout: 'table',
                layoutConfig: {
                    columns: 8
                }
            });
            var inpTitle = new Ext.form.Label({
                cls: 'p_labels',
                text: 'Enter your email addresses:',
                colspan: 8
            });
            extPanel.add(inpTitle);
            for (start; start <= data; start++) {
                var inpL = new Ext.form.Label({
                    cls: 'p_labels',
                    text: 'Email' + start
                });
                var inp = new Ext.form.TextField({
                    name: 'email' + start,
                    id: 'email' + start,
                    width: 180,
                    regex: /^([\w]+)(.[\w]+)*@([\w]+)(.[\w]{2,4}){1,2}$/,
                    regexText: 'Enter a correct email',
                    allowBlank:true
                });                
                if (existingArray.length > 0 && start <= existingArray.length) {
                    inp.setValue(existingArray[start]);
                }
                extPanel.add(inpL);
                extPanel.add(inp);
            }
            extPanel.render('fields');
        }
    });

    var bodyQuestionPanel = new Ext.Panel({
        id: "bodyQuestionPanel",
        width: 'auto',
        layout: 'table',
        layoutConfig: {
            columns: 1
        },
        items: [
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'Please enter any other questions:'
            },
            {
                xtype: 'textarea',
                name: 'otherQuestions',
                id: 'otherQuestions',
                width: 300,
                height: 100,
                allowBlank: true
            }
        ]
    });

    var emailPanel = new Ext.Panel({
        title: 'Help Yourself',
        width: 'auto',
        layout: 'table',
        layoutConfig: {
            columns: 1
        },
        items: [
            {
                width: 'auto',
                layout: 'table',
                layoutConfig: {
                    columns: 2
                },
                items: [
                    {
                        xtype: 'label',
                        cls: 'p_labels',
                        text: 'How many people are in your group'
                    },
                    numberInGroupCombo
                ],
                colspan: 8
            },
            {
                id: "fields",
                width: 'auto'
            },
            bodyQuestionPanel
        ]

    });
    Ext.namespace('Ext.ux');
    /**
     * Start bottom panel elements
     */
    var writer = new Ext.data.JsonWriter({
        encode: true,
        writeAllFields: true
    });

    var storeHelpSelected = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            api: {
                read :    '../../getHelpYourSelfPicturesByUser.json',
                update :    '../../getHelpYourSelfPicturesByUserUpdate.json',
                destroy : '../../getHelpYourSelfPicturesByUserDelete.json'
            }
        }),
        reader: new Ext.data.JsonReader({
            root:'pictures',
            totalProperty: 'total',
            idProperty: 'helpyourselfId'
        },
                ['id','url', 'base64code', 'name', 'cameraId','cameraName',
                    {
                        name: 'selected',
                        type: 'boolean'
                    },
                    {
                        name: 'creationDate'
                    },
                    {
                        name: 'logoMainUrl'
                    },
                    {
                        name: 'logoUrl'
                    },
                    {
                        name: 'logoText'
                    },
                    {
                        name: 'selectedToSend',
                        type: 'boolean'
                    },
                    {
                        name:"toPrintNumber"
                    },
                    {
                        name:"helpyourselfId"
                    }
                ]),
        writer: writer,
        baseParams:{
            helpselfs: GLOBAL.helpselfs
        }
    });

    Ext.ux.PageSizePlugin = function() {
        Ext.ux.PageSizePlugin.superclass.constructor.call(this, {
            store: new Ext.data.SimpleStore({
                fields: ['text', 'value'],
                data: [
                    ['5', 5],
                    ['10', 10],
                    ['20', 20],
                    ['25', 25],
                    ['30', 30],
                    ['50', 50]
                ]
            }),
            mode: 'local',
            displayField: 'text',
            valueField: 'value',
            editable: false,
            allowBlank: false,
            triggerAction: 'all',
            width: 40
        });
    };

    Ext.extend(Ext.ux.PageSizePlugin, Ext.form.ComboBox, {
        init: function(paging) {
            paging.on('render', this.onInitView, this);
            paging.addEvents('pagesizechanged');
        },

        onInitView: function(paging) {
            paging.add('Page size',
                    this,
                    '-'
                    );
            this.setValue(paging.pageSize);
            this.on('select', this.onPageSizeChanged, paging);
        },

        onPageSizeChanged: function(combo) {
            this.pageSize = parseInt(combo.getValue());
            this.fireEvent('pagesizechanged', this, this.pageSize);
            this.doLoad(0);
        },
        listWidth: 40,
        shadow: false,
        autoScroll: false

    });

    var pagingbar = new Ext.PagingToolbar({
        store: storeHelpSelected,
        pageSize: GLOBAL.pageSize,
        prependButtons: true,
        beforePageText: '',
        style:{float: 'right'},
        hideLabel: true,
        listeners: {
            pagesizechanged: function(paging, pageSize) {
                Ext.Ajax.request({
                    url: '../../getHelpYourSelfPicturesPageSizeUpdate.json',
                    method: 'GET',
                    params: {
                        id: 'id',
                        pageSize: pageSize
                    },
                    callback: function(options, success, response) {

                    }
                });
            }
        },
        plugins: [new Ext.ux.PageSizePlugin()]
    });

    pagingbar.refresh.hide();

    var numberComboStore = new Ext.data.ArrayStore({
        id: 0,
        fields: [
            'idN',
            'countN'
        ],
        data: [
            [0, '0'],
            [1, '1'],
            [2, '2'],
            [3, '3'],
            [4, '4'],
            [5, '5'],
            [6, '6'],
            [7, '7'],
            [8, '8'],
            [9, '9'],
            [10, '10'],
            [11, '11'],
            [12, '12'],
            [13, '13'],
            [14, '14'],
            [15, '15'],
            [16, '16'],
            [17, '17'],
            [18, '18'],
            [19, '19'],
            [20, '20'],
            [21, '21'],
            [22, '22'],
            [23, '23'],
            [24, '24'],
            [25, '25'],
            [26, '26'],
            [27, '27'],
            [28, '28'],
            [29, '29'],
            [30, '30']
        ]
    });


    Ext.util.Format.comboRenderer = function(combo) {
        return function(value) {
            var record = combo.findRecord(combo.valueField, value);
            return record ? record.get(combo.displayField) : '0';
        }
    }
    var numberCombo = new Ext.form.ComboBox({
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
        store: numberComboStore,
        valueField: 'idN',
        displayField: 'countN',
        width: 40,
        maxHeight: 100,
        shadow : false		
    });


    var gridColumns = [
        {
            dataIndex: 'id',
            header:'Action',
            width: 100,
            menuDisabled: true,
            renderer: function (v, p, r)
            {
                return '<div class="purchase_action">' +
                       '<img src="../../js/ux/images/custom/purchaseactions/delete.png">' +
                       '</div>';
            }
        },
        {
            dataIndex: 'base64code',
            header:'Pictures to Email',
            width: 250,
            menuDisabled: true,
            renderer: function (v, p, r)
            {

                if (r.data.selectedToSend)
                    return '<div class="purchase_pict_selected">' +
                           '<img src="../' + r.data.base64code + '"  class="purchase_watermark_box_img">' +
                           '<div class="purchase_watermark_text">' + r.data.logoText + '</div>' +
                           '<img src="..' + r.data.logoUrl + '" class="purchase_watermark_logoimg"/>' +
                           '<img src="..' + r.data.logoMainUrl + '" class="purchase_watermark_logoimg_second"/>' +
                           '</div>';
                else return '<div class="purchase_pict">' +
                            '<img src="../' + r.data.base64code + '"  class="purchase_watermark_box_img">' +
                            '<div class="purchase_watermark_text">' + r.data.logoText + '</div>' +
                            '<img src="..' + r.data.logoUrl + '" class="purchase_watermark_logoimg"/>' +
                            '<img src="..' + r.data.logoMainUrl + '" class="purchase_watermark_logoimg_second"/>' +
                            '</div>';
            }
        },
        {
            dataIndex: 'toPrintNumber',
            header:'Pictures to Print',
            width:150,
            menuDisabled: true,
            editor: numberCombo,
            renderer: function (v, p, r)
            {
                return r.data.toPrintNumber;
            }
        },
        {
            dataIndex:'creationDate',
            header:'Date',
            width:150,
            menuDisabled: true,
            renderer: function (v, p, r) {
                return v;
            }
        }
    ];

    var gridColModel = new Ext.grid.ColumnModel(gridColumns);


    var selectedImagesGrid = new Ext.grid.EditorGridPanel({
        id: 'selectedImageGrid',
        store: storeHelpSelected,
        height: 380,
        width: 950,
        cm: gridColModel
    });

    selectedImagesGrid.on({
        'cellclick' : function(grid, rowIndex, columnIndex, e) {
            var record = grid.getStore().getAt(rowIndex);
            if (columnIndex == 0)
            {
                Ext.Ajax.request({
                    url: '../../getHelpYourSelfPicturesByUserDelete.json',
                    params:{
                        'pictures': record.get("helpyourselfId"),
                        'helpselfs': GLOBAL.helpselfs
                    },
                    callback: function(options, success, response) {
                        var result = response.responseText;
                        var array = Ext.util.JSON.decode(result).helpselfs;
                        GLOBAL.helpselfs = array;
                        storeHelpSelected.baseParams.helpselfs = array;
                        storeHelpSelected.reload({
                            params:
                            {
                                'helpselfs': array
                            }
                        })
                    }
                });
            }
            if (columnIndex == 1)
            {
                record.set('selectedToSend', !record.get('selectedToSend'));
                grid.getStore().save();
            }
            if (columnIndex == 2)
            {
                grid.startEditing(rowIndex, columnIndex);
            }

        },
        scope: this
    });

    selectedImagesGrid.setAutoScroll(true);


    /**
     * Select/DeselectAll Panel
     */
    var selectBox = new Ext.Panel({
        defaultType:'label',
        items: [
            {
                text:'Select',
                cls: 'selDeselPanel_label'
            }
        ],
        listeners: {render: function(c) {
            c.body.on('click', function() {
                var countrecords = storeHelpSelected.getCount();
                if (countrecords > 0) {
                    for (var i = 0; i < countrecords; i++) {
                        var record = storeHelpSelected.getAt(i);
                        record.set('selectedToSend', true);
                    }
                    storeHelpSelected.save();
                }
            });
        },
            scope: this
        }
    });

    var deselectBox = new Ext.Panel({
        defaultType:'label',
        items: [
            {
                text:'Deselect All',
                cls: 'selDeselPanel_label'
            }
        ],
        listeners: {render: function(c) {
            c.body.on('click', function() {
                var countrecords = storeHelpSelected.getCount();
                if (countrecords > 0) {
                    for (var i = 0; i < countrecords; i++) {
                        var record = storeHelpSelected.getAt(i);
                        record.set('selectedToSend', false);
                    }
                    storeHelpSelected.save();
                }
            });
        },
            scope: this
        }
    });

    var selDeselPanel = new Ext.Panel({
        id: 'selDeselPanel',
        cls:'selDeselPanel',
        layout: 'table',
        layoutConfig: {
            columns: 3
        },
        items:[selectBox,
            {
                xtype: 'label',
                cls: 'label_deselLinks',
                text: '/'
            },deselectBox
        ]
    });

    var selectedImagesPanel = new Ext.Panel({
        id: 'selectedImage',
        width: 'auto',
        height: 500,
        title:'Selected Images',
        layout: 'table',
        layoutConfig: {
            columns: 1
        },
        items: [pagingbar,selDeselPanel, selectedImagesGrid]
    });

    /**
     *  Start button Panel
     */

    var purchaseButton = new Ext.Button({
        text: 'Purchase',
        handler: function() {
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            var errorMsg = "";
            var errorEmails = "";
            var i = 1;
            for (i; i <= 20; i++) {
                var id = "email" + i;
                var elem = document.getElementById(id);
                if (elem != null) {
                    var address = elem.value;
                    if (address != null && address != "" && !reg.test(address)) {
                        errorEmails += id + ","
                    }
                }
            }
            if (errorEmails.length > 0) {
                errorEmails = 'Invalid Email Addresses: ' + errorEmails;
                errorMsg = errorEmails.substring(0, errorEmails.length - 1);
                Ext.Msg.alert('Failure:', errorMsg);
                return;
            }

            mainPanel.getForm().submit({
                url: '../../submitHelpYourSelfForm.json',
                clientValidation:true,
                scope:this,
                params: {
                    'helpselfs': storeHelpSelected.baseParams.helpselfs
                },
                success: function(form, action) {
                    window.opener.slideshow.refreshSlides(); // todo remove for slideshow with support touch
                    Ext.Msg.alert('Success:', 'The request was successfully created', function() {
                        window.close();
                    });
                },
                failure: function(form, action) {
                    switch (action.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure:', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure:', 'Server connection problems. Please, try to reload page.');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure:', action.result.msg);
                    }

                }
            });
        }
    });

    var cancelButton = new Ext.Button({
        text: 'Cancel',
        handler: function() {
            Ext.Ajax.request({
                url: '../../cancelHelpYourselfForm.json',
                params:{
                    'helpselfs': storeHelpSelected.baseParams.helpselfs
                },
                success: function (result, request) {
                  /*  window.opener.slideshow.refreshSlides();
                    window.opener.slideshow.selections = [];
                    window.opener.slideshow.dataview.clearSelections();*/
                    // todo for slideshow with support touch
                    window.opener.selectedIds = [];
                    //window.opener.jQuery.unselectedPictures();
                    window.close();
                },
                failure: function (result, request) {
                    Ext.MessageBox.alert('Failed', result.responseText);
                }
            });
        }
    });


    var buttonPanel = new Ext.Panel({
        id: 'buttonPanel',
        width: 'auto',
        height: 50,
        layout: 'table',
        layoutConfig: {
            columns: 2
        },
        items: [purchaseButton, cancelButton]
    });

    /**
     * Start main panel
     */

    var mainPanel = new Ext.FormPanel({
        xtype: 'form',
        id: 'helpyour-view',
        layout: 'table',
        layoutConfig: {
            columns: 1
        },
        items: [emailPanel ,selectedImagesPanel,buttonPanel]
    });


    storeHelpSelected.load();
    mainPanel.render('content');

});
