Ext.onReady(function () {

    var storeLocation = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            api: {
                read: '../../locations/getRealLocations.json'
            }
        }),
        autoLoad: true,
        reader: new Ext.data.JsonReader({
            root: 'locations'
        }, [
            {
                name: 'id'
            },
            {
                name: 'locationName'
            }
        ]),
        listeners: {
            load: function () {
                if (GLOBAL.access != 1) {
                    var basicForm = form.getForm()
                    var locationField = basicForm.findField('location');
                    locationField.setValue(GLOBAL.location);
                    locationField.setDisabled(true);
                    var recordId = storeLocation.find('id', GLOBAL.location);
                    var record = storeLocation.getAt(recordId);
                    locationField.fireEvent('select', locationField, record);
                }
            }
        }
    });

    var form = new Ext.form.FormPanel({
        id: 'settings-view',
        title: "Image Metadata Settings",
        layout: 'table',
        width: '100%',
        layoutConfig: {
            columns: 2
        },
        items: [
            {
                xtype: 'label',
                class: 'p_labels',
                text: 'Location'
            },
            {
                xtype: 'combo',
                itemId: 'location',
                store: storeLocation,
                //disabled: GLOBAL.access != 1,
                mode: 'local',
                name: 'locationId',
                forceSelection: true,
                triggerAction: 'all',
                displayField: 'locationName',
                valueField: 'id',
                listeners: {
                    select: function (combo, record, index) {
                        var locationId = record.get('id');
                        form.find('itemId', 'saveButton')[0].setDisabled(false);
                        Ext.Ajax.request({
                            url: GLOBAL.contextPath + "/locations/getImageMetadata.json",
                            params: {
                                locationId: locationId
                            },
                            success: function (response) {
                                var metadata = Ext.util.JSON.decode(response.responseText);
                                form.getForm().findField('title').setValue(metadata.title);
                                form.getForm().findField('comment').setValue(metadata.comment);
                                form.getForm().findField('copyright').setValue(metadata.copyright);
                            }
                        })
                    }
                }
            },
            {
                xtype: 'label',
                class: 'p_labels',
                text: 'Subject/Title'
            },
            {
                xtype: 'textfield',
                itemId: 'title',
                name: 'title'
            },
            {
                xtype: 'label',
                class: 'p_labels',
                text: 'Comment'
            },
            {
                xtype: 'textfield',
                itemId: 'comment',
                name: 'comment'
            },
            {
                xtype: 'label',
                class: 'p_labels',
                text: 'Copyright'
            },
            {
                xtype: 'textfield',
                itemId: 'copyright',
                name: 'copyright'
            },
            {
                xtype: 'button',
                text: 'Save',
                itemId: 'saveButton',
                disabled: GLOBAL.access == 1,
                handler: function () {
                    Ext.Ajax.request({
                        url: GLOBAL.contextPath + "/locations/updateImageMetadata.json",
                        params: form.getForm().getFieldValues(),
                        success: function (response) {
                            var responseData = Ext.util.JSON.decode(response.responseText);
                            Ext.Msg.alert("Success", responseData.msg);
                        }
                    })
                }
            }
        ]
    });

    form.render('content');

});