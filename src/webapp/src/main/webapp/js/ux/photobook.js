/**
 *
 */
Ext.onReady(function() {
    Ext.namespace('Ext.ux');

    Ext.Ajax.timeout = 300000;
    var selectedPictures = [];
    var countSelected = 0;
    /**
     * Start top panel elements
     */

    Ext.override(Ext.ux.form.LovCombo, {
        beforeBlur: Ext.emptyFn
    });

    var urlParams =  Ext.urlDecode(location.search.substring(1));

    var participantStore = new Ext.data.ArrayStore({
        id: 0,
        fields: [
            'idE',
            'countE'
        ],
        data: (function(count){
            var result = [],
                count = count || 10;
            for(var i=1; i<=count; i++){
                result.push([i, ''+i]);
            }
            return result;
        })(4)
    });

    var storeStaff = new Ext.data.Store({
        autoLoad: true,
        proxy: new Ext.data.HttpProxy({
            api: {
                read: '../../getStaff.json'
            }
        }),
        baseParams: {
            locationId: GLOBAL.location
        },
        reader: new Ext.data.JsonReader({
            root:'staff'
        },[{
            name: 'id',
            type: 'int'
        },{
            name: 'staffName'
        }])
    });

    var participantCombo = new Ext.form.ComboBox({
        valueField: 'idE',
        displayField: 'countE',
//        hiddenName : 'participant',
        width: 163,
        submitValue: false,
        fieldLabel: '# of Emails',
        mode: 'local',
        store: participantStore,
        forceSelection: true,
        triggerAction: 'all',
        selectOnFocus:true,
        editable: false,
        allowBlank: false,
        lazyRender:true,
        maxHeight: 100,
        shadow : false,
        listeners:{
            afterrender: function(combo) {
                var store = combo.getStore(),
                    field = combo.valueField,
                    value,
                    index;
                if (!(GLOBAL.emailCount > 0)){
                    combo.setValue(1);
                }else{
                    combo.setValue(GLOBAL.emailCount);
                }

                value = combo.getValue();
                index = store.find(field, value);
                combo.fireEvent('select', combo, store.getAt(index), index);
            },
            'select': function(box, record, index) {
                if(!Ext.getCmp('fields').rendered){
                    Ext.getCmp('fields').on('afterrender', function(){
                        box.fireEvent('select', box, record, index);
                    })
                    return;
                }

                showMailFields(box.getValue());
            }
        }
    });

    var showMailFields = function(count){
        var panel = Ext.getCmp('fields');
        if (count > 0) {
            var el;

            var showField = function(field, mode){
                var el = field.getEl(),
                    lb = el.up('div').prev('label');
                if(!mode){
                    field.reset();
                }
                el.setVisible(mode);
                lb.setVisible(mode);
            }

            Ext.each(panel.items.items, function(item, i){
                el = item.getEl();
                if(el){
                    showField(item, i < count)
                }else{
                    item.on('afterrender', function(it){
                        showField(item, i < count)
                    })
                }
            });
        }
    }

    var staffCombo = new Ext.ux.form.LovCombo({
        displayField: 'staffName',
        valueField: 'id',
        width: 163,
        hiddenName : 'staff',
        fieldLabel: 'Staff',
        mode: 'local',
        hideOnSelect:false,
        forceSelection: true,
        store: storeStaff,
        triggerAction: 'all',
        emptyText:'Select a person...',
        selectOnFocus:true,
        editable: false,
        allowBlank: true,
        maxHeight: 100
    });

    var emailPanel = new Ext.Panel({
        width: 'auto',
        layout: 'form',
        padding: 5,
        defaults: {
            labelSeparator: ''
        },
        items: [
            participantCombo,
            {
                id: 'fields',
                layout: 'form',
                cls: 'hform-panel',
                style: {
                    width: 'auto'
                },
                anchor: '100%',
                width: 'auto',
                defaultType: 'textfield',
                defaults: {
                    hideMode: 'display',
                    labelSeparator: '',
                    labelStyle: 'width: 90px; text-align: right;',
                    allowBlank: true,
                    regex: /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/,
                    regexText: 'Please enter valid email address.'
                },
                listeners: {
                    afterlayout:function(it){
                        var el = it.getEl();
                        el.setStyle('width', 'auto');
                        el.child('.x-panel-body').setStyle('width', 'auto');
                    }
                },
                items: [{
                    name: 'email',
                    fieldLabel: 'Email 1',
                    width: 163,
                    labelStyle: '',
                    allowBlank: false
                },{
                    name: 'email',
                    fieldLabel: 'Email 2'
                },{
                    name: 'email',
                    fieldLabel: 'Email 3'
                },{
                    name: 'email',
                    fieldLabel: 'Email 4'
                }]
            },{
                xtype: 'textfield',
                name: 'customerName',
                fieldLabel: 'Customer Name',
                width: 163,
                allowBlank:true
            },
            staffCombo]
    });

    /**
     * Start bottom panel elements
     */
    var writer = new Ext.data.JsonWriter({
        encode: true,
        writeAllFields: true
    });

    var storeCatalogSelected = new Ext.data.Store({
        autoSave: false,
        baseParams: urlParams,
        proxy: new Ext.data.HttpProxy({
            api: {
                read: '../../getAllPicturesSelected.json'
            }

        }),
        reader: new Ext.data.JsonReader({
                root:'pictures',
                totalProperty: 'total',
                idProperty: 'id'
            },
            ['id','url', 'base64code', 'name', 'cameraId','cameraName',
                {
                    name: 'selected',
                    type: 'boolean'
                },
                {
                    name: 'creationDate'
                },
                {
                    name: 'logoMainUrl'
                },
                {
                    name: 'logoUrl'
                },
                {
                    name: 'pictureSize'
                },
                {
                    name: 'logoText'
                },
                {
                    name: 'selectedPurchase',
                    type: 'boolean'
                },
                {
                    name: 'addToPhotobook',
                    type: 'boolean'
                },
                {
                    name: 'rotated',
                    type: 'boolean'
                }

            ]),

        writer: writer
    });
    var total = storeCatalogSelected.getTotalCount();

    Ext.ux.PageSizePlugin = function() {
        Ext.ux.PageSizePlugin.superclass.constructor.call(this, {
            store: new Ext.data.SimpleStore({
                fields: ['text', 'value'],
                data: [
                    ['5', 5],
                    ['10', 10],
                    ['20', 20],
                    ['25', 25],
                    ['30', 30],
                    ['50', 50]
                ]
            }),
            mode: 'local',
            displayField: 'text',
            valueField: 'value',
            editable: false,
            allowBlank: false,
            triggerAction: 'all',
            width: 40
        });
    };

    Ext.extend(Ext.ux.PageSizePlugin, Ext.form.ComboBox, {
        init: function(paging) {
            paging.on('render', this.onInitView, this);
            paging.addEvents('pagesizechanged');
        },

        onInitView: function(paging) {
            paging.add('Page size',
                this,
                '-'
            );
            this.setValue(paging.pageSize);
            this.on('select', this.onPageSizeChanged, paging);
        },

        onPageSizeChanged: function(combo) {
            this.pageSize = parseInt(combo.getValue());
            this.fireEvent('pagesizechanged', this, this.pageSize);
            this.doLoad(0);
        },
        listWidth: 40,
        shadow: false,
        autoScroll: false

    });

    var pagingbar = new Ext.PagingToolbar({
        store: storeCatalogSelected,
        pageSize: GLOBAL.pageSize,
        prependButtons: true,
        beforePageText: '',
        style:{float: 'right'},
        hideLabel: true,
        listeners: {
            pagesizechanged: function(paging, pageSize) {
                Ext.Ajax.request({
                    url: '../../getPurchasePicturesPageSizeUpdate.json',
                    method: 'GET',
                    params: {
                        id: 'id',
                        pageSize: pageSize
                    },
                    callback: function(options, success, response) {
                        storeCatalogSelected.reload();
                    }
                });
            }
        },
        plugins: [new Ext.ux.PageSizePlugin()]
    });

    pagingbar.refresh.hide();

    Ext.util.Format.comboRenderer = function(combo) {
        return function(value) {
            var record = combo.findRecord(combo.valueField, value);
            return record ? record.get(combo.displayField) : '0';
        }
    }

    var gridColumns = [{
        dataIndex: 'id',
        header:'Action',
        width: 100,
        menuDisabled: true,
        renderer: function (v, p, r){
            if (r.data.selected)
                return '<div class="purchase_action">' +
                    '<img src="../../js/ux/images/custom/purchaseactions/delete.png">' +
                    '</div>';
        }
    },{
        dataIndex: 'base64code',
        header:'Pictures to Export',
        width: 250,
        menuDisabled: true,
        renderer: function (v, p, r){
            if (r.data.selected) {
                var log1 =  (r.data.logoUrl.length==0)?'<img src="" style="visibility:hidden;"/>':'<img src="..' + r.data.logoUrl+ '" class="purchase_watermark_logoimg '+r.data.rotated+' '+r.data.pictureSize+'" title = "" alt=""/>';
                var log2 =  (r.data.logoMainUrl.length==0)?'<img src="" style="visibility:hidden;"/>':'<img src="..' + r.data.logoMainUrl+ '" class="purchase_watermark_logoimg_second '+r.data.rotated+' '+r.data.pictureSize+'" title = "" alt=""/>';
                if (r.data.selectedPurchase){
                    return '<div class="purchase_pict_selected">' +
                        '<div style="background: url(\'../'+ r.data.base64code +'\') center center no-repeat; background-size: contain;" class="purchase_watermark_box_img"></div>' +
                        '<div class="purchase_watermark_text '+r.data.rotated+'">' + r.data.logoText + '</div>' +
                        log1+ log2+
                        '</div>';
                }else
                    return '<div class="purchase_pict">' +
                        '<div style="background: url(\'../'+ r.data.base64code +'\') center center no-repeat; background-size: contain;" class="purchase_watermark_box_img"></div>' +
                        '<div class="purchase_watermark_text '+r.data.rotated+'">' + r.data.logoText + '</div>' +
                        log1+log2+
                        '</div>';

            }
        }
    },{
        dataIndex: 'exportToPhotobook',
        header:'Picture to Email',
        width: 130,
        menuDisabled: true,
        renderer: function (v, p, r){
            if (r.data.selected)
                if (r.data.addToPhotobook){
                    return '<div class="purchase_export_action_selected">' +
                        '<img style="width: 110px;" src="../../js/ux/images/custom/purchaseactions/edge-imaging-logo.jpg"></div>';
                }else{
                    return '<div class="purchase_export_action">' +
                        '<img style="width: 110px;" src="../../js/ux/images/custom/purchaseactions/edge-imaging-logo.jpg"></div>';

                }
        }

    }];

    var gridColModel = new Ext.grid.ColumnModel(gridColumns);

    var selectedImagesGrid = new Ext.grid.EditorGridPanel({
        id: 'selectedImageGrid',
        store: storeCatalogSelected,
        height: 380,
        width: 950,
        cm: gridColModel
    });

    selectedImagesGrid.on({
        'cellclick' : function(grid, rowIndex, columnIndex, e) {
            var store = grid.getStore(),
                record = grid.getStore().getAt(rowIndex);
            if (columnIndex == 0){
                Ext.Ajax.request({
                    url: '../../getPicturesByUserDelete.json',
                    params:{
                        'pictures': record.get("id")
                    },
                    callback: function(options, success, response) {
                        storeCatalogSelected.reload();
                    }
                });
                /*if (record.get('selectedPurchase')){
                    total = total - 1;
                    totalLabel.setText(total);
                }*/
                total = total - 1;
                totalLabel.setText(total);
            }
            if (columnIndex == 1){
                /*if (record.get('selectedPurchase')) {
                    total = total - 1;
                } else {
                    total = total + 1;
                }
                totalLabel.setText(total);*/
                record.set('selectedPurchase', !record.get('selectedPurchase'));
            }
            if (columnIndex == 2){
                store.each(function(r, i){
                    if(record.get('id') == r.get('id')){
                        record.set('addToPhotobook', !record.get('addToPhotobook'));
                    }else{
                        r.set('addToPhotobook', false);
                    }
                });
            }
        },
        scope: this
    });

    selectedImagesGrid.setAutoScroll(true);

    /**
     * Select/DeselectAll Panel
     */
    var selectBox = new Ext.Panel({
        defaultType:'label',
        items: [
            {
                text:'Select',
                cls: 'selDeselPanel_label'
            }
        ],
        listeners: {render: function(c) {
            c.body.on('click', function() {
                var countrecords = storeCatalogSelected.getCount();
                if (countrecords > 0) {
                    for (var i = 0; i < countrecords; i++) {
                        var record = storeCatalogSelected.getAt(i);
                        /*if(!record.get('selectedPurchase')){
                            total = total + 1;
                        }*/
                        record.set('selectedPurchase', true);
                    }
                    //storeCatalogSelected.save();
                    //totalLabel.setText(total);
                }
            });
        },
            scope: this
        }
    });

    var deselectBox = new Ext.Panel({
        defaultType:'label',
        items: [
            {
                text:'Deselect All',
                cls: 'selDeselPanel_label'
            }
        ],
        listeners: {render: function(c) {
            c.body.on('click', function() {
                var countrecords = storeCatalogSelected.getCount();
                if (countrecords > 0) {
                    for (var i = 0; i < countrecords; i++) {
                        var record = storeCatalogSelected.getAt(i);
                        /*if(record.get('selectedPurchase')){
                            total = total - 1;
                        }*/
                        record.set('selectedPurchase', false);
                    }
                    //storeCatalogSelected.save();
                    //totalLabel.setText(total);
                }
            });
        },
            scope: this
        }
    });

    var selDeselPanel = new Ext.Panel({
        id: 'selDeselPanel',
        cls:'selDeselPanel',
        layout: 'table',
        layoutConfig: {
            columns: 3
        },
        items:[
            selectBox,
            {
                xtype: 'label',
                cls: 'label_deselLinks',
                text: '/'
            },
            deselectBox
        ]
    });

    var selectedImagesPanel = new Ext.Panel({
        id: 'selectedImage',
        width: 'auto',
        height: 500,
        title:'Selected Images',
        layout: 'table',
        layoutConfig: {
            columns: 1
        },
        items: [pagingbar, selDeselPanel, selectedImagesGrid]
    });

    /**
     *  Start button Panel
     */

    var myMask = new Ext.LoadMask(Ext.getBody(), { msg:" Please wait..."});

    var purchaseHandler = function() {
        var values = getFormValues();

        if(values){
            console.log(values)
            Ext.Ajax.request({
                url: '../submitNotebookPurchaseFrom.json',
                params: values,
                success: function (result, request) {
                    console.log('result', result.responseText)
                    var response = Ext.util.JSON.decode(result.responseText),
                        code;
                    if(response.error!=""){
                        Ext.Msg.alert('Failed', response.error);
                    }else{
                        Ext.Msg.alert('', 'Pictures were sent successfully', function(){
                            var lo = Ext.urlAppend(GLOBAL.contextPath + '/pictures/main/common', Ext.urlEncode({getFilter: "photo"}));
                            window.location = lo;
                        });
                    }

                },
                failure: function (result, request) {
                    Ext.Msg.alert('Failed', result.responseText);
                }
            });
        }
    }

    var getFormValues = function(){
        var values,
            records = storeCatalogSelected.getRange(),
            recordsData = [],
            exportList = [],
            photobook;

        Ext.each(records, function(record, i){
            recordsData.push(record.data);
            if(record.get('selectedPurchase')){
                exportList.push(record.get('id'));
            }
            if(record.get('addToPhotobook')){
                photobook = record.get('id');
            }
        });

        if(!photobook){
            Ext.MessageBox.alert('', "Please choose one of the Edge imaging icons.");
            return null;
        }

        if(!mainPanel.getForm().isValid()){
            Ext.MessageBox.alert('', "Please enter valid email address.");
            return null;
        }

        values = mainPanel.getForm().getValues();

        Ext.apply(values, {
//            'children': Ext.encode(recordsData),
            'export': exportList.join(','),
            'photobook': photobook
        })

        return values;
    }

    var cancelHandler = function(){
        var lo = Ext.urlAppend(GLOBAL.contextPath + '/pictures/main/common', Ext.urlEncode({getFilter: "load"}));
        window.location = lo;
    }

    var buttonPanel = new Ext.Panel({
        width: 'auto',
        height: 50,
        layout: 'table',
        layoutConfig: {
            columns: 5
        },
        items: [{
            xtype: 'button',
            text: 'Purchase',
            clientValidation: true,
            handler: purchaseHandler
        },{
            xtype: 'button',
            text: 'Cancel',
            handler: cancelHandler
        }]
    });

    var totalLabel = new Ext.form.Label({
        cls: 'p_labels_total',
        name: 'totalLabel',
        text: total
    });

    var totalPanel = new Ext.Panel({
        id: 'totalPanel',
        width: 'auto',
        height: 50,
        layout: 'table',
        layoutConfig: {
            columns: 5
        },
        items: [{
                xtype: 'label',
                cls: 'p_labels_total',
                text: 'Total number of pictures:'
            },
            totalLabel
        ]
    });

    /**
     * Start main panel
     */
    var mainPanel = new Ext.FormPanel({
        xtype: 'form',
        id: 'purch-view',
        title: 'Purchase Photo Book',
        padding: 5,
        layout: 'table',
        layoutConfig: {
            columns: 1
        },
        items: [emailPanel, totalPanel, buttonPanel, selectedImagesPanel, buttonPanel.cloneConfig()],
        listeners: {
            beforerender: function(pn) {
            }
        }
    });

    storeCatalogSelected.load({
        callback: function(store, records, options) {
            if (GLOBAL.selectedids != null && GLOBAL.selectedids != "") {
                if(GLOBAL.selectedids==" "){
                    total=0;
                }else{
                    total = GLOBAL.selectedids.split(",").length;
                }
            } else {
                total = this.getTotalCount();
            }
            totalLabel.setText(total);

            var countrecords = storeCatalogSelected.getTotalCount();
            if (countrecords > 0) {
                for (var i = 0; i < countrecords; i++) {
                    var record = storeCatalogSelected.getAt(i);
                    var temp = false;
                    if (GLOBAL.selectedids != null && GLOBAL.selectedids != "") {
                        var dd = GLOBAL.selectedids.split(",");
                        for (var j = 0; j < dd.length; j++) {
                            if (dd[j] == record.get('id')) {
                                temp = true;
                            }
                        }
                        if (!temp) {
                            record.set('selectedPurchase', false);
                        }
                    }else{
                        record.set('selectedPurchase',true);
                    }
                }
            }
            storeCatalogSelected.save();
        }
    });

    mainPanel.render('content');
});
