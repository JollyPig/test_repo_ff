/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 02.12.13
 * Time: 17:22
 * To change this template use File | Settings | File Templates.
 */

Ext.onReady(function() {

    var filter = null;
    if(GLOBAL.filter){
        try{
            filter = JSON.parse(GLOBAL.filter);
        }catch(e){
            console.error(e);
        }
    }

    /*SEARCH PANEL ELEMENTS */
    var locationStore = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            url: '../../getLocations.json'
        }),
        reader: new Ext.data.JsonReader({
                root: 'locations'
            },
            [
                {
                    name: 'id'
                },
                {
                    name: 'locationName'
                }
            ])
    });


    var locationCombo = new Ext.form.ComboBox({
        id: 'locationsCombo',
        width: 200,
        store: locationStore,
        displayField: 'locationName',
        valueField: 'id',
        hiddenName: 'id',
        cls: 'location-combo',
        typeAhead: true,
        mode: 'local',
        anchor: '100%',
        forceSelection: true,
        triggerAction: 'all',
        emptyText: 'Select a location...',
        selectOnFocus: true,
        autoSelect: true,
        hidden: isUser(),
        shadow: false
    });

    function isUser() {
        if (GLOBAL.userId != 1) {
            return true;
        }
    }

    function isPublicUser() {
        if (GLOBAL.userId == 3 || GLOBAL.userId == 4 || GLOBAL.userId == 5) {
            return true;
        }
    }

    function isPublicOnlyUser() {
        if (GLOBAL.userId == 3) {
            return true;
        }
    }

    function isPublicTagEmailUser() {
        if (GLOBAL.userId == 4 || GLOBAL.userId == 5) {
            return true;
        }
    }

    var myStartDate = GLOBAL.startDay==""?new Date(GLOBAL.startDate).format('Y-m-d'):GLOBAL.startDay;
    var myEndDate = GLOBAL.endDay==""?new Date(GLOBAL.endDate).format('Y-m-d'):GLOBAL.endDay;
    var time = GLOBAL.startTime==""?new Date(GLOBAL.startDate).format('g:i A'):GLOBAL.startTime;
    var timeEnd = GLOBAL.endTime==""?new Date(GLOBAL.endDate).format('g:i A'):GLOBAL.endTime;

    /*SEARCH PANEL ELEMENTS */

    var startdate = new Ext.form.DateField({
        fieldLabel: 'Start Date',
        name: 'startdate',
        id: 'startdate',
        cls: 'date-field',
        showToday: false,
        width: 120,
        format: 'Y-m-d',
        disabled: isPublicOnlyUser(),
        value: myStartDate
        //value: new Date(myStartDate).format('Y-m-d'),
    });

    var enddate = new Ext.form.DateField({
        fieldLabel: 'End Date',
        name: 'enddate',
        id: 'enddate',
        cls: 'date-field',
        showToday: false,
        width: 120,
        format: 'Y-m-d',
        disabled: isPublicOnlyUser(),
        value: myEndDate
    });

    var starttime = new Ext.form.TimeField({
        minValue: '0:00 AM',
        maxValue: '23:00 PM',
        id: 'starttime',
        cls: 'time-field',
        increment: 30,
        value: time,
        emptyText: time,
        width: 75,
        autoScroll: true,
        shadow: false,
        style: {
            marginLeft: isTablet() ? '5px' : '0'
        }
    });

    var endtime = new Ext.form.TimeField({
        minValue: '0:00 AM',
        maxValue: '23:00 PM',
        cls: 'time-field',
        id: 'endtime',
        value: timeEnd,
        increment: 30,
        emptyText: timeEnd,
        width: 75,
        shadow: false,
        style: {
            marginLeft: isTablet() ? '5px' : '0'
        }
    });

    var emails = new Ext.form.TextField({
        xtype: 'textfield',
        name: 'emails',
        id: 'emails',
        width: 200,
        allowBlank:true,
        value: "All Emails"
    });

    var codes = new Ext.form.TextField({
        xtype: 'textfield',
        name: 'codes',
        id: 'codes',
        width: 200,
        allowBlank:true,
        value: "All Codes"
    });

    var tags = new Ext.form.TextField ({
        xtype: 'textfield',
        name: 'tags',
        id: 'tags',
        width: 200,
        allowBlank: true,
        value: 'All Tags'
    });

    var submitHandler = function () {
        submitForm();
    };

    var buttons = {
        xtype: 'button',
        text: 'Search',
        id: 'saveBtn',
        cls: 'search-btn',
        width: 100,
        handler: submitHandler
    };

    var startlabel = new Ext.form.Label({
        initialConfig: {
            text: 'Start Date',
            cls: 'search-label'
        }

    });
    var endlabel = new Ext.form.Label({
        initialConfig: {
            text: 'End Date',
            cls: 'search-label'

        }

    });

    var locationlabel = new Ext.form.Label({
        initialConfig: {
            text: 'Location',
            hidden: isUser(),
            cls: 'search-label'
        }

    });

    var emaillabel = new Ext.form.Label({
        initialConfig: {
            text: 'Email',
            cls: 'search-label'
        }
    });

    var codelabel = new Ext.form.Label({
        initialConfig: {
            text: 'Code',
            cls: 'search-label'
        }
    });

    function getValueFromSFByFieldName(fieldName) {
        var value;
        try {
            value = searchForm.getForm().findField(fieldName).getValue();
        } catch(err) {
            console.log(err);
        }
        return value;
    }

    function isTablet() {
        return /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent);
    }

    /*SEARCH PANELs */
    var searchForm = new Ext.FormPanel({
        frame: true,
        height: isTablet() ? 90 : 70,
        hidden: isPublicTagEmailUser(),
        cls: 'search-panel',
        bodyStyle: 'padding: 10px 0px 10px 5px',
        id: 'searchPanel',
        layout: 'table',
        layoutConfig: {
            columns: isTablet() ? 6 : 14
        },
        items: isTablet() ? [
            startlabel,
            {
                xtype: 'panel',
                layout: 'hbox',
                items: [
                    startdate,
                    starttime
                ]
            },
            endlabel,
            {
                xtype: 'panel',
                layout: 'hbox',
                items: [
                    enddate,
                    endtime
                ]
            },
            locationlabel,
            locationCombo,
            emaillabel,
            emails,
            codelabel,
            codes,
            tags,
            buttons
        ] : [
            startlabel,
            startdate,
            starttime,
            endlabel,
            enddate,
            endtime,
            locationlabel,
            locationCombo,
            emaillabel,
            emails,
            codelabel,
            codes,
            tags,
            buttons
        ]
    });

    Ext.namespace('Ext.ux');
    /**
     * Start bottom panel elements
     */
    var writer = new Ext.data.JsonWriter({
        encode: true,
        writeAllFields: true
    });

    var storeSavedPurchase = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            api: {
                read :    '../../getHistoryPurchase.json',
                destroy:  '../../getHistoryPurchaseDeleted.json' ,
                update :    '../../getHistoryPurchaseUpdate.json'
            }
        }),
        reader: new Ext.data.JsonReader({
                root:'savedPurchases',
                idProperty: 'id'
            },
            ['id','packages', 'emails','staff','subject',
                {
                    name: 'startDate'
                },
                {
                    name: 'startTime'
                },
                {
                    name: 'endDate'
                },
                {
                    name: 'endTime'
                },
                {
                    name: 'isTagPurchase',
                    type: 'boolean'
                },
                'location','camera','code','tag', 'firstpicturetime','tagPurchase'

            ]),
        writer: writer,
        params: {
            'startdate': getValueFromSFByFieldName('startdate'),
            'starttime': getValueFromSFByFieldName('starttime'),
            'enddate': getValueFromSFByFieldName('enddate'),
            'endtime': getValueFromSFByFieldName('endtime'),
            'locationId': getValueFromSFByFieldName('id'),
            'emails': getValueFromSFByFieldName('emails'),
            'codes': getValueFromSFByFieldName('codes'),
            'tags': getValueFromSFByFieldName ('tags'),
            'getFilter': 'save'
        }

    });

    // combobox renderer
    var comboboxViewHandler = function(cb, id){
        var combo = Ext.get(cb);
        var input = combo.parent().prev();
        input.dom.value = combo.getValue();
    }

    document.comboboxViewHandler = comboboxViewHandler;

    function createCombobox(value){
        var res = value.split(", ");
        var combo;

        combo = '<div class="ux-combobox-view tag-div">'+
            '<input type="text" tabindex="2" readonly class="ux-combobox-input" value="' + res[0] + '"/>'+
            '<div>'+
            '<select tabindex="1000" class="ux-combobox-dropdownlist" onchange="comboboxViewHandler(this);">';
        res.forEach(function(entry) {
            combo += '<option>'+entry+'</option>';
        });
        combo += '</select></div></div>'

        return combo;
    }

    var gridColumns = [
        {
            dataIndex: 'fff',
            header:'Action',
            width: 10,
            menuDisabled: true,
            resizable: false,
            renderer: function (v, p, r)
            {
                return '<div class="request_action">' +
                    '<img src="../../js/ux/images/custom/requestactions/delete.png">' +
                    '</div>';
            }
        },
        {
            dataIndex: 'ff',
            header:'Purchase',
            width:15,
            menuDisabled: true,
            resizable: false,
            renderer: function (v, p, r)
            {

                if(r.data.tagPurchase==true){
                    return ''/*'<div class="request_action">' +
                        '<img src="../../js/ux/images/custom/requestactions/addDisabled.png">' +
                        '</div>'*/;
                }else{
                    return '<div class="request_action">' +
                        '<img src="../../js/ux/images/custom/requestactions/add.png">' +
                        '</div>';
                }
            }
        },
        {
            dataIndex:'packages',
            header:'PackageType',
            width:20,
            menuDisabled: true ,
            resizable: false
        },
        {
            dataIndex:'emails',
            header:'Email',
            width:20,
            resizable: false,
            menuDisabled: true
        } ,
        {
            dataIndex:'staff',
            header:'Staff',
            width:20,
            resizable: false,
            menuDisabled: true
        },
        {
            dataIndex:'subject',
            header:'Subject',
            width:20,
            resizable: false,
            menuDisabled: true
        },
        {
            dataIndex: 'ffff',
            header:'Catalog',
            width:15,
            menuDisabled: true,
            resizable: false,
            renderer: function (v, p, r)
            {
                return '<div class="request_action">' +
                    '<img src="../../js/ux/images/custom/requestactions/add.png">' +
                    '</div>';
            }
        },
        {
            dataIndex:'startDate',
            header:'Start Date',
            width:20,
            menuDisabled: true,
            sortable: true,
            resizable: false,
            renderer: function (v, p, r) {
                return new Date(v).format("m/d/Y");
            }
        },
        {
            dataIndex:'startTime',
            header:'Start Time',
            width:20,
            menuDisabled: true,
            sortable: true,
            resizable: false,
            renderer: function (v, p, r) {
                return new Date(v).format("g:i a");
            }
        },
        {
            dataIndex:'endDate',
            header:'End Date',
            width:20,
            menuDisabled: true,
            sortable: true,
            resizable: false,
            renderer: function (v, p, r) {
                return new Date(v).format("m/d/Y");
            }
        },
        {
            dataIndex:'endTime',
            header:'End Time',
            width:20,
            menuDisabled: true,
            sortable: true,
            resizable: false,
            renderer: function (v, p, r) {
                return new Date(v).format("g:i a");
            }
        },
        {
            dataIndex:'location',
            header:'Location',
            width:30,
            hidden: isUser(),
            resizable: false,
            menuDisabled: true
        },
        {
            dataIndex:'camera',
            header:'Camera',
            width:20,
            resizable: false,
            menuDisabled: true
        },
        {
            dataIndex:'code',
            header:'Code',
            width:20,
            resizable: false,
            menuDisabled: true
        },
        {
            dataIndex:'tag',
            header:'Tag',
            width:30,
            resizable: true,
            menuDisabled: true,
            renderer:function (value, metaData, record, rowIndex, colIndex, store) {
                if(value!=""){
                    return createCombobox(value);
                }
                else {
                    return '';
                }
            }
        },
        {
            dataIndex:'firstpicturetime',
            header:'First picture time',
            width:20,
            resizable: false,
            menuDisabled: true
        }
    ];

    var gridColModel = new Ext.grid.ColumnModel(gridColumns);

    var tmp =   new Ext.Template('<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}" tabIndex="0" {cellAttr}><div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div></td>')

    var savedPurchaseGrid = new Ext.grid.GridPanel({
        id: 'savedPurchaseGrid',
        store: storeSavedPurchase,
        height: 500,
        width: 1700,
        boxMaxWidth: 1700,
        cm: gridColModel,
        viewConfig: {

            templates:{
                cell: tmp
            },
            forceFit: true,
            getRowClass: function(record, index) {
                var a = index % 2;
                if (a > 0) {
                    return 'selected-class';
                }
            }
        }
    });

    function submitForm(){
        storeSavedPurchase.load({
            params: {
                'startdate': getValueFromSFByFieldName('startdate'),
                'starttime': getValueFromSFByFieldName('starttime'),
                'enddate': getValueFromSFByFieldName('enddate'),
                'endtime': getValueFromSFByFieldName('endtime'),
                'locationId': getValueFromSFByFieldName('id'),
                'emails': getValueFromSFByFieldName('emails'),
                'codes': getValueFromSFByFieldName('codes'),
                'tags': getValueFromSFByFieldName('tags'),
                'getFilter': 'save'
            }
        });
    }

    savedPurchaseGrid.on({
        'cellclick' : function(grid, rowIndex, columnIndex, e) {
            var record = grid.getStore().getAt(rowIndex);
            if (columnIndex == 0)
            {
                Ext.Ajax.request({
                    url: '../../deletePurchase.json',
                    method: 'POST',
                    params:{
                        'purchaseId': record.get('id')
                    },
                    success: function (result, request) {
                        Ext.MessageBox.alert('Success', "The purchase was removed");
                    },
                    failure: function (result, request) {
                        Ext.MessageBox.alert('Failed', "The purchase was NOT removed");
                    }
                });
                submitForm();
            }
            if (columnIndex == 6)
            {
                var tagApproval = record.get('tagPurchase');
                if(tagApproval==true){
                    Ext.Ajax.request({
                        url: '../../applyPurchase.json',
                        method: 'POST',
                        params:{
                            'purchaseId': record.get('id')
                        },
                        success: function (result, request) {
                            var ids = result.responseText;
                            var array = Ext.util.JSON.decode(ids);
                            var purchaseId = array.purchaseId;
                            var lo = Ext.urlAppend(GLOBAL.contextPath + '/pictures/main/common', Ext.urlEncode({tagPurchaseId: purchaseId} || {tagPurchaseId: '-1'}));
                            window.location = lo;
                        },
                        failure: function (result, request) {
                            Ext.MessageBox.alert('Failed', result.responseText);
                        }
                    });
                }else{
                    Ext.Ajax.request({
                        url: '../../applyPurchase.json',
                        method: 'POST',
                        params:{
                            'purchaseId': record.get('id')
                        },
                        success: function (result, request) {
                            var ids = result.responseText;
                            var array = Ext.util.JSON.decode(ids);
                            var purchaseId = array.purchaseId;
                            var lo = Ext.urlAppend(GLOBAL.contextPath + '/pictures/main/common', Ext.urlEncode({purchaseId: purchaseId} || {purchaseId: '-1'}));
                            window.location = lo;
                        },
                        failure: function (result, request) {
                            Ext.MessageBox.alert('Failed', result.responseText);
                        }
                    });
                }
            }
            if (columnIndex == 1)
            {
                var tagApproval = record.get('tagPurchase');
                if(tagApproval!=true){
                    Ext.Ajax.request({
                        url: '../../applyPurchase.json',
                        method: 'POST',
                        params:{
                            'purchaseId': record.get('id')
                        },
                        success: function (result, request) {
                            var ids = result.responseText;
                            var array = Ext.util.JSON.decode(ids);
                            var purchaseId = array.purchaseId;
                            var lo = Ext.urlAppend(GLOBAL.contextPath + '/pictures/main/purchase', Ext.urlEncode({gen: purchaseId} || {gen: '-1'}));
                            window.location = lo;
                        },
                        failure: function (result, request) {
                            Ext.MessageBox.alert('Failed', result.responseText);
                        }
                    });
                }
            }
            if (columnIndex == 14)
            {
              //  alert(record);
            }
        },
        scope: this
    });

    /**
     * Start main panel
     */
    var mainPanel = new Ext.Panel({
        id: 'saved-purch-view',
        title: 'Purchase History',
        layout: 'fit',
        width: 'auto',
        autoScroll: true,
        items: [savedPurchaseGrid]
    });

    storeSavedPurchase.on('beforeload', function (s) {
        s.setBaseParam('startdate', getValueFromSFByFieldName('startdate'));
        s.setBaseParam('starttime', getValueFromSFByFieldName('starttime'));
        s.setBaseParam('enddate', getValueFromSFByFieldName('enddate'));
        s.setBaseParam('endtime', getValueFromSFByFieldName('endtime'));
        s.setBaseParam('locationId', getValueFromSFByFieldName('id'));
        s.setBaseParam('emails', getValueFromSFByFieldName('emails'));
        s.setBaseParam('codes', getValueFromSFByFieldName('codes'));
        s.setBaseParam('tags', getValueFromSFByFieldName('tags'));
        s.setBaseParam('getFilter', 'save');
    });

    searchForm.render('content');

    locationStore.load({
        callback: function () {
            var form = searchForm.getForm();
            // set filter
            if(filter){
                if(filter.startdate){
                    filter.startdate = new Date(filter.startdate).format('Y-m-d')
                }
                if(filter.enddate){
                    filter.enddate = new Date(filter.enddate).format('Y-m-d')
                }
                form.setValues(filter);
                locationCombo.setValue(filter.locationId);
            }else{
                locationCombo.setValue(GLOBAL.location);
            }
            storeSavedPurchase.load();
        }
    });

    mainPanel.render('content');

});
