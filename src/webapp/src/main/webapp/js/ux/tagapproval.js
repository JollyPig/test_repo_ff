Ext.onReady(function () {
    Ext.namespace('Ext.ux');

    Ext.Ajax.timeout = 300000;

    Ext.ux.PageSizePlugin = function () {
        Ext.ux.PageSizePlugin.superclass.constructor.call(this, {
            store: new Ext.data.SimpleStore({
                fields: ['text', 'value'],
                data: [
                    ['5', 5],
                    ['10', 10],
                    ['20', 20],
                    ['25', 25],
                    ['30', 30],
                    ['50', 50]
                ]
            }),
            mode: 'local',
            displayField: 'text',
            valueField: 'value',
            editable: false,
            allowBlank: false,
            triggerAction: 'all',
            width: 40
        });
    };

    Ext.override(Ext.form.ComboBox, {
        doQuery: function(q, forceAll){
            q = Ext.isEmpty(q) ? '' : q;
            var qe = {
                query: q,
                forceAll: forceAll,
                combo: this,
                cancel:false
            };
            if(this.fireEvent('beforequery', qe)===false || qe.cancel){
                return false;
            }
            q = qe.query;
            forceAll = qe.forceAll;
            if(forceAll === true || (q.length >= this.minChars)){
                if(this.lastQuery !== q){
                    this.lastQuery = q;
                    if(this.mode == 'local'){
                        this.selectedIndex = -1;
                        if(forceAll){
                            this.store.clearFilter();
                        }else{
                            this.store.filter(this.displayField, q, this.queryAnyMatch);
                        }
                        this.onLoad();
                    }else{
                        this.store.baseParams[this.queryParam] = q;
                        this.store.load({
                            params: this.getParams(q)
                        });
                        this.expand();
                    }
                }else{
                    this.selectedIndex = -1;
                    this.onLoad();
                }
            }
        }
    });

    Ext.override(Ext.ux.form.LovCombo, {
        beforeBlur: Ext.emptyFn
    });

    Ext.override(Ext.form.BasicForm, {
        findInvalid: function() {
            var result = [], it = this.items.items, l = it.length, i, f;
            for (i = 0; i < l; i++) {
                if(!(f = it[i]).disabled && !f.hidden && f.boxReady) {
                    if(f.el.hasClass(f.invalidClass)){
                        result.push(f);
                    }
                }
            }

            return result;
        }
    });

    function isTablet() {
        return /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent);
    }

    function isUser(){
        return (GLOBAL.userId != 1);
    }

    function isPublicUser(){
        return (GLOBAL.userId == 3 || GLOBAL.userId == 4 || GLOBAL.userId == 5 || GLOBAL.userId == 6);
    }

    function isCodeUser() {
        return (GLOBAL.userId == 6);
    }

    function isPublicOnlyUser() {
        return (GLOBAL.userId == 3);
    }

    function isPublicEmailUser(){
        return (GLOBAL.userId == 4);
    }

    function isPublicTagEmailUser(){
        return (GLOBAL.userId == 4 || GLOBAL.userId == 5 || GLOBAL.userId == 6);
    }

    function isTotalShowUser(){
        return (GLOBAL.userId == 3 || GLOBAL.userId == 4 ||  GLOBAL.userId == 5);
    }

    function isNullPriceAndPublic(){
        return (GLOBAL.userId == 1 || GLOBAL.userId == 2 || GLOBAL.price == 0 || GLOBAL.price == "" || GLOBAL.userId == 6);
    }

    function isJustNullPriceAndPublic(){
        return (GLOBAL.price == 0 || GLOBAL.price == "" || GLOBAL.userId == 6);
    }

    /* get field value by name */
    function getValue(name) {
        var value,
            form = mainPanel,
            field;
        if(!form){
            console.log('Warning. There are no form on the page.');
            return;
        }
        field = form.getForm().findField(name);
        if(!field){
            console.log('Warning. There are no field with name "' + name + '" in the form.');
            return;
        }
        value = field.getValue();
        return value;
    }

    /* get field value by name */
    function setValue(name, value) {
        var value,
            form = mainPanel,
            field;
        if(!form){
            console.log('Warning. There are no form on the page.');
            return;
        }
        field = form.getForm().findField(name);
        if(!field){
            console.log('Warning. There are no field with name "' + name + '" in the form.');
            return;
        }
        value = field.setValue(value);
    }

    /* Stores */
    var tagStore = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            url: '../../getExistedTags.json'
        }),
        baseParams: {
            location: GLOBAL.location
        },
        reader: new Ext.data.JsonReader({
            root: 'tags',
            idProperty: 'id'
        },[{
            name: 'id'
        },{
            name: 'tagIdName'
        },{
            name: 'location'
        }])
    });
    var tagRefundStore = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            url: '../../getExistedTags.json'
        }),
        baseParams: {
            location: GLOBAL.location
        },
        reader: new Ext.data.JsonReader({
            root: 'tags',
            idProperty: 'id'
        },[{
            name: 'id'
        },{
            name: 'tagIdName'
        },{
            name: 'location'
        }])
    });
    var codeStore = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            url: '../../getTagPurchaseCodes.json'
        }),
        baseParams: {
            location: GLOBAL.location
        },
        reader: new Ext.data.JsonReader({
            root: 'codes',
            idProperty: 'id'
        },[{
            name: 'id'
        },{
            name: 'code'
        },{
            name: 'location'
        }])
    });
    var tagGeneralStore = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            url: '../../getTagsQuery.json'
        }),
        reader: new Ext.data.JsonReader({
            root: 'tagsQuery',
            idProperty: 'id'
        },[{
            name: 'id'
        },{
            name: 'tagIdName'
        },{
            name: 'location'
        }])
    });
    var storePackage = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            api: {
                read: '../../packages/getPackagesPurchase.json'
            }
        }),
        baseParams: {
            locationId: GLOBAL.location
        },
        reader: new Ext.data.JsonReader({
            root:'packages'
        },[{
            name: 'id'
        },{
            name: 'packageName'
        }])
    });
    var storeStaff = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            api: {
                read: '../../getStaff.json'
            }
        }),
        baseParams: {
            locationId: GLOBAL.location
        },
        reader: new Ext.data.JsonReader({
            root:'staff'
        },[{
            name: 'id',
            type: 'int'
        },{
            name: 'staffName'
        }])
    });
    var participantStaff = new Ext.data.ArrayStore({
        fields: [
            'idE',
            'countE'
        ],
        data: (function(count){
            var result = [],
                count = count || 10;
            for(var i=1; i<=count; i++){
                result.push([i, ''+i]);
            }
            return result;
        })(20)
    });


    function reloadTagStore() {
        var tacom = tagCombo.getValue(),
            codecom = codeCombo.getValue(),
            tagrefcom = getValue('tagRefund'),
            tagrefundCombo = mainPanel.getForm().findField('tagRefund'),
            values = mainPanel.getForm().getValues();

        Ext.apply(values, {
            'startdate': getValue('startDate'),
            'enddate': getValue('endDate')
        });

        tagStore.load({
            params: values,
            callback: function () {
                tagCombo.setValue(tacom);
            }
        });
        tagRefundStore.load({
            params: values,
            callback: function () {
                if(tagrefundCombo){
                    tagrefundCombo.setValue(tagrefcom);
                }
            }
        });
        codeStore.load({
            params: values,
            callback: function () {
                if(codeCombo){
                    codeCombo.setValue(codecom);
                }
            }
        });
    }

    /* PANEL ELEMENTS */

    var myMask = new Ext.LoadMask(Ext.getBody(), { msg:" Please wait..."});

    function updateTagCombo(tacom) {
        //var tacom = tagCombo.getValue();
        var newComboValue = tacom;
        if ((tacom.substring(0, 1) == "0") && tacom.length > 1) {
            newComboValue = tacom.substring(2, tacom.length);
        } else {
            if (tacom.length == 0) {
                newComboValue = "0";
            }
        }
        tagCombo.setValue(newComboValue)
    }

    var tagCombo = new Ext.form.ComboBox({
//        id: 'camerasCombo1',
        width: 200,
        store: tagStore,
        displayField: 'tagIdName',
        valueField: 'tagIdName',
        hiddenName: 'tag',
        cls: 'camera-combo',
        typeAhead: true,
        mode: 'local',
        anchor: '100%',
        forceSelection: true,
        triggerAction: 'all',
        emptyText: 'Select a tag...',
        selectOnFocus: true,
        autoScroll: true,
        editable: true,
        queryAnyMatch: true,
        allowBlank: true,
        maxHeight: 100,
        shadow: false,
        value: null
    });

    var codeCombo = new Ext.form.ComboBox({
        width: 200,
        store: codeStore,
        displayField: 'code',
        valueField: 'code',
        hiddenName: 'codeRefund',
        cls: 'camera-combo',
        typeAhead: true,
        mode: 'local',
        anchor: '100%',
        forceSelection: true,
        triggerAction: 'all',
        emptyText: 'Select a code...',
        selectOnFocus: true,
        autoScroll: true,
        editable: true,
        queryAnyMatch: true,
        allowBlank: true,
        maxHeight: 100,
        shadow: false
    });

    /**
     * Start top panel elements
     */

    var myStartDate = new Date().add(Date.HOUR, -12);
    var myEndDate = new Date().add(Date.HOUR, 12);
    var time = myStartDate;
    var timeEnd = myEndDate;


    var tagWin = new Ext.Window({
        id: 'myWin',
        height: 150,
        width: 415,
        closeAction: 'hide',
        items: [
            //windowForm
        ],
        onHide: function () {
            searchForm.setDisabled(false);
        }
    });
    tagWin.hide();

    storePackage.on('load', function(ds, records, o) {
        var group = Ext.getCmp('package-type');
        if(group && group.rendered){
            group.fireEvent('update', group);
        }
    });

    var staffCombo = new Ext.ux.form.LovCombo({
        displayField: 'staffName',
        valueField: 'id',
        hiddenName : 'staff',
        mode: 'local',
        hideOnSelect:false,
        forceSelection: true,
        width: 150,
        store: storeStaff,
        triggerAction: 'all',
        emptyText:'Select a person...',
        selectOnFocus:true,
        editable: false,
        allowBlank: true,
//        disabled: !isUser(),
//        hidden: isPublicUser(),
        maxHeight: 100,
        tabIndex :5
    });

    /*staffCombo.on('render', function(ds, records, o) {
        if (isPublicEmailUser()) {
            Ext.Ajax.request({
                url: '../../getPurchaseStaffForPublicUser.json',
                method: 'GET',
                success: function (result, request) {
                    myMask.hide();
                    var resText = Ext.util.JSON.decode(result.responseText);
                    var st = resText.staff;
                    if(st!=null && st.length!=0){
                        staffCombo.setValue(st);
                    }
                },
                failure: function (result, request) {
                    myMask.hide();
                    Ext.MessageBox.alert('Failed', 'Failed to load staff');
                }
            });
        }
    });*/

    var participantCombo = new Ext.form.ComboBox({
        id: 'participantCombo',
        valueField: 'idE',
        displayField: 'countE',
        hiddenName : 'participant',
        mode: 'local',
        width: 40,
        store: participantStaff,
        forceSelection: true,
        triggerAction: 'all',
        selectOnFocus:true,
        editable: false,
        allowBlank: false,
        lazyRender:true,
        maxHeight: 100,
//        hidden: isPublicUser(),
        shadow : false,
        tabIndex :4,
        listeners:{
            afterrender: function(combo) {
                var store = combo.getStore(),
                    field = combo.valueField,
                    value,
                    index;
                if (!(GLOBAL.emailCount > 0)){
                    combo.setValue(1);
                }else{
                    combo.setValue(GLOBAL.emailCount);
                }

                value = combo.getValue();
                index = store.find(field, value);
                combo.fireEvent('select', combo, store.getAt(index), index);
            },
            'select': function(box, record, index) {
                if(!Ext.getCmp('fields').rendered){
                    Ext.getCmp('fields').on('afterrender', function(){
                        box.fireEvent('select', box, record, index);
                    })
                    return;
                }
                var startF = 1;
                var existingArray = [];
                for (startF; startF <= 20; startF++) {
                    if (document.getElementById('email' + startF) != null) {
                        existingArray[startF] = document.getElementById('email' + startF).value;
                    }
                }
                document.getElementById('fields').innerHTML = "";
                var data = box.getValue();
                var start = 1;

                if (data > 0) {
                    var extPanel = new Ext.Panel({
                        width: 'auto',
                        layout: 'table',
                        layoutConfig: {
                            columns: 16
                        }
                    });

                    var array = new Array();
                    if (GLOBAL.emails.length > 0) {
                        var emails = GLOBAL.emails.substring(1, GLOBAL.emails.length - 1);
                        array = emails.split(", ");
                    }
                    for (start; start <= data; start++) {
                        var startLbl = start;
                        if (start <= 9) {
                            startLbl = '0' + startLbl;
                        }
                        var inpL = new Ext.form.Label({
                            cls: 'p_labels',
                            text: 'Email' + startLbl
                        });
                        var inp = new Ext.form.TextField({
                            name: 'email' + start,
                            id: 'email' + start,
                            width: 180,
                            allowBlank: true,
                            regex: /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/,
                            regexText: 'Email address is not valid',
                            tabIndex :indexx++
                        });

                        if (array.length > 0 && start <= array.length) {
                            inp.setValue(array[start]);
                        }
                        if (existingArray.length > 0 && start <= existingArray.length) {
                            inp.setValue(existingArray[start]);
                        } else{
                            inp.setValue("");
                        }
                        extPanel.add(inpL);
                        extPanel.add({
                            xtype : 'box',
                            width : 13
                        });
                        extPanel.add(inp);
                        extPanel.add({
                            xtype : 'box',
                            width : 20
                        });
                    }

                    extPanel.render('fields');
                }
            }
        }
    });

    var indexx = 6;

    var emailPanel = new Ext.Panel({
        itemId: 'email-panel',
        width: 'auto',
        cls: 'search-panel',
        layout: 'table',
        padding: '0 0 0 9',
        layoutConfig: {
            columns: 1
        },
        items: [
            {
                xtype: 'panel',
                id: "fields",
                width: 'auto'
            },
            {
                xtype: 'panel',
                cls: 'mail-fields',
                width: 'auto',
                layout: 'table',
                layoutConfig: {
                    columns: 4
                },
                defaults: {
                    style: 'margin-bottom: 4px;'
                },
                items: [
                    {
                        xtype: 'label',
                        cls: 'p_labels',
                        text: 'Subject',
                        hidden: isPublicUser()
                    },
                    {
                        xtype: 'textfield',
                        name: 'subject',
                        width: 300,
                        allowBlank:true,
                        hidden: isPublicUser(),
                        tabIndex :55
                    },
                    {
                        xtype: 'label',
                        cls: 'p_labels',
                        text: 'Stringed Filename',
                        hidden: isPublicUser()
                    },
                    {
                        xtype: 'textfield',
                        name: 'strfilename',
                        width: 235,
                        allowBlank:true,
                        hidden: isPublicUser(),
                        tabIndex :56,
                        value: (GLOBAL.fileName ? GLOBAL.fileName : "")
                    },
                    {
                        xtype: 'label',
                        cls: 'p_labels',
                        text: 'Email Body',
                        style:{verticalAlign: 'top'},
                        hidden: isPublicUser()
                    },
                    {
                        xtype: 'textarea',
                        name: 'emailbody',
                        width: 300,
                        height: 100,
                        allowBlank:true,
                        hidden: isPublicUser(),
                        tabIndex :57
                    }
                ]
            }
        ]

    });

    /**
     * Handlers
     */

    function completeHandler(){
        var form = mainPanel.getForm(),
            values = form.getValues(),
            errors,
            message,
            tags = [];
        console.log('Complete', values);

        if(!form.isValid()){
            errors = form.findInvalid();
            message = "";
            message += 'Define the ';
            message += errors[0].previousSibling('label').text;
            /*Ext.each(errors, function(field){
                message += '<br>' + field.activeError + ': ' + field.previousSibling('label').text + '';
            })*/
            Ext.Msg.alert('Failure:', message);
            return;
        }

        Ext.each(Ext.getCmp('tagCheckbox').getEl().query('.x-form-checkbox'), function(item){
            var cb = Ext.getCmp(Ext.get(item).getAttribute('id')),
                lb;
            lb = cb.boxLabel;
            if(cb.getValue()){
                tags.push(lb);
            }
        });

        if(tags.length == 0){
            Ext.Msg.alert('Failure:', 'Define the tags');
            return;
        }

        Ext.apply(values, {tagsToComplete: JSON.stringify(tags)});

        var id = "email1",
            el, mail,
            mailRegex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (isPublicUser()) {
            el = Ext.getCmp(id);
            if (el) {
                mail = el.getValue();
                if (!mail) {
                    Ext.Msg.alert('Failure:', 'Please enter email Address');
                    return;
                }
            }
        }
        errors = [];
        for (var i=1; i <= 20; i++) {
            id = "email" + i;
            el = Ext.getCmp(id);
            if (el) {
                mail = el.getValue();
                if (mail && !mailRegex.test(mail)) {
                    errors.push(id);
                }
            }
        }

        if (errors.length > 0) {
            Ext.Msg.alert('Failure:', 'Invalid Email Addresses: ' + errors.join(', '));
            return;
        }

        var emails = [];
        for (var j = 1; j <= participantCombo.getValue(); j++) {
            id = 'email'+j;
            el = Ext.getCmp(id);
            if(el){
                if(el.getValue()){
                    emails.push( el.getValue() );
                }
            }
        }

        if(emails.length > 0){
            /*Ext.Msg.alert('Failure:', 'Please enter email Address');
            return;*/
            if(!values['subject']){
                myMask.hide();
                Ext.Msg.alert('Failure:', 'Define the Subject');
                return;
            }
            if(!values['strfilename']){
                myMask.hide();
                Ext.Msg.alert('Failure:', 'Define the File Name');
                return;
            }
            if(!values['emailbody']){
                myMask.hide();
                Ext.Msg.alert('Failure:', 'Define the Body');
                return;
            }
        }/*else{
            myMask.hide();
            Ext.Msg.alert('Failure:', 'Define the Email(s)');
            return;
        }*/

        Ext.apply(values, {emails: JSON.stringify(emails)});

        Ext.Ajax.request({
            url: '../../completeTag.json',
            params: values,
            success: function (result, request) {
                console.log('result', result.responseText)
                var response = Ext.util.JSON.decode(result.responseText),
                    code;
                code = response.code;
                code = '</br><div style="position: absolute;left: 60px; font-size: 18pt; font-weight: bold;">'+ code + '</div>';

                Ext.Msg.alert('Code:', 'Code was successfully generated' + (GLOBAL.giveaway ? '' : (':'+code)), function(){
                    cleanEverything();
                    // mainPanel.reload();

                  //  window.location.reload(true);
                });

            },
            failure: function (result, request) {
                Ext.Msg.alert('Failed', result.responseText);
            }
        });
    }

    function cleanEverything(){
        total = 0;
        var store = participantCombo.getStore(),
            field = participantCombo.valueField,
            value,
            index;

        document.getElementById('fields').innerHTML = "";
        participantCombo.setValue(1);

        value = participantCombo.getValue();
        index = store.find(field, value);
        participantCombo.fireEvent('select', participantCombo, store.getAt(index), index);
        document.getElementById('totalLabel').textContent = total;

        if (!GLOBAL.staff || GLOBAL.staff == "") {
            staffCombo.clearValue();
        }

        setValue('startDate', new Date(myStartDate).format('Y-m-d'));
        setValue('startTime', new Date(time).format('g:i A'));
        setValue('endDate', new Date(myEndDate).format('Y-m-d'));
        setValue('endTime', new Date(timeEnd).format('g:i A'));

        mainPanel.getForm().findField('subject').setValue(GLOBAL.subject);

        var body = Ext.getCmp('tagCheckbox').getEl().child('.x-panel-body');
        Ext.each(body.query('.x-form-check-wrap'), function(item){
            Ext.get(item).remove();
        })

    }

    function refundTagHandler(btn){
        var field = mainPanel.getForm().findField('tagRefund'),
            value = getValue('tagRefund');
        console.log('Tag Refund', value)
        if(!value){
            Ext.Msg.alert('Failure', 'Define the tag(s)');
            return;
        }

        Ext.Ajax.request({
            url: '../../refundTag.json',
            params: {
                tag: value
            },
            success: function (result, request) {
                var response = Ext.util.JSON.decode(result.responseText);
                if(response.success){
                    Ext.MessageBox.alert('Notice', 'The tag(s) <b>' + value + '</b> was refunded');
                    field.reset();
                }else{
                    Ext.MessageBox.alert('Error', response.error);
                }

            },
            failure: function (result, request) {
                Ext.MessageBox.alert('Failed', result.responseText);
            }
        });
    }

    function refundCodeHandler(btn){
        var field = mainPanel.getForm().findField('codeRefund'),
            value = getValue('codeRefund');
        console.log('Code Refund', value)
        if(!value){
            Ext.Msg.alert('Failure', 'Define the code(s)');
            return;
        }

        Ext.Ajax.request({
            url: '../../refundCode.json',
            params: {
                code: value
            },
            success: function (result, request) {
                var response = Ext.util.JSON.decode(result.responseText);
                if(response.success){
                    Ext.MessageBox.alert('Notice', 'The code(s) <b>' + value + '</b> was refunded');
                    field.reset();
                }else{
                    Ext.MessageBox.alert('Error', response.error);
                }
            },
            failure: function (result, request) {
                Ext.MessageBox.alert('Failed', result.responseText);
            }
        });
    }

    var tagRefundPanel = new Ext.Panel({
        title: 'Tag Refund',
        width: 'auto',
//        height: 70,
        layout: 'table',
        layoutConfig: {
            columns: 5
        },
        items: [{
            xtype: 'label',
            text: 'Tag ID',
            cls: 'p_labels_purch_menu'
        },{
            xtype: 'box',
            width: 21
        },
            new Ext.form.ComboBox(
                tagCombo.cloneConfig({
                    hiddenName: 'tagRefund',
                    allowBlank: true,
                    store: tagRefundStore
                })
            )
        ,{
            xtype: 'button',
            text: 'Refund',
            clientValidation: true,
            handler: refundTagHandler
        }]
    });

    var codeRefundPanel = new Ext.Panel({
        title: 'Code Refund',
        width: 'auto',
//        height: 50,
        layout: 'table',
        layoutConfig: {
            columns: 5
        },
        items: [{
            xtype: 'label',
            text: 'Code',
            cls: 'p_labels_purch_menu'
        },{
            xtype: 'box',
            width: 26
        },
            codeCombo
        ,{
            xtype: 'button',
            text: 'Refund',
            clientValidation: true,
            handler: refundCodeHandler
        }]
    });

    var addTagHandler = function(){
        var value = tagCombo.getValue(),
            panel = Ext.getCmp('tagCheckbox'),
            hf,hv,contains,
            cb,
            body;

        if(!value){
            return;
        }

        body = panel.getEl().child('.x-panel-body');

        Ext.each(body.query('.x-form-checkbox'), function(item){
            var cb = Ext.getCmp(Ext.get(item).getAttribute('id')),
                lb;
            lb = cb.boxLabel;
            if(value == lb){
                contains = true;
            }
        })
        if(contains){
            return;
        }

        tagCombo.clearValue();
        tagCombo.lastQuery = "";
        reloadTagStore();
        cb = new Ext.form.Checkbox({
            boxLabel: value,
            name: 'tags',
            ctCls: 'p_labels',
            style: {
                verticalAlign: 'middle'
            },
            listeners: {
                check: function(checkbox, checked) {
                    if(checked){
                        total = total+1;
                    }else{
                        total= total-1;
                    }
                    var store = participantCombo.getStore(),
                        field = participantCombo.valueField,
                        value,
                        index;
                    if (!(total > 0)){
                        participantCombo.setValue(1);
                    }else{
                        participantCombo.setValue(total);
                    }

                    value = participantCombo.getValue();
                    index = store.find(field, value);
                    participantCombo.fireEvent('select', participantCombo, store.getAt(index), index);
                    document.getElementById('totalLabel').textContent =total;
                }
            }



        })
        cb.render(body, 0);

        cb.setValue(true);
    }

    tagCombo.on('specialkey', function(field, e){
        if(e.getKey() == e.TAB){
            field.fireEvent('blur', field);
            addTagHandler.call(field, e);
            field.focus(); // TODO
        }
    })

    tagCombo.on('blur', function(combo){
        var value = combo.getValue(),
            store = combo.getStore(),
            field = combo.valueField,
            index;

        index = store.find(field, value);
        if(index > -1){
            value = store.getAt(index).get(combo.displayField);
        }else{
            value = null;
        }

        if(!value){
            value = combo.lastQuery;
        }
        combo.setValue(value);
    });

    /*SEARCH PANELs */
    var searchForm = new Ext.Panel({
        //title: 'Tag Approval',
        frame: true,
//        height: 100,
//        hidden: isPublicTagEmailUser(),
        cls: 'search-panel tag-approval-complete-panel',
        id: 'tagApprovalCompletePanel',
        layout: 'table',
        layoutConfig: {
            columns: 8
        },
        items: [{
            xtype: 'panel',
            width: 330,
            itemId: 'tagField',
            layoutConfig: {
                columns: 1
            },
            layout: 'table',
            items: [{
                xtype: 'panel',
//                width: 310,
                itemId: 'tagField',
                layoutConfig: {
                    columns: 5
                },
                layout: 'table',
                items: [{
                    xtype: 'label',
                    text: 'Tag ID',
                    cls: 'p_labels'
                },{
                    xtype: 'box',
                    width: 18
                },
                    tagCombo
                ,{
                    xtype: 'button',
                    text: 'Add',
                    listeners: {
                        afterrender: function(button){
                            var cell = button.getEl().up('.x-table-layout-cell');
                            cell.setStyle('vertical-align', 'middle');
                            cell.setStyle('padding-top', '0');
                        }
                    },
                    handler: addTagHandler
                }]
            },{
                xtype: 'panel',
                autoScroll: true,
                height: 80,
                padding: 2,
                id: 'tagCheckbox',
                items: [{
                    xtype: 'hidden',
                    name: 'tagsToComplete'
                }]
            }]
        },{
            xtype: 'label',
            cls: 'p_labels_purch_menu',
//            hidden: isPublicUser(),
            text: 'Package Type'
        },{
            xtype: 'panel',
            itemId: 'package-type',
//            hidden: isPublicUser(),
            columns: 1,
            defaults: {
                xtype: 'checkbox'
            },
            items: [
            ],
            listeners: {
                'afterrender': function(panel){
                    panel.fireEvent('update', panel);
                },
                'update': function(panel){
                    if(panel.rendered){
                        storePackage.on('load', function(records){
                            panel.fireEvent('update', panel);
                        })

                        var rel = panel.getEl().first(),
                            t;
                        while(rel){
                            t = rel.next();
                            rel.remove();
                            rel = t;
                        }
                        var cb,
                            hf = new Ext.form.Hidden({
                                name: 'package',
                                allowBlank: false
                            });
                        var group = [],
                            oncheck = function(el, check){
                                Ext.each(group, function(item, i){
                                    item.un('check', oncheck);
                                    if(item != el){
                                        item.reset();
                                    }else{
                                        item.setValue(true);
                                        hf.setValue(item.getName());
                                    }
                                    item.on('check', oncheck);
                                })
                            };

                        hf.render(panel.getEl());

                        storePackage.each(function(record, i){
                            if(i>5){
                                return false;
                            }
                            cb = new Ext.form.Checkbox({
                                boxLabel: record.get('packageName'),
                                name: record.get('id'),
                                ctCls: 'p_labels',
                                style: {
                                    verticalAlign: 'middle'
                                }
                            })
                            cb.on('check', oncheck)
                            cb.render(panel.getEl());
                            group.push(cb);
                        })
                        if(group[0]){
                            group[0].setValue(true);
                        }
                    }
                }
            }
        },
            {
                xtype: 'label',
                cls: 'p_labels_purch_menu',
                bodyPadding: '0 0 0 10',
                text: '# of Participants'/*,
                hidden: isPublicUser()*/
            },
            participantCombo,
            {
                xtype: 'label',
                cls: 'p_labels_purch_menu',
                text: 'Staff'/*,
                hidden: isPublicUser()*/
            },
            staffCombo,
            {
            xtype: 'button',
            text: 'Complete',
//            id: 'saveBtn',
//            cls: 'search-btn',
            width: 100,
            listeners: {
                afterrender: function(button){
                    var cell = button.getEl().up('.x-table-layout-cell');
                    cell.setStyle('vertical-align', 'top');
                    //cell.setStyle('padding', '0');
                    button.getEl().setStyle('margin-top', '5px');
                }
            },
            handler: completeHandler
        }]
    });

    var dateRangeForm = new Ext.Panel({
        id: 'dateRangePanel',
        cls: 'hform-panel',
        layout: 'form',
        anchor: '100%',
        width: 'auto',
        labelWidth: 70,
        defaults: {
            labelSeparator: '',
            labelStyle: 'padding-left: 15px; color: #666666;'
        },
        listeners: {
            afterlayout:function(it){
                var el = it.getEl();
                el.setStyle('width', 'auto');
                el.child('.x-panel-body').setStyle('width', 'auto');
                var itemEl = el.child('.x-form-item');
                while(itemEl){
                    itemEl.setStyle('padding-right', '15px');
                    itemEl = itemEl.next('.x-form-item');
                }
            }
        },
        items: [{
            xtype: 'datefield',
            fieldLabel: 'Start Date',
            name: 'startDate',
            cls: 'date-field',
            showToday: false,
            width: 120,
            format: 'Y-m-d',
            disabled: isPublicOnlyUser(),
            value: myStartDate,
            listeners: {
                select: function (startdt, newValue, oldValue) {
                    reloadTagStore();
                }
            }
        },{
            xtype: 'timefield',
            hideLabel: true,
            minValue: '0:00 AM',
            maxValue: '23:00 PM',
            name: 'startTime',
            cls: 'time-field',
            increment: 30,
            value: time,
            emptyText: time,
            width: 78,
            autoScroll: true,
            shadow: false,
            listeners: {
                select: function (startdt, newValue, oldValue) {
                    reloadTagStore()
                }
            }
        },{
            xtype: 'datefield',
            fieldLabel: 'End Date',
            name: 'endDate',
            cls: 'date-field',
            showToday: false,
            width: 120,
            format: 'Y-m-d',
            disabled: isPublicOnlyUser(),
            value: myEndDate,
            listeners: {
                select: function (startdt, newValue, oldValue) {
                    reloadTagStore()
                }
            }
        },{
            xtype: 'timefield',
            hideLabel: true,
            minValue: '0:00 AM',
            maxValue: '23:00 PM',
            cls: 'time-field',
            name: 'endTime',
            value: timeEnd,
            increment: 30,
            emptyText: timeEnd,
            width: 78,
            shadow: false,
            listeners: {
                select: function (startdt, newValue, oldValue) {
                    reloadTagStore();
                }
            }
        }]
    });

    var totalLabel = new Ext.form.Label({
        cls: 'p_labels_total',
        id: 'totalLabel',
        name: 'totalLabel',
        text: '0'
    });


    var totalPanel = new Ext.Panel({
        id: 'totalPanel',
        width: 'auto',
        height: 20,
        layout: 'table',
        layoutConfig: {
            columns: 2
        },
        items: [
            {
                xtype: 'label',
                cls: 'p_labels_total',
                text: 'Total number of tags:'
            },
            totalLabel
        ]
    });

    /**
     * Start main panel
     */
    var mainPanel = new Ext.FormPanel({
        xtype: 'form',
        id: 'tagapproval-view',
        layout: 'table',
        layoutConfig: {
            columns: 1
        },
        items: [{
            xtype: 'hidden',
            name: 'locationId',
            allowBlank: false,
            value: GLOBAL.location
        }, totalPanel, searchForm, dateRangeForm, emailPanel,
            (GLOBAL.giveaway ? tagRefundPanel : codeRefundPanel)
        ],
        listeners: {
            beforerender: function(pn) {
                Ext.Ajax.request({
                    url: '../../getEmailBody.json',
                    method: 'POST',
                    params:{
                        purchaseId: GLOBAL.purchId
                    },
                    success: function (result, request) {
                        var ids = result.responseText;
                        var array = Ext.util.JSON.decode(ids);
                        var emailbody = array.emailbody;
                        mainPanel.getForm().findField('emailbody').setValue(emailbody);
                    },
                    failure: function (result, request) {
                        Ext.MessageBox.alert('Failed', result.responseText);
                    }
                });
                if(GLOBAL.subject!=null && GLOBAL.subject!=""){
                    mainPanel.getForm().findField('subject').setValue(GLOBAL.subject);
                }
            },
            afterrender: function(){
                reloadTagStore();
            }
        }
    });

    storePackage.load();
    storeStaff.load(/*{
        callback: function () {
            if (GLOBAL.staff != null && GLOBAL.staff != "") {
                staffCombo.setValue(GLOBAL.staff);
            }
        }
    }*/);

    mainPanel.render('content');

});
