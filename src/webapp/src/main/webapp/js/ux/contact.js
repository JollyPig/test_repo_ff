Ext.onReady(function() {

    /**
     * Contact section
     */

    var firstNameTextfield = new Ext.form.TextField({
        name: 'firstName',
        id: 'firstName',
        width: 240,
        allowBlank:false
    });

    var lastNameTextfield = new Ext.form.TextField({
        name: 'lastName',
        id: 'lastName',
        width: 240,
        allowBlank:false
    });

    var emailfield = new Ext.form.TextField({
        name: 'email',
        id: 'email',
        width: 240,
        allowBlank:false
    });
    var phonefield = new Ext.form.TextField({
        name: 'phone',
        id: 'phone',
        width: 240,
        allowBlank:false
    });

    /**
     * Button Panel
     */
    var saveButton = new Ext.Button({
        text: 'Send',
        id: 'saveButtonId',
        //  disable:  isDisable(),
        style : {
            float : 'left'
        },

        handler: function() {
            var validateMessage = "";
            if (firstNameTextfield.getValue().length == 0) {
                validateMessage += " First Name,"
                //   Ext.Msg.alert('Failure:', 'Plese enter your name');
                //  return;
            }
            if (lastNameTextfield.getValue().length == 0) {
                validateMessage += " Last Name,"
            }
            if (phonefield.getValue().length == 0) {
                validateMessage += " Pnone number,"
            }

            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            var address1 = emailfield.getValue();
            var errorMsg = "";

            if (address1.length == 0 || !reg.test(address1)) {
                validateMessage += " Email address(Example: test@test.test),";
            }
            var bodyQuestion = document.getElementById("area");
            if (bodyQuestion == null || (bodyQuestion.value == null || bodyQuestion.value.length == 0 || bodyQuestion.value.length > 255 )) {
                validateMessage += " Comments and Questions(Should be not empty and less then 255 symbols).";
            }

            if (validateMessage.length > 0) {
                errorMsg = 'The following fields are mandatory and should be correct: ' + validateMessage;
                Ext.Msg.alert('Failure:', errorMsg);
                return;
            }

            contactForm.getForm().submit({
                url: '../../sendContactEmail.json',
                scope:this,
                clientValidation: false,
                success: function(f, a) {
                    Ext.Msg.alert('Success:', a.result.msg);
                },
                failure: function(f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure:', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure:', 'Server connection problems. Please, try to reload page.');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure:', a.result.msg);
                    }

                }
            });
        }
    });

    var userPanel = new Ext.Panel({
        id: 'userPanel',
        width: 'auto',
        height: 'auto',
        layout: 'table',
        layoutConfig: {
            tableAttrs : {
                align : 'left',
                width: '550'
            },
            columns:2
        },
        items: [
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'First Name'
            },
            firstNameTextfield,
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'Last Name'
            },
            lastNameTextfield,
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'Your Email'

            },
            emailfield,
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'Phone number'

            },
            phonefield,
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'Questions and Comments'
            },
            {
                xtype: 'textarea',
                name: 'area',
                id: 'area',
                fieldLabel: 'Enter some text',
                width: 300,
                height: 100,
                anchor: '90%',
                multiline: true
            },
            {
                xtype : 'box',
                width : 20
            },
            saveButton

        ]
    });

    /**
     * Info Panel
     */
    var infoPanel = new Ext.Panel({
        id: 'infoPanel',
        width: 'auto',
        height: 100,
        layout: 'table',
        layoutConfig: {
            columns: 1
        },
        items: [
            {
                xtype: 'label',
                name: 'text1',
                id: 'text1',
                cls: 'p_labels',
                text: 'To contact Fotaflo please fill out the following form and we will get back to you at our earliest convenience.'
            },
            {
                xtype: 'label',
                cls: 'p_labels',
                name: 'text2',
                id: 'text2',
                text: 'Please be very specific with your questions or comments and include all of your contact info, including your phone number.'
            }
        ]
    });





    /**
     * Main Panel
     */
    var contactForm = new Ext.form.FormPanel({
        title: 'Contact Information',
        id: 'contact-view',
        layout: 'table',
        width: 600,
        layoutConfig: {
            columns: 1
        },
        items: [
            infoPanel,
            userPanel
        ]
    });



    contactForm.render('content');


});