/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 02.12.13
 * Time: 17:22
 * To change this template use File | Settings | File Templates.
 */

Ext.onReady(function() {
    var storeLocation = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            api: {
                read: '../../locations/getActualLocations.json'
            }
        }),
        reader: new Ext.data.JsonReader({
                root:'locations'
            },[{ name: 'id'},{name: 'locationName'}])
    });

    var locationCombo = new Ext.form.ComboBox({
        id: 'locationCombo',
        displayField: 'locationName',
        valueField: 'id',
        hiddenName : 'iDlocationCombo',
        mode: 'local',
        width: 100,
        store: storeLocation,
        forceSelection: true,
        triggerAction: 'all',
        emptyText:'Select a location...',
        selectOnFocus:true,
        editable: false,
        allowBlank: false,
        hidden: isUser(),
        shadow : false
    });

    function isUser()
    {
        if (GLOBAL.userId != 1) {
            return true;
        }
    }

    locationCombo.on('select', function(box, record, index) {
        var data = record.get('id');
        storeSavedPurchase.load(
            {
                params:
                {
                    'locationId': data
                }
            });
    });

    Ext.namespace('Ext.ux');
    /**
     * Start bottom panel elements
     */
    var writer = new Ext.data.JsonWriter({
        encode: true,
        writeAllFields: true
    });

    var storeSavedPurchase = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            api: {
                read :    '../../getSavedPurchase.json',
                destroy:  '../../getSavedPurchaseDeleted.json' ,
                update :    '../../getSavedPurchaseUpdate.json'
            }
        }),
        reader: new Ext.data.JsonReader({
                root:'savedPurchases',
                idProperty: 'id'
            },
            ['id','packages', 'emails','staff','subject',
                {
                   name: 'startDate'
                },
                {
                    name: 'startTime'
                },
                {
                    name: 'endDate'
                },
                {
                    name: 'endTime'
                },
                'location','camera','code','tag', 'firstpicturetime'

            ]),
        writer: writer
    });

    var gridColumns = [
        {
            dataIndex: 'fff',
            header:'Action',
            width: 10,
            menuDisabled: true,
            resizable: false,
            renderer: function (v, p, r)
            {
                return '<div class="request_action">' +
                    '<img src="../../js/ux/images/custom/requestactions/delete.png">' +
                    '</div>';
            }
        },
        {
            dataIndex: 'ff',
            header:'Purchase',
            width:15,
            menuDisabled: true,
            resizable: false,
            renderer: function (v, p, r)
            {
                return '<div class="request_action">' +
                    '<img src="../../js/ux/images/custom/requestactions/add.png">' +
                    '</div>';
            }
        },
        {
            dataIndex:'packages',
            header:'PackageType',
            width:20,
            menuDisabled: true ,
            resizable: false
        },
        {
            dataIndex:'emails',
            header:'Email',
            width:20,
            resizable: false,
            menuDisabled: true
        } ,
        {
            dataIndex:'staff',
            header:'Staff',
            width:20,
            resizable: false,
            menuDisabled: true
        },
        {
            dataIndex:'subject',
            header:'Subject',
            width:20,
            resizable: false,
            menuDisabled: true
        },
        {
            dataIndex: 'ffff',
            header:'Catalog',
            width:15,
            menuDisabled: true,
            resizable: false,
            renderer: function (v, p, r)
            {
                return '<div class="request_action">' +
                    '<img src="../../js/ux/images/custom/requestactions/add.png">' +
                    '</div>';
            }
        },
        {
            dataIndex:'startDate',
            header:'Start Date',
            width:20,
            menuDisabled: true,
            sortable: true,
            resizable: false,
            renderer: function (v, p, r) {
                return new Date(v).format("m/d/Y");
            }
        },
        {
            dataIndex:'startTime',
            header:'Start Time',
            width:20,
            menuDisabled: true,
            sortable: true,
            resizable: false,
            renderer: function (v, p, r) {
                return new Date(v).format("g:i a");
            }
        },
        {
            dataIndex:'endDate',
            header:'End Date',
            width:20,
            menuDisabled: true,
            sortable: true,
            resizable: false,
            renderer: function (v, p, r) {
                return new Date(v).format("m/d/Y");
            }
        },
        {
            dataIndex:'endTime',
            header:'End Time',
            width:20,
            menuDisabled: true,
            sortable: true,
            resizable: false,
            renderer: function (v, p, r) {
                return new Date(v).format("g:i a");
            }
        },
        {
            dataIndex:'location',
            header:'Location',
            width:30,
            hidden: isUser(),
            resizable: false,
            menuDisabled: true
        },
        {
            dataIndex:'camera',
            header:'Camera',
            width:30,
            resizable: false,
            menuDisabled: true
        },
        {
            dataIndex:'code',
            header:'Code',
            width:30,
            resizable: false,
            menuDisabled: true
        },
        {
            dataIndex:'tag',
            header:'Tag',
            width:30,
            resizable: false,
            menuDisabled: true
        },
        {
            dataIndex:'firstpicturetime',
            header:'First picture time',
            width:30,
            resizable: false,
            menuDisabled: true
        }
    ];

    var gridColModel = new Ext.grid.ColumnModel(gridColumns);


    var tmp =   new Ext.Template('<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}" tabIndex="0" {cellAttr}><div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div></td>')

    var savedPurchaseGrid = new Ext.grid.GridPanel({
        id: 'savedPurchaseGrid',
        store: storeSavedPurchase,
        height: 500,
        width: 1800,
        cm: gridColModel,
        viewConfig: {
            templates:{
                cell: tmp
            },
            forceFit: true,
            getRowClass: function(record, index) {
                var a = index % 2;
                if (a > 0) {
                    return 'selected-class';
                }
            }
        }

    });

    savedPurchaseGrid.on({
        'cellclick' : function(grid, rowIndex, columnIndex, e) {
            var record = grid.getStore().getAt(rowIndex);
            if (columnIndex == 0)
            {
                Ext.Ajax.request({
                    url: '../../deletePurchase.json',
                    method: 'POST',
                    params:{
                        'purchaseId': record.get('id')
                    },
                    success: function (result, request) {
                        Ext.MessageBox.alert('Success', "The purchase was removed");
                    },
                    failure: function (result, request) {
                        Ext.MessageBox.alert('Failed', "The purchase was NOT removed");
                    }
                });
                submitForm();
            }
            if (columnIndex == 6)
            {
                Ext.Ajax.request({
                    url: '../../applyPurchase.json',
                    method: 'POST',
                    params:{
                        'purchaseId': record.get('id')
                    },
                    success: function (result, request) {
                        var ids = result.responseText;
                        var array = Ext.util.JSON.decode(ids);
                        var purchaseId = array.purchaseId;
                        var lo = Ext.urlAppend(GLOBAL.contextPath + '/pictures/main/common', Ext.urlEncode({purchaseId: purchaseId} || {purchaseId: '-1'}));
                        window.location = lo;
                    },
                    failure: function (result, request) {
                        Ext.MessageBox.alert('Failed', result.responseText);
                    }
                });
            }
            if (columnIndex == 1)
            {
                Ext.Ajax.request({
                    url: '../../applyPurchase.json',
                    method: 'POST',
                    params:{
                        'purchaseId': record.get('id')
                    },
                    success: function (result, request) {
                        var ids = result.responseText;
                        var array = Ext.util.JSON.decode(ids);
                        var purchaseId = array.purchaseId;
                        var lo = Ext.urlAppend(GLOBAL.contextPath + '/pictures/main/purchase', Ext.urlEncode({gen: purchaseId} || {gen: '-1'}));
                        window.location = lo;
                    },
                    failure: function (result, request) {
                        Ext.MessageBox.alert('Failed', result.responseText);
                    }
                });
            }

        },
        scope: this
    });

    var locationPanel = new Ext.Panel({
        layout: 'table',
        layoutConfig: {
            columns: 2
        },
        items: [
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'Location'
            },
            locationCombo
        ],
        hidden: isUser()
    });

    function submitForm(){
        storeSavedPurchase.load();
    }

    storeSavedPurchase.on('beforeload', function (s) {
        s.setBaseParam('locationId', locationCombo.getValue());
        s.setBaseParam('getFilter', 'save');
    });

    /**
     * Start main panel
     */
    var mainPanel = new Ext.Panel({
        id: 'saved-purch-view',
        title: 'Saved Purchases',
        layout: 'table',
        layoutConfig: {
            columns: 1
        },
        items: [locationPanel,savedPurchaseGrid]
    });

    storeLocation.load({
        callback: function () {
            if(GLOBAL.location!=0){
                locationCombo.setValue(GLOBAL.location);
            }
            storeSavedPurchase.load();
        }
    });

    mainPanel.render('content');

});
