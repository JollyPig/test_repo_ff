Ext.onReady(function() {
    var pageLoading = true;

    var storeLocation = new Ext.data.Store({
        proxy : new Ext.data.HttpProxy({
            api : {
                read : '../../locations/getRealLocations.json'
            }
        }),

        reader : new Ext.data.JsonReader({
            root : 'locations'
        }, [
            {
                name : 'id'
            },
            {
                name : 'locationName'
            }
        ])
    });

    var locationCombo = new Ext.form.ComboBox({
        displayField : 'locationName',
        valueField : 'id',
        hiddenName : 'locationIdImg',
        mode : 'local',
        width : 180,
        store : storeLocation,
        forceSelection : true,
        triggerAction : 'all',
        emptyText : 'Select a location...',
        selectOnFocus : true,
        editable : false,
        allowBlank : true,
        shadow : false
    });

    locationCombo.on('select', function(box, record, index) {
        storeAttachments.load({
            params : {
                'start' : 0, 'limit' : 10, 'locationId' : box.getValue()
            }
        });
    });

    if (GLOBAL.userId != 1) {
        locationCombo.setDisabled(true);
    }

    var uploadForm = new Ext.Panel({
        layout : 'table',
        id: 'textLogoPaneL',
        layoutConfig: {
            columns: 5
        },
        items : [
            {
                xtype : 'label',
                cls : 'p_labels',
                text : 'Location:'
            },
            locationCombo,
            {
                xtype : 'label',
                cls : 'p_labels',
                text : 'Path:'
            },
            {
                xtype : 'field',
                id : 'imageLogo',
                allowBlank : true,
                inputType : 'file'
            },
            {
                xtype : 'button',
                text : 'Upload',
                handler : function() {
                    if (GLOBAL.userId != 1) {
                        locationCombo.setDisabled(false);
                    }
                    var value = form.getForm().findField('imageLogo').getValue();
                    if(!value){
                        Ext.Msg.alert('Warning', 'Choose image!');
                        return;
                    }
                    setAllowBlank(imageLogoFields, false);
                    form.getForm().fileUpload = true;
                    form.getForm().submit({
                        clientValidation : true,
                        url : '../../addLocationImage.json',
                        success : function(form, action) {
                            if (GLOBAL.userId != 1) {
                                locationCombo.setDisabled(true);
                            }
                            setAllowBlank(imageLogoFields, true);
                            form.fileUpload = false;
                            Ext.Msg.alert('Success', action.result.msg);
                            storeAttachments.load({
                                params : {
                                    'start' : 0, 'limit' : 10, 'locationId' : locationCombo.getValue()
                                }
                            });
                            form.findField('imageLogo').reset();
                        },
                        failure : function(form, action) {
                            if (GLOBAL.userId != 1) {
                                locationCombo.setDisabled(true);
                            }
                            setAllowBlank(imageLogoFields, true);
                            form.fileUpload = false;
                            switch (action.failureType) {
                                case Ext.form.Action.CLIENT_INVALID:
                                    Ext.Msg.alert('Failure',
                                        'Form fields may not be submitted with invalid values');
                                    break;
                                case Ext.form.Action.CONNECT_FAILURE:
                                    Ext.Msg.alert('Failure', 'Server connection problems. Please, try to reload page.');
                                    break;
                                case Ext.form.Action.SERVER_INVALID:
                                    Ext.Msg.alert('Failure', action.result.msg);
                            }
                        }
                    });
                }

            }
        ]
    });
    // ---------------------------

    var selectedRecord;

    var storeAttachments = new Ext.data.Store({
        paramNames : {
            start : 'start',
            limit : 'limit',
            locationId : 'locationId'
        },
        proxy : new Ext.data.HttpProxy({
            api : {
                read : '../../getLocationImages.json',
                //create : '../../createStaff.json',
                //update : '../../updateStaff.json',
                destroy : '../../deleteLocationImage.json'
            }
        }),

        reader : new Ext.data.JsonReader({
            root : 'pictures'
        }, [
            {
                name : 'id',
                type : 'int'
            },
            {
                name : 'name'
            },
            {
                name : 'url'
            },
            {
                name : 'locationId',
                type : 'int'
            },
            {
                name : 'creationDate',
                type : 'date'
            }
        ]), writer : new Ext.data.JsonWriter({
            encode : true, writeAllFields : true
        }),
        listeners: {
            write: function() {
                storeAttachments.reload();
            }
        }
    });

    var pagingToolBar = new Ext.PagingToolbar({
        pageSize : 10, width : 620, store : storeAttachments, displayInfo : true,
        displayMsg : '{0} - {1} of {2}', emptyMsg : "No records to display", hideBorders : true,
        listeners : {
            beforechange : function(toolbar, params) {
                params = Ext.apply(params, {
                    locationId : locationCombo.getValue()
                })
            }
        }
    });
    pagingToolBar.refresh.hide();

    var grid = new Ext.grid.GridPanel({
        id : 'userManagementPanel',
        baseCls: 'noborder',
        border: false,
        store : storeAttachments,
        width : 420,
        height : 350,
//        bbar : pagingToolBar,
        viewConfig: {
            forceFit: true,
            getRowClass: function(record, index) {
                var a = index % 2;
                if (a > 0) {
                    return 'selected-class';
                }
            }
        },
        columns : [
            {
                header : '#',
                width : 60,
                dataIndex : 'id',
                menuDisabled : true,
                renderer: function(value, meta, record, row, col, store){
                    var index = store.indexOf(record);
                    return index + 1;
                }
            },
            {
                header : 'Name',
                width : 200,
                dataIndex : 'name',
                menuDisabled : true
            },
            {
                header : 'Delete',
                width : 80,
                menuDisabled : true,
                renderer : function() {
                    return '<div class ="user-edit">Delete</div>';
                }
            }
        ]
    });

    grid.on({
        'cellclick' : function(grid, rowIndex, columnIndex, e) {
            selectedRecord = grid.getStore().getAt(rowIndex);
            if (columnIndex == 2) {

                Ext.Msg.confirm('Confirm Delete', 'Are you sure you want to delete this attachment', function(
                    btnText) {
                    if (btnText == 'yes') {
                        storeAttachments.remove(selectedRecord);
                        storeAttachments.save();
                        //storeAttachments.reload();
                        selectedRecord = -1;
                    }
                }, this);
            }

        }, scope : this
    });
    // -----------------------------------------------

    var form = new Ext.form.FormPanel({
        title : 'Attachments Settings',
        id : 'settings-view',
        layout : 'table',
        width : '100%',
        layoutConfig : {
            columns : 1
        },
        items : [
            uploadForm,
            grid
        ]
    });
    storeLocation.load({
        callback : function() {
            if (pageLoading) {
                locationCombo.setValue(GLOBAL.locationId);
                pageLoading = false;
            }
            storeAttachments.load({
                params : {
                    'start' : 0, 'limit' : 10, 'locationId' : locationCombo.getValue()
                }
            });
        }
    });
    form.render('content');
    var imageLogoFields = [ locationCombo, form.getForm().findField('imageLogo') ];
})

function setAllowBlank(items, allowBlank) {
    Ext.each(items, function(item) {
        item.allowBlank = allowBlank;
    });
}

