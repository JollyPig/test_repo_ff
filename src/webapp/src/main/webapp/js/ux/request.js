Ext.onReady(function() {
    var storeLocation = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            api: {
                read: '../../locations/getRealLocations.json'
            }
        }),

        reader: new Ext.data.JsonReader(
        {
            root:'locations'
        },
                [
                    {
                        name: 'id'
                    },
                    {
                        name: 'locationName'
                    }
                ])
    });

    var locationCombo = new Ext.form.ComboBox({
        id: 'locationCombo',
        displayField: 'locationName',
        valueField: 'id',
        hiddenName : 'iDlocationCombo',
        mode: 'local',
        width: 100,
        store: storeLocation,
        forceSelection: true,
        triggerAction: 'all',
        emptyText:'Select a location...',
        selectOnFocus:true,
        editable: false,
        allowBlank: false,
        hidden: isUser(),
        shadow : false		
    });

    function isUser()
    {
        if (GLOBAL.userId != 1) {
            return true;
        }
    }

    locationCombo.on('select', function(box, record, index) {
        var data = record.get('id');
        storeRequest.load(
        {
            params:
            {
                'locationId': data
            }
        });
    });

    Ext.namespace('Ext.ux');
    /**
     * Start bottom panel elements
     */
    var writer = new Ext.data.JsonWriter({
        encode: true,
        writeAllFields: true
    });

    var storeRequest = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            api: {
                read :    '../../getRequests.json',
                destroy:  '../../getRequestsDeleted.json' ,
                update :    '../../getRequestsUpdate.json'
            }
        }),
        reader: new Ext.data.JsonReader({
            root:'requests',
            idProperty: 'id'
        },
                ['id','emails', 'comments', 'totalToPrint', 'totalToSend',
                    {
                        name: 'creationDate'
                    }
                ]),
        writer: writer
    });

    var gridColumns = [
        {
            dataIndex: 'fff',
            header:'Action',
            width: 50,
            menuDisabled: true,
            resizable: false,
            renderer: function (v, p, r)
            {
                return '<div class="request_action">' +
                       '<img src="../../js/ux/images/custom/requestactions/delete.png">' +
                       '</div>';
            }
        },
        {
            dataIndex: 'ff',
            header:'',
            width:50,
            menuDisabled: true,
            resizable: false,
            renderer: function (v, p, r)
            {
                return '<div class="request_action">' +
                       '<img src="../../js/ux/images/custom/requestactions/add.png">' +
                       '</div>';
            }
        },
        {
            dataIndex:'creationDate',
            header:'Date',
            width:200,
            menuDisabled: true,
            sortable: true,
            resizable: false,
            renderer: function (v, p, r) {
                return new Date(v).format("m/d/Y g:i a");
            }
        },
        {
            dataIndex:'totalToPrint',
            header:'Total to Print',
            width:190,
            menuDisabled: true ,
            resizable: false
        },
        {
            dataIndex:'totalToSend',
            header:'Total to Send',
            width:190,
            resizable: false,
            menuDisabled: true
        },
        {
            dataIndex:'emails',
            header:'Email',
            width:200,
            resizable: false,
            menuDisabled: true
        } ,
        {
            dataIndex:'comments',
            header:'Comments',
            width:300,
            resizable: false,
            menuDisabled: true
        }


    ];

    var gridColModel = new Ext.grid.ColumnModel(gridColumns);


    var requestGrid = new Ext.grid.GridPanel({
        id: 'requestGrid',
        store: storeRequest,
        height: 380,
        width: 1200,
        cm: gridColModel,
        viewConfig: {
            forceFit: true,
            getRowClass: function(record, index) {
                var a = index % 2;
                if (a > 0) {
                    return 'selected-class';
                }
            }
        }

    });

    requestGrid.on({
        'cellclick' : function(grid, rowIndex, columnIndex, e) {
            var record = grid.getStore().getAt(rowIndex);
            if (columnIndex == 0)
            {
                grid.store.remove(record);
                grid.store.save();
                grid.getView().refresh();
            }
            if (columnIndex == 1)
            {
                Ext.Ajax.request({
                    url: '../../applyRequest.json',
                    method: 'POST',
                    params:{
                        'requestId': record.get('id')
                    },
                    success: function (result, request) {
                        var ids = result.responseText;
                        var array = Ext.util.JSON.decode(ids);
                        var requestId = array.requestId;
                        var lo = Ext.urlAppend(GLOBAL.contextPath + '/pictures/main/purchase', Ext.urlEncode({requestId: requestId} || {requestId: '-1'}));
                        window.location = lo;
                    },
                    failure: function (result, request) {
                        Ext.MessageBox.alert('Failed', result.responseText);
                    }
                });
            }

        },
        scope: this
    });

    var locationPanel = new Ext.Panel({
        layout: 'table',
        layoutConfig: {
            columns: 2
        },
        items: [
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'Location'
            },
            locationCombo
        ],
        hidden: isUser()
    });

    /**
     * Start main panel
     */
    var mainPanel = new Ext.Panel({
        id: 'request-view',
        title: 'Purchase Requests',
        layout: 'table',
        layoutConfig: {
            columns: 1
        },
        items: [locationPanel,requestGrid]
    });
    storeLocation.load();
    storeRequest.load();
    mainPanel.render('content');

});
