/**
 *  Slideshow page
 */

var prev = new Ext.Button({
    xtype: 'button',
    cls: 'previous',
    overCls: 'previous-over',
    height: 54,
    width: 27,
    text: '',
    handler: function() {
        slideshow.prevSlide();
    }
});

var next = new Ext.Button({
    xtype: 'button',
    cls: 'next',
    overCls: 'next-over',
    height: 54,
    width: 27,
    text: '',
    handler: function() {
        slideshow.nextSlide();
    }
});

var refresh = new Ext.Button({
    xtype: 'button',
    text: 'Refresh',
    handler: function() {
        slideshow.refreshSlides();
    }
});

var play = new Ext.Button({
    id: 'playBtn',
    xtype: 'button',
    text: 'Play',
    handler: function(btn, evt) {
        btn.setText(slideshow.togglePlayback() ? 'Pause' : 'Play');
    }
});

var purchase = new Ext.Button({
    xtype: 'button',
    text: 'Purchase',
    handler: function(btn, evt) {
        slideshow.purchaseSlides();
    }
});

var selectAll = new Ext.Button({
    xtype: 'button',
    text: 'Select/Deselect All',
    handler: function(btn, evt) {
        slideshow.applySelectionsAll();
    }
});

var slideshow = new Fotaflo.Slideshow({
    region: 'center',
    listeners: {
        statechanged: function(pageData) {
            prev.setDisabled(pageData.activePage == 1);
            next.setDisabled(pageData.activePage == pageData.pages);
        }
    }
});

var leftImage = new Ext.BoxComponent({
    autoEl: {
        tag: 'img',
        //src: '../../img/banners/banners_left.JPG'
        src: ".."+GLOBAL.leftUrl,
        id: 'banner_slide'
    },
    listeners: {
        beforerender: function(bc) {
            Ext.Ajax.request({
                url: '../../getBanner.json',
                method: 'POST',
                params:
                {
                    'type':'left'
                },
                success: function (result, request) {
                    var ids = result.responseText;
                    var array = Ext.util.JSON.decode(ids);
                    var avaliable = array.avaliable;
                    if (avaliable == "true") {
                        bc.setVisible(true);
                    } else {
                        bc.setVisible(false);
                    }
                },
                failure: function (result, request) {
                    Ext.MessageBox.alert('Failed', result.responseText);
                }
            });
        }
    }

});

var rightImage = new Ext.BoxComponent({
    autoEl: {
        tag: 'img',
        //src: '../../img/banners/banners_right.JPG'
        src: ".."+GLOBAL.rightUrl
    },
    listeners: {
        beforerender: function(bc) {
            Ext.Ajax.request({
                url: '../../getBanner.json',
                method: 'POST',
                params:
                {
                    'type':'right'
                },
                success: function (result, request) {
                    var ids = result.responseText;
                    var array = Ext.util.JSON.decode(ids);
                    var avaliable = array.avaliable;
                    if (avaliable == "true") {
                        bc.setVisible(true);
                    } else {
                        bc.setVisible(false);
                    }
                },
                failure: function (result, request) {
                    Ext.MessageBox.alert('Failed', result.responseText);
                }
            });
        }
    }
});

var vp = new Ext.Panel({
    layout: 'border',
    height: 900,
    width: 1600,
    items: [
        {
            region: 'north',
            width: 1600,
            height: 9
        },
        {
            region: 'west',
            layout:  {
                type: 'fit'/*,
                 align: 'middle',
                 pack: 'center'*/
            },
            height: 900,
            width: 270,
            items: [
                leftImage
            ]
        },
        {
            region: 'east',
            layout:  {
                type: 'fit'/*,
                 align: 'middle',
                 pack: 'center'*/
            },
            height: 900,
            width: 270,
            items: [
                rightImage
            ]
        },
        {
            region: 'south',
            layout: 'border',
            height: 100,
            width: 1600,
            items: [
                {
                    region: 'north',
                    height: 9,
                    width: 1600
                },
                {
                    region: 'west',
                    layout:  {
                        type: 'hbox',
                        align: 'middle',
                        pack: 'center'
                    },
                    width: 270,
                    height: 100,
                    items: [
                        prev
                    ]
                },
                {
                    region: 'east',
                    layout:  {
                        type: 'hbox',
                        align: 'middle',
                        pack: 'center'
                    },
                    width: 270,
                    height: 100,
                    items: [
                        next
                    ]
                },
                {
                    region: 'south',
                    height: 9,
                    width: 1600
                },
                {
                    id: 'slideshow-view',
                    region: 'center',
                    width: 1060,
                    layout: {
                        type: 'hbox',
                        align: 'middle',
                        pack: 'center',
                        defaultMargins: '0 5'
                    },
                    defaults: {
                        width: 130
                    },
                    height: 100,
                    items: [
                        purchase,
                        play,
                        refresh,
                        selectAll
                    ]
                }
            ]
        },
        slideshow
    ]
});



/*var vp = new Ext.Panel({
 layout: 'border',
 height: 850,
 width: 1143,
 items: [
 {
 region: 'north',
 width: 1143,
 height: 20
 },
 {
 region: 'west',
 layout:  {
 type: 'hbox',
 align: 'middle',
 pack: 'center'
 },
 height: 850,
 width: 100,
 items: [
 leftImage
 ]
 },
 {
 region: 'east',
 layout:  {
 type: 'hbox',
 align: 'middle',
 pack: 'center'
 },
 height: 850,
 width: 100,
 items: [
 rightImage
 ]
 },
 {
 region: 'south',
 layout: 'border',
 height: 120,
 width: 1143,
 items: [
 {
 region: 'north',
 height: 10,
 width: 1143
 },
 {
 region: 'west',
 layout:  {
 type: 'hbox',
 align: 'middle',
 pack: 'center'
 },
 width: 100,
 height: 100,
 items: [
 prev
 ]
 },
 {
 region: 'east',
 layout:  {
 type: 'hbox',
 align: 'middle',
 pack: 'center'
 },
 width: 100,
 height: 100,
 items: [
 next
 ]
 },
 {
 region: 'south',
 height: 10,
 width: 1143
 },
 {
 id: 'slideshow-view',
 region: 'center',
 layout: {
 type: 'hbox',
 align: 'middle',
 pack: 'center',
 defaultMargins: '0 5'
 },
 defaults: {
 width: 120
 },
 height: 80,
 items: [
 purchase,
 play,
 refresh,
 selectAll
 ]
 }
 ]
 },
 slideshow
 ]
 });*/

var mainPanel = new Ext.Panel({
    id: 'aaaaa',
    height: 900,
    width: 1600,
    renderTo: 'content',
    layout: 'absolute',
    items:[vp]
});