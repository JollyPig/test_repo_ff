Ext.onReady(function() {

    var leftImage = new Ext.BoxComponent({      
        autoEl: {
            tag: 'img',
            src: '../../img/banners/banners_left.JPG'
        }
    });

    var rightImage = new Ext.BoxComponent({
        autoEl: {
            tag: 'img',
            src: '../../img/banners/banners_right.JPG'
        }
    });

    var storeLocationLeft = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            api: {
                read: '../../locations/getRealLocations.json'
            }
        }),

        reader: new Ext.data.JsonReader(
        {
            root:'locations'
        },
                [
                    {
                        name: 'id'
                    },
                    {
                        name: 'locationName'
                    }
                ])
    });

    var storeLocationRight = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            api: {
                read: '../../locations/getRealLocations.json'
            }
        }),

        reader: new Ext.data.JsonReader(
        {
            root:'locations'
        },
                [
                    {
                        name: 'id'
                    },
                    {
                        name: 'locationName'
                    }
                ])
    });


    var locationComboLeft = new Ext.form.ComboBox({
        id: 'locationComboTextLeft',
        displayField: 'locationName',
        valueField: 'id',
        hiddenName : 'locationId',
        mode: 'local',
        width: 150,
        store: storeLocationLeft,
        forceSelection: true,
        triggerAction: 'all',
        emptyText:'Select a location...',
        selectOnFocus:true,
        editable: false,
        allowBlank: true,
        shadow : false

    });

    locationComboLeft.on('select', function(box, record, index) {
        var data = record.get('id');
        store4.load(
        {
            params:
            {
                'locationId':data,
                'type': 'left'
            }
        });
        dataview.refresh();
    });


    var locationComboRight = new Ext.form.ComboBox({
        id: 'locationComboTextRight',
        displayField: 'locationName',
        valueField: 'id',
        hiddenName : 'locationId',
        mode: 'local',
        width: 150,
        store: storeLocationRight,
        forceSelection: true,
        triggerAction: 'all',
        emptyText:'Select a location...',
        selectOnFocus:true,
        editable: false,
        allowBlank: true,
        shadow : false
    });

    locationComboRight.on('select', function(box, record, index) {
        var data = record.get('id');
        storeRight.load(
        {
            params:
            {
                'locationId':data,
                'type': 'right'
            }
        });
        dataviewRight.refresh();
    });

    var writer = new Ext.data.JsonWriter({
        encode: true,
        writeAllFields: true
    });

    var store4 = new Ext.data.Store({
        autoSave: false,
        proxy: new Ext.data.HttpProxy({
            method: 'POST',
            api: {
                read : '../../getBanners.json'
            }
        }),
        reader: new Ext.data.JsonReader({
            root:'banners',
            idProperty: 'id'
        },
                ['id','bannerUrl']),
        writer: writer
    });


    var tpl = new Ext.XTemplate(
            '<tpl for=".">',
            '<div class="thumb-wrap" id="{id}">',
            '<img src="..{bannerUrl}" title="{id}" class="thumb">',
            '</div>',
            '</tpl>'
            );

    var dataview = new Ext.DataView({
        store: store4,
        tpl: tpl,
        multiSelect: false,
        simpleSelect: false,
        emptyText: 'No left banner is uploaded to display',
        style: 'border-top-width: 7'
    });


    var storeRight = new Ext.data.Store({
        autoSave: false,
        proxy: new Ext.data.HttpProxy({
            method: 'POST',
            api: {
                read : '../../getBanners.json'
            }
        }),
        reader: new Ext.data.JsonReader({
            root:'banners',
            idProperty: 'id'
        },
                ['id','bannerUrl']),
        writer: new Ext.data.JsonWriter({
            encode: true,
            writeAllFields: true
        })
    });


    var tplRight = new Ext.XTemplate(
            '<tpl for=".">',
            '<div class="thumb-wrap" id="{id}">',
            '<img src="..{bannerUrl}" title="{id}" class="thumb">',
            '</div>',
            '</tpl>'
            );

    var dataviewRight = new Ext.DataView({
        store: storeRight,
        tpl: tplRight,
        multiSelect: false,
        simpleSelect: false,
        emptyText: 'No right banner is uploaded to display',
        style: 'border-top-width: 7'
    });


    var myMask = new Ext.LoadMask(Ext.getBody(), { msg:" Please wait..."});
    var importPanel = new Ext.form.FormPanel({
        title: 'Upload Left banner (270*800px)',
        layout: 'table',
        fileUpload: true,
        layoutConfig: {
            columns: 3,
            tableAttrs: {
                style: {width: '600px'}
            }
        },
        items: [
            {
                xtype: 'label',
                cls: 'p_labels bold',
                text: 'Left banner path:',
                cellCls: 'labelCell'
            },
            locationComboLeft,
            {
                xtype: 'field',
                id: 'bannerPicture',
                allowBlank: true,
                inputType: 'file'
            },
            {
                xtype : 'box',
                width : 10
            },
            {
                xtype: 'button',
                colspan: 1,
                text: "Upload Left Banner",
                handler: function() {
                    if(locationComboLeft.getValue()==null || locationComboLeft.getValue().length==0){
                      Ext.Msg.alert('Warning', "Please select the location");
                      return;
                    }
                    if(document.getElementById('bannerPicture').value==""){
                      Ext.Msg.alert('Warning', "Please select the banner to upload");
                      return;  
                    }
                    myMask.show();
                    importPanel.getForm().submit({
                        clientValidation: true,
                        url: '../../loadBanner.json',
                        params: {
                            banner: 'left'
                        },
                        success: function(form, action) {
                            myMask.hide();
                            form.fileUpload = false;
                            Ext.Msg.alert('Success', action.result.msg);
                            //document.getElementById("imgId").innerHTML="<img src ='../../img/banners/banners_left.JPG'/>";
                            store4.load(
                            {
                                params:
                                {
                                    locationId: locationComboLeft.getValue(),
                                    type: 'left'
                                }
                            });
                            dataview.refresh();
                        },
                        failure: function(form, action) {
                            myMask.hide();
                            form.fileUpload = false;
                            switch (action.failureType) {
                                case Ext.form.Action.CLIENT_INVALID:
                                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                    break;
                                case Ext.form.Action.CONNECT_FAILURE:
                                    Ext.Msg.alert('Failure', 'Server connection problems. Please, try to reload page.');
                                    break;
                                case Ext.form.Action.SERVER_INVALID:
                                    Ext.Msg.alert('Failure', action.result.msg);
                            }
                        }
                    });

                }
            },
            {
                xtype: 'button',
                colspan: 1,
                text: "Delete Left Banner ",
                handler: function() {
                    if(locationComboLeft.getValue()==null || locationComboLeft.getValue().length==0){
                          Ext.Msg.alert('Warning', "Please select the location");
                          return;
                    }
                    myMask.show();
                    importPanel.getForm().submit({
                        clientValidation: true,
                        url: '../../deleteBanner.json',
                        params: {
                            banner: 'left'
                        },
                        success: function(f, action) {
                            myMask.hide();
                            f.fileUpload = false;
                            Ext.Msg.alert('Success', action.result.msg);
                            store4.load(
                            {
                                params:
                                {
                                    locationId: locationComboLeft.getValue(),//importPanel.getForm().findField('locationComboTextLeft').getValue(),
                                    type: 'left'
                                }
                            });
                            dataview.refresh();
                        },
                        failure: function(form, action) {
                            myMask.hide();
                            form.fileUpload = false;
                            switch (action.failureType) {
                                case Ext.form.Action.CLIENT_INVALID:
                                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                    break;
                                case Ext.form.Action.CONNECT_FAILURE:
                                    Ext.Msg.alert('Failure', 'Server connection problems. Please, try to reload page.');
                                    break;
                                case Ext.form.Action.SERVER_INVALID:
                                    Ext.Msg.alert('Failure', action.result.msg);
                            }
                        }
                    });

                }

            }
            /* leftImage*/
        ]
    });

    var importedLeftPanel = new Ext.form.FormPanel({
        title: 'Uploaded left banner: ',
        layout: 'table',
        fileUpload: true,
        layoutConfig: {
            columns: 3,
            tableAttrs: {
                style: {width: '600px'}
            }
        },
        items: [

            {
                xtype : 'box',
                width : 10
            },
            {
                xtype : 'box',
                width : 10
            },
            dataview
        ]
    });

    var importedRightPanel = new Ext.form.FormPanel({
        title: 'Uploaded right banner: ',
        layout: 'table',
        fileUpload: true,
        layoutConfig: {
            columns: 3,
            tableAttrs: {
                style: {width: '600px'}
            }
        },
        items: [

            {
                xtype : 'box',
                width : 10
            },
            {
                xtype : 'box',
                width : 10
            },
            dataviewRight
        ]
    });


    var importPanel2 = new Ext.form.FormPanel({
        title: 'Upload Right banner (270*800px)',
        layout: 'table',
        fileUpload: true,
        layoutConfig: {
            columns: 3,
            tableAttrs: {
                style: {width: '600px'}
            }
        },
        items: [
            {
                xtype: 'label',
                cls: 'p_labels bold',
                text: 'Right banner path:',
                cellCls: 'labelCell'
            },
            locationComboRight,
            {
                xtype: 'field',
                id: 'bannerPictureRight',
                allowBlank: true,
                inputType: 'file'
            },
            {
                xtype : 'box',
                width : 10
            },
            {
                xtype: 'button',
                colspan: 1,
                text: "Upload Right Banner ",
                handler: function() {
                    if(locationComboRight.getValue()==null || locationComboRight.getValue().length==0){
                          Ext.Msg.alert('Warning', "Please, select the location");
                          return;
                    }
                    if(document.getElementById('bannerPictureRight').value==""){
                        Ext.Msg.alert('Warning', "Please select the banner to upload");
                      return;
                    }
                    myMask.show();
                    importPanel2.getForm().submit({
                        clientValidation: true,
                        url: '../../loadBannerRight.json',
                        params: {
                            banner: 'right'                           
                        },
                        success: function(f, action) {
                            myMask.hide();
                            f.fileUpload = false;
                            Ext.Msg.alert('Success', action.result.msg);
                            storeRight.load(
                            {
                                params:
                                {
                                    locationId: locationComboRight.getValue(),
                                    type: 'right'
                                }
                            });
                            dataviewRight.refresh();
                        },
                        failure: function(form, action) {
                            myMask.hide();
                            form.fileUpload = false;
                            switch (action.failureType) {
                                case Ext.form.Action.CLIENT_INVALID:
                                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                    break;
                                case Ext.form.Action.CONNECT_FAILURE:
                                    Ext.Msg.alert('Failure', 'Server connection problems. Please, try to reload page.');
                                    break;
                                case Ext.form.Action.SERVER_INVALID:
                                    Ext.Msg.alert('Failure', action.result.msg);
                            }
                        }
                    });

                }

            },
            {
                xtype: 'button',
                colspan: 1,
                text: "Delete Right Banner ",
                handler: function() {
                    if(locationComboRight.getValue()==null || locationComboRight.getValue().length==0){
                          Ext.Msg.alert('Warning', "Please, select the location");
                          return;
                    }
                    myMask.show();
                    importPanel2.getForm().submit({
                        clientValidation: true,
                        url: '../../deleteBannerRight.json',
                        params: {
                            banner: 'right'
                        },
                        success: function(f, action) {
                            myMask.hide();
                            f.fileUpload = false;
                            Ext.Msg.alert('Success', action.result.msg);
                            storeRight.load(
                            {
                                params:
                                {
                                    locationId: locationComboRight.getValue(),
                                    type: 'right'
                                }
                            });
                            dataviewRight.refresh();
                        },
                        failure: function(form, action) {
                            myMask.hide();
                            form.fileUpload = false;
                            switch (action.failureType) {
                                case Ext.form.Action.CLIENT_INVALID:
                                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                    break;
                                case Ext.form.Action.CONNECT_FAILURE:
                                    Ext.Msg.alert('Failure', 'Server connection problems. Please, try to reload page.');
                                    break;
                                case Ext.form.Action.SERVER_INVALID:
                                    Ext.Msg.alert('Failure', action.result.msg);
                            }
                        }
                    });

                }

            }
            /*,
             rightImage*/
        ]
    });


    var items
    if (GLOBAL.userId == 1) {
        items = [ importPanel,importPanel2,importedLeftPanel,importedRightPanel ];
    }

    var form = new Ext.Panel({
        title: 'Import banner pictures',
        id: 'banner-view',
        layout: 'table',
        width: '100%',
        layoutConfig: {
            columns: 2,
            tableAttrs: {
                style: {width: '100%'}
            }

        },
        items: items
    });

    storeLocationLeft.load();
    storeLocationRight.load();
    form.render('content');

})
