/**
 * Created by .
 * User: MilkevichP
 * Date: 19.04.11
 * Time: 15:25
 * To change this template use File | Settings | File Templates.
 */

Ext.ns('Fotaflo');

Fotaflo.Slideshow = Ext.extend(Ext.Panel, {

    slideInterval: (GLOBAL.slideSpeed == null ? 12000 : (60 / GLOBAL.slideSpeed * 1000)),
    slidesOnPage: (GLOBAL.slideCount == null ? 1 : GLOBAL.slideCount),
    continuosPlayback: true,
    layout: 'fit',
    selections: [],
    selectAll: false,
    showclasspref: GLOBAL.slideCount > 1 ? '' : '-single',

    constructor: function(config) {
        var me = this;
        Ext.apply(this, config);

       // var urlParams = Ext.urlDecode(location.search.substring(1));
        var urlParams =  Ext.urlDecode(location.search.replace("getFilter=save","getFilter=skip").substring(1));
        this.store = new Ext.data.Store({
            autoSave: false,
            baseParams: urlParams,
            proxy: new Ext.data.HttpProxy(GLOBAL.slideshowProxyConfig),
            reader: new Ext.data.JsonReader({
                root: 'pictures',
                totalProperty: 'total',
                idProperty: 'id',
                fields:   ['id','url', 'base64code', 'name', 'cameraId','cameraName',
                    {
                        name: 'selected',
                        type: 'boolean'
                    },
                    {
                        name: 'creationDate'
                    },
                    {
                        name: 'logoMainUrl'
                    },
                    {
                        name: 'logoUrl'
                    },
                    {
                        name: 'logoText'
                    },
                    {
                        name: 'rotated',
                        type: 'boolean'
                    },
                    {
                        name: 'pictureSize'
                    }

                ]
            })
        });

        this.dataview = new Ext.DataView({
            store: me.store,
            itemSelector: 'div.picture-wrapper' + me.showclasspref,
            overClass: 'picture-wrapper-over' + me.showclasspref,
            selectedClass: 'picture-wrapper-selected' + me.showclasspref,
            simpleSelect: true,
            multipleSelect: true,
            tpl: new Ext.XTemplate(
                    '<tpl for=".">',
                    '<div class ="picture-wrapper' + me.showclasspref + '">',
                    '<div class ="picture-wrapper-watermark-div' + me.showclasspref + '">',
                    '<div class="picture' + me.showclasspref + '"  style="background: url(\'../{pictureurl}\') center center no-repeat; background-size: contain;" title="{name}"></div>',
                    '<span class="picture-wrapper-watermark' + me.showclasspref + ' {rotated}'+ ' {pictureSize}">{logoText}</span>',
                //  '<div class="picture-index' + me.showclasspref + '">{[this.formatDate(values.creationDate)]}</div>',
                    '<span class="picture-index' + me.showclasspref + ' {rotated}'+ ' {pictureSize}">{[this.formatDate(values.creationDate)]}</span>',
                    '<tpl if="logoUrl.length == 0">',
                    '<img src="..{logoUrl}" style="visibility:hidden;"/>',
                    '</tpl>',
                    '<tpl if="logoUrl.length != 0">',
                    '<img src="..{logoUrl}" class="picture-wrapper-logo-watermark' + me.showclasspref + ' {rotated}'+ ' {pictureSize}"  title = "" alt=""/>',
                    '</tpl>',
                    '<tpl if="logoMainUrl.length == 0">',
                    '<img src="..{logoMainUrl}" style="visibility:hidden;"/>',
                    '</tpl>',
                    '<tpl if="logoMainUrl.length != 0">',
                    '<img src="..{logoMainUrl}" class="picture-wrapper-logoMain-watermark' + me.showclasspref + ' {rotated}'+ ' {pictureSize}" title = "" alt=""/>',
                    '</tpl>',
                    '<tpl if="this.isPublicUser()">',            
                    '<img src="../../css/images/logo.png" class="picture-watermark-opaque' + me.showclasspref + '"  title = "" alt=""/>',
                    '</tpl>',
                    '</div>',
                    '</div>',
                    '</tpl>',
                    '<div class="x-clear"></div>',
            {
                    isPublicUser: function() {
                        return GLOBAL.userId == 3;
                    }
            },
            {
                // XTemplate configuration:
                compiled: true,
                disableFormats: true,
                // member functions:
                formatDate: function(date) {
                    return date.substring(date.indexOf(" ") + 1, date.length);
                }
            }

                    ),
            prepareData: function(data) {
                data.dateString = data.creationDate;
                data.pictureurl = (GLOBAL.slideCount == null || GLOBAL.slideCount == 1) ? data.base64code : data.base64code;
                return data;
            },
            listeners: {
                click : function(dataview, index, node, e) {
                    var rec = dataview.getRecord(node);
                    if (me.selections.indexOf(rec.id) < 0) {
                        me.selections.push(rec.id);
                        dataview.select(rec)
                    } else {
                        me.selections.remove(rec.id);
                        dataview.deselect(rec);
                    }
                }
            }
        });

        this.items = [
            this.dataview
        ];

        this.pagination = new Ext.PagingToolbar({
            pageSize: me.slidesOnPage,
            hidden: true,
            store: me.store,
            displayInfo: false,
            emptyMsg: me.emptyText,
            listeners: {
                change: function(paging, pageData) {
                    me.applySelections();
                    me.fireEvent('statechanged', pageData);
                }
            }
        });

        this.bbar = this.pagination;

        this.addEvents('statechanged');

        Fotaflo.Slideshow.superclass.initComponent.call(this, config);

        this.on('afterrender', me.refreshSlides);
    },

    nextSlide: function(cmp) {
        var me = cmp || this;
        if (me.continuosPlayback) {
            var pageData = me.pagination.getPageData();
            if (pageData.activePage < pageData.pages) {
                me.pagination.moveNext();
            } else {
                // jump to first
                me.pagination.moveFirst();
            }
        } else {
            me.pagination.moveNext();
        }
    },

    prevSlide: function(cmp) {
        var me = cmp || this;
        if (this.continuosPlayback) {
            var pageData = me.pagination.getPageData();
            if (pageData.activePage > 1) {
                me.pagination.movePrevious();
            } else {
                // jump to first
                me.pagination.moveLast();
            }
        } else {
            me.pagination.movePrevious();
        }
    },

    togglePlayback: function(cmp) {
        var me = cmp || this;
        if (!me.timer)
            me.timer = setInterval(function() {
                me.nextSlide(me);
            }, me.slideInterval);
        else {
            clearInterval(me.timer);
            me.timer = undefined;
        }
        return Ext.isDefined(me.timer);
    },

    refreshSlides: function(cmp) {
        var me = cmp || this;
        me.pagination.doRefresh();
    },

    purchaseSlides: function(cmp) {
        var me = cmp || this;
        var len_selections = me.selections.length;
        var lo = Ext.urlAppend(GLOBAL.contextPath + '/pictures/main/helpyourself', Ext.urlEncode({ids: me.selections} || {}));
        if (len_selections > 0) {
            var mywindow = window.open(
                    lo,
                    'helpYourSelf' + new Date().getMilliseconds(),
                    'scrollbars=yes,menubar=no,height=850,width=1200,resizable=yes,toolbar=no,location=no,status=no'
                    );
            mywindow.moveTo(10, 10);
            /* me.selections = [];
             me.dataview.clearSelections();*/
        }
        else {
            Ext.Msg.alert('Notice:', 'Select pictures to purchase');
        }
    },

    applySelections: function() {
        var me = this;
        var nodes = this.dataview.getNodes();
        for (var i = 0; i < nodes.length; i++) {
            var rec = me.dataview.getRecord(nodes[i]);
            if (me.selections.indexOf(rec.id) >= 0)
                me.dataview.select(rec);
            else
                me.dataview.deselect(rec);
        }
    },

    applySelectionsAll: function() {
        var me = this;
        if (!this.selectAll) {
            Ext.Ajax.request({
                url: '../../selectAllSlideshow.json',
                params:{
                    'startdate': me.store.baseParams.startdate,
                    'starttime': me.store.baseParams.starttime,
                    'enddate': me.store.baseParams.enddate,
                    'endtime': me.store.baseParams.endtime,
                    'location': me.store.baseParams.location,
                    'camera': me.store.baseParams.camera,
                    'tag': me.store.baseParams.tag
                },
                success: function (result, request) {
                    var ids = result.responseText;
                    var array = Ext.util.JSON.decode(ids).ids;
                    var ids_items = new Array();
                    var i = 0;
                    for (i; i < array.length; i++) {
                        ids_items[i] = parseInt(array[i]);
                    }
                    me.selections = ids_items;
                    me.applySelections();
                },
                failure: function (result, request) {
                    me.selections = [];
                    me.applySelections();
                }
            });
        }
        else {
            me.selections = [];
            me.applySelections();
        }
        this.selectAll = (!this.selectAll);
    }

});

Ext.reg('slideshow', Fotaflo.Slideshow);
