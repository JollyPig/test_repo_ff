Ext.onReady(function() {

    /**
     * Social Network Settings
     */
    var storeLocation = new Ext.data.Store({
        proxy: new Ext.data.HttpProxy({
            api: {
                read: '../../locations/getRealLocations.json'
            }
        }),

        reader: new Ext.data.JsonReader(
        {
            root:'locations'
        },
                [
                    {
                        name: 'id'
                    },
                    {
                        name: 'locationName'
                    }
                ])
    });

    var currencyStore = new Ext.data.ArrayStore({
        id: 0,
        fields: [
            'idCurrency',
            'currency'
        ],
        data: [
            [0, 'USD'],
            [1, 'CAD']
        ]
    });

    var currencyCombo = new Ext.form.ComboBox({
        id: 'currencyComboText',
        displayField: 'currency',
        valueField: 'currency',
        hiddenName : 'currency',
        mode: 'local',
        width: 150,
        store: currencyStore,
        forceSelection: true,
        triggerAction: 'all',
        emptyText:'Select a currency...',
        selectOnFocus:true,
        editable: false,
        allowBlank: true,
        shadow : false
    });

    var locationCombo = new Ext.form.ComboBox({
        id: 'locationComboText',
        displayField: 'locationName',
        valueField: 'id',
        hiddenName : 'locationId',
        mode: 'local',
        width: 150,
        store: storeLocation,
        forceSelection: true,
        triggerAction: 'all',
        emptyText:'Select a location...',
        selectOnFocus:true,
        editable: false,
        allowBlank: true,
        shadow : false
    });


    var pricefield = new Ext.form.TextField({
           xtype: 'textfield',
           name: 'price',
           id: 'price',
           width: 100,
           allowBlank:false
       });

    var packagePriceField = new Ext.form.TextField ({
        xtype: 'textfield',
        name: 'packagePrice',
        id: 'packagePrice',
        width: 100,
        hidden: true
    });

    var packagePriceLabel = new Ext.form.Label ({
        xtype: 'label',
        cls: 'p_labels',
        text: 'Package price',
        hidden: true
    });
    
    locationCombo.on('select', function(box, record, index) {
        var data = record.get('id');
        Ext.Ajax.request({
            url: '../../getPayPalSettings.json',
            params:{
                'locationId': data
            },
            success: function (result, request) {
                var ids = result.responseText;
                var array = Ext.util.JSON.decode(ids);
                var price = array.price;
                var currency = array.currency;
                pricefield.setValue(price);
               // currencyfield.setValue(currency);
                currencyCombo.setValue(currency);

                var isVisiblePackagePrice = record.json['rfid'];
                packagePriceField.setVisible (isVisiblePackagePrice);
                packagePriceLabel.setVisible (isVisiblePackagePrice);
                packagePriceField.setValue (isVisiblePackagePrice ? array["packagePrice"] : "");
                document.getElementById ('package_price_hint').style.display = isVisiblePackagePrice ? "none" : "block";
            },
            failure: function (result, request) {
                Ext.MessageBox.alert('Failed', result.responseText);
            }
        });

    });


    var merchantPanel = new Ext.Panel({
        title: 'Merchant account',
        id: 'merchantPanel',
        height   : 'auto',
        layout: 'table',
        layoutConfig: {
            columns: 2
        },
        items: [
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'Location'
            },
            locationCombo,          
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'Price per picture'
            },
            pricefield,
            {
                xtype: 'label',
                cls: 'p_labels',
                text: 'Currency'
            },
            currencyCombo,
            packagePriceLabel,
            packagePriceField
        ]
    });

    var packagePricePanel = new Ext.Panel({
        id: 'packagePricePanel',
        height: 'auto',
        layout: 'table',
        layoutConfig: {
            columns: 1
        },
        items: [{
            html: '<label id="package_price_hint" class="p_labels" style="display: none;">If you want to set \"Package price\", please set \"Enable Tags for slideshow\" for location</label>'
        }]
    });

    /**
     * Button Panel
     */
    var saveButton = new Ext.Button({
        text: 'Save',
        handler: function() {
            if (locationCombo.getValue().length == 0) {
                Ext.Msg.alert('Failure:', 'Choose the Location');
                return;
            }

            var errorMassage  = "";
            var elem5 = document.getElementById("price");
            var elem6 = document.getElementById("currency");

            if (elem5 != null) {
                var reg =/^[0-9]{1,1}\d{0,9}$/;
                var val5 = elem5.value;
                if (val5.length == 0 || !reg.test(val5)) {
                    errorMassage += "price,";
                }
            }
            if (elem6 != null) {
                var val6 = elem6.value;
                if (val6.length == 0) {
                    errorMassage += "currency ";
                }
            }
            if (errorMassage.length > 0) {
                errorMassage = 'Invalid Fields: ' + errorMassage;
                Ext.Msg.alert('Failure:', errorMassage);
                return;
            }

            mainPanel.getForm().submit({
                url: '../../saveAccountSettings.json',
                scope:this,
                clientValidation: false,
                success: function(f, a) {
                    Ext.Msg.alert('Success:', a.result.msg);
                },
                failure: function(f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure:', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure:', 'Server connection problems. Please, try to reload page.');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure:', a.result.msg);
                    }

                }
            });
        }
    });

    var cancelButton = new Ext.Button({
        text: 'Cancel',
        handler: function() {
           var lo = Ext.urlAppend(GLOBAL.contextPath + '/pictures/main/common', Ext.urlEncode({getFilter: "load"}));
           window.location = lo;
        }
    });


    var buttonPanel = new Ext.Panel({
        id: 'buttonPanel',
        width: 'auto',
        height: 50,
        layout: 'table',
        layoutConfig: {
            columns: 2
        },
        items: [saveButton, cancelButton]
    });
    /**
     * Main Panel
     */
    var mainPanel = new Ext.FormPanel({
        title: 'Administration - Paypal Settings',
        id: 'emailSettingsPanel',
        height   : 600,
        layout: 'table',
        layoutConfig: {
            columns: 1
        },
        items: [merchantPanel, packagePricePanel, buttonPanel]
    });

    storeLocation.load();    
    mainPanel.render('content');
});