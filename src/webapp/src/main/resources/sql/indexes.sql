ALTER TABLE `picture`
  ADD INDEX `CREATION_DATE_INDEX` (`creation_date`);

ALTER TABLE `systemuser`
  ADD INDEX `LOGIN_NAME_INDEX` (`login_name`);