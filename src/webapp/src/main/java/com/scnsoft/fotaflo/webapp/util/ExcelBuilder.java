package com.scnsoft.fotaflo.webapp.util;

import com.scnsoft.fotaflo.common.util.NumberUtils;
import com.scnsoft.fotaflo.common.util.StringUtils;
import com.scnsoft.fotaflo.webapp.bean.StatisticBean;
import com.scnsoft.fotaflo.webapp.bean.StatisticStaffBean;
import com.scnsoft.fotaflo.webapp.model.Staff;
import com.scnsoft.fotaflo.webapp.model.Statistic;
import com.scnsoft.fotaflo.webapp.model.Package;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.*;
import jxl.write.Number;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 02.04.13
 * Time: 15:53
 * To change this template use File | Settings | File Templates.
 */
public class ExcelBuilder {
    public static int printPrice = 5;

    WritableWorkbook workbook;

    private SortedMap reportHash;
    private List reportGeneralList;
    private List<Statistic> statisticList;

    public ExcelBuilder(OutputStream outputStream) throws IOException{
        if(outputStream == null){
            throw new IllegalArgumentException("outputStream cannot be null");
        }

        WorkbookSettings wbSettings = new WorkbookSettings();

        wbSettings.setLocale(new Locale("en", "EN"));

        this.workbook = Workbook.createWorkbook(outputStream, wbSettings);
    }

    public void writeDailyReport(int sheetNumber, String locationName, List<Staff> staff, Date date) throws IOException, WriteException {
        workbook.createSheet(locationName, sheetNumber);
        WritableSheet excelSheet = workbook.getSheet(sheetNumber);
        createDailyLabelReport(excelSheet, locationName, date, staff);
    }

    public void writeWeeklyReport(int sheetNumber, String locationName, int colsNumber) throws IOException, WriteException {
        workbook.createSheet(locationName, sheetNumber);
        WritableSheet excelSheet = workbook.getSheet(sheetNumber);
        createWeeklyMonthlyReport(excelSheet, colsNumber, locationName, false);
    }

    public void writeMonthlyReport(int sheetNumber, String locationName, int colsNumber) throws IOException, WriteException {
        workbook.createSheet(locationName, sheetNumber);
        WritableSheet excelSheet = workbook.getSheet(sheetNumber);

        createWeeklyMonthlyReport(excelSheet, colsNumber, locationName, true);
    }

    public void closeWorkbook() throws IOException, WriteException {
        workbook.write();
        workbook.close();
    }

    private int getPackagePrice(Package pack) {
        int price = -1;
        Integer result = null;
        if (pack.getAccess() > Package.PackageAccess.DEFAULT.getId()) {
            if (pack.getLocation().getPaypallocationSettings() != null){
                if(Package.PackageAccess.GROUP_PHOTO.getId().equals(pack.getAccess())){
                    if(!StringUtils.isEmpty(pack.getLocation().getPaypallocationSettings().getPackagePrice())){
                        result = NumberUtils.getInteger(pack.getLocation().getPaypallocationSettings().getPackagePrice());
                    }
                }else if(!StringUtils.isEmpty(pack.getLocation().getPaypallocationSettings().getPrice())){
                    result = NumberUtils.getInteger(pack.getLocation().getPaypallocationSettings().getPrice());
                }
            }
        } else {
            java.util.regex.Pattern p1 = java.util.regex.Pattern.compile("(\\d+)");
            Matcher m1 = p1.matcher(pack.getPackageName());
            if (m1.find()) {
                String size = m1.group(1);
                result = NumberUtils.getInteger(size);
            }
        }

        if(result != null){
            price = result.intValue();
        }

        return price;
    }

    private void createDailyLabelReport(WritableSheet sheet, String locationName, Date date, List<Staff> staff)
            throws WriteException {
        ReportStylesUtil reportStylesUtil = new ReportStylesUtil();
        sheet.getSettings().setVerticalFreeze(6);
        sheet.getSettings().setHorizontalFreeze(5);
        sheet.setColumnView(1, 15);
        sheet.setColumnView(2, 10);
        sheet.setColumnView(3, 30);
        sheet.setColumnView(4, 30);
        sheet.setColumnView(5, 15);
        sheet.setColumnView(7, 70);
        sheet.setColumnView(8, 15);
        sheet.setColumnView(10, 50);
        sheet.setColumnView(11, 50);
        sheet.setColumnView(12, 50);
        sheet.setColumnView(13, 50);

        String title = "";

        String titleLocation = "ALL DAILY TRANSACTIONS";

        String dateConverted = DateConvertationUtils.reportStringDateFormat(date);

        sheet.addCell(new Label(1, 1, dateConverted, reportStylesUtil.getBoldStyle()));
        sheet.mergeCells(2, 1, 4, 1);
        sheet.addCell(new Label(2, 1, title + "(previous day's date)", reportStylesUtil.getRegularStyle()));
        sheet.mergeCells(1, 3, 3, 3);
        sheet.addCell(new Label(1, 3, titleLocation, reportStylesUtil.getBoldStyle()));
        sheet.mergeCells(4, 3, 5, 3);
        sheet.addCell(new Label(4, 3, "Location: " + locationName, reportStylesUtil.getBoldStyle()));

        List<String> headerStrings = Arrays.asList("Date:", "Time:", "Location:", "Package:",
                "# of Pictures:", "Picture Name (first):", "# of copies to Print:", "# of participants:", "Email Count:", "E-mails:",
                "E-mail Subject Line", "Subscribing E-mails", "teasing E-mails: ");

        int camSize = 0;
        for (Statistic statisc : statisticList) {
            int cams = Arrays.asList(statisc.getCameraNames().split(",")).size();
            if (camSize < cams) {
                camSize = cams;
            }
        }

        //Creating Header
        int col = 1;
        int row = 5;
        for (int i = col; i < col + headerStrings.size(); i++) {
            sheet.addCell(new Label(i, row, headerStrings.get(i - 1), reportStylesUtil.getHeaderBorderStyleLeft()));
        }
        for (int i = 0; i < camSize; i++) {
            sheet.addCell(new Label(col + headerStrings.size() + i, row, "Camera " + (i + 1), reportStylesUtil.getHeaderBorderStyleLeft()));
        }
        Collections.sort(staff);

        for (int i = 0; i < staff.size(); i++) {
            sheet.addCell(new Label(col + headerStrings.size() + camSize + i, row, ((Staff) staff.get(i)).getStaffName(), reportStylesUtil.getHeaderBorderStyleLeft()));
        }
        row++;

        int pack = 0;
        for (Statistic statisc : statisticList) {
            int colu = 1;
            sheet.addCell(new Label(colu, row + pack, dateConverted, reportStylesUtil.getRegularBorderStyleLeft()));
            colu++;
            sheet.addCell(new Label(colu, row + pack, DateConvertationUtils.reportStringTimeFormat(statisc.getCreationDate()), reportStylesUtil.getRegularBorderStyleLeft()));
            colu++;
            sheet.addCell(new Label(colu, row + pack, locationName, reportStylesUtil.getRegularBorderStyleLeft()));
            colu++;
            sheet.addCell(new Label(colu, row + pack, statisc.getPackages().getPackageName(), reportStylesUtil.getRegularBorderStyleLeft()));
            colu++;
            sheet.addCell(new Label(colu, row + pack, String.valueOf(statisc.getTotalPictureNumber()), reportStylesUtil.getRegularBorderStyleLeft()));
            colu++;
            sheet.addCell(new Label(colu, row + pack, String.valueOf(statisc.getAnyPictureName()), reportStylesUtil.getRegularBorderStyleLeft()));
            colu++;
            sheet.addCell(new Label(colu, row + pack, String.valueOf(statisc.getNumberToPrintAll()), reportStylesUtil.getRegularBorderStyleLeft()));
            colu++;
            sheet.addCell(new Label(colu, row + pack, String.valueOf(statisc.getNumberOfParticipantAll()), reportStylesUtil.getRegularBorderStyleLeft()));
            colu++;
            sheet.addCell(new Label(colu, row + pack, ((statisc.getEmails() != null && statisc.getEmails().length() != 0) ? String.valueOf(statisc.getEmails().split(",").length) : "0"), reportStylesUtil.getRegularBorderStyleLeft()));
            colu++;
            sheet.addCell(new Label(colu, row + pack, String.valueOf(statisc.getEmails()), reportStylesUtil.getRegularBorderStyleLeft()));
            colu++;
            sheet.addCell(new Label(colu, row + pack, statisc.getEmailSubject() == null ? "" : statisc.getEmailSubject(), reportStylesUtil.getRegularBorderStyleLeft()));
            colu++;
            sheet.addCell(new Label(colu, row + pack, String.valueOf(statisc.getSubscribeEmails()), reportStylesUtil.getRegularBorderStyleLeft()));
            colu++;
            sheet.addCell(new Label(colu, row + pack, String.valueOf(statisc.getTeasers()), reportStylesUtil.getRegularBorderStyleLeft()));
            colu++;
            //Cameras
            int camColu = colu;
            List<String> camsList = Arrays.asList(statisc.getCameraNames().split(","));
            for (int i = 0; i < camSize; i++) {
                if (i < camsList.size()) {
                    sheet.addCell(new Label(camColu, row + pack, camsList.get(i), reportStylesUtil.getRegularBorderStyleLeft()));
                } else {
                    sheet.addCell(new Label(camColu, row + pack, "", reportStylesUtil.getRegularBorderStyleLeft()));
                }
                camColu++;
            }
            Set<Staff> staffSet = statisc.getStaff();
            List<String> staffNames = new ArrayList<String>();
            for (Staff stItem : staffSet) {
                staffNames.add(stItem.getStaffName());
            }
            int staffColu = camColu;
            for (int i = 0; i < staff.size(); i++) {
                String staffName = ((Staff) staff.get(i)).getStaffName();
                if (staffNames.contains(staffName)) {
                    sheet.addCell(new Label(staffColu, row + pack, String.valueOf(statisc.getTotalPictureNumber()), reportStylesUtil.getRegularBorderStyleLeft()));
                } else {
                    sheet.addCell(new Label(staffColu, row + pack, "", reportStylesUtil.getRegularBorderStyleLeft()));
                }
                staffColu++;
            }
            pack++;
        }

    }


    private void createWeeklyMonthlyReport(WritableSheet sheet, int colsNumber, String locationName, boolean monthly)
            throws WriteException {
        ReportStylesUtil reportStylesUtil = new ReportStylesUtil();
        int freeze = monthly ? 7 : 6;
        sheet.getSettings().setVerticalFreeze(6);
        sheet.getSettings().setHorizontalFreeze(freeze);
        sheet.setColumnView(1, 15);
        int j = monthly ? 4 : 3;
        sheet.setColumnView(3, 15);
        if (monthly) {
            sheet.setColumnView(4, 15);
        }
        sheet.setColumnView(++j, 30);
        sheet.setColumnView(++j, 30);
        j += 2;
        sheet.setColumnView(++j, 12);
        sheet.setColumnView(++j, 12);
        j++;
        sheet.setColumnView(++j, 10);

        int count = 1;
        String title = "";
        String titleStart = "";
        String titleEnd = "";
        String[] days = new String[7];
        for (Iterator nE = reportHash.keySet().iterator(); nE.hasNext(); ) {
            String key = (String) nE.next();
            if (count == 1) {
                if (monthly) {
                    Pattern p1 = Pattern.compile("Week\\d+\\s+\\((.+)-(.+)\\)");
                    Matcher m1 = p1.matcher(key);
                    if (m1.find()) {
                        title = m1.group(1);
                        titleStart = title;
                    }
                } else {
                    title = key;
                }
            }
            if (count == colsNumber) {
                if (monthly) {
                    Pattern p1 = Pattern.compile("Week\\d+\\s+\\((.+)-(.+)\\)");
                    Matcher m1 = p1.matcher(key);
                    if (m1.find()) {
                        titleEnd = m1.group(2);
                        title = title + " - " + titleEnd;
                    }
                } else {
                    title = title + " - " + key;
                }
            }
            days[count - 1] = key;
            count++;
        }
        List<String> months = Arrays.asList("JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER");
        String month = "";
        if (titleStart.length() != 0 && titleStart.length() > 6) {
            try {
                month = months.get(Integer.parseInt(titleStart.substring(0, 2)) - 1) + " " + titleStart.substring(6, titleStart.length());
            } catch (NumberFormatException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        String titleLocation = "ALL SALES AND STAFF - " + (monthly ? month : "WEEK IN REVIEW");

        for (int i = 2; i < (monthly ? 7 : 6); i++) {
            sheet.addCell(new Label(i, 0, "", reportStylesUtil.getPlainBorderBottomStyle()));
            sheet.addCell(new Label(i, 4, "", reportStylesUtil.getPlainBorderTopStyle()));

            if (i < 5)
                sheet.addCell(new Label(1, i - 1, "", reportStylesUtil.getPlainBorderRightStyle()));
        }

        sheet.addCell(new Label(2, 1, "Period: ", reportStylesUtil.getBoldStyle()));
        sheet.mergeCells(3, 1, 4, 1);
        sheet.addCell(new Label(3, 1, title, reportStylesUtil.getRegularStyle()));
        sheet.mergeCells(5, 1, 6, 1);
        sheet.addCell(new Label(5, 1, monthly ? "*Weeks run Sunday unless the month begins/ends midweek" : "", reportStylesUtil.getRegularStyle()));
        sheet.mergeCells(2, 3, 4, 3);
        sheet.addCell(new Label(2, 3, titleLocation, reportStylesUtil.getBoldStyle()));
        sheet.mergeCells(5, 3, monthly ? 6 : 5, 3);
        sheet.addCell(new Label(5, 3, "Location:  " + locationName, reportStylesUtil.getBoldStyle()));

        List<String> headerStrings = monthly ? Arrays.asList("Month #", "Start Date", "End Date", "Location", "Package Type", "Price", "# of Packages", "# of Participants", "Av. # of Participants", "# of 5$ Prints", "$Revenue", "Monthly Visitors", "Confirmed /Estimated", "Conversion Rate")
                : Arrays.asList("Month #", "Date", "Location", "Package Type", "Price", "# of Packages", "# of Participants", "Av. # of Participants", "# of 5$ Prints", "$Revenue");
        //Creating Header
        int col = 2;
        int row = 5;
        for (int i = col; i < col + headerStrings.size(); i++) {
            sheet.addCell(new Label(i, row, headerStrings.get(i - 2), reportStylesUtil.getHeaderBorderStyleLeft()));
        }
        row++;

        String keyDate = "    ";
        for (Iterator nE = reportHash.keySet().iterator(); nE.hasNext(); ) {
            String key = (String) nE.next();
            List<StatisticBean> statisticBeanlist = (List<StatisticBean>) reportHash.get(key);

            String start = "";
            String end = "";
            String week = "";
            if (monthly) {
                Pattern p1 = Pattern.compile("(Week\\d+)\\s+\\((.+)-(.+)\\)");
                Matcher m1 = p1.matcher(key);
                if (m1.find()) {
                    start = m1.group(2);
                    end = m1.group(3);
                    week = m1.group(1);
                }
            }

            sheet.mergeCells(1, row, 1, row + statisticBeanlist.size() - 1);
            sheet.addCell(new Label(1, row, key.replaceAll("\\/", "\\."), reportStylesUtil.getBoldBorderCenterStyle()));
            if (monthly) {
                sheet.addCell(new Label(1, row, week, reportStylesUtil.getBoldBorderCenterStyle()));
            }

            int pack = 0;
            for (Iterator stListitem = statisticBeanlist.iterator(); stListitem.hasNext(); ) {
                StatisticBean statisticBean = (StatisticBean) stListitem.next();
                int packPrice = getPackagePrice(statisticBean.getPackages());
                int colu = 2;
                keyDate = key;
                if (monthly) {
                    sheet.addCell(new Label(colu, row + pack, start.substring(0, 2), reportStylesUtil.getRegularBorderStyleRight()));
                    colu++;
                    sheet.addCell(new Label(colu, row + pack, start.replaceAll("\\/", "\\."), reportStylesUtil.getRegularBorderStyleRight()));
                    colu++;
                    sheet.addCell(new Label(colu, row + pack, end.replaceAll("\\/", "\\."), reportStylesUtil.getRegularBorderStyleRight()));
                    colu++;

                } else {
                    sheet.addCell(new Label(colu, row + pack, key.substring(0, 2), reportStylesUtil.getRegularBorderStyleRight()));
                    colu++;
                    sheet.addCell(new Label(colu, row + pack, key.replaceAll("\\/", "\\."), reportStylesUtil.getRegularBorderStyleRight()));
                    colu++;
                }

                sheet.addCell(new Label(colu, row + pack, locationName, reportStylesUtil.getRegularBorderStyleLeft()));
                colu++;
                sheet.addCell(new Label(colu, row + pack, statisticBean.getPackages().getPackageName(), reportStylesUtil.getRegularBorderStyleLeft()));
                colu++;
                sheet.addCell(new Number(colu, row + pack, (packPrice > 0 ? packPrice : 0), reportStylesUtil.getRegualarBorderDollarStyle()));
                colu++;


                if (statisticBean.getPackages().getAccess().intValue() <= Package.PackageAccess.DEFAULT.getId()) {
                    sheet.addCell(new Number(colu, row + pack, statisticBean.getTotalpackages(), reportStylesUtil.getRegularBorderStyleRight()));
                } else {
                    sheet.addCell(new Number(colu, row + pack, statisticBean.getTotalPictures(), reportStylesUtil.getRegularBorderStyleRight()));
                }

                colu++;
                sheet.addCell(new Number(colu, row + pack, statisticBean.getParticipantNumber(), reportStylesUtil.getRegularBorderStyleRight()));
                colu++;
                sheet.addCell(new Label(colu, row + pack, statisticBean.getParticipantNumber() != 0 ?
                        String.valueOf(statisticBean.getParticipantNumber().floatValue() / statisticBean.getTotalpackages().floatValue()) : "NA", reportStylesUtil.getRegularBorderStyleRight()));
                colu++;
                sheet.addCell(new Number(colu, row + pack, statisticBean.getPrintNumber(), reportStylesUtil.getRegularBorderStyleRight()));
                colu++;
                ///FORMULA FOR COUNTING REVENUE TOTAL TEMPORARY COMMENTED
                long revenue = (packPrice > 0 ? packPrice * statisticBean.getParticipantNumber() : 0) + statisticBean.getPrintNumber() * printPrice;
                sheet.addCell(new Label(colu, row + pack, "", reportStylesUtil.getBoldBorderStyle()));
                colu++;

                if (monthly) {
                    sheet.addCell(new Label(colu, row + pack, "    ", reportStylesUtil.getBoldBorderStyle()));
                    colu++;
                    sheet.addCell(new Label(colu, row + pack, "    ", reportStylesUtil.getBoldBorderStyle()));
                    colu++;
                    sheet.addCell(new Label(colu, row + pack, "    ", reportStylesUtil.getBoldBorderStyle()));
                    colu++;
                }

                colu++;
                int coluStaff = colu;
                //Staff
                for (Iterator staffIter = statisticBean.getStaff().iterator(); staffIter.hasNext(); ) {
                    StatisticStaffBean statisticStaffBean = (StatisticStaffBean) staffIter.next();

                    sheet.addCell(new Label(coluStaff, 5, statisticStaffBean.getStaff().getStaffName(), reportStylesUtil.getHeaderBorderStyleLeft()));
                    sheet.addCell(new Number(coluStaff, row + pack, statisticStaffBean.getParticipantSum(), reportStylesUtil.getRegularBorderStyleRight()));
                    coluStaff++;
                }
                pack++;
            }
            row = row + statisticBeanlist.size();
        }
        row += 2;
        //Total
        sheet.mergeCells(1, row, 1, row + reportGeneralList.size() + 1);
        sheet.addCell(new Label(1, row, "Totals", reportStylesUtil.getBoldBorderCenterStyle()));
        int pack = 0;
        long totalRevenue = 0;
        Map<Integer, Integer> totalStaff = new HashMap<Integer, Integer>();
        StatisticBean statisticBean = null;
        for (Iterator stListitem = reportGeneralList.iterator(); stListitem.hasNext(); ) {
            statisticBean = (StatisticBean) stListitem.next();

            int colu = 2;
            if (monthly) {
                sheet.addCell(new Label(colu, row + pack, "-", reportStylesUtil.getRegularBorderStyleRight()));
                colu++;
                sheet.addCell(new Label(colu, row + pack, titleStart.replaceAll("\\/", "\\."), reportStylesUtil.getRegularBorderStyleRight()));
                colu++;
                sheet.addCell(new Label(colu, row + pack, titleEnd.replaceAll("\\/", "\\."), reportStylesUtil.getRegularBorderStyleRight()));
                colu++;

            } else {
                sheet.addCell(new Label(colu, row + pack, keyDate.substring(0, 2), reportStylesUtil.getRegularBorderStyleRight()));
                colu++;
                sheet.addCell(new Label(colu, row + pack, keyDate.replaceAll("\\/", "\\."), reportStylesUtil.getRegularBorderStyleRight()));
                colu++;
            }

            sheet.addCell(new Label(colu, row + pack, locationName, reportStylesUtil.getHeaderBorderStyleLeft()));
            colu++;
            sheet.addCell(new Label(colu, row + pack, statisticBean.getPackages().getPackageName(), reportStylesUtil.getHeaderBorderStyleLeft()));
            colu++;
            int packPrice = getPackagePrice(statisticBean.getPackages());
            sheet.addCell(new Number(colu, row + pack, (packPrice > 0 ? packPrice : 0), reportStylesUtil.getHeaderBorderDollarStyle()));
            colu++;

            if (statisticBean.getPackages().getAccess().intValue() <= Package.PackageAccess.DEFAULT.getId()) {
                sheet.addCell(new Number(colu, row + pack, (statisticBean.getTotalpackages()), reportStylesUtil.getHeaderBorderStyleRight()));
            } else {
                sheet.addCell(new Number(colu, row + pack, (statisticBean.getTotalPictures()), reportStylesUtil.getHeaderBorderStyleRight()));
            }

            colu++;
            sheet.addCell(new Number(colu, row + pack, (statisticBean.getParticipantNumber()), reportStylesUtil.getHeaderBorderStyleRight()));
            colu++;
            sheet.addCell(new Label(colu, row + pack, statisticBean.getParticipantNumber() != 0 ?
                    String.valueOf(statisticBean.getParticipantNumber().floatValue() / statisticBean.getTotalpackages().floatValue()) : "NA", reportStylesUtil.getHeaderBorderStyleRight()));
            colu++;
            sheet.addCell(new Number(colu, row + pack, (statisticBean.getPrintNumber()), reportStylesUtil.getHeaderBorderStyleRight()));
            colu++;
            long revenue = (packPrice > 0 ? packPrice * statisticBean.getParticipantNumber() : 0) + statisticBean.getPrintNumber() * printPrice;
            totalRevenue = totalRevenue + revenue;
            //sheet.addCell(new Number(colu, row + pack, revenue, reportStylesUtil.getHeaderBorderDollarStyle()));
            sheet.addCell(new Label(colu, row + pack, "", reportStylesUtil.getHeaderBorderStyleRight()));
            colu++;
            if (monthly) {
                sheet.addCell(new Label(colu, row + pack, "    ", reportStylesUtil.getBoldBorderStyle()));
                colu++;
                sheet.addCell(new Label(colu, row + pack, "    ", reportStylesUtil.getBoldBorderStyle()));
                colu++;
                sheet.addCell(new Label(colu, row + pack, "    ", reportStylesUtil.getBoldBorderStyle()));
                colu++;
            }
            colu++;
            int coluStaff = colu;
            //Staff

            for (Iterator staffIter = statisticBean.getStaff().iterator(); staffIter.hasNext(); ) {
                StatisticStaffBean statisticStaffBean = (StatisticStaffBean) staffIter.next();
                sheet.addCell(new Label(coluStaff, row + pack, String.valueOf(statisticStaffBean.getParticipantSum()), reportStylesUtil.getHeaderBorderStyleRight()));
                coluStaff++;
                if (statisticBean.getPackages().getAccess().intValue() <= Package.PackageAccess.DEFAULT.getId()) {
                    if (!statisticBean.getPackages().getPackageName().equals("Mistake") && !statisticBean.getPackages().getPackageName().equals("Preview From Home")) {
                        Integer total = totalStaff.get(statisticStaffBean.getStaff().getId());
                        if (total == null) {
                            totalStaff.put(statisticStaffBean.getStaff().getId(), new Integer(statisticStaffBean.getParticipantSum().intValue()));
                        } else {
                            totalStaff.put(statisticStaffBean.getStaff().getId(), new Integer(total.intValue() + statisticStaffBean.getParticipantSum().intValue()));
                        }
                    }
                }
            }
            pack++;
        }
        row = row + reportGeneralList.size() + 1;
        int colu = 2;
        if (monthly) {
            sheet.addCell(new Label(colu, row, "-", reportStylesUtil.getRegularBorderStyleRight()));
            colu++;
            sheet.addCell(new Label(colu, row, titleStart.replaceAll("\\/", "\\."), reportStylesUtil.getRegularBorderStyleRight()));
            colu++;
            sheet.addCell(new Label(colu, row, titleEnd.replaceAll("\\/", "\\."), reportStylesUtil.getRegularBorderStyleRight()));
            colu++;

        } else {
            sheet.addCell(new Label(colu, row, keyDate.substring(0, 2), reportStylesUtil.getRegularBorderStyleRight()));
            colu++;
            sheet.addCell(new Label(colu, row, keyDate.replaceAll("\\/", "\\."), reportStylesUtil.getRegularBorderStyleRight()));
            colu++;
        }

        sheet.addCell(new Label(colu, row, locationName, reportStylesUtil.getHeaderBorderStyleLeft()));
        colu = colu + 7;
        //sheet.addCell(new Number(colu, row, totalRevenue, reportStylesUtil.getHeaderBorderDollarStyle()));
        sheet.addCell(new Label(colu, row, "", reportStylesUtil.getHeaderBorderStyleRight()));

        colu = colu + 2;
        if (monthly) {
            sheet.addCell(new Label(colu, row, "    ", reportStylesUtil.getRegularStyle()));
            colu++;
            sheet.addCell(new Label(colu, row, "    ", reportStylesUtil.getRegularStyle()));
            colu++;
            sheet.addCell(new Label(colu, row, "    ", reportStylesUtil.getRegularStyle()));
            colu++;
        }
        if (statisticBean != null) {
            for (Iterator staffIter = statisticBean.getStaff().iterator(); staffIter.hasNext(); ) {
                StatisticStaffBean statisticStaffBean = (StatisticStaffBean) staffIter.next();
                sheet.addCell(new Number(colu, row, totalStaff.get(statisticStaffBean.getStaff().getId()), reportStylesUtil.getHeaderBorderStyleRight()));
                colu++;
            }
        }
    }

    public void setReportHash(SortedMap reportHash) {
        this.reportHash = reportHash;
    }

    public void setReportGeneralList(List reportGeneralList) {
        this.reportGeneralList = reportGeneralList;
    }

    public void setStatisticList(List<Statistic> statisticList) {
        this.statisticList = statisticList;
    }

}
