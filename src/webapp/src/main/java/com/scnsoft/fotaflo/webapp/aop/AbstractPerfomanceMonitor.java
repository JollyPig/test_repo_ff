package com.scnsoft.fotaflo.webapp.aop;

import com.scnsoft.fotaflo.common.analytics.ga.Tracker;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

abstract public class AbstractPerfomanceMonitor {
    protected Logger logger = Logger.getLogger("perfomanceMonitor");

    @Autowired
    private Tracker tracker;

    public Object monitor(ProceedingJoinPoint point){
        Object value = null;
        try {
            value = point.proceed();
        }catch (Throwable e) {
            logger.error("Error has been thrown by " + point.getSignature().toShortString(), e);
            throw new RuntimeException(e);
        }
        return value;
    }

    protected void logLoadTime(String action, String label, int loadTime){
        if (logger.isTraceEnabled()){
            logger.trace("Load time for " +  action + ": " + loadTime);
        }
        tracker.trackEvent(SecurityContextHolder.getContext().getAuthentication().getName(), "load_time", action,
                label, loadTime);
    }

}
