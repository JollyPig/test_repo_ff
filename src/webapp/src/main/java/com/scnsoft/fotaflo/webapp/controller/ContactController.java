package com.scnsoft.fotaflo.webapp.controller;

import com.scnsoft.fotaflo.webapp.service.ContactService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 23.12.2011
 * Time: 17:57:15
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class ContactController {

    private final static String successProperty = "success";
    private final static String msgProperty = "msg";
    protected static Logger logger = Logger.getLogger(ContactController.class);

    @Autowired
    private ContactService contactService;


    @RequestMapping("sendContactEmail.json")
    public
    @ResponseBody
    Map<String, Object> createContactInfo(
            @RequestParam("area") String area, @RequestParam("email") String email,
            @RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName, @RequestParam("phone") String phone) {
        Map<String, Object> modelMap = createModelMap(true);
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        boolean result = contactService.sendEmail(firstName, lastName, email, area, currentUser, phone);
        Integer success = result ? new Integer(1) : new Integer(0);
        contactService.saveContact(firstName, lastName, email, area, phone, currentUser, success);
        if (result) {
            modelMap.put(msgProperty, "Your comment was sent successfully");
            logger.info("User [ " + currentUser + " ]" + "send contact MESSAGE < " + area + " > successfully");
        } else {
            modelMap.put(msgProperty, "Your comment was not sent successfully");
            logger.warn("User [ " + currentUser + " ]" + "send contact MESSAGE < " + area + " > NOT SUCCESSFULLY");
        }
        return modelMap;
    }

    @RequestMapping("saveInfoEmail.json")
    public
    @ResponseBody
    Map<String, Object> updateContactMail(
            @RequestParam("email") String email) {
        Map<String, Object> modelMap = createModelMap(true);
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        List emails = Arrays.asList(email.split(","));
        boolean result = contactService.saveEmail(emails, currentUser);
        if (result) {
            modelMap.put(msgProperty, "The email was saved successfully");
            logger.info("User [ " + currentUser + " ]" + "saved contact email information successfully");
        } else {
            modelMap.put(msgProperty, "Failed to save the email");
            logger.warn("User [ " + currentUser + " ]" + " FAILED to save contact email information");
        }
        return modelMap;
    }

    @RequestMapping("getInfoEmail.json")
    public
    @ResponseBody
    Map<String, Object> getReportPath() {

        Map<String, Object> modelMap = createModelMap(true);
        String email = contactService.getInfoEmail();
        modelMap.put("email", email != null ? email : "");
        modelMap.put(msgProperty, "Emails was retrieved successfully");

        return modelMap;
    }

    @RequestMapping("contactEmailSettingsSave.json")
    public
    @ResponseBody
    Map<String, Object> reportEmailSettingsSave(
            @RequestParam("smtpServer") String smtpServer,
            @RequestParam("fromEmail") String fromEmail, @RequestParam("subjectText") String subjectText,
            @RequestParam("smtpServerPort") String smtpServerPort,
            @RequestParam("smtpServerFromPassword") String smtpServerFromPassword, @RequestParam(value = "sslcheck", required = false) String sslcheck) {
        Map<String, Object> modelMap = createModelMap(true);
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        contactService.saveEmailContactSettings(smtpServer, smtpServerPort, fromEmail, smtpServerFromPassword, subjectText, sslcheck);
        modelMap.put(msgProperty, "Contact Settings were updated successfully");
        logger.warn("User [ " + currentUser + " ]" + " saved contact setting info");
        return modelMap;
    }

    @RequestMapping("contactEmailSettingsDelete.json")
    public
    @ResponseBody
    Map<String, Object> reportEmailSettingsDelete() {
        Map<String, Object> modelMap = createModelMap(true);
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        contactService.deleteEmailContactSettings();
        modelMap.put(msgProperty, "Contact Settings were deleted successfully");
        logger.warn("User [ " + currentUser + " ]" + " deleted contact setting info");
        return modelMap;
    }


    @RequestMapping("getEmailContactSettings.json")
    public
    @ResponseBody
    Map<String, Object> getEmailContactSettings() {

        //Map<String, Object> modelMap = createModelMap(true);
        Map<String, Object> sets = contactService.getEmailContactSettings();
        return sets;
    }

    private Map<String, Object> createModelMap(Boolean success) {
        Map<String, Object> modelMap = new HashMap<String, Object>();
        modelMap.put(successProperty, success);
        return modelMap;
    }
}