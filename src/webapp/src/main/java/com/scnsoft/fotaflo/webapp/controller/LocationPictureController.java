package com.scnsoft.fotaflo.webapp.controller;

import com.scnsoft.fotaflo.common.util.JsonMapper;
import com.scnsoft.fotaflo.webapp.bean.LocationImageBean;
import com.scnsoft.fotaflo.webapp.model.Picture;
import com.scnsoft.fotaflo.webapp.service.PictureService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 12.05.14
 * Time: 11:55
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class LocationPictureController {
    protected static Logger logger = Logger.getLogger(MainLogoController.class);

    private final static String successProperty = "success";
    private final static String msgProperty = "msg";
    private PictureService pictureService;

    @RequestMapping("addLocationImage.json")
    public
    @ResponseBody
    ResponseEntity<String> addLocationImage(@RequestParam("locationIdImg") Integer locationId, @RequestParam("imageLogo") MultipartFile imageLogo) {
        Map<String, Object> modelMap = createModelMap(true);
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        try {
            pictureService.addLocationPictures(locationId, imageLogo, currentUser);
            modelMap.put(msgProperty, "Picture was added successfully");
            modelMap.put(successProperty, true);
            logger.info("User [ " + currentUser + " ]" + " updated main image logo for location with id " + locationId);
        } catch (IOException e) {
            modelMap = createModelMap(false);
            modelMap.put(msgProperty, "Cannot add image to location");
            modelMap.put(successProperty, false);
            logger.error("[ " + currentUser + " ]" + " was not able to update main logo for locationId " + locationId);
        }

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("Content-Type", "text/html; charset=utf-8");
        return new ResponseEntity<String>(JsonMapper.getJson(modelMap), responseHeaders, HttpStatus.CREATED);
    }

    @RequestMapping("getLocationImages.json")
    public
    @ResponseBody
    Map<String, ? extends Object> getLocationImage(@RequestParam("locationId") Integer locationId) {
        Map<String, Object> modelMap = createModelMap(true);
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        List<Picture> locationAddPictures = new ArrayList<Picture>();
        try {
            locationAddPictures = pictureService.getLocationPictures(locationId);
        } catch (IOException e) {
            modelMap = createModelMap(false);
            modelMap.put(msgProperty, "Cannot add image to location");
            logger.error("[ " + currentUser + " ]" + " was not able to update pictures for locationId " + locationId);
            modelMap.put("pictures", locationAddPictures);
            return modelMap;
        }
        logger.info("User [ " + currentUser + " ]" + " updated main image logo for location with id " + locationId);
        modelMap.put(msgProperty, "Picture was added successfully");
        List<LocationImageBean> result = new ArrayList<LocationImageBean>();
        for (Picture pic : locationAddPictures) {
            result.add(new LocationImageBean(pic, locationId));
        }
        modelMap.put("pictures", result);
        return modelMap;
    }

    @RequestMapping("deleteLocationImage.json")
    public
    @ResponseBody
    Map<String, Object> deleteImageLogo(@RequestParam("pictures") Integer locationIdImg) {
        Map<String, Object> modelMap = createModelMap(true);
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        try {
            pictureService.deleteLocationPicture(locationIdImg);
        } catch (IOException e) {
            modelMap = createModelMap(false);
            logger.error("User [ " + currentUser + " ]" + " was not able to delete image by id " + locationIdImg, e);
            modelMap.put(msgProperty, "Cannot delele main image logo");
            return modelMap;
        }
        logger.info("User [ " + currentUser + " ]" + " deleted main image  for location with id " + locationIdImg);
        modelMap.put(msgProperty, "Main image logo was deleted successfully");
        return modelMap;
    }

    private Map<String, Object> createModelMap(Boolean success) {
        Map<String, Object> modelMap = new HashMap<String, Object>();
        modelMap.put(successProperty, success);
        return modelMap;
    }

    @Autowired
    public void setPictureService(PictureService pictureService) {
        this.pictureService = pictureService;
    }

}
