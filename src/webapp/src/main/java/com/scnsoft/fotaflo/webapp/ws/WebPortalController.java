package com.scnsoft.fotaflo.webapp.ws;

import com.scnsoft.fotaflo.webapp.bean.UserBeanExtended;
import com.scnsoft.fotaflo.webapp.controller.MainController;
import com.scnsoft.fotaflo.webapp.model.SystemUser;
import com.scnsoft.fotaflo.webapp.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */
@Controller
@RequestMapping("/web/users")
public class WebPortalController {
    protected static Logger logger = Logger.getLogger(MainController.class);

    @Autowired
    private UserService userService;

    @RequestMapping(value = "{name}", method = RequestMethod.GET)
    public
    @ResponseBody
    UserBeanExtended getShopInJSON(@PathVariable String name) {
        SystemUser user = userService.getUserByLogin(name);

        String currency = "";
        String price = "";
        String packagePrice = "";

        if (user != null && user.getLocation() != null && user.getLocation().getPaypallocationSettings() != null) {
            price = user.getLocation().getPaypallocationSettings().getPrice();
            currency = user.getLocation().getPaypallocationSettings().getCurrency();
            packagePrice = user.getLocation ().getPaypallocationSettings ().getPackagePrice ();
        }

        UserBeanExtended userBean = null;
        if (user != null) {
            userBean = new UserBeanExtended(user.getId(), user.getLoginName(), user.getPassword(), user.getFirstName(), user.getLastName(), String.valueOf(user.getLocation().getId()), null, user.getAccess(), currency, price, packagePrice);
        }

        return userBean;
    }

}
