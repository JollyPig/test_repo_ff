package com.scnsoft.fotaflo.webapp.common.pictures;

import com.scnsoft.fotaflo.webapp.bean.PurchasePictureBean;
import com.scnsoft.fotaflo.webapp.common.PagingCollectionResponse;

import java.util.Collection;

/**
 *
 * @author Anatoly Selitsky
 *
 */
public class PicturesResponse extends PagingCollectionResponse<PurchasePictureBean> {

    private int requestId;

    public PicturesResponse(int total, int pageSize, int requestId) {
        super(total, pageSize);
        this.requestId = requestId;
    }

    public PicturesResponse(Collection<PurchasePictureBean> children, int total, int pageSize, int requestId) {
        super(children, total, pageSize);
        this.requestId = requestId;
    }

    public PicturesResponse(Collection<PurchasePictureBean> children, boolean success, int total, int pageSize, int requestId) {
        super(children, success, total, pageSize);
        this.requestId = requestId;
    }

    public PicturesResponse(int total, int pageSize) {
        super(total, pageSize);
    }

    public PicturesResponse(Collection<PurchasePictureBean> children, int total, int pageSize) {
        super(children, total, pageSize);
    }

    public PicturesResponse(Collection<PurchasePictureBean> children, int total, int pageSize, boolean success) {
        super(children, success, total, pageSize);
    }

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

}
