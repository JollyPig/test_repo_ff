package com.scnsoft.fotaflo.webapp.dao.impl;

import com.scnsoft.fotaflo.webapp.bean.StatisticBean;
import com.scnsoft.fotaflo.webapp.bean.StatisticStaffBean;
import com.scnsoft.fotaflo.webapp.dao.IReportDao;
import com.scnsoft.fotaflo.webapp.model.*;
import com.scnsoft.fotaflo.webapp.model.Package;

import java.util.*;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */
public class ReportDao extends BaseDao implements IReportDao {

    public List<Location> getLocations() {
        return getHibernateTemplate().find(
                "from Location location");
    }

    public List<Package> getLocationsByPackage(Location location) {
        return getHibernateTemplate().find(
                "from Package packages where packages.location = ?", location);
    }

    public List<Staff> getStaffByLocation(Location location) {
        return getHibernateTemplate().find(
                "from Staff staff where staff.location = ?", location);
    }

    public List<Statistic> getStatisticByLocationForThePeriod(Location location, Date startDate, Date endDate) {
        List<Statistic> notifications = new ArrayList<Statistic>();
        List<Package> listPackages = getLocationsByPackage(location);
        for (Package pack : listPackages) {
            StringBuffer query = new StringBuffer();
            query.append("select statistic from Statistic as statistic where ");
            query.append("statistic.packages  = :package ");
            query.append("and statistic.creationDate >= :start ");
            query.append("and statistic.creationDate <= :end ");

            List list1 = getSession().createQuery(query.toString())
                    .setParameter("package", pack)
                    .setParameter("start", startDate)
                    .setParameter("end", endDate)
                    .list();
            notifications.addAll(list1);
        }
        return notifications;
    }

    public List<StatisticBean> findExtendedPackages(Date startDate, Date endDate, Location location) { // todo refactor!
        List<StatisticBean> list = new ArrayList<StatisticBean>();
        //String queryPackages = "select packages from Package as packages where packages.location="+location;
        // List listPackages = getSession().createQuery(queryPackages).list();
        List listPackages = getLocationsByPackage(location);
        Iterator itPackages = listPackages.iterator();
        while (itPackages.hasNext()) {
            com.scnsoft.fotaflo.webapp.model.Package pack = (com.scnsoft.fotaflo.webapp.model.Package) itPackages.next();
            StringBuffer query2 = new StringBuffer();
            query2.append("select count(statistic.packages), statistic.packages, statistic.creationDate, sum(statistic.numberOfParticipantAll), sum(statistic.numberToPrintAll) , sum(statistic.totalPictureNumber)");
            query2.append(" from Statistic as statistic where ");
            query2.append("statistic.creationDate >= :start ");
            query2.append("and statistic.creationDate <= :end and statistic.packages =:pack group by statistic.packages");

            StringBuffer tagApprovalQuery = new StringBuffer();
            tagApprovalQuery.append("select count(tagPurchase.purchasePackage), tagPurchase.purchasePackage, tagPurchase.creationDate, sum(tagPurchase.participants)");
            tagApprovalQuery.append(" from TagPurchase as tagPurchase where ");
            tagApprovalQuery.append("tagPurchase.creationDate >= :start ");
            tagApprovalQuery.append("and tagPurchase.creationDate <= :end and tagPurchase.purchasePackage =:pack group by tagPurchase.purchasePackage");

            List listPack = getSession().createQuery(query2.toString())
                    .setParameter("start", startDate)
                    .setParameter("end", endDate)
                    .setParameter("pack", pack)
                    .list();

            List tagApprovalListPack = getSession().createQuery(tagApprovalQuery.toString())
                    .setParameter("start", startDate)
                    .setParameter("end", endDate)
                    .setParameter("pack", pack)
                    .list();

            Object[] array = null,
                    tagsPacks = null;
            //if statistic exists for the current package
            if (listPack.size() != 0 || tagApprovalListPack.size() != 0) {
                if (listPack.size() > 0) {
                    array = (Object[]) listPack.get(0);
                }
                if (tagApprovalListPack.size() > 0) {
                    tagsPacks = (Object[]) tagApprovalListPack.get(0);
                }
                Long totalPackages = 0L, participants = 0L, printed = 0L, totalPictures = 0L;
                com.scnsoft.fotaflo.webapp.model.Package packageEntity = null;
                Date creationDate = null;
                if (tagsPacks != null) {
                    totalPackages += (Long) tagsPacks[0];
                    packageEntity = (com.scnsoft.fotaflo.webapp.model.Package) tagsPacks[1];
                    creationDate = (Date) tagsPacks[2];
                    participants += (Long) tagsPacks[3];
                }
                if (array != null) {
                    totalPackages += (Long) array[0];
                    packageEntity = (com.scnsoft.fotaflo.webapp.model.Package) array[1];
                    creationDate = (Date) array[2];
                    participants += (Long) array[3];
                    printed += (Long) array[4];
                    totalPictures += (Long) array[5];
                }
                StatisticBean statisticBean = new StatisticBean();
                statisticBean.setTotalpackages(totalPackages);
                statisticBean.setPackages(packageEntity);
                statisticBean.setDate(creationDate);
                statisticBean.setParticipantNumber(participants);
                statisticBean.setPrintNumber(printed);
                statisticBean.setTotalPictures(totalPictures);

                Map<Integer, StatisticStaffBean> statisticStaffBeanMap = getStaffEmptyStatistics(location);

                StringBuffer queryStaff = new StringBuffer();
                queryStaff.append("select staff, count(staff),sum(statistic.numberOfParticipantAll)");
                queryStaff.append(" from  Statistic as statistic join statistic.staff as staff where ");
                queryStaff.append("statistic.creationDate >= :start and statistic.creationDate <= :end and statistic.packages = :pack group by staff");
                List listStaff = getSession().createQuery(queryStaff.toString())
                        .setParameter("start", startDate)
                        .setParameter("end", endDate)
                        .setParameter("pack", pack)
                        .list();

                statisticStaffBeanMap = addStaffStatistics(listStaff, statisticStaffBeanMap);

                queryStaff = new StringBuffer();
                queryStaff.append("select staff, count(staff),sum(statistic.participants)");
                queryStaff.append(" from TagPurchase as statistic join statistic.staffList as staff where ");
                queryStaff.append("statistic.creationDate >= :start and statistic.creationDate <= :end and statistic.purchasePackage = :pack group by staff");
                listStaff = getSession().createQuery(queryStaff.toString())
                        .setParameter("start", startDate)
                        .setParameter("end", endDate)
                        .setParameter("pack", pack)
                        .list();

                statisticStaffBeanMap = addStaffStatistics(listStaff, statisticStaffBeanMap);


                List<StatisticStaffBean> StatisticStaffBeanlist = new ArrayList<StatisticStaffBean>(statisticStaffBeanMap.values());

                //Here we need to sort Staff!!!
                Collections.sort(StatisticStaffBeanlist);
                statisticBean.setStaff(StatisticStaffBeanlist);
                list.add(statisticBean);
            } else {
                StatisticBean statisticBean = new StatisticBean();
                statisticBean.setTotalpackages(0L);
                statisticBean.setPackages((com.scnsoft.fotaflo.webapp.model.Package) pack);
                statisticBean.setDate(startDate);
                statisticBean.setParticipantNumber(0L);
                statisticBean.setPrintNumber(0L);
                statisticBean.setTotalPictures(0L);

                List<StatisticStaffBean> StatisticStaffBeanlist = new ArrayList<StatisticStaffBean>();
                /*   String queryAllStaff = "select staff from Staff as staff";
             List listAllStaff = getSession().createQuery(queryAllStaff).list();*/
                List<Staff> listAllStaff = getStaffByLocation(location);
                Iterator itStaff = listAllStaff.iterator();
                while (itStaff.hasNext()) {
                    Staff staffItem = (Staff) itStaff.next();
                    StatisticStaffBean statisticStaffBean = new StatisticStaffBean();
                    statisticStaffBean.setStaff(staffItem);
                    statisticStaffBean.setStaffnumber(0L);
                    statisticStaffBean.setParticipantSum(0L);
                    StatisticStaffBeanlist.add(statisticStaffBean);
                }
                Collections.sort(StatisticStaffBeanlist);
                statisticBean.setStaff(StatisticStaffBeanlist);
                list.add(statisticBean);
            }
        }
        return list;
    }

    protected Map<Integer, StatisticStaffBean> addStaffStatistics(List staffs, Map<Integer, StatisticStaffBean> result){
        if(result == null){
            result = new HashMap<Integer, StatisticStaffBean>();
        }

        if(staffs != null && !staffs.isEmpty()){
            StatisticStaffBean staffBean;
            Staff staff;
            Long staffNumber, participantNumber;
            Object[] staffItem;
            for (Object item: staffs){
                staffItem = (Object[])item;
                staff = (Staff)staffItem[0];
                staffNumber = (Long)staffItem[1];
                participantNumber = (Long)staffItem[2];

                if(staff != null){
                    staffBean = result.get(staff.getId());
                    if(staffBean != null){
                        if(staffNumber != null && staffNumber > 0){
                            staffBean.setStaffnumber(staffBean.getStaffnumber() + staffNumber);
                        }
                        if(participantNumber != null && participantNumber > 0){
                            staffBean.setParticipantSum(staffBean.getParticipantSum() + participantNumber);
                        }
                    }
                }
            }
        }

        return result;
    }

    protected Map<Integer, StatisticStaffBean> getStaffEmptyStatistics(Location location){
        Map<Integer, StatisticStaffBean> result = new HashMap<Integer, StatisticStaffBean>();

        List<Staff> staffs = getStaffByLocation(location);

        if(staffs != null && !staffs.isEmpty()){
            StatisticStaffBean staffBean;
            for (Staff staff: staffs){
                staffBean = new StatisticStaffBean();
                staffBean.setStaff(staff);
                staffBean.setStaffnumber(0L);
                staffBean.setParticipantSum(0L);
                result.put(staff.getId(), staffBean);
            }
        }

        return result;
    }

    public List<PublicStatistic> getPublicStatisticByLocation(Date startDate, Date endDate, Location location) {
        StringBuffer query = new StringBuffer();
        query.append("select  publicStatistic from PublicStatistic as publicStatistic where ");
        query.append("publicStatistic.location  = :location ");
        query.append("and publicStatistic.creationDate >= :start ");
        query.append("and publicStatistic.creationDate <= :end ");

        List list1 = getSession().createQuery(query.toString())
                .setParameter("location", location)
                .setParameter("start", startDate)
                .setParameter("end", endDate)
                .list();
        return list1;
    }

    public List<ReportEmails> getReportEmails(Date startDate, Date endDate, Location location) {
        StringBuffer query = new StringBuffer();
        query.append("select  reportEmails from ReportEmails as reportEmails where ");
        query.append("reportEmails.location  = :location ");
        query.append("and reportEmails.creationDate >= :start ");
        query.append("and reportEmails.creationDate <= :end ");

        List list1 = getSession().createQuery(query.toString())
                .setParameter("location", location)
                .setParameter("start", startDate)
                .setParameter("end", endDate)
                .list();
        return list1;
    }

}
