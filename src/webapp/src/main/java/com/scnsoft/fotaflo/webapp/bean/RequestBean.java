package com.scnsoft.fotaflo.webapp.bean;

import java.util.Date;

/**
 * Created by Nadezda Drozdova
 * Date: Apr 27, 2011
 * Time: 8:56:50 PM
 */
public class RequestBean {

    protected int id;
    String emails;
    String comments;
    Date creationDate;
    Integer totalToPrint;
    Integer totalToSend;

    public RequestBean() {
    }

    public RequestBean(int id, String emails, String comments, Date creationDate, Integer totalToPrint, Integer totalToSend) {
        this.id = id;
        this.emails = emails;
        this.comments = comments;

        this.creationDate = creationDate;
        this.totalToPrint = totalToPrint;
        this.totalToSend = totalToSend;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmails() {
        return emails;
    }

    public void setEmails(String emails) {
        this.emails = emails;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Integer getTotalToPrint() {
        return totalToPrint;
    }

    public void setTotalToPrint(Integer totalToPrint) {
        this.totalToPrint = totalToPrint;
    }

    public Integer getTotalToSend() {
        return totalToSend;
    }

    public void setTotalToSend(Integer totalToSend) {
        this.totalToSend = totalToSend;
    }
}
