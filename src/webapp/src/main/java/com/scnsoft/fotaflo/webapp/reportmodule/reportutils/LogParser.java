package com.scnsoft.fotaflo.webapp.reportmodule.reportutils;

import com.scnsoft.fotaflo.webapp.reportmodule.model.PictureObj;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 20.08.2012
 * Time: 9:38:36
 * To change this template use File | Settings | File Templates.
 */
public class LogParser {


    String fileName;
    Map<String, PictureObj> pictureMap;
    String fileDate;
    int nullsizable = 0;
    List<String> pictures = new ArrayList<String>();


    int counter = 0;
    int counterfalse = 0;
    int sizebefore = 0;
    int devicelocation = 0;
    int timezone = 0;
    int important = 0;
    int alreadyexist = 0;
    int synchronizedError = 0;
    int expectednotequal = 0;


    public LogParser(String fileName, Map<String, PictureObj> pictureMap, String fileDate) {
        this.fileName = fileName;
        this.pictureMap = pictureMap;
        this.fileDate = fileDate;
    }


    public void analyze() {
        {
            try {
                FileInputStream fstream = new FileInputStream(fileName);
                DataInputStream in = new DataInputStream(fstream);
                BufferedReader br = new BufferedReader(new InputStreamReader(in));
                String strLine;
                //Read File Line By Line
                while ((strLine = br.readLine()) != null) {
                    // Print the content on the console
                    parseLine(strLine);
                }
                //Close the input stream
                in.close();
            } catch (Exception e) {//Catch exception if any
                System.err.println("Error: " + e.getMessage());
            }
        }
    }

    public void parseLine(String line) {
        Pattern p3 = Pattern.compile("(\\[\\s*[\\w\\s]+\\$[\\w\\s]+\\$(\\w+_)?\\d+.+\\.jpg\\s*\\])");
        Matcher m3 = p3.matcher(line);
        if (m3.find()) {
            String name = m3.group(1);
            PictureObj picture = pictureMap.get(name);
            if (picture == null) {
                picture = new PictureObj();
            }
            if (!line.contains("!!!!!!!!!!EXPECTED SIZE 0!!!!!!!!!!!")) {
                pictureMap.put(name, getInfo(line, picture));
            } else {
                nullsizable++;
            }
        }
    }

    public PictureObj getInfo(String line, PictureObj picture) {

        //20246952 23 Jul 2012 10:52:37 [http-8080-10] INFO  com.scnsoft.fotaflo.webapp.controller.UploadController  -[ 2012-07-22_11-02-06_648.jpg ] UPLOAD INFO: Filename 2012-07-22_11-02-06_648.jpg number X received. Content length 1326859
        Pattern p0 = Pattern.compile("number .+ received");
        Matcher m0 = p0.matcher(line);
        if (m0.find()) {
            int counter = picture.getCounter();
            counter++;
            picture.setCounter(counter);
        }
        Pattern p00 = Pattern.compile("Filename\\s+(.+)\\s+number");
        Matcher m00 = p00.matcher(line);
        if (m00.find()) {
            String name = m00.group(1);
            picture.setFileName(name);
            if (!name.contains(fileDate) && !pictures.contains(name)) {
                pictures.add(name);
            }
                            /*if(!pictures.contains(picture.getFileName())){
                                pictures.add(picture.getFileName());
                            }*/

        }
        if (picture.getFirstUploadTime() == null) {
            Pattern p000 = Pattern.compile("\\d+\\s+(.+)\\s+\\[http");
            Matcher m000 = p000.matcher(line);
            if (m000.find()) {
                String timeUpload = m000.group(1);
                picture.setFirstUploadTime(timeUpload);
            }
        }

        //20246952 23 Jul 2012 10:52:37 [http-8080-10] INFO  com.scnsoft.fotaflo.webapp.controller.UploadController  -[ 2012-07-22_11-02-06_648.jpg ] PICTURES SIZE BEFORE: 2
        Pattern p1 = Pattern.compile("PICTURES SIZE BEFORE:\\s+(\\d+)");
        Matcher m1 = p1.matcher(line);
        if (m1.find()) {
            String size = m1.group(1);
            picture.setBeforeNumber(Integer.parseInt(size));
            sizebefore++;
            return picture;
        }

        //20246952 23 Jul 2012 10:52:37 [http-8080-10] INFO  com.scnsoft.fotaflo.webapp.controller.UploadController  -[ 2012-07-22_11-02-06_648.jpg ] UPLOAD INFO: Filename 2012-07-22_11-02-06_648.jpg number X received. Content length 1326859
        //20246952 23 Jul 2012 10:52:37 [http-8080-10] INFO  com.scnsoft.fotaflo.webapp.controller.UploadController  -[ 2012-07-22_11-02-06_648.jpg ] UPLOAD INFO: deviceId: Dogwood location: ZipQuest
        Pattern p2 = Pattern.compile("deviceId:\\s+(.+)\\s+location:\\s+(.+)");
        Matcher m2 = p2.matcher(line);
        if (m2.find()) {
            String camera = m2.group(1);
            picture.setCameraId(camera);
            String location = m2.group(2);
            picture.setLocationName(location);
            devicelocation++;
            return picture;
        }
        //20246953 23 Jul 2012 10:52:37 [http-8080-10] INFO  com.scnsoft.fotaflo.webapp.controller.UploadController  -[ 2012-07-22_11-02-06_648.jpg ] UPLOAD INFO: Received time: 1342969326000 timezone: America/New_York
        Pattern p3 = Pattern.compile("timezone:\\s+(.+)");
        Matcher m3 = p3.matcher(line);
        if (m3.find()) {
            String timeZone = m3.group(1);
            picture.setTimeZone(timeZone);
            timezone++;
            return picture;
        }
        //20246953 23 Jul 2012 10:52:37 [http-8080-10] INFO  com.scnsoft.fotaflo.webapp.controller.UploadController  -[ 2012-07-22_11-02-06_648.jpg ] UPLOAD INFO: TIME OF THE PICTURE [IMPORTANT]: Sun Jul 22 11:02:06 EDT 2012
        Pattern p4 = Pattern.compile("\\[IMPORTANT\\]:\\s+(.+)");
        Matcher m4 = p4.matcher(line);
        if (m4.find()) {
            String timeReal = m4.group(1);
            picture.setReceivedFromPhoneTime(timeReal);
            important++;
            return picture;
        }
        //20246953 23 Jul 2012 10:52:37 [http-8080-10] INFO  com.scnsoft.fotaflo.webapp.controller.UploadController  -[ 2012-07-22_11-02-06_648.jpg ] SYNCHRONIZED UPLOAD WARN: file 2012-07-22_11-02-06_648.jpgseems already exist and tries to be rewritten incorrectly
        Pattern p5 = Pattern.compile("seems already exist and tries to be rewritten incorrectly");
        Matcher m5 = p5.matcher(line);
        if (m5.find()) {
            picture.setAlreadyExistWarn(true);
            alreadyexist++;
            return picture;
        }

        //20348483 23 Jul 2012 10:54:18 [http-8080-10] INFO  com.scnsoft.fotaflo.webapp.controller.UploadController  -[ 2012-07-22_11-02-06_648.jpg ] SYNCHRONIZED DELETED CAUGHT AN EXCEPTION
        //20348483 23 Jul 2012 10:54:18 [http-8080-10] ERROR com.scnsoft.fotaflo.webapp.controller.UploadController  -[ 2012-07-22_11-02-06_648.jpg ] SYNCHRONIZED ERROR DURING FILE UPLOAD: java.net.SocketTimeoutException: Read timed out
        Pattern p6 = Pattern.compile("SYNCHRONIZED ERROR DURING FILE UPLOAD:(.+)");
        Matcher m6 = p6.matcher(line);
        if (m6.find()) {
            String error = m6.group(1);
            String errorType = picture.getErrorType();
            if (errorType == null) {
                errorType = error;
            } else {
                errorType = errorType + "; " + error;
            }
            picture.setErrorType(errorType);
            synchronizedError++;
            return picture;
        }
        //20348492 23 Jul 2012 10:54:18 [http-8080-10] INFO  com.scnsoft.fotaflo.webapp.controller.UploadController  -[ 2012-07-22_11-02-06_648.jpg ] UPLOAD ERROR: File was not processed as  expectedSize != realSize
        Pattern p7 = Pattern.compile("File was not processed as  expectedSize != realSize");
        Matcher m7 = p7.matcher(line);
        if (m7.find()) {
            picture.setSuccessfull(false);
            expectednotequal++;
            return picture;
        }
        //20021055 23 Jul 2012 10:48:51 [http-8080-18] INFO  com.scnsoft.fotaflo.webapp.controller.UploadController  -[ 2012-07-22_15-00-28_851.jpg ] SYNCHRONIZED UPLOAD: 1234863 / 1234863 Loaded completely: true
        Pattern p8 = Pattern.compile("Loaded completely:\\s+(.+)");
        Matcher m8 = p8.matcher(line);
        if (m8.find()) {
            String result = m8.group(1);
            if (result.equals("true")) {
                picture.setSuccessfull(true);
                counter++;
            } else {
                picture.setSuccessfull(false);
                counterfalse++;
                String errorType = picture.getErrorType();
                if (errorType == null) {
                    errorType = "expectedSize != realSize";
                } else {
                    errorType = errorType + "; " + "expectedSize != realSize";
                }

                picture.setErrorType(errorType);
            }
            Pattern p88 = Pattern.compile("\\d+\\s+(.+)\\s+\\[http");
            Matcher m88 = p88.matcher(line);
            if (m88.find()) {
                String timeUpload = m88.group(1);
                picture.setLastUploadTime(timeUpload);
            }
            return picture;
        }

        //20021055 23 Jul 2012 10:48:51 [http-8080-18] INFO  com.scnsoft.fotaflo.webapp.controller.UploadController  -[ 2012-07-22_15-00-28_851.jpg ] UPLOAD SUCCESS: loaded successfully.
        Pattern p9 = Pattern.compile("loaded successfully");
        Matcher m9 = p9.matcher(line);
        if (m9.find()) {
            picture.setSuccessfull(true);
            return picture;

        }
        // 20021391 23 Jul 2012 10:48:51 [http-8080-18] INFO  com.scnsoft.fotaflo.webapp.controller.UploadController  -[ 2012-07-22_15-00-28_851.jpg ] UPLOAD SUCCESS FULL TOTAL!!: 195
        Pattern p10 = Pattern.compile("UPLOAD SUCCESS FULL TOTAL!!:\\s+(\\d+)");
        Matcher m10 = p10.matcher(line);
        if (m10.find()) {
            String result = m10.group(1);
            picture.setTotalTimeUpload(Integer.parseInt(result));
            return picture;
        }

        //34369912 22 Jul 2012 11:33:04 [http-8080-12] ERROR com.scnsoft.fotaflo.webapp.controller.UploadController  -[ 2012-07-21_17-05-57_55.jpg ] REJECTED AS ALREADY EXISTS IN THE PROCCESSING LIST: Lake Geneva Canopy Tours$Honeybee$2012-07-21_17-05-57_55.jpg
        Pattern p11 = Pattern.compile("REJECTED AS ALREADY EXISTS IN THE PROCCESSING LIST:");
        Matcher m11 = p11.matcher(line);
        if (m11.find()) {
            int rejected = picture.getRejected();
            rejected++;
            picture.setRejected(rejected);
            return picture;
        }
        return picture;
    }

    public int getNullsizable() {
        return nullsizable;
    }

    public void setNullsizable(int nullsizable) {
        this.nullsizable = nullsizable;
    }

    public List<String> getPictures() {
        return pictures;
    }

    public void setPictures(List<String> pictures) {
        this.pictures = pictures;
    }
}
