package com.scnsoft.fotaflo.webapp.controller;

import com.scnsoft.fotaflo.common.util.NumberUtils;
import com.scnsoft.fotaflo.common.util.StringUtils;
import com.scnsoft.fotaflo.webapp.bean.*;
import com.scnsoft.fotaflo.webapp.common.BaseResponse;
import com.scnsoft.fotaflo.webapp.common.Response;
import com.scnsoft.fotaflo.webapp.common.ResultList;
import com.scnsoft.fotaflo.webapp.common.pictures.PicturesResponse;
import com.scnsoft.fotaflo.webapp.model.*;
import com.scnsoft.fotaflo.webapp.service.*;
import com.scnsoft.fotaflo.webapp.util.*;
import com.scnsoft.fotaflo.webapp.web.model.GlobalSettings;
import org.apache.log4j.Logger;
import org.apache.sanselan.ImageReadException;
import org.apache.sanselan.ImageWriteException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by Nadezda Drozdova
 * Date: 06.04.2011
 */

@Controller
public class PurchaseController {
    protected static Logger logger = Logger.getLogger(PurchaseController.class);

    @Autowired
    private PictureService pictureService;

    @Autowired
    private RequestService requestService;

    @Autowired
    private UserService userService;

    @Autowired
    private ExportService exportService;

    @Autowired
    private PurchaseService purchaseService;

    @Autowired
    private LocationService locationService;

    private final static String successProperty = "success";

    private final static String msgProperty = "msg";

    private final static String subjectMessage = "Email From Fotaflo";

    private final static String bodyMessage = "Thank you for purchasing photos from Fotaflo, your pictures are attached.";

    @Value("${fotaflo.paypal.url}")
    private String payPalUrl;

    @Value("${facebook.clientId}")
    private String facebookAppId;

    @RequestMapping(value = "getPicturesByUser.json")
    public
    @ResponseBody
    PicturesResponse loadPictures(HttpServletRequest request) {
        return fetchPictures(request);
    }

    @RequestMapping(value = "getPicturesByUserUpdate.json")
    public
    @ResponseBody
    PicturesResponse updateSelectedPurchasePictures(
            @RequestParam(value = "children", required = false) Object data,
            HttpServletRequest request) {
        return fetchPictures(request, data, true);
    }

    @RequestMapping(value = "getPicturesByUserDelete.json")
    public
    @ResponseBody
    BaseResponse getHelpYourSelfPicturesDeleted(HttpServletRequest request) throws Exception {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        int requestId = FotafloUtil.parseParameterToInteger(request, "requestId", "-1");
        int purchaseId = FotafloUtil.parseParameterToInteger(request, "purchaseId", "-1");
        String pictureIds = FotafloUtil.parseParameter(request, "pictures", "EMPTY");
        if (!"EMPTY".equals(pictureIds)) {
            pictureService.deletePurchase(pictureIds, currentUser, !(requestId < 0));
        }
        logger.info(" [ " + currentUser + " ] deleted helpypurself pictures ");
        return new BaseResponse();
    }

    @RequestMapping(value = "getPurchasePicturesPageSizeUpdate.json")
    public
    @ResponseBody
    Map<String, ? extends Object> updatePageSize(HttpServletRequest httpServletRequest)
            throws Exception {

        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        String pageLimit = pictureService.getPageSize(currentUser);
        String pageSize = httpServletRequest.getParameter("pageSize") != null ? httpServletRequest.getParameter("pageSize") : pageLimit;

        HashMap modelMap = new HashMap();
        modelMap.put("pageSize", pageSize);
        logger.info(" [ " + currentUser + " ] updated purchase picture size ");
        return modelMap;
    }

    private Map<String, Boolean> getPurchaseEmails(Integer numberParticipant, HttpServletRequest request){
        Map<String, Boolean> result = getEmails(numberParticipant, request, "email", "checkemail");

        return result;
    }

    private Map<String, Boolean> getPurchaseOtherEmails(Integer numberParticipant, HttpServletRequest request){
        Map<String, Boolean> result = getEmails(numberParticipant, request, "emailOther", "checkemailOther");

        return result;
    }

    private Map<String, Boolean> getEmails(Integer number, HttpServletRequest request, String valueToken, String checkToken){
        Map<String, Boolean> result = new HashMap<String, Boolean>();
        if(number != null && request != null){
            String email, check;
            boolean checked;
            for (int i = 1; i <= number; i++) {
                email = request.getParameter(valueToken + i);
                if (!StringUtils.isEmpty(email)) {
                    check = request.getParameter(checkToken + i);
                    if(check != null && check.equals("on")){
                        checked = true;
                    }else{
                        checked = false;
                    }

                    if(result.containsKey(email) && result.get(email) != null && result.get(email)){
                        continue;
                    }

                    result.put(email, checked);
                }
            }
        }

        return result;
    }

    @RequestMapping(value = "submitPurchaseForm.json")
    public
    @ResponseBody
    Map<String, ? extends Object> submitForm(@RequestParam("iDlocationCombo") String idLocation,
                                             @RequestParam("iDpackageCombo") String idPackage,
                                             @RequestParam("iDparticipantCombo") String numberParticipant,
                                             @RequestParam("iDparticipantOtherCombo") String numberOtherParticipant,
                                             @RequestParam("iDstaffCombo") String idStaff,
                                             @RequestParam("subject") String subject,
                                             @RequestParam("strfilename") String attach_filename,
                                             @RequestParam("emailbody") String email_body,
                                             @RequestParam("requestId") String requestId,
                                             @RequestParam("purchaseId") String purchaseId,
                                             @RequestParam(value = "accessToken", required = false) String accessToken,
                                             HttpServletRequest httpServletRequest) {
        HashMap<String, Object> modelMap = new HashMap<String, Object>();

        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();

        long time1 = System.currentTimeMillis();

        logger.info("User " + currentUser + " submitted the purchase form");
        if (accessToken == null) {
            accessToken = "";
        }
        int access = pictureService.getUserAccess(currentUser);
        boolean isPublic = AccessID.isPublic(access);

        // check location
        Location locationUser = locationService.getLocationById(idLocation);
        if (locationUser == null) {
            locationUser = pictureService.getUserLocation(currentUser);
        }

        if (isPublic) {
            subject = subjectMessage;
            email_body = bodyMessage;
            if (attach_filename == null || attach_filename.isEmpty()) {
                attach_filename = locationUser.getLocationName();
            }
        }

        Integer packageId = Integer.valueOf(idPackage != null ? idPackage : "-1");

        int numberOfParticipant = numberParticipant == null ? 0 : Integer.valueOf(numberParticipant);
        int numberOfOtherParticipant = numberOtherParticipant == null ? 0 : Integer.valueOf(numberOtherParticipant);

        long time2 = System.currentTimeMillis();
        logger.info("TIME STEP 1:  " + (time2 - time1) / 1000 + " sec " + (time2 - time1) + " milisec ");

        boolean result = false;
        String errorMessage = null;

        List<PreviewPrintBean> picturesToPrint = null;
        try{
            picturesToPrint = purchaseService.doPurchase(currentUser, access, locationUser,
                    NumberUtils.getInteger(requestId), NumberUtils.getInteger(purchaseId), packageId, idStaff, numberOfParticipant,
                    subject, email_body, attach_filename, accessToken,
                    getPurchaseEmails(numberOfParticipant, httpServletRequest), getPurchaseOtherEmails(numberOfOtherParticipant, httpServletRequest));
            result = true;
        }catch (Exception e){
            errorMessage = e.getMessage();
        }

        if (result) {
            modelMap.put(successProperty, true);
            modelMap.put("logoLocation", String.valueOf(getLogoPosition(locationUser)));  // todo wtf?
            modelMap.put("mainLogoLocation", String.valueOf(getMainLogoPosition(locationUser)));         // todo wtf?
            modelMap.put("picturesToPrint", picturesToPrint);
        } else {
            modelMap.put(successProperty, false);
            modelMap.put(msgProperty, errorMessage);
        }

        modelMap.put("current", currentUser);
        modelMap.put("pageSize", pictureService.getPageSize(currentUser));
        modelMap.put("userId", pictureService.getUserAccess(currentUser));
        modelMap.put("startDate", DefaultValues.getStartDate());
        modelMap.put("endDate", DefaultValues.getEndDate());
        modelMap.put("startDay", "");
        modelMap.put("endDay", "");
        modelMap.put("startTime", "");
        modelMap.put("endTime", "");
        if (isPublic) {
            modelMap.put("exportUrl",
                    String.format("../../exportPurchasedPictures.json?currentUser=%s&requestId=%s", currentUser, requestId));
        }
        return modelMap;
    }

    private int getLogoPosition(Location location){
        String locationImgLogo = null;
        if(location != null && !StringUtils.isEmpty(location.getLocationImageLogoUrl())){
            locationImgLogo = pictureService.getPath(location.getLocationImageLogoUrl().replace("/", File.separator));
        }
        int size = ImageWatermark.getLogoSize(locationImgLogo);
        return size/2;
    }

    private int getMainLogoPosition(Location location){
        String locationImgLogo = null;
        if(location != null){
            String mainLogo = pictureService.getMainLogo(location);
            if(!StringUtils.isEmpty(mainLogo)){
                locationImgLogo = pictureService.getPath(mainLogo.replace("/", File.separator));
            }
        }
        int size = ImageWatermark.getLogoSize(locationImgLogo);
        return size/2;
    }

    @RequestMapping("exportPurchasedPictures.json")
    public void exportPurchasedPictures(@RequestParam("currentUser") String currentUser, @RequestParam("requestId") String requestId,
                                        HttpServletResponse response)
            throws ImageWriteException, IOException, ImageReadException {
        List<PurchasePictureBean> pictures = pictureService.getPurchasePictureBeansAllWater(currentUser, Integer.valueOf(requestId));
        try {
            if (pictures.size() > 0) {
                byte[] ar = exportService.createZipArchive(pictures, "photo", true);
                response.setContentType("application/zip");
                response.setHeader("Content-Encoding", "zip");
                response.setContentLength(ar.length);
                response.setHeader("Content-Disposition", "attachment;filename=\"archive.zip\"");
                response.getOutputStream().write(ar);
                response.getOutputStream().flush();
            }
            logger.warn("User [ " + currentUser + " ] " + "exported pictures during the purchase");
        } catch (IOException e) {
            logger.error("Cannot create archive with pictures: " + e.getMessage());
        } finally {
            if (!pictures.isEmpty()) {
                pictureService.clearPurchaseSelections(pictures, currentUser);
            }
        }
    }

    @RequestMapping(value = "cancelPurchaseForm.json")
    public
    @ResponseBody
    Response<GlobalSettings> cancelForm() {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();

        List<PurchasePictureBean> purchaseBeans = pictureService.getPurchasePictureBeansAllResult(currentUser, Integer.valueOf("-1"), Integer.valueOf("-1"), Integer.valueOf("-1")).getItems();
        if (!purchaseBeans.isEmpty()) {
            pictureService.clearPurchaseSelections(purchaseBeans, currentUser);
        }
        return new Response<GlobalSettings>(new GlobalSettings(
                pictureService.getPageSize(currentUser),
                DefaultValues.getStartDate(),
                DefaultValues.getEndDate(),
                pictureService.getUserAccess(currentUser),
                currentUser
        ));
    }

    @RequestMapping(value = "/main/purchase")
    public String getPurchasePage(ModelMap model, HttpServletRequest request) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User " + currentUser + " opened a purchase page");
        int requestId = FotafloUtil.parseParameterToInteger(request, "requestId", "-1");
        int purchaseId = FotafloUtil.parseParameterToInteger(request, "gen", "-1");
        int purchaseIdCat = FotafloUtil.parseParameterToInteger(request, "genCat", "-1");

        if (purchaseId == -1) {
            if (purchaseIdCat != -1) {
                purchaseId = purchaseIdCat;
            }
        }

        String fileName = pictureService.getStringFileName(currentUser);
        String staffs = pictureService.getPucrchaseStaffs(currentUser);
        int access = pictureService.getUserAccess(currentUser);
        SystemUser systemUser = userService.getUserByLogin(currentUser);
        Date userDate = systemUser.getCreationDate();
        if (access != AccessID.PUBLICEMAILUSER.getValue() && access != AccessID.PUBLICPURCHASEUSER.getValue()) {
            if (access == AccessID.PUBLICUSER.getValue() || access == AccessID.PUBLICTAGUSER.getValue()) {
                model.put("startDate", DateConvertationUtils.getStartDate(0, userDate).getTime());
                model.put("endDate", DateConvertationUtils.getEndDate(0, userDate).getTime());
                model.put("startDay", DateConvertationUtils.getDateString(DateConvertationUtils.getStartDate(0, userDate)));
                model.put("endDay", DateConvertationUtils.getDateString(DateConvertationUtils.getEndDate(0, userDate)));
                model.put("startTime", DateConvertationUtils.getTimeString(DateConvertationUtils.getStartDate(0, userDate)));
                model.put("endTime", DateConvertationUtils.getTimeString(DateConvertationUtils.getEndDate(0, userDate)));
                model.put("emails", "");
                model.put("emailCount", -1);
                model.put("email", "");
                model.put("emailsOther", "");
                model.put("emailOtherCount", 0);
                model.put("location", "");
                model.put("packages", "");
                model.put("staff", "");
            } else {
                model.put("startDate", DefaultValues.getStartDate());
                model.put("endDate", DefaultValues.getEndDate());
                model.put("startDay", "");
                model.put("endDay", "");
                model.put("startTime", "");
                model.put("endTime", "");
                List<String> emails = requestService.getEmailsFromRequest(requestId);
                model.put("emails", emails);
                model.put("emailCount", emails.size());
                model.put("emailsOther", "");
                model.put("emailOtherCount", 0);
                if (!systemUser.getAccess().equals(String.valueOf(AccessID.ADMIN.getValue()))) {
                    model.put("location", systemUser.getLocation().getId());
                } else {
                    model.put("location", "");
                }
                model.put("packages", "");
                model.put("staff", staffs);
            }
        } else {
            UserFilter userFilter = pictureService.getUserFilter(currentUser);
            model.put("startDate", userFilter.getStartDate().getTime());
            model.put("endDate", userFilter.getEndDate().getTime());
            model.put("startDay", DateConvertationUtils.getDateString(userFilter.getStartDate()));
            model.put("endDay", DateConvertationUtils.getDateString(userFilter.getEndDate()));
            model.put("startTime", DateConvertationUtils.getTimeString(userFilter.getStartDate()));
            model.put("endTime", DateConvertationUtils.getTimeString(userFilter.getEndDate()));
            model.put("emails", "");
            model.put("emailCount", -1);
            model.put("emailsOther", "");
            model.put("emailOtherCount", 0);
            model.put("location", "");
            model.put("packages", "");
            model.put("staff", "");
            model.put("email", "");
        }

        if (purchaseId != -1) {
            Purchase purchase = purchaseService.getPurchaseById(purchaseId);
            if (purchase.getStatusCode() == 2 || purchase.getStatusCode() == 3 || purchase.getStatusCode() == 4) {
                model.put("startDate", DefaultValues.getStartDate());
                model.put("endDate", DefaultValues.getEndDate());
                model.put("startDay", "");
                model.put("endDay", "");
                model.put("startTime", "");
                model.put("endTime", "");

                ArrayList<String> emails = new ArrayList<String>();
                if (purchase.getParticipantsEmails() != null && !purchase.getParticipantsEmails().equals("")) {
                    List emailss = Arrays.asList(purchase.getParticipantsEmails().split(","));
                    emails.addAll(emailss);
                }

                int emailsSize = Integer.valueOf(purchase.getParticipants());
                int realsize = emails.size();
                if (realsize < emailsSize) {
                    for (int i = 0; i < (emailsSize - realsize); i++) {
                        emails.add("");
                    }
                }
                model.put("emails", emails);
                model.put("emailCount", purchase.getParticipants());

                ArrayList<String> emailsOther = new ArrayList<String>();
                if (purchase.getParticipantsOtherEmails() != null && !purchase.getParticipantsOtherEmails().equals("")) {
                    List emailssOther = Arrays.asList(purchase.getParticipantsOtherEmails().split(","));
                    emailsOther.addAll(emailssOther);
                }
                int emailsOtherSize = Integer.valueOf(purchase.getParticipantsOther());
                int otherRealsize = emailsOther.size();
                if (otherRealsize < emailsOtherSize) {
                    for (int i = 0; i < (emailsOtherSize - otherRealsize); i++) {
                        emailsOther.add("");
                    }
                }
                model.put("emailsOther", emailsOther);
                model.put("emailOtherCount", purchase.getParticipantsOther());

                if (!systemUser.getAccess().equals(String.valueOf(AccessID.ADMIN.getValue()))) {
                    model.put("location", systemUser.getLocation().getId());
                } else {
                    model.put("location", purchase.getLocationPurchase() != null ? purchase.getLocationPurchase().getId() : "");
                }
                model.put("packages", purchase.getPurchasePackage() != null ? purchase.getPurchasePackage().getId() : "");
                model.put("staff", purchase.getStaffs());
                model.put("subject", purchase.getSubject());
                fileName = purchase.getStringFileName();

                Set<PurchasePictures> purchasePictureses = purchase.getPurchasePictures();
            }
        }

        PayPalLocationSettings payPalLocationSettings = systemUser.getLocation().getPaypallocationSettings();
        if (payPalLocationSettings != null && payPalLocationSettings.getPrice() != null && !payPalLocationSettings.getPrice().equals("")) {
            model.put("price", payPalLocationSettings.getPrice());
        } else {
            model.put("price", 0);
        }

        model.put("pageSize", pictureService.getPageSize(currentUser));
        model.put("current", currentUser);
        model.put("userId", access);
        model.put("requestId", requestId);
        model.put("fileName", fileName);
        model.put("payed", false);
        model.put("purchId", purchaseId);
        model.put("purchIdCat", purchaseIdCat);

        pictureService.createPurchasePicturesSelectionModel(currentUser, requestId);
        if (requestId == -1 && purchaseIdCat == -1) {
            String selected = purchaseService.createSelectionModelFromSaved(currentUser, purchaseId);

            model.put("selectedids", selected);
        }

        model.put("redirectpaypal", getPayPalRedirectUrl());
        model.put("facebookPage", systemUser.getLocation().getLocationToFacebookEmail());
        model.put("facebookAppId", facebookAppId);
        Location location = userService.getUserLocation(currentUser);
        boolean tagged = (location != null && location.getTagged() != null && location.getTagged() == true) ? true : false;
        model.put("tagApp", tagged);
        return "purchasepage";
    }

    @RequestMapping(value = "getPurchaseStaffForPublicUser.json")
    public
    @ResponseBody
    Map<String, ? extends Object> getPurchaseStaffForPublicUser(HttpServletRequest httpServletRequest)
            throws Exception {
        HashMap<String, Object> modelMap = new HashMap<String, Object>();
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        UserFilter userFilter = pictureService.getUserFilter(currentUser);
        String staff = userFilter.getStaff();
        modelMap.put("staff", staff);
        modelMap.put(msgProperty, "Staff is retrieved");
        logger.info(" [ " + currentUser + " ] retrieved a staff for public purchase ");
        return modelMap;
    }

    @RequestMapping("getEmailBody.json")
    public
    @ResponseBody
    Map<String, Object> getEmailBody(HttpServletRequest httpServletRequest) {
        Map<String, Object> modelMap = new HashMap<String, Object>();
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        int purchaseId = FotafloUtil.parseParameterToInteger(httpServletRequest, "purchaseId", "-1");
        String emailbody = pictureService.getEmailBody(currentUser);
        if (purchaseId != -1) {
            Purchase purchase = purchaseService.getPurchaseById(purchaseId);
            if (purchase.getStatusCode() > 1) {
                emailbody = purchase.getEmailBody();
            }
        }
        modelMap.put("emailbody", emailbody != null ? emailbody : "");
        modelMap.put(successProperty, true);
        modelMap.put(msgProperty, "Email body  was retrieved successfully");
        return modelMap;
    }

    @RequestMapping(value = "/main/previewprint")
    public String getPreviewPrintPage(ModelMap model, HttpServletRequest httpServletRequest) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        Object data = httpServletRequest.getParameter("picturesToPrint");
        Object data2 = httpServletRequest.getParameter("logoLocation");
        Object data1 = httpServletRequest.getParameter("mainLogoLocation");
        model.put("picturesToPrint", data);
        model.put("mainLogoLocation", data1);
        model.put("logoLocation", data2);
        model.put("current", currentUser);
        model.put("userId", pictureService.getUserAccess(currentUser));
        logger.info("User [ " + currentUser + " ] " + "opened print preview page");
        return "previewprintpage";
    }

    public String getPayPalRedirectUrl() {
        return payPalUrl;
    }

    private PicturesResponse fetchPictures(HttpServletRequest request) {
        return fetchPictures(request, null, false);
    }

    private PicturesResponse fetchPictures(HttpServletRequest request, Object data, boolean editable) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        String pageLimit = pictureService.getPageSize(currentUser);
        int requestId = FotafloUtil.parseParameterToInteger(request, "requestId", "-1");
        int purchaseId = FotafloUtil.parseParameterToInteger(request, "purchaseId", "-1");
        int purchaseIdCat = FotafloUtil.parseParameterToInteger(request, "purchaseIdCat", "-1");
        int deleted = FotafloUtil.parseParameterToInteger(request, "deleted", "-1");

        int start = FotafloUtil.parseParameterToInteger(request, "start", "0");
        int limit = FotafloUtil.parseParameterToInteger(request, "limit", pageLimit);
        if (editable) {
            pictureService.updatePurchasePictureBeans(data, currentUser, requestId);
        }
        ResultList<PurchasePictureBean> pictureBeanResultList = pictureService.getPurchasePictureBeansAllResult(start, limit, currentUser, requestId, purchaseId, purchaseIdCat, deleted);

        PicturesResponse response = new PicturesResponse(
                pictureBeanResultList.getItems(),
                pictureBeanResultList.getTotal().intValue(),
                limit
        );
        response.setRequestId(requestId);
        if (editable) {
            logger.info(" [ " + currentUser + " ] update selected purchase pictures ");
        } else {
            logger.info(" [ " + currentUser + " ] selected " + response.getTotal() + " pictures for purchase page");
        }
        return response;
    }

}
