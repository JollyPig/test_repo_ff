package com.scnsoft.fotaflo.webapp.aop;

import com.scnsoft.fotaflo.common.util.ServletUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class ControllerPerfomanceMonitor extends AbstractPerfomanceMonitor {

    @Around("purchasecontroller() || maincontroller() || catalogcontroller() || uploadcontroller()")
    public Object doMonitoring(ProceedingJoinPoint point){
        long time = System.currentTimeMillis();
        Object value = null;
        try {
            value = point.proceed();
        }catch (Throwable e) {
            logger.error("Error has been thrown by " + point.getSignature().toShortString(), e);
            throw new RuntimeException(e);
        }finally {
            int loadTime = (int)(System.currentTimeMillis() - time);
            try{
                String mapping = ServletUtil.getRequestMapping();
                logLoadTime(mapping, "Monitoring information of page loading, purchases, catalog pictures and picture uploads", loadTime);
            }catch (Exception e){
                logger.warn(e.getMessage(), e);
            }
        }

        return value;
    }

    @Pointcut("execution(public * com.scnsoft.fotaflo.webapp.controller.MainController.*(..))")
    public void maincontroller(){}

    @Pointcut("execution(public * com.scnsoft.fotaflo.webapp.controller.PurchaseController.*(..))")
    public void purchasecontroller(){}

    @Pointcut("execution(public * com.scnsoft.fotaflo.webapp.controller.CatalogController.*(..))")
    public void catalogcontroller(){}

    @Pointcut("execution(public * com.scnsoft.fotaflo.webapp.controller.UploadController.create*(..))")
    public void uploadcontroller(){}

}
