package com.scnsoft.fotaflo.webapp.controller;

import com.scnsoft.fotaflo.common.util.JsonMapper;
import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.service.LocationService;
import com.scnsoft.fotaflo.webapp.service.SettingsService;
import com.scnsoft.fotaflo.webapp.service.UserService;
import com.scnsoft.fotaflo.webapp.util.Resolution;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */
@Controller
@RequestMapping("/settings")
public class SettingsController {
    protected static Logger logger = Logger.getLogger(SettingsController.class);

    private final static String successProperty = "success";
    private final static String msgProperty = "msg";

    private final static String FOUR_PER_PAGE = "Four";

    @Autowired
    private SettingsService settingsService;

    @Autowired
    private UserService userService;

    @Autowired
    private LocationService locationService;

    @RequestMapping("/save.json")
    public
    @ResponseBody
    Map<String, Object> saveSettings(@RequestParam("speed") Integer speed,
                                     @RequestParam("pictureNumber") String pictureNumber,
                                     @RequestParam("startTime") Integer startTime,
                                     @RequestParam("delay") Integer delay,
                                     @RequestParam(value = "reducedcheck", required = false) String reducedcheck) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        Integer count = 1;
        if (FOUR_PER_PAGE.equals(pictureNumber)) {
            count = 4;
        }
        settingsService.updateUserSettings(currentUser, count, speed, startTime, delay);
        Location location = userService.getUserLocation(currentUser);
        if (location != null) {
            if (reducedcheck == null || !reducedcheck.trim().equals("on")) {
                location.setReduced(false);
                location.setResolution(Resolution.STANDARD.getCode());
            } else {
                location.setReduced(false);
                location.setResolution(Resolution.REDUCED.getCode());
            }
            locationService.saveLocations(Arrays.asList(location)
            );
        }
        Map<String, Object> modelMap = createModelMap(true);
        modelMap.put(msgProperty, "Settings were successfully updated");
        logger.info("User [ " + currentUser + " ]" + " updated setting successfully");

        return modelMap;
    }

    @RequestMapping("/updateTextLogo.json")
    public
    @ResponseBody
    Map<String, Object> updateLocationTextLogo(@RequestParam("locationIdText") Integer locationId,
                                               @RequestParam("textLogo") String textLogo) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        locationService.updateLocationTextLogo(locationId, textLogo);
        Map<String, Object> modelMap = createModelMap(true);
        modelMap.put(msgProperty, "Location text logo was updated successfully");
        logger.info("User [ " + currentUser + " ]" + " updated text logo successfully");
        return modelMap;
    }

    @RequestMapping("/updateImageLogo.json")
    public
    @ResponseBody
    ResponseEntity<String> updateImageLogo(@RequestParam("locationIdImg") Integer locationId,
                           @RequestParam("imageLogo") MultipartFile imageLogo) {
        Map<String, Object> modelMap = createModelMap(true);
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        try {
            locationService.updateLocationImageLogo(locationId, imageLogo);
            modelMap.put(msgProperty, "Location image logo was updated successfully");
            modelMap.put(successProperty, true);
            logger.info("User [ " + currentUser + " ]" + " updated image logo successfully");
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            modelMap.put(msgProperty, "Cannot update image logo");
            modelMap.put(successProperty, false);
            logger.warn("User [ " + currentUser + " ]" + " was not able to update image logo");
        }

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("Content-Type", "text/html; charset=utf-8");
        return new ResponseEntity<String>(JsonMapper.getJson(modelMap), responseHeaders, HttpStatus.CREATED);
    }

    @RequestMapping("/deleteImageLogo.json")
    public
    @ResponseBody
    Map<String, Object> deleteImageLogo(@RequestParam("locationIdImg") Integer locationId) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        Map<String, Object> modelMap = createModelMap(true);
        Location location = locationService.getLocation(locationId);
        locationService.deleteLocationImageLogo(locationId);
        modelMap.put(msgProperty, "Second logo for " + location.getLocationName() + " was deleted successfully");
        logger.info("User [ " + currentUser + " ]" + " deleted image logo successfully");
        return modelMap;
    }

    private Map<String, Object> createModelMap(Boolean success) {
        Map<String, Object> modelMap = new HashMap<String, Object>();
        modelMap.put(successProperty, success);
        return modelMap;
    }
}
