package com.scnsoft.fotaflo.webapp.dao.impl;

import com.scnsoft.fotaflo.webapp.dao.IHelpYourSelfSelctedDao;
import com.scnsoft.fotaflo.webapp.model.HelpYourselfSelected;
import com.scnsoft.fotaflo.webapp.model.Picture;
import com.scnsoft.fotaflo.webapp.model.SystemUser;

import java.util.List;

/**
 * Created by Nadezda Drozdova
 * Date: Apr 27, 2011
 * Time: 1:19:54 PM
 */
public class HelpYourSelfSelctedDao extends BaseDao implements IHelpYourSelfSelctedDao {

    public List<HelpYourselfSelected> getHelpYourselfByUser(SystemUser user) {
        StringBuffer query = new StringBuffer();
        query.append("select helpYourselfSelected from HelpYourselfSelected as helpYourselfSelected where ");
        query.append("helpYourselfSelected.systemUser = :user ");
        List list = getSession().createQuery(query.toString())
                .setParameter("user", user)
                .list();
        return list;
    }

}
