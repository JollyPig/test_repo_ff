package com.scnsoft.fotaflo.webapp.dao.impl;

import com.scnsoft.fotaflo.common.dao.AbstractDao;
import com.scnsoft.fotaflo.common.dao.util.CriteriaBuilder;
import com.scnsoft.fotaflo.webapp.dao.ISystemUserDao;
import com.scnsoft.fotaflo.webapp.model.ContactInfo;
import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.model.SystemUser;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */
@Repository
public class SystemUserDao extends AbstractDao<SystemUser, Integer> implements ISystemUserDao {
    @Override
    protected Class<SystemUser> getDomainClass() {
        return SystemUser.class;
    }

    public SystemUser getUserByLogin(String login) {
        StringBuffer query = new StringBuffer();
        query.append("from SystemUser user where user.loginName = :login");

        List result = getSession().createQuery(query.toString())
                .setParameter("login", login)
                .list();
        if (!result.isEmpty()) {
            return (SystemUser) result.get(0);
        }

        return null;
    }

    public SystemUser getUsersByLoginAndLocation(Location location, String login) {
        StringBuffer query = new StringBuffer();
        query.append("from SystemUser as user where ");
        query.append("user.loginName = :login ");
        query.append("and user.location = :location");

        List list1 = getSession().createQuery(query.toString())
                .setParameter("login", login)
                .setParameter("location", location)
                .list();
        if (!list1.isEmpty()) {
            return (SystemUser) list1.get(0);
        }

        return null;
    }

    public List<SystemUser> getPublicUsersByDateAndLocation(Location location, Date startDate, Date endDate, String access) {
        StringBuffer query = new StringBuffer();
        query.append("from SystemUser as user where ");
        query.append("user.access = :access ");
        query.append("and user.location = :location ");
        query.append("and user.creationDate >= :start ");
        query.append("and user.creationDate <= :end ");

        List list1 = getSession().createQuery(query.toString())
                .setParameter("location", location)
                .setParameter("start", startDate)
                .setParameter("end", endDate)
                .setParameter("access", access)
                .list();

        return list1;
    }

    public List<SystemUser> getPublicUsersByDate(Date startDate, Date endDate, String access) {
        StringBuffer query = new StringBuffer();
        query.append("from SystemUser as user where ");
        query.append("user.access = :access ");
        query.append("and user.expirationDate >= :start ");
        query.append("and user.expirationDate <= :end ");

        List list1 = getSession().createQuery(query.toString())
                .setParameter("start", startDate)
                .setParameter("end", endDate)
                .setParameter("access", access)
                .list();

        return list1;
    }

    public List<ContactInfo> getContcatInfosForUser(SystemUser user) {
        StringBuffer query = new StringBuffer();
        query.append("from ContactInfo contact where contact.systemUser = :user");

        return getSession().createQuery(query.toString())
                .setParameter("user", user)
                .list();
    }

    @Override
    public List<SystemUser> findByName(String name) {
        return getCriteriaFindByName(name).getCriteria().list();
    }

    protected CriteriaBuilder getCriteriaFindByName(String name) {
        CriteriaBuilder criteriaBuilder = getCriteriaBuilder();
        Criteria criteria = criteriaBuilder.getCriteria();

        String value = '%' + name + '%';
        criteria.add(Restrictions.or(
                Restrictions.ilike("loginName", value),
                Restrictions.or(
                        Restrictions.ilike("firstName", value),
                        Restrictions.ilike("lastName", value)
                )
        )
        );
        return criteriaBuilder;
    }

    @Override
    public List<SystemUser> findByName(String name, int start, int limit) {
        return getCriteriaFindByName(name).addLimit(start, limit).list();
    }

    @Override
    public long countByName(String name) {
        return ((Number) getCriteriaFindByName(name).getCount().uniqueResult()).longValue();
    }
}
