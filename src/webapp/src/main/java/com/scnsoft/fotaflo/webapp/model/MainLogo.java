package com.scnsoft.fotaflo.webapp.model;

import com.scnsoft.fotaflo.common.model.AbstractEntity;

import javax.persistence.*;

/**
 * Created by Paradinets
 * Date: 03.05.2011
 * Time: 17:11:25
 */
@Entity
@Table(name = "mainlogo")
public class MainLogo extends AbstractEntity {

    String imageMainLogo;
    Location location;

    @Column(name = "location_image_logo", length = 255)
    public String getImageMainLogo() {
        return imageMainLogo;
    }

    public void setImageMainLogo(String imageMainLogo) {
        this.imageMainLogo = imageMainLogo;
    }

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "location_id")
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
