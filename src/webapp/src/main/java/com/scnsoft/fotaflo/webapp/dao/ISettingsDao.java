package com.scnsoft.fotaflo.webapp.dao;

import com.scnsoft.fotaflo.webapp.model.SystemUser;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 31.03.2012
 * Time: 10:56:36
 * To change this template use File | Settings | File Templates.
 */
public interface ISettingsDao {

    public void updateUserSettings(SystemUser user, Integer pictureCount, Integer speed, Integer startTime, Integer delay);
}
