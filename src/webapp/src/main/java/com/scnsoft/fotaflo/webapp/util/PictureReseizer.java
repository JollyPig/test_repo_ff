package com.scnsoft.fotaflo.webapp.util;

import com.scnsoft.fotaflo.webapp.service.CrashUploadPictureService;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */

public class PictureReseizer {
    protected static Logger logger = Logger.getLogger(PictureReseizer.class);

    public static int pictureWidth = DefaultValues.pictureWidth;
    public static int pictureHeight = DefaultValues.pictureHeight;

    public static final float COMPRESS_QUALITY = 0.7f;

    public static BufferedImage resizePicture(String srcImg, String pictureSize) {
        if (pictureSize.equals("medium")) {
            pictureWidth = DefaultValues.pictureWidth;
            pictureHeight = DefaultValues.pictureHeight;
        }
        if (pictureSize.equals("small")) {
            pictureWidth = DefaultValues.pictureWidth / 8;
            pictureHeight = DefaultValues.pictureHeight / 8;
        }

        try {
            boolean rotated = false;
            File _file = new File(srcImg);
            if (!_file.exists()) return null;

            int scaledWidth = pictureWidth;
            int scaledHeight = pictureHeight;

            BufferedImage image = ImageIO.read(_file);
            int width = image.getWidth();
            int height = image.getHeight();
            if(height > 0 && width > 0){
                double ratio = (double) width / (double) height;
                if (height > width) {
                    rotated = true;
                    ratio = (double) height / (double) width;
                }
                if (ratio == (double) 16 / (double) 9) {
                    logger.info("SPECIAL PICTURE FORMAT!");
                    scaledWidth = DefaultValues.pictureWidthFormat2;
                    scaledHeight = DefaultValues.pictureHeightFormat2;
                }
            }

            if (rotated) {
                int temp = scaledWidth;
                scaledWidth = scaledHeight;
                scaledHeight = temp;
            }

            return getScaledImage(image, scaledWidth, scaledHeight);
        } catch (Exception e) {
            logger.info("UPLOAD ERROR: The reseizing was UNSUCCESSFULL" + e);
        }

        return null;
    }

    public static BufferedImage getScaledImage(BufferedImage image, int scaledWidth, int scaledHeight){
        if(image == null){
            return null;
        }

        int width = image.getWidth();
        int height = image.getHeight();
        BufferedImage scaledImage = new BufferedImage(scaledWidth, scaledHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = scaledImage.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.drawImage(image, 0, 0, scaledWidth, scaledHeight, 0, 0, width, height, null);
        g.dispose();

        return scaledImage;
    }

    public static Map<String, Boolean> saveResizedPicture(String srcImg, String dstImg) {
        logger.info("UPLOAD: Start resizing picture srcImg: " + srcImg + " dstImg: " + dstImg);
        Map<String, Boolean> resulMap = new HashMap<String, Boolean>();
        resulMap.put("rotated", false);
        resulMap.put("pictureSize", false);
        boolean rotated = false;
        BufferedImage image = resizePicture(srcImg, "medium");
        //return pictures without watermarks in case watermark was not correctly created

        if (srcImg == null) {
            srcImg = "";
        }
        if (image == null) {
            logger.warn("UPLOAD ERROR: Picture could not be reseized image==null. It is copied in the real size");
            try {
                File inputFile = new File(srcImg);
                File outputFile = new File(dstImg);

                FileReader in = new FileReader(inputFile);
                FileWriter out = new FileWriter(outputFile);
                int c;

                while ((c = in.read()) != -1)
                    out.write(c);

                in.close();
                out.close();
                sendReportAboutCrash("UPLOAD ERROR: Picture could not be reseized image==null. It is copied in the real size");
            } catch (IOException e) {
                logger.error("UPLOAD ERROR: Picture could not be copied " + e);
                e.printStackTrace();
            }

        } else {
            logger.info("image.getHeight() " + image.getHeight() + " image.getWidth() " + image.getWidth());
            if (image.getHeight() > image.getWidth()) {
                rotated = true;
                resulMap.put("rotated", true);
            }
            if (image.getHeight() != 0 && (double) image.getWidth() / (double) image.getHeight() == (double) 16 / (double) 9) {
                resulMap.put("pictureSize", true);
                logger.info("Picture " + srcImg + " has s special standart");
            } else {
                logger.info("Picture " + srcImg + " is regular");
            }
            try {
                File outputFile = new File(dstImg);
                OutputStream out = new PrintStream(outputFile);
                //ImageIO.write(image, "jpg", out);
                saveImage(image, outputFile, "jpg", COMPRESS_QUALITY);
                out.close();
            } catch (IOException e) {
                logger.warn("UPLOAD ERROR: Reseized picture could not be put to " + dstImg + e);
                e.printStackTrace();
            }
        }
        return resulMap;
    }

    protected static void saveImage(BufferedImage image, File out, String format, float quality) throws IOException{
        Iterator iter = ImageIO.getImageWritersByFormatName(format);
        if(!iter.hasNext()){
            throw new IllegalArgumentException("Unsupported format: " + format);
        }
        ImageWriter writer = (ImageWriter) iter.next();

        if(quality <= 0 || quality > 1){
            quality = 1;
        }

        ImageWriteParam iwp = writer.getDefaultWriteParam();
        iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        // reduced quality.
        iwp.setCompressionQuality(quality);

        writer.setOutput(new FileImageOutputStream(out));

        IIOImage img = new IIOImage(image, null, null);
        writer.write(null, img, iwp);

        writer.dispose();
    }

    private static void sendReportAboutCrash(String message) {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        Map<String, String> params = new HashMap<String, String>();
        params.put("message", message);
        params.put("location", requestAttributes.getAttribute("location", 0).toString());
        params.put("deviceId", requestAttributes.getAttribute("deviceId", 0).toString());
        params.put("fileName", requestAttributes.getAttribute("fileName", 0).toString());
        params.put("timeZone", requestAttributes.getAttribute("timeZone", 0).toString());
        params.put("fileDate", requestAttributes.getAttribute("fileDate", 0).toString());
        params.put("tags", requestAttributes.getAttribute("tags", 0).toString());
        params.put("expectedSize", requestAttributes.getAttribute("expectedSize", 0).toString());
        ApplicationContext context = ApplicationContextProvider.getApplicationContext();
        CrashUploadPictureService crashUploadPictureService = (CrashUploadPictureService) context.getBean("crashUploadPictureService");
        crashUploadPictureService.sendReportAboutCrashUploadPictures(params);
    }

    public static boolean createPictureDirectorty(String path) {
        boolean success = false;
        try {
            String strManyDirectories = path;

            if (new File(strManyDirectories).exists())
                return true;
            // Create multiple directories
            success = (new File(strManyDirectories)).mkdirs();
            if (success) {
                logger.warn("Directories: " + strManyDirectories + " created");
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        return success;
    }

    public static boolean saveRotatePicture(String srcImg, String dstImg, int angle) {
        boolean success = false;
        if (srcImg == null) {
            srcImg = "";
        }
        BufferedImage image = rotatePicture(srcImg, angle);
        if (image == null) {
            logger.warn("ROTATE ERROR: Picture could not be reseized image==null. It is copied in the real size");
            try {
                File inputFile = new File(srcImg);
                File outputFile = new File(dstImg);

                FileReader in = new FileReader(inputFile);
                FileWriter out = new FileWriter(outputFile);
                int c;

                while ((c = in.read()) != -1)
                    out.write(c);

                in.close();
                out.close();
            } catch (IOException e) {
                logger.error("ROTATE ERROR: Picture could not be copied " + e);
            }
        } else {
            try {
                File outputFile = new File(dstImg);
                OutputStream out = new PrintStream(outputFile);
                ImageIO.write(image, "jpg", out);
                out.close();
            } catch (IOException e) {
                logger.warn("ROTATE ERROR: Reseized picture could not be put to " + dstImg + e);
            }
        }

        return success;
    }

    public static BufferedImage rotatePicture(String srcImg, int angle) {
        try {
            File _file = new File(srcImg);
            if (!_file.exists()) return null;

            Image img = ImageIO.read(_file);

            double rads = Math.toRadians(angle);
            double sin = Math.abs(Math.sin(rads)), cos = Math.abs(Math.cos(rads));
            int w = img.getWidth(null);
            int h = img.getHeight(null);
            int newWidth = (int) Math.floor(w * cos + h * sin);
            int newHeight = (int) Math.floor(h * cos + w * sin);

            BufferedImage rotated = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_RGB);
            Graphics2D g2d = rotated.createGraphics();
            AffineTransform at = new AffineTransform();
            at.translate((newWidth - w) / 2, (newHeight - h) / 2);

            int x = w / 2;
            int y = h / 2;

            at.rotate(Math.toRadians(angle), x, y);
            g2d.setTransform(at);
            g2d.drawImage(img, 0, 0, null);
            g2d.dispose();

            return rotated;
        } catch (Exception e) {
            logger.info("ROTATE ERROR: The rotation was UNSUCCESSFULL" + e);
        }
        return null;
    }

}