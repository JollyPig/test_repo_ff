package com.scnsoft.fotaflo.webapp.controller;

import com.scnsoft.fotaflo.common.util.NumberUtils;
import com.scnsoft.fotaflo.common.util.StringUtils;
import com.scnsoft.fotaflo.webapp.model.*;
import com.scnsoft.fotaflo.webapp.service.*;
import com.scnsoft.fotaflo.webapp.util.AccessID;
import com.scnsoft.fotaflo.webapp.util.DateConvertationUtils;
import com.scnsoft.fotaflo.webapp.util.DefaultValues;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */

@Controller
@RequestMapping("/main")
public class MainController {
    protected static Logger logger = Logger.getLogger(MainController.class);

    @Autowired
    private PictureService pictureService;

    @Autowired
    private UserService userService;

    @Autowired
    private PurchaseService purchaseService;

    @Autowired
    private TagPurchaseService tagPurchaseService;

    @Autowired
    private SettingsService settingsService;

    @Autowired
    private ReportService reportService;

    @Value("${fotaflo.app_title}")
    private String pageTitle;

    /**
     * Handles and retrieves the common JSP page that everyone can see
     *
     * @return the name of the JSP page
     */
    // This will resolve to /WEB-INF/jsp/commonpage.jsp
    @RequestMapping(value = "/common", method = RequestMethod.GET)
    public String getCommonPage(ModelMap model, HttpServletRequest httpServletRequest) {
        if (pageTitle == null) {
            pageTitle = "Fotaflo";
        }
        model.put("title", pageTitle);

        String picturePath = pictureService.getPicturePath();

        long time1 = System.currentTimeMillis();
        DefaultValues.img_path = picturePath;
        DefaultValues.logo_path = picturePath;

        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("[ " + currentUser + " ]" + " opened common page");
        SystemUser systemUser = userService.getUserByLogin(currentUser);

        model.put("current", currentUser);
        int access = Integer.valueOf(systemUser.getAccess());
        model.put("userId", access);
        Location location = systemUser.getLocation();
        model.put("slideShowInNewWindow", location.getSlideShowInNewWindow () == null ? false : location.getSlideShowInNewWindow ());
        boolean tagged = (location != null && location.getTagged() != null && location.getTagged() == true) ? true : false;
        model.put("tagApp", tagged);
        boolean rfidEnabled = (location != null && location.getRfid() != null && location.getRfid() == true);
        model.put("rfidEnabled", rfidEnabled);
        boolean cameraEnabled = (location != null && location.getCamera() != null && location.getCamera() == true);
        model.put("cameraEnabled", cameraEnabled);
        boolean photobookEnabled = (location != null && location.getPhotobook() != null && location.getPhotobook() == true);
        model.put("photobook", photobookEnabled);
        UserSettings settings = systemUser.getUserSettings();
        if (settings != null) {
            if (settings.getStartTime() == null) {
                settingsService.updateStartTimeSettings(currentUser, DefaultValues.startTime);
            }
            model.put("slideshowStartTime", settings.getStartTime());
            model.put("slideshowDelay", settings.getDelay());
        }

        model.put("pageSize", pictureService.getPageSize(currentUser));
        UserFilter userFilter = pictureService.getUserFilter(currentUser);
        model.put("showTags", systemUser.getLocation().getTagged() == null ? false : systemUser.getLocation().getTagged());

        long time2 = System.currentTimeMillis();
        logger.info("STEP 1 STARTING OPENING THE CATALOG :  " + (time2 - time1) / 1000 + " sec " + (time2 - time1) + " milisec ");

        String purchaseId = httpServletRequest.getParameter("purchaseId");
        if (purchaseId != null && !purchaseId.equals("-1")) {
            Integer pId = Integer.parseInt(purchaseId);
            Purchase purchase = purchaseService.getPurchaseById(pId);

            model.put("startDate", purchase.getStartDate().getTime());
            model.put("endDate", purchase.getEndDate().getTime());

            model.put("startDay", DateConvertationUtils.getDateString(purchase.getStartDate()));
            model.put("endDay", DateConvertationUtils.getDateString(purchase.getEndDate()));
            model.put("startTime", DateConvertationUtils.getTimeString(purchase.getStartDate()));
            model.put("endTime", DateConvertationUtils.getTimeString(purchase.getEndDate()));

            model.put("location", purchase.getLocation() != null ? purchase.getLocation().getId() : "0");
            model.put("camera", purchase.getCameras());
            model.put("tag", purchase.getTags());
            model.put("purchaseId", pId);
            pictureService.clearAllSelections(currentUser);
            long time3 = System.currentTimeMillis();
            logger.info("STEP 2 GETTING PURCHASE INFO :  " + (time3 - time2) / 1000 + " sec " + (time3 - time2) + " milisec ");
            return "commonpage";
        }

        String tagPurchaseId = httpServletRequest.getParameter("tagPurchaseId");
        if (tagPurchaseId != null && !tagPurchaseId.equals("-1")) {
            Integer pId = Integer.parseInt(tagPurchaseId);
            TagPurchase tagPurchase = tagPurchaseService.getPurchaseTagById(pId);

            model.put("startDate", tagPurchase.getStartDate().getTime());
            model.put("endDate", tagPurchase.getEndDate().getTime());

            model.put("startDay", DateConvertationUtils.getDateString(tagPurchase.getStartDate()));
            model.put("endDay", DateConvertationUtils.getDateString(tagPurchase.getEndDate()));
            model.put("startTime", DateConvertationUtils.getTimeString(tagPurchase.getStartDate()));
            model.put("endTime", DateConvertationUtils.getTimeString(tagPurchase.getEndDate()));

            model.put("location", tagPurchase.getLocationPurchase() != null ? tagPurchase.getLocationPurchase().getId() : "0");
            model.put("camera", tagPurchase.getCameras());
            Set<Tag> tags = tagPurchase.getTags();
            String tagString = "";

            for (Tag tag : tags) {
                tagString = tagString.length() == 0 ? tag.getId() + "" : tagString + "," + tag.getId();
            }
            logger.info("tagString loaded with pId " + pId + " tagstring: " + tagString);
            model.put("tag", tagString);
            pictureService.clearAllSelections(currentUser);
            long time3 = System.currentTimeMillis();
            logger.info("STEP 3 GETTING TAG PURCHASE INFO :  " + (time3 - time2) / 1000 + " sec " + (time3 - time2) + " milisec ");
            return "commonpage";
        }

        String getFilter = httpServletRequest.getParameter("getFilter");
        if (getFilter != null &&
                (getFilter.equals("load") || getFilter.equals("photo"))
                && userFilter != null) {
            model.put("startDate", userFilter.getStartDate().getTime());
            model.put("endDate", userFilter.getEndDate().getTime());

            model.put("startDay", DateConvertationUtils.getDateString(userFilter.getStartDate()));
            model.put("endDay", DateConvertationUtils.getDateString(userFilter.getEndDate()));
            model.put("startTime", DateConvertationUtils.getTimeString(userFilter.getStartDate()));
            model.put("endTime", DateConvertationUtils.getTimeString(userFilter.getEndDate()));

            model.put("location", userFilter.getLocationId());
            model.put("camera", userFilter.getCameraId());
            model.put("tag", userFilter.getTagIds());

            if (getFilter.equals("photo")) {
                pictureService.clearAllSelections(currentUser);
            }
        } else {
            pictureService.clearAllSelections(currentUser);

            if (access == AccessID.PUBLICEMAILUSER.getValue()) {
                logger.info("Public Email User [ " + currentUser + " ]" + " is on common page");
                model.put("startDate", userFilter.getStartDate().getTime());
                model.put("endDate", userFilter.getEndDate().getTime());

                model.put("startDay", DateConvertationUtils.getDateString(userFilter.getStartDate()));
                model.put("endDay", DateConvertationUtils.getDateString(userFilter.getEndDate()));
                model.put("startTime", DateConvertationUtils.getTimeString(userFilter.getStartDate()));
                model.put("endTime", DateConvertationUtils.getTimeString(userFilter.getEndDate()));

                model.put("location", userFilter.getLocationId());
                model.put("camera", userFilter.getCameraId());
                model.put("tag", userFilter.getTagIds());
            } else {
                if (access == AccessID.PUBLICUSER.getValue()) {
                    logger.info("Public User [ " + currentUser + " ]" + " is on common page");
                    model.put("startDate", userFilter.getStartDate().getTime());
                    model.put("endDate", userFilter.getEndDate().getTime());

                    model.put("startDay", DateConvertationUtils.getDateString(userFilter.getStartDate()));
                    model.put("endDay", DateConvertationUtils.getDateString(userFilter.getEndDate()));
                    model.put("startTime", DateConvertationUtils.getTimeString(userFilter.getStartDate()));
                    model.put("endTime", DateConvertationUtils.getTimeString(userFilter.getEndDate()));
                } else {
                    model.put("startDate", DefaultValues.getStartDate());
                    model.put("endDate", DefaultValues.getEndDate());
                    model.put("startDay", "");
                    model.put("endDay", "");
                    model.put("startTime", "");
                    model.put("endTime", "");
                }

                model.put("location", "0");
                model.put("camera", "0");
                model.put("tag", "");
            }
        }
        long time4 = System.currentTimeMillis();
        logger.info("STEP 4 ALL THE REST :  " + (time4 - time2) / 1000 + " sec " + (time4 - time2) + " milisec ");
        return "commonpage";
    }

    @RequestMapping(value = "/usermanagement")
    public String getUserManagementPage(ModelMap model) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User [ " + currentUser + " ]" + " is on usermanagement page");
        addCurrentUserToModel(model);

        return "usermanagement";
    }

    @RequestMapping(value = "/mainlogomanagement")
    public String getMainLogoManagement(ModelMap model) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User [ " + currentUser + " ]" + " is on mainlogo page");
        addCurrentUserToModel(model);

        return "mainlogomanagement";
    }

    @RequestMapping(value = "/reports")
    public String getReportSettings(ModelMap model) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User [ " + currentUser + " ]" + " is on report settings page");
        addCurrentUserToModel(model);
        model.put("reportPath", reportService.getReportPath());

        return "reportsettings";
    }

    @RequestMapping(value = "/emailsettings")
    public String getEmailSettingsPage(ModelMap model) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User [ " + currentUser + " ]" + " is on email settings page");
        addCurrentUserToModel(model);

        return "emailsettings";
    }

    @RequestMapping(value = "/paypalsettings")
    public String getPayPalSettingsPage(ModelMap model) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User [ " + currentUser + " ]" + " is on paypal settings page");
        addCurrentUserToModel(model);

        return "paypalsettings";
    }

    @RequestMapping(value = "/merchantsettings")
    public String getMerchantSettingsPage(ModelMap model) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User [ " + currentUser + " ]" + " is on merchant settings page");
        addCurrentUserToModel(model);

        return "merchantsettings";
    }

    @RequestMapping(value = "/slideshow", method = RequestMethod.GET)
    public String getSlideshowPage(ModelMap model) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();

        Location location = userService.getUserByLogin(currentUser).getLocation();

        model.put("slidespeed", pictureService.getSlideShowSpeed(currentUser));
        model.put("slidecount", pictureService.getSlideShowCount(currentUser));
        model.put("current", currentUser);
        model.put("userId", pictureService.getUserAccess(currentUser));
        model.put("pageSize", pictureService.getPageSize(currentUser));
        model.put("slideShowInNewWindow", location.getSlideShowInNewWindow () == null ? false : location.getSlideShowInNewWindow ());

        List<BannerSlideShow> bannersLeft = pictureService.getSlideShowBanner("left", location);
        List<BannerSlideShow> bannersRight = pictureService.getSlideShowBanner("right", location);
        String leftUrl = "";
        String rightUrl = "";
        if (bannersLeft.size() == 1) {
            leftUrl = bannersLeft.get(0).getBannerUrl();
        }
        if (bannersRight.size() == 1) {
            rightUrl = bannersRight.get(0).getBannerUrl();
        }
        model.put("leftUrl", leftUrl);
        model.put("rightUrl", rightUrl);
        logger.info("User [ " + currentUser + " ]" + " opened SLIDESHOW page for location " + location.getLocationName());

        return "slideshowpage";
    }

    @RequestMapping(value = "/preview", method = RequestMethod.GET)
    public String getPreviewSlideshowPage(ModelMap model) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();

        model.put("slidespeed", pictureService.getSlideShowSpeed(currentUser));
        model.put("slidecount", pictureService.getSlideShowCount(currentUser));
        model.put("current", currentUser);
        model.put("userId", pictureService.getUserAccess(currentUser));
        model.put("pageSize", pictureService.getPageSize(currentUser));
        Location location = userService.getUserByLogin(currentUser).getLocation();
        List<BannerSlideShow> bannersLeft = pictureService.getSlideShowBanner("left", location);
        List<BannerSlideShow> bannersRight = pictureService.getSlideShowBanner("right", location);
        String leftUrl = "";
        String rightUrl = "";
        if (bannersLeft.size() == 1) {
            leftUrl = bannersLeft.get(0).getBannerUrl();
        }
        if (bannersRight.size() == 1) {
            rightUrl = bannersRight.get(0).getBannerUrl();
        }
        model.put("leftUrl", leftUrl);
        model.put("rightUrl", rightUrl);
        logger.info("User [ " + currentUser + " ]" + " opened PREVIEW page for location " + location.getLocationName());

        return "previewslideshow";
    }

    @RequestMapping(value = "/locations", method = RequestMethod.GET)
    public String getLocationsPage(ModelMap model) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User [ " + currentUser + " ]" + " opened locations page");
        addCurrentUserToModel(model);

        return "locationspage";
    }

    @RequestMapping(value = "/packages", method = RequestMethod.GET)
    public String getPackagesPage(ModelMap model) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User [ " + currentUser + " ]" + " opened packages page");
        addCurrentUserToModel(model);

        return "packagespage";
    }

    @RequestMapping(value = "/cameras", method = RequestMethod.GET)
    public String getCamerasPage(ModelMap model) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User [ " + currentUser + " ]" + " opened cameras page");
        addCurrentUserToModel(model);

        return "cameraspage";
    }

    /**
     * Handles and retrieves the admin JSP page that only admins can see
     *
     * @return the name of the JSP page
     */
    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String getAdminPage(ModelMap model) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User [ " + currentUser + " ]" + " opened administration page");
        addCurrentUserToModel(model);

        return "adminpage";
    }

    /**
     * Handles and retrieves the settings JSP page
     *
     * @return the name of the JSP page
     */
    @RequestMapping(value = "/settings", method = RequestMethod.GET)
    public String getSettingsPage(ModelMap model) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User [ " + currentUser + " ]" + " opened settings page");

        Location location = userService.getUserLocation(currentUser);
        UserSettings settings = userService.getUserSettings(currentUser);
        addCurrentUserToModel(model);

        model.put("locationLogo", location.getLocationTextLogo());
        model.put("speed", settings.getSpeed());
        model.put("picCount", settings.getPictureCount());
        if (settings.getStartTime() == null) {
            settingsService.updateStartTimeSettings(currentUser, DefaultValues.startTime);
        }
        model.put("startTime", settings.getStartTime());
        model.put("delay", settings.getDelay());
        model.put("reduced", (location == null || location.isReduced() == null) ? false : location.isReduced());

        return "settingspage";
    }

    /**
     * Handles and retrieves the import export settings JSP page
     *
     * @return the name of the JSP page
     */
    @RequestMapping(value = "/importexport", method = RequestMethod.GET)
    public String getImportExportPage(ModelMap model) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User [ " + currentUser + " ]" + " opened import-export page");
        addCurrentUserToModel(model);

        return "importexportsettingspage";
    }

    @RequestMapping(value = "/exportemails", method = RequestMethod.GET)
    public String getExportEmailsPage(ModelMap model) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User [ " + currentUser + " ]" + " opened export emails page");
        addCurrentUserToModel(model);

        return "exportEmails";
    }

    @RequestMapping(value = "/exportpublicreport", method = RequestMethod.GET)
    public String getExportPublicPurchase(ModelMap model) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User [ " + currentUser + " ]" + " opened export public report page");
        addCurrentUserToModel(model);

        return "exportPublicPurchase";
    }


    @RequestMapping(value = "/publicgenerate", method = RequestMethod.GET)
    public String getPublicLoginPagePage(ModelMap model) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User [ " + currentUser + " ]" + " opened public login generation page");
        addCurrentUserToModel(model);
        Date expirationDate = DateConvertationUtils.getStartDate(-15, new Date());
        model.put("expectedDate", expirationDate.getTime());

        return "publicgenerate";
    }


    /**
     * Handles and retrieves the import  banner settings JSP page
     *
     * @return the name of the JSP page
     */
    @RequestMapping(value = "/banners", method = RequestMethod.GET)
    public String getImportBanners(ModelMap model) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User [ " + currentUser + " ]" + " opened banners page");
        addCurrentUserToModel(model);

        return "banners";
    }


    @RequestMapping(value = "/contact")
    public String getMainContact(ModelMap model) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User [ " + currentUser + " ]" + " opened contact page");
        addCurrentUserToModel(model);

        return "contact";
    }

    @RequestMapping(value = "/contactadmin")
    public String getMainContactAdmin(ModelMap model) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User [ " + currentUser + " ]" + " opened contact administration page");
        addCurrentUserToModel(model);

        return "contactadmin";
    }

    @RequestMapping(value = "/emailfooter")
    public String getEmailFooter(ModelMap model) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User [ " + currentUser + " ]" + " opened email footer page");
        addCurrentUserToModel(model);

        return "emailfooter";
    }

    @RequestMapping(value = "/publicemailcontent")
    public String getPublicEmailContent(ModelMap model) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User [ " + currentUser + " ]" + " opened public email content page");
        addCurrentUserToModel(model);

        return "publicemailcontent";
    }

    @RequestMapping(value = "/qrtaggeneration")
    public String getQRTagGeneration(ModelMap model) {
        addCurrentUserToModel(model);

        return "qrtaggeneration";
    }

    @RequestMapping(value = "/imagemetadata")
    public String getImageMetadata(ModelMap model) {
        addCurrentUserToModel(model);

        return "imagemetadata";
    }

    @RequestMapping(value = "/tagapproval")
    public String getTagApproval(ModelMap model) {
        SystemUser systemUser = addCurrentUserToModel(model);

        String staffs = "";
        String fileName = "";
        if(systemUser != null){
            if(!StringUtils.isEmpty(systemUser.getStaffs())){
                staffs = systemUser.getStaffs();
            }
            if(!StringUtils.isEmpty(systemUser.getStringedFileName())){
                fileName = systemUser.getStringedFileName();
            }
        }

        model.put("fileName", fileName);
        model.put("staff", staffs);

        return "tagapproval";
    }

    /**
     * Handles and retrieves the settings JSP page
     *
     * @return the name of the JSP page
     */
    @RequestMapping(value = "/attachmentsettings", method = RequestMethod.GET)
    public String getAttachmentSettingsPage(ModelMap model) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        addCurrentUserToModel(model);

        Location location = userService.getUserLocation(currentUser);
        model.put("locationLogo", location.getLocationTextLogo());
        model.put("speed", userService.getUserSettings(currentUser).getSpeed());
        model.put("picCount", userService.getUserSettings(currentUser).getPictureCount());
        model.put("reduced", (location == null || location.isReduced() == null) ? false : location.isReduced());

        return "attachmentsettings";
    }

    /**
     * Handles and retrieves the settings JSP page
     *
     * @return the name of the JSP page
     */
    @RequestMapping(value = "/qrcode", method = RequestMethod.GET)
    public String getQrCodePage(ModelMap model) {
        addCurrentUserToModel(model);

        return "qrcodecam";
    }

    protected SystemUser addCurrentUserToModel(ModelMap model){
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        SystemUser systemUser = userService.getUserByLogin(currentUser);

        Integer access = null;
        Location location = null;
        boolean tagged = false, giveaway = false;
        UserSettings settings = null;
        String pageSize = null;

        if(systemUser != null){
            access = NumberUtils.getInteger(systemUser.getAccess());
            location = systemUser.getLocation();
            settings = systemUser.getUserSettings();
        }

        if(location != null){
            if(location.getTagged() != null){
                tagged = location.getTagged();
            }
            if(location.getGiveaway() != null){
                giveaway = location.getGiveaway();
            }
        }

        if(settings != null){
            if(settings.getPageSize() != null){
                pageSize = settings.getPageSize();
            }
        }

        if(pageSize == null){
            pageSize = DefaultValues.pageSize;
        }

        model.put("current", currentUser);
        model.put("userId", access);  // fixme
        model.put("location", location != null ? location.getId() : null);
        model.put("userAccess", access);
        model.put("tagApp", tagged);
        model.put("giveaway", giveaway);
        model.put("pageSize", pageSize);

        return systemUser;
    }

}
