package com.scnsoft.fotaflo.webapp.bean;

import java.util.ArrayList;
import java.util.List;

public class LocationDetailsBean extends LocationBean {

    private String emailFooter;
    private String fromEmail;

    private List<PictureBean> pictures;

    public LocationDetailsBean(int id, String locationName) {
        super(id, locationName);
    }

    public String getEmailFooter() {
        return emailFooter;
    }

    public void setEmailFooter(String emailFooter) {
        this.emailFooter = emailFooter;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public List<PictureBean> getPictures() {
        return pictures;
    }

    public void setPictures(List<PictureBean> pictures) {
        this.pictures = pictures;
    }

    public void addPicture(PictureBean picture) {
        if (this.pictures == null) {
            this.pictures = new ArrayList<PictureBean>();
        }
        this.pictures.add(picture);
    }
}
