package com.scnsoft.fotaflo.webapp.service;

import com.scnsoft.fotaflo.webapp.bean.PictureBean;
import com.scnsoft.fotaflo.webapp.bean.PurchasePictureBean;
import com.scnsoft.fotaflo.webapp.common.ResultList;
import com.scnsoft.fotaflo.webapp.dao.*;
import com.scnsoft.fotaflo.webapp.model.*;
import com.scnsoft.fotaflo.webapp.model.Package;
import com.scnsoft.fotaflo.webapp.util.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class PictureService {
    protected static Logger logger = Logger.getLogger(PictureService.class);

    @Value("${fotaflo.picture_path}")
    private String picturePath;

    public static final String IMG_DIR = File.separator + "img" + File.separator;
    public static final String IMG_SMALL_DIR = File.separator + "img_small" + File.separator;
    public static final String TAG_SEPARATOR = ";";

    public static final String FILENAME_REGEXP = "^(.+?)\\.([^\\.]+)$";

    private static String MAIN_LOGO_DIR = "mainlogo";
    private static String MAIN_LOCATIONS_PICTURES_DIR = "picturesLocations";
    private static String BANNER_DIR = "banners";

    public static String BANNER_DIR_BASE_PATH = PictureService.IMG_DIR + BANNER_DIR;
    public static String MAIN_LOGO_IMAGE_BASE_PATH = PictureService.IMG_DIR + MAIN_LOGO_DIR;
    public static String MAIN_LOCATIONS_PICTURES_DIR_PATH = PictureService.IMG_DIR + MAIN_LOCATIONS_PICTURES_DIR;
    public static String MAIN_LOCATIONS_PICTURES_SMALL_DIR_PATH = PictureService.IMG_SMALL_DIR + MAIN_LOCATIONS_PICTURES_DIR;

    @Autowired
    private IPictureDao pictureDao;

    @Autowired
    private LocationService locationService;

    @Autowired
    private IDao baseDao;

    @Autowired
    private IBannerDao bannerDao;

    @Autowired
    private CameraService cameraService;

    @Autowired
    private IPurchaseSelectedDao purchaseSelectedDao;

    @Autowired
    private IMainLogoDao mainLogoDao;

    @Autowired
    private IFilterDao filterDao;

    @Autowired
    private MailService mailService;

    @Autowired
    private UserService userService;

    @Autowired
    private TagService tagService;

    @Autowired
    PublicLoginService publicLoginService;

    public String getPicturePath() {
        return picturePath;
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public void loadPictureToDB(String location, String deviceId, String fileName, String folderPath, Date imageDate, String tags, boolean rotated, boolean pictureSize) {
        logger.info("UPLOAD: Start loading to DB: location: " + location + " deviceId: " + deviceId + " fileName: " +
                fileName + " imageDate: " + imageDate);
        List<String> pictureTags = new ArrayList<String>();

        if (tags != null && !tags.equals("")) {
            pictureTags = Arrays.asList(tags.split(TAG_SEPARATOR));
        }

        Camera specificCamera = new Camera();
        List<Camera> cameraList = baseDao.retrieveAll(Camera.class);

        for (Camera camera : cameraList) {
            if (camera != null && camera.getLocation() != null && camera.getCameraName() != null && camera.getLocation().getLocationName() != null)
                if ((camera.getLocation().getLocationName().equals(location)) && (camera.getCameraName().equals(deviceId))) {
                    specificCamera = camera;
                    logger.info("UPLOAD: Camera was found");
                    break;
                }
        }

        if (specificCamera.getCameraName() == null) {
            logger.info("UPLOAD ERROR: Camera " + deviceId + " was not found.");
        } else {
            Set<Tag> pictureTagObjects = new HashSet<Tag>();
            if (pictureTags.size() != 0) {
                for (String pictureTagName : pictureTags) {
                    Tag picTag = tagService.getTagByNameLocation(pictureTagName, specificCamera.getLocation());
                    Date today = new Date();
                    if (picTag == null) {
                        picTag = new Tag(today, pictureTagName, specificCamera.getLocation());
                        baseDao.create(picTag);
                        publicLoginService.createTagPublicLogin(pictureTagName, specificCamera.getLocation(), today, String.valueOf(AccessID.PUBLICTAGUSER.getValue()));
                    } else {
                        picTag.setCreationDate(new Date());
                        baseDao.update(picTag);
                        boolean updated = publicLoginService.updateTagPublicLoginIfNotExist(pictureTagName, specificCamera.getLocation(), today, String.valueOf(AccessID.PUBLICTAGUSER.getValue()));
                        if (updated) {
                            logger.info("Tag already existed but user did not we have created the user");
                        } else {
                            logger.info("User for the tag already existed");
                        }
                    }
                    pictureTagObjects.add(picTag);

                }
                logger.info("UPLOAD Tags: picture received with the following tags: " + tags);
            } else {
                logger.warn("UPLOAD Tags: picture received without any tags");
            }
            Picture picture = new Picture();
            picture.setName(fileName);
            picture.setCamera(specificCamera);
            picture.setCreationDate(imageDate);
            picture.setTags(pictureTagObjects);
            picture.setRotated(rotated);
            if (pictureSize == true) {
                picture.setPictureSize("special");
            } else {
                picture.setPictureSize("");
            }
            picture.setUrl(IMG_DIR + folderPath + File.separator + fileName);
            logger.info("UPLOAD: Picture  url: " + picture.getUrl());
            baseDao.create(picture);
            logger.info("UPLOAD SUCCESS: Picture  was successfully added to db");
        }
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public void savePictureToDB(Integer locationID, Integer cameraID, Date creationDate, MultipartFile pictureFile) throws IllegalStateException, IOException {
        boolean rotated = false;
        if (pictureFile == null || cameraID == null || creationDate == null) {
            logger.info("EXPORT ERROR: Picture could not be exported");
            return;
        }
        final String userLogin = SecurityContextHolder.getContext().getAuthentication().getName();
        SystemUser user = userService.getUserByLogin(userLogin);
        Set<SystemUser> userSet = new HashSet<SystemUser>();
        userSet.add(user);
        Camera camera = cameraService.getCamera(cameraID);
        if (camera == null) {
            logger.info("EXPORT ERROR: Camera could not be found");
            return;
        }
        Picture picture = new Picture();
        picture.setCamera(camera);
        picture.setCreationDate(creationDate);
        picture.setSystemUsers(userSet);
        String pictureName = pictureFile.getOriginalFilename().replaceAll("[\\/:*?\"<>|]", "");
        pictureName = pictureName.replace(".JPG", ".jpg");
        picture.setName(pictureName);

        Location location = locationService.getLocation(locationID);

        if (location == null) {
            logger.info("IMPORT ERROR: Location could not be found");
            return;
        }

        String folderPath = FotafloUtil.buildFolderPath(creationDate, location.getLocationName(), camera.getCameraName());

        boolean created = PictureReseizer.createPictureDirectorty(getPicturePath() + IMG_DIR + folderPath + File.separator);
        boolean createdSmall = PictureReseizer.createPictureDirectorty(getPicturePath() + IMG_SMALL_DIR + folderPath + File.separator);
        if (created) {
            String originalName = pictureFile.getOriginalFilename().replace(".JPG", ".jpg");
            picture.setUrl(createServerPictureUrl(IMG_DIR + folderPath + File.separator, originalName, creationDate));

            File targetFile = createServerPicture(IMG_DIR + folderPath + File.separator, originalName, creationDate);
            pictureFile.transferTo(targetFile);

            String serverFilePath = getPicturePath() + (createServerPictureUrl(IMG_DIR + folderPath + File.separator, originalName, creationDate));
            String serverFileSmallPath = getPicturePath() + (createServerPictureUrl(IMG_SMALL_DIR + folderPath + File.separator, originalName, creationDate));
            String pictureSize = "";
            if (createdSmall) {
                Map<String, Boolean> resultMap = PictureReseizer.saveResizedPicture(serverFilePath, serverFileSmallPath);
                rotated = resultMap.get("rotated");
                pictureSize = resultMap.get("pictureSize") ? "special" : "";
            }

            picture.setRotated(rotated);
            picture.setPictureSize(pictureSize);
            baseDao.create(picture);
        }

    }

    private File createServerPicture(String folder, String fileName, Date creationDate) throws IOException {
        fileName = fileName.replaceAll("[\\/:*?\"<>|]", "");
        String serverPictureUrl = createServerPictureUrl(folder, fileName, creationDate);
        File serverFile = new File(getPicturePath() + serverPictureUrl);
        serverFile.createNewFile();
        return serverFile;
    }

    private String createServerPictureUrl(String folder, String fileName, Date creationDate) {
        fileName = fileName.replaceAll("[\\/:*?\"<>|]", "");
        Pattern pattern = Pattern.compile(FILENAME_REGEXP);
        Matcher matcher = pattern.matcher(fileName);
        matcher.find();
        StringBuffer serverPictureUrl = new StringBuffer(folder);
        serverPictureUrl.append(matcher.group(1));
        serverPictureUrl.append("-");
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH-mm");
        serverPictureUrl.append(dateFormat.format(creationDate));
        serverPictureUrl.append(".");
        serverPictureUrl.append(matcher.group(2));
        return serverPictureUrl.toString();
    }

    public UserFilter getUserFilter(String userlogin) {
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        UserFilter userFilter = filterDao.getFilterByUser(systemUser);
        if (userFilter != null) {
            return userFilter;
        }
        return null;
    }

    public UserFilter getUserFilter(SystemUser systemUser) {
        UserFilter userFilter = filterDao.getFilterByUser(systemUser);
        if (userFilter != null) {
            return userFilter;
        }
        return null;
    }

    public boolean resetUserFilter(String userlogin) {
        UserFilter userFilter = getUserFilter(userlogin);
        if (userFilter != null) {
            userFilter.setStartDate(new Date(DefaultValues.getStartDate()));
            userFilter.setEndDate(new Date(DefaultValues.getEndDate()));
            userFilter.setCameraId("0");
            userFilter.setLocationId("0");
            userFilter.setTagIds("0");
            baseDao.update(userFilter);
            return true;
        }
        return false;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void saveFilter(UserFilter userFilter, String userlogin) {
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        userFilter.setSystemUser(systemUser);
        UserFilter filter = getUserFilter(systemUser);
        if (filter != null) {
            filter.setStartDate(userFilter.getStartDate());
            filter.setEndDate(userFilter.getEndDate());
            filter.setLocationId(userFilter.getLocationId());
            filter.setCameraId(userFilter.getCameraId());
            filter.setSystemUser(userFilter.getSystemUser());
            filter.setTagIds(userFilter.getTagIds());
            filter.setPictures(userFilter.getPictures());
            baseDao.update(filter);
        } else {
            baseDao.create(userFilter);
        }
    }

    public List<Picture> getPicturesForCameras(Integer[] cameras) {
        return pictureDao.getPicturesForCameras(cameras);
    }

    public List<PictureBean> getAllPicturesSelected(String userlogin, Filter filter) {
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        List<PictureBean> pictureBeanList = new ArrayList<PictureBean>();
        if (systemUser != null) {
            Set<Picture> pictures = systemUser.getUserPictures();
            for (Picture picture : pictures) {
                if (systemUser.getAccess().equals(String.valueOf(AccessID.PUBLICTAGUSER.getValue())) ||
                        (picture.getCamera() != null && picture.getCreationDate() != null && picture.getCreationDate().after(filter.getStartDate()) && picture.getCreationDate().before(filter.getEndDate()))) {
                    Boolean selected = picture.getSystemUsers().contains(systemUser);
                    Location location = picture.getCamera().getLocation();
                    if (picture != null && picture.getCamera() != null && location != null) {
                        Boolean rotated = picture.getRotated() == null || picture.getRotated() == false ? false : true;
                        PictureBean pictureBean = new PictureBean(picture.getId(), picture.getName(),
                                picture.getUrl(), String.valueOf(picture.getCamera().getId()),
                                picture.getCamera().getCameraName(),
                                DateConvertationUtils.viewStringDateFormat(picture.getCreationDate()), selected, rotated, picture.getPictureSize());

                        String locationImage = location.getLocationImageLogoUrl();
                        String locationText = location.getLocationTextLogo();
                        MainLogo mainLocationLogo = mainLogoDao.getMainLogoByLocation(picture.getCamera().getLocation());

                        pictureBean.setLogoUrl(locationImage != null ? locationImage.replace(File.separator, "/") : "");

                        pictureBean.setLogoText(locationText != null ? locationText : "");

                        pictureBean.setLogoMainUrl(mainLocationLogo != null ? mainLocationLogo.getImageMainLogo().replace(File.separator, "/") : "");

                        pictureBeanList.add(pictureBean);
                    }

                }
            }
        }
        return pictureBeanList;
    }

    public List<PictureBean> getPicturesFiltered(int start, int limit, String userlogin, Filter filter) {
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        UserFilter userFilter = getUserFilter(userlogin);
        List<Picture> pictureList = new ArrayList<Picture>();
        if (systemUser.getAccess().equals(AccessID.PUBLICTAGUSER.getValue())) {
            pictureList = pictureDao.findExtendedTagOnly(Arrays.asList(tagService.getTagByNameLocation(userlogin, systemUser.getLocation())));
        } else {
            if (userFilter != null && userFilter.getPictures().size() > 0) {
                pictureList = userFilter.getPictures();
            } else {
                List<String> tags = Arrays.asList(filter.getTag().split(","));
                if (tags == null || tags.size() == 0 || filter.getTag().equals("")) {
                    pictureList = pictureDao.findExtended(filter.getStartDate(), filter.getEndDate(), getCameraList(filter.getLocation(), filter.getCamera(), systemUser));
                } else {
                    if (tags.size() > 1 || !tags.contains("0")) {
                        pictureList.addAll(pictureDao.findExtendedByTag(filter.getStartDate(), filter.getEndDate(), getCameraList(filter.getLocation(), filter.getCamera(), systemUser), getTagList(tags)));
                    }
                    if (tags.contains("0")) {
                        pictureList.addAll(pictureDao.findExtendedWithoutTag(filter.getStartDate(), filter.getEndDate(), getCameraList(filter.getLocation(), filter.getCamera(), systemUser)));
                    }
                }
            }
        }
        Collections.sort(pictureList);

        List<PictureBean> pictureBeanList = new ArrayList<PictureBean>();

        int i = 1;
        for (Picture picture : pictureList) {
            if (i > start && i <= start + limit) {
                Boolean selected = picture.getSystemUsers().contains(systemUser);
                if (picture != null && picture.getCamera() != null && picture.getCamera().getLocation() != null) {
                    Boolean rotated = picture.getRotated() == null || picture.getRotated() == false ? false : true;
                    PictureBean pictureBean = new PictureBean(picture.getId(), picture.getName(),
                            picture.getUrl(), String.valueOf(picture.getCamera().getId())
                            , picture.getCamera().getCameraName(), DateConvertationUtils.viewStringDateFormat(picture.getCreationDate()), selected, rotated, picture.getPictureSize());

                    String locationImage = picture.getCamera().getLocation().getLocationImageLogoUrl();
                    String locationText = picture.getCamera().getLocation().getLocationTextLogo();

                    MainLogo mainLocationLogo = mainLogoDao.getMainLogoByLocation(picture.getCamera().getLocation());
                    pictureBean.setLogoMainUrl(mainLocationLogo != null ? mainLocationLogo.getImageMainLogo().replace(File.separator, "/") : "");

                    pictureBean.setLogoUrl(locationImage != null ? locationImage.replace(File.separator, "/") : "");

                    pictureBean.setLogoText(locationText != null ? locationText : "");


                    pictureBeanList.add(pictureBean);
                }
            }
            i++;
        }
        return pictureBeanList;
    }

    public List<PictureBean> getPicturesFiltered(String userlogin, Filter filter) {
        return getPicturesFiltered(0, Integer.MAX_VALUE, userlogin, filter);
    }

    public List<Tag> getTagList(List<String> tagIds) {
        List<Tag> tags = new ArrayList<Tag>();
        for (String tagId : tagIds) {
            Tag tg = (Tag) baseDao.retrieve(Tag.class, Integer.valueOf(tagId));
            if (tg != null) {
                tags.add(tg);
            }
        }
        return tags;
    }

    public List<Camera> getCameraList(String location, String camera, SystemUser systemUser) {
        //user is logged in
        List<Camera> cameras = new ArrayList();
        if (camera == null || camera.equals("")) {
            logger.info("No cameras were retrieved because cameraCombo is null or empty");
            return cameras;
        }
        List<String> camerasSelected = Arrays.asList(camera.split(","));
        if (!systemUser.getAccess().equals(AccessID.ADMIN.getStringValue())) {
            Location userLocation = systemUser.getLocation();
            //all cameras is selected

            if (camerasSelected.contains("0")) {
                cameras = cameraService.getCamerasByLocation(userLocation);
            } else {
                for (String cameraId : camerasSelected) {
                    Camera currentCamera = (Camera) baseDao.retrieve(Camera.class, Integer.valueOf(cameraId));
                    if (currentCamera != null) {
                        cameras.add(currentCamera);
                    }
                }
            }
            //admin is logged in
        } else {
            //all locations selected
            if (location == null || location.length() == 0) {
                logger.info("No cameras were retrieved because locationCombo is null or empty while user is admin");
                return cameras;
            }
            if (location.equals("0")) {
                //all cameras selected
                if (camerasSelected.contains("0")) {
                    cameras = baseDao.retrieveAll(Camera.class);
                } else {
                    for (String cameraId : camerasSelected) {
                        Camera currentCamera = (Camera) baseDao.retrieve(Camera.class, Integer.valueOf(cameraId));
                        if (currentCamera != null) {
                            cameras.add(currentCamera);
                        }
                    }
                }

            } else {
                Location userLocation = (Location) baseDao.retrieve(Location.class, Integer.valueOf(location));
                if (camerasSelected.contains("0")) {
                    cameras = cameraService.getCamerasByLocation(userLocation);
                } else {
                    for (String cameraId : camerasSelected) {
                        Camera currentCamera = (Camera) baseDao.retrieve(Camera.class, Integer.valueOf(cameraId));
                        if (currentCamera != null) {
                            cameras.add(currentCamera);
                        }
                    }
                }
            }

        }
        return cameras;
    }

    public ResultList<PurchasePictureBean> getPurchasePictureBeansAllResult(String userlogin, int requestId, int purchaseId, int purchaseIdCat) {
        return getPurchasePictureBeansAllResult(0, Integer.MAX_VALUE, userlogin, requestId, purchaseId, purchaseIdCat, -1);
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public ResultList<PurchasePictureBean> getPurchasePictureBeansAllResult(int start, int limit, String userlogin, int requestId, int purchaseId, int purchaseIdCat, int deleted) {
        ResultList<PurchasePictureBean> pictureBeanResultList = new ResultList<PurchasePictureBean>();
        List<PurchasePictureBean> purchasePictureBeanList = new ArrayList<PurchasePictureBean>();
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        PurchasePictureBean purchasePictureBean;

        if (requestId == -1) {

            ResultList<PurchaseSelected> purchaseSelectedResultList = purchaseSelectedDao.getPicturesSelectedByUser(start, limit, systemUser);
            List<PurchaseSelected> purchaseSelectedList = purchaseSelectedResultList.getItems();
            for (PurchaseSelected purchaseSelected : purchaseSelectedList) {
                Picture picture = purchaseSelected.getPicture();
                Boolean rotated = picture.getRotated() == null || picture.getRotated() == false ? false : true;
                Boolean selectedPurchase = purchaseSelected.getSend_to_email_selected() == 1;
                Integer toPrintNumber = purchaseSelected.getNumber_pict_selected();
                Boolean addToFacebook = purchaseSelected.getAdd_to_facebook() == 1;
                Boolean addToFlickr = purchaseSelected.getAdd_to_flickr() == 1;
                purchasePictureBean = formPurchasePictureBean(picture, selectedPurchase, toPrintNumber, addToFacebook, addToFlickr, rotated);
                if(purchasePictureBean != null){
                    purchasePictureBeanList.add(purchasePictureBean);
                }
            }
            pictureBeanResultList.setItems(purchasePictureBeanList);
            pictureBeanResultList.setTotal(purchaseSelectedResultList.getTotal());
        } else {
            //Retrieve purchase pictures from request
            Requests requests = (Requests) baseDao.retrieve(Requests.class, requestId);
            ResultList<RequestsSelected> requestsSelectedResultList = purchaseSelectedDao.getPicturesSelectedByRequest(start, limit, requests);
            List<RequestsSelected> requestsSelecteds = requestsSelectedResultList.getItems();
            for (RequestsSelected requestsSelected : requestsSelecteds) {
                Picture picture = requestsSelected.getPicture();
                Boolean rotated = picture.getRotated() == null || picture.getRotated() == false ? false : true;
                Boolean selectedPurchase = requestsSelected.getSend_to_email_selected() == 1;
                Integer toPrintNumber = requestsSelected.getNumber_pict_selected();
                Boolean addToFacebook = false;
                Boolean addToFlickr = false;
                purchasePictureBean = formPurchasePictureBean(picture, selectedPurchase, toPrintNumber, addToFacebook, addToFlickr, rotated);
                if(purchasePictureBean != null){
                    purchasePictureBeanList.add(purchasePictureBean);
                }
            }
            pictureBeanResultList.setItems(purchasePictureBeanList);
            pictureBeanResultList.setTotal(requestsSelectedResultList.getTotal());
        }

        return pictureBeanResultList;
    }

    public PurchasePictureBean formPurchasePictureBean(Picture picture, Boolean selectedPurchase, Integer toPrintNumber,
                                                       Boolean addToFacebook, Boolean addToFlickr, Boolean rotated) {
        PurchasePictureBean bean = null;
        if(picture != null){
            Camera camera = picture.getCamera();
            Location location = null;
            String cameraId = null,
                    cameraName = null,
                    creationDate = null,
                    locationImage = null,
                    locationText = null;

            if(camera != null){
                cameraId = String.valueOf(camera.getId());
                cameraName = camera.getCameraName();
                location = camera.getLocation();
                if(camera.getLocation() != null){
                    locationImage = location.getLocationImageLogoUrl();
                    locationText = location.getLocationTextLogo();
                }
            }
            if(picture.getCreationDate() != null){
                creationDate = DateConvertationUtils.viewStringDateFormat(picture.getCreationDate());
            }
            bean = new PurchasePictureBean(picture.getId(), picture.getName(),
                    picture.getUrl(), cameraId, cameraName, creationDate, true,
                    selectedPurchase, toPrintNumber, addToFacebook, addToFlickr, rotated, picture.getPictureSize());
            bean.setDate(picture.getCreationDate());

            if (locationImage != null) {
                bean.setLogoUrl(locationImage != null ? locationImage.replace(File.separator, "/") : "");
            }
            if (locationText != null) {
                bean.setLogoText(locationText != null ? locationText : "");
            }
            if(location != null){
                MainLogo mainLocationLogo = mainLogoDao.getMainLogoByLocation(location);
                if (mainLocationLogo != null) {
                    bean.setLogoMainUrl(mainLocationLogo != null ? mainLocationLogo.getImageMainLogo().replace(File.separator, "/") : "");
                }
            }
        }
        return bean;
    }

    public List<PurchasePictureBean> getPurchasePictureBeansAllWater(String userlogin, int requestId) {
        int start = 0;
        int limit = Integer.MAX_VALUE;
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        Location location = systemUser.getLocation();
        List<PurchasePictureBean> purchasePictureBeanList = new ArrayList<PurchasePictureBean>();
        List<PurchaseSelected> purchaseSelectedList = baseDao.retrieveAll(PurchaseSelected.class);
        int i = 1;
        for (PurchaseSelected purchaseSelected : purchaseSelectedList) {
            Picture picture = purchaseSelected.getPicture();
            Boolean rotated = picture.getRotated() == null || picture.getRotated() == false ? false : true;
            SystemUser user = purchaseSelected.getSystemUser();
            Boolean selected = (systemUser == user);
            Boolean selectedPurchase = purchaseSelected.getSend_to_email_selected() == 1;
            Integer toPrintNumber = purchaseSelected.getNumber_pict_selected();
            Boolean addToFacebook = purchaseSelected.getAdd_to_facebook() == 1;
            Boolean addToFlickr = purchaseSelected.getAdd_to_flickr() == 1;
            if (selected || requestId != -1) {
                PurchasePictureBean bean = new PurchasePictureBean(picture.getId(), picture.getName(),
                        picture.getUrl(), String.valueOf(picture.getCamera().getId())
                        , picture.getCamera().getCameraName(), DateConvertationUtils.viewStringDateFormat(picture.getCreationDate()),
                        selected, selectedPurchase, toPrintNumber, addToFacebook, addToFlickr, rotated, picture.getPictureSize());
                if (bean.isSelectedPurchase()) {
                    String locationImage = picture.getCamera().getLocation().getLocationImageLogoUrl();
                    String locationText = picture.getCamera().getLocation().getLocationTextLogo();
                    MainLogo mainLocationLogo = mainLogoDao.getMainLogoByLocation(picture.getCamera().getLocation());
                    if (locationImage != null) {
                        bean.setLogoUrl(locationImage != null ? locationImage.replace(File.separator, "/") : "");
                    }
                    if (locationText != null) {
                        bean.setLogoText(locationText != null ? locationText : "");
                    }
                    if (mainLocationLogo != null) {
                        bean.setLogoMainUrl(mainLocationLogo != null ? mainLocationLogo.getImageMainLogo().replace(File.separator, "/") : "");
                    }

                    if (i > start && i <= start + limit) {
                        String imagePath = getPath(
                                File.separator + bean.getUrl(false).replace("/", File.separator));
                        String locationImgLogo = (bean.getLogoUrl() == null) ? null
                                : getPath(bean.getLogoUrl().replace("/", File.separator));
                        String mainLogo = bean.getLogoMainUrl();
                        mainLogo = getPath(mainLogo.replace("/", File.separator));
                        String textLogo = bean.getLogoText();
                        String tmpPath = mailService.getTmpImagePath(bean, false, location.isReduced(), location.getResolution());//!location.isReduced() should be if we want to make public users receive in archive reduced pictures

                        String path = ImageWatermark.getOrSaveConvertedFile(null, tmpPath, imagePath, location.getImageMetadata());
                        if (path.equals(imagePath)) {
                            path = ImageWatermark.addLogoToImage(bean.getRotated(), imagePath,
                                    locationImgLogo,
                                    mainLogo,
                                    textLogo,
                                    tmpPath,
                                    location.isReduced() ? "reduced" : "high", "", location.getResolution(), location.getImageMetadata());     //importing for public user is always in high quality
                            logger.info("We don't have this picture small. Need to create it " + path);
                        }

                        bean.setUrl(path);

                        purchasePictureBeanList.add(bean);
                    }
                }
                i++;
            }
        }

        return purchasePictureBeanList;
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public void createPurchasePicturesSelectionModel(String userlogin, Integer requestId) {
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        List<PurchaseSelected> inPurch = purchaseSelectedDao.getPurchsePictureByUser(systemUser);
        if (inPurch != null) {
            baseDao.deleteAll(inPurch);
        }
        if (requestId != -1) {
            Requests request = (Requests) baseDao.retrieve(Requests.class, requestId);
            Set<RequestsSelected> requestSelected = request.getRequestsSelected();
            for (RequestsSelected requests : requestSelected) {
                Picture pic = requests.getPicture();
                PurchaseSelected ps = new PurchaseSelected();
                ps.setSystemUser(systemUser);
                ps.setPicture(pic);
                ps.setSend_to_email_selected(requests.getSend_to_email_selected());
                ps.setNumber_pict_selected(requests.getNumber_pict_selected());
                ps.setAdd_to_facebook(0);
                ps.setAdd_to_flickr(0);
                baseDao.create(ps);
            }
        } else {
            SystemUser ouruser = userService.getUserByLogin(userlogin);
            Set<Picture> pictureList = ouruser.getUserPictures();
            for (Picture picture : pictureList) {
                Boolean selected = picture.getSystemUsers().contains(systemUser);
                if (selected) {
                    PurchaseSelected ps = new PurchaseSelected();
                    ps.setSystemUser(systemUser);
                    ps.setPicture(picture);
                    ps.setSend_to_email_selected(1);
                    ps.setNumber_pict_selected(0);
                    ps.setAdd_to_facebook(0);
                    ps.setAdd_to_flickr(0);
                    baseDao.create(ps);
                }
            }
        }
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public String getPurchasePicturesSelectionModel(String userlogin) {
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        List<PurchaseSelected> inPurch = purchaseSelectedDao.getPurchsePictureByUser(systemUser);
        String sel = "";
        for (PurchaseSelected purSel : inPurch) {
            if (purSel.getSend_to_email_selected().equals(1)) {
                if (sel.equals("")) {
                    sel = String.valueOf(purSel.getPicture().getId());
                } else {
                    sel = sel + "," + String.valueOf(purSel.getPicture().getId());
                }
            }
        }
        return sel;
    }

    public List<PictureBean> updatePictureBeans(Object data, String userlogin) {
        SystemUser systemUser = userService.getUserByLogin(userlogin);

        List<PictureBean> updatedPictureBeans = new PictureUtil().getPicturesFromRequest(data);

        for (PictureBean pictureBean : updatedPictureBeans) {
            Picture picture = (Picture) baseDao.retrieve(Picture.class, pictureBean.getId());
            picture.setCamera((Camera) baseDao.retrieve(Camera.class, Integer.valueOf(pictureBean.getCameraId())));
            picture.setUrl(pictureBean.getUrl());
            picture.setName(pictureBean.getName());

            Set<SystemUser> systemUsers = picture.getSystemUsers();
            if (pictureBean.isSelected()) {
                systemUsers.add(systemUser);
            } else {
                systemUsers.remove(systemUser);
                PurchaseSelected ps = purchaseSelectedDao.getPurchsePictureByUser(picture, systemUser);
                if (ps != null) {
                    baseDao.delete(ps);
                }
            }

            baseDao.update(picture);
        }

        return updatedPictureBeans;
    }

    public List<PurchasePictureBean> updatePurchasePictureBeans(Object data, String userlogin, Integer requestId) {
        SystemUser systemUser = userService.getUserByLogin(userlogin);

        List<PurchasePictureBean> updatedPictureBeans = new PictureUtil().getPurchasePicturesFromRequest(data);

        for (PurchasePictureBean purchasePictureBean : updatedPictureBeans) {
            Picture picture = (Picture) baseDao.retrieve(Picture.class, purchasePictureBean.getId());

            Set<SystemUser> systemUsers = picture.getSystemUsers();
            if (requestId == -1) {
                if (purchasePictureBean.isSelected()) {
                    systemUsers.add(systemUser);
                } else {
                    systemUsers.remove(systemUser);
                }
            }

            Set<PurchaseSelected> purchaseSystemUsers = picture.getSelectedUsers();
            Iterator iterator = purchaseSystemUsers.iterator();
            while (iterator.hasNext()) {
                PurchaseSelected ps = (PurchaseSelected) iterator.next();
                if (ps.getSystemUser().equals(systemUser) && ps.getPicture().equals(picture)) {
                    ps.setSend_to_email_selected(purchasePictureBean.isSelectedPurchase() ? 1 : 0);
                    ps.setNumber_pict_selected(purchasePictureBean.getToPrintNumber());
                    ps.setAdd_to_facebook(purchasePictureBean.getAddToFacebook() ? 1 : 0);
                    ps.setAdd_to_flickr(purchasePictureBean.getAddToFlickr() ? 1 : 0);
                    baseDao.update(ps);
                    break;
                }
            }
            if (requestId == -1) {
                baseDao.update(picture);
            }
        }

        return updatedPictureBeans;
    }

    public void updatePageSize(String userlogin, String pageSize) {
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        UserSettings userSettings = systemUser.getUserSettings();
        if (userSettings == null) {
            userSettings = new UserSettings();
            systemUser.setUserSettings(userSettings);
        }
        userSettings.setPageSize(pageSize);
    }

    public void clearAllSelections(String userlogin) {
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        pictureDao.removePicturesSelectedForUser(systemUser);

    }

    public void selectDeselectPictures(String[] pictureIds, String userlogin) {
        SystemUser systemUser = userService.getUserByLogin(userlogin);

        int countSelected = 0;

        for (String pictureId : pictureIds) {
            if (pictureId != null && !pictureId.equals("")) {
                Picture pict = (Picture) baseDao.retrieve(Picture.class, Integer.valueOf(pictureId));
                Set<SystemUser> systemUsers = pict.getSystemUsers();
                if (systemUsers.contains(systemUser)) {
                    countSelected++;
                }
            }
            //check if all pictures selected -> then unselect them  all else select them all
        }

        boolean addOrRemove = true; //means add by default
        if (pictureIds.length == countSelected) {
            addOrRemove = false;
        }
        for (String pictureId : pictureIds) {
            if (pictureId != null && !pictureId.equals("")) {
                Picture pict = (Picture) baseDao.retrieve(Picture.class, Integer.valueOf(pictureId));
                Set<SystemUser> systemUsers = pict.getSystemUsers();
                if (addOrRemove) {
                    systemUsers.add(systemUser);
                } else {
                    systemUsers.remove(systemUser);
                }
                baseDao.update(pict);
            }
        }
    }

    public void deletePictures(String[] pictureIds, String userlogin, Boolean singledeletion) {
        SystemUser systemUser = userService.getUserByLogin(userlogin);

        for (String pictureId : pictureIds) {
            if (pictureId != null && !pictureId.equals("")) {
                Picture pict = (Picture) baseDao.retrieve(Picture.class, Integer.valueOf(pictureId));
                if (singledeletion != null && singledeletion) {
                    baseDao.delete(pict);
                    return;
                }
                Set<SystemUser> systemUsers = pict.getSystemUsers();
                if (systemUsers.contains(systemUser)) {
                    systemUsers.remove(systemUser);
                    baseDao.update(pict);
                    baseDao.delete(pict);
                }
            }
            //check if all pictures selected -> then unselect them  all else select them all
        }
    }

    public void rotatePictures(String pictureId, String userlogin, String direction) {
        if (pictureId != null && !pictureId.equals("")) {
            Picture pict = (Picture) baseDao.retrieve(Picture.class, Integer.valueOf(pictureId));
            int angle = (direction != null && direction.equals("right")) ? -90 : 90;

            String newName = angle == 90 ? pict.getUrl().replace(".jpg", "r.jpg") : pict.getUrl().replace(".jpg", "l.jpg");
            String newNameSmall = angle == 90 ? pict.getUrl(true).replace(".jpg", "r.jpg") : pict.getUrl(true).replace(".jpg", "l.jpg");

            String imagePath = getPath(
                    getUrl(false, pict.getUrl().replace("/", File.separator)));
            String imagePathDest = getPath(
                    getUrl(false, newName.replace("/", File.separator)));


            String imagePathSmall = getPath(
                    getUrl(true, pict.getUrl().replace("/", File.separator)));
            String imagePathDestSmall = getPath(
                    getUrl(true, newName.replace("/", File.separator)));

            logger.info("URL:::::                 " + pict.getUrl());
            logger.info("imagePath:::::           " + imagePath);
            logger.info("imagePathDest:::::       " + imagePathDest);
            logger.info("imagePathSmall:::::      " + imagePathSmall);
            logger.info("imagePathDestSmall:::::  " + imagePathDestSmall);

            boolean success1 = PictureReseizer.saveRotatePicture(imagePath, imagePathDest, angle);
            boolean success2 = PictureReseizer.saveRotatePicture(imagePathSmall, imagePathDestSmall, angle);

            pict.setUrl(newName);
            if (pict.getRotated() == null || pict.getRotated() == false) {
                pict.setRotated(true);
            } else {
                pict.setRotated(false);
            }
            baseDao.update(pict);
        }
        //check if all pictures selected -> then unselect them  all else select them all
    }

    public String getUrl(boolean small, String url) {
        String dir1 = File.separator + "img" + File.separator;
        String dirsmall = File.separator + "img_small" + File.separator;
        String dir2 = "img" + File.separator;

        if (small) {
            if (url.contains(dir1)) {
                url = url.replace(dir1, dirsmall);
            } else {
                url = url.replace(dir2, dirsmall);
            }
        } else {
            if (!url.contains(dir1)) {
                url = url.replace(dir2, dir1);
            }
        }
        return url;
    }

    public void deletePurchase(String pictureIds, String userlogin, boolean fromRequest) {
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        Picture picture = (Picture) baseDao.retrieve(Picture.class, Integer.valueOf(pictureIds));
        if (!fromRequest) {
            picture.getSystemUsers().remove(systemUser);
            baseDao.update(picture);
        }
        PurchaseSelected purchSel = purchaseSelectedDao.getPurchsePictureByUser(picture, systemUser);
        if (purchSel != null) {
            baseDao.delete(purchSel);
        }
    }

    public void clearPurchaseSelections(List<PurchasePictureBean> helpPictureBeans, String userlogin) {
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        Iterator iterator = helpPictureBeans.iterator();
        while (iterator.hasNext()) {
            PurchasePictureBean hyspb = (PurchasePictureBean) iterator.next();
            Picture picture = (Picture) baseDao.retrieve(Picture.class, hyspb.getId());
            PurchaseSelected helpSel = purchaseSelectedDao.getPurchsePictureByUser(picture, systemUser);
            if (helpSel != null) {
                baseDao.delete(helpSel);
            }
        }
    }

    public String getPackageById(int packageId) {
        Package pack = null;
        String packName = "";
        if (packageId != -1) {
            pack = (Package) baseDao.retrieve(Package.class, packageId);
            if (pack != null)
                packName = pack.getPackageName();
        }
        return packName;
    }

    public String getPackageLocation(int packageId) {
        Package pack = null;
        String locationName = "";
        if (packageId != -1) {
            pack = (Package) baseDao.retrieve(Package.class, packageId);
            if (pack != null)
                locationName = pack.getLocation().getLocationName();
        }
        return locationName;
    }

    public Set<String> getStaffByIds(List<Integer> staffIds) {
        Set<String> staffArray = new HashSet<String>();
        if (!staffIds.isEmpty()) {
            for (Integer staffId : staffIds) {
                Staff staff = (Staff) baseDao.retrieve(Staff.class, staffId);
                staffArray.add(staff.getStaffName());
            }
        }
        return staffArray;
    }

    public int getUserAccess(String userlogin) {
        return Integer.valueOf(userService.getUserByLogin(userlogin).getAccess());
    }

    public UserSettings getUserSettings(String username){
        if(username == null || username.isEmpty()){
            throw new IllegalArgumentException("'username' cannot be empty");
        }
        SystemUser user = userService.getUserByLogin(username);
        if(user != null){
            return user.getUserSettings();
        }else{
            return null;
        }
    }

    public int getSlideShowSpeed(String username) {
        UserSettings settings = getUserSettings(username);
        if(settings != null && settings.getSpeed() != null){
            return settings.getSpeed();
        }else{
            return DefaultValues.speed;
        }
    }

    public int getSlideShowCount(String username) {
        UserSettings settings = getUserSettings(username);
        if(settings != null && settings.getPictureCount() != null){
            return settings.getPictureCount();
        }else{
            return DefaultValues.pictureCount;
        }
    }

    public String getPageSize(String username) {
        UserSettings settings = getUserSettings(username);
        if(settings != null && settings.getPageSize() != null){
            return settings.getPageSize();
        }else{
            return DefaultValues.pageSize;
        }
    }

    public PurchasePictureBean checkPictureExistence(PurchasePictureBean purchaseItem) {
        String pictureFullPath = getPicturePath() + (purchaseItem.getUrl().startsWith(File.separator) ? purchaseItem.getUrl() : File.separator + purchaseItem.getUrl());
        logger.info("pictureFullPath " + pictureFullPath);
        pictureFullPath = pictureFullPath.replace("/", File.separator);
        File file = new File(pictureFullPath);
        if (!file.exists()) {
            String copyPath = purchaseItem.getUrl();
            copyPath = copyPath.replace("/", File.separator);
            String pictureFullPathSmall = getPicturePath() + (copyPath.startsWith(File.separator) ? copyPath : File.separator + copyPath);
            logger.warn("pictureFullPathSmall " + pictureFullPathSmall);
            pictureFullPathSmall = pictureFullPathSmall.replace(IMG_DIR, IMG_SMALL_DIR);
            logger.warn("pictureFullPathSmallAfter " + pictureFullPathSmall);
            File fileSmall = new File(pictureFullPathSmall);
            if (!fileSmall.exists()) {
                logger.warn("ALARM!!! DO NOT EXIST ANY VERSION OF FILE " + purchaseItem.getName());
                return null;
            } else {
                logger.warn("ALARM!! ONLY SMALL VERSION OF FILE EXIST " + purchaseItem.getName());
                copyPath = copyPath.replace("img" + File.separator, "img_small" + File.separator);
                purchaseItem.setUrl(copyPath);
                return purchaseItem;
            }
        }

        return purchaseItem;
    }

    public Location getUserLocation(String userlogin) {
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        return systemUser.getLocation();
    }

    public void updateMainImageLogo(Integer locationId, MultipartFile imagelogo, String userlogin) throws IOException {
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        if (systemUser.getAccess().equals("1")) {
            Location location = (Location) baseDao.retrieve(Location.class, locationId);
            String fileName = MAIN_LOGO_DIR + "_" + location.getLocationName();
            Pattern pattern = Pattern.compile(PictureService.FILENAME_REGEXP);
            Matcher matcher = pattern.matcher(imagelogo.getOriginalFilename());
            matcher.find();
            fileName += "." + matcher.group(2);

            MainLogo mainLogo = mainLogoDao.getMainLogoByLocation(location) != null ? mainLogoDao.getMainLogoByLocation(location) : new MainLogo();
            mainLogo.setLocation(location);
            mainLogo.setImageMainLogo(MAIN_LOGO_IMAGE_BASE_PATH + File.separator + fileName);
            baseDao.update(mainLogo);

            boolean created = PictureReseizer.createPictureDirectorty(getPicturePath() + MAIN_LOGO_IMAGE_BASE_PATH +
                    File.separator);
            if (created) {
                File logoImage = new File(getPicturePath() + (MAIN_LOGO_IMAGE_BASE_PATH +
                        File.separator + fileName));
                logger.debug("servletContext:  " + getPicturePath() + (MAIN_LOGO_IMAGE_BASE_PATH +
                        File.separator + fileName));
                logoImage.createNewFile();

                logger.debug("createNewFile");
                imagelogo.transferTo(logoImage);
            }
        }
    }

    public void addLocationPictures(Integer locationId, MultipartFile image, String userlogin) throws IOException {
        Location location = (Location) baseDao.retrieve(Location.class, locationId);

        String fileName = image.getOriginalFilename();

        String directory = MAIN_LOCATIONS_PICTURES_DIR_PATH +
                File.separator + location.getLocationName();
        String directoryFull = getPicturePath() + directory;

        String directorySmall = MAIN_LOCATIONS_PICTURES_SMALL_DIR_PATH +
                File.separator + location.getLocationName();
        String directoryFullSmall = getPicturePath() + directorySmall;

        List<Picture> locationAddPictures = location.getLocationAddPictures();
        if (locationAddPictures == null) {
            locationAddPictures = new ArrayList<Picture>();
        }
        String picturePath = directory + File.separator + fileName;
        if (picturePath != null) {
            picturePath = picturePath.replace('\\', '/');
            if (picturePath.startsWith("/") && picturePath.length() > 1) {
                picturePath = picturePath.substring(1);
            }
        }
        Picture locationAddPicture = new Picture();
        locationAddPicture.setLocation(location);
        locationAddPicture.setUrl(picturePath);
        locationAddPicture.setCreationDate(new Date());
        locationAddPicture.setName(fileName);

        locationAddPictures.add(locationAddPicture);
        location.setLocationAddPictures(locationAddPictures);

        boolean created = PictureReseizer.createPictureDirectorty(directoryFull);
        boolean createdSmall = PictureReseizer.createPictureDirectorty(directoryFullSmall);

        if (created) {

            File imagePicture = new File(directoryFull + File.separator + fileName);

            logger.debug("Full Directory of the location file:  " + directoryFull + File.separator + fileName);
            imagePicture.createNewFile();

            image.transferTo(imagePicture);
            logger.debug("Location file created  ");
        }
        if (createdSmall) {
            PictureReseizer.saveResizedPicture(directoryFull + File.separator + fileName, directoryFullSmall + File.separator + fileName);
            logger.debug("Location small file created  ");
        }

        baseDao.create(locationAddPicture);

        baseDao.update(location);
    }

    public List<Picture> getLocationPictures(Integer locationId) throws IOException {
        Location location = (Location) baseDao.retrieve(Location.class, locationId);
        if (location != null) {
            return location.getLocationAddPictures();
        } else {
            return new ArrayList<Picture>();
        }
    }

    public boolean deleteLocationPicture(Integer locationPictureId) throws IOException {
        if (locationPictureId != null) {
            baseDao.delete(Picture.class, locationPictureId);
            return true;
        }
        return false;
    }

    public void updateSlideShowBanner(Location location, String bannerType, MultipartFile imagebanner, String userlogin) throws IOException {
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        if (systemUser.getAccess().equals("1") && location != null) {
            String fileName = BANNER_DIR + "_" + location.getLocationName() + "_" + bannerType;
            Pattern pattern = Pattern.compile(PictureService.FILENAME_REGEXP);
            Matcher matcher = pattern.matcher(imagebanner.getOriginalFilename());
            matcher.find();
            fileName += "_" + matcher.group(1);
            fileName += "." + matcher.group(2);
            fileName = fileName.replace(".JPG", ".jpg");

            List<BannerSlideShow> banners = bannerDao.getBannerSlideShowByLocation(location);

            boolean exist = false;
            if (banners != null || !banners.isEmpty()) {
                for (BannerSlideShow banner : banners) {
                    if (banner.getBannerType() != null && banner.getBannerType().equals(bannerType)) {
                        banner.setBannerUrl(BANNER_DIR_BASE_PATH + File.separator + fileName);
                        baseDao.update(banner);
                        exist = true;
                    }
                }
            }
            if (banners == null || banners.isEmpty() || !exist) {
                BannerSlideShow bannerSlideShow = new BannerSlideShow();
                bannerSlideShow.setBannerType(bannerType);
                bannerSlideShow.setBannerUrl(BANNER_DIR_BASE_PATH + File.separator + fileName);
                bannerSlideShow.setLocation(location);
                baseDao.create(bannerSlideShow);
            }

            boolean created = PictureReseizer.createPictureDirectorty(getPicturePath() + BANNER_DIR_BASE_PATH +
                    File.separator);
            if (created) {

                File bannerFile = new File(getPicturePath() + (BANNER_DIR_BASE_PATH +
                        File.separator + fileName));
                logger.debug("servletContext:  " + getPicturePath() + (BANNER_DIR_BASE_PATH +
                        File.separator + fileName));
                bannerFile.createNewFile();

                logger.debug("createNewFile");
                imagebanner.transferTo(bannerFile);
            }
        }
    }

    public List<BannerSlideShow> getSlideShowBanner(String bannerType, Location location) {
        List<BannerSlideShow> bannersSlide = new ArrayList<BannerSlideShow>();
        if (location == null) return bannersSlide;
        List<BannerSlideShow> banners = bannerDao.getBannerSlideShowByLocation(location);

        if (banners != null || !banners.isEmpty()) {
            for (BannerSlideShow banner : banners) {
                if (banner.getBannerType() != null && banner.getBannerType().equals(bannerType)) {
                    if (banner.getBannerUrl() != null) {
                        String url = banner.getBannerUrl();
                        url = url.replace('\\', '/');
                        banner.setBannerUrl(url);
                    }
                    ;
                    bannersSlide.add(banner);
                }
            }
        }
        return bannersSlide;
    }

    public boolean getBanner(String bannerType, Location location) throws IOException {
        boolean result = false;
        if (location == null) return result;
        List<BannerSlideShow> banners = bannerDao.getBannerSlideShowByLocation(location);
        if (banners != null || !banners.isEmpty()) {
            for (BannerSlideShow banner : banners) {
                if (banner.getBannerType() != null && banner.getBannerType().equals(bannerType)) {
                    result = true;
                }
            }
        }
        return result;
    }

    public void deleteSlideShowBanner(Location location, String bannerType, String userlogin) throws IOException {
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        if (systemUser.getAccess().equals("1") && location != null) {
            deleteSlideShowBanner(location, bannerType);
        }
    }

    public void deleteSlideShowBanner(Location location, String bannerType) throws IOException {
        if (location != null) {

            List<BannerSlideShow> banners = bannerDao.getBannerSlideShowByLocation(location);
            if (banners != null || !banners.isEmpty()) {
                for (BannerSlideShow banner : banners) {
                    if (banner.getBannerType() != null && banner.getBannerType().equals(bannerType)) {
                        baseDao.delete(banner);
                    }
                }
            }
        }
    }

    public void deleteMainImageLogo(Integer locationId, String userlogin) throws IOException {
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        if (systemUser.getAccess().equals("1")) {
            Location location = (Location) baseDao.retrieve(Location.class, locationId);
            MainLogo mainLogo = mainLogoDao.getMainLogoByLocation(location) != null ? mainLogoDao.getMainLogoByLocation(location) : new MainLogo();
            if (mainLogo != null) {
                baseDao.delete(mainLogo);
            }
        }
    }

    public List<Integer> getPicturesFilteredIds(String userlogin, Filter filter) {
        SystemUser systemUser = userService.getUserByLogin(userlogin);

        List<Picture> pictureList = new ArrayList<Picture>();
        List<String> tags = Arrays.asList(filter.getTag().split(","));
        if (tags == null || tags.size() == 0 || filter.getTag().equals("")) {
            pictureList = pictureDao.findExtended(filter.getStartDate(), filter.getEndDate(), getCameraList(filter.getLocation(), filter.getCamera(), systemUser));
        } else {
            if (tags.size() > 1 || !tags.contains("0")) {
                pictureList.addAll(pictureDao.findExtendedByTag(filter.getStartDate(), filter.getEndDate(), getCameraList(filter.getLocation(), filter.getCamera(), systemUser), getTagList(tags)));
            }
            if (tags.contains("0")) {
                pictureList.addAll(pictureDao.findExtendedWithoutTag(filter.getStartDate(), filter.getEndDate(), getCameraList(filter.getLocation(), filter.getCamera(), systemUser)));
            }
        }
        List<Integer> pictureIdList = new ArrayList<Integer>();
        for (Picture picture : pictureList) {
            pictureIdList.add(picture.getId());
        }
        return pictureIdList;
    }

    public String getPath(String localPath) {
        return getPicturePath() + (localPath);
    }

    public String getStringFileName(String userlogin) {
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        String fileName = systemUser.getStringedFileName();
        fileName = fileName == null ? "" : fileName;
        return fileName;
    }

    public String getPucrchaseStaffs(String userlogin) {
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        String staffs = systemUser.getStaffs();
        staffs = staffs == null ? "" : staffs;
        return staffs;
    }

    public Picture getPictureById(Integer id) {
        return (Picture) baseDao.retrieve(Picture.class, id);
    }

    public String getEmailBody(String userlogin) {
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        String body = systemUser.getEmailBody();
        body = body == null ? "" : body;
        return body;
    }

    public void updateStringFileName(String userlogin, String newStr) {
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        if (newStr != null && !newStr.equals("")) {
            systemUser.setStringedFileName(newStr);
        }
    }

    public void updateMessageBody(String userlogin, String body) {
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        if (body != null && !body.equals("")) {
            systemUser.setEmailBody(body);
            baseDao.update(systemUser);
        }
    }

    public void updateStaffs(String userlogin, String staff) {
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        if (staff != null /*&& !staff.equals("")*/) {
            systemUser.setStaffs(staff);
            baseDao.update(systemUser);
        }
    }

    public String getMainLogo(Location pictureLocation) {
        MainLogo mainLogos = mainLogoDao.getMainLogoByLocation(pictureLocation);
        return (mainLogos != null) ? mainLogos.getImageMainLogo() : "";
    }

    public void saveSubscribe(List<Subscribe> emailsTosubscribe) {
        if (emailsTosubscribe == null) return;
        for (Subscribe emailTosubscribe : emailsTosubscribe) {
            baseDao.create(emailTosubscribe);
        }
    }

    public void savePurchaseStatisticEmails(List<PurchaseEmail> emailsToSave) {
        if (emailsToSave == null) return;
        for (PurchaseEmail emailToSave : emailsToSave) {
            baseDao.create(emailToSave);
        }
    }

    public void saveReportEmail(List<ReportEmails> reportEmails) {
        if (reportEmails == null)
            return;
        for (ReportEmails reportEmail : reportEmails) {
            baseDao.create(reportEmail);
        }
    }
    
}
