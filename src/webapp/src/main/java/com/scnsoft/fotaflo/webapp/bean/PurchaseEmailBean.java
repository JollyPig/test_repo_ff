package com.scnsoft.fotaflo.webapp.bean;

import java.util.Date;

public class PurchaseEmailBean {

    String email;
    Date date;
    LocationBean location;

    public PurchaseEmailBean() {
    }

    public PurchaseEmailBean(String email, Date date, LocationBean location) {
        this.email = email;
        this.date = date;
        this.location = location;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public LocationBean getLocation() {
        return location;
    }

    public void setLocation(LocationBean location) {
        this.location = location;
    }
}
