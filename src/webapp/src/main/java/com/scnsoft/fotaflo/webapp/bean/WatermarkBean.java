package com.scnsoft.fotaflo.webapp.bean;

import com.scnsoft.fotaflo.webapp.model.LocationImageMetadata;

/**
 * Created by Nadezda Drozdova
 * Date: May 20, 2011
 * Time: 12:04:05 PM
 */
public class WatermarkBean {

    String img;
    String logoImg1;
    String logoImg2;
    String logoText;
    String output;
    String watermark;
    boolean reduced;
    String resolution;
    Boolean rotated;

    LocationImageMetadata metadata;

    public WatermarkBean(String img, String logoImg1, String logoImg2, String logoText, String output, String watermark, boolean reduced, String resolution, Boolean rotated) {
        this.img = img;
        this.logoImg1 = logoImg1;
        this.logoImg2 = logoImg2;
        this.logoText = logoText;
        this.output = output;
        this.watermark = watermark;
        this.reduced = reduced;
        this.resolution = resolution;
        this.rotated = rotated;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getLogoImg1() {
        return logoImg1;
    }

    public void setLogoImg1(String logoImg1) {
        this.logoImg1 = logoImg1;
    }

    public String getLogoImg2() {
        return logoImg2;
    }

    public void setLogoImg2(String logoImg2) {
        this.logoImg2 = logoImg2;
    }

    public String getLogoText() {
        return logoText;
    }

    public void setLogoText(String logoText) {
        this.logoText = logoText;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getWatermark() {
        return watermark;
    }

    public void setWatermark(String watermark) {
        this.watermark = watermark;
    }

    public boolean isReduced() {
        return reduced;
    }

    public void setReduced(boolean reduced) {
        this.reduced = reduced;
    }

    public LocationImageMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(final LocationImageMetadata metadata) {
        this.metadata = metadata;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public Boolean getRotated() {
        return rotated;
    }

    public void setRotated(Boolean rotated) {
        this.rotated = rotated;
    }
}
