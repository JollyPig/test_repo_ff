package com.scnsoft.fotaflo.webapp.bean;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 03.12.13
 * Time: 13:27
 * To change this template use File | Settings | File Templates.
 */

public class SavedPurchaseBean {

    protected int id;
    String packages;
    String emails;
    String staff;
    String subject;
    Date startDate;
    Date endDate;
    Date startTime;
    Date endTime;
    String location;
    String camera;
    String code;
    String tag;
    String firstpicturetime;

    public SavedPurchaseBean() {
    }

    public SavedPurchaseBean(int id, String packages, String emails, String staff, String subject, Date startDate, Date endDate,
                             Date startTime, Date endTime, String location, String camera, String code, String tag, String firstpicturetime) {
        this.id = id;
        this.packages = packages;
        this.emails = emails;
        this.staff = staff;
        this.subject = subject;
        this.startDate = startDate;
        this.endDate = endDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.location = location;
        this.camera = camera;
        this.code = code;
        this.tag = tag;
        this.firstpicturetime = firstpicturetime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPackages() {
        return packages;
    }

    public void setPackages(String packages) {
        this.packages = packages;
    }

    public String getEmails() {
        return emails;
    }

    public void setEmails(String emails) {
        this.emails = emails;
    }

    public String getStaff() {
        return staff;
    }

    public void setStaff(String staff) {
        this.staff = staff;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCamera() {
        return camera;
    }

    public void setCamera(String camera) {
        this.camera = camera;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getFirstpicturetime() {
        return firstpicturetime;
    }

    public void setFirstpicturetime(String firstpicturetime) {
        this.firstpicturetime = firstpicturetime;
    }
}
