package com.scnsoft.fotaflo.webapp.model;

import com.scnsoft.fotaflo.common.model.AbstractEntity;

import javax.persistence.*;

/**
 * Created by Drazdova
 */
@Entity
@Table(name = "packages")
public class Package extends AbstractEntity {

    String packageName;
    Location location;
    Integer access;

    public Package() {
    }

    public Package(Location location, PackageAccess access) {
        this.location = location;
        if(access != null){
            this.access = access.getId();
            this.packageName = access.getValue();
        }
    }

    @Column(name = "package_name", length = 64)
    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "location_id")
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Column(name = "package_access")
    public Integer getAccess() {
        return access;
    }

    public void setAccess(Integer access) {
        this.access = access;
    }

    public enum PackageAccess {
        DEFAULT             (1),
        PHOTOBOOK           (2, "Photo Book"),
        PUBLIC_DAILY        (3, "Daily login public sale"),
        PUBLIC_INDIVIDUAL   (4, "Individual login public sale"),
        PUBLIC_TAG          (5, "Tag login public sale"),
        SINGLE_PHOTO        (6, "Single Photo Purchase From Home"),
        GROUP_PHOTO         (7, "Group Photo Purchase From Home");

        private Integer id;
        private String value;

        private PackageAccess(Integer id) {
            this(id, null);
        }

        private PackageAccess(Integer id, String value) {
            this.id = id;
            this.value = value;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
