package com.scnsoft.fotaflo.webapp.controller;

import com.scnsoft.fotaflo.common.util.JsonMapper;
import com.scnsoft.fotaflo.webapp.service.PictureService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */
@Controller
public class MainLogoController {
    protected static Logger logger = Logger.getLogger(MainLogoController.class);

    private final static String successProperty = "success";
    private final static String msgProperty = "msg";
    private PictureService pictureService;

    @RequestMapping("updateMainImageLogo.json")
    public
    @ResponseBody
    ResponseEntity<String> updateImageLogo(@RequestParam("locationIdImg") Integer locationId,
                                           @RequestParam("imageLogo") MultipartFile imageLogo) {
        Map<String, Object> modelMap = createModelMap(true);
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        try {
            pictureService.updateMainImageLogo(locationId, imageLogo, currentUser);
            modelMap.put(msgProperty, "Main image logo was updated successfully");
            modelMap.put(successProperty, true);
            logger.info("User [ " + currentUser + " ]" + " updated main image logo for location with id " + locationId);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            modelMap.put(msgProperty, "Cannot update main image logo");
            modelMap.put(successProperty, false);
            logger.error("[ " + currentUser + " ]" + " was not able to update main logo for locationId " + locationId);
        }

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("Content-Type", "text/html; charset=utf-8");
        return new ResponseEntity<String>(JsonMapper.getJson(modelMap), responseHeaders, HttpStatus.CREATED);
    }

    @RequestMapping("deleteMainImageLogo.json")
    public
    @ResponseBody
    Map<String, Object> deleteImageLogo(@RequestParam("locationIdImg") Integer locationId) {
        Map<String, Object> modelMap = createModelMap(true);
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        try {
            pictureService.deleteMainImageLogo(locationId, currentUser);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            logger.error("User [ " + currentUser + " ]" + " was not able to delete main logo for locationId " + locationId);
            modelMap.put(msgProperty, "Cannot detele main image logo");
            return modelMap;
        }
        logger.info("User [ " + currentUser + " ]" + " deleted main image logo for location with id " + locationId);
        modelMap.put(msgProperty, "Main image logo was deleted successfully");
        return modelMap;
    }

    private Map<String, Object> createModelMap(Boolean success) {
        Map<String, Object> modelMap = new HashMap<String, Object>();
        modelMap.put(successProperty, success);
        return modelMap;
    }

    @Autowired
    public void setPictureService(PictureService pictureService) {
        this.pictureService = pictureService;
    }

}
