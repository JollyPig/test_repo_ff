package com.scnsoft.fotaflo.webapp.util;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 18.03.13
 * Time: 5:28
 * To change this template use File | Settings | File Templates.
 */
public enum Resolution {
    //3.2mp (2048 x 1536) and 5m (2560x1920).
    REDUCED("0", "REDUCED", 1200, 900), STANDARD("1", "STANDARD", 1600, 1200), MP3("2", "2048 x 1536", 2048, 1536), MP5("3", "2560x1920", 2560, 1920), X16X9("4", "2944x1656", 2944, 1656);

    private String code;
    private String name;
    private int width;
    private int height;

    private Resolution(String code, String name, int width, int height) {
        this.code = code;
        this.name = name;
        this.width = width;
        this.height = height;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }


}
