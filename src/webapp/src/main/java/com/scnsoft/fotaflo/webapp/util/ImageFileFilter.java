package com.scnsoft.fotaflo.webapp.util;

import java.io.File;
import java.io.FileFilter;

/**
 * Created by Nadezda Drozdova
 * Date: Jun 22, 2011
 * Time: 2:22:03 PM
 */
public class ImageFileFilter implements FileFilter {
    private final String[] okFileExtensions = new String[]{"jpg", "png", "gif"};

    public boolean accept(File file) {
        for (String extension : okFileExtensions) {
            if (file.getName().toLowerCase().endsWith(extension)) {
                return true;
            }
        }
        return false;
    }
}
