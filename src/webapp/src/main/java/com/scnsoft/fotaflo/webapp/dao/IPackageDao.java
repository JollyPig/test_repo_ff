package com.scnsoft.fotaflo.webapp.dao;

import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.model.Package;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 31.03.2012
 * Time: 10:46:28
 * To change this template use File | Settings | File Templates.
 */
public interface IPackageDao {

    public List<Package> getPackagesByLocation(Location location);

    public List<Package> getNotPublicPackagesByLocation(Location location);

    public List<Package> getPublicPackagesByLocation(Location location, int access);

    public List getStatisticForPackage(Integer packageId);

    public List getPublicStatisticForPackage(Integer packageId);
}
