package com.scnsoft.fotaflo.webapp.service;

import com.scnsoft.fotaflo.webapp.bean.HelpYourselfSelectedPictureBean;
import com.scnsoft.fotaflo.webapp.bean.RequestBean;
import com.scnsoft.fotaflo.webapp.dao.*;
import com.scnsoft.fotaflo.webapp.model.*;
import com.scnsoft.fotaflo.webapp.util.DateConvertationUtils;
import com.scnsoft.fotaflo.webapp.util.PictureUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.*;

/**
 * Created by Nadezda Drozdova
 * Date Apr 21, 2011
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class RequestService {

    @Autowired
    private IRequestDao requestDao;

    @Autowired
    private UserService userService;

    @Autowired
    private IMainLogoDao mainLogoDao;

    @Autowired
    private IDao baseDao;

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<Integer> createHelpYourSelfPicturesSelectionModel(String userlogin, String[] ids) {
        List<Integer> helplist = new ArrayList<Integer>();

        List<Picture> pictureList = new ArrayList<Picture>();
        for (String id : ids) {
            if (id != null) {
                Integer pictureId = Integer.valueOf(id);
                Picture pic = (Picture) baseDao.retrieve(Picture.class, pictureId);
                pictureList.add(pic);
            }
        }
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        for (Picture picture : pictureList) {
            HelpYourselfSelected ps = new HelpYourselfSelected();
            ps.setSystemUser(systemUser);
            ps.setPicture(picture);
            ps.setSend_to_email_selected(1);
            ps.setNumber_pict_selected(0);
            baseDao.create(ps);
            helplist.add(ps.getId());
        }
        return helplist;
    }

    public List<HelpYourselfSelectedPictureBean> getHelpYourSelfPictureBeansAll(String userlogin, List<Integer> helpselfsIds) {
        return getHelpYourSelfPictureBeansAll(0, Integer.MAX_VALUE, userlogin, helpselfsIds);
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<HelpYourselfSelectedPictureBean> getHelpYourSelfPictureBeansAll(int start, int limit, String userlogin, List<Integer> helpselfsIds) {
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        List<HelpYourselfSelectedPictureBean> pictureBeanList = new ArrayList<HelpYourselfSelectedPictureBean>();
        List<HelpYourselfSelected> helpYourselfSelected = new ArrayList<HelpYourselfSelected>();
        if (helpselfsIds != null && !helpselfsIds.isEmpty()) {
            for (Integer helpselfsId : helpselfsIds) {
                HelpYourselfSelected hs = (HelpYourselfSelected) baseDao.retrieve(HelpYourselfSelected.class, helpselfsId);
                helpYourselfSelected.add(hs);
            }
        }
        int i = 1;
        for (HelpYourselfSelected helpSelected : helpYourselfSelected) {
            if (i > start && i <= start + limit) {
                Picture picture = helpSelected.getPicture();
                Boolean selected = picture.getSystemUsers().contains(systemUser);
                Boolean rotated = picture.getRotated() == null || picture.getRotated() == false ? false : true;
                Boolean selectedToSend = helpSelected.getSend_to_email_selected() == 1;
                Integer toPrintNumber = helpSelected.getNumber_pict_selected();
                int helpyourselfId = helpSelected.getId();

                HelpYourselfSelectedPictureBean bean = new HelpYourselfSelectedPictureBean(picture.getId(),
                        picture.getName(),
                        picture.getUrl(), String.valueOf(picture.getCamera().getId()),
                        picture.getCamera().getCameraName(),
                        DateConvertationUtils.viewStringDateFormat(picture.getCreationDate()),
                        selected, selectedToSend, toPrintNumber, helpyourselfId, rotated, picture.getPictureSize());
                Location location = picture.getCamera().getLocation();
                String locationImage = location.getLocationImageLogoUrl();
                String locationText = location.getLocationTextLogo();
                MainLogo mainLocationLogo = mainLogoDao.getMainLogoByLocation(location);
                if (locationImage != null) {
                    bean.setLogoUrl(locationImage.replace(File.separator, "/"));
                }
                if (locationText != null) {
                    bean.setLogoText(locationText);
                }
                if (mainLocationLogo != null) {
                    bean.setLogoMainUrl(mainLocationLogo.getImageMainLogo().replace(File.separator, "/"));
                }
                pictureBeanList.add(bean);
            }
            i++;
        }

        return pictureBeanList;
    }

    public List<Integer> updateHelpYourSelfPictureBeans(Object data, String userlogin) {
        List<HelpYourselfSelectedPictureBean> updatedPictureBeans = new PictureUtil().getHelpYourselfPicturesFromRequest(data);
        List<Integer> psIds = new ArrayList<Integer>();
        for (HelpYourselfSelectedPictureBean helpYourselfPictureBean : updatedPictureBeans) {
            Picture picture = (Picture) baseDao.retrieve(Picture.class, helpYourselfPictureBean.getId());
            Set<HelpYourselfSelected> helpYourSelfSelected = picture.getHelpYourselfSelected();
            Iterator iterator = helpYourSelfSelected.iterator();
            while (iterator.hasNext()) {
                HelpYourselfSelected ps = (HelpYourselfSelected) iterator.next();
                if (ps.getId() == helpYourselfPictureBean.getHelpyourselfId()) {
                    ps.setSend_to_email_selected(helpYourselfPictureBean.isSelectedToSend() ? 1 : 0);
                    ps.setNumber_pict_selected(helpYourselfPictureBean.getToPrintNumber());
                    psIds.add(helpYourselfPictureBean.getHelpyourselfId());
                    baseDao.update(ps);
                }
            }
        }
        return psIds;
    }

    public boolean createRequestFromHelpYourSelf(List<Integer> helpselfs,
                                                 List<String> emails, String otherQuestions,
                                                 String userlogin) {
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        Location location = systemUser.getLocation();
        Requests requests = new Requests();
        requests.setEmails(listToString(emails));
        requests.setComments(otherQuestions);
        requests.setSystemUser(systemUser);
        requests.setCreationDate(new Date());
        requests.setLocation(location);
        Iterator iterator = helpselfs.iterator();
        while (iterator.hasNext()) {
            Integer hyspb = (Integer) iterator.next();
            HelpYourselfSelected hys = (HelpYourselfSelected) baseDao.retrieve(HelpYourselfSelected.class, hyspb);
            if (hys != null) {
                Integer number = hys.getNumber_pict_selected();
                int isToSend = hys.getSend_to_email_selected();
                Picture picture = (Picture) baseDao.retrieve(Picture.class, hys.getPicture().getId());
                if (number > 0 || isToSend > 0) {
                    RequestsSelected rs = new RequestsSelected();
                    rs.setPicture(picture);
                    rs.setRequest(requests);
                    rs.setNumber_pict_selected(number);
                    rs.setSend_to_email_selected(isToSend);
                    baseDao.update(rs);
                }
            }
        }
        baseDao.create(requests);
        return true;
    }

    public void clearHelpYourSelfSelections(List<Integer> helpselfsIds, String userlogin) {
        Iterator iterator = helpselfsIds.iterator();
        while (iterator.hasNext()) {
            Integer hyspb = (Integer) iterator.next();
            HelpYourselfSelected helpSel = (HelpYourselfSelected) baseDao.retrieve(HelpYourselfSelected.class, hyspb);
            if (helpSel != null && helpSel.getId() > 0) {
                baseDao.delete(helpSel);
            }
        }
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<RequestBean> getRequestBeansAll(String userlogin, int locationId) {
        List<RequestBean> list = new ArrayList<RequestBean>();
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        if (!systemUser.getAccess().equals("1")) {
            Location location = systemUser.getLocation();
            List<Requests> requests = requestDao.getRequestsByLocation(location);
            Iterator iter = requests.iterator();
            while (iter.hasNext()) {
                Requests req = (Requests) iter.next();
                list.add(new RequestBean(req.getId(), req.getEmails(),
                        req.getComments(), req.getCreationDate(), countOfTotalPrint(req), countOfTotalSend(req)));
            }
        } else {
            if (locationId != 0) {
                Location location = (Location) baseDao.retrieve(Location.class, locationId);
                List<Requests> reqList = requestDao.getRequestsByLocation(location);
                for (Requests req : reqList) {
                    list.add(new RequestBean(req.getId(), req.getEmails(),
                            req.getComments(), req.getCreationDate(), countOfTotalPrint(req), countOfTotalSend(req)));
                }
            } else {
                List<Requests> reqList = baseDao.retrieveAll(Requests.class);
                for (Requests req : reqList) {
                    list.add(new RequestBean(req.getId(), req.getEmails(),
                            req.getComments(), req.getCreationDate(), countOfTotalPrint(req), countOfTotalSend(req)));
                }
            }
        }
        return list;
    }

    private Integer countOfTotalPrint(Requests req) {
        int count = 0;
        Set<RequestsSelected> requestsSelected = req.getRequestsSelected();
        for (RequestsSelected requests : requestsSelected) {
            int toPrint = requests.getNumber_pict_selected();
            count += toPrint;
        }
        return count;
    }

    private Integer countOfTotalSend(Requests req) {
        int count = 0;
        Set<RequestsSelected> requestsSelected = req.getRequestsSelected();
        for (RequestsSelected requests : requestsSelected) {
            int toSend = requests.getSend_to_email_selected();
            if (toSend == 1)
                count++;
        }
        return count;
    }

    public String listToString(List<String> emails) {
        StringBuffer email = new StringBuffer();
        String result = "";
        if (emails != null && !emails.isEmpty()) {
            Iterator iterator = emails.iterator();
            while (iterator.hasNext()) {
                email.append(iterator.next()).append(", ");
            }
            result = email.toString().trim();
            result = result.substring(0, result.length() - 1);
        }
        return result;
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public String[] refreshHelpYourSelfPicturesSelectionModel(List<Integer> helpyourselfsIds) {
        String[] ids = new String[helpyourselfsIds.size()];
        if (helpyourselfsIds.size() > 0) {
            int i = 0;
            for (Integer helpPicture : helpyourselfsIds) {
                HelpYourselfSelected hys = (HelpYourselfSelected) baseDao.retrieve(HelpYourselfSelected.class, helpPicture);
                Integer id = hys.getPicture().getId();
                ids[i] = id.toString();
                i++;
            }
        }
        return ids;
    }

    public List<Integer> deleteHelpYourSelfs(String helpsIds, String userlogin, List<Integer> helpselfs) {
        HelpYourselfSelected helpSel = (HelpYourselfSelected) baseDao.retrieve(HelpYourselfSelected.class, Integer.valueOf(helpsIds));
        if (helpSel != null) {
            if (helpselfs.size() > 0) {
                Picture pic = helpSel.getPicture();
                pic.getHelpYourselfSelected().remove(helpSel);
                helpselfs.remove(Integer.valueOf(helpsIds));
            }
            baseDao.delete(helpSel);
        }
        return helpselfs;
    }

    public void deleteRequest(String requestIds, String userlogin) {
        Requests req = (Requests) baseDao.retrieve(Requests.class, Integer.valueOf(requestIds));
        if (req != null) {
            Set<RequestsSelected> requestsSelected = req.getRequestsSelected();
            baseDao.deleteAll(requestsSelected);
            baseDao.delete(Requests.class, Integer.valueOf(requestIds));
        }
    }

    public List<String> getEmailsFromRequest(Integer requestId) {
        List<String> emails = new ArrayList<String>();
        if (requestId != -1) {
            Requests req = (Requests) baseDao.retrieve(Requests.class, Integer.valueOf(requestId));
            String email = null;
            if (req != null) {
                email = req.getEmails();
            }
            if (email != null) {
                StringTokenizer str = new StringTokenizer(email, ",");
                while (str.hasMoreElements()) {
                    String emailItem = str.nextToken().trim();
                    emails.add(emailItem);
                }
            }
        }
        return emails;
    }

    public String getEmailsStrFromRequest(Integer requestId) {
        String emails = "";
        if (requestId != -1) {
            Requests req = (Requests) baseDao.retrieve(Requests.class, Integer.valueOf(requestId));
            emails = req.getEmails();
        }
        return emails;
    }

}
