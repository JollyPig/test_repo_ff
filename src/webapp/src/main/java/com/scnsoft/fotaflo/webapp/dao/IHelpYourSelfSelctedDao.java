package com.scnsoft.fotaflo.webapp.dao;

import com.scnsoft.fotaflo.webapp.model.HelpYourselfSelected;
import com.scnsoft.fotaflo.webapp.model.Picture;
import com.scnsoft.fotaflo.webapp.model.SystemUser;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 31.03.2012
 * Time: 10:52:48
 * To change this template use File | Settings | File Templates.
 */
public interface IHelpYourSelfSelctedDao {

    public List<HelpYourselfSelected> getHelpYourselfByUser(SystemUser user);

}
