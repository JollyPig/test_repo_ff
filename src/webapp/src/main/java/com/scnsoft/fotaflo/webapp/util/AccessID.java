package com.scnsoft.fotaflo.webapp.util;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 02.02.2012
 * Time: 18:16:40
 * To change this template use File | Settings | File Templates.
 */

public enum AccessID {
    /* application users */
    ADMIN(1),               // administrator
    USER(2),                // user (client)
    /* public users */
    PUBLICUSER(3),
    PUBLICEMAILUSER(4),
    PUBLICTAGUSER(5),
    PUBLICPURCHASEUSER(6),
    PUBLICTAGCODEUSER(7),
    PUBLICTAGACODE(8);

    private int value;

    private AccessID(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

    public String getStringValue() {
        return String.valueOf(this.value);
    }

    public static boolean isPublic(int access){
        boolean isPublic = (access == AccessID.PUBLICUSER.getValue() || access == AccessID.PUBLICEMAILUSER.getValue() ||
                access == AccessID.PUBLICTAGUSER.getValue() || access == AccessID.PUBLICPURCHASEUSER.getValue());

        return isPublic;
    }

    public static boolean isUser(int access){
        boolean isUser = (access == AccessID.ADMIN.getValue() || access == AccessID.USER.getValue());

        return isUser;
    }

    public static boolean isAdmin(int access){
        boolean isUser = (access == AccessID.ADMIN.getValue());

        return isUser;
    }
}