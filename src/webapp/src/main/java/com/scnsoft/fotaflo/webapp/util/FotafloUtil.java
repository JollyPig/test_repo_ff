package com.scnsoft.fotaflo.webapp.util;

import com.scnsoft.fotaflo.webapp.bean.HelpYourselfSelectedPictureBean;
import com.scnsoft.fotaflo.webapp.bean.PictureBean;
import com.scnsoft.fotaflo.webapp.bean.PurchasePictureBean;
import com.scnsoft.fotaflo.webapp.model.UserFilter;
import com.scnsoft.fotaflo.webapp.service.PictureService;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * @author Anatoly Selitsky
 */
public abstract class FotafloUtil {

    public static int parseParameterToInteger(HttpServletRequest request, String paramName, String defaultValue) {
        return Integer.valueOf(parseParameter(request, paramName, defaultValue));
    }

    public static String parseParameter(HttpServletRequest request, String paramName, String defaultValue) {
        return (request.getParameter(paramName) != null && !request.getParameter(paramName).equals("")) ? request.getParameter(paramName) : defaultValue;
    }

    public static PurchasePictureBean createPurchasePictureBean(Map<String, String> pictureMap) {
        return new PurchasePictureBean(
                Integer.valueOf(pictureMap.get("id")),
                pictureMap.get("name"),
                pictureMap.get("url"),
                pictureMap.get("cameraId"),
                pictureMap.get("cameraName"),
                pictureMap.get("creationDate"),
                Boolean.valueOf(pictureMap.get("selected")),
                Boolean.valueOf(pictureMap.get("selectedPurchase")),
                Integer.valueOf(pictureMap.get("toPrintNumber")),
                Boolean.valueOf(pictureMap.get("addToFacebook")),
                Boolean.valueOf(pictureMap.get("addToFlickr")),
                Boolean.valueOf(pictureMap.get("rotated")), pictureMap.get("pictureSize"))
                ;
    }

    public static PictureBean createPictureBean(Map<String, String> pictureMap) {
        return new PictureBean(
                Integer.valueOf(pictureMap.get("id")),
                pictureMap.get("name"),
                pictureMap.get("url"),
                pictureMap.get("base64code"),
                pictureMap.get("cameraId"),
                pictureMap.get("cameraName"),
                pictureMap.get("creationDate"),
                Boolean.valueOf(pictureMap.get("selected")), Boolean.valueOf(pictureMap.get("rotated")), pictureMap.get("pictureSize"));
    }

    public static HelpYourselfSelectedPictureBean createHelpYourselfSelectedPictureBean(Map<String, String> pictureMap) {
        return new HelpYourselfSelectedPictureBean(
                Integer.valueOf(pictureMap.get("id")),
                pictureMap.get("name"),
                pictureMap.get("url"),
                pictureMap.get("cameraId"),
                pictureMap.get("cameraName"),
                pictureMap.get("creationDate"),
                Boolean.valueOf(pictureMap.get("selected")),
                Boolean.valueOf(pictureMap.get("selectedToSend")),
                Integer.valueOf(pictureMap.get("toPrintNumber")),
                Integer.valueOf(pictureMap.get("helpyourselfId")),
                Boolean.valueOf(pictureMap.get("rotated")),
                pictureMap.get("pictureSize")
        );
    }

    public static Filter initFilterForCatalog(HttpServletRequest request) {
        return new Filter(
                request.getParameter("startdate"),
                request.getParameter("starttime"),
                request.getParameter("enddate"),
                request.getParameter("endtime"),
                request.getParameter("location"),
                request.getParameter("camera"),
                request.getParameter("tag"));
    }

    public static void saveFilter(PictureService pictureService, Filter filter, String currentUser, HttpServletRequest request) {

        if (request.getParameter("getFilter") == null || (!request.getParameter("getFilter").equals("load") && !request.getParameter("getFilter").equals("skip"))) {
            UserFilter userFilter = new UserFilter();
            userFilter.setStartDate(filter.getStartDate());
            userFilter.setEndDate(filter.getEndDate());
            userFilter.setLocationId(filter.getLocation());
            userFilter.setCameraId(filter.getCamera());
            userFilter.setTagIds(filter.getTag());
            pictureService.saveFilter(userFilter, currentUser);
        }
    }

    public static String buildFolderPath(Date pictureDate, String location, String camera) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String formatDate = sdf.format(pictureDate);
        String[] mass = formatDate.split("-");
        StringBuilder builder = new StringBuilder();
        builder.append(location)
                .append(File.separator)
                .append(mass[0])
                .append(File.separator)
                .append(mass[1])
                .append(File.separator)
                .append(mass[2])
                .append(File.separator)
                .append(camera);
        return builder.toString();
    }

}
