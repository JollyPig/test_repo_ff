package com.scnsoft.fotaflo.webapp.model;

import com.scnsoft.fotaflo.common.model.AbstractEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Nadezda Drozdova
 * Date: May 5, 2011
 * Time: 1:55:49 PM
 */

@Entity
@Table(name = "statistic_items")
public class Statistic extends AbstractEntity {

    Package packages;
    Integer numberOfParticipantAll;
    Integer numberToPrintAll;
    Date creationDate;
    String anyPictureName;
    String emails;
    String subscribeEmails;
    String emailSubject;
    String teasers;
    String cameraNames;
    Integer totalPictureNumber;
    private Set<Staff> staff = new HashSet<Staff>(0);

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "packages_id")
    public Package getPackages() {
        return packages;
    }

    public void setPackages(Package packages) {
        this.packages = packages;
    }

    @Column(name = "number_participant")
    public Integer getNumberOfParticipantAll() {
        return numberOfParticipantAll;
    }

    public void setNumberOfParticipantAll(Integer numberOfParticipantAll) {
        this.numberOfParticipantAll = numberOfParticipantAll;
    }

    @Column(name = "number_printed")
    public Integer getNumberToPrintAll() {
        return numberToPrintAll;
    }

    public void setNumberToPrintAll(Integer numberToPrintAll) {
        this.numberToPrintAll = numberToPrintAll;
    }

    @Column(name = "camera_names")
    public String getCameraNames() {
        return cameraNames;
    }

    public void setCameraNames(String cameraNames) {
        this.cameraNames = cameraNames;
    }

    @Column(name = "creation_date")
    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinTable(name = "staff_statistic", joinColumns = {@JoinColumn(name = "statistic_id")}, inverseJoinColumns = {@JoinColumn(name = "m_staff_id")})
    public Set<Staff> getStaff() {
        return staff;
    }

    public void setStaff(Set<Staff> staff) {
        this.staff = staff;
    }

    @Column(name = "emails", length = 4096)
    public String getEmails() {
        return emails;
    }

    public void setEmails(String emails) {
        this.emails = emails;
    }

    @Column(name = "subscribeEmails")
    public String getSubscribeEmails() {
        return subscribeEmails;
    }

    public void setSubscribeEmails(String subscribeEmails) {
        this.subscribeEmails = subscribeEmails;
    }

    @Column(name = "teaserEmails")
    public String getTeasers() {
        return teasers;
    }

    public void setTeasers(String teasers) {
        this.teasers = teasers;
    }

    @Column(name = "totalPictureNumber")
    public Integer getTotalPictureNumber() {
        return totalPictureNumber;
    }

    public void setTotalPictureNumber(Integer totalPictureNumber) {
        this.totalPictureNumber = totalPictureNumber;
    }

    @Column(name = "anyPictureName")
    public String getAnyPictureName() {
        return anyPictureName;
    }

    public void setAnyPictureName(String anyPictureName) {
        this.anyPictureName = anyPictureName;
    }

    @Column(name = "emailSubject")
    public String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }
}
