package com.scnsoft.fotaflo.webapp.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Paradinets Tatsiana
 * Date: 13.05.2011
 * Time: 0:48:41
 */
public class StatisticBean {

    Date date;
    com.scnsoft.fotaflo.webapp.model.Package packages;
    Long participantNumber;
    Long printNumber;
    Long totalpackages;
    Long totalPictures;
    List<StatisticStaffBean> staff = new ArrayList<StatisticStaffBean>();

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public com.scnsoft.fotaflo.webapp.model.Package getPackages() {
        return packages;
    }

    public void setPackages(com.scnsoft.fotaflo.webapp.model.Package packages) {
        this.packages = packages;
    }

    public Long getParticipantNumber() {
        return participantNumber;
    }

    public void setParticipantNumber(Long participantNumber) {
        this.participantNumber = participantNumber;
    }

    public Long getPrintNumber() {
        return printNumber;
    }

    public void setPrintNumber(Long printNumber) {
        this.printNumber = printNumber;
    }

    public Long getTotalpackages() {
        return totalpackages;
    }

    public void setTotalpackages(Long totalpackages) {
        this.totalpackages = totalpackages;
    }

    public List getStaff() {
        return staff;
    }

    public void setStaff(List staff) {
        this.staff = staff;
    }

    public Long getTotalPictures() {
        return totalPictures;
    }

    public void setTotalPictures(Long totalPictures) {
        this.totalPictures = totalPictures;
    }
}
