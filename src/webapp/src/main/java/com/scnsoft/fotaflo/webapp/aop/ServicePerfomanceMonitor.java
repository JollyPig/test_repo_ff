package com.scnsoft.fotaflo.webapp.aop;

import com.scnsoft.fotaflo.common.util.ReflectionUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class ServicePerfomanceMonitor extends AbstractPerfomanceMonitor {

    @Around("mailpicture() || markpicture() || resizepicture() || uploadpicture() || sendtoftppicture() || getbpicture() || savetodbpicture()")
    public Object doMonitoring(ProceedingJoinPoint point){
        long time = System.currentTimeMillis();
        Object value = null;
        try {
            value = point.proceed();
        }catch (Throwable e) {
            logger.error("Error has been thrown by " + point.getSignature().toShortString(), e);
            throw new RuntimeException(e);
        }finally {
            int loadTime = (int)(System.currentTimeMillis() - time);
            try{
                String mapping = ReflectionUtil.getMethodMapping(((MethodSignature)point.getSignature()).getMethod());
                logLoadTime(mapping, "Monitoring information of picture uploading, saving, processing and obtaining", loadTime);
            }catch (Exception e){
                logger.warn(e.getMessage(), e);
            }
        }

        return value;
    }

    @Pointcut("execution(public * com.scnsoft.fotaflo.webapp.util.MailSenderUtil.sendMailMessage(..))")
    public void mailpicture(){}
    @Pointcut("execution(public * com.scnsoft.fotaflo.webapp.util.MailSenderUtil.uploadPicturesToFTP(..))")
    public void sendtoftppicture(){}

    @Pointcut("execution(public * com.scnsoft.fotaflo.webapp.util.ImageWatermark.*(..))")
    public void markpicture(){}

    @Pointcut("execution(public * com.scnsoft.fotaflo.webapp.util.PictureReseizer.*(..))")
    public void resizepicture(){}

    @Pointcut("execution(public * com.scnsoft.fotaflo.webapp.controller.UploadController.savePicture(..))")
    public void uploadpicture(){}

    @Pointcut("execution(public * com.scnsoft.fotaflo.webapp.service.PictureService.*PictureToDB(..))")
    public void savetodbpicture(){}

    @Pointcut("execution(public * com.scnsoft.fotaflo.webapp.service.PictureService.get*Picture*(..)) " +
            "|| execution(public * com.scnsoft.fotaflo.webapp.service.PictureService.updateP*(..)) " +
            "|| execution(public * com.scnsoft.fotaflo.webapp.service.PictureService.rotateP*(..))")
    public void getbpicture(){}

}
