package com.scnsoft.fotaflo.webapp.controller;

import com.scnsoft.fotaflo.webapp.bean.PictureBean;
import com.scnsoft.fotaflo.webapp.bean.TagBean;
import com.scnsoft.fotaflo.webapp.model.BannerSlideShow;
import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.model.SystemUser;
import com.scnsoft.fotaflo.webapp.service.PictureService;
import com.scnsoft.fotaflo.webapp.service.PictureSlideShowService;
import com.scnsoft.fotaflo.webapp.service.TagService;
import com.scnsoft.fotaflo.webapp.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/rfid")
public class RfidSlideshowController {
    protected static Logger logger = Logger.getLogger(RfidSlideshowController.class);
    protected final static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    protected final Calendar calendar = Calendar.getInstance();

    @Autowired
    private UserService userService;

    @Autowired
    private TagService tagService;

    @Autowired
    private PictureService pictureService;

    @Autowired
    private PictureSlideShowService pictureSlideShowService;

    @RequestMapping(value = "/slideshow", method = RequestMethod.GET)
    public ModelAndView getPictures(ModelMap model, HttpServletRequest request) {
        logger.debug("Received request to show slideshow page");
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        SystemUser user = userService.getUserByLogin(currentUser);

        model.put("slidespeed", user.getUserSettings().getSpeed());
        model.put("slidecount", user.getUserSettings().getPictureCount());
        model.put("current", currentUser);
        model.put("userId", Integer.valueOf(user.getAccess()));
        model.put("pageSize", user.getUserSettings().getPageSize());

        Location location = user.getLocation();
        List<BannerSlideShow> bannersLeft = pictureService.getSlideShowBanner("left", location);
        List<BannerSlideShow> bannersRight = pictureService.getSlideShowBanner("right", location);
        String leftUrl = "";
        String rightUrl = "";
        if (bannersLeft.size() == 1) {
            leftUrl = bannersLeft.get(0).getBannerUrl();
        }
        if (bannersRight.size() == 1) {
            rightUrl = bannersRight.get(0).getBannerUrl();
        }
        model.put("leftUrl", leftUrl);
        model.put("rightUrl", rightUrl);
        logger.info("User [ " + currentUser + " ]" + " opened SLIDESHOW page for location " + location.getLocationName());

        ModelAndView view = new ModelAndView("rfidslideshow");

        return view;
    }

    @RequestMapping(value = "getPictures.json")
    public
    @ResponseBody
    Map<String, ? extends Object> loadPictures(HttpServletRequest request) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        HashMap modelMap = new HashMap();
        List<PictureBean> pictureBeans = pictureSlideShowService.getPicturesForRFIDSlideShow(currentUser);
        modelMap.put("pictures", pictureBeans != null ? pictureBeans : new ArrayList<PictureBean>());
//        modelMap.put("total", pictureBeans.size());

        return modelMap;
    }

    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public
    @ResponseBody
    Map<String, Object> chechTag(@RequestParam(value = "creationDate", required = false) Long time,
                                 @RequestParam(value = "tagIdName", required = false) String tag) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();

        Map<String, Object> modelMap = new HashMap<String, Object>();
        boolean result = false;

        Date date = null;
        if (time != null) {
            date = new Date(time);
        }
        if (date == null) {
            date = getStartTime();
        }
        if (tag == null) {
            tag = "";
        }

        result = tagService.isSlideShowToBeUpdated(tag, date, userService.getUserByLogin(currentUser));
        modelMap.put("success", result);

        if (result) {
            putTags(modelMap, currentUser);
            logger.info("Slideshow must be updated");
        }

        return modelMap;
    }

    @RequestMapping(value = "/tags", method = RequestMethod.GET)
    public
    @ResponseBody
    Map<String, Object> getTags() {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();

        Map<String, Object> modelMap = new HashMap<String, Object>();

        boolean result = true;
        modelMap.put("success", result);

        putTags(modelMap, currentUser);

        return modelMap;
    }

    protected List<TagBean> putTags(Map<String, Object> modelMap, final String currentUser) {
        List<TagBean> result;
        Date startDate, endDate;
        startDate = getStartTime();
        endDate = getEndTime();
        result = tagService.getTagsByUserBetweenDates(startDate, endDate, userService.getUserByLogin(currentUser));
        modelMap.put("data", result);
        return result;
    }

    private Date getStartTime() {
        calendar.setTime(new Date());
        calendar.clear(Calendar.MILLISECOND);
        calendar.clear(Calendar.SECOND);
        calendar.clear(Calendar.MINUTE);
        if (calendar.get(Calendar.HOUR_OF_DAY) < 5) {
            calendar.add(Calendar.DAY_OF_MONTH, -1);
        }
        calendar.set(Calendar.HOUR_OF_DAY, 5);
        return calendar.getTime();
    }

    private Date getEndTime() {
        calendar.setTime(new Date());
        return calendar.getTime();
    }

}
