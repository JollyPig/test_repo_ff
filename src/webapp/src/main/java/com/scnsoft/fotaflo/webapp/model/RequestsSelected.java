package com.scnsoft.fotaflo.webapp.model;

import com.scnsoft.fotaflo.common.model.AbstractEntity;

import javax.persistence.*;

/**
 * Created by Nadezda Drozdova
 * Date: Apr 27, 2011
 * Time: 12:59:48 PM
 */
@Entity
@Table(name = "requests_selected")
public class RequestsSelected extends AbstractEntity {

    Requests request;
    Picture picture;

    Integer send_to_email_selected;
    Integer number_pict_selected;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "requests_id")
    public Requests getRequest() {
        return request;
    }

    public void setRequest(Requests request) {
        this.request = request;
    }

    @ManyToOne
    @JoinColumn(name = "picture_id")
    public Picture getPicture() {
        return picture;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    @Column(name = "send_to_email_selected")
    public Integer getSend_to_email_selected() {
        return send_to_email_selected;
    }

    public void setSend_to_email_selected(Integer send_to_email_selected) {
        this.send_to_email_selected = send_to_email_selected;
    }

    @Column(name = "number_pict_selected")
    public Integer getNumber_pict_selected() {
        return number_pict_selected;
    }

    public void setNumber_pict_selected(Integer number_pict_selected) {
        this.number_pict_selected = number_pict_selected;
    }
}
