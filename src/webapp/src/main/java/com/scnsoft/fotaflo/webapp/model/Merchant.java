package com.scnsoft.fotaflo.webapp.model;

import com.scnsoft.fotaflo.common.model.AbstractEntity;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 21.02.2012
 * Time: 15:25:02
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "merchant")
public class Merchant extends AbstractEntity {

    String account;
    String password;
    String signature;
    String environment;
    byte[] passCode;
    byte[] signCode;

    @Column(name = "account")
    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    @Transient
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Transient
    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    @Column(name = "environment")
    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    @Lob
    @Column(name = "password")
    public byte[] getPassCode() {
        return passCode;
    }

    public void setPassCode(byte[] passCode) {
        this.passCode = passCode;
    }

    @Lob
    @Column(name = "signature")
    public byte[] getSignCode() {
        return signCode;
    }

    public void setSignCode(byte[] signCode) {
        this.signCode = signCode;
    }
}