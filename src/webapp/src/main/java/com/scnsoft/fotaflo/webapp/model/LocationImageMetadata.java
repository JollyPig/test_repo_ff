package com.scnsoft.fotaflo.webapp.model;

import com.scnsoft.fotaflo.common.model.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "imageMetadata")
public class LocationImageMetadata extends AbstractEntity {
    String titleMetadata;
    String commentMetadata;
    String copyrightMetadata;

    @Column(name = "title_metadata", length = 255)
    public String getTitleMetadata() {
        return titleMetadata;
    }

    public void setTitleMetadata(final String titleMetadata) {
        this.titleMetadata = titleMetadata;
    }

    @Column(name = "comment_metadata", length = 255)
    public String getCommentMetadata() {
        return commentMetadata;
    }

    public void setCommentMetadata(final String commentMetadata) {
        this.commentMetadata = commentMetadata;
    }

    @Column(name = "copyright_metadata", length = 255)
    public String getCopyrightMetadata() {
        return copyrightMetadata;
    }

    public void setCopyrightMetadata(final String copyrightMetadata) {
        this.copyrightMetadata = copyrightMetadata;
    }
}
