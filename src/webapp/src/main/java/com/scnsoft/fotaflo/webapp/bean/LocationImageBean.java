package com.scnsoft.fotaflo.webapp.bean;

import com.scnsoft.fotaflo.webapp.model.Picture;

import java.util.Date;

public class LocationImageBean {

    Integer id;
    String name;
    String url;
    Date creationDate;
    Integer locationId;

    public LocationImageBean() {
    }

    public LocationImageBean(Picture picture, Integer locationId) {
        if (picture == null) {
            throw new IllegalArgumentException("Field 'picture' cannot be null");
        }
        this.setId(picture.getId());
        this.setName(picture.getName());
        this.setUrl(picture.getUrl());
        this.setCreationDate(picture.getCreationDate());
        this.locationId = locationId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    @Override
    public String toString() {
        return "LocationImageBean{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", creationDate=" + creationDate +
                ", locationId=" + locationId +
                '}';
    }
}
