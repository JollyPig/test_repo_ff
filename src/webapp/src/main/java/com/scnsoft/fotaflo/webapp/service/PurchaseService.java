package com.scnsoft.fotaflo.webapp.service;

import com.scnsoft.fotaflo.common.model.IEntity;
import com.scnsoft.fotaflo.common.util.NumberUtils;
import com.scnsoft.fotaflo.common.util.StringUtils;
import com.scnsoft.fotaflo.webapp.bean.*;
import com.scnsoft.fotaflo.webapp.dao.IPurchaseDao;
import com.scnsoft.fotaflo.webapp.dao.IPurchaseSelectedDao;
import com.scnsoft.fotaflo.webapp.dao.impl.BaseDao;
import com.scnsoft.fotaflo.webapp.model.*;
import com.scnsoft.fotaflo.webapp.util.*;
import org.apache.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by Nadezda Drozdova
 * Date Apr 21, 2011
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class PurchaseService {
    protected static Logger logger = Logger.getLogger(PurchaseService.class);

    @Autowired
    private LocationService locationService;

    @Autowired
    private BaseDao baseDao;

    @Autowired
    private IPurchaseDao purchaseDao;

    @Autowired
    private UserService userService;

    @Autowired
    private IPurchaseSelectedDao purchaseSelectedDao;

    @Autowired
    private PublicLoginService publicLoginService;

    @Autowired
    private PictureService pictureService;

    @Autowired
    private MailService mailService;

    @Autowired
    private PrintService printService;

    @Autowired
    private StatisticService statisticService;

    @Autowired
    private SendMessagesTask sendMessagesTask;

    @Autowired
    private CameraService cameraService;

    @Autowired
    private RequestService requestService;

    public Integer savePurchase(int status, Purchase purchase, SystemUser user, String generatedCode, String idPurchaseLocation, String idPackage, String participant, String participantOther,
                                String idStaff, String subject, String attach_filename, String email_body, Object data, Object emails, Object otherEmails, String purchaseIdCat) {
        if (purchase.getStatusCode() == 3) {
            Integer newPurchaseId = savePurchase(user, String.valueOf(purchase.getUserLocation().getId()), purchase.getCameras(), purchase.getTags(),
                    purchase.getStartDate(), purchase.getEndDate());
            purchase = getPurchaseById(newPurchaseId);
        }
        purchase.setStatusCode(status);
        purchase.setLocationPurchase((idPurchaseLocation != null && !idPurchaseLocation.equals("")) ? locationService.getLocation(Integer.valueOf(idPurchaseLocation)) : null);
        purchase.setPurchasePackage((idPackage != null && !idPackage.equals("")) ? (com.scnsoft.fotaflo.webapp.model.Package) baseDao.retrieve(com.scnsoft.fotaflo.webapp.model.Package.class, Integer.valueOf(idPackage)) : null);
        purchase.setStaffs(idStaff);
        purchase.setParticipants(participant);
        purchase.setParticipantsOther(participantOther);
        purchase.setSubject(subject);
        purchase.setSystemUser(user);
        purchase.setGeneratedCode(generatedCode);
        purchase.setEmailBody(email_body);
        purchase.setStringFileName(attach_filename);
        List<String> emailsStringList = PictureUtil.getPurchaseEmails(emails);
        List<String> emailsOtherStringList = PictureUtil.getPurchaseEmails(otherEmails);
        String emailsString = "";
        if (!participant.equals("0")) {
            for (String em : emailsStringList) {
                if (em != null && em.length() != 0) {
                    emailsString = emailsString.length() == 0 ? em : emailsString + "," + em;
                }
            }
        }
        String otherEmailsString = "";
        if (!participantOther.equals("0")) {
            for (String em : emailsOtherStringList) {
                if (em != null && em.length() != 0) {
                    otherEmailsString = otherEmailsString.length() == 0 ? em : otherEmailsString + "," + em;
                }
            }
        }

        purchase.setParticipantsEmails(emailsString);
        purchase.setParticipantsOtherEmails(otherEmailsString);

        purchase.setPurchaseDate(new Date());

        updateSavedPurchasePictureBeans(user, purchase, purchaseIdCat);

        baseDao.update(purchase);

        return purchase.getId();
    }

    public void updatePurchase(Purchase purchase, String idLocation, String cameraIds, String tagIds,
                               Date startDate, Date endDate) {
        if (purchase != null) {

            purchase.setLocation(locationService.getLocation(idLocation != null ? Integer.valueOf(idLocation) : null));
            purchase.setCameras(cameraIds);
            purchase.setTags(tagIds);

            purchase.setStartDate(startDate);
            purchase.setEndDate(endDate);
            purchase.setStatusCode(purchase.getStatusCode());
            baseDao.update(purchase);
        }
    }

    public Integer copyPurchase(Purchase purchaseToCopy) {
        //проверка есть ли такой пурчас уже
        Purchase purchase = new Purchase();
        purchase.setUserLocation(purchaseToCopy.getUserLocation());
        purchase.setLocation(purchaseToCopy.getLocation());
        purchase.setCameras(purchaseToCopy.getCameras());
        purchase.setTags(purchaseToCopy.getTags());

        purchase.setStartDate(purchaseToCopy.getStartDate());
        purchase.setEndDate(purchaseToCopy.getEndDate());
        purchase.setStatusCode(4);

        purchase.setLocationPurchase(purchaseToCopy.getLocationPurchase());
        purchase.setPurchasePackage(purchaseToCopy.getPurchasePackage());
        purchase.setStaffs(purchaseToCopy.getStaffs());
        purchase.setParticipants(purchaseToCopy.getParticipants());
        purchase.setParticipantsOther(purchaseToCopy.getParticipantsOther());
        purchase.setSubject(purchaseToCopy.getSubject());
        purchase.setSystemUser(purchaseToCopy.getSystemUser());

        purchase.setParticipantsEmails(purchaseToCopy.getParticipantsEmails());
        purchase.setParticipantsOtherEmails(purchaseToCopy.getParticipantsOtherEmails());
        purchase.setPurchaseDate(new Date());

        Integer purchaseId = baseDao.create(purchase);
        return purchaseId;
    }

    public Integer savePurchase(SystemUser user, String idLocation, String cameraIds, String tagIds,
                                Date startDate, Date endDate) {
        //проверка есть ли такой пурчас уже
        Purchase purchase = new Purchase();
        purchase.setUserLocation(user.getLocation());
        purchase.setLocation(locationService.getLocation(idLocation != null ? Integer.valueOf(idLocation) : null));
        purchase.setCameras(cameraIds);
        purchase.setTags(tagIds);

        purchase.setStartDate(startDate);
        purchase.setEndDate(endDate);
        purchase.setStatusCode(1);
        // purchase.setPurchaseDate(new Date());
        Integer purchaseId = baseDao.create(purchase);
        return purchaseId;
    }

    public void savePurchase(Purchase purchase, String firstPictureName) {
        if (purchase != null) {
            purchase.setFirstPictureTime(firstPictureName);
            baseDao.update(purchase);
        }
    }

    public String createSelectionModelFromSaved(String userLogin, int purchaseId) {
        String selected = " ";
        Purchase purchase = getPurchaseById(purchaseId);
        if (purchase != null && purchase.getStatusCode() > 1 && purchase.getStatusCode() < 4) {
            SystemUser systemUser = userService.getUserByLogin(userLogin);
            List<PurchaseSelected> inPurch = purchaseSelectedDao.getPurchsePictureByUser(systemUser);
            if (inPurch != null) {
                baseDao.deleteAll(inPurch);
            }
            if (purchase != null) {
                Set<PurchasePictures> purchaseSelected = purchase.getPurchasePictures();
                for (PurchasePictures purchases : purchaseSelected) {
                    Picture pic = purchases.getPicture();
                    PurchaseSelected ps = new PurchaseSelected();
                    ps.setSystemUser(systemUser);
                    ps.setPicture(pic);
                    ps.setSend_to_email_selected(purchases.getSend_to_email_selected());
                    if (purchases.getSend_to_email_selected() == 1) {
                        selected = selected.equals(" ") ? ps.getPicture().getId() + "" : selected + "," + ps.getPicture().getId();
                    }
                    ps.setNumber_pict_selected(purchases.getNumber_pict_selected());
                    ps.setAdd_to_facebook(0);
                    ps.setAdd_to_flickr(0);
                    baseDao.create(ps);
                }
            }
        } else {
            selected = "";
        }
        return selected;
    }

    public List<Integer> getIdStaffsFromString(String staffStr) {
        List<Integer> staffsIds = new ArrayList<Integer>();
        if (staffStr != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(staffStr, ",");
            if (!staffStr.equals("") && stringTokenizer.countTokens() == 1) {
                staffsIds.add(Integer.valueOf(staffStr.trim()));
            } else if (!staffStr.equals("") && stringTokenizer.countTokens() > 1) {
                while (stringTokenizer.hasMoreElements()) {
                    staffsIds.add(Integer.valueOf(stringTokenizer.nextToken().trim()));
                }
            }
        }
        return staffsIds;
    }

    public void updateSavedPurchasePictureBeans(SystemUser systemUser, Purchase purchase, String purchaseIdCat) {
        List<PurchaseSelected> inPurchSelected = purchaseSelectedDao.getPurchsePictureByUser(systemUser);
        inPurchSelected = inPurchSelected == null ? new ArrayList<PurchaseSelected>() : inPurchSelected;
        // List<PurchasePictureBean> updatedPictureBeans = new PictureUtil().getPurchasePicturesFromRequest(data);
        Set<PurchasePictures> purchasePictureses = purchase.getPurchasePictures();
        Picture firstPicture = null;
        if (purchasePictureses.size() == 0 || (purchaseIdCat != null && !purchaseIdCat.equals("-1")) || inPurchSelected.size() != purchasePictureses.size()) {
            if (purchasePictureses.size() != 0) {
                Iterator iterator = purchasePictureses.iterator();
                while (iterator.hasNext()) {
                    PurchasePictures ps = (PurchasePictures) iterator.next();
                    baseDao.delete(ps);
                }
            }
            Set<PurchasePictures> purchasePicturesSet = new HashSet<PurchasePictures>();

            for (PurchaseSelected inPurchaseSelected : inPurchSelected) {
                if (firstPicture == null) {
                    firstPicture = inPurchaseSelected.getPicture();
                } else {
                    Picture picture = inPurchaseSelected.getPicture();
                    if (firstPicture.getCreationDate().after(picture.getCreationDate())) {
                        firstPicture = picture;
                    }
                }
                PurchasePictures pp = new PurchasePictures();
                pp.setPicture(inPurchaseSelected.getPicture());
                pp.setSend_to_email_selected(inPurchaseSelected.getSend_to_email_selected());
                pp.setNumber_pict_selected(inPurchaseSelected.getNumber_pict_selected());
                pp.setAdd_to_facebook(inPurchaseSelected.getAdd_to_facebook());
                pp.setAdd_to_flickr(inPurchaseSelected.getAdd_to_flickr());
                pp.setPurchase(purchase);

                try {
                    baseDao.create(pp);

                } catch (ConstraintViolationException ex) {
                    logger.error(ex.getMessage() + "; constraint [" + ex.getConstraintName() + "]", ex);
                }
                purchasePicturesSet.add(pp);
            }

            purchase.setPurchasePictures(purchasePicturesSet);
        } else {
            Iterator iterator = purchasePictureses.iterator();
            while (iterator.hasNext()) {
                PurchasePictures ps = (PurchasePictures) iterator.next();

                for (PurchaseSelected inPurchaseSelected : inPurchSelected) {
                    if (inPurchaseSelected.getPicture().getId() == ps.getPicture().getId()) {
                        if (firstPicture == null) {
                            firstPicture = inPurchaseSelected.getPicture();
                        } else {
                            Picture picture = inPurchaseSelected.getPicture();
                            if (firstPicture.getCreationDate().after(picture.getCreationDate())) {
                                firstPicture = picture;
                            }
                        }
                        ps.setSend_to_email_selected(inPurchaseSelected.getSend_to_email_selected());
                        ps.setNumber_pict_selected(inPurchaseSelected.getNumber_pict_selected());
                        ps.setAdd_to_facebook(inPurchaseSelected.getAdd_to_facebook());
                        ps.setAdd_to_flickr(inPurchaseSelected.getAdd_to_flickr());
                        baseDao.update(ps);
                    }
                }
            }
        }
        purchase.setFirstPictureTime(firstPicture == null ? "" : DateConvertationUtils.getDateString(firstPicture.getCreationDate()) + " " + DateConvertationUtils.getTimeString(firstPicture.getCreationDate()));
        baseDao.update(purchase);
    }

    public Purchase getPurchaseByCode(String generatedCode) {
        Purchase purchase = null;
        return purchaseDao.getPurchaseByCode(generatedCode);
    }

    protected List<PurchaseEmail> getPurchaseEmailsByParameters(Location location, Date startDate, Date endDate) {
        return purchaseDao.getEmailsByParameters(location, startDate, endDate);
    }

    public List<PurchaseEmailBean> getPurchaseEmails(Location location, Date startDate, Date endDate) {
        List<PurchaseEmailBean> result = new ArrayList<PurchaseEmailBean>();
        LocationBean locationBean;
        for(PurchaseEmail email: getPurchaseEmailsByParameters(location, startDate, endDate)){
            if(email != null){
                locationBean = null;
                if(email.getLocation() != null){
                    locationBean = new LocationBean(email.getLocation().getId(), email.getLocation().getLocationName());
                }
                result.add(new PurchaseEmailBean(email.getEmail(), email.getDate(), locationBean));
            }
        }
        return result;
    }

    public Integer getPurchaseEmailCount(Location location, Date startDate, Date endDate) {
        List<PurchaseEmail> result = getPurchaseEmailsByParameters(location, startDate, endDate);
        if(result != null){
            return result.size();
        }
        return null;
    }

    public Purchase getPurchaseById(Integer id) {
        if (id == null || id == -1) return null;
        Purchase purchase = (Purchase) baseDao.retrieve(Purchase.class, id);
        return purchase;
    }

    public boolean removePurchase(Integer id) {
        if (id == null || id == -1) return false;
        Purchase purchase = (Purchase) baseDao.retrieve(Purchase.class, id);
        if (purchase == null) return false;
        purchase.setStatusCode(0);
        baseDao.update(purchase);
        return true;
    }

    protected Map getMap(List<? extends IEntity> entityList){  // todo make as a part of dao interface
        Map<String, IEntity> map = new HashMap<String, IEntity>();
        for (IEntity entity: entityList) {
            if(entity != null){
                map.put(String.valueOf(entity.getId()), entity);
            }
        }

        return map;
    }

    public List<TagSavedPurchaseBean> getHistoryPurchasesByParameters(Filter filter, String email, String code, String targetTag) {
        List<TagSavedPurchaseBean> savedPurchaseBeanList = new ArrayList<TagSavedPurchaseBean>();

        Location location = null;
        if (! filter.getLocation ().equals ("0")) {
            location = locationService.getLocation (Integer.valueOf (filter.getLocation ()));
        }

        List<Purchase> purchases = purchaseDao.findPurchasesByFilter (location, filter.getStartDate(), filter.getEndDate(), email, code, targetTag, 3);

        List<Staff> staffss = baseDao.retrieveAll(Staff.class);
        Map<String, Staff> staffMap = getMap(staffss);

        List<Camera> camerass = baseDao.retrieveAll(Camera.class);
        Map<String, Camera> cameraMap = getMap(camerass);

        for (Purchase purchase : purchases) {
            String staffString = "";
            if (purchase.getStaffs() != null) {
                String[] staffs = purchase.getStaffs().split(",");
                for (String st : staffs) {
                    if (!StringUtils.isEmpty(st)) {
                        if (staffMap.containsKey(st)) {
                            String stStr = staffMap.get(st).getStaffName();
                            staffString = staffString.length() == 0 ? stStr : staffString + "," + stStr;
                        }
                    }
                }
            }

            String camerasString = "";
            if (purchase.getCameras() != null) {
                String[] cameras = purchase.getCameras().split(",");
                for (String cam : cameras) {
                    if (!StringUtils.isEmpty(cam) && !cam.equals("0")) {
                        if (cameraMap.containsKey(cam)) {
                            String camStr = cameraMap.get(cam).getCameraName();
                            camerasString = camerasString.length() == 0 ? camStr : camerasString + "," + camStr;
                        }
                    }
                }
            } else {
                camerasString = "All Cameras";
            }

            String tags = purchase.getTags();

            String[] tagsId = tags != null ? tags.split(",") : "".split(",");
            String tagsString = "";
            for (String tadId : tagsId) {
                if (tadId != null && tadId.trim().length() != 0 && !tadId.trim().equals("0")) {
                    Tag tag = (Tag) baseDao.retrieve(Tag.class, Integer.valueOf(tadId));
                    if (tag != null) {
                        tagsString = tagsString.equals("") ? tag.getTagName() : tagsString + ", " + tag.getTagName();
                    }
                } else {
                    if (tadId != null && tadId.trim().length() != 0 && tadId.trim().equals("0")) {
                        tagsString = tagsString.equals("") ? "No tag" : tagsString + ", " + "No Tag";
                    }
                }
            }

            if (purchase.getGeneratedCode() != null && !purchase.getGeneratedCode().equals("")) {
                SavedPurchaseBean savedPurchaseBean = new SavedPurchaseBean(purchase.getId(), purchase.getPurchasePackage() != null ? purchase.getPurchasePackage().getPackageName() : "", purchase.getParticipantsEmails(),
                        staffString, purchase.getSubject(), purchase.getStartDate(), purchase.getEndDate(),
                        purchase.getStartDate(), purchase.getEndDate(), purchase.getLocationPurchase() != null ? purchase.getLocationPurchase().getLocationName() : "", camerasString, purchase.getGeneratedCode(), tagsString, purchase.getFirstPictureTime());

                TagSavedPurchaseBean tagSavedPurchaseBean = new TagSavedPurchaseBean(savedPurchaseBean);
                savedPurchaseBeanList.add(tagSavedPurchaseBean);
            }
        }

        return savedPurchaseBeanList;
    }

    public List<SavedPurchaseBean> getSavedPurchasesByLocation(Integer id) {
        List<SavedPurchaseBean> savedPurchaseBeanList = new ArrayList<SavedPurchaseBean>();

        List<Purchase> purchases = new ArrayList<Purchase>();
        if (id != 0) {
            Location location = locationService.getLocation(id);
            purchases = purchaseDao.getPurchaseByStatusAndLocation(location, 2);
        } else {
            purchases = purchaseDao.getPurchaseByStatus(2);
            logger.info("PurchaseService with status 2:" + purchases.size());
        }

        List<Staff> staffss = baseDao.retrieveAll(Staff.class);
        Map<String, Staff> staffMap = getMap(staffss);

        List<Camera> camerass = baseDao.retrieveAll(Camera.class);
        Map<String, Camera> cameraMap = getMap(camerass);

        for (Purchase purchase : purchases) {
            String staffString = "";
            if (purchase.getStaffs() != null) {
                String[] staffs = purchase.getStaffs().split(",");
                for (String st : staffs) {
                    if (!StringUtils.isEmpty(st)) {
                        if (staffMap.containsKey(st)) {
                            String stStr = staffMap.get(st).getStaffName();
                            staffString = staffString.length() == 0 ? stStr : staffString + "," + stStr;
                        }
                    }
                }
            }

            String camerasString = "";
            if (purchase.getCameras() != null) {
                String[] cameras = purchase.getCameras().split(",");
                for (String cam : cameras) {
                    if (!StringUtils.isEmpty(cam) && !cam.equals("0")) {
                        if (cameraMap.containsKey(cam)) {
                            String camStr = cameraMap.get(cam).getCameraName();
                            camerasString = camerasString.length() == 0 ? camStr : camerasString + "," + camStr;
                        }
                    }
                }
            } else {
                camerasString = "All Cameras";
            }

            String locationName = (purchase.getLocation() != null) ? purchase.getLocation().getLocationName() : "All";
            String tags = purchase.getTags();
            String[] tagsId = tags != null ? tags.split(",") : "".split(",");
            String tagsString = "";
            for (String tadId : tagsId) {
                if (!StringUtils.isEmpty(tadId) && !tadId.trim().equals("0")) {
                    Tag tag = (Tag) baseDao.retrieve(Tag.class, Integer.valueOf(tadId));
                    if (tag != null) {
                        tagsString = tagsString.equals("") ? tag.getTagName() : tagsString + ", " + tag.getTagName();
                    }
                } else {
                    if (!StringUtils.isEmpty(tadId) && tadId.trim().equals("0")) {
                        tagsString = tagsString.equals("") ? "No tag" : tagsString + ", " + "No Tag";
                    }
                }
            }

            SavedPurchaseBean savedPurchaseBean = new SavedPurchaseBean(purchase.getId(), (purchase.getPurchasePackage() != null) ? purchase.getPurchasePackage().getPackageName() : "", purchase.getParticipantsEmails(),
                    staffString, purchase.getSubject(), purchase.getStartDate(), purchase.getEndDate(),
                    purchase.getStartDate(), purchase.getEndDate(), locationName, camerasString, purchase.getGeneratedCode(), tagsString, purchase.getFirstPictureTime());

            savedPurchaseBeanList.add(savedPurchaseBean);
        }

        return savedPurchaseBeanList;
    }

    private Subscribe createSubscribe(Date creationDate, String email, Location location){
        Subscribe subscribe = new Subscribe();
        subscribe.setDate(creationDate);
        subscribe.setEmail(email);
        subscribe.setLocation(location);

        return subscribe;
    }

    public PurchaseEmailDetails getPurchaseEmailDetails(Purchase purchase, String staff,
                                                         Location location, SystemUser user,
                                                         Map<String, Boolean> participantEmails,
                                                         Map<String, Boolean> participantOtherEmails){
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();

        Date creationDate = new Date();
        Date publicExpirationDate = DateConvertationUtils.getStartDate(-21, creationDate);
        Date expirationDate = DateConvertationUtils.getStartDate(-365, new Date());

        PurchaseEmailDetails details = new PurchaseEmailDetails(creationDate);

        Boolean checked;
        int i = 0;
        for (String email: participantEmails.keySet()) {
            checked = participantEmails.get(email);
            i++;
            if (!StringUtils.isEmpty(email)) {
                details.addEmail(email);
                ReportEmails reportEmail = new ReportEmails();
                reportEmail.setEmail(email);
                reportEmail.setLocation(location);
                reportEmail.setCreationDate(new Date());
                details.addReportEmail(reportEmail);
                if (checked != null && checked) {
                    details.addSubscribe(createSubscribe(creationDate, email, location));
                }
                PurchaseEmail purchaseEmail = new PurchaseEmail();
                purchaseEmail.setEmail(email);
                purchaseEmail.setDate(new Date());
                purchaseEmail.setLocation(location);
                details.addPurchase(purchaseEmail);

                Map<String, String> credentials = publicLoginService.generateACodesFromPurchaseCode(
                        purchase.getGeneratedCode(), i, location, creationDate, expirationDate, String.valueOf(
                        AccessID.PUBLICPURCHASEUSER.getValue()));

                credentials.put("email", email);
                details.addCredential(credentials);
            }
        }
        logger.info("User " + currentUser + " sends emails: " + participantEmails.keySet());

        for (String email: participantOtherEmails.keySet()) {
            checked = participantOtherEmails.get(email);
            if (!StringUtils.isEmpty(email)) {
                ReportEmails reportEmail = new ReportEmails();
                reportEmail.setCreationDate(new Date());
                reportEmail.setEmail(email);
                reportEmail.setLocation(location);
                details.addReportEmail(reportEmail);

                Map<String, String> credentials = publicLoginService.createGeneralPublicLogin(location, creationDate, publicExpirationDate,
                        String.valueOf(AccessID.PUBLICEMAILUSER.getValue()), true);
                credentials.put("email", email);

                logger.info("User " + currentUser + " sends TEASING email: " + email +
                        ", login/password: " + credentials.get("login") + "/" + credentials.get("password"));

                saveUserFilter(user, credentials, staff);

                details.addTeaser(credentials);

                if (checked != null && checked) {
                    details.addSubscribe(createSubscribe(creationDate, email, location));
                }
            }
        }

        return details;
    }

    protected Map<String, String> saveUserFilter(SystemUser user, Map<String, String> credentials, String staff) {
        if(user != null && credentials != null){
            UserFilter userFilter = pictureService.getUserFilter(user);
            if (userFilter != null) {
                UserFilter publicUserFilter = new UserFilter();
                publicUserFilter.setCameraId(userFilter.getCameraId());
                publicUserFilter.setLocationId(userFilter.getLocationId());
                publicUserFilter.setStartDate(userFilter.getStartDate());
                publicUserFilter.setEndDate(userFilter.getEndDate());
                if (!StringUtils.isEmpty(staff)) {
                    publicUserFilter.setStaff(staff);
                }
                Set<PurchaseSelected> selectedPics = user.getSelectedPictures();
                List<Picture> pics = new ArrayList<Picture>();
                for (PurchaseSelected selected : selectedPics) {
                    pics.add(selected.getPicture());
                }
                publicUserFilter.setPictures(pics);
                pictureService.saveFilter(publicUserFilter, credentials.get("login"));
            } else {
                credentials.put("filter", "false");
            }
        }

        return credentials;
    }

    private void sendMessages(List<SendMessagesBean> messagesToSend){
        sendMessagesTask.setMessages(messagesToSend);
        sendMessagesTask.sendMessages();
    }

    private void sendNotifications(List<NotificationMessageBean> notificationSend, Location location, int packageId, int numberOfParticipant, List<Integer> staffsIds){
        sendMessagesTask.setDatePurchase(new Date());
        sendMessagesTask.setLocation(pictureService.getPackageLocation(packageId));
        sendMessagesTask.setNumberOfPart(String.valueOf(numberOfParticipant));
        sendMessagesTask.setPackageName(pictureService.getPackageById(packageId));
        sendMessagesTask.setStaffs(pictureService.getStaffByIds(staffsIds));
        sendMessagesTask.setFromEmail(location.getLocationFromEmail());
        sendMessagesTask.setPassword(location.getLocationSmtpServerPassword());
        sendMessagesTask.setPort(location.getLocationSmtpServerPort().toString());
        sendMessagesTask.setSmtpServer(location.getLocationSmtpServer());
        sendMessagesTask.setSsl(location.getLocationSslCheck());
        sendMessagesTask.setMessagesN(notificationSend);
        sendMessagesTask.sendNotification();
    }

    private List<SendMessagesBean> getTeaserMessages(PurchaseEmailDetails purchaseEmailDetails,
                                                     String subject, String emailBody, String attachmentBaseName,
                                                     Location location, List<PurchasePictureBean> pictures,
                                                     String currentUser,
                                                     boolean isLink){
        List<SendMessagesBean> sendBeans = new ArrayList<SendMessagesBean>();
        List<Map<String, String>> teasers = purchaseEmailDetails.getTeasers();
        if (teasers != null && !teasers.isEmpty()) {
            for (Map<String, String> teaseEmail : teasers) {
                if (!isLink) {
                    sendBeans = mailService.splitThumbnailEmail(teaseEmail, subject, emailBody, attachmentBaseName, location, pictures);
                } else {
                    sendBeans = mailService.thumbnailEmail(teaseEmail, subject, emailBody, attachmentBaseName, location, pictures);
                }
            }
            logger.info("User [ " + currentUser + " ]" + " is sending pictures to " + teasers.size() + " teasing  emails ");
        }

        return sendBeans;
    }

    private List<SendMessagesBean> getMessages(PurchaseEmailDetails purchaseEmailDetails, List<PurchasePictureBean> messageAttachment,
                                               String subject, String emailBody, String attachmentBaseName,
                                               Location location, int access, String currentUser, boolean isLink){
        List<SendMessagesBean> sendBeans = new ArrayList<SendMessagesBean>();
        SendMessagesBean sendBean = null;
        if(purchaseEmailDetails != null && messageAttachment != null && !messageAttachment.isEmpty()){
            if (!isLink) {
                List<String> emails = purchaseEmailDetails.getEmails();
                if (!emails.isEmpty()) {
                    sendBean = mailService.createPurchaseEmail(emails, subject, emailBody, attachmentBaseName, location, messageAttachment, true, "", null);
                    sendBeans.add(sendBean);
                    logger.info("User [ " + currentUser + " ]" + " is sending pictures to " + emails.size() + " emails ");
                }
            } else {
                List<Map<String, String>> toSendEmailsCredentials = purchaseEmailDetails.getCredentials();
                //This is SENDING THE LINK IF  checkbox check instead is checked.
                if (toSendEmailsCredentials.size() > 0 && AccessID.isUser(access)) {
                    for (Map<String, String> toSendEmailCredentials : toSendEmailsCredentials) {
                        sendBean = mailService.createACodeEmail(toSendEmailCredentials, subject, emailBody, location);
                        sendBeans.add(sendBean);
                    }
                    logger.info("User [ " + currentUser + " ]" + " is sending LINK TO pictures to " + toSendEmailsCredentials.size() + " emails ");
                }
            }
            logPurchasedPictures(currentUser, messageAttachment, "emails");
        }

        return sendBeans;
    }

    private List<SendMessagesBean> getShareMessages(List<PurchasePictureBean> messageAttachment, String typeLabel,
                                                    String subject, String emailBody, String attachmentBaseName,
                                                    Location location, List<String> emails,
                                                    String accessToken, String page,
                                                    String currentUser){
        List<SendMessagesBean> sendBeans = new ArrayList<SendMessagesBean>();
        if (messageAttachment != null && !messageAttachment.isEmpty()) {
            SendMessagesBean sendBean = mailService.createPurchaseEmail(emails, subject, emailBody, attachmentBaseName, location, messageAttachment, false, accessToken, page);
            sendBeans.add(sendBean);
            logger.info("User [ " + currentUser + " ]" + " is sending pictures to " + typeLabel);
            logPurchasedPictures(currentUser, messageAttachment, typeLabel);
        }

        return sendBeans;
    }

    private void saveStatisticData(PurchaseEmailDetails purchaseEmailDetails, Integer packageId, List<Integer> staffs, List<String> cameras,
                                   int numberOfParticipant, PurchasePictureBean purchaseItemFirst, int printedAmount, int totalAmount,
                                   String subject, String currentUser){
        List<Map<String, String>> toTeaseEmails = purchaseEmailDetails.getTeasers();
        List<String> toSendEmails = purchaseEmailDetails.getEmails();
        List<Subscribe> toSubscribeEmails = purchaseEmailDetails.getSubscribes();

        String purchaseEmails = "";
        String subscribeEmails = "";
        String teasingEmails = "";
        for (String purS : toSendEmails) {
            purchaseEmails = purchaseEmails.equals("") ? purS : purchaseEmails + ", " + purS;
        }
        for (Subscribe sub : toSubscribeEmails) {
            subscribeEmails = subscribeEmails.equals("") ? sub.getEmail() : subscribeEmails + ", " + sub.getEmail();
        }
        for (Map<String, String> tease : toTeaseEmails) {
            teasingEmails = teasingEmails.equals("") ? tease.get("email") : teasingEmails + ", " + tease.get("email");
        }

        statisticService.insertStatisticItems(packageId, staffs, numberOfParticipant, new Date(), printedAmount, purchaseEmails,           // fixme
                subscribeEmails, teasingEmails, totalAmount, purchaseItemFirst != null ? purchaseItemFirst.getUrl() : "", subject, cameras);
        logger.info("User [ " + currentUser + " ]" + " purchase statistic:  " + "totalPrinted: " + printedAmount + " sendedPictures: " + totalAmount);
    }

    private void updateStaffByLocation(String currentUser, Location location, String staff) {
        boolean staffsaveEnabled = (location != null && location.getStaffsave() != null && location.getStaffsave() == true);
        pictureService.updateStaffs(currentUser, staffsaveEnabled ? staff : "");
    }

    private void logPurchasedPictures(String currentUser, List<PurchasePictureBean> attachment, String type) {
        String pictures = "";
        if (attachment != null && attachment.size() != 0) {
            for (PurchasePictureBean pictBean : attachment) {
                pictures = pictures + " " + pictBean.getName();
            }
        }
        logger.info("User" + currentUser + "send to " + type + "the following pictures: " + pictures);
    }

    public List<PreviewPrintBean> doPurchase(String currentUser, int access, Location locationUser,
                                              Integer requestId, Integer purchaseId, Integer packageId, String staffs, int numberOfParticipant,
                                              String subject, String emailBody, String attachFilename, String accessToken,
                                              Map<String, Boolean> purchaseEmails, Map<String, Boolean> purchaseOtherEmails){
        long time2 = System.currentTimeMillis();
        SystemUser currSystemUser = userService.getUserByLogin(currentUser);
        boolean isPublic = AccessID.isPublic(access);

        Purchase purchase = this.getPurchaseById(purchaseId);

        List<Integer> staffsIds = getIdStaffsFromString(staffs);

        List<PurchasePictureBean> purchasePictureBeans =
                pictureService.getPurchasePictureBeansAllResult(currentUser, requestId, purchaseId, Integer.valueOf("-1")).getItems();

        long time3 = System.currentTimeMillis();
        logger.info("TIME STEP 2 GETTING PUCRHASE PICTURES:  " + (time3 - time2) / 1000 + " sec " + (time3 - time2) + " milisec ");

        PurchaseEmailDetails purchaseEmailDetails = this.getPurchaseEmailDetails(purchase, staffs, locationUser, currSystemUser,
                purchaseEmails,purchaseOtherEmails);

        long time4 = System.currentTimeMillis();
        logger.info("TIME STEP 3 CREATING GENERAL PURCHASE LOGINs:  " + (time4 - time3) / 1000 + " sec " + (time4 - time3) + " milisec ");

        pictureService.saveSubscribe(purchaseEmailDetails.getSubscribes());
        pictureService.savePurchaseStatisticEmails(purchaseEmailDetails.getPurchases());

        pictureService.saveReportEmail(purchaseEmailDetails.getReportEmails());

        List<String> toSendEmails = purchaseEmailDetails.getEmails();

        boolean sended = false;
        boolean printed = false;
        boolean emptyPurchase = true;

        int totalPrinted = 0;

        List<SendMessagesBean> messagesToSend = new ArrayList<SendMessagesBean>();
        List<NotificationMessageBean> notificationSend = new ArrayList<NotificationMessageBean>();

        List<PreviewPrintBean> picturesToPrint = new ArrayList<PreviewPrintBean>();

        List<String> toFlickrSendEmails = new ArrayList<String>();

        String facebookPage = "";

        String errorMessage = "";

        String toFacebookEmail = locationUser.getLocationToFacebookEmail();
        String toFlickrEmail = locationUser.getLocationToFlickrEmail();
        if (!StringUtils.isEmpty(toFlickrEmail)) {
            toFlickrSendEmails.add(toFlickrEmail);
        }
        if (!StringUtils.isEmpty(toFacebookEmail)) {
            facebookPage = toFacebookEmail;
        }

        String fromEmail = locationUser.getLocationFromEmail();
        String smtpServer = locationUser.getLocationSmtpServer();

        List<PurchasePictureBean> messageAttachment = new ArrayList<PurchasePictureBean>();
        List<PurchasePictureBean> messageFlickrAttachment = new ArrayList<PurchasePictureBean>();
        List<PurchasePictureBean> messageFacebookAttachment = new ArrayList<PurchasePictureBean>();

        int totalSize = 0;

        long time5 = System.currentTimeMillis();
        logger.info("TIME STEP 4 SUBSCRIBING REPORTS:  " + (time5 - time4) / 1000 + " sec " + (time5 - time4) + " milisec ");

        pictureService.updateStringFileName(currentUser, attachFilename);
        pictureService.updateMessageBody(currentUser, emailBody);

        PurchasePictureBean firstPurchaseItem = null;
        List<String> camerasList = new ArrayList<String>();
        Camera camera;
        Location location;
        for (PurchasePictureBean purchaseItem : purchasePictureBeans) {
            purchaseItem = pictureService.checkPictureExistence(purchaseItem);
            if (purchaseItem == null) {
                logger.error("PICTURE CAN NOT BE FOUND");
                continue;
            }

            camera = cameraService.getCamera(NumberUtils.getInteger(purchaseItem.getCameraId()));
            if(camera == null){
                logger.error("CAMERA CAN NOT BE FOUND");
                continue;
            }
            location = camera.getLocation();

            updateStaffByLocation(currentUser, location, staffs);

            Boolean toEmailSelected = purchaseItem.isSelectedPurchase();
            Boolean toFacebook = purchaseItem.getAddToFacebook();
            Boolean toFlickr = purchaseItem.getAddToFlickr();
            if (toEmailSelected || toFacebook || toFlickr) {
                emptyPurchase = false;
            }

            if (toEmailSelected) {
                totalSize++;
                if (!camerasList.contains(camera.getCameraName())) {
                    camerasList.add(camera.getCameraName());
                }

                if (firstPurchaseItem == null) {
                    firstPurchaseItem = purchaseItem;
                } else {
                    if (firstPurchaseItem.getDate().after(purchaseItem.getDate())) {
                        firstPurchaseItem = purchaseItem;
                    }
                }
            }

            int numbersToPrint = purchaseItem.getToPrintNumber();
            totalPrinted += numbersToPrint;

            if (numbersToPrint > 0) {
                picturesToPrint.add(printService.createPreviewPrintBean(purchaseItem, location, numbersToPrint));
                printed = true;
                if (!printed) {
                    errorMessage = "Error of printing";
                    break;
                }
            } else {
                printed = true;
            }

            if (toFacebook) {        // todo move validation to start of this controller
                if (StringUtils.isEmpty(toFacebookEmail)) {
                    sended = false;
                    errorMessage = "Please, define the Facebook Email in the settings";
                    break;
                }
            }
            if (toFlickr) {          // todo move validation to start of this controller
                if (StringUtils.isEmpty(toFlickrEmail)) {
                    sended = false;
                    errorMessage = "Please, define the Flickr Email in the settings";
                    break;
                }
            }
            // todo move validation to start of this controller
            if ((!toSendEmails.isEmpty() && toEmailSelected) ||
                    (!toFlickrSendEmails.isEmpty() && toFlickr) ||
                    (!facebookPage.equals("") && toFacebook)) {
                if (fromEmail == null || fromEmail.equals("")) {
                    sended = false;
                    errorMessage = "Please, define the SMTP Account(From Email) in the settings";
                    break;
                }
                if (smtpServer == null || smtpServer.equals("")) {
                    sended = false;
                    errorMessage = "Please, define the SMTP Server in the settings";
                    break;
                }

                if (toEmailSelected) {
                    messageAttachment.add(purchaseItem);
                    sended = true;
                }
                if (toFlickr) {
                    messageFlickrAttachment.add(purchaseItem);
                    sended = true;
                }
                if (toFacebook) {
                    messageFacebookAttachment.add(purchaseItem);
                    sended = true;
                }
            }

            if (numbersToPrint > 0 || !toSendEmails.isEmpty() ||
                    !toFlickrSendEmails.isEmpty() || !facebookPage.equals("")) {
                List<String> allEmails = new ArrayList<String>();
                if (!toSendEmails.isEmpty() && toEmailSelected) {
                    allEmails.addAll(toSendEmails);
                }
                if (!toFlickrSendEmails.isEmpty() && toFlickr) {
                    allEmails.addAll(toFlickrSendEmails);
                }

                NotificationMessageBean notificationBean =
                        mailService.createNotificationMessage(allEmails, purchaseItem, attachFilename, numbersToPrint);
                notificationSend.add(notificationBean);
            }
        }

        long time6 = System.currentTimeMillis();
        logger.info("TIME STEP 5 PREPARING PURCHASE ITEMS:  " + (time6 - time5) / 1000 + " sec " + (time6 - time5) + " milisec ");

        List<SendMessagesBean> sendBeans;
        boolean linkInstead = locationUser != null && locationUser.getFwpenable() != null ? locationUser.getFwpenable() : false;

        sendBeans = getTeaserMessages(purchaseEmailDetails, subject, emailBody, attachFilename,
                locationUser, purchasePictureBeans, currentUser, linkInstead);
        if(sendBeans != null && !sendBeans.isEmpty()){
            messagesToSend.addAll(sendBeans);
            sended = true;
        }

        long time7 = System.currentTimeMillis();
        logger.info("TIME STEP 6 SPLITTING SUBNAIL EMAILS:  " + (time7 - time6) / 1000 + " sec " + (time7 - time6) + " milisec ");

        sendBeans = getMessages(purchaseEmailDetails, messageAttachment, subject, emailBody, attachFilename,
                locationUser, access, currentUser, linkInstead);
        if(sendBeans != null && !sendBeans.isEmpty()){
            messagesToSend.addAll(sendBeans);
            sended = true;
        }

        long time8 = System.currentTimeMillis();
        logger.info("TIME STEP 7 CREATING PURCHASE EMEAILS:  " + (time8 - time7) / 1000 + " sec " + (time8 - time7) + " milisec ");

        sendBeans = getShareMessages(messageFacebookAttachment, "facebook", subject, emailBody, attachFilename,
                locationUser, null, accessToken, facebookPage, currentUser);
        if(sendBeans != null && !sendBeans.isEmpty()){
            messagesToSend.addAll(sendBeans);
        }
        sendBeans = getShareMessages(messageFlickrAttachment, "flickr", subject, emailBody, attachFilename,
                locationUser, toFlickrSendEmails, "", null, currentUser);
        if(sendBeans != null && !sendBeans.isEmpty()){
            messagesToSend.addAll(sendBeans);
        }

        if (sended && printed) {
            if (!purchasePictureBeans.isEmpty() && !isPublic) {
                pictureService.clearPurchaseSelections(purchasePictureBeans, currentUser);
            }

            saveStatisticData(purchaseEmailDetails, packageId, staffsIds, camerasList, numberOfParticipant, firstPurchaseItem,
                    totalPrinted, totalSize, subject, currentUser);

            if (access == AccessID.PUBLICUSER.getValue() || access == AccessID.PUBLICEMAILUSER.getValue() ||
                    access == AccessID.PUBLICTAGUSER.getValue()) {
                String email = !purchaseEmailDetails.getEmails().isEmpty() ? purchaseEmailDetails.getEmails().get(0) : "";
                statisticService.insertPublicStatisticItems(packageId, email, currentUser, new Date(), totalSize);         // fixme
            }
            if (requestId != null && !requestId.equals(-1)) {
                requestService.deleteRequest(String.valueOf(requestId), currentUser);
            }
            long time9 = System.currentTimeMillis();
            logger.info("TIME STEP 8 BEFORE SENDING EMEAILS:  " + (time9 - time8) / 1000 + " sec " + (time9 - time8) + " milisec ");

            sendMessages(messagesToSend);

            long time10 = System.currentTimeMillis();
            logger.info("TIME STEP 8-1 SENT MESSAGES:  " + (time10 - time9) / 1000 + " sec " + (time10 - time9) + " milisec ");

            sendNotifications(notificationSend, locationUser, packageId, numberOfParticipant, staffsIds);

            long time11 = System.currentTimeMillis();
            logger.info("TIME STEP 8-2 SENT NOTIFICATIONS:  " + (time11 - time10) / 1000 + " sec " + (time11 - time10) + " milisec ");

            long time12 = System.currentTimeMillis();
            logger.info("TIME STEP 9 THREADS STARTED:  " + (time12 - time11) / 1000 + " sec " + (time12 - time11) + " milisec ");
        } else {
            long time10 = System.currentTimeMillis();
            if (errorMessage.equals("") && totalPrinted == 0 && emptyPurchase) {
                errorMessage += "No pictures are selected for purchase";
                logger.warn("User [ " + currentUser + " ]" + " selected no pictures for purchase  ");
            } else {
                if (errorMessage.equals("") && (!sended || !printed)) {
                    if (!sended) {
                        errorMessage += "The problem with sending. See the log file for more detail information.";
                    }
                    if (!printed) {
                        errorMessage += "The problem with printing. See the log file for more detail information.";
                    }
                    logger.warn("User [ " + currentUser + " ] " + errorMessage);
                }
            }
            long time11 = System.currentTimeMillis();
            logger.info("TIME STEP 10 ERROR:  " + (time11 - time10) / 1000 + " sec " + (time11 - time10) + " milisec ");
            throw new RuntimeException(errorMessage);

        }

        return picturesToPrint;
    }

}
