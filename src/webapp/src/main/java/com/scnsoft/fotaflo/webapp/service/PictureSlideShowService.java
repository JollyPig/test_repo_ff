package com.scnsoft.fotaflo.webapp.service;

import com.scnsoft.fotaflo.webapp.bean.PictureBean;
import com.scnsoft.fotaflo.webapp.common.ResultList;
import com.scnsoft.fotaflo.webapp.dao.*;
import com.scnsoft.fotaflo.webapp.model.*;
import com.scnsoft.fotaflo.webapp.util.AccessID;
import com.scnsoft.fotaflo.webapp.util.DateConvertationUtils;
import com.scnsoft.fotaflo.webapp.util.DefaultValues;
import com.scnsoft.fotaflo.webapp.util.Filter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.*;

/**
 * @author Anatoly Selitsky
 */
@Service
public class PictureSlideShowService {
    public static Logger logger = Logger.getLogger(PictureSlideShowService.class);

    @Autowired
    private SettingsService settingsService;

    @Autowired
    private IPictureSlideShowDao pictureSlideShowDao;

    @Autowired
    private UserService userService;

    @Autowired
    private IFilterDao filterDao;

    @Autowired
    private IDao baseDao;

    @Autowired
    private ICameraDao cameraDao;

    @Autowired
    private TagService tagService;

    @Autowired
    private IMainLogoDao mainLogoDao;

    public ResultList<Picture> getPicturesFilteredList(String userLogin, Filter filter, int start, int limit) {
        SystemUser systemUser = userService.getUserByLogin(userLogin);
        UserFilter userFilter = getUserFilter(userLogin);
        if (String.valueOf(AccessID.PUBLICTAGUSER.getValue()).equals(systemUser.getAccess())) {
            return pictureSlideShowDao.findExtendedTagOnly(start, limit, Arrays.asList(tagService.getTagByNameLocation(userLogin, systemUser.getLocation())));
        }
        if (userFilter != null && userFilter.getPictures().size() > 0) {
            return getPicturesFromFilter(userFilter, start, limit);
        }
        List<String> tags = null;
        if (filter.getTag() != null) {
            tags = Arrays.asList(filter.getTag().split(","));
        }
        Date startDate = filter.getStartDate(),
                endDate = filter.getEndDate();
        String location = filter.getLocation(),
                camera = filter.getCamera();
        if (tags == null || tags.size() == 0 || "".equals(filter.getTag())) {
            return pictureSlideShowDao.findExtended(
                    start, limit, startDate, endDate, getCameraList(location, camera, systemUser));
        } else {
            if (tags.size() > 1 || !tags.contains("0")) {

                return pictureSlideShowDao.findExtendedByTag(
                        start, limit, startDate, endDate, getCameraList(location, camera, systemUser), getTagList(tags)
                );
            }
            if (tags.contains("0")) {
                return pictureSlideShowDao.findExtendedWithoutTag(
                        start, limit, startDate, endDate, getCameraList(location, camera, systemUser)
                );
            }
        }
        return new ResultList<Picture>(0L, new ArrayList<Picture>());
    }

    protected ResultList<Picture> getPicturesFromFilter(UserFilter userFilter, int start, int limit){
        List<Picture> pictures = userFilter.getPictures();
        long total = 0;
        List<Picture> result = new ArrayList<Picture>();
        if(pictures != null && !pictures.isEmpty()){
            total = pictures.size();
            if(start < 0){
                start = 0;
            }
            if(limit > 0){
                for(int i=start; i<(start+limit)&&i<pictures.size(); i++){
                    result.add(pictures.get(i));
                }
            }else{
                result = pictures;
            }
        }

        return new ResultList<Picture>(total, result);
    }

    @SuppressWarnings("unchecked")
    public List<Camera> getCameraList(String location, String camera, SystemUser systemUser) {
        List<Camera> cameras = new ArrayList<Camera>();
        if (camera == null || "".equals(camera)) {
            logger.info("No cameras were retrieved because cameraCombo is null or empty");
            return cameras;
        }
        List<String> camerasSelected = Arrays.asList(camera.split(","));
        if (!AccessID.ADMIN.getStringValue().equals(systemUser.getAccess())) {
            Location userLocation = systemUser.getLocation();
            if (camerasSelected.contains("0")) {
                cameras = cameraDao.getCamerasByLocation(userLocation);
            } else {
                for (String cameraId : camerasSelected) {
                    Camera currentCamera = (Camera) baseDao.retrieve(Camera.class, Integer.valueOf(cameraId));
                    if (currentCamera != null) {
                        cameras.add(currentCamera);
                    }
                }
            }
        } else {
            if (location == null || location.length() == 0) {
                logger.info("No cameras were retrieved because locationCombo is null or empty while user is admin");
                return cameras;
            }
            if ("0".equals(location)) {
                if (camerasSelected.contains("0")) {
                    cameras = (List<Camera>) baseDao.retrieveAll(Camera.class);
                } else {
                    for (String cameraId : camerasSelected) {
                        Camera currentCamera = (Camera) baseDao.retrieve(Camera.class, Integer.valueOf(cameraId));
                        if (currentCamera != null) {
                            cameras.add(currentCamera);
                        }
                    }
                }
            } else {
                Location userLocation = (Location) baseDao.retrieve(Location.class, Integer.valueOf(location));
                if (camerasSelected.contains("0")) {
                    cameras = cameraDao.getCamerasByLocation(userLocation);
                } else {
                    for (String cameraId : camerasSelected) {
                        Camera currentCamera = (Camera) baseDao.retrieve(Camera.class, Integer.valueOf(cameraId));
                        if (currentCamera != null) {
                            cameras.add(currentCamera);
                        }
                    }
                }
            }

        }
        return cameras;
    }

    public List<Tag> getTagList(List<String> tagIds) {
        List<Tag> tags = new ArrayList<Tag>();
        for (String tagId : tagIds) {
            Tag tg = (Tag) baseDao.retrieve(Tag.class, Integer.valueOf(tagId));
            if (tg != null) {
                tags.add(tg);
            }
        }
        return tags;
    }

    public UserFilter getUserFilter(String userlogin) {
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        UserFilter userFilter = filterDao.getFilterByUser(systemUser);
        if (userFilter != null) {
            return userFilter;
        }
        return null;
    }

    protected Map<String, String> getMainLogoMap(){
        List<MainLogo> mainLocationLogos = (List<MainLogo>) baseDao.retrieveAll(MainLogo.class);
        Map<String, String> logosMap = new HashMap<String, String>();
        if (mainLocationLogos.size() != 0) {
            for (MainLogo mainLogo : mainLocationLogos) {
                if (mainLogo != null && mainLogo.getLocation() != null) {
                    String locationName = mainLogo.getLocation().getLocationName();
                    String imageMainLogo = mainLogo.getImageMainLogo();
                    if (imageMainLogo != null) {
                        logosMap.put(locationName, imageMainLogo.replace(File.separator, "/"));
                    } else {
                        logosMap.put(locationName, "");
                    }
                }
            }
        }

        return logosMap;
    }

    @SuppressWarnings("unchecked")
    public List<PictureBean> getPicturesFilteredPictureList(String userLogin, List<Picture> pictureList, Date fromDate) {
        SystemUser systemUser = userService.getUserByLogin(userLogin);
        Collections.sort(pictureList);

        Map<String, String> logosMap = getMainLogoMap();

        List<PictureBean> pictureBeanList = new ArrayList<PictureBean>();

        for (Picture picture : pictureList) {
            if (picture != null) {
                if(fromDate != null && fromDate.after(picture.getCreationDate())){
                    continue;
                }
                Boolean selected = picture.getSystemUsers().contains(systemUser);
                Location location = picture.getCamera().getLocation();
                if (picture.getCamera() != null && location != null) {
                    Boolean rotated = picture.getRotated() == null || picture.getRotated() == false ? false : true;
                    PictureBean pictureBean = new PictureBean(picture.getId(), picture.getName(),
                            picture.getUrl(), String.valueOf(picture.getCamera().getId()),
                            picture.getCamera().getCameraName(),
                            DateConvertationUtils.viewStringDateFormat(picture.getCreationDate()), selected, rotated, picture.getPictureSize());

                    String locationImage = location.getLocationImageLogoUrl();
                    String locationText = location.getLocationTextLogo();
                    pictureBean.setLogoUrl(locationImage != null ? locationImage.replace(File.separator, "/") : "");
                    pictureBean.setLogoText(locationText != null ? locationText : "");
                    String mainLogo = logosMap.get(location.getLocationName());
                    pictureBean.setLogoMainUrl(mainLogo != null ? mainLogo : "");

                    if (picture.getTags() != null && !picture.getTags().isEmpty()) {
                        Tag tag = picture.getTags().iterator().next();
                        if (tag != null) {
                            pictureBean.setTag(tag.getTagName());
                        }
                    }

                    pictureBeanList.add(pictureBean);
                }
            }
        }

        return pictureBeanList;
    }

    @SuppressWarnings("unchecked")
    public List<PictureBean> getPicturesFilteredPictureList(String userLogin, List<Picture> pictureList) {
        return getPicturesFilteredPictureList(userLogin, pictureList, null);
    }

    public List<PictureBean> getPicturesForRFIDSlideShow(String userLogin) {
        SystemUser systemUser = userService.getUserByLogin(userLogin);

        String mainLogo = "";
        if (systemUser != null && systemUser.getLocation() != null) {
            MainLogo logo = mainLogoDao.getMainLogoByLocation(systemUser.getLocation());
            if (logo != null) {
                String imageMainLogo = logo.getImageMainLogo();
                if (imageMainLogo != null) {
                    mainLogo = imageMainLogo.replace(File.separator, "/");
                }
            }
        }

        List<PictureBean> pictureBeanList = new ArrayList<PictureBean>();

        if (systemUser != null && systemUser.getUserSettings() != null && systemUser.getUserSettings().getLastTag() != null) {
            Tag userTag = systemUser.getUserSettings().getLastTag();
            if (systemUser.getUserSettings().getStartTime() == null) {
                settingsService.updateStartTimeSettings(userLogin, DefaultValues.startTime);
            }
            Integer startTime = systemUser.getUserSettings().getStartTime();
            Date startDate = null;
            if (startTime != null && startTime > 0) {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.SECOND, -startTime);
                startDate = calendar.getTime();
            }
            logger.info("date: " + new Date() + ", startTime: " + startTime + ", startDate: " + startDate);

            List<Tag> tagsDependant = tagService.getDependantTags(null, null, String.valueOf(userTag.getId()));
            for (Tag tagDependant : tagsDependant) {
                if (tagDependant != null && tagDependant.getTagedpictures() != null) {
                    Set<Picture> tagPicturesSet = tagDependant.getTagedpictures();
                    for (Picture picture : tagPicturesSet) {
                        if (picture != null && picture.getCreationDate() != null) {
                            if (startDate != null && startDate.after(picture.getCreationDate())) {
                                continue;
                            }
                            Boolean selected = picture.getSystemUsers().contains(systemUser);
                            if (picture.getCamera() != null && picture.getCamera().getLocation() != null) {
                                Location location = picture.getCamera().getLocation();
                                Boolean rotated = picture.getRotated() == null || picture.getRotated() == false ? false : true;
                                PictureBean pictureBean = new PictureBean(picture.getId(), picture.getName(),
                                        picture.getUrl(), String.valueOf(picture.getCamera().getId()),
                                        picture.getCamera().getCameraName(),
                                        DateConvertationUtils.viewStringDateFormat(picture.getCreationDate()), selected, rotated, picture.getPictureSize());

                                String locationImage = location.getLocationImageLogoUrl();
                                String locationText = location.getLocationTextLogo();
                                pictureBean.setLogoUrl(locationImage != null ? locationImage.replace(File.separator, "/") : "");
                                pictureBean.setLogoText(locationText != null ? locationText : "");
                                pictureBean.setLogoMainUrl(mainLogo != null ? mainLogo : "");
                                pictureBean.setTag(tagDependant.getTagName());
                                pictureBeanList.add(pictureBean);
                            }
                        }

                    }
                }
            }
        }

        return pictureBeanList;
    }

}
