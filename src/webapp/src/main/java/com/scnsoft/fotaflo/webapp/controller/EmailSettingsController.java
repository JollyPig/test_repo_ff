package com.scnsoft.fotaflo.webapp.controller;

import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.service.LocationService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nadezda Drozdova
 * Date: May 4, 2011
 * Time: 12:40:25 PM
 */
@Controller
public class EmailSettingsController {

    protected static Logger logger = Logger.getLogger(EmailSettingsController.class);
    private final static String successProperty = "success";
    private final static String msgProperty = "msg";

    private LocationService locationService;

    @RequestMapping("saveEmailSettings.json")
    public
    @ResponseBody
    Map<String, Object> saveSettings(@RequestParam("locationId") String locId,
                                     @RequestParam("toFlickrEmail") String toFlickrEmail,
                                     @RequestParam("toFacebookEmail") String toFacebookEmail,
                                     @RequestParam("smtpServer") String smtpServer,
                                     @RequestParam("fromEmail") String fromEmail,
                                     @RequestParam("smtpServerFromPassword") String smtpServerFromPassword,
                                     @RequestParam("smtpServerPort") String smtpServerPort,
                                     @RequestParam(value = "toEmail", required = false) String toEmail,
                                     @RequestParam(value = "sslcheck", required = false) String sslcheck,
                                     @RequestParam(value = "photobook_email", required = false) String photobookEmail) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        Map<String, Object> modelMap = new HashMap<String, Object>();
        boolean ok = true;
        String msg = "";
        if (locId != null && !locId.equals("")) {
            Integer locationId = Integer.valueOf(locId);
            if (toFlickrEmail != null && !toFlickrEmail.equals(""))
                locationService.updateLocationToFlickrEmail(locationId, toFlickrEmail.trim());
            if (toFacebookEmail != null && !toFacebookEmail.equals(""))
                locationService.updateLocationToFacebookEmail(locationId, toFacebookEmail.trim());
            if (smtpServer != null && !smtpServer.equals("")) {
                locationService.updateLocationSmtpServer(locationId, smtpServer.trim());
                ok = true;
            } else {
                ok = false;
                msg = "Enter Valid SMTP Server";
            }
            if (fromEmail != null && !fromEmail.equals(""))
                locationService.updateLocationFromEmail(locationId, fromEmail.trim());
            if (smtpServerFromPassword != null && !smtpServerFromPassword.equals(""))
                locationService.updateLocationFromEmailPassword(locationId, smtpServerFromPassword.trim());
            if (smtpServerPort != null && !smtpServerPort.equals("")) {
                Integer port = Integer.valueOf(smtpServerPort.trim());
                locationService.updateLocationSmtpServerPort(locationId, port);
            }
            if (toEmail != null)
                locationService.updateLocationToEmail(locationId, toEmail.trim());
            if (photobookEmail != null)
                locationService.updateLocationToPhotobookEmail(locationId, photobookEmail.trim());
            locationService.updateLocationSmtpSslCheck(locationId, sslcheck);
        }
        if (ok) {
            modelMap = createModelMap(true);
            modelMap.put(msgProperty, "Settings were successfully updated");
            logger.info("User [ " + currentUser + " ]" + " updated email settings for location " + locId != null ? locationService.getLocation(Integer.valueOf(locId)) : "NULL");
        } else {
            modelMap = createModelMap(false);
            modelMap.put(msgProperty, msg);
            logger.warn("User [ " + currentUser + " ]" + " FAILED to update email settings for location " + locId != null ? locationService.getLocation(Integer.valueOf(locId)) : "NULL");
        }
        return modelMap;
    }

    @RequestMapping("selectSettingsLocation.json")
    public
    @ResponseBody
    Map<String, Object> selectSettings(@RequestParam("locationId") Integer locationId) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        Map<String, Object> modelMap = createModelMap(true);
        if (locationId != null) {
            Location location = locationService.getLocation(locationId);
            modelMap.put("toFlickrEmail", location.getLocationToFlickrEmail());
            modelMap.put("toFacebookEmail", location.getLocationToFacebookEmail());
            modelMap.put("smtpServer", location.getLocationSmtpServer());
            modelMap.put("fromEmail", location.getLocationFromEmail());
            modelMap.put("smtpServerFromPassword", location.getLocationSmtpServerPassword());
            modelMap.put("smtpServerPort", location.getLocationSmtpServerPort());
            modelMap.put("toEmail", location.getLocationToEmail() != null ? location.getLocationToEmail() : "");
            modelMap.put("sslcheck", location.getLocationSslCheck() != null ? location.getLocationSslCheck() : "off");
            modelMap.put("photobook_email", location.getLocationToPhotobookEmail());
            modelMap.put("isHiddenTo", (location.getBcconly() != null) ? !location.getBcconly() : true);
        }
        modelMap.put(msgProperty, "Settings were successfully updated");
        return modelMap;
    }

    private Map<String, Object> createModelMap(Boolean success) {
        Map<String, Object> modelMap = new HashMap<String, Object>();
        modelMap.put(successProperty, success);
        return modelMap;
    }

    @Autowired
    public void setPictureService(LocationService locationService) {
        this.locationService = locationService;
    }
}
