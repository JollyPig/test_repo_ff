package com.scnsoft.fotaflo.webapp.job;

import com.scnsoft.fotaflo.webapp.reportmodule.service.ReportUploadService;
import com.scnsoft.fotaflo.webapp.service.ReportService;
import com.scnsoft.fotaflo.webapp.util.ApplicationContextProvider;
import com.scnsoft.fotaflo.webapp.util.DateConvertationUtils;
import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 20.08.2012
 * Time: 8:49:14
 * To change this template use File | Settings | File Templates.
 */
public class DailyUploadReportJob extends QuartzJobBean {
    protected static Logger logger = Logger.getLogger(DailyUploadReportJob.class);

    private ReportService reportService;

    protected void executeInternal(JobExecutionContext ctx) throws JobExecutionException {
        logger.info("Start generating Daily upload report");
        Date fireTime = ctx.getFireTime();
        Date date = DateConvertationUtils.getStartDate(1, fireTime);
        String newDate = DateConvertationUtils.getDateString(date);

        //ReportUploadService reportUploadService = new ReportUploadService();
        ApplicationContext context = ApplicationContextProvider.getApplicationContext();
        ReportUploadService reportUploadService = (ReportUploadService) context.getBean("reportUploadService");
        Map<String, Object> reportMap = reportUploadService.fileReportCreate(date);

        if (reportMap != null) {
            reportService.sendDailyUploadReport((List) reportMap.get("emails"), (String) reportMap.get("report"), newDate);
        } else {
            logger.error("Report was not generated because of some error");
        }
    }

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }

}
