package com.scnsoft.fotaflo.webapp.dao.impl;

import com.scnsoft.fotaflo.webapp.common.ResultList;
import com.scnsoft.fotaflo.webapp.dao.IPurchaseSelectedDao;
import com.scnsoft.fotaflo.webapp.model.*;
import org.hibernate.Query;

import java.util.List;

/**
 * Created by Nadezda Drozdova
 * Date Apr 20, 2011
 */
public class PurchaseSelectedDao extends BaseDao implements IPurchaseSelectedDao {
    public PurchaseSelected getPurchsePictureByUser(Picture picture, SystemUser user) {
        StringBuffer query = new StringBuffer();
        query.append("select purchseSelected from PurchaseSelected as purchseSelected where ");
        query.append("purchseSelected.picture = :picture ");
        query.append("and purchseSelected.systemUser = :user ");
        List list1 = getSession().createQuery(query.toString())
                .setParameter("picture", picture)
                .setParameter("user", user)
                .list();
        if (!list1.isEmpty()) {
            return (PurchaseSelected) list1.get(0);
        }
        return null;
    }

    public List<PurchaseSelected> getPurchsePictureByUser(SystemUser user) {
        StringBuffer query = new StringBuffer();
        query.append("select purchseSelected from PurchaseSelected as purchseSelected where ");
        query.append("purchseSelected.systemUser = :user ");
        List list1 = getSession().createQuery(query.toString())
                .setParameter("user", user)
                .list();
        if (!list1.isEmpty()) {
            return list1;
        }
        return null;
    }

    public ResultList<PurchaseSelected> getPicturesSelectedByUser(int start, int limit, SystemUser user) {
        StringBuilder builder = new StringBuilder();
        builder.append(" from PurchaseSelected as purchseSelected left join purchseSelected.picture as picture where ")
                .append("purchseSelected.systemUser = :user ");
        builder.append(" order by picture.creationDate desc");
        return getPictures(builder.toString(), start, limit, user);
    }

    @SuppressWarnings("unchecked")
    public ResultList<PurchaseSelected> getPictures(String queryString, int start, int limit, SystemUser user) {
        ResultList<PurchaseSelected> resultList = new ResultList<PurchaseSelected>();
        String queryTotal = "select count(*) " + queryString;
        String queryPictures = "select purchseSelected " + queryString;

        Query query = getSession().createQuery(queryPictures);
        query.setParameter("user", user)
                .setFirstResult(start)
                .setMaxResults(limit);
        resultList.setItems((List<PurchaseSelected>) query.list());

        query = getSession().createQuery(queryTotal);
        query.setParameter("user", user);
        Long total = (Long) query.uniqueResult();
        resultList.setTotal(total);
        return resultList;
    }

    @SuppressWarnings("unchecked")
    public ResultList<RequestsSelected> getPictures(String queryString, int start, int limit, Requests requests) {
        ResultList<RequestsSelected> resultList = new ResultList<RequestsSelected>();
        String queryTotal = "select count(*) " + queryString;
        String queryPictures = "select requestsSelected " + queryString;

        Query query = getSession().createQuery(queryPictures);
        query.setParameter("requests", requests)
                .setFirstResult(start)
                .setMaxResults(limit);
        resultList.setItems((List<RequestsSelected>) query.list());

        query = getSession().createQuery(queryTotal);
        query.setParameter("requests", requests);
        Long total = (Long) query.uniqueResult();
        resultList.setTotal(total);
        return resultList;
    }

    public ResultList<RequestsSelected> getPicturesSelectedByRequest(int start, int limit, Requests requests) {
        StringBuilder builder = new StringBuilder();
        builder.append(" from RequestsSelected as requestsSelected left join requestsSelected.picture as picture where ")
                .append("requestsSelected.request = :requests ");
        builder.append(" order by picture.creationDate desc");
        return getPictures(builder.toString(), start, limit, requests);
    }


}
