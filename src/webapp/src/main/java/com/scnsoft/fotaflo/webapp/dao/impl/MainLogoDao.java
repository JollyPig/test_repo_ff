package com.scnsoft.fotaflo.webapp.dao.impl;

import com.scnsoft.fotaflo.webapp.dao.IMainLogoDao;
import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.model.MainLogo;

import java.util.List;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */
public class MainLogoDao extends BaseDao implements IMainLogoDao {

    public MainLogo getMainLogoByLocation(Location location) {
        List result = getHibernateTemplate().find(
                "from MainLogo mainLogo where mainLogo.location = ?", location);
        if (!result.isEmpty()) {
            return (MainLogo) result.get(0);
        }

        return null;
    }
}
