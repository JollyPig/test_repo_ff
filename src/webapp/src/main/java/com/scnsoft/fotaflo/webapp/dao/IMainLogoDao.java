package com.scnsoft.fotaflo.webapp.dao;

import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.model.MainLogo;

/**
 * Created by Paradinets.
 * Date: 31.03.2012
 * Time: 6:01:26
 */
public interface IMainLogoDao {

    public MainLogo getMainLogoByLocation(Location location);

}
