package com.scnsoft.fotaflo.webapp.dao;

import com.scnsoft.fotaflo.webapp.model.ContactInfo;
import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.model.SystemUser;

import java.util.Date;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 30.03.2012
 * Time: 12:21:37
 * To change this template use File | Settings | File Templates.
 */
public interface ISystemUserDao extends com.scnsoft.fotaflo.common.dao.IDao<SystemUser, Integer> {

    public SystemUser getUserByLogin(String login);

    public SystemUser getUsersByLoginAndLocation(Location location, String login);

    public List<SystemUser> getPublicUsersByDateAndLocation(Location location, Date startDate, Date endDate, String access);

    public List<SystemUser> getPublicUsersByDate(Date startDate, Date endDate, String access);

    public List<ContactInfo> getContcatInfosForUser(SystemUser user);

    List<SystemUser> findByName(String name);

    List<SystemUser> findByName(String name, int start, int limit);

    long countByName(String name);
}
