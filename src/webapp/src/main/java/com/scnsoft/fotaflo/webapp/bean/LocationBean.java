package com.scnsoft.fotaflo.webapp.bean;

/**
 * User: MilkevichP
 * Date: 22.04.11
 * Time: 16:44
 */
public class LocationBean {

    protected int id;
    private String locationName;

    public LocationBean(int id, String locationName) {
        this.id = id;
        this.locationName = locationName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }
}
