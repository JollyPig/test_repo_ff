package com.scnsoft.fotaflo.webapp.dao;

import com.scnsoft.fotaflo.webapp.model.Payment;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 31.03.2012
 * Time: 10:49:04
 * To change this template use File | Settings | File Templates.
 */
public interface IPayPalDao {

    public List<Payment> getPaymentByTransaction(String token);
}
