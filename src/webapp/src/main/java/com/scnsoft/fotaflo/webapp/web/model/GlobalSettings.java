package com.scnsoft.fotaflo.webapp.web.model;

import java.io.Serializable;

/**
 * @author Anatoly Selitsky
 *
 */
public class GlobalSettings implements Serializable {

    private String pageSize;
    private Long startDate;
    private Long endDate;
    private int userId;
    private String current;

    public GlobalSettings(String pageSize, Long startDate, Long endDate, int userId, String current) {
        this.pageSize = pageSize;
        this.startDate = startDate;
        this.endDate = endDate;
        this.userId = userId;
        this.current = current;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getCurrent() {
        return current;
    }

    public void setCurrent(String current) {
        this.current = current;
    }

}
