package com.scnsoft.fotaflo.webapp.bean;

import java.util.List;

/**
 * Created by Nadezda Drozdova
 * Date: May 23, 2011
 * Time: 4:43:30 PM
 */
public class NotificationMessageBean {
    String filename;
    int numberOfCopiesToPrint;
    List<String> to;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public int getNumberOfCopiesToPrint() {
        return numberOfCopiesToPrint;
    }

    public void setNumberOfCopiesToPrint(int numberOfCopiesToPrint) {
        this.numberOfCopiesToPrint = numberOfCopiesToPrint;
    }

    public List<String> getTo() {
        return to;
    }

    public void setTo(List<String> to) {
        this.to = to;
    }

    public NotificationMessageBean(String filename, int numberOfCopiesToPrint, List<String> to) {
        this.filename = filename;
        this.numberOfCopiesToPrint = numberOfCopiesToPrint;
        this.to = to;
    }
}
