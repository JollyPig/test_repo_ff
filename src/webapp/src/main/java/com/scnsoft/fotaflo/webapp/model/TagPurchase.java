package com.scnsoft.fotaflo.webapp.model;

import com.scnsoft.fotaflo.common.model.AbstractEntity;

import javax.persistence.*;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: paradinets
 * Date: 23.05.14
 * Time: 17:54
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "tagpurchase")
public class TagPurchase extends AbstractEntity {

    private SystemUser systemUser;
    private String generatedCode;
    private Date creationDate;
    private Date refundedDate;
    private Date startDate;
    private Date endDate;
    private String startDay;
    private String endDay;
    private String startTime;
    private String endTime;
    private Package purchasePackage;
    private Location userLocation;
    private Location location;
    private Location locationPurchase;
    private String subject;
    private String stringFileName;
    private String emailBody;
    private String firstPictureTime;
    private String cameras;
    private int statusCode;
    private Date purchaseDate;
    private Set<Tag> tags = new HashSet<Tag>();

    private String staffs;        // todo remove and use staffList
    private Integer participants;
    private String participantsOther;
    private String participantsEmails;
    private String participantsOtherEmails;

    private List<Staff> staffList = new ArrayList<Staff>();

    public enum StatusCode{
        ACTUAL(1),
        REFUNDED(2),
        REMOVED(0);

        private int value;

        private StatusCode(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    @ManyToOne
    /*(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH} )*/
    @JoinColumn(name = "system_user_id")
    public SystemUser getSystemUser() {
        return systemUser;
    }

    public void setSystemUser(SystemUser systemUser) {
        this.systemUser = systemUser;
    }

    @Column(name = "purchase_code", length = 255)
    public String getGeneratedCode() {
        return generatedCode;
    }

    public void setGeneratedCode(String generatedCode) {
        this.generatedCode = generatedCode;
    }

    @Column(name = "start_date")
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Column(name = "end_date")
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Column(name = "creation_date")
    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Column(name = "refunded_date")
    public Date getRefundedDate() {
        return refundedDate;
    }

    public void setRefundedDate(Date refundedDate) {
        this.refundedDate = refundedDate;
    }

    @ManyToOne
    /*(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH} )*/
    @JoinColumn(name = "packege_id")
    public Package getPurchasePackage() {
        return purchasePackage;
    }

    public void setPurchasePackage(Package purchasePackage) {
        this.purchasePackage = purchasePackage;
    }

    @ManyToOne
    @JoinColumn(name = "user_location_id")
    public Location getUserLocation() {
        return userLocation;
    }

    public void setUserLocation(Location userLocation) {
        this.userLocation = userLocation;
    }

    @ManyToOne
    /*(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH} )*/
    @JoinColumn(name = "location_id")
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }


    @ManyToOne
    /*(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH} )*/
    @JoinColumn(name = "location_purchase_id")
    public Location getLocationPurchase() {
        return locationPurchase;
    }

    public void setLocationPurchase(Location locationPurchase) {
        this.locationPurchase = locationPurchase;
    }

    @Column(name = "subject", length = 2048)
    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Column(name = "stringfilename", length = 2048)
    public String getStringFileName() {
        return stringFileName;
    }

    public void setStringFileName(String stringFileName) {
        this.stringFileName = stringFileName;
    }

    @Lob
    @Column(name = "emailbody")
    public String getEmailBody() {
        return emailBody;
    }

    public void setEmailBody(String emailBody) {
        this.emailBody = emailBody;
    }

    @Column(name = "first_picture_time", length = 2048)
    public String getFirstPictureTime() {
        return firstPictureTime;
    }

    public void setFirstPictureTime(String firstPictureTime) {
        this.firstPictureTime = firstPictureTime;
    }

    @Column(name = "cameras", length = 2048)
    public String getCameras() {
        return cameras;
    }

    public void setCameras(String cameras) {
        this.cameras = cameras;
    }

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinTable(name = "purchase_tags", joinColumns = {@JoinColumn(name = "purchase_tag_id")}, inverseJoinColumns = {@JoinColumn(name = "tag_id")})
    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinTable(name = "tagpurchase_staff", joinColumns = {@JoinColumn(name = "tagpurchase_id")}, inverseJoinColumns = {@JoinColumn(name = "staff_id")})
    public List<Staff> getStaffList() {
        return staffList;
    }

    public void setStaffList(List<Staff> staffList) {
        this.staffList = staffList;
    }

    @Column(name = "participants")
    public Integer getParticipants() {
        return participants;
    }

    public void setParticipants(Integer participants) {
        this.participants = participants;
    }

    @Column(name = "participantsOther", length = 2048)
    public String getParticipantsOther() {
        return participantsOther;
    }

    public void setParticipantsOther(String participantsOther) {
        this.participantsOther = participantsOther;
    }

    @Column(name = "participantsEmails", length = 2048)
    public String getParticipantsEmails() {
        return participantsEmails;
    }

    public void setParticipantsEmails(String participantsEmails) {
        this.participantsEmails = participantsEmails;
    }

    @Column(name = "participantsOtherEmails", length = 2048)
    public String getParticipantsOtherEmails() {
        return participantsOtherEmails;
    }

    public void setParticipantsOtherEmails(String participantsOtherEmails) {
        this.participantsOtherEmails = participantsOtherEmails;
    }

    @Column(name = "staff", length = 2048)
    public String getStaffs() {
        return staffs;
    }

    public void setStaffs(String staffs) {
        this.staffs = staffs;
    }

    @Column(name = "startDay", length = 2048)
    public String getStartDay() {
        return startDay;
    }

    public void setStartDay(String startDay) {
        this.startDay = startDay;
    }

    @Column(name = "endDay", length = 2048)
    public String getEndDay() {
        return endDay;
    }

    public void setEndDay(String endDay) {
        this.endDay = endDay;
    }

    @Column(name = "startTime", length = 2048)
    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    @Column(name = "endTime", length = 2048)
    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @Column(name = "statusCode")
    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    @Column(name = "purchaseDate")
    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }
}