package com.scnsoft.fotaflo.webapp.dao.impl;

import com.scnsoft.fotaflo.webapp.common.ResultList;
import com.scnsoft.fotaflo.webapp.dao.IPictureSlideShowDao;
import com.scnsoft.fotaflo.webapp.model.Camera;
import com.scnsoft.fotaflo.webapp.model.Picture;
import com.scnsoft.fotaflo.webapp.model.Tag;
import org.hibernate.Query;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Anatoly Selitsky
 */
public class PictureSlideShowDao extends BaseDao implements IPictureSlideShowDao {

    @Override
    @SuppressWarnings("unchecked")
    public ResultList<Picture> findExtended(int start, int limit, Date startDate, Date endDate, List<Camera> cameras) {
        StringBuilder builder = new StringBuilder();
        builder.append(" from Picture as picture where ")
                .append("picture.creationDate >= :start ")
                .append("and picture.creationDate <= :end ");
        if (cameras.size() > 0) {
            builder.append("and picture.camera.id in (")
                    .append(parseList(cameras))
                    .append(")");
        } else {
            return new ResultList<Picture>(0L, new ArrayList<Picture>());
        }
        builder.append(" order by picture.creationDate desc");
        return getPictures(builder.toString(), start, limit, startDate, endDate);
    }


    @Override
    @SuppressWarnings("unchecked")
    public ResultList<Picture> findExtendedByTag(int start, int limit, Date startDate, Date endDate, List<Camera> cameras, List<Tag> tags) {
        StringBuilder builder = new StringBuilder();
        builder.append(" from Picture as picture join picture.tags as tags where ")
                .append("picture.creationDate >= :start ")
                .append("and picture.creationDate <= :end ");
        if (cameras.size() > 0) {
            builder.append("and picture.camera.id in (")
                    .append(parseList(cameras))
                    .append(")");
        } else {
            return new ResultList<Picture>(0L, new ArrayList<Picture>());
        }
        if (tags.size() > 0) {
            builder.append("and tags.id in (")
                    .append(parseTagList(tags))
                    .append(")");
        }
        builder.append(" order by picture.creationDate desc");
        return getPictures(builder.toString(), start, limit, startDate, endDate);
    }

    @Override
    @SuppressWarnings("unchecked")
    public ResultList<Picture> findExtendedWithoutTag(int start, int limit, Date startDate, Date endDate, List<Camera> cameras) {
        StringBuilder builder = new StringBuilder();
        builder.append(" from Picture as picture left join picture.tags as tags where ")
                .append("picture.creationDate >= :start ")
                .append("and picture.creationDate <= :end ");
        if (cameras.size() > 0) {
            builder.append("and picture.camera.id in (")
                    .append(parseList(cameras))
                    .append(") ");
        } else {
            return new ResultList<Picture>(0L, new ArrayList<Picture>());
        }
        builder.append("and tags.id = null")
                .append(" order by picture.creationDate desc");
        return getPictures(builder.toString(), start, limit, startDate, endDate);
    }

    @Override
    @SuppressWarnings("unchecked")
    public ResultList<Picture> findExtendedTagOnly(int start, int limit, List<Tag> tags) {
        StringBuilder builder = new StringBuilder();
        builder.append(" from Picture as picture join picture.tags as tags where ");
        if (tags.size() > 0) {
            builder.append("tags.id in (")
                    .append(parseTagList(tags))
                    .append(") ");
        } else {
            return new ResultList<Picture>(0L, new ArrayList<Picture>());
        }
        builder.append(" order by picture.creationDate desc");
        return getPictures(builder.toString(), start, limit);
    }

    private String parseList(List list) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            if (i != 0) {
                result.append(", ");
            }
            result.append(((Camera) list.get(i)).getId());
        }
        return result.toString();
    }

    private String parseTagList(List list) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            if (i != 0) {
                result.append(", ");
            }
            result.append(((Tag) list.get(i)).getId());
        }
        return result.toString();
    }

    @SuppressWarnings("unchecked")
    public ResultList<Picture> getPictures(String queryString, int start, int limit, Date startDate, Date endDate) {
        ResultList<Picture> resultList = new ResultList<Picture>();
        String queryTotal = "select count(*) " + queryString;
        String queryPictures = "select picture " + queryString;

        Query query = getSession().createQuery(queryPictures);
        query.setParameter("start", startDate)
                .setParameter("end", endDate)
                .setFirstResult(start)
                .setMaxResults(limit);
        resultList.setItems((List<Picture>) query.list());

        query = getSession().createQuery(queryTotal);
        query.setParameter("start", startDate)
                .setParameter("end", endDate);
        Long total = (Long) query.uniqueResult();
        resultList.setTotal(total);
        return resultList;
    }

    @SuppressWarnings("unchecked")
    public ResultList<Picture> getPictures(String queryString, int start, int limit) {
        ResultList<Picture> resultList = new ResultList<Picture>();
        String queryTotal = "select count(*) " + queryString;
        String queryPictures = "select picture " + queryString;

        Query query = getSession().createQuery(queryPictures);
        query.setFirstResult(start)
                .setMaxResults(limit);
        resultList.setItems((List<Picture>) query.list());

        query = getSession().createQuery(queryTotal);
        resultList.setTotal((Long) query.uniqueResult());
        return resultList;
    }

}
