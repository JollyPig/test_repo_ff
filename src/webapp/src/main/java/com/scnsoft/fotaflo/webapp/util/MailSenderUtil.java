package com.scnsoft.fotaflo.webapp.util;

import com.scnsoft.fotaflo.webapp.bean.PictureToFTPBean;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Security;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Created by Nadezda Drozdova
 * Date Apr 21, 2011
 */
public class MailSenderUtil {
    private static final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
    protected static Logger logger = Logger.getLogger(MailSenderUtil.class);

    public boolean sendContactQuestions(List<String> to, String msgText1, String from, String subject,
                                        String mailHostServer, String port, String password, boolean isSsl) {
        if (from != null && mailHostServer != null && port != null) {
            if (password == null || password.isEmpty()) {
                password = null;
            }
            boolean result = sendMailMessage(to, from, subject, msgText1, null,
                    mailHostServer, port, password, isSsl, null);
            return result;
        } else {
            logger.error("Please set contact email settings correctly");
            return false;
        }
    }

    public void sendMonthlyReports(List<String> to, String msgText1, Map<String, File> attachment, String from,
                                   String subject, String mailHostServer, String port, String password, boolean isSsl) {
        if (from != null && mailHostServer != null && port != null) {
            if (password == null || password.isEmpty()) {
                password = null;
            }
            sendMailMessage(to, from, subject, msgText1, attachment,
                    mailHostServer, port, password, isSsl, null);
        } else {
            logger.error("Please set report settings correctly");
        }
    }

    public boolean sendMailMessage(List<String> to, String from, String subject, String msgText1, Map<String, File> attachment,
                                   String mailHostServer, String port, String password, boolean isSsl, String bccEmail) {
        if (isSsl) {
            Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
            logger.debug("Add SSL Provider");
        }

        Properties props = new Properties();
        props = configureSMTPServer(props, mailHostServer, port, password, isSsl);

        Session session = configureSession(props, from, password, port, isSsl);
        try {
            // create a message
            MimeMessage msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(from));

            InternetAddress[] address = new InternetAddress[to.size()];
            for (int i = 0; i < to.size(); i++) {
                address[i] = new InternetAddress(to.get(i));
            }

            if (bccEmail == null || address.length == 1) {
                msg.setRecipients(Message.RecipientType.TO, address);
            } else {
                if (bccEmail.equals("")) {
                    if (address.length > 0) {
                        msg.setRecipient(Message.RecipientType.TO, address[0]);
                        msg.setRecipients(Message.RecipientType.BCC, address);
                    }
                } else {
                    msg.setRecipients(Message.RecipientType.TO, bccEmail);
                    msg.setRecipients(Message.RecipientType.BCC, address);
                }
            }

            msg.setSubject(subject);

            // create the Multipart and add its parts to it
            Multipart mp = new MimeMultipart();
            // create and fill the first message part
            MimeBodyPart mbp1 = new MimeBodyPart();
            if (msgText1.contains("<html>") && msgText1.contains("</html>")) {
                mbp1.setContent(msgText1, "text/html");
            } else {
                mbp1.setText(msgText1);
            }
            mp.addBodyPart(mbp1);
            // create the second message part

            // attach the file to the message
            if (attachment != null && attachment != null && !attachment.isEmpty()) {
                for (String key : attachment.keySet()) {
                    MimeBodyPart mbp2 = new MimeBodyPart();
                    File attach = attachment.get(key);
                    FileDataSource fds = new FileDataSource(attach);
                    if (!fds.getFile().exists()) {
                        logger.warn("File does not exit.. skipped " + fds.getFile().getName());
                        continue;
                    }
                    mbp2.setDataHandler(new DataHandler(fds));
                    mbp2.setFileName(key + getFileExtension(fds.getName()));
                    mbp2.setHeader("Content-ID", "<" + String.valueOf(key.hashCode()) + ">");
                    mp.addBodyPart(mbp2);
                }
            }
            // add the Multipart to the message
            msg.setContent(mp);

            // set the Date: header
            msg.setSentDate(new Date());

            // send the message
            Transport.send(msg);

        } catch (MessagingException mex) {
            logger.error("Error of sending message. ", mex);
            return false;
        } catch (Throwable mex) {
            logger.error("Error of sending message. ", mex);
            return false;
        }
        return true;
    }

    public String getFileExtension(String filename) {
        String ext = "";
        int mid = filename.lastIndexOf(".");
        ext = filename.substring(mid, filename.length());
        return ext;
    }

    public Properties configureSMTPServer(Properties props, String mailHostServer, String port, String password, boolean isSsl) {
        String message = "mail host - " + mailHostServer;
        logger.debug("Start SMTP server configuration");
        props.put("mail.smtp.host", mailHostServer);
        props.put("mail.smtp.port", port);
        //props.put("mail.debug", "true");
        if (isSsl) {
            props.put("mail.smtp.socketFactory.port", port);
            props.put("mail.smtp.socketFactory.class", SSL_FACTORY);
            props.put("mail.smtp.socketFactory.fallback", "false");
            message += ", port - " + port;
            props.put("mail.smtp.auth", "true");
        } else {
            props.remove("mail.smtp.socketFactory.port");
            props.remove("mail.smtp.socketFactory.class");
            props.remove("mail.smtp.socketFactory.fallback");
            if (port == null || password == null) {
                props.put("mail.smtp.auth", "false");
            } else {
                props.put("mail.smtp.auth", "true");
            }
        }
        logger.debug("End SMTP server configuration: " + message);
        return props;
    }

    public Session configureSession(Properties props, final String from, final String password, String port, boolean isSsl) {
        logger.debug("Configure SMTP server");
        Session session = null;
        if (isSsl || (port != null && password != null)) {
            session = Session.getDefaultInstance(props,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(from, password);
                        }
                    });
        } else {
            session = Session.getInstance(props, null);
        }
        return session;
    }

    public void uploadPicturesToFTP(PictureToFTPBean ftpBean) {
        if (ftpBean == null) return;

        String server = ftpBean.getServer();
        String user = ftpBean.getLogin();
        String pass = ftpBean.getPassword();
        List<String> picturesToUpload = ftpBean.getPicturePathes();
        List<String> pictureNames = ftpBean.getPictureNames();

        FTPClient ftpClient = new FTPClient();
        try {

            ftpClient.connect(server);
            ftpClient.login(user, pass);
            ftpClient.enterLocalPassiveMode();

            String dirToCreate = ftpBean.getLocationName();
            boolean success = ftpClient.makeDirectory(dirToCreate);
            logger.debug("Directory " + dirToCreate + "was created " + success);
            ftpClient.changeWorkingDirectory(dirToCreate);

            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            int i = 0;
            if (picturesToUpload != null && picturesToUpload.size() != 0) {
                for (String picturePath : picturesToUpload) {
                    File firstLocalFile = new File(picturePath);

                    String firstRemoteFile = pictureNames.get(i);
                    InputStream inputStream = new FileInputStream(firstLocalFile);

                    logger.debug("Start uploading first file");
                    boolean done = ftpClient.storeFile(firstRemoteFile, inputStream);
                    inputStream.close();
                    i++;
                    if (done) {
                        logger.info("The file " + picturePath + "is uploaded successfully.");
                    }
                }
            }
        } catch (IOException ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                logger.error(ex.getMessage(), ex);
            }
        }
    }

}
