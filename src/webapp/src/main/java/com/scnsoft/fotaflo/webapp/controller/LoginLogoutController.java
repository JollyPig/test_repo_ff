package com.scnsoft.fotaflo.webapp.controller;


import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.service.PictureService;
import com.scnsoft.fotaflo.webapp.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */

/**
 * Handles and retrieves the login or denied page depending on the URI template
 */
@Controller
@RequestMapping("/auth")
public class LoginLogoutController {
    private PictureService pictureService;

    private UserService userService;

    protected static Logger logger = Logger.getLogger(LoginLogoutController.class);

    private static AuthenticationManager authenticationManager = new AuthenticationManager() {

        private List<GrantedAuthority> AUTHORITIES = new ArrayList<GrantedAuthority>() {
            {
                add(new GrantedAuthorityImpl("ROLE_ADMIN"));
                add(new GrantedAuthorityImpl("ROLE_PUBLIC"));
            }
        };

        public Authentication authenticate(Authentication auth) throws AuthenticationException {
            if (auth.getName().equals(auth.getCredentials())) {
                return new UsernamePasswordAuthenticationToken(auth.getName(),
                        auth.getCredentials(), AUTHORITIES);
            }
            throw new BadCredentialsException("Bad Credentials");
        }

    };

    /**
     * Handles and retrieves the login JSP page
     *
     * @return the name of the JSP page
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String getLoginPage(@RequestParam(value = "error", required = false) boolean error,
                               ModelMap model, HttpServletRequest httpServletRequest) {
        logger.debug("Received request to show login page");

        // Add an error message to the model if login is unsuccessful
        // The 'error' parameter is set to true based on the when the authentication has failed.
        // We declared this under the authentication-failure-url attribute inside the spring-security.xml


        if (error) {
            // Assign an error message
            logger.warn("UNSUCCESSFUL LOGIN TRIAL");
            model.put("error", "You have entered an invalid username or password!");

        } else {
            model.put("error", "");
            HttpSession session = httpServletRequest.getSession();
            final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
           /* Location location = userService.getUserLocation( currentUser );
            if(location!=null && location.getTagged()!=null && location.getTagged()==true ){
                session.setAttribute("tagEnabled",true);
                System.out.println("I am setting to the session "+true );
            }else{
                session.setAttribute("tagEnabled",false);
                System.out.println("I am setting to the session "+false );
            }
            logger.info("tag enabled: " + session.getAttribute("tagEnabled"));*/
        }
        // This will resolve to /WEB-INF/jsp/loginpage.jsp
        return "loginpage";
    }


    @RequestMapping(value = "/autoLogin", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView login(@RequestParam(value = "j_username", required = false) String username,
                              @RequestParam(value = "j_password", required = false) String password,
                              @RequestParam(value = "tag", required = false) String tag) {
        try {
            Authentication request = new UsernamePasswordAuthenticationToken(username, password);
            Authentication result = authenticationManager.authenticate(request);
            SecurityContextHolder.getContext().setAuthentication(result);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        ModelAndView modelAndView = new ModelAndView("redirect:/pictures/main/common");
        if (tag != null) {
            modelAndView.addObject("myFilter", tag);
        }
        return modelAndView;
    }

    /**
     * Handles and retrieves the denied JSP page. This is shown whenever a regular user
     * tries to access an admin only page.
     *
     * @return the name of the JSP page
     */
    @RequestMapping(value = "/denied", method = RequestMethod.GET)
    public String getDeniedPage(ModelMap model) {
        logger.debug("Received request to show denied page");
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        model.put("userId", pictureService.getUserAccess(currentUser));
        model.put("current", currentUser);
        logger.warn("User [ " + currentUser + " ]" + " got a denied page");
        Location location = userService.getUserLocation(currentUser);
        boolean tagged = (location != null && location.getTagged() != null && location.getTagged() == true) ? true : false;
        model.put("tagApp", tagged);
        // This will resolve to /WEB-INF/jsp/deniedpage.jsp
        return "deniedpage";
    }

    @Autowired
    public void setPictureService(PictureService pictureService) {
        this.pictureService = pictureService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}