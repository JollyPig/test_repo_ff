package com.scnsoft.fotaflo.webapp.util;

import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 11.04.13
 * Time: 17:44
 * To change this template use File | Settings | File Templates.
 */
public class ReportStylesUtil {

    WritableFont.FontName DEFAUT_FONT = WritableFont.createFont("Calibri");
    WritableFont.FontName SPECIAL_FONT = WritableFont.ARIAL;
    private static final short DEFAULT_FONT_HEIGHT = (short) 11;
    private static final short SPECIAL_FONT_HEIGHT = (short) 10;

    // private static final String HEADER_STYLE = "HeaderStyle";
    private static final String REGULAR_STYLE = "RegularStyle";
    private static final String REGULAR_SPECIAL_STYLE = "RegularSpecialStyle";
    private static final String HEADER_BORDER_STYLE_LEFT = "HeaderBorderStyleLeft";
    private static final String REGULAR_BORDER_STYLE_LEFT = "RegularBorderStyleLeft";
    private static final String HEADER_BORDER_STYLE_RIGHT = "HeaderBorderStyleRight";
    private static final String REGULAR_BORDER_STYLE_RIGHT = "RegularBorderStyleRight";
    private static final String HEADER_BORDER_DOLLAR_STYLE = "HeaderDollarBorderStyle";
    private static final String REGULAR_BORDER_DOLLAR_STYLE = "RegularBorderDollarStyle";
    private static final String BOLD_STYLE = "BoldStyle";
    private static final String BOLD_BORDER_STYLE = "BoldBorderStyle";
    private static final String BOLD_CENTRE_BORDER_STYLE = "BoldBorderCenterStyle";

    private static final String BOLD_BORDER_DOLLAR_STYLE = "BoldBorderDollarStyle";
    private static final String PLAIN_BORDER_BOTTOM_STYLE = "PlainBorderBottomStyle";
    private static final String PLAIN_BORDER_TOP_STYLE = "PlainBorderTopStyle";
    private static final String PLAIN_BORDER_RIGHT_STYLE = "PlainBorderRightStyle";

    private static Map<String, WritableCellFormat> existingStyles = new HashMap<String, WritableCellFormat>();

    public WritableCellFormat getRegularStyle() {
        /* find in existing */
        return buildStyle(REGULAR_STYLE, DEFAUT_FONT, false, DEFAULT_FONT_HEIGHT, false, false, false, false,
                false, Alignment.LEFT, VerticalAlignment.CENTRE, false);
    }

    public WritableCellFormat getRegularSpecialStyle() {
		/* find in existing */
        return buildStyle(REGULAR_SPECIAL_STYLE, SPECIAL_FONT, false, SPECIAL_FONT_HEIGHT, false, false, false, false,
                false, Alignment.LEFT, VerticalAlignment.CENTRE, false);
    }

    public WritableCellFormat getHeaderBorderStyleLeft() {
		/* find in existing */
        return buildStyle(HEADER_BORDER_STYLE_LEFT, DEFAUT_FONT, true, DEFAULT_FONT_HEIGHT, false, true, true, true, true,
                Alignment.LEFT, VerticalAlignment.BOTTOM, true);
    }

    public WritableCellFormat getHeaderBorderStyleRight() {
		/* find in existing */
        return buildStyle(HEADER_BORDER_STYLE_RIGHT, DEFAUT_FONT, true, DEFAULT_FONT_HEIGHT, false, true, true, true, true,
                Alignment.RIGHT, VerticalAlignment.BOTTOM, true);
    }

    public WritableCellFormat getRegularBorderStyleLeft() {
		/* find in existing */
        return buildStyle(REGULAR_BORDER_STYLE_LEFT, DEFAUT_FONT, false, DEFAULT_FONT_HEIGHT, false, true, true, true, true,
                Alignment.LEFT, VerticalAlignment.CENTRE, false);
    }

    public WritableCellFormat getRegularBorderStyleRight() {
		/* find in existing */
        return buildStyle(REGULAR_BORDER_STYLE_RIGHT, DEFAUT_FONT, false, DEFAULT_FONT_HEIGHT, false, true, true, true, true,
                Alignment.RIGHT, VerticalAlignment.CENTRE, false);
    }

    public WritableCellFormat getHeaderBorderDollarStyle() {
		/* find in existing */
        return buildStyle(HEADER_BORDER_DOLLAR_STYLE, DEFAUT_FONT, true, DEFAULT_FONT_HEIGHT, true, true, true, true,
                true, Alignment.LEFT, VerticalAlignment.CENTRE, true);
    }

    public WritableCellFormat getRegualarBorderDollarStyle() {
		/* find in existing */
        return buildStyle(REGULAR_BORDER_DOLLAR_STYLE, DEFAUT_FONT, false, DEFAULT_FONT_HEIGHT, true, true, true, true,
                true, Alignment.LEFT, VerticalAlignment.CENTRE, false);
    }

    public WritableCellFormat getBoldBorderDollarStyle() {
		/* find in existing */
        return buildStyle(BOLD_BORDER_DOLLAR_STYLE, SPECIAL_FONT, true, SPECIAL_FONT_HEIGHT, true, true, true, true,
                true, Alignment.LEFT, VerticalAlignment.CENTRE, false);
    }

    public WritableCellFormat getBoldStyle() {
		/* find in existing */
        return buildStyle(BOLD_STYLE, DEFAUT_FONT, true, DEFAULT_FONT_HEIGHT, false, false, false, false,
                false, Alignment.LEFT, VerticalAlignment.CENTRE, false);
    }

    public WritableCellFormat getBoldBorderStyle() {
		/* find in existing */
        return buildStyle(BOLD_BORDER_STYLE, DEFAUT_FONT, true, DEFAULT_FONT_HEIGHT, false, true, true, true, true,
                Alignment.LEFT, VerticalAlignment.CENTRE, false);
    }

    public WritableCellFormat getBoldBorderCenterStyle() {
		/* find in existing */
        return buildStyle(BOLD_CENTRE_BORDER_STYLE, DEFAUT_FONT, true, DEFAULT_FONT_HEIGHT, false, true, true, true, true,
                Alignment.CENTRE, VerticalAlignment.CENTRE, false);
    }

    public WritableCellFormat getPlainBorderBottomStyle() {
		/* find in existing */
        return buildStyle(PLAIN_BORDER_BOTTOM_STYLE, DEFAUT_FONT, false, DEFAULT_FONT_HEIGHT, false, false, true, false, false,
                Alignment.LEFT, VerticalAlignment.CENTRE, false);
    }

    public WritableCellFormat getPlainBorderTopStyle() {
		/* find in existing */
        return buildStyle(PLAIN_BORDER_TOP_STYLE, DEFAUT_FONT, false, DEFAULT_FONT_HEIGHT, false, true, false, false, false,
                Alignment.LEFT, VerticalAlignment.CENTRE, false);
    }

    public WritableCellFormat getPlainBorderRightStyle() {
		/* find in existing */
        return buildStyle(PLAIN_BORDER_RIGHT_STYLE, DEFAUT_FONT, false, DEFAULT_FONT_HEIGHT, false, false, false, false, true,
                Alignment.LEFT, VerticalAlignment.CENTRE, false);
    }

    public WritableCellFormat buildStyle(String styleName, WritableFont.FontName font, boolean isBold, short height, boolean dollar,
                                         boolean topBorder, boolean bottomBorder, boolean leftBorder, boolean rightBorder,
                                         Alignment alignment, VerticalAlignment verticalAlignment, boolean header) {
		/* find in existing */
        WritableCellFormat style = null;
        style = findInExisting(styleName);
        if (style != null) {
            return style;
        }
		/* ==== */

        WritableFont writableFont = new WritableFont(font, height, isBold ? WritableFont.BOLD : WritableFont.NO_BOLD);
        if (dollar) {
            NumberFormat dollarCurrency =
                    new NumberFormat("_(\"$\"* # ##0_);_(\"$\"* (# ##0);_(\"$\"* \"-\"??_);_(@_)", NumberFormat.COMPLEX_FORMAT);
            style = new WritableCellFormat(dollarCurrency);
        } else {
            style = new WritableCellFormat();
        }
        style.setFont(writableFont);
        // Lets automatically wrap the cells
        try {
            style.setWrap(true);
            if (topBorder)
                style.setBorder(Border.TOP, BorderLineStyle.THIN);
            if (bottomBorder)
                style.setBorder(Border.BOTTOM, BorderLineStyle.THIN);
            if (leftBorder)
                style.setBorder(Border.LEFT, BorderLineStyle.THIN);
            if (rightBorder)
                style.setBorder(Border.RIGHT, BorderLineStyle.THIN);
            style.setAlignment(alignment);
            style.setVerticalAlignment(verticalAlignment);
            if (header) {
                style.setBackground(Colour.GREY_25_PERCENT);
            }
        } catch (WriteException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return style;
    }

    private static WritableCellFormat findInExisting(String styleName) {
        if (existingStyles.containsKey(styleName)) {
            return existingStyles.get(styleName);
        }
        return null;
    }

}
