package com.scnsoft.fotaflo.webapp.util;

import com.scnsoft.fotaflo.webapp.bean.PictureToFTPBean;
import com.scnsoft.fotaflo.webapp.service.ImageService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 01.08.14
 * Time: 17:46
 * To change this template use File | Settings | File Templates.
 */
public class FTPUploadAndSendMessageTask {

    protected static Logger logger = Logger.getLogger("MailSender");

    @Autowired
    private ImageService imageService;

    @Value("${fotaflo.email.bcc}")
    private String bccEmail;

    private class MessageFTPSendTask implements Runnable {

        //  private SendMessagesBean sendMessagesBean;

        List<String> toSendEmails;
        String fromEmail;
        Map<String, File> attachment;
        String subject;
        String email_body;
        String smtpServer;
        String port;
        String password;
        String ssl;
        private MailSenderUtil ms;

        public MessageFTPSendTask(MailSenderUtil ms, List<String> toSendEmails, String fromEmail, String subject, String email_body,
                                  String smtpServer, String port, String password, String ssl, Map<String, File> attachment) {

            this.toSendEmails = toSendEmails;
            this.fromEmail = fromEmail;
            this.subject = subject;
            this.email_body = email_body;
            this.smtpServer = smtpServer;
            this.port = port;
            this.password = password;
            this.ssl = ssl;
            this.ms = ms;
            this.attachment = attachment;
        }

        public void run() {
            boolean sslBoolean = "on".equals(ssl);
            ms.sendMailMessage(toSendEmails, fromEmail, subject, email_body, attachment, smtpServer, port, password, sslBoolean, null);
        }

    }

    private class FTPSendTask implements Runnable {

        PictureToFTPBean picturesToFTP;
        private MailSenderUtil ms;

        private FTPSendTask(PictureToFTPBean picturesToFTP, MailSenderUtil ms) {
            this.ms = ms;
            this.picturesToFTP = picturesToFTP;
        }

        public void run() {
            logger.info("I starting to upload pictures to FTP");
            ms.uploadPicturesToFTP(picturesToFTP);
            logger.info("The pcitures to FTP uploaded successfully");
        }
    }

    PictureToFTPBean picturesToFTP;

    String location;

    String packageName;

    List<String> toSendEmails;

    Map<String, File> attachment;

    String subject;

    String email_body;

    String fromEmail;

    String smtpServer;

    String port;

    String password;

    String ssl;

    private TaskExecutor taskExecutor;

    public FTPUploadAndSendMessageTask(TaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

    public void sendMessages() {
        MailSenderUtil ms = new MailSenderUtil();
        taskExecutor.execute(new MessageFTPSendTask(ms, toSendEmails, fromEmail, subject, email_body,
                smtpServer, port, password, ssl, attachment));

    }

    public void sendToFTP() {
        MailSenderUtil ms = new MailSenderUtil();
        taskExecutor.execute(new FTPSendTask(picturesToFTP, ms));

    }

    public PictureToFTPBean getPicturesToFTP() {
        return picturesToFTP;
    }

    public void setPicturesToFTP(PictureToFTPBean picturesToFTP) {
        this.picturesToFTP = picturesToFTP;
    }


    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        SendMessagesTask.logger = logger;
    }

    public Map<String, File> getAttachment() {
        return attachment;
    }

    public void setAttachment(Map<String, File> attachment) {
        this.attachment = attachment;
    }

    public List<String> getToSendEmails() {
        return toSendEmails;
    }

    public void setToSendEmails(List<String> toSendEmails) {
        this.toSendEmails = toSendEmails;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getEmail_body() {
        return email_body;
    }

    public void setEmail_body(String email_body) {
        this.email_body = email_body;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getSmtpServer() {
        return smtpServer;
    }

    public void setSmtpServer(String smtpServer) {
        this.smtpServer = smtpServer;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public TaskExecutor getTaskExecutor() {
        return taskExecutor;
    }

    public void setTaskExecutor(TaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

    public String getSsl() {
        return ssl;
    }

    public void setSsl(String ssl) {
        this.ssl = ssl;
    }

}
