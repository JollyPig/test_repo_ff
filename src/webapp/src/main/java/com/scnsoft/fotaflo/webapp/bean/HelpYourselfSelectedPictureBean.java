package com.scnsoft.fotaflo.webapp.bean;


/**
 * Created by Nadezda Drozdova
 * Date: Apr 27, 2011
 * Time: 1:53:40 PM
 */
public class HelpYourselfSelectedPictureBean extends PictureBean {
    int helpyourselfId;
    Boolean selectedToSend;
    Integer toPrintNumber;

    public HelpYourselfSelectedPictureBean(int id, String name, String url, String cameraId, String cameraName,
                                           String creationDate, Boolean selected, Boolean selectedToSend, Integer toPrintNumber, int helpyourselfId, Boolean rotated, String pictureSize) {
        super(id, name, url, cameraId, cameraName, creationDate, selected, rotated, pictureSize);
        this.selectedToSend = selectedToSend;
        this.toPrintNumber = toPrintNumber;
        this.helpyourselfId = helpyourselfId;

    }

    public Boolean isSelectedToSend() {
        return selectedToSend;
    }

    public void setSelectedToSend(Boolean selectedToSend) {
        this.selectedToSend = selectedToSend;
    }

    public Integer getToPrintNumber() {
        return toPrintNumber;
    }

    public void setToPrintNumber(Integer toPrintNumber) {
        this.toPrintNumber = toPrintNumber;
    }

    public int getHelpyourselfId() {
        return helpyourselfId;
    }

    public void setHelpyourselfId(int helpyourselfId) {
        this.helpyourselfId = helpyourselfId;
    }
}
