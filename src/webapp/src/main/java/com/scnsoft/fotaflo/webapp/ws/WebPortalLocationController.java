package com.scnsoft.fotaflo.webapp.ws;

import com.scnsoft.fotaflo.webapp.bean.LocationBean;
import com.scnsoft.fotaflo.webapp.bean.LocationDetailsBean;
import com.scnsoft.fotaflo.webapp.bean.LocationMetadataBean;
import com.scnsoft.fotaflo.webapp.bean.PictureBean;
import com.scnsoft.fotaflo.webapp.controller.MainController;import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.model.LocationImageMetadata;
import com.scnsoft.fotaflo.webapp.model.Picture;
import com.scnsoft.fotaflo.webapp.service.LocationService;
import com.scnsoft.fotaflo.webapp.service.PictureService;
import com.scnsoft.fotaflo.webapp.util.DateConvertationUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 22.04.14
 * Time: 15:52
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/web/locations")
public class WebPortalLocationController {
    protected static Logger logger = Logger.getLogger(MainController.class);

    @Autowired
    private LocationService locationService;

    @Autowired
    private PictureService pictureService;

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public
    @ResponseBody
    LocationDetailsBean getLocation(@PathVariable String id) {
        Location location = locationService.getLocationById(id);
        if (location == null) {
            return null;
        }
        LocationDetailsBean details = new LocationDetailsBean(location.getId(), location.getLocationName());
        if (location.getLocationSettings() != null) {
            details.setEmailFooter(location.getLocationSettings().getArea());
        }
        details.setFromEmail(location.getLocationFromEmail());
        PictureBean bean;
        for (Picture p : location.getLocationAddPictures()) {
            bean = new PictureBean(p.getId(), p.getName(), p.getUrl(), null, null, DateConvertationUtils.viewStringDateFormat(p.getCreationDate()), null, null, "");
            bean.setLogoUrl(location.getLocationImageLogoUrl());
            bean.setLogoText(location.getLocationTextLogo());
            bean.setLogoMainUrl(pictureService.getMainLogo(location));
            details.addPicture(bean);
        }
        return details;
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public
    @ResponseBody
    List<LocationBean> getLocations() {
        List<LocationBean> result = locationService.getAllLocations();
        return result;
    }

    @RequestMapping(value = "{id}/metadata", method = RequestMethod.GET)
    public
    @ResponseBody
    LocationMetadataBean getLocationMetadata(@PathVariable String id) {
        LocationMetadataBean bean = null;
        Location location = locationService.getLocationById(id);
        if (location == null) {
            return null;
        }

        LocationImageMetadata imageMetadata = location.getImageMetadata();

        if(imageMetadata != null){
            bean = new LocationMetadataBean();
            bean.setTitle(imageMetadata.getTitleMetadata());
            bean.setComment(imageMetadata.getCommentMetadata());
            bean.setCopyright(imageMetadata.getCopyrightMetadata());
        }

        return bean;
    }
}
