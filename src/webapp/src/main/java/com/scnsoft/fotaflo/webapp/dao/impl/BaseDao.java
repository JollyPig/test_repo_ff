package com.scnsoft.fotaflo.webapp.dao.impl;

import com.scnsoft.fotaflo.common.model.IEntity;
import com.scnsoft.fotaflo.webapp.dao.IDao;
import org.hibernate.NonUniqueObjectException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateSystemException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.Collection;
import java.util.List;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */

public class BaseDao extends HibernateDaoSupport implements IDao {

    public Integer create(IEntity entity) {
        return (Integer) getHibernateTemplate().save(entity);
    }

    public IEntity retrieve(Class entityClass, Integer id) {
        return (IEntity) getHibernateTemplate().load(entityClass, id);
    }

    public List retrieveAll(Class entityClass) {
        return getHibernateTemplate().loadAll(entityClass);
    }

    public List retrieveAll(Class entityClass, String orderBy, boolean asc) {
        String query = "from "
                + entityClass.getName()
                + " pobj"
                + ((orderBy == null) ? "" : (" order by pobj." + orderBy)
                + (asc ? " asc" : " desc"));
        return getHibernateTemplate().find(query);
    }


    public void delete(Class entityClass, Integer id) {
        getHibernateTemplate().delete(retrieve(entityClass, id));
    }

    public void delete(IEntity entity) {
        getHibernateTemplate().delete(entity);
    }

    public void deleteAll(Collection objects) {
        getHibernateTemplate().deleteAll(objects);
    }

    public void update(IEntity entity) {
        try {
            getHibernateTemplate().saveOrUpdate(entity);
        } catch (HibernateSystemException ex) {
            if (ex.getCause() instanceof NonUniqueObjectException) {
                getHibernateTemplate().merge(entity);
            } else {
                throw ex;
            }
        }
    }

    public void deleteInBatch(Iterable<IEntity> entities) {
        if (entities == null) {
            throw new IllegalArgumentException("Arguments not be must empty");
        }
        Session session = getHibernateTemplate().getSessionFactory().openSession();
        session.beginTransaction();
        int i = 0;
        for (IEntity entity : entities) {
            session.delete(entity);
            if (i++ % 20 == 0) {
                session.flush();
                session.clear();
            }
        }
        session.getTransaction().commit();
        session.close();
    }

    public void saveOrUpdateInBatch(Iterable<IEntity> entities) {
        if (entities == null) {
            throw new IllegalArgumentException("Arguments not be must empty");
        }
        Session session = getHibernateTemplate().getSessionFactory().openSession();
        session.beginTransaction();
        int i = 0;
        for (IEntity entity : entities) {
            session.saveOrUpdate(entity);
            if (i++ % 20 == 0) {
                session.flush();
                session.clear();
            }
        }
        session.getTransaction().commit();
        session.close();
    }

}
