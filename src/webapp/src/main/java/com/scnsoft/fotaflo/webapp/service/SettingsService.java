package com.scnsoft.fotaflo.webapp.service;

import com.scnsoft.fotaflo.webapp.dao.ISettingsDao;
import com.scnsoft.fotaflo.webapp.model.SystemUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional(propagation = Propagation.REQUIRED)
public class SettingsService {

    @Autowired
    private UserService userService;

    @Autowired
    private ISettingsDao settingsDao;

    public void updateUserSettings(String userLogin, Integer pictureCount, Integer speed, Integer startTime, Integer delay) {
        SystemUser user = userService.getUserByLogin(userLogin);
        settingsDao.updateUserSettings(user, pictureCount, speed, startTime, delay);
    }

    public void updateStartTimeSettings(String userLogin, Integer startTime) {
        SystemUser user = userService.getUserByLogin(userLogin);
        settingsDao.updateUserSettings(user, user.getUserSettings().getPictureCount(), user.getUserSettings().getSpeed(), startTime, user.getUserSettings().getDelay());
    }

}
