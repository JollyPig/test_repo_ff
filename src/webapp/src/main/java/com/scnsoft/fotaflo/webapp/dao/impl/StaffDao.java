package com.scnsoft.fotaflo.webapp.dao.impl;

import com.scnsoft.fotaflo.webapp.dao.IStaffDao;
import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.model.Staff;
import org.hibernate.Query;

import java.util.List;

/**
 * Created by Drazdova
 */
public class StaffDao extends BaseDao implements IStaffDao {

    @Override
    public List<Staff> getStaffByLocation(Location location) {
        return getHibernateTemplate().find(
                "from Staff staff where staff.location = ?", location);
    }

    @Override
    public List<Staff> getPagedStaffByLocation(Location location, int start, int limit) {
        Query query = null;
        if (location != null) {
            query = getSession().createQuery("from Staff staff where staff.location = ?");
            query.setEntity(0, location);
        } else {
            query = getSession().createQuery("from Staff staff");
        }
        query.setFirstResult(start);
        query.setMaxResults(limit);
        return query.list();
    }

    @Override
    public Integer countStaffForLocation(Location location) {
        Object res = null;
        if (location != null) {
            Query q = getSession().createQuery("select count(staff) from Staff staff where staff.location = ?");
            q.setEntity(0, location);
            res = q.uniqueResult();
        } else {
            Query q = getSession().createQuery("select count(staff) from Staff staff");
            res = q.uniqueResult();
        }
        return ((Long) res).intValue();
    }

    @Override
    public List getStatisticForStaff() {
        StringBuffer query = new StringBuffer();
        query.append("select statistic from Statistic as statistic");
        return getSession().createQuery(query.toString()).list();
    }
}
