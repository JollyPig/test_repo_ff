package com.scnsoft.fotaflo.webapp.dao;

import com.scnsoft.fotaflo.webapp.common.ResultList;
import com.scnsoft.fotaflo.webapp.model.*;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 31.03.2012
 * Time: 10:29:02
 * To change this template use File | Settings | File Templates.
 */
public interface IPurchaseSelectedDao {

    public PurchaseSelected getPurchsePictureByUser(Picture picture, SystemUser user);

    public List<PurchaseSelected> getPurchsePictureByUser(SystemUser user);

    public ResultList<PurchaseSelected> getPicturesSelectedByUser(int start, int limit, SystemUser user);

    public ResultList<RequestsSelected> getPicturesSelectedByRequest(int start, int limit, Requests requests);
}
