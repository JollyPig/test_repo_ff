package com.scnsoft.fotaflo.webapp.dao;

import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.model.Purchase;
import com.scnsoft.fotaflo.webapp.model.PurchaseEmail;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 03.12.13
 * Time: 13:52
 * To change this template use File | Settings | File Templates.
 */
public interface IPurchaseDao {

    public List<Purchase> getPurchaseByStatusAndLocation(Location location, int status);

    public List<Purchase> findPurchasesByFilter(Location location, Date startDate, Date endDate, String email, String code, String tag, int status);

    public List<Purchase> getPurchaseByStatus(int status);

    public List<PurchaseEmail> getEmailsByParameters(Location location, Date startDate, Date endDate);

    public Purchase getPurchaseByCode(String generatedCode);

}
