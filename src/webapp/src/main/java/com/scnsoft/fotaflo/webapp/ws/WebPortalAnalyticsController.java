package com.scnsoft.fotaflo.webapp.ws;

import com.scnsoft.fotaflo.common.util.DateFormatUtil;
import com.scnsoft.fotaflo.webapp.bean.PurchaseEmailBean;
import com.scnsoft.fotaflo.webapp.controller.MainController;
import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.service.LocationService;
import com.scnsoft.fotaflo.webapp.service.PackageService;
import com.scnsoft.fotaflo.webapp.service.PurchaseService;
import com.scnsoft.fotaflo.webapp.service.StatisticService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 10.09.14
 * Time: 16:18
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/web/purchases")
public class WebPortalAnalyticsController {
    protected static Logger logger = Logger.getLogger(MainController.class);

    @Autowired
    private LocationService locationService;

    @Autowired
    private PurchaseService purchaseService;

    @Autowired
    private PackageService packageService;

    @Autowired
    private StatisticService statisticService;

    @RequestMapping(value = "count", method = RequestMethod.GET)
    public
    @ResponseBody
    Integer getEmailCount(@RequestParam(value = "id", required = false) String id,
                         @RequestParam(value = "start") @DateTimeFormat(pattern = DateFormatUtil.ISO_DATETIME_FORMAT_PATTERN) Date startDate,
                         @RequestParam(value = "end") @DateTimeFormat(pattern = DateFormatUtil.ISO_DATETIME_FORMAT_PATTERN) Date endDate) {

        Location location = (id == null || id.equals("0")) ? null : locationService.getLocationById(id);
        Integer emails = purchaseService.getPurchaseEmailCount(location, startDate, endDate);

        return emails;
    }

    @RequestMapping(method = RequestMethod.GET)
    public
    @ResponseBody
    List<PurchaseEmailBean> getEmails(@RequestParam(value = "id", required = false) String id,
                                  @RequestParam(value = "start") @DateTimeFormat(pattern = DateFormatUtil.ISO_DATETIME_FORMAT_PATTERN) Date startDate,
                                  @RequestParam(value = "end") @DateTimeFormat(pattern = DateFormatUtil.ISO_DATETIME_FORMAT_PATTERN) Date endDate) {

        Location location = (id == null || id.equals("0")) ? null : locationService.getLocationById(id);
        List<PurchaseEmailBean> emails = purchaseService.getPurchaseEmails(location, startDate, endDate);

        return emails;
    }

    @RequestMapping(value = "report", method = RequestMethod.POST)
    public Boolean saveReportData(@RequestParam(value = "locationId") Integer locationId,
                               @RequestParam(value = "date", required = false) @DateTimeFormat(pattern = DateFormatUtil.ISO_DATETIME_FORMAT_PATTERN) Date date,
                               @RequestParam(value = "total") int total,
                               @RequestParam(value = "groupPackage") boolean groupPackage) {

        Location location = locationService.getLocation(locationId);
        Integer notebookPackageId;

        if(groupPackage){
            notebookPackageId = packageService.getGroupPhotoPackage(location);
            total = 1;
        }else{
            notebookPackageId = packageService.getSinglePhotoPackage(location);
        }

        if(date == null){
            date = new Date();
        }

        statisticService.insertStatisticItems(notebookPackageId, null, 0, date, 0, "", "", "", total, "", "", new ArrayList<String>());

        return true;
    }

}
