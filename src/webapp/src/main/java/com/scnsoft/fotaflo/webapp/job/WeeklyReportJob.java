package com.scnsoft.fotaflo.webapp.job;

import com.scnsoft.fotaflo.webapp.service.LocationService;
import com.scnsoft.fotaflo.webapp.service.PictureService;
import com.scnsoft.fotaflo.webapp.service.ReportService;
import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;


/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */
public class WeeklyReportJob extends QuartzJobBean {

    private ReportService reportService;

    private PictureService pictureService;

    private LocationService locationService;

    protected static Logger logger = Logger.getLogger(WeeklyPublicReportJob.class);

    protected void executeInternal(JobExecutionContext ctx) throws JobExecutionException {

        boolean result = reportService.generateAndSendWeeklyStatisticV2(ctx.getFireTime());
        if (!result) {
            logger.error("The error occurred during WEEKLY REPORT generation or sending ");
        }

    }

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }

    public void setPictureService(PictureService pictureService) {
        this.pictureService = pictureService;
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }
}