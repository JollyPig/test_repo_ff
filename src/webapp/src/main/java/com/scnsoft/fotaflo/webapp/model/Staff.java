package com.scnsoft.fotaflo.webapp.model;

import com.scnsoft.fotaflo.common.model.AbstractEntity;

import javax.persistence.*;

/**
 * Created by Drazdova
 */
@Entity
@Table(name = "staff")
public class Staff extends AbstractEntity implements Comparable<Staff> {

    String staffName;
    Location location;

    @Column(name = "staff_name", length = 64)
    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    @ManyToOne
    @JoinColumn(name = "location_id")
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public int compareTo(Staff o) {
        return this.getStaffName().compareTo(o.getStaffName());
    }

}
