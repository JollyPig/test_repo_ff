package com.scnsoft.fotaflo.webapp.controller;

import com.scnsoft.fotaflo.webapp.model.Camera;
import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.service.CameraService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.List;

/**
 * User: Litvinchuk
 * Date: 4/19/11
 * Time: 2:26 PM
 */
@Controller
@RequestMapping(value = "/devices")
public class BuildXMLController {

    @Autowired
    CameraService cameraService;

    protected static Logger logger = Logger.getLogger(BuildXMLController.class);

    @RequestMapping(value = "/locations.xml", method = RequestMethod.GET)
    public ModelAndView getLocationsXML(HttpServletRequest request) {
        ModelAndView result = new ModelAndView("devicesxml");

        String xml = new String();
        xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        xml += "<!DOCTYPE Server SYSTEM \"opt/pdos/etc/pdoslrd.dtd\">\n";
        xml += "<vacationpictures>\n<locations>\n";

        HashSet<Location> locations = new HashSet<Location>();

        List<Camera> cameraList = cameraService.getAllCameras();
        for (Camera camera : cameraList) {
            if (camera.getLocation() != null)
                locations.add(camera.getLocation());
        }

        for (Location location : locations) {
            xml += "<location>\n<name>" + location.getLocationName() + "</name>\n";
            for (Camera camera : cameraList) {
                if (camera.getLocation() != null)
                    if (camera.getLocation().getLocationName().equals(location.getLocationName())) {
                        xml += "<deviceid>" + camera.getCameraName() + "</deviceid>\n";
                    }
            }
            xml += "</location>\n";
        }

        xml += "</locations>\n</vacationpictures>";

        result.addObject("xml", xml);
        logger.info("LocationsXML was requested");
        return result;
    }

    @Autowired
    public void setCameraService(CameraService cameraService) {
        this.cameraService = cameraService;
    }
}
