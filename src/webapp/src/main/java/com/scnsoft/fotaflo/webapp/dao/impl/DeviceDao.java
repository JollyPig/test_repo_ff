package com.scnsoft.fotaflo.webapp.dao.impl;

import com.scnsoft.fotaflo.common.dao.AbstractDao;
import com.scnsoft.fotaflo.webapp.dao.IDeviceDao;
import com.scnsoft.fotaflo.webapp.model.RegisteredDevice;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DeviceDao extends AbstractDao<RegisteredDevice, Integer> implements IDeviceDao {
    @Override
    protected Class<RegisteredDevice> getDomainClass() {
        return RegisteredDevice.class;
    }

    @Override
    public RegisteredDevice getByRegisteredId(String registeredID) {
        Criteria criteria = getSession().createCriteria(RegisteredDevice.class);
        criteria.add(Restrictions.eq("registeredID", registeredID));
        List<RegisteredDevice> devices = criteria.list();
        if (devices.size() == 0) {
            return null;
        } else {
            return devices.get(0);
        }
    }
}
