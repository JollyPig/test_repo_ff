package com.scnsoft.fotaflo.webapp.dao;

import com.scnsoft.fotaflo.webapp.model.SystemUser;
import com.scnsoft.fotaflo.webapp.model.UserFilter;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 30.03.2012
 * Time: 12:15:21
 * To change this template use File | Settings | File Templates.
 */
public interface IFilterDao {

    public UserFilter getFilterByUser(SystemUser user);
}
