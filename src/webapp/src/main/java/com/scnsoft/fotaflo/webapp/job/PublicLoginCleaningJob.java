package com.scnsoft.fotaflo.webapp.job;

import com.scnsoft.fotaflo.webapp.service.UserService;
import com.scnsoft.fotaflo.webapp.util.AccessID;
import com.scnsoft.fotaflo.webapp.util.DateConvertationUtils;
import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 01.02.2012
 * Time: 19:00:13
 * To change this template use File | Settings | File Templates.
 */
public class PublicLoginCleaningJob extends QuartzJobBean {

    protected static org.apache.log4j.Logger logger = Logger.getLogger("PublicLoginCleaningJob");
    private UserService userService;

    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        Date currentDate = jobExecutionContext.getFireTime();
        Date fromDate = DateConvertationUtils.getStartDate(0, currentDate);
        Date toDate = DateConvertationUtils.getEndDate(0, currentDate);

        logger.info("Deleting PUBLIC logins starting from currentDate: " + fromDate + " ending with " + toDate);
        userService.deletePublicLoginExpired(fromDate, toDate, String.valueOf(AccessID.PUBLICUSER.getValue()));
        logger.info("Deleting EMAIL PUBLIC logins starting from currentDate: " + fromDate + " ending with " + toDate);
        userService.deletePublicLoginExpired(fromDate, toDate, String.valueOf(AccessID.PUBLICEMAILUSER.getValue()));
        logger.info("Deleting EMAIL TAG PUBLIC logins starting from currentDate: " + fromDate + " ending with " + toDate);
        userService.deletePublicLoginExpired(fromDate, toDate, String.valueOf(AccessID.PUBLICTAGUSER.getValue()));
    }


    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
