package com.scnsoft.fotaflo.webapp.controller;

import com.scnsoft.fotaflo.common.util.JsonMapper;
import com.scnsoft.fotaflo.webapp.bean.PictureBean;
import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.model.PublicStatistic;
import com.scnsoft.fotaflo.webapp.model.ReportEmails;
import com.scnsoft.fotaflo.webapp.model.SystemUser;
import com.scnsoft.fotaflo.webapp.service.*;
import com.scnsoft.fotaflo.webapp.util.Filter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Nadezda Drozdova
 * Date: 06.04.2011
 */

@Controller
public class ImportExportController {

    protected static Logger logger = Logger.getLogger(ImportExportController.class);
    private final static String successProperty = "success";
    private final static String messageProperty = "msg";

    @Autowired
    private LocationService locationService;

    @Autowired
    private CameraService cameraService;

    @Autowired
    private PictureService pictureService;

    @Autowired
    private ExportService exportService;

    @Autowired
    private UserService userService;

    @Autowired
    private StatisticService statisticService;

    @RequestMapping(value = "importPictures.json")
    public
    @ResponseBody
    ResponseEntity<String> importPictures(@RequestParam("pictures") List<MultipartFile> pictures,
                          @RequestParam("iDcameraCombo") Integer cameraId,
                          @RequestParam("iDlocationCombo") Integer locationId,
                          @RequestParam("uploadHours") Date uploadDate,
                          HttpServletResponse response) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        Map modelMap = new HashMap();
        try {
            for (MultipartFile picture : pictures) {
                pictureService.savePictureToDB(locationId, cameraId, uploadDate, picture);
            }
            modelMap.put("msg", "Files were imported successfully");
            modelMap.put(successProperty, true);
        } catch (IllegalStateException e) {
            logger.error(e.getMessage(), e);
            modelMap.put(messageProperty, "Import wasn't finished due internal error");
            modelMap.put(successProperty, false);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            modelMap.put(messageProperty, "Import wasn't finished due internal error");
            modelMap.put(successProperty, false);
        }

        logger.info("User [ " + currentUser + " ]" + " imported pictures for location: " + locationService.getLocation(locationId) + " camera: " + cameraService.getCamera(cameraId));

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("Content-Type", "text/html; charset=utf-8");
        return new ResponseEntity<String>(JsonMapper.getJson(modelMap), responseHeaders, HttpStatus.CREATED);
    }

    @RequestMapping(value = "getPicturesToExport.json", method = RequestMethod.POST)
    public
    @ResponseBody
    Map<String, Object> getPicturesToExport(@RequestParam("start") Date from,
                                            @RequestParam("end") Date to,
                                            @RequestParam("locationId") String locationId) {


        Map<String, Object> map = new HashMap<String, Object>();
        Filter filter = new Filter(from, to, locationId, "", "");
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        List<PictureBean> pictures = pictureService.getPicturesFiltered(currentUser, filter);
        map.put("size", pictures.size());
        return map;
    }

    @RequestMapping(value = "exportPictures.json", method = RequestMethod.POST)
    public void exportPictures(@RequestParam("start") Date from,
                               @RequestParam("end") Date to,
                               @RequestParam("locationId") String locationId,
                               @RequestParam("baseFileName") String fileBaseName,
                               HttpServletResponse response) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        Filter filter = new Filter(from, to, locationId, "", "");
        List<PictureBean> pictures = pictureService.getPicturesFiltered(currentUser, filter);
        int size = 0;
        try {
            byte[] ar = exportService.createZipArchive(pictures, fileBaseName);
            response.setContentType("application/zip");
            response.setHeader("Content-Encoding", "zip");
            response.setContentLength(ar.length);
            response.setHeader("Content-Disposition", "attachment;filename=\"archive.zip\"");
            response.getOutputStream().write(ar);
            response.getOutputStream().flush();

        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        logger.info("User [ " + currentUser + " ]" + " exported pictures for location: " + locationService.getLocation(Integer.valueOf(locationId)) + "for the period from: " + from + "to " + to);
    }

    @RequestMapping(value = "exportEmails.json", method = RequestMethod.POST)
    public void exportEmails(@RequestParam("dateFrom") String from,
                             @RequestParam("dateTo") String to,
                             @RequestParam("locationId") String locationId, HttpServletResponse response) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();

        Date date1 = new Date();
        Date date2 = new Date();
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            date1 = formatter.parse(from);
            date2 = formatter.parse(to);
        } catch (ParseException e) {

        }
        List<ReportEmails> emailReports = exportService.getReportEmails(date1, date2, locationService.getLocation(Integer.valueOf(locationId)));

        try {
            byte[] ar = exportService.createCSV(emailReports);
            response.setContentType("text/csv");
            response.setHeader("Content-Disposition", "attachment; filename=emailReport.csv");
            response.setContentLength(ar.length);
            ServletOutputStream out = response.getOutputStream();
            out.write(ar);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        logger.info("User [ " + currentUser + " ]" + " exported emails for location: " + locationService.getLocation(Integer.valueOf(locationId)) + "for the period from: " + date1 + "to " + date2);

    }

    @RequestMapping(value = "exportPublicReports.json", method = { RequestMethod.POST, RequestMethod.GET })
    public void exportPublicReports(@RequestParam("dateFrom") String from,
                                    @RequestParam("dateTo") String to,
                                    @RequestParam("locationId") String locationId, HttpServletResponse response) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();

        Date date1 = new Date();
        Date date2 = new Date();
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            date1 = formatter.parse(from);
            date2 = formatter.parse(to);
        } catch (ParseException e) {

        }
        List<PublicStatistic> publicStatistics = statisticService.getPublicStatisticForLocation(locationService.getLocation(Integer.valueOf(locationId)), date1, date2);

        try {
            byte[] ar = exportService.createTextIO(publicStatistics);
            response.setContentType("application/txt");
            response.setHeader("Content-Disposition", "attachment; filename=publicPuchase.txt");
            response.setContentLength(ar.length);
            ServletOutputStream out = response.getOutputStream();
            out.write(ar);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        logger.info("User [ " + currentUser + " ]" + " exported public reports for location: " + locationService.getLocation(Integer.valueOf(locationId)) + "for the period from: " + date1 + "to " + date2);
    }

    @RequestMapping("loadBanner.json")
    public
    @ResponseBody
    ResponseEntity<String> importBanner(@RequestParam("banner") String banner,
                                        @RequestParam("bannerPicture") MultipartFile bannerPicture,
                                        @RequestParam("locationId") Integer locationId) {
        Map<String, Object> modelMap = new HashMap<String, Object>();
        modelMap.put(successProperty, true);
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        try {
            Location location = locationService.getLocation(locationId);
            if (location != null) {
                pictureService.updateSlideShowBanner(location, banner, bannerPicture, currentUser);
                logger.info("User [ " + currentUser + " ]" + " updated banner : " + banner + " for the location:  " + location.getLocationName());
                modelMap.put(messageProperty, banner + " banner was updated successfully");
                modelMap.put(successProperty, true);
            }else{
                modelMap.put(messageProperty, "Cannot update image banner logo");
                modelMap.put(successProperty, false);
            }
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            modelMap.put(messageProperty, "Cannot update image banner logo");
            modelMap.put(successProperty, false);
        }

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("Content-Type", "text/html; charset=utf-8");
        return new ResponseEntity<String>(JsonMapper.getJson(modelMap), responseHeaders, HttpStatus.CREATED);
    }

    @RequestMapping("loadBannerRight.json")
    public
    @ResponseBody
    ResponseEntity<String> importBannerRight(@RequestParam("banner") String banner,
                                             @RequestParam("bannerPictureRight") MultipartFile bannerPicture,
                                             @RequestParam("locationId") Integer locationId) {
        return importBanner(banner, bannerPicture, locationId);
    }

    @RequestMapping("deleteBanner.json")
    public
    @ResponseBody
    ResponseEntity<String> deleteBanner(@RequestParam("banner") String banner,
                                        @RequestParam("bannerPicture") MultipartFile bannerPicture,
                                        @RequestParam("locationId") Integer locationId) {
        Map<String, Object> modelMap = new HashMap<String, Object>();
        modelMap.put(successProperty, true);

        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        try {
            Location location = locationService.getLocation(locationId);
            if (location != null) {
                pictureService.deleteSlideShowBanner(location, banner, currentUser);
                logger.info("User [ " + currentUser + " ]" + " deleted banner : " + banner + " for the location:  " + location.getLocationName());
                modelMap.put(messageProperty, banner + " banner was deleted successfully");
                modelMap.put(successProperty, true);
            }else{
                modelMap.put(messageProperty, "Cannot update image banner logo");
                modelMap.put(successProperty, false);
            }
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            modelMap.put(messageProperty, "Cannot update image banner logo");
            modelMap.put(successProperty, false);
        }

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("Content-Type", "text/html; charset=utf-8");
        return new ResponseEntity<String>(JsonMapper.getJson(modelMap), responseHeaders, HttpStatus.CREATED);
    }

    @RequestMapping("deleteBannerRight.json")
    public
    @ResponseBody
    ResponseEntity<String> deleteBannerRight(@RequestParam("banner") String banner,
                                             @RequestParam("bannerPictureRight") MultipartFile bannerPicture,
                                             @RequestParam("locationId") Integer locationId) {
        return deleteBanner(banner, bannerPicture, locationId);
    }

    @RequestMapping(value = "getBanners.json", method = RequestMethod.POST)
    public
    @ResponseBody
    Map<String, ? extends Object> getBanners(@RequestParam("type") String type, @RequestParam("locationId") String locationId) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        SystemUser systemUser = userService.getUserByLogin(currentUser);
        HashMap modelMap = new HashMap();

        modelMap.put("banners", pictureService.getSlideShowBanner(type, locationService.getLocation(Integer.valueOf(locationId))));
        modelMap.put(successProperty, true);
        return modelMap;
    }

    @RequestMapping(value = "getBanner.json")
    public
    @ResponseBody
    Map<String, ? extends Object> getBanner(@RequestParam("type") String type) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        SystemUser systemUser = userService.getUserByLogin(currentUser);
        HashMap modelMap = new HashMap();
        modelMap.put("avaliable", "false");
        try {
            boolean result = pictureService.getBanner(type, systemUser.getLocation());
            if (result) {
                modelMap.put("avaliable", "true");
            } else {
                modelMap.put("avaliable", "false");
            }
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }

        modelMap.put(successProperty, true);
        return modelMap;
    }

}
