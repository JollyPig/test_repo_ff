package com.scnsoft.fotaflo.webapp.bean;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 05.08.14
 * Time: 10:17
 * To change this template use File | Settings | File Templates.
 */
public class PictureToFTPBean {

    String server;
    String login;
    String password;
    String locationName;
    List<String> picturePathes;
    List<String> pictureNames;

    public PictureToFTPBean(String server, String login, String password, String locationName, List<String> picturePathes, List<String> pictureNames) {
        this.server = server;
        this.login = login;
        this.password = password;
        this.locationName = locationName;
        this.picturePathes = picturePathes;
        this.pictureNames = pictureNames;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public List<String> getPicturePathes() {
        return picturePathes;
    }

    public void setPicturePathes(List<String> picturePathes) {
        this.picturePathes = picturePathes;
    }

    public List<String> getPictureNames() {
        return pictureNames;
    }

    public void setPictureNames(List<String> pictureNames) {
        this.pictureNames = pictureNames;
    }
}
