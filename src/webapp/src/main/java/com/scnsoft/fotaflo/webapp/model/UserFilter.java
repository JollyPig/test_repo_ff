package com.scnsoft.fotaflo.webapp.model;

import com.scnsoft.fotaflo.common.model.AbstractEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */

@Entity
@Table(name = "userfilter")
public class UserFilter extends AbstractEntity {

    Date startDate;
    Date endDate;

    String locationId;
    String cameraId;
    String tagIds;
    SystemUser systemUser;
    String staff;

    List<Picture> pictures;

    public UserFilter() {
    }

    public UserFilter(Date startDate, Date endDate, String locationId, String cameraId, String tagIds, SystemUser systemUser) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.locationId = locationId;
        this.cameraId = cameraId;
        this.tagIds = tagIds;
        this.systemUser = systemUser;
    }

    @Column(name = "start_date")
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Column(name = "end_date")
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }


    /* @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})*/

    @Column(name = "location_id")
    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    /*   @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})*/

    @Column(name = "camera_id")
    public String getCameraId() {
        return cameraId;
    }

    public void setCameraId(String cameraId) {
        this.cameraId = cameraId;
    }

    @Column(name = "tag_id")
    public String getTagIds() {
        return tagIds;
    }

    public void setTagIds(String tagIds) {
        this.tagIds = tagIds;
    }

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "system_user_id")
    public SystemUser getSystemUser() {
        return systemUser;
    }

    public void setSystemUser(SystemUser systemUser) {
        this.systemUser = systemUser;
    }

    @Column(name = "staff_ids")
    public String getStaff() {
        return staff;
    }

    public void setStaff(String staff) {
        this.staff = staff;
    }

    @ManyToMany(targetEntity = Picture.class, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinTable(name = "UserFilter_Picture",
            joinColumns = @JoinColumn(name = "userfilter_id"),
            inverseJoinColumns = @JoinColumn(name = "pictures_id")
    )
    public List<Picture> getPictures() {
        return pictures;
    }


    public void setPictures(List<Picture> pictures) {
        this.pictures = pictures;
    }
}
