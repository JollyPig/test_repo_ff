package com.scnsoft.fotaflo.webapp.job;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.scnsoft.fotaflo.webapp.model.RegisteredDevice;
import com.scnsoft.fotaflo.webapp.service.DeviceService;
import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WakeUpAndroidJob extends QuartzJobBean {
    protected final static Logger logger = Logger.getLogger(WakeUpAndroidJob.class);

    private final static String INVALID_REGISTRATION = "InvalidRegistration";

    private final static String NOT_REGISTERED = "NotRegistered";

    private String apiKey;

    private DeviceService deviceService;

    public void setDeviceService(DeviceService deviceService) {
        this.deviceService = deviceService;
    }

    public void setApiKey(final String apiKey) {
        this.apiKey = apiKey;
    }

    @Override
    protected void executeInternal(final JobExecutionContext jobExecutionContext)
            throws JobExecutionException {
        List<RegisteredDevice> devices = deviceService.getAll();
        List<String> regIDs = new ArrayList<String>();
        for (RegisteredDevice device : devices) {
            regIDs.add(device.getRegisteredID());
        }
        if (regIDs.size() > 0) {
            Sender sender = new Sender(apiKey);
            Message message = new Message.Builder().build();
            try{
                MulticastResult result = sender.sendNoRetry(message, regIDs);
                List<Result> results = result.getResults();
                for (Result messageResult : results) {
                    if (INVALID_REGISTRATION.equals(messageResult.getErrorCodeName()) ||
                            NOT_REGISTERED.equals(messageResult.getErrorCodeName())) {
                        int index = results.indexOf(messageResult);
                        RegisteredDevice device = devices.get(index);
                        deviceService.delete(device);
                    }
                }
            }catch(IOException e) {
                logger.error(e.getMessage(), e);
            }catch(Exception e){
                logger.error(e.getMessage());
            }
        }
    }

}
