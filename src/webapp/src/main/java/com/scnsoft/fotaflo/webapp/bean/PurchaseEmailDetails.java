package com.scnsoft.fotaflo.webapp.bean;

import com.scnsoft.fotaflo.common.util.StringUtils;
import com.scnsoft.fotaflo.webapp.model.PurchaseEmail;
import com.scnsoft.fotaflo.webapp.model.ReportEmails;
import com.scnsoft.fotaflo.webapp.model.Subscribe;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class PurchaseEmailDetails {

    private Date creationDate;

    private List<Subscribe> subscribes = new ArrayList<Subscribe>();
    private List<PurchaseEmail> purchases = new ArrayList<PurchaseEmail>();
    private List<ReportEmails> reportEmails = new ArrayList<ReportEmails>();
    private List<Map<String, String>> credentials = new ArrayList<Map<String, String>>();
    private List<Map<String, String>> teasers = new ArrayList<Map<String, String>>();
    private List<String> emails = new ArrayList<String>();

    public PurchaseEmailDetails() {
        this.creationDate = new Date();
    }

    public PurchaseEmailDetails(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public List<Subscribe> getSubscribes() {
        return subscribes;
    }

    public void addSubscribe(Subscribe subscribe){
        if(subscribe != null){
            subscribes.add(subscribe);
        }
    }

    public List<PurchaseEmail> getPurchases() {
        return purchases;
    }

    public void addPurchase(PurchaseEmail purchaseEmail){
        if(purchaseEmail != null){
            purchases.add(purchaseEmail);
        }
    }

    public List<ReportEmails> getReportEmails() {
        return reportEmails;
    }

    public void addReportEmail(ReportEmails reportEmail){
        if(reportEmail != null){
            reportEmails.add(reportEmail);
        }
    }

    public List<Map<String, String>> getCredentials() {
        return credentials;
    }

    public void addCredential(Map<String, String> credential){
        if(credential != null){
            credentials.add(credential);
        }
    }

    public List<Map<String, String>> getTeasers() {
        return teasers;
    }

    public void addTeaser(Map<String, String> teaser){
        if(teaser != null){
            teasers.add(teaser);
        }
    }

    public List<String> getEmails() {
        return emails;
    }

    public void addEmail(String email){
        if(!StringUtils.isEmpty(email)){
            emails.add(email);
        }
    }
}
