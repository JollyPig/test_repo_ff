package com.scnsoft.fotaflo.webapp.model;

import com.scnsoft.fotaflo.common.model.AbstractEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Nadezda Drozdova
 * Date: Apr 27, 2011
 * Time: 12:39:10 PM
 */
@Entity
@Table(name = "requests")
public class Requests extends AbstractEntity {

    String emails;
    String comments;
    Date creationDate;
    SystemUser systemUser;
    Location location;

    private Set<RequestsSelected> requestsSelected = new HashSet<RequestsSelected>(0);

    @Column(name = "emails", length = 1024)
    public String getEmails() {
        return emails;
    }

    public void setEmails(String emails) {
        this.emails = emails;
    }

    @Column(name = "comments", length = 1024)
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Column(name = "creationdate")
    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @ManyToOne
    @JoinColumn(name = "system_user_id")
    public SystemUser getSystemUser() {
        return systemUser;
    }

    public void setSystemUser(SystemUser systemUser) {
        this.systemUser = systemUser;
    }

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE}, mappedBy = "request")
    public Set<RequestsSelected> getRequestsSelected() {
        return requestsSelected;
    }

    public void setRequestsSelected(Set<RequestsSelected> requestsSelected) {
        this.requestsSelected = requestsSelected;
    }

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "location_id")
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
