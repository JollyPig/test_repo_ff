package com.scnsoft.fotaflo.webapp.service;

import com.scnsoft.fotaflo.webapp.dao.IReportDao;
import com.scnsoft.fotaflo.webapp.dao.impl.BaseDao;
import com.scnsoft.fotaflo.webapp.model.*;
import com.scnsoft.fotaflo.webapp.model.Package;
import com.scnsoft.fotaflo.webapp.util.DateConvertationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by Nadezda Drozdova
 * Date: May 5, 2011
 * Time: 2:29:39 PM
 */

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class StatisticService {

    @Autowired
    private BaseDao baseDao;

    @Autowired
    private IReportDao reportDao;

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public void insertStatisticItems(Integer idPackage, List<Integer> staffs, int numberOfParticipant,
                                     Date date, int numberPrinted, String emails, String subscribe, String teasers,
                                     int totalNumner, String anyPictureName, String emailTopic, List<String> cameras) {
        Package pack = (Package) baseDao.retrieve(Package.class, idPackage);
        Statistic statistic = new Statistic();
        statistic.setPackages(pack);
        statistic.setAnyPictureName(anyPictureName);
        statistic.setNumberOfParticipantAll(numberOfParticipant);
        statistic.setCreationDate(date);
        statistic.setNumberToPrintAll(numberPrinted);
        statistic.setEmails(emails);
        statistic.setSubscribeEmails(subscribe);
        statistic.setTeasers(teasers);
        statistic.setTotalPictureNumber(totalNumner);
        statistic.setEmailSubject(emailTopic);
        String cameraStatistic = "";
        if(cameras != null && !cameras.isEmpty()){
            for (String camera : cameras) {
                if (cameraStatistic.equals("")) {
                    cameraStatistic = camera;
                } else {
                    cameraStatistic = cameraStatistic + ", " + camera;
                }
            }
        }
        statistic.setCameraNames(cameraStatistic);
        if (staffs != null && !staffs.isEmpty()) {
            Set<Staff> staffArray = new HashSet<Staff>();
            for (Integer staffId : staffs) {
                Staff staff = (Staff) baseDao.retrieve(Staff.class, staffId);
                staffArray.add(staff);
            }
            statistic.setStaff(staffArray);
        }

        baseDao.create(statistic);
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public void insertPublicStatisticItems(Integer idPackage, String email, String userLogin,
                                           Date date, int numberSent) {
        Package pack = (Package) baseDao.retrieve(Package.class, idPackage);
        Location lock = null;
        if (pack != null) {
            lock = pack.getLocation();
        }
        PublicStatistic statistic = new PublicStatistic();
        statistic.setPackages(pack);
        statistic.setLocation(lock);
        statistic.setCreationDate(date);
        statistic.setNumberOfPicturesSent(numberSent);
        statistic.setParticipantEmail(email);
        statistic.setUserLogin(userLogin);
        baseDao.create(statistic);
    }

    public HashMap getPublicStatistic(Date fireTime) {
        HashMap<Integer, List> statisticByLocationGeneral = new HashMap<Integer, List>();
        Date start = DateConvertationUtils.getStartDate(6, fireTime);
        Date end = DateConvertationUtils.getEndDate(0, fireTime);
        List locations = reportDao.getLocations();
        Iterator itLocations = locations.iterator();
        while (itLocations.hasNext()) {
            Location location = (Location) itLocations.next();
            List<PublicStatistic> publicStatistics = reportDao.getPublicStatisticByLocation(start, end, location);
            statisticByLocationGeneral.put(location.getId(), publicStatistics);
        }

        return statisticByLocationGeneral;
    }

    public List<PublicStatistic> getPublicStatisticForLocation(Location location, Date startTime, Date endDate) {
        Date start = DateConvertationUtils.getStartDate(0, startTime);
        Date end = DateConvertationUtils.getEndDate(0, endDate);
        if (location != null) {
            return reportDao.getPublicStatisticByLocation(start, end, location);
        }

        return null;
    }
}
