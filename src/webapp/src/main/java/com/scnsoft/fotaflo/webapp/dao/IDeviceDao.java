package com.scnsoft.fotaflo.webapp.dao;

import com.scnsoft.fotaflo.webapp.model.RegisteredDevice;

public interface IDeviceDao extends com.scnsoft.fotaflo.common.dao.IDao<RegisteredDevice, Integer> {

    RegisteredDevice getByRegisteredId(String registeredID);

}
