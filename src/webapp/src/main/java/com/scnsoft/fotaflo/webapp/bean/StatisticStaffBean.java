package com.scnsoft.fotaflo.webapp.bean;

import com.scnsoft.fotaflo.webapp.model.Staff;

/**
 * Created Paradinets Tatsiana.
 * Date: 13.05.2011
 * Time: 13:05:00
 */
public class StatisticStaffBean implements Comparable<StatisticStaffBean> {

    Staff staff;
    Long staffnumber;
    Long participantSum;

    public Staff getStaff() {
        return staff;
    }

    public void setStaff(Staff staff) {
        this.staff = staff;
    }

    public Long getStaffnumber() {
        return staffnumber;
    }

    public void setStaffnumber(Long staffnumber) {
        this.staffnumber = staffnumber;
    }

    public int compareTo(StatisticStaffBean o) {
        return this.staff.getStaffName().compareTo(o.staff.getStaffName());
    }

    public Long getParticipantSum() {
        return participantSum;
    }

    public void setParticipantSum(Long participantSum) {
        this.participantSum = participantSum;
    }
}
