package com.scnsoft.fotaflo.webapp.model;

import com.scnsoft.fotaflo.common.model.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by
 * User: Paradinets
 * Date: 23.12.2011
 * Time: 18:12:44
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "contactEmail")
public class ContactEmail extends AbstractEntity {

    String email;

    @Column(name = "email", length = 255)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}