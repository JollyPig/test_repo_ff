package com.scnsoft.fotaflo.webapp.validators;

import com.scnsoft.fotaflo.webapp.exceptions.FormValidationException;

import java.util.regex.Pattern;

public class QrTagSlideShowFormValidator {

    public static final String PATTERN = "^[A-Z0-9]*$";

    private final String value;

    public QrTagSlideShowFormValidator (final String value) {
        this.value = value;
    }

    public String validate () throws FormValidationException {
        SharedValidator.validate ("QR tag", value).required ();

        final Pattern pattern = Pattern.compile (PATTERN);

        if (! pattern.matcher (value).matches ()) {
            throw new FormValidationException ("QR tag should contain only capital letters and numbers.");
        }

        return value;
    }

}
