package com.scnsoft.fotaflo.webapp.controller;

import com.scnsoft.fotaflo.common.util.JsonMapper;
import com.scnsoft.fotaflo.common.util.NumberUtils;
import com.scnsoft.fotaflo.common.util.StringUtils;
import com.scnsoft.fotaflo.webapp.bean.TagSavedPurchaseBean;
import com.scnsoft.fotaflo.webapp.model.*;
import com.scnsoft.fotaflo.webapp.service.*;
import com.scnsoft.fotaflo.webapp.util.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 19.11.13
 * Time: 11:38
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class SavePurchaseController {
    protected static Logger logger = Logger.getLogger(PurchaseController.class);

    @Autowired
    private PurchaseService purchaseService;

    @Autowired
    private UserService userService;

    @Autowired
    private PublicLoginService publicLoginService;

    @Autowired
    private LocationService locationService;

    @Autowired
    private PictureService pictureService;

    @Autowired
    private TagPurchaseService tagPurchaseService;

    private final static String successProperty = "success";

    private final static String codeProperty = "code";

    private final static String msgProperty = "msg";

    @RequestMapping(value = "firstSavePurchase.json")
    public
    @ResponseBody
    Map<String, ? extends Object> firstSavePurchaseForm(HttpServletRequest httpServletRequest) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        SystemUser user = userService.getUserByLogin(currentUser);

        HashMap<String, Object> modelMap = new HashMap<String, Object>();
        Filter filter = FotafloUtil.initFilterForCatalog(httpServletRequest);
        String purchIdFromRequest = httpServletRequest.getParameter("purchaseId");
        Integer purchaseIdCatalog = null;
        Integer purchaseId = null;
        String locationRequest = filter.getLocation();
        if (!user.getAccess().equals(String.valueOf(AccessID.ADMIN.getValue()))) {
            locationRequest = String.valueOf(user.getLocation().getId());
        }
        if (purchIdFromRequest != null && purchIdFromRequest != "-1" && !purchIdFromRequest.equals("")) {
            Purchase purchase = purchaseService.getPurchaseById(Integer.valueOf(purchIdFromRequest));

            if (purchase.getStatusCode() == 3) {
                purchaseId = purchaseService.copyPurchase(purchase);

            } else {
                purchaseService.updatePurchase(purchaseService.getPurchaseById(Integer.valueOf(purchIdFromRequest)), locationRequest, filter.getCamera(), filter.getTag(),
                        filter.getStartDate(), filter.getEndDate());
                purchaseIdCatalog = Integer.parseInt(purchIdFromRequest);
            }
        } else {
            purchaseId = purchaseService.savePurchase(user, locationRequest, filter.getCamera(), filter.getTag(),
                    filter.getStartDate(), filter.getEndDate());
        }

        modelMap.put(successProperty, true);
        modelMap.put("purchaseId", purchaseId);
        modelMap.put("purchaseIdCatalog", purchaseIdCatalog);

        return modelMap;
    }

    @RequestMapping(value = "applyPurchase.json", method = RequestMethod.POST)
    public
    @ResponseBody
    Map<String, ? extends Object> applyRequests(HttpServletRequest httpServletRequest) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        HashMap modelMap = new HashMap();
        int purchaseId = Integer.valueOf(httpServletRequest.getParameter("purchaseId") != null ? httpServletRequest.getParameter("purchaseId") : "-1");
        modelMap.put("purchaseId", purchaseId);
        logger.info("User [ " + currentUser + " ]" + " applied the purchase " + purchaseId);
        modelMap.put(successProperty, true);
        return modelMap;
    }

    @RequestMapping(value = "deletePurchase.json", method = RequestMethod.POST)
    public
    @ResponseBody
    Map<String, ? extends Object> deleteRequests(HttpServletRequest httpServletRequest) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        HashMap modelMap = new HashMap();
        int purchaseId = Integer.valueOf(httpServletRequest.getParameter("purchaseId") != null ? httpServletRequest.getParameter("purchaseId") : "-1");
        boolean result = purchaseService.removePurchase(purchaseId);
        modelMap.put(successProperty, result);
        return modelMap;
    }

    @RequestMapping(value = "saveBeforePurchaseForm.json")
    public
    @ResponseBody
    Map<String, ? extends Object> saveBeforePurchaseForm(@RequestParam(value = "location", required = false) String idLocation,
                                                         @RequestParam(value = "package", required = false) String idPackage,
                                                         @RequestParam(value = "participant", required = false) String participant,
                                                         @RequestParam(value = "participantOther", required = false) String participantOther,
                                                         @RequestParam(value = "staff", required = false) String idStaff,
                                                         @RequestParam(value = "subject", required = false) String subject,
                                                         @RequestParam(value = "strfilename", required = false) String attach_filename,
                                                         @RequestParam(value = "emailbody", required = false) String email_body,
                                                         @RequestParam(value = "purchaseId", required = false) String purchaseId,
                                                         @RequestParam(value = "purchaseIdCat", required = false) String purchaseIdCat,
                                                         @RequestParam(value = "children", required = false) Object data,
                                                         @RequestParam(value = "emails", required = false) Object emails,
                                                         @RequestParam(value = "emailsOther", required = false) Object emailsOther,
                                                         HttpServletRequest httpServletRequest) {

        String emailBodyFace = email_body;
        HashMap<String, Object> modelMap = new HashMap<String, Object>();
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User " + currentUser + " saved the purchase form");
        SystemUser user = userService.getUserByLogin(currentUser);

        Purchase purchase = purchaseService.getPurchaseById(Integer.parseInt(purchaseId));

        if (purchase == null) {
            modelMap.put(msgProperty, "the purchase do not exist somehow");
        } else {
            Location location = locationService.getLocationById(idLocation);
            if (location == null) {
                location = user.getLocation();
            }
            String generatedLogin = purchase.getGeneratedCode();
            if (generatedLogin == null || purchase.getStatusCode() == 3) {
                generatedLogin = publicLoginService.generateABTPublicLoginCode("A");
            }

            int result = purchaseService.savePurchase(3, purchase, user, generatedLogin, idLocation, idPackage, participant, participantOther,
                    idStaff, subject, attach_filename, email_body, data, emails, emailsOther, purchaseIdCat);

            Date currentDate = new Date();
            Map<String, String> createdUserCredentials = publicLoginService.createPurchasePublicLogin(location, new Date(), DateConvertationUtils.getStartDate(21, currentDate), String.valueOf(AccessID.PUBLICPURCHASEUSER.getValue()), generatedLogin);

            UserFilter publicUserFilter = new UserFilter();
            publicUserFilter.setCameraId(purchase.getCameras());
            publicUserFilter.setLocationId(String.valueOf(purchase.getLocationPurchase().getId()));
            publicUserFilter.setStartDate(purchase.getStartDate());
            publicUserFilter.setEndDate(purchase.getEndDate());
            if (idStaff != null && idStaff.length() != 0) {
                publicUserFilter.setStaff(idStaff);
            }
            Set<PurchasePictures> purchasePictureses = purchase.getPurchasePictures();

            List<Picture> pics = new ArrayList<Picture>();

            String pictus = "";

            for (PurchasePictures selected : purchasePictureses) {
                pics.add(selected.getPicture());
                if (selected.getPicture() != null) {
                    pictus = pictus + " " + selected.getPicture().getName();
                }
            }
            publicUserFilter.setPictures(pics);
            logger.info("User " + currentUser + "saves the following pictures to public purchasing user " + pictus);
            pictureService.saveFilter(publicUserFilter, createdUserCredentials.get("login"));
            modelMap.put(successProperty, true);
            modelMap.put(codeProperty, generatedLogin);
            modelMap.put("reload", -1);
        }

        return modelMap;
    }

    @RequestMapping(value = "savePurchaseForm.json")
    public
    @ResponseBody
    Map<String, ? extends Object> savePurchaseForm(@RequestParam(value = "location", required = false) String idLocation,
                                                   @RequestParam(value = "package", required = false) String idPackage,
                                                   @RequestParam(value = "participant", required = false) String participant,
                                                   @RequestParam(value = "participantOther", required = false) String participantOther,
                                                   @RequestParam(value = "staff", required = false) String idStaff,
                                                   @RequestParam(value = "subject", required = false) String subject,
                                                   @RequestParam(value = "strfilename", required = false) String attach_filename,
                                                   @RequestParam(value = "emailbody", required = false) String email_body,
                                                   @RequestParam(value = "purchaseId", required = false) String purchaseId,
                                                   @RequestParam(value = "purchaseIdCat", required = false) String purchaseIdCat,
                                                   @RequestParam(value = "children", required = false) Object data,
                                                   @RequestParam(value = "emails", required = false) Object emails,
                                                   @RequestParam(value = "emailsOther", required = false) Object emailsOther,
                                                   HttpServletRequest httpServletRequest) {

        String emailBodyFace = email_body;
        HashMap<String, Object> modelMap = new HashMap<String, Object>();
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User " + currentUser + " saved the purchase form");
        SystemUser user = userService.getUserByLogin(currentUser);

        Purchase purchase = purchaseService.getPurchaseById(Integer.parseInt(purchaseId));

        if (purchase == null) {
            modelMap.put(msgProperty, "the purchase do not exist somehow");
        } else {
            String generatedLogin = purchase.getGeneratedCode();
            if (generatedLogin == null || purchase.getStatusCode() == 3) {
                generatedLogin = publicLoginService.generateABTPublicLoginCode("A");
            }
            int id = purchaseService.savePurchase(2, purchase, user, generatedLogin, idLocation, idPackage, participant, participantOther,
                    idStaff, subject, attach_filename, email_body, data, emails, emailsOther, purchaseIdCat);
            int reload = -1;
            if (purchase.getId() != id) {
                reload = id;
            }
            modelMap.put(successProperty, true);
            modelMap.put(codeProperty, generatedLogin);
            modelMap.put("reload", reload);
        }

        return modelMap;
    }

    @RequestMapping(value = "/main/savedPurchases")
    public String getHelpYourSelfPage(ModelMap model, HttpServletRequest httpServletRequest) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        SystemUser user = userService.getUserByLogin(currentUser);
        model.put("current", currentUser);
        model.put("userId", pictureService.getUserAccess(currentUser));
        logger.info("User [ " + currentUser + " ]" + " opened the request page");
        String location = "0";
        if (!Integer.valueOf(user.getAccess()).equals(AccessID.ADMIN.getValue())) {
            location = String.valueOf(user.getLocation().getId());
        }
        model.put("location", location);
        Location location1 = userService.getUserLocation(currentUser);
        boolean tagged = (location1 != null && location1.getTagged() != null && location1.getTagged() == true) ? true : false;
        model.put("tagApp", tagged);
        return "savedpurchase";
    }

    @RequestMapping(value = "/main/purchaseHistory")
    public String getHistoreyPurchases(ModelMap model, HttpServletRequest httpServletRequest) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        SystemUser user = userService.getUserByLogin(currentUser);
        model.put("current", currentUser);
        model.put("userId", pictureService.getUserAccess(currentUser));
        model.put("startDate", DefaultValues.getHistoryStartDate());
        model.put("endDate", (new Date()).getTime());
        model.put("startDay", "");
        model.put("endDay", "");
        model.put("startTime", "");
        model.put("endTime", "");
        String location = "0";
        if (!Integer.valueOf(user.getAccess()).equals(AccessID.ADMIN.getValue())) {
            location = String.valueOf(user.getLocation().getId());
        }
        model.put("location", location);
        logger.info("User [ " + currentUser + " ]" + " opened the request page");
        Location location1 = userService.getUserLocation(currentUser);
        boolean tagged = (location1 != null && location1.getTagged() != null && location1.getTagged() == true) ? true : false;
        model.put("tagApp", tagged);
        return "historypurchase";
    }

    @RequestMapping(value = "/main/refundedPurchases")
    public String getRefundedPurchases(ModelMap model, HttpServletRequest httpServletRequest) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        SystemUser user = userService.getUserByLogin(currentUser);
        model.put("current", currentUser);
        model.put("userId", pictureService.getUserAccess(currentUser));
        model.put("startDate", DefaultValues.getHistoryStartDate());
        model.put("endDate", (new Date()).getTime());
        model.put("startDay", "");
        model.put("endDay", "");
        model.put("startTime", "");
        model.put("endTime", "");
        String location = "0";
        if (!Integer.valueOf(user.getAccess()).equals(AccessID.ADMIN.getValue())) {
            location = String.valueOf(user.getLocation().getId());
        }
        model.put("location", location);
        logger.info("User [ " + currentUser + " ]" + " opened the request page");
        Location location1 = userService.getUserLocation(currentUser);
        boolean tagged = (location1 != null && location1.getTagged() != null && location1.getTagged() == true) ? true : false;
        model.put("tagApp", tagged);
        return "refundedpurchase";
    }

    @RequestMapping(value = "getSavedPurchase.json")
    public
    @ResponseBody
    Map<String, ? extends Object> loadPurchases(HttpServletRequest httpServletRequest) {
        HashMap modelMap = new HashMap();
        Integer locationId = NumberUtils.getInteger(httpServletRequest.getParameter("locationId"));
        if(locationId == null){
            locationId = 0;
        }

        modelMap.put("savedPurchases", purchaseService.getSavedPurchasesByLocation(locationId));
        return modelMap;
    }

    @RequestMapping(value = "getHistoryPurchase.json")
    public
    @ResponseBody
    Map<String, ? extends Object> loadHistoryPurchases(final HttpServletRequest httpServletRequest) {
        HashMap modelMap = new HashMap();

        Filter filter = new Filter(
                httpServletRequest.getParameter("startdate"),
                httpServletRequest.getParameter("starttime"),
                httpServletRequest.getParameter("enddate"),
                httpServletRequest.getParameter("endtime"),
                httpServletRequest.getParameter("locationId"),
                null, null);

        String emails = httpServletRequest.getParameter("emails");
        emails = (emails == null || emails.equals("") || emails.equals("All Emails")) ? "" : emails;

        String codes = httpServletRequest.getParameter("codes");
        codes = (codes == null || codes.equals("") || codes.equals("All Codes")) ? "" : codes;

        String tags = httpServletRequest.getParameter ("tags");
        if (StringUtils.isEmpty (tags) || "All Tags".equals (tags)) {
            tags = "";
        }

        httpServletRequest.getSession().setAttribute("purchaseHistoryFilter", JsonMapper.getJson(new HashMap<String, String>() {{
            put("startdate", httpServletRequest.getParameter("startdate"));
            put("starttime", httpServletRequest.getParameter("starttime"));
            put("enddate", httpServletRequest.getParameter("enddate"));
            put("endtime", httpServletRequest.getParameter("endtime"));
            put("locationId", httpServletRequest.getParameter("locationId"));
            put("emails", httpServletRequest.getParameter("emails"));
            put("codes", httpServletRequest.getParameter("codes"));
            put("tags", httpServletRequest.getParameter("tags"));
        }}));

        List<TagSavedPurchaseBean> historyPurchases = purchaseService.getHistoryPurchasesByParameters(filter, emails, codes, tags);
        List<TagSavedPurchaseBean> tagPurchases = tagPurchaseService.getHistoryTagPurchasesByParameters(filter, emails, codes, tags);

        historyPurchases.addAll(tagPurchases);
        modelMap.put("savedPurchases", historyPurchases);

        return modelMap;
    }

    @RequestMapping(value = "getRefundedPurchase.json")
    public
    @ResponseBody
    Map<String, ? extends Object> loadRefundedPurchases(HttpServletRequest httpServletRequest) {
        HashMap modelMap = new HashMap();

        Filter filter = new Filter(
                httpServletRequest.getParameter("startdate"),
                httpServletRequest.getParameter("starttime"),
                httpServletRequest.getParameter("enddate"),
                httpServletRequest.getParameter("endtime"),
                httpServletRequest.getParameter("locationId"),
                null, null);

        String emails = httpServletRequest.getParameter("emails");
        emails = (emails == null || emails.equals("") || emails.equals("All Emails")) ? "" : emails;

        String codes = httpServletRequest.getParameter("codes");
        codes = (codes == null || codes.equals("") || codes.equals("All Codes")) ? "" : codes;

        modelMap.put("savedPurchases", tagPurchaseService.getRefundedTagPurchasesByParameters(filter, emails, codes));
        return modelMap;
    }

    @RequestMapping(value = "deleteTagPurchase.json", method = RequestMethod.POST)
    public
    @ResponseBody
    Map<String, ? extends Object> deleteRefundedPurchase(HttpServletRequest httpServletRequest) {
        HashMap modelMap = new HashMap();
        int purchaseId = Integer.valueOf(httpServletRequest.getParameter("purchaseId") != null ? httpServletRequest.getParameter("purchaseId") : "-1");
        boolean result = tagPurchaseService.removeTagPurchase(purchaseId);
        modelMap.put(successProperty, result);
        return modelMap;
    }

}
