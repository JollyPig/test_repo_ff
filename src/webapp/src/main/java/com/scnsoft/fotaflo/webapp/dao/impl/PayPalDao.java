package com.scnsoft.fotaflo.webapp.dao.impl;

import com.scnsoft.fotaflo.webapp.dao.IPayPalDao;
import com.scnsoft.fotaflo.webapp.model.Payment;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 16.02.2012
 * Time: 11:11:32
 * To change this template use File | Settings | File Templates.
 */
public class PayPalDao extends BaseDao implements IPayPalDao {

    public List<Payment> getPaymentByTransaction(String token) {
        List<Payment> payments = getHibernateTemplate().find(
                "from Payment payment where payment.token = ?", token);

        return payments;
    }
}