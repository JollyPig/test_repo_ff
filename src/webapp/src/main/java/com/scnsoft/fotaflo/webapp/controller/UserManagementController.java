package com.scnsoft.fotaflo.webapp.controller;

import com.scnsoft.fotaflo.common.util.JsonMapper;
import com.scnsoft.fotaflo.common.util.StringUtils;
import com.scnsoft.fotaflo.webapp.bean.UserBean;
import com.scnsoft.fotaflo.webapp.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */
@Controller
public class UserManagementController {
    protected static Logger logger = Logger.getLogger(StaffController.class);

    private final static String successProperty = "success";

    @Autowired
    private UserService userService;

    @RequestMapping(value = "getUsers.json")
    public
    @ResponseBody
    Map<String, ? extends Object> loadUsers(@RequestParam(value = "start", required = false) Integer start,
                                            @RequestParam(value = "limit", required = false) Integer limit,
                                            @RequestParam(value = "searchName", required = false) String username) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();

        if(start == null){
            start = 0;
        }
        if(limit == null){
            limit = 10;
        }

        HashMap modelMap = new HashMap();
        if (StringUtils.isEmpty(username)) {
            modelMap.put("users", userService.getUsersAll(start, limit, currentUser));
            modelMap.put("total", userService.getUserCount());
        } else {
            modelMap.put("users", userService.getUsersByName(username, start, limit));
            modelMap.put("total", userService.getUserCountByName(username));
        }

        return modelMap;
    }

    @RequestMapping(value = "getUsersUpdate.json")
    public
    @ResponseBody
    Map<String, ? extends Object> updateUser(@RequestParam(value = "users", required = false) Object data) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        UserBean bean = JsonMapper.fromJson(data, UserBean.class);
        userService.updateUserBean(bean, currentUser);
        HashMap modelMap = new HashMap();
        modelMap.put(successProperty, true);
        logger.info("User [ " + currentUser + " ]" + " updated user");
        return modelMap;
    }

    @RequestMapping(value = "getUsersCreate.json")
    public
    @ResponseBody
    Map<String, ? extends Object> createUser(@RequestParam(value = "users", required = false) Object data) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        UserBean bean = JsonMapper.fromJson(data, UserBean.class);
        userService.createUserBean(bean, currentUser);
        HashMap modelMap = new HashMap();
        modelMap.put(successProperty, true);
        logger.info("User [ " + currentUser + " ]" + " created user");
        return modelMap;
    }

    @RequestMapping(value = "checkLogin.json")
    public
    @ResponseBody
    Map<String, ? extends Object> checkLogin(@RequestParam(value = "login") String username){
        HashMap modelMap = new HashMap();
        if (username.contains(";") || username.contains(":") || username.contains("--") || username.contains("/") || username.contains("*") || username.contains("xp_")) {
            logger.error("Login with the name " + username + "can not be created because of the special characters");
            modelMap.put("loginCharacter", false);
        } else {
            modelMap.put("login", userService.checkLogin(username));
        }
        return modelMap;
    }

    @RequestMapping(value = "getUsersDelete.json")
    public
    @ResponseBody
    Map<String, ? extends Object> getUsersDelete(@RequestParam(value = "users", required = false) Object data) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        userService.deleteUserBean(data, currentUser);
        HashMap modelMap = new HashMap();
        modelMap.put(successProperty, true);
        logger.info("User [ " + currentUser + " ]" + " deleted user");
        return modelMap;
    }

}
