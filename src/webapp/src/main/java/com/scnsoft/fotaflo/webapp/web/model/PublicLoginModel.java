package com.scnsoft.fotaflo.webapp.web.model;

/**
 *
 * Model for save information about public login for system user.
 *
 * @author Anatoly Selitsky
 */
public class PublicLoginModel {

    private String login;
    private String password;
    private String expirationdate;
    private String startDate;
    private String endDate;
    private String generatedLogin;

    public PublicLoginModel() {
        // do nothing
    }

    public PublicLoginModel(String login, String password, String expirationdate, String startDate, String endDate) {
        this.login = login;
        this.password = password;
        this.expirationdate = expirationdate;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getExpirationdate() {
        return expirationdate;
    }

    public void setExpirationdate(String expirationDate) {
        this.expirationdate = expirationDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getGeneratedLogin() {
        return generatedLogin;
    }

    public void setGeneratedLogin(String generatedLogin) {
        this.generatedLogin = generatedLogin;
    }

}
