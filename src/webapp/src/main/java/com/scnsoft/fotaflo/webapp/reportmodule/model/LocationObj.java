package com.scnsoft.fotaflo.webapp.reportmodule.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 20.08.2012
 * Time: 9:25:09
 * To change this template use File | Settings | File Templates.
 */
public class LocationObj {

    String name;
    int total;
    int successfull;
    int unsucessfull;
    int withTImeDifference;
    String errorTypes;
    int alreadyExist;
    int rejected;
    int minTimeUpload;
    int maxTimeUpload;
    List<Integer> totals;
    int maxBeforeNumber;
    List<PictureObj> problematicPictures;
    Map<String, CameraObj> cameras = new HashMap<String, CameraObj>();
    int countUnsuccessfull;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int isSuccessfull() {
        return successfull;
    }

    public void setSuccessfull(int successfull) {
        this.successfull = successfull;
    }

    public int isUnsucessfull() {
        return unsucessfull;
    }

    public void setUnsucessfull(int unsucessfull) {
        this.unsucessfull = unsucessfull;
    }

    public int getWithTImeDifference() {
        return withTImeDifference;
    }

    public void setWithTImeDifference(int withTImeDifference) {
        this.withTImeDifference = withTImeDifference;
    }

    public String getErrorTypes() {
        return errorTypes;
    }

    public void setErrorTypes(String errorTypes) {
        this.errorTypes = errorTypes;
    }

    public int getAlreadyExist() {
        return alreadyExist;
    }

    public void setAlreadyExist(int alreadyExist) {
        this.alreadyExist = alreadyExist;
    }

    public int getRejected() {
        return rejected;
    }

    public void setRejected(int rejected) {
        this.rejected = rejected;
    }

    public int getMinTimeUpload() {
        return minTimeUpload;
    }

    public void setMinTimeUpload(int minTimeUpload) {
        this.minTimeUpload = minTimeUpload;
    }

    public int getMaxTimeUpload() {
        return maxTimeUpload;
    }

    public void setMaxTimeUpload(int maxTimeUpload) {
        this.maxTimeUpload = maxTimeUpload;
    }

    public int getMaxBeforeNumber() {
        return maxBeforeNumber;
    }

    public void setMaxBeforeNumber(int maxBeforeNumber) {
        this.maxBeforeNumber = maxBeforeNumber;
    }

    public List<PictureObj> getProblematicPictures() {
        return problematicPictures;
    }

    public void setProblematicPictures(List<PictureObj> problematicPictures) {
        this.problematicPictures = problematicPictures;
    }

    public Map<String, CameraObj> getCameras() {
        return cameras;
    }

    public void setCameras(Map<String, CameraObj> cameras) {
        this.cameras = cameras;
    }

    public int getCountUnsuccessfull() {
        return countUnsuccessfull;
    }

    public void setCountUnsuccessfull(int countUnsuccessfull) {
        this.countUnsuccessfull = countUnsuccessfull;
    }

    public List<Integer> getTotals() {
        return totals;
    }

    public void setTotals(List<Integer> totals) {
        this.totals = totals;
    }
}