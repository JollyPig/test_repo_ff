package com.scnsoft.fotaflo.webapp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.scnsoft.fotaflo.common.model.AbstractEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 03.10.2012
 * Time: 3:43:24
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "tag")
public class Tag extends AbstractEntity {

    Date creationDate;
    String tagName;

    Location location;

    Set<Picture> tagedpictures = new HashSet<Picture>();

    Set<TagPurchase> tagPurchases = new HashSet<TagPurchase>();

    Set<UserSettings> tagUsers = new HashSet<UserSettings>();

    public Tag() {
    }

    public Tag(Date creationDate, String tagName, Location location) {
        this.creationDate = creationDate;
        this.tagName = tagName;
        this.location = location;
        this.tagedpictures = new HashSet<Picture>();
        this.tagUsers = new HashSet<UserSettings>();
        this.tagPurchases = new HashSet<TagPurchase>();
    }

    @Column(name = "creation_date")
    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Column(name = "tag_name")
    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "location_id")
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @ManyToMany(mappedBy = "tags")
    @JoinTable(name = "picture_tags", joinColumns = {@JoinColumn(name = "tag_id")}, inverseJoinColumns = {@JoinColumn(name = "picture_id")})
    public Set<Picture> getTagedpictures() {
        return tagedpictures;
    }

    public void setTagedpictures(Set<Picture> tagedpictures) {
        this.tagedpictures = tagedpictures;
    }

    @ManyToMany(mappedBy = "tags")
    @JoinTable(name = "purchase_tags", joinColumns = {@JoinColumn(name = "tag_id")}, inverseJoinColumns = {@JoinColumn(name = "purchase_tag_id")})
    public Set<TagPurchase> getTagPurchases() {
        return tagPurchases;
    }

    public void setTagPurchases(Set<TagPurchase> tagPurchases) {
        this.tagPurchases = tagPurchases;
    }

    @ManyToMany(mappedBy = "tags")
    @JoinTable(name = "user_tags", joinColumns = {@JoinColumn(name = "tag_id")}, inverseJoinColumns = {@JoinColumn(name = "user_tag_id")})
    public Set<UserSettings> getTagUsers() {
        return tagUsers;
    }

    public void setTagUsers(Set<UserSettings> tagUsers) {
        this.tagUsers = tagUsers;
    }
}
