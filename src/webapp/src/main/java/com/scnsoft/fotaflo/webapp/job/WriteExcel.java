package com.scnsoft.fotaflo.webapp.job;

import com.scnsoft.fotaflo.webapp.bean.StatisticBean;
import com.scnsoft.fotaflo.webapp.bean.StatisticStaffBean;
import com.scnsoft.fotaflo.webapp.model.Package;
import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.UnderlineStyle;
import jxl.write.*;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.SortedMap;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */

public class WriteExcel {

    private WritableCellFormat timesBoldUnderline;
    private WritableCellFormat times;
    private WritableCellFormat timesBold;
    private String inputFile;
    private SortedMap reportHash;
    private List reportGeneralList;


    public WriteExcel(SortedMap reportHash, List reportGeneralList) {
        this.reportHash = reportHash;
        this.reportGeneralList = reportGeneralList;
    }

    public WriteExcel() {

    }

    public void setOutputFile(String inputFile) {
        this.inputFile = inputFile;
    }

    public WritableWorkbook openWorkBook() throws IOException, WriteException {
        File file = new File(inputFile);
        WorkbookSettings wbSettings = new WorkbookSettings();

        wbSettings.setLocale(new Locale("en", "EN"));


        WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
        return workbook;
    }

    public void writeReport(int sheetNumber, String locationName, WritableWorkbook workbook, int colsNumber) throws IOException, WriteException {
        workbook.createSheet(locationName, sheetNumber);
        WritableSheet excelSheet = workbook.getSheet(sheetNumber);
        createLabelReport(excelSheet, colsNumber);
    }

    public void closeWorkBook(WritableWorkbook workbook) throws IOException, WriteException {
        workbook.write();
        workbook.close();
    }


    private void createLabelReport(WritableSheet sheet, int colsNumber)
            throws WriteException {
        // Lets create a times font
        WritableFont times11pt = new WritableFont(WritableFont.TIMES, 11);
        // Define the cell format
        times = new WritableCellFormat(times11pt);
        // Lets automatically wrap the cells
        times.setWrap(true);

        // Create create a bold font with unterlines
        WritableFont times11ptBoldUnderline = new WritableFont(
                WritableFont.TIMES, 11, WritableFont.BOLD, false,
                UnderlineStyle.SINGLE);
        timesBoldUnderline = new WritableCellFormat(times11ptBoldUnderline);
        // Lets automatically wrap the cells
        timesBoldUnderline.setWrap(true);

        // Create create a bold font with unterlines
        WritableFont times11ptBold = new WritableFont(
                WritableFont.TIMES, 11, WritableFont.BOLD, false);
        timesBold = new WritableCellFormat(times11ptBold);
        // Lets automatically wrap the cells
        timesBold.setWrap(true);


        CellView cv = new CellView();
        cv.setFormat(times);
        cv.setFormat(timesBoldUnderline);
        cv.setFormat(timesBold);


        int count = 1;
        String title = "";
        String[] days = new String[7];
        for (Iterator nE = reportHash.keySet().iterator(); nE.hasNext(); ) {
            String key = (String) nE.next();
            if (count == 1) {
                title = key;
            }
            if (count == colsNumber) {
                title = title + " - " + key;
            }
            days[count - 1] = key;
            count++;
        }
        int merge = colsNumber * 2;
        sheet.mergeCells(0, 0, merge - 1, 0);
        Label label = new Label(0, 0, title, timesBold);
        sheet.addCell(label);
        int y = 0;
        for (int i = 0; i < merge; i = i + 2) {
            sheet.mergeCells(i, 1, i + 1, 1);
            sheet.addCell(new Label(i, 1, days[y], times));
            y++;
        }
        int dayNumber = 0;
        for (Iterator nE = reportHash.keySet().iterator(); nE.hasNext(); ) {
            String key = (String) nE.next();
            List statisticBeanlist = (List) reportHash.get(key);
            int packNumber = 0;
            for (Iterator stListitem = statisticBeanlist.iterator(); stListitem.hasNext(); ) {
                StatisticBean statisticBean = (StatisticBean) stListitem.next();
                int staffCount = statisticBean.getStaff().size();
                if (dayNumber == 0) {
                    sheet.mergeCells(0, packNumber * (staffCount + 5) + 2, merge - 1, packNumber * (staffCount + 5) + 2);
                    sheet.addCell(new Label(0, packNumber * (staffCount + 5) + 2, statisticBean.getPackages().getPackageName(), timesBold));
                }
                int col = dayNumber;
                int row = packNumber * (staffCount + 5) + 3;
                sheet.setColumnView(col, merge + 1);
                sheet.setColumnView(col + 1, merge + 1);
                sheet.addCell(new Label(col, row, statisticBean.getPackages().getPackageName(), times));
                //  sheet.addCell(new Label(col + 1, row, String.valueOf(statisticBean.getTotalpackages()), times));
                sheet.addCell(new Label(col + 1, row, String.valueOf(statisticBean.getTotalPictures()), times));
                row++;
                sheet.addCell(new Label(col, row, "# of Participants", times));
                sheet.addCell(new Label(col + 1, row, String.valueOf(statisticBean.getParticipantNumber()), times));
                row++;
                if (statisticBean.getPackages().getAccess() == Package.PackageAccess.DEFAULT.getId()) {
                    sheet.addCell(new Label(col, row, "# of Prints", times));
                    sheet.addCell(new Label(col + 1, row, String.valueOf(statisticBean.getPrintNumber()), times));
                } else {
                    sheet.addCell(new Label(col, row, "# of Pictures", times));
                    sheet.addCell(new Label(col + 1, row, String.valueOf(statisticBean.getTotalPictures()), times));
                }
                row++;
                sheet.addCell(new Label(col, row, "Staff", times));

                for (Iterator staffIter = statisticBean.getStaff().iterator(); staffIter.hasNext(); ) {
                    StatisticStaffBean statisticStaffBean = (StatisticStaffBean) staffIter.next();
                    row++;
                    sheet.addCell(new Label(col, row, statisticStaffBean.getStaff().getStaffName(), times));
                    sheet.addCell(new Label(col + 1, row, String.valueOf(statisticStaffBean.getStaffnumber()), times));
                }
                packNumber++;
            }
            dayNumber = dayNumber + 2;
        }
        // int col = 14;
        int col = colsNumber * 2;
        int row = 1;
        sheet.setColumnView(col, 25);
        sheet.addCell(new Label(col, row, "Totals", timesBold));
        int packNumber = 0;
        //dayNumber = 14;
        dayNumber = colsNumber * 2;
        for (Iterator stListitem = reportGeneralList.iterator(); stListitem.hasNext(); ) {
            StatisticBean statisticBean = (StatisticBean) stListitem.next();
            int staffCount = statisticBean.getStaff().size();
            sheet.addCell(new Label(col, packNumber * (staffCount + 5) + 2, statisticBean.getPackages().getPackageName() + " Total", timesBold));
            col = dayNumber;
            row = packNumber * (staffCount + 5) + 3;
            // sheet.addCell(new Label(col, row, String.valueOf(statisticBean.getTotalpackages()), times));
            sheet.addCell(new Label(col, row, String.valueOf(statisticBean.getTotalPictures()), times));
            row++;
            sheet.addCell(new Label(col, row, String.valueOf(statisticBean.getParticipantNumber()), times));
            row++;
            if (statisticBean.getPackages().getAccess() == Package.PackageAccess.DEFAULT.getId()) {
                sheet.addCell(new Label(col, row, String.valueOf(statisticBean.getPrintNumber()), times));
            } else {
                sheet.addCell(new Label(col, row, String.valueOf(statisticBean.getTotalPictures()), times));
            }

            row++;

            for (Iterator staffIter = statisticBean.getStaff().iterator(); staffIter.hasNext(); ) {
                StatisticStaffBean statisticStaffBean = (StatisticStaffBean) staffIter.next();
                row++;
                sheet.addCell(new Label(col, row, String.valueOf(statisticStaffBean.getStaffnumber()), times));
            }
            packNumber++;
        }


        // Write a few headers

    }

    public SortedMap getReportHash() {
        return reportHash;
    }

    public void setReportHash(SortedMap reportHash) {
        this.reportHash = reportHash;
    }

    public List getReportGeneralList() {
        return reportGeneralList;
    }

    public void setReportGeneralList(List reportGeneralList) {
        this.reportGeneralList = reportGeneralList;
    }
}
