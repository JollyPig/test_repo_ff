package com.scnsoft.fotaflo.webapp.model;

import com.scnsoft.fotaflo.common.model.AbstractEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name = "systemuser")
public class SystemUser extends AbstractEntity {

    public static final String ADMIN_ACCESS = "1";

    String loginName;
    String password;
    Date creationDate;
    Date expirationDate;
    String firstName;
    String lastName;
    String email;
    String access;
    Location location;
    String stringedFileName;
    String emailBody;
    String staffs;
    UserSettings userSettings;
    Set<Picture> userPictures;
    private Set<PurchaseSelected> selectedPictures = new HashSet<PurchaseSelected>(0);
    private Set<Requests> requests = new HashSet<Requests>(0);

    /**
     * @return Returns the loginName.
     */
    @Column(name = "login_name", length = 64)
    public String getLoginName() {
        return loginName;
    }

    /**
     * @param loginName The loginName to set.
     */

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    /**
     * @return Returns the password.
     */
    @Column(name = "password", length = 64)
    public String getPassword() {
        return password;
    }

    /**
     * @param password The password to set.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return Returns the firstname.
     */

    @Column(name = "firstname", length = 64)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return Returns the lastname.
     */
    @Column(name = "lastname", length = 64)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return Returns the email.
     */
    @Column(name = "email", length = 64)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "creation_date")
    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Column(name = "expiration_date")
    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    /**
     * @return Returns the access.
     */
    @Column(name = "access", length = 10)
    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }


    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "location_id")
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }


    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "usersettings_id")
    public UserSettings getUserSettings() {
        return userSettings;
    }

    public void setUserSettings(UserSettings userSettings) {
        this.userSettings = userSettings;
    }


    @OneToMany(cascade = {CascadeType.REMOVE}, mappedBy = "systemUser")
    public Set<PurchaseSelected> getSelectedPictures() {
        return selectedPictures;
    }

    public void setSelectedPictures(Set<PurchaseSelected> selectedPictures) {
        this.selectedPictures = selectedPictures;
    }

    @OneToMany(mappedBy = "systemUser")
    public Set<Requests> getRequests() {
        return requests;
    }

    public void setRequests(Set<Requests> requests) {
        this.requests = requests;
    }


    @Column(name = "stringed_filename", length = 64)
    public String getStringedFileName() {
        return stringedFileName;
    }

    public void setStringedFileName(String stringedFileName) {
        this.stringedFileName = stringedFileName;
    }


    @Lob
    @Column(name = "emailBody")
    public String getEmailBody() {
        return emailBody;
    }

    public void setEmailBody(String emailBody) {
        this.emailBody = emailBody;
    }

    @Column(name = "saved_staff", length = 1024)
    public String getStaffs() {
        return staffs;
    }

    public void setStaffs(String staffs) {
        this.staffs = staffs;
    }

    @ManyToMany(mappedBy = "systemUsers")
    @JoinTable(name = "selected_pictures", joinColumns = {@JoinColumn(name = "system_user_id")}, inverseJoinColumns = {@JoinColumn(name = "picture_id")})
    public Set<Picture> getUserPictures() {
        return userPictures;
    }

    public void setUserPictures(Set<Picture> userPictures) {
        this.userPictures = userPictures;
    }
}
