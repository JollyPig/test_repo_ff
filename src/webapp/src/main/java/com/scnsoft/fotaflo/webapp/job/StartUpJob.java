package com.scnsoft.fotaflo.webapp.job;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 21.06.13
 * Time: 16:46
 * To change this template use File | Settings | File Templates.
 */
/*
File : StartUpJob.java
*/

import com.scnsoft.fotaflo.webapp.startupService.StartUpService;
import com.scnsoft.fotaflo.webapp.util.ApplicationContextProvider;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;


public class StartUpJob extends QuartzJobBean {

    protected void executeInternal(JobExecutionContext ctx) throws JobExecutionException {

        ApplicationContext context = ApplicationContextProvider.getApplicationContext();
        StartUpService startUpService = (StartUpService) context.getBean("smtpStartUpService");
        startUpService.sendStartUpNotification();

    }
}