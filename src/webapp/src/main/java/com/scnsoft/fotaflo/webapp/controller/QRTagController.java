package com.scnsoft.fotaflo.webapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;

@Controller
@RequestMapping("/qrtag")
public class QRTagController {

    private static int SECURITY_CODE_LENGTH = 4;

    @RequestMapping("generate.json")
    @ResponseBody
    public ModelMap generateQRTag(@RequestParam("date") Long date) {
        //We got date in form of seconds from 1970, converting to milliseconds
        Date clientDate = new Date(date * 1000l);
        ModelMap map = new ModelMap();
        map.put("success", "true");
        map.put("qrtag", createQRTag(clientDate));
        return map;
    }

    private String createQRTag(Date date) {
        Integer securityCode = new Double(Math.random() * Math.pow(10, SECURITY_CODE_LENGTH)).intValue();

        return String.format("%1$ty-%1$tm-%1$td_%1$tT_%2$0" + SECURITY_CODE_LENGTH + "d", date, securityCode);
    }
}
