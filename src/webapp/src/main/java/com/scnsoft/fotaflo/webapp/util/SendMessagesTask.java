package com.scnsoft.fotaflo.webapp.util;

import com.scnsoft.fotaflo.common.util.StringUtils;
import com.scnsoft.fotaflo.webapp.bean.NotificationMessageBean;
import com.scnsoft.fotaflo.webapp.bean.SendMessagesBean;
import com.scnsoft.fotaflo.webapp.bean.WatermarkBean;
import com.scnsoft.fotaflo.webapp.service.ImageService;
import com.scnsoft.fotaflo.webapp.service.ShareService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.task.TaskExecutor;

import java.io.File;
import java.util.*;

/**
 * Created by Nadezda Drozdova
 * Date: May 20, 2011
 * Time: 11:06:08 AM
 */
public class SendMessagesTask {
    protected static Logger logger = Logger.getLogger("MailSender");

    @Autowired
    private ImageService imageService;

    @Value("${fotaflo.email.bcc}")
    private String bccEmail;

    @Autowired
    private ShareService shareService;

    private class MessageSendTask implements Runnable {

        private SendMessagesBean sendMessagesBean;

        private MailSenderUtil ms;

        public MessageSendTask(SendMessagesBean sendMessagesBean, MailSenderUtil ms) {
            this.sendMessagesBean = sendMessagesBean;
            this.ms = ms;
        }

        public void run() {
            List<String> toSendEmails = sendMessagesBean.getTo();
            String fromEmail = sendMessagesBean.getFrom();
            String subject = sendMessagesBean.getSubject();
            String email_body = sendMessagesBean.getMsgText1();

            Map<String, WatermarkBean> attached = sendMessagesBean.getAttachments();

            Map<String, File> attachment = new HashMap<String, File>();
            List<Resource> facebookPhotos = new ArrayList<Resource>();
            if (attached != null) {
                for (String nameAttach : attached.keySet()) {
                    WatermarkBean attach = attached.get(nameAttach);
                    logger.info("Creating file to send: " + attach.getOutput());
                    try {
                        String imgCreated = imageService.createPurchaseImage(attach);
                        File fileToSend = new File(imgCreated);

                        attachment.put(nameAttach, fileToSend);
                        facebookPhotos.add(new FileSystemResource(fileToSend));
                    } catch (Exception e) {
                        logger.error(e.getMessage());
                    }

                }
            }
            String smtpServer = sendMessagesBean.getMailHostServer();
            String port = sendMessagesBean.getPort();
            String password = sendMessagesBean.getPassword();
            String bccEmail = sendMessagesBean.getBccEmail();
            boolean ssl = "on".equals(sendMessagesBean.getSsl());
            logger.info("Sending message");
            logger.info("From email:" + fromEmail);

            if (!StringUtils.isEmpty(sendMessagesBean.getFacebookAccessToken())
                    && !StringUtils.isEmpty(sendMessagesBean.getFacebookPage())) {
                sendPictureToFaceBook(sendMessagesBean.getFacebookAccessToken(), facebookPhotos, subject, sendMessagesBean.getFacebookPage());
            } else {
                if (toSendEmails != null) {
                    ms.sendMailMessage(toSendEmails, fromEmail, subject, email_body, attachment, smtpServer, port, password, ssl, bccEmail);
                }
            }
        }
    }

    private void sendPictureToFaceBook(String accessToken, List<Resource> images, String subject, String page) {
        shareService.shareImagesViaFacebook(accessToken, images, subject, page);
    }

    private class BCCSendTask implements Runnable {
        List<String> toSendEmails;

        String fromEmail;

        String subject;

        String email_body;

        String smtpServer;

        String port;

        String password;

        String ssl;

        private MailSenderUtil ms;

        private BCCSendTask(MailSenderUtil ms, List<String> toSendEmails, String fromEmail, String subject, String email_body,
                            String smtpServer, String port, String password, String ssl) {
            this.ms = ms;
            this.toSendEmails = toSendEmails;
            this.fromEmail = fromEmail;
            this.subject = subject;
            this.email_body = email_body;
            this.smtpServer = smtpServer;
            this.port = port;
            this.password = password;
            this.ssl = ssl;
        }

        public void run() {
            boolean sslBoolean = "on".equals(ssl);
            ms.sendMailMessage(toSendEmails, fromEmail, subject, email_body, null, smtpServer, port, password, sslBoolean, null);
        }

    }

    List<SendMessagesBean> messages;

    List<NotificationMessageBean> messagesN;

    String location;

    String packageName;

    String numberOfPart;

    Date datePurchase;

    Set<String> staffs;

    String fromEmail;

    String smtpServer;

    String port;

    String password;

    String ssl;

    private TaskExecutor taskExecutor;

    public SendMessagesTask(TaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

    public void sendMessages() {
        MailSenderUtil ms = new MailSenderUtil();

        if (!messages.isEmpty()) {
            for (SendMessagesBean sendMessagesBean : messages) {

                if (sendMessagesBean.getAttachments() == null) {
                    taskExecutor.execute(new MessageSendTask(sendMessagesBean, ms));
                } else {
                    int attachmentCount = sendMessagesBean.getAttachments().size();
                    if (attachmentCount > sendMessagesBean.getMaxAttachments()) {
                        List<SendMessagesBean> sendMessages = new ArrayList<SendMessagesBean>();
                        int attachCounter = 0;
                        Map<String, WatermarkBean> attach = sendMessagesBean.getAttachments();
                        Map<String, WatermarkBean> attachments = new HashMap<String, WatermarkBean>();
                        int globalCounter = 0;
                        for (String key : attach.keySet()) {
                            if (attachCounter < sendMessagesBean.getMaxAttachments()) {
                                attachments.put(key, attach.get(key));
                            } else if (attachCounter == sendMessagesBean.getMaxAttachments()) {
                                SendMessagesBean sendBean =
                                        new SendMessagesBean(sendMessagesBean.getTo(), sendMessagesBean.getFrom(), sendMessagesBean.getSubject(),
                                                sendMessagesBean.getMailHostServer(), sendMessagesBean.getPort(),
                                                sendMessagesBean.getPassword(), sendMessagesBean.getMsgText1(), attachments,
                                                sendMessagesBean.getSsl(), sendMessagesBean.getBccEmail(), sendMessagesBean.getFacebookAccessToken(), sendMessagesBean.getFacebookPage());
                                sendMessages.add(sendBean);
                                attachments = new HashMap<String, WatermarkBean>();
                                attachCounter = 0;
                                attachments.put(key, attach.get(key));
                            }
                            if (globalCounter == attach.size() - 1) {
                                SendMessagesBean sendBean =
                                        new SendMessagesBean(sendMessagesBean.getTo(), sendMessagesBean.getFrom(), sendMessagesBean.getSubject(),
                                                sendMessagesBean.getMailHostServer(), sendMessagesBean.getPort(),
                                                sendMessagesBean.getPassword(), sendMessagesBean.getMsgText1(), attachments,
                                                sendMessagesBean.getSsl(), sendMessagesBean.getBccEmail(), sendMessagesBean.getFacebookAccessToken(), sendMessagesBean.getFacebookPage());
                                sendMessages.add(sendBean);
                                attachCounter = 0;
                                attachments = new HashMap<String, WatermarkBean>();
                            }
                            attachCounter++;
                            globalCounter++;
                        }
                        int counter = 0;
                        for (SendMessagesBean sended : sendMessages) {
                            taskExecutor.execute(new MessageSendTask(sended, ms));
                            if (counter % 10 == 0) {
                                try {
                                    Thread.sleep(5000);
                                } catch (InterruptedException e) {
                                    logger.error(e);
                                }
                            }
                            counter++;
                        }

                    } else {
                        taskExecutor.execute(new MessageSendTask(sendMessagesBean, ms));
                    }
                }
            }
        }

    }

    public void sendNotification() {
        MailSenderUtil ms = new MailSenderUtil();
        StringBuffer bccBody = new StringBuffer();
        String bccSubject = "Email Notification from Fotaflo";
        List<String> toSendEmails = readEmailTo();
        bccBody.append("Date:\t").append(datePurchase.toString()).append("\n").
                append("Location:\t").append(location).append("\n").
                append("Package:\t").append(packageName).append("\n").
                append("# of participants:\t").append(numberOfPart).append("\n").
                append("Staff:\t").append(staffs.isEmpty() ? "" : staffs).append("\n\n");

        if (!messagesN.isEmpty()) {
            for (NotificationMessageBean notifBean : messagesN) {
                bccBody.append("Name of the file:\t").append(notifBean.getFilename()).append("\n").
                        append("E-mails: \t").append(
                        notifBean.getTo().isEmpty() ? "picture has not been selected for sending" : notifBean.getTo()).append("\n").
                        append("Number of copies to Print:\t").append(notifBean.getNumberOfCopiesToPrint()).append("\n\n");
            }
        }
        if (!messagesN.isEmpty()) {
            taskExecutor.execute(
                    new BCCSendTask(ms, toSendEmails, fromEmail, bccSubject, bccBody.toString(), smtpServer, port, password, ssl));
        }
    }

    private List<String> readEmailTo() {
        List<String> toSendEmails = new ArrayList<String>();
        if (bccEmail != null) {
            toSendEmails.add(bccEmail);
        }

        return toSendEmails;
    }

    public List<SendMessagesBean> getMessages() {
        return messages;
    }

    public void setMessages(List<SendMessagesBean> messages) {
        this.messages = messages;
    }

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        SendMessagesTask.logger = logger;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getNumberOfPart() {
        return numberOfPart;
    }

    public void setNumberOfPart(String numberOfPart) {
        this.numberOfPart = numberOfPart;
    }

    public Date getDatePurchase() {
        return datePurchase;
    }

    public void setDatePurchase(Date datePurchase) {
        this.datePurchase = datePurchase;
    }

    public Set<String> getStaffs() {
        return staffs;
    }

    public void setStaffs(Set<String> staffs) {
        this.staffs = staffs;
    }

    public List<NotificationMessageBean> getMessagesN() {
        return messagesN;
    }

    public void setMessagesN(List<NotificationMessageBean> messagesN) {
        this.messagesN = messagesN;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getSmtpServer() {
        return smtpServer;
    }

    public void setSmtpServer(String smtpServer) {
        this.smtpServer = smtpServer;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public TaskExecutor getTaskExecutor() {
        return taskExecutor;
    }

    public void setTaskExecutor(TaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

    public String getSsl() {
        return ssl;
    }

    public void setSsl(String ssl) {
        this.ssl = ssl;
    }

    public ImageService getImageService() {
        return imageService;
    }

    public void setImageService(final ImageService imageService) {
        this.imageService = imageService;
    }
}
