package com.scnsoft.fotaflo.webapp.job;

import com.scnsoft.fotaflo.webapp.service.ReportService;
import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * Created by
 * User: Paradinets
 * Date: 20.12.2011
 */

public class MonthlyStatisticJob1 extends QuartzJobBean {

    private ReportService reportService;

    protected static Logger logger = Logger.getLogger(MonthlyStatisticJob15.class);

    protected void executeInternal(JobExecutionContext ctx) throws JobExecutionException {
        boolean result = reportService.generateAndSendMonthlyStatisticV2(ctx.getFireTime(), ReportService.ReportNumber.FIRST);
        if (!result) {
            logger.error("The error occurred during FIRST (FIRST DAY OF MONTH) monthly reports generation or sending ");
        }
    }

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }
}