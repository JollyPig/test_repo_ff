package com.scnsoft.fotaflo.webapp.service;

import com.scnsoft.fotaflo.webapp.bean.*;
import com.scnsoft.fotaflo.webapp.model.Camera;
import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.util.Resolution;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class MailService {
    protected static Logger logger = Logger.getLogger(MailService.class);

    @Value("${fotaflo.web_portal_path}")
    private String portalRedirectUrl;

    private final int MAX_THUMBNAIL_ATTACHMENTS = 16;

    private final String IMAGE_TEMPLATE = "<img src='cid:%s' width=160 height=120 style='margin: 10px;'>";
    private final String IMAGE_TEMPLATE_ROTATED = "<img src='cid:%s' width=90 height=120 style='margin: 10px;'>";

    private Format formatter = new SimpleDateFormat("yyyy.MM.dd HH");

    @Autowired
    private ReportService reportService;

    @Autowired
    private PictureService pictureService;

    @Autowired
    private CameraService cameraService;

    @Autowired
    private MessageSource messageSource;

    public SendMessagesBean createPurchaseEmail(List<String> recipients, String subject,
                                                String emailBody, String attachmentBaseName,
                                                Location location,
                                                List<PurchasePictureBean> pictures, boolean htmlEmail, boolean isBccPossible, String facebookAccessToken, String facebookPage) {
        if (htmlEmail) {
            String footer = reportService.getLocationFooter(location);
            emailBody = messageSource.getMessage("mail.template.message", new Object[]{emailBody.replace("\n", "<br>"), footer}, Locale.ROOT);
        }
        Map<String, WatermarkBean> attachments = getMailAttachments(pictures, location, attachmentBaseName);
        return createSendMessagesBean(recipients, subject, emailBody, location, attachments, isBccPossible, facebookAccessToken, facebookPage);
    }

    public SendMessagesBean createPurchaseEmail(List<String> recipients, String subject,
                                                String emailBody, String attachmentBaseName,
                                                Location location,
                                                List<PurchasePictureBean> pictures, boolean isBccPossible, String facebookAccessToken, String facebookPage) {
        return createPurchaseEmail(recipients, subject, emailBody, attachmentBaseName, location, pictures, true, isBccPossible, facebookAccessToken, facebookPage);
    }

    public SendMessagesBean createThumbnailEmail(Map<String, String> recipientData, String subject,
                                                 String emailBody, String attachmentBaseName,
                                                 Location location, List<PurchasePictureBean> pictures) {
        Map<String, WatermarkBean> attachments = getMailAttachments(pictures, location, attachmentBaseName, true);
        List<String> recipient = new ArrayList<String>();
        recipient.add(recipientData.get("email"));

        boolean isPortal = location != null && location.getFwpenable() != null && location.getFwpenable() == true;

        String messageBody;
        String footer = location.getLocationSettings().getPublicemalarea();

        if (isPortal) {
            String link = portalRedirectUrl + recipientData.get("login");
            String code = recipientData.get("login");
            messageBody = messageSource.getMessage("mail.template.link_message", new Object[]{link, code, footer}, Locale.ROOT);
        } else {
            StringBuilder content = new StringBuilder();
            for (String attachmentName : attachments.keySet()) {
                String template = attachments.get(attachmentName).getRotated() ? IMAGE_TEMPLATE_ROTATED : IMAGE_TEMPLATE;
                content.append(String.format(template, String.valueOf(attachmentName.hashCode())));
            }
            messageBody = messageSource.getMessage("mail.template.cred_message", new Object[]{content.toString(),
                    recipientData.get("login"), recipientData.get("password"), footer}, Locale.ROOT);
        }

        SendMessagesBean message = createSendMessagesBean(recipient, subject, messageBody, location, isPortal ? null : attachments, false, null, null);
        message.setMaxAttachments(MAX_THUMBNAIL_ATTACHMENTS);
        return message;
    }

    public SendMessagesBean createACodeEmail(Map<String, String> recipientData, String subject, String emailBody, Location location) {
        List<String> recipient = new ArrayList<String>();
        recipient.add(recipientData.get("email"));

        String code = recipientData.get("login");
        String link = portalRedirectUrl + code;
        String footer = reportService.getLocationFooter(location);
        String messageBody = messageSource.getMessage("mail.template.link_message", new Object[]{link, code, footer}, Locale.ROOT);

        SendMessagesBean message = createSendMessagesBean(recipient, subject, messageBody, location, null, false, null, null);

        return message;
    }

    public List<SendMessagesBean> splitThumbnailEmail(Map<String, String> recipientData, String subject,
                                                      String emailBody, String attachmentBaseName,
                                                      Location location, List<PurchasePictureBean> pictures) {
        List<SendMessagesBean> messages = new ArrayList<SendMessagesBean>();
        for (int i = 0; i <= pictures.size() - 1; i = i + MAX_THUMBNAIL_ATTACHMENTS) {
            int toIndex = (i + MAX_THUMBNAIL_ATTACHMENTS) > (pictures.size() - 1) ?
                    pictures.size() : (i + MAX_THUMBNAIL_ATTACHMENTS);
            List<PurchasePictureBean> attachmentPart = pictures.subList(i, toIndex);
            SendMessagesBean messagePart = createThumbnailEmail(recipientData, subject, emailBody,
                    attachmentBaseName, location, attachmentPart);
            messages.add(messagePart);
        }
        return messages;
    }

    public List<SendMessagesBean> thumbnailEmail(Map<String, String> recipientData, String subject,
                                                 String emailBody, String attachmentBaseName,
                                                 Location location, List<PurchasePictureBean> pictures) {
        List<SendMessagesBean> messages = new ArrayList<SendMessagesBean>();

        SendMessagesBean messagePart = createThumbnailEmail(recipientData, subject, emailBody,
                attachmentBaseName, location, pictures);
        messages.add(messagePart);

        return messages;
    }

    public NotificationMessageBean createNotificationMessage(List<String> recipients, PurchasePictureBean purchaseItem,
                                                             String baseAttachmentName, Integer numberToPrint) {
        String nameAttach = purchaseItem.getUrl();
        return new NotificationMessageBean(nameAttach, numberToPrint, recipients);
    }

    private Map<String, WatermarkBean> getMailAttachments(List<PurchasePictureBean> pictures,
                                                          Location location, String attachmentBaseName,
                                                          boolean thumbnail) {
        Map<String, WatermarkBean> attachments = new HashMap<String, WatermarkBean>();
        for (PurchasePictureBean picture : pictures) {
            WatermarkBean watermarkBean = toWatermarkBean(picture, location, thumbnail);
            String attachmentName = String.format("%s_%s_%s", picture.getId(), attachmentBaseName, picture.getCreationDate());
            attachments.put(attachmentName, watermarkBean);
        }

        return attachments;
    }

    /**
     * *********
     */
    private Map<String, WatermarkBean> getMailAttachments(List<PurchasePictureBean> pictures,
                                                          Location location, String attachmentBaseName) {
        return getMailAttachments(pictures, location, attachmentBaseName, false);
    }

    private WatermarkBean toWatermarkBean(PurchasePictureBean picture, Location location, boolean thumbnail) {
        boolean reduced = location.isReduced();
        String picturePath = pictureService.getPicturePath();
        Camera pictureCamera = cameraService.getCamera(Integer.valueOf(picture.getCameraId()));
        String imagePath = pictureService.getPath(
                File.separator + picture.getUrl(thumbnail).replace("/", File.separator));
        String locationImgLogo = (picture.getLogoUrl() == null) ? null
                : pictureService.getPath(picture.getLogoUrl().replace("/", File.separator));
        String mainLogo = picture.getLogoMainUrl();
        mainLogo = pictureService.getPath(mainLogo.replace("/", File.separator));
        String textLogo = picture.getLogoText();
        String tmpPath = getTmpImagePath(picture, thumbnail, reduced, location.getResolution());
        String water = "";
        if (thumbnail) {
            water = picturePath + "\\img\\watermark\\logo.png";
        }
        String resolution = (location == null || location.getResolution() == null || location.getResolution().equals("")) ? Resolution.STANDARD.getCode() : location.getResolution();
        WatermarkBean bean = new WatermarkBean(imagePath, locationImgLogo, mainLogo, textLogo, tmpPath, water, reduced, resolution, picture.getRotated());
        bean.setMetadata(pictureCamera.getLocation().getImageMetadata());
        return bean;
    }

    public String getTmpImagePath(PictureBean pictureBean, boolean thumbnail, boolean reduced, String resolution) {
        StringBuilder path = new StringBuilder();
        path.append(System.getProperty("java.io.tmpdir"));
        path.append(File.separator);
        path.append(formatter.format(new Date()));
        path.append(pictureBean.getName());
        path.append(pictureBean.getId());
        if (thumbnail) {
            path.append("small");
        } else {
            if (reduced) {
                path.append("red");
            } else {
                path.append("res_" + resolution);
            }
        }
        path.append(".jpg");
        return path.toString();
    }

    private SendMessagesBean createSendMessagesBean(List<String> recipients, String subject,
                                                    String emailBody, Location location,
                                                    Map<String, WatermarkBean> attachments, boolean isBccPossible, String facebookAccessToken, String facebookPage) {
        String bccEmail = null;
        if (isBccPossible && location.getBcconly() != null && location.getBcconly()) {
            bccEmail = (location.getLocationToEmail() != null && location.getLocationToEmail() != "") ? location.getLocationToEmail() : "";
        }
        return new SendMessagesBean(recipients, location.getLocationFromEmail(),
                subject, location.getLocationSmtpServer(),
                location.getLocationSmtpServerPort().toString(),
                location.getLocationSmtpServerPassword(),
                emailBody, attachments, location.getLocationSslCheck(), bccEmail, facebookAccessToken, facebookPage);
    }
}
