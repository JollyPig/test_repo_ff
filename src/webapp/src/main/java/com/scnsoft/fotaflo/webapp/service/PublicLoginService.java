package com.scnsoft.fotaflo.webapp.service;

import com.scnsoft.fotaflo.webapp.dao.IDao;
import com.scnsoft.fotaflo.webapp.dao.IFilterDao;
import com.scnsoft.fotaflo.webapp.dao.IPurchaseDao;
import com.scnsoft.fotaflo.webapp.dao.ISystemUserDao;
import com.scnsoft.fotaflo.webapp.model.*;
import com.scnsoft.fotaflo.webapp.util.AccessID;
import com.scnsoft.fotaflo.webapp.util.DateConvertationUtils;
import com.scnsoft.fotaflo.webapp.web.model.PublicLoginModel;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Paradinets
 * Date: 15.02.2012
 * Time: 17:48:58
 * To change this template use File | Settings | File Templates.
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class PublicLoginService {
    protected static Logger logger = Logger.getLogger(PublicLoginService.class);

    @Autowired
    private ISystemUserDao systemUserDao;

    @Autowired
    private IFilterDao filterDao;

    @Autowired
    private IPurchaseDao purchaseDao;

    @Autowired
    private IDao baseDao;

    @Autowired
    private UserService userService;

    @Autowired
    private PictureService pictureService;

    public String generatePublicPassword() {
        return new BigInteger(40, new SecureRandom()).toString(32);
    }

    public String generatePublicLogin() {
        return new BigInteger(20, new SecureRandom()).toString(32);
    }

    public String generatePublicCode6() {
        String seq = new BigInteger(60, new SecureRandom()).toString(32);
        return seq.substring(0, 6);
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public boolean createTagPublicLogin(String tagName, Location location, Date today, String access) {
        Date expirationDate = DateConvertationUtils.getStartDate(-100, today);

        if (access == null || (!access.equals(String.valueOf(AccessID.PUBLICTAGUSER.getValue())) && !access.equals(String.valueOf(AccessID.PUBLICTAGCODEUSER.getValue())))) {
            logger.error("user access should be indicated");
            return false;
        } else {
            SystemUser systemUser = new SystemUser();
            systemUser.setLoginName(tagName);
            Md5PasswordEncoder md5PasswordEncoder = new Md5PasswordEncoder();
            systemUser.setPassword(md5PasswordEncoder.encodePassword(tagName, null));
            systemUser.setAccess(access);
            systemUser.setLocation(location);
            systemUser.setCreationDate(today);
            systemUser.setExpirationDate(expirationDate);
            baseDao.update(systemUser);
            return true;
        }
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public boolean updateTagPublicLoginIfNotExist(String tagName, Location location, Date today, String access) {
        SystemUser tagUser = systemUserDao.getUsersByLoginAndLocation(location, tagName);
        if (tagUser == null) {
            createTagPublicLogin(tagName, location, today, access);
            return true;
        }
        return false;
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public Map<String, String> createPurchasePublicLogin(Location location, Date today, Date expirationDate, String access, String login) {
        Map<String, String> credentials = new HashMap<String, String>();

        if (login != null && !login.isEmpty()) {
            String password = login;

            logger.info("PUBLIC LOGIN: " + login);
            logger.info("PUBLIC PASSWORD: " + password);

            credentials.put("login", login);
            credentials.put("password", password);

            SystemUser systemUser = new SystemUser();
            systemUser.setLoginName(login);
            Md5PasswordEncoder md5PasswordEncoder = new Md5PasswordEncoder();
            systemUser.setPassword(md5PasswordEncoder.encodePassword(password, null));
            if (access == null || location == null || (!access.equals(String.valueOf(AccessID.PUBLICUSER.getValue())) && !access.equals(String.valueOf(AccessID.PUBLICPURCHASEUSER.getValue())) && !access.equals(String.valueOf(AccessID.PUBLICEMAILUSER.getValue())) && !access.equals(String.valueOf(AccessID.PUBLICTAGUSER.getValue())))) {
                credentials.put("success", "false");
                return credentials;
            }
            systemUser.setAccess(access);
            systemUser.setLocation(location);
            systemUser.setCreationDate(today);
            systemUser.setExpirationDate(expirationDate);
            baseDao.update(systemUser);

            UserFilter userFilter = new UserFilter();
            userFilter.setStartDate(today);
            userFilter.setEndDate(expirationDate);
            userFilter.setSystemUser(systemUser);
            userFilter.setLocationId("0");
            userFilter.setCameraId("0");
            userFilter.setTagIds("");

            baseDao.update(userFilter);

            credentials.put("success", "true");
            return credentials;
        }
        credentials.put("success", "false");
        return credentials;
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public Map<String, String> generateACodesFromPurchaseCode(String purchaseCode, int position, Location location, Date today, Date expirationDate, String access) {
        List<String> alphabet = new ArrayList<String>(Arrays.asList("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "w", "v", "x", "y", "z"));
        Map<String, String> credentials = new HashMap<String, String>();
        if (position <= 25) {
            String newCode = purchaseCode + alphabet.get(position - 1);
            credentials.put("login", newCode);
            credentials.put("password", newCode);

            credentials.put("success", createUserFromCode(newCode, purchaseCode, newCode, location, today, expirationDate, access) ? "true" : "false");

        } else {
            credentials.put("success", "false");
        }
        return credentials;
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public Map<String, String> createGeneralPublicLogin(Location location, Date today, Date expirationDate, String access, boolean email) {
        String login = "";
        if (email) {
            login = generateABTPublicLoginCode("B");
        } else {
            login = generateGeneralPublicLogin(location, today, access);
        }
        Map<String, String> credentials = new HashMap<String, String>();

        if (login != null && !login.isEmpty()) {
            String password = login;
            if (!email) {
                password = generatePublicPassword();
            }

            logger.info("PUBLIC LOGIN: " + login);
            logger.info("PUBLIC PASSWORD: " + password);

            credentials.put("login", login);
            credentials.put("password", password);

            credentials.put("success", createUserFromCode(login, null, password, location, today, expirationDate, access) ? "true" : "false");
            return credentials;
        }
        credentials.put("success", "false");
        return credentials;
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public boolean createUserFromCode(String login, String parentLogin, String password, Location location, Date today, Date expirationDate, String access) {
        SystemUser systemUser = new SystemUser();
        systemUser.setLoginName(login);
        Md5PasswordEncoder md5PasswordEncoder = new Md5PasswordEncoder();
        systemUser.setPassword(md5PasswordEncoder.encodePassword(password, null));
        if (access == null || location == null || (!access.equals(String.valueOf(AccessID.PUBLICUSER.getValue())) && !access.equals(String.valueOf(AccessID.PUBLICEMAILUSER.getValue())) && !access.equals(String.valueOf(AccessID.PUBLICTAGCODEUSER.getValue())) && !access.equals(String.valueOf(AccessID.PUBLICPURCHASEUSER.getValue())))) {
            return false;
        }
        systemUser.setAccess(access);
        systemUser.setLocation(location);
        systemUser.setCreationDate(today);
        systemUser.setExpirationDate(expirationDate);
        baseDao.create(systemUser);

        UserFilter userFilter = new UserFilter();
        userFilter.setSystemUser(systemUser);
        userFilter.setStartDate(today);
        userFilter.setEndDate(expirationDate);
        userFilter.setLocationId("0");
        userFilter.setCameraId("0");
        userFilter.setTagIds("");

        if (parentLogin != null) {
            UserFilter filter = pictureService.getUserFilter(parentLogin);
            if (filter != null) {
                userFilter.setStartDate(filter.getStartDate());
                userFilter.setEndDate(filter.getEndDate());
                userFilter.setSystemUser(systemUser);
                userFilter.setLocationId(filter.getLocationId());
                userFilter.setCameraId(filter.getCameraId());
                userFilter.setTagIds(filter.getTagIds());
                userFilter.setStaff(filter.getStaff());
                List<Picture> refPictures = filter.getPictures();
                List<Picture> newPictures = new ArrayList<Picture>();
                if (refPictures != null) {
                    for (Picture refPicture : refPictures) {
                        newPictures.add(refPicture);
                    }
                }
                userFilter.setPictures(newPictures);
            }
        }
        baseDao.create(userFilter);
        return true;
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public String generateGeneralPublicLogin(Location location, Date today, String access) {
        String login = "";
        if (location != null) {
            Date startDate = DateConvertationUtils.getStartDate(0, today);
            Date endDate = DateConvertationUtils.getEndDate(0, today);
            List<SystemUser> locationTodayUsers = systemUserDao.getPublicUsersByDateAndLocation(location, startDate, endDate, access);
            int lastExistent = -1;
            if (locationTodayUsers != null && !locationTodayUsers.isEmpty()) {
                Collections.sort(locationTodayUsers, new Comparator<SystemUser>() {
                    public int compare(SystemUser o1, SystemUser o2) {
                        if (o1.getLoginName() != null && o2.getLoginName() != null) {
                            String[] loginParts1 = o1.getLoginName().split("_");
                            String[] loginParts2 = o2.getLoginName().split("_");
                            if (loginParts1.length != 0 && loginParts2.length != 0) {
                                String lastpart1 = loginParts1[loginParts1.length - 1];
                                String lastpart2 = loginParts2[loginParts2.length - 1];
                                try {
                                    int last1 = Integer.parseInt(lastpart1);
                                    int last2 = Integer.parseInt(lastpart2);
                                    return last1 > last2 ? 1 : -1;
                                } catch (NumberFormatException e) {
                                    e.printStackTrace();
                                }
                            }

                        } else {
                            return 0;
                        }
                        return 0;
                    }
                });
                SystemUser lastuser = locationTodayUsers.get(locationTodayUsers.size() - 1);
                String[] loginParts = lastuser.getLoginName().split("_");
                if (loginParts.length != 0) {
                    String lastpart = loginParts[loginParts.length - 1];
                    lastExistent = Integer.parseInt(lastpart);
                }
            } else {
                lastExistent = 0;
            }
            if (lastExistent != -1) {
                lastExistent++;
                login = generatePublicLogin() + location.getId() + "_" + DateConvertationUtils.shortStringDateFile(today) + "_" + lastExistent;
            }
        }
        return login;
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public String generateABTPublicLoginCode(String codeLetter) {
        boolean flag = true;
        String login = codeLetter + generatePublicCode6();
        while (flag) {
            Purchase purchase = purchaseDao.getPurchaseByCode(login);
            SystemUser user = userService.getUserByLogin(login);
            //TODO:Check tag purchase
            if (purchase == null && user == null) {
                flag = false;
            } else {
                login = codeLetter + generatePublicCode6();
            }
        }
        return login;
    }

    public boolean updateGeneralPublicLogin(PublicLoginModel model) {
        SystemUser systemUser = userService.getUserByLogin(model.getGeneratedLogin());
        if (systemUser != null) {
            systemUser.setLoginName(model.getLogin());
            Md5PasswordEncoder md5PasswordEncoder = new Md5PasswordEncoder();
            systemUser.setPassword(md5PasswordEncoder.encodePassword(model.getPassword(), null));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                systemUser.setExpirationDate(sdf.parse(model.getExpirationdate()));
                UserFilter filter = filterDao.getFilterByUser(systemUser);

                if (filter == null) {
                    filter = new UserFilter();
                    filter.setSystemUser(systemUser);
                }

                filter.setStartDate(sdf.parse(model.getStartDate()));
                filter.setEndDate(sdf.parse(model.getEndDate()));

                baseDao.update(filter);
                baseDao.update(systemUser);

            } catch (ParseException ex) {
                ex.printStackTrace();
                return false;
            }
            return true;
        }
        return false;
    }

}
