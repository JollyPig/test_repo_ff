package com.scnsoft.fotaflo.webapp.controller;

import com.scnsoft.fotaflo.webapp.service.PictureService;
import com.scnsoft.fotaflo.webapp.util.DateConvertationUtils;
import com.scnsoft.fotaflo.webapp.util.FotafloUtil;
import com.scnsoft.fotaflo.webapp.util.PictureReseizer;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 25.06.2012
 * Time: 10:33:11
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class UploadController {
    protected static Logger logger = Logger.getLogger(UploadController.class);

    final private static String LOCATION_NAME = "location";
    final private static String DEVICE_NAME = "deviceId";
    final private static String FILE_NAME = "filename";
    final private static String TIMEZONE_NAME = "Timezone";
    final private static String FILEDATE_NAME = "FileDate";
    final private static String TAG_NAME = "Tags";

    static {
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            public void uncaughtException(Thread t, Throwable e) {
                logger.error("thread = " + t.getName() + " e.getMessage = " + e.getMessage());
                e.printStackTrace();
            }
        });
    }

    @Autowired
    PictureService pictureService;

    private volatile List<String> pictures = new ArrayList<String>();

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public void createTestNew(HttpServletRequest request, HttpServletResponse response) throws IOException {
        long currentTimeStart = System.currentTimeMillis();
        ServletInputStream is = request.getInputStream();

        String picturePath = pictureService.getPicturePath();

        String location = request.getHeader(LOCATION_NAME);
        String deviceId = request.getHeader(DEVICE_NAME);
        String fileName = request.getHeader(FILE_NAME);
        String timeZone = request.getHeader(TIMEZONE_NAME);
        String fileDate = request.getHeader(FILEDATE_NAME);
        String tags = request.getHeader(TAG_NAME);
        int expectedSize = request.getContentLength();

        String id = location + "$" + deviceId + "$" + fileName;

        if (fileName != null && fileName.endsWith("JPG")) {
            logger.info("[ " + id + " ]" + " NAME WILL BE CONVERTED");
            fileName = fileName.replaceAll("JPG", "jpg");
        }
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        requestAttributes.setAttribute("location", location, 0);
        requestAttributes.setAttribute("deviceId", deviceId, 0);
        requestAttributes.setAttribute("fileName", fileName, 0);
        requestAttributes.setAttribute("timeZone", timeZone, 0);
        requestAttributes.setAttribute("fileDate", fileDate, 0);
        requestAttributes.setAttribute("tags", tags, 0);
        requestAttributes.setAttribute("expectedSize", expectedSize + "", 0);

        if (expectedSize == 0) {
            logger.error("[ " + id + " ] " + "!!!!!!!!!!!!!!!!!!!!!!!!!EXPECTED SIZE 0!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            response.setStatus(HttpServletResponse.SC_OK);
            return;
        }
        synchronized (pictures) {
            if (pictures.contains(id)) {
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                logger.error("[ " + id + " ] " + "REJECTED AS ALREADY EXISTS IN THE PROCCESSING LIST: " + id);
                return;
            } else {
                pictures.add(id);
            }
        }
        logger.info("[ " + id + " ] " + "PICTURES SIZE BEFORE: " + pictures.size());
        Date pictureDate = DateConvertationUtils.getPictureTimeParsed(timeZone, Long.parseLong(fileDate), fileName);

        logger.info("[ " + id + " ]" + " UPLOAD INFO: Filename " + fileName + " number X" + " received. Content length " + expectedSize);
        logger.info("[ " + id + " ]" + " UPLOAD INFO: deviceId: " + deviceId + " location: " + location);
        logger.info("[ " + id + " ]" + " UPLOAD INFO: Received time: " + fileDate + " timezone: " + timeZone);
        logger.info("[ " + id + " ]" + " UPLOAD INFO: TIME OF THE PICTURE [IMPORTANT]: " + pictureDate);
        logger.info("[ " + id + " ]" + " UPLOAD INFO: TAGS: " + tags);

        String folderPath = FotafloUtil.buildFolderPath(pictureDate, location, deviceId);
        String fullPath = picturePath + PictureService.IMG_DIR + folderPath + File.separator;
        String fullPathSmall = picturePath + PictureService.IMG_SMALL_DIR + folderPath + File.separator;

        //Not Synchronized
        boolean result = savePicture(fullPath, fullPathSmall, fileName, id, is, expectedSize);
        if (result) {
            logger.info("[ " + id + " ]" + " UPLOAD SUCCESS: loaded successfully.");
            Map<String, Boolean> resultMap = PictureReseizer.saveResizedPicture(fullPath + fileName, fullPathSmall + fileName);
            logger.info("[ " + id + " ]" + " ROTATED:" + resultMap.get("rotated"));
            pictureService.loadPictureToDB(location, deviceId, fileName, folderPath, pictureDate, tags, resultMap.get("rotated"), resultMap.get("pictureSize"));

            response.setStatus(HttpServletResponse.SC_OK);
            long currentTimeEnd = System.currentTimeMillis();
            logger.info("[ " + id + " ]" + " UPLOAD SUCCESS FULL TOTAL!!: " + (currentTimeEnd - currentTimeStart) / 1000);
        } else {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            logger.info("[ " + id + " ]" + " UPLOAD ERROR: File was not processed as  expectedSize != realSize");
        }
        synchronized (pictures) {
            if (pictures.contains(id)) {
                pictures.remove(id);
            }
        }
        logger.info("[ " + id + " ] " + "PICTURES SIZE AFTER: " + pictures.size());
    }

    public boolean savePicture(String fullPath, String fullPathSmall, String fileName, String id, InputStream is, int expectedSize) {
        boolean created = PictureReseizer.createPictureDirectorty(fullPath);
        boolean createdsmall = PictureReseizer.createPictureDirectorty(fullPathSmall);
        if (!created || !createdsmall) {
            logger.info("[ " + id + " ]" + " SYNCHRONIZED UPLOAD ERROR: Directory" + fullPath + " or " + "fullPathSmall where not created");
        }
        File serverFile = new File(fullPath + fileName);
        if (serverFile.exists()) {
            logger.info("[ " + id + " ]" + " SYNCHRONIZED UPLOAD WARN: file " + fileName + "seems already exist and tries to be rewritten incorrectly");
        }
        long currentTime1Before = System.currentTimeMillis();
        long realSize = 0;
        try {
            OutputStream outputStream = new FileOutputStream(serverFile);
            byte buffer[] = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0){
                outputStream.write(buffer, 0, length);
            }
            outputStream.flush();
            outputStream.close();
            is.close();
            long currentTime1After = System.currentTimeMillis();
            logger.info("[ " + id + " ]" + " SYNCHRONIZED UPLOAD TIME RECEIVING: " + (currentTime1After - currentTime1Before) / 1000);

            realSize = serverFile.length();
            logger.info("[ " + id + " ]" + " SYNCHRONIZED UPLOAD: " + expectedSize + " / " + realSize + " Loaded completely: " + (expectedSize == realSize));
            if (realSize != expectedSize) {
                logger.info("[ " + id + " ]" + " SYNCHRONIZED DELETED");
                serverFile.delete();
                return false;
            } else {
                return true;
            }
        } catch (IOException e) {
            if (serverFile.exists()) {
                logger.info("[ " + id + " ]" + " SYNCHRONIZED DELETED CAUGHT AN EXCEPTION");
                serverFile.delete();
            }
            logger.error("[ " + id + " ]" + " SYNCHRONIZED ERROR DURING FILE UPLOAD: " + e);
            return false;
        }

    }

}