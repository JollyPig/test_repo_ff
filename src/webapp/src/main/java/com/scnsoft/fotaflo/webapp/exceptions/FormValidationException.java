package com.scnsoft.fotaflo.webapp.exceptions;

public class FormValidationException extends Exception {

    protected String message;

    public FormValidationException () {
    }

    public FormValidationException (final String message) {
        super (message);
    }

}
