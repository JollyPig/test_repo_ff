package com.scnsoft.fotaflo.webapp.dao;

import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.model.Tag;
import com.scnsoft.fotaflo.webapp.model.TagPurchase;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 12.10.12
 * Time: 5:30
 * To change this template use File | Settings | File Templates.
 */
public interface ITagDao {

    public List<Tag> getTagsByLocation(Location location, Date start, Date end);

    public List<Tag> getTagsByUser(Integer[] usertagsIds, Date start, Date end);

    public Tag getTagsByUserAndName(Integer[] usertagsIds, String name);

    public List<Tag> getTagsByLocation(Location location);

    public Tag getTagByNameLocation(String tag, Location location);

    public TagPurchase getTagPurchaseByCode(String code);

    public List<TagPurchase> getTagPurchaseByDatesLocation(Date startDate, Date endDate, Location location);

    public List<TagPurchase> getTagPurchaseByDatesLocationStatus(Date startDate, Date endDate, Location location, int status);

    public List<TagPurchase> getPurchaseByFilter(Location location, Date startDate, Date endDate, String email, String code, int status, String targetTag);

}
