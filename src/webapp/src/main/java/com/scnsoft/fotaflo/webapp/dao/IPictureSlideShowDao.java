package com.scnsoft.fotaflo.webapp.dao;

import com.scnsoft.fotaflo.webapp.common.ResultList;
import com.scnsoft.fotaflo.webapp.model.Camera;
import com.scnsoft.fotaflo.webapp.model.Picture;
import com.scnsoft.fotaflo.webapp.model.Tag;

import java.util.Date;
import java.util.List;

public interface IPictureSlideShowDao extends IDao {
    @SuppressWarnings("unchecked")
    ResultList<Picture> findExtended(int start, int limit, Date startDate, Date endDate, List<Camera> cameras);

    @SuppressWarnings("unchecked")
    ResultList<Picture> findExtendedByTag(int start, int limit, Date startDate, Date endDate, List<Camera> cameras, List<Tag> tags);

    @SuppressWarnings("unchecked")
    ResultList<Picture> findExtendedWithoutTag(int start, int limit, Date startDate, Date endDate, List<Camera> cameras);

    @SuppressWarnings("unchecked")
    ResultList<Picture> findExtendedTagOnly(int start, int limit, List<Tag> tags);
}
