package com.scnsoft.fotaflo.webapp.dao;

import com.scnsoft.fotaflo.webapp.model.*;
import com.scnsoft.fotaflo.webapp.model.Package;

import java.util.Date;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 31.03.2012
 * Time: 10:38:46
 * To change this template use File | Settings | File Templates.
 */
public interface IReportDao {

    public List<Location> getLocations();

    public List<Package> getLocationsByPackage(Location location);

    public List<Staff> getStaffByLocation(Location location);

    public List<Statistic> getStatisticByLocationForThePeriod(Location location, Date startDate, Date endDate);

    public List findExtendedPackages(Date startDate, Date endDate, Location location);

    public List<PublicStatistic> getPublicStatisticByLocation(Date startDate, Date endDate, Location location);

    public List<ReportEmails> getReportEmails(Date startDate, Date endDate, Location location);
}
