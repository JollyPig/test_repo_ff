package com.scnsoft.fotaflo.webapp.dao;

import com.scnsoft.fotaflo.webapp.model.Location;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 12.05.14
 * Time: 12:55
 * To change this template use File | Settings | File Templates.
 */
public interface ILocationDao extends com.scnsoft.fotaflo.common.dao.IDao<Location, Integer> {

    Location findById(Integer id);

}
