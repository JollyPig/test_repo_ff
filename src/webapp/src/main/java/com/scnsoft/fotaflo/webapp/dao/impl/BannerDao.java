package com.scnsoft.fotaflo.webapp.dao.impl;

import com.scnsoft.fotaflo.webapp.dao.IBannerDao;
import com.scnsoft.fotaflo.webapp.model.BannerSlideShow;
import com.scnsoft.fotaflo.webapp.model.Location;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 13.03.2012
 * Time: 11:54:15
 * To change this template use File | Settings | File Templates.
 */
public class BannerDao extends BaseDao implements IBannerDao {

    public List<BannerSlideShow> getBannerSlideShowByLocation(Location
                                                                      location) {
        return getHibernateTemplate().find(
                "from BannerSlideShow bannerSlideShow where bannerSlideShow.location = ?", location);
    }
}
