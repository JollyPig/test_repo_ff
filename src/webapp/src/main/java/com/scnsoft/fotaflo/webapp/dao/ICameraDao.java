package com.scnsoft.fotaflo.webapp.dao;

import com.scnsoft.fotaflo.webapp.model.Camera;
import com.scnsoft.fotaflo.webapp.model.Location;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 31.03.2012
 * Time: 8:45:10
 * To change this template use File | Settings | File Templates.
 */
public interface ICameraDao {

    public List<Camera> getCamerasByLocation(Location location);

    public Camera getCamera(Integer cameraId);
}
