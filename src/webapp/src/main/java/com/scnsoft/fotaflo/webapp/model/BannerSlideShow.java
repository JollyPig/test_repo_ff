package com.scnsoft.fotaflo.webapp.model;

import com.scnsoft.fotaflo.common.model.AbstractEntity;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 21.01.2012
 * Time: 16:49:41
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name = "bannerSlideShow")
public class BannerSlideShow extends AbstractEntity {

    String bannerType;
    String bannerUrl;
    Location location;

    @Column(name = "bannerType", length = 255)
    public String getBannerType() {
        return bannerType;
    }

    public void setBannerType(String bannerType) {
        this.bannerType = bannerType;
    }

    @Column(name = "bannerUrl", length = 255)
    public String getBannerUrl() {
        return bannerUrl;
    }

    public void setBannerUrl(String bannerUrl) {
        this.bannerUrl = bannerUrl;
    }

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "location_id")
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}

