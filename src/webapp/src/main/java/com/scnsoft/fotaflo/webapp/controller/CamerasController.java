package com.scnsoft.fotaflo.webapp.controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.scnsoft.fotaflo.webapp.model.Camera;
import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.model.Picture;
import com.scnsoft.fotaflo.webapp.service.CameraService;
import com.scnsoft.fotaflo.webapp.service.LocationService;
import com.scnsoft.fotaflo.webapp.service.PictureService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by MilkevichP
 * Date: 22.04.11
 * Time: 12:23
 */

@Controller
@RequestMapping("/cameras")
public class CamerasController {
    protected static Logger logger = Logger.getLogger(CamerasController.class);

    private final static String rootProperty = "cameras";
    private final static String successProperty = "success";
    private final static String locationId = "locationId";

    @Autowired
    CameraService cameraService;

    @Autowired
    LocationService locationService;

    @Autowired
    PictureService pictureService;

    @RequestMapping(value = "getCameras.json")
    public
    @ResponseBody
    Map<String, ? extends Object> getCameras(@RequestParam(value = CamerasController.locationId, required = false) Integer locationId) {

        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        HashMap modelMap = new HashMap();
        modelMap.put(rootProperty, cameraService.getCameras(locationId == null ? 0 : locationId, currentUser));
        modelMap.put(successProperty, true);
        return modelMap;
    }

    @RequestMapping(value = "getRealCameras.json")
    public
    @ResponseBody
    Map<String, ? extends Object> getRealCameras(@RequestParam(value = CamerasController.locationId, required = false) Integer locationId,
                                                 HttpServletRequest httpServletRequest) {

        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        HashMap modelMap = new HashMap();
        int start = Integer.valueOf(httpServletRequest.getParameter("start") != null ? httpServletRequest.getParameter("start") : "0");
        int limit = Integer.valueOf(httpServletRequest.getParameter("limit") != null ? httpServletRequest.getParameter("limit") : "0");
        modelMap.put(rootProperty, cameraService.getRealCameras(locationId == null ? 0 : locationId, currentUser, start, limit));
        modelMap.put("total", cameraService.getRealCameras(locationId == null ? 0 : locationId, currentUser, 0, 0).size());
        modelMap.put(successProperty, true);
        return modelMap;
    }

    @RequestMapping(value = "saveCameras.json")
    public
    @ResponseBody
    Map<String, ? extends Object> saveCameras(@RequestParam(value = CamerasController.rootProperty, required = false) String json,
                                              @RequestParam(value = CamerasController.locationId, required = false) Integer locationId) {

        HashMap modelMap = new HashMap();
        Gson g = new Gson();
        List ls;

        try {
            Type listType = new TypeToken<List<Camera>>() {
            }.getType();
            ls = g.fromJson(json, listType);
        } catch (Exception e) {
            ls = new ArrayList();
            ls.add(g.fromJson(json, Camera.class));
        }

        Location loc = locationService.getLocation(locationId);
        if (loc != null) {
            for (Object l : ls) {
                Camera p = (Camera) l;
                p.setLocation(loc);
            }
        }
        cameraService.saveCameras(ls);
        modelMap.put(rootProperty, ls.size() == 1 ? ls.get(0) : ls);
        modelMap.put(successProperty, true);
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User [ " + currentUser + " ]" + " saved cameras for location " + loc.getLocationName());
        return modelMap;
    }

    @RequestMapping(value = "deleteCameras.json")
    public
    @ResponseBody
    Map<String, ? extends Object> deleteCameras(@RequestParam(value = CamerasController.rootProperty, required = false) Integer[] ids) {
        HashMap<String, Object> modelMap = new HashMap<String, Object>();

        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        List<Picture> ps = pictureService.getPicturesForCameras(ids);

        if(ps != null && !ps.isEmpty()){
            modelMap.put(successProperty, false);
            modelMap.put("msg", "Cannot delete camera. There are pictures associated with it.");
            logger.info("User [ " + currentUser + " ]" + " couldn't delete cameras " + ids.toString());
        }else{
            cameraService.deleteCameras(ids);
            modelMap.put(successProperty, true);
            logger.info("User [ " + currentUser + " ]" + " deleted cameras " + ids.toString());
        }

        return modelMap;
    }

}
