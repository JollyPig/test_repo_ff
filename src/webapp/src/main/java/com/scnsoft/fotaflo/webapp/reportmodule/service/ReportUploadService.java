package com.scnsoft.fotaflo.webapp.reportmodule.service;

import com.scnsoft.fotaflo.webapp.reportmodule.model.LocationObj;
import com.scnsoft.fotaflo.webapp.reportmodule.model.PictureObj;
import com.scnsoft.fotaflo.webapp.reportmodule.reportutils.LogAnalizer;
import com.scnsoft.fotaflo.webapp.reportmodule.reportutils.LogParser;
import com.scnsoft.fotaflo.webapp.reportmodule.reportutils.ReportFileCreator;
import com.scnsoft.fotaflo.webapp.util.DateConvertationUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 20.08.2012
 * Time: 9:23:22
 * To change this template use File | Settings | File Templates.
 */
public class ReportUploadService {
    protected static Logger logger = Logger.getLogger(ReportUploadService.class);

    private String fullPath;
    private String emails;
    private String reportPath;
    private String logName;
    private String reportName;

    //returns the report path
    //if null the report was not generated because of some error
    public Map<String, Object> fileReportCreate(Date date) {
        Map<String, Object> reportMap = new HashMap<String, Object>();
        String fileNameFull = "";
        String fileDate = DateConvertationUtils.getDateString(date);

        logger.info("Report path: " + reportPath + ", Emails: " + emails);
        Map<String, PictureObj> pictureMap = new HashMap<String, PictureObj>();

        boolean logExist = true;
        if (new File(fullPath + logName + "." + fileDate).exists()) {
            logger.info("Log File " + logName + "." + fileDate + " exists and is going to be parsed:");
            fileNameFull = fullPath + logName + "." + fileDate;
        } else {
            File logFile = new File(fullPath + logName);
            if (logFile.exists() && logFile.lastModified() >= date.getTime()) {
                logger.info("Log File " + logName + "." + fileDate + " DO NOT EXIST. The file " + logName + " will be parsed");
                fileNameFull = fullPath + logName;
            } else {
                logger.error("Log File " + logName + " DO NOT EXIST OR TOO OLD!!!!!!!, REPORT will not be generated");
                logExist = false;
            }
        }
        String fullReportName = reportPath + reportName + "_" + fileDate + ".txt";
        ReportFileCreator fileWriter = new ReportFileCreator();

        if (logExist) {
            LogParser analizer = new LogParser(fileNameFull, pictureMap, fileDate);
            analizer.analyze();
            LogAnalizer searcher = new LogAnalizer(pictureMap);
            Map<String, LocationObj> seObjMap = searcher.groupByLocationCamera();
            fileWriter.writeTofile(fullReportName, seObjMap, analizer.getNullsizable(), analizer.getPictures());
        } else {
            fileWriter.writeTofile(fullReportName, new HashMap<String, LocationObj>(), 0, new ArrayList<String>());
        }

        reportMap.put("report", fullReportName);
        reportMap.put("emails", Arrays.asList(emails.split(",")));
        return reportMap;
    }

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        ReportUploadService.logger = logger;
    }

    public String getFullPath() {
        return fullPath;
    }

    public void setFullPath(String fullPath) {
        this.fullPath = fullPath;
    }

    public String getEmails() {
        return emails;
    }

    public void setEmails(String emails) {
        this.emails = emails;
    }

    public String getReportPath() {
        return reportPath;
    }

    public void setReportPath(String reportPath) {
        this.reportPath = reportPath;
    }

    public String getLogName() {
        return logName;
    }

    public void setLogName(String logName) {
        this.logName = logName;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }
}
