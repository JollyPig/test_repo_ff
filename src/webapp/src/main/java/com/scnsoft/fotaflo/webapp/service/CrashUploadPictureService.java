package com.scnsoft.fotaflo.webapp.service;

import com.scnsoft.fotaflo.webapp.util.MailSenderUtil;
import org.apache.log4j.Logger;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

/**
 * @author Anatoly Selitsky
 */
public class CrashUploadPictureService {
    protected static Logger logger = Logger.getLogger(CrashUploadPictureService.class);

    private ReportService reportService;

    private String emailsForSending;

    private String subject;

    private MailSenderUtil mailSender = new MailSenderUtil();

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }

    public void setEmailsForSending(String emailsForSending) {
        this.emailsForSending = emailsForSending;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void sendReportAboutCrashUploadPictures(Map<String, String> parameters) {
        Map<String, Object> settings = reportService.getEmailReportSettings();
        String body = buildReportBody(parameters);
        mailSender.sendMailMessage(
                Arrays.asList(emailsForSending.split(",")),
                parseParam(settings, "fromEmail"),
                subject,
                body,
                null,
                parseParam(settings, "smtpServer"),
                parseParam(settings, "smtpServerPort"),
                parseParam(settings, "smtpServerFromPassword"),
                "on".equals(parseParam(settings, "sslcheck")),
                null
        );
    }

    private String buildReportBody(final Map<String, String> parameters) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = new Date(Long.valueOf(parseParam(parameters, "fileDate")));
        String dateAndTime = sdf.format(date);
        StringBuilder builder = new StringBuilder("Dear colleagues!");
        builder.append("\n\nReceived picture has wrong format and cannot be processed.\n")
                .append("Please see picture details below\n\n")
                .append("Location: {0}\n")
                .append("DeviceId: {1}\n")
                .append("FileName: {2}\n")
                        //.append("TimeZone: {3}\n")
                .append("FileDate: {3}\n")
                .append("Tags: {4}\n")
                .append("ExceptedSize: {5}\n");
        MessageFormat reportBody = new MessageFormat(builder.toString());
        String location = parseParam(parameters, "location");
        String deviceId = parseParam(parameters, "deviceId");
        String fileName = parseParam(parameters, "fileName");
        String timeZone = parseParam(parameters, "timeZone");
        String tags = parseParam(parameters, "tags");
        String expectedSize = parseParam(parameters, "expectedSize");
        logger.info("Received picture has wrong format and cannot be processed: location - " + location + ", deviceId - " + deviceId + ", fileName - " + fileName + ", timeZone - " + timeZone + ", tags - " + tags + ", expectedSize - " + expectedSize);
        return reportBody.format(new Object[]{
                location,
                deviceId,
                fileName,
                //timeZone,
                dateAndTime,
                tags,
                expectedSize
        });
    }

    private String parseParam(Map<String, ? extends Object> settings, String paramName) {
        if (settings == null) {
            throw new IllegalStateException();
        }
        return settings.get(paramName) != null ? settings.get(paramName).toString() : null;
    }

}
