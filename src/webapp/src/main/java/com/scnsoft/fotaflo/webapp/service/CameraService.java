package com.scnsoft.fotaflo.webapp.service;

import com.scnsoft.fotaflo.common.util.NumberUtils;
import com.scnsoft.fotaflo.common.util.StringUtils;
import com.scnsoft.fotaflo.webapp.bean.CameraBean;
import com.scnsoft.fotaflo.webapp.dao.ICameraDao;
import com.scnsoft.fotaflo.webapp.dao.IDao;
import com.scnsoft.fotaflo.webapp.model.Camera;
import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.model.SystemUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class CameraService {

    @Autowired
    private ICameraDao cameraDao;

    @Autowired
    private UserService userService;

    @Autowired
    private IDao baseDao;

    @Autowired
    private LocationService locationService;

    public List<Camera> getAllCameras() {
        List<Camera> cameraList = baseDao.retrieveAll(Camera.class);
        return cameraList;
    }

    //gets cameras for  the current user and current location from the database. If location id =0  and  user has admin rights that
    // means that all locations were selected and all cameras should be retrieved from the database

    public List<CameraBean> getCameras(int locationId, String userlogin) {
        List<CameraBean> list = new ArrayList();
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        //if not admin is logged in  we are getting all the cameras for the user acoording to the location he belongs to
        if (!systemUser.getAccess().equals("1")) {
            Location location = systemUser.getLocation();
            List<Camera> cameraList = cameraDao.getCamerasByLocation(location);
            for (Camera camera : cameraList) {
                list.add(new CameraBean(camera));
            }
            //all camera element was created for the current location
            list.add(new CameraBean(createAllCamerasElement(location)));
        } else {
            //if admin is logged in
            list = getCamerasForAdminByLocation(locationId);
            list.add(new CameraBean(createAllCamerasElement(null)));
        }
        return list;
    }

    public List<CameraBean> getRealCameras(int locationId, String userlogin, int start, int limit) {
        List<CameraBean> list = new ArrayList();
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        //if not admin is logged in  we are getting all the cameras for the user acoording to the location he belongs to
        if (!systemUser.getAccess().equals("1")) {
            Location location = systemUser.getLocation();
            List<Camera> cameraList = cameraDao.getCamerasByLocation(location);
            int count = cameraList.size();
            if (start == 0 && limit == 0)
                limit = count;
            for (int i = start; (i < start + limit) && i < count; i++) {
                list.add(new CameraBean(cameraList.get(i)));
            }
        } else {
            //if admin is logged in
            if (locationId != 0) {
                Location location = (Location) baseDao.retrieve(Location.class, locationId);
                List<Camera> cameraList = cameraDao.getCamerasByLocation(location);
                int count = cameraList.size();
                if (start == 0 && limit == 0)
                    limit = count;
                for (int i = start; (i < start + limit) && i < count; i++) {
                    list.add(new CameraBean(cameraList.get(i)));
                }

            } else {
                List<Camera> cameraList = baseDao.retrieveAll(Camera.class);
                int count = cameraList.size();
                if (start == 0 && limit == 0)
                    limit = count;
                for (int i = start; (i < start + limit) && i < count; i++) {
                    list.add(new CameraBean(cameraList.get(i)));
                }
            }
        }
        return list;
    }

    //get cameras for the admin.
    public List<CameraBean> getCamerasForAdminByLocation(int locationId) {
        List<CameraBean> list = new ArrayList();
        //not all locations were selected
        if (locationId != 0) {
            Location location = (Location) baseDao.retrieve(Location.class, locationId);
            List<Camera> cameraList = cameraDao.getCamerasByLocation(location);
            for (Camera camera : cameraList) {
                list.add(new CameraBean(camera));
            }

        } else {
            List<Camera> cameraList = baseDao.retrieveAll(Camera.class);
            for (Camera camera : cameraList) {
                list.add(new CameraBean(camera));
            }
        }
        return list;
    }

    public void deleteCameras(Integer[] ids) {
        for (Integer id : ids) {
            baseDao.delete(Camera.class, id);
        }
    }

    public void saveCameras(List<Camera> cameras) {
        for (Camera camera : cameras) {
            baseDao.update(camera);
        }
    }

    public void saveCamera(CameraBean cameraBean) {
        Camera camera = new Camera();
        if(cameraBean != null && !StringUtils.isEmpty(cameraBean.getCameraName())){
            Location location = locationService.getLocation(NumberUtils.getInteger(cameraBean.getLocation()));
            if(location != null){
                camera.setId(cameraBean.getId());
                camera.setCameraName(cameraBean.getCameraName());
                camera.setLocation(location);

                baseDao.update(camera);
            }
        }
    }

    protected Camera createAllCamerasElement(Location location) {
        Camera camera = new Camera();
        camera.setId(0);
        camera.setCameraName("All Cameras");
        camera.setLocation(location);
        return camera;
    }

    public Camera getCamera(Integer cameraId) {
        return cameraDao.getCamera(cameraId);
    }

    public List<Camera> getCamerasByLocation(Location location){
        return cameraDao.getCamerasByLocation(location);
    }

}
