package com.scnsoft.fotaflo.webapp.model;

import com.scnsoft.fotaflo.common.model.AbstractEntity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */

@Entity
@Table(name = "usersettings")
public class UserSettings extends AbstractEntity {

    String pageSize;
    Integer speed;
    Integer pictureCount;
    Integer startTime;
    Integer delay;

    private Set<Tag> tags = new HashSet<Tag>();
    private Tag lastTag;

    @Column(name = "pictureCount")
    public Integer getPictureCount() {
        return pictureCount;
    }

    public void setPictureCount(Integer pictureCount) {
        this.pictureCount = pictureCount;
    }

    @Column(name = "speed")
    public Integer getSpeed() {
        return speed;
    }

    public void setSpeed(Integer speed) {
        this.speed = speed;
    }

    @Column(name = "pageSize", length = 45)
    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    @Column(name = "startTime")
    public Integer getStartTime() {
        return startTime;
    }

    public void setStartTime(Integer startTime) {
        this.startTime = startTime;
    }

    @Column(name = "delay")
    public Integer getDelay() {
        return delay;
    }

    public void setDelay(Integer delay) {
        this.delay = delay;
    }

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinTable(name = "user_tags", joinColumns = {@JoinColumn(name = "user_tag_id")}, inverseJoinColumns = {@JoinColumn(name = "tag_id")})
    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "last_tag_id")
    public Tag getLastTag() {
        return lastTag;
    }

    public void setLastTag(Tag lastTag) {
        this.lastTag = lastTag;
    }
}
