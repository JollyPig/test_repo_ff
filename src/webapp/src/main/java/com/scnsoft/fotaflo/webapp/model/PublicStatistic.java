package com.scnsoft.fotaflo.webapp.model;

import com.scnsoft.fotaflo.common.model.AbstractEntity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 07.02.2012
 * Time: 12:54:51
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "public_statistic")
public class PublicStatistic extends AbstractEntity {

    Location location;
    Package packages;
    Integer numberOfPicturesSent;
    String participantEmail;
    Date creationDate;
    String userLogin;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "packages_id")
    public Package getPackages() {
        return packages;
    }

    public void setPackages(Package packages) {
        this.packages = packages;
    }

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "location_id")
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Column(name = "number_pictures_sent")
    public Integer getNumberOfPicturesSent() {
        return numberOfPicturesSent;
    }

    public void setNumberOfPicturesSent(Integer numberOfPicturesSent) {
        this.numberOfPicturesSent = numberOfPicturesSent;
    }

    @Column(name = "user_login")
    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }


    @Column(name = "creation_date")
    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Column(name = "participant_email")
    public String getParticipantEmail() {
        return participantEmail;
    }

    public void setParticipantEmail(String participantEmail) {
        this.participantEmail = participantEmail;
    }
}
