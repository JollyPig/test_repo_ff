package com.scnsoft.fotaflo.webapp.dao.impl;

import com.scnsoft.fotaflo.webapp.dao.IPictureDao;
import com.scnsoft.fotaflo.webapp.model.Camera;
import com.scnsoft.fotaflo.webapp.model.Picture;
import com.scnsoft.fotaflo.webapp.model.SystemUser;
import com.scnsoft.fotaflo.webapp.model.Tag;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */
public class PictureDao extends BaseDao implements IPictureDao {

    public List getPicturesForCameras(Integer[] cameras) {
        StringBuffer query = new StringBuffer();
        query.append("select  picture from Picture as picture where ");
        query.append("picture.camera.id in ( :cameras )");

        return getSession().createQuery(query.toString()).setParameterList("cameras", cameras).list();
    }

    public List<Picture> findExtended(Date startDate, Date endDate, List<Camera> cameras) {
        StringBuffer query = new StringBuffer();
        query.append("select  picture from Picture as picture where ");
        query.append("picture.creationDate >= :start ");
        query.append("and picture.creationDate <= :end ");
        if (cameras.size() > 0) {
            query.append("and picture.camera.id in (");
            query.append(parseList(cameras));
            query.append(")");
        } else {
            return new ArrayList<Picture>();
        }

        List list1 = getSession().createQuery(query.toString())
                .setParameter("start", startDate)
                .setParameter("end", endDate)
                .list();
        return list1;
    }

    public List<Picture> findExtendedByTag(Date startDate, Date endDate, List<Camera> cameras, List<Tag> tags) {
        StringBuffer query = new StringBuffer();
        query.append("select  picture from Picture as picture join picture.tags as tags where ");
        query.append("picture.creationDate >= :start ");
        query.append("and picture.creationDate <= :end ");
        if (cameras.size() > 0) {
            query.append("and picture.camera.id in (");
            query.append(parseList(cameras));
            query.append(")");
        } else {
            return new ArrayList<Picture>();
        }
        if (tags.size() > 0) {
            query.append("and tags.id in (");
            query.append(parseTagList(tags));
            query.append(")");
        }

        List list1 = getSession().createQuery(query.toString())
                .setParameter("start", startDate)
                .setParameter("end", endDate)
                .list();
        return list1;
    }

    public List<Picture> findExtendedWithoutTag(Date startDate, Date endDate, List<Camera> cameras) {
        StringBuffer query = new StringBuffer();
        query.append("select  picture from Picture as picture left join picture.tags as tags where ");
        query.append("picture.creationDate >= :start ");
        query.append("and picture.creationDate <= :end ");
        if (cameras.size() > 0) {
            query.append("and picture.camera.id in (");
            query.append(parseList(cameras));
            query.append(")");
        } else {
            return new ArrayList<Picture>();
        }

        query.append("and tags.id = null");

        List list1 = getSession().createQuery(query.toString())
                .setParameter("start", startDate)
                .setParameter("end", endDate)
                .list();
        return list1;
    }

    public List<Picture> findExtendedTagOnly(List<Tag> tags) {
        StringBuffer query = new StringBuffer();
        query.append("select  picture from Picture as picture join picture.tags as tags where ");
        if (tags.size() > 0) {
            query.append("tags.id in (");
            query.append(parseTagList(tags));
            query.append(")");
        }
        List list1 = getSession().createQuery(query.toString())
                .list();
        return list1;
    }

    private String parseList(List list) {
        StringBuffer result = new StringBuffer();
        for (int i = 0; i < list.size(); i++) {
            if (i != 0)
                result.append(", ");
            result.append(((Camera) list.get(i)).getId());
        }
        return result.toString();
    }

    private String parseTagList(List list) {
        StringBuffer result = new StringBuffer();
        for (int i = 0; i < list.size(); i++) {
            if (i != 0)
                result.append(", ");
            result.append(((Tag) list.get(i)).getId());
        }
        return result.toString();
    }

    public void removePicturesSelectedForUser(SystemUser user) {
        Set<Picture> pictures = user.getUserPictures();
        for (Picture picture : pictures) {
            if (picture.getSystemUsers().contains(user)) {
                Set<SystemUser> systemUsers = picture.getSystemUsers();
                systemUsers.remove(user);
                picture.setSystemUsers(systemUsers);
                update(picture);
            }
        }
    }

}
