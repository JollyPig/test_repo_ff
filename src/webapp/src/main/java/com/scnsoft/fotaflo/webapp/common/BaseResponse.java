package com.scnsoft.fotaflo.webapp.common;

/**
 * Response for ExtJS request.
 *
 * @author Anatoly Selitsky
 */
public class BaseResponse {

    private boolean success;

    public BaseResponse() {
        this(true);
    }

    public BaseResponse(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

}
