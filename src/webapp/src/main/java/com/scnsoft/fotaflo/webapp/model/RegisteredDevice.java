package com.scnsoft.fotaflo.webapp.model;

import com.scnsoft.fotaflo.common.model.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "registeredDevice")
public class RegisteredDevice extends AbstractEntity {

    private String registeredID;

    @Column(name = "registeredId")
    public String getRegisteredID() {
        return registeredID;
    }

    public void setRegisteredID(final String registredID) {
        this.registeredID = registredID;
    }
}
