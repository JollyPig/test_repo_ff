package com.scnsoft.fotaflo.webapp.bean;

/**
 * Created by Nadezda Drozdova
 * Date: May 11, 2011
 * Time: 5:52:15 PM
 */
public class PreviewPrintBean {

    protected int id;
    String url;
    String logoMainUrl;
    String logoUrl;
    String logoText;
    int numbersToPrint;

    public PreviewPrintBean() {
    }

    public PreviewPrintBean(String url, String logoMainUrl, String logoUrl, String logoText, int numbersToPrint) {
        this.url = url;
        this.logoMainUrl = logoMainUrl;
        this.logoUrl = logoUrl;
        this.logoText = logoText;
        this.numbersToPrint = numbersToPrint;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLogoMainUrl() {
        return logoMainUrl;
    }

    public void setLogoMainUrl(String logoMainUrl) {
        this.logoMainUrl = logoMainUrl;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getLogoText() {
        return logoText;
    }

    public void setLogoText(String logoText) {
        this.logoText = logoText;
    }

    public int getNumbersToPrint() {
        return numbersToPrint;
    }

    public void setNumbersToPrint(int numbersToPrint) {
        this.numbersToPrint = numbersToPrint;
    }
}
