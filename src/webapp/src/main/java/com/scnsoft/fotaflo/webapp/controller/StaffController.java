package com.scnsoft.fotaflo.webapp.controller;

import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.model.Staff;
import com.scnsoft.fotaflo.webapp.service.LocationService;
import com.scnsoft.fotaflo.webapp.service.StaffService;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */
@Controller
public class StaffController {

    protected static Logger logger = Logger.getLogger(StaffController.class);
    private final static String successProperty = "success";

    @Autowired
    private StaffService staffService;

    @Autowired
    private LocationService locationService;

    @RequestMapping(value = "getStaff.json")
    public
    @ResponseBody
    Map<String, ? extends Object> loadStaffByLocation(HttpServletRequest httpServletRequest) {

        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        HashMap modelMap = new HashMap();
        int locationId = Integer.valueOf(httpServletRequest.getParameter("locationId") != null ? httpServletRequest.getParameter("locationId") : "0");
        modelMap.put("staff", staffService.getStaffByLocation(currentUser, locationId));
        return modelMap;
    }

    @RequestMapping(value = "getPagingStaff.json")
    public
    @ResponseBody
    Map<String, Object> loadPagingStaffByLocation(
            @RequestParam("locationId") Integer locationId, @RequestParam("limit") Integer limit, @RequestParam("start") Integer start) {

        final String currentUser = SecurityContextHolder.getContext()
                .getAuthentication().getName();
        Map<String, Object> modelMap = createModelMap(true);
        modelMap.put("staff",
                staffService.getPagedStaffForLocation(currentUser, start, limit));
        modelMap.put("total", staffService.countStaffForLocation(currentUser));
        return modelMap;
    }

    @RequestMapping("createStaff.json")
    public
    @ResponseBody
    Map<String, Object> createStaff(@RequestParam("staff") JSONObject jsonStaff) {

        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        Location location = locationService.getLocation(jsonStaff
                .getInt("locationId"));
        Staff newStaff = new Staff();
        newStaff.setLocation(location);
        newStaff.setStaffName(jsonStaff.getString("staffName"));
        staffService.saveStaff(newStaff);
        Map<String, Object> modelMap = createModelMap(true);
        modelMap.put("staff", newStaff);
        logger.info("User [ " + currentUser + " ]" + " created staff " + newStaff.getStaffName());
        return modelMap;
    }

    @RequestMapping("deleteStaff.json")
    public
    @ResponseBody
    Map<String, Object> deleteStaff(@RequestParam("staff") Integer staffId) {
        final String currentUser = SecurityContextHolder.getContext()
                .getAuthentication().getName();
        staffService.deleteStaffById(staffId);
        logger.info("User [ " + currentUser + " ]" + " deletd staff " + staffId);
        return createModelMap(true);
    }

    @RequestMapping(value = "updateStaff.json", method = RequestMethod.POST)
    public
    @ResponseBody
    Map<String, Object> updateStaff(@RequestParam("staff") JSONObject jsonStaff) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        Staff staff = staffService.getStaffById(jsonStaff.getInt("id"));
        Location location = locationService.getLocation(jsonStaff
                .getInt("locationId"));
        staff.setStaffName(jsonStaff.getString("staffName"));
        staff.setLocation(location);
        staffService.updateStaff(staff);
        logger.info("User [ " + currentUser + " ]" + " updated staff " + staff.getStaffName());
        return createModelMap(true);
    }

    private Map<String, Object> createModelMap(Boolean success) {
        Map<String, Object> modelMap = new HashMap<String, Object>();
        modelMap.put(successProperty, success);
        return modelMap;
    }
}
