package com.scnsoft.fotaflo.webapp.util;

import org.apache.log4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */
public class Filter {

    Date startDate;
    Date endDate;

    String location;
    String camera;
    String tag;

    protected static Logger logger = Logger.getLogger("Filter");


    public Filter(String startDay, String startTime, String endDay, String endTime, String location, String camera, String tag) {

        this.startDate = formDate(startDay, startTime, new Date());
        this.endDate = formDate(endDay, endTime, new Date());

        this.location = (location == null || location.equals("")) ? "0" : location;
        this.camera = (camera == null || camera.equals("") || camera.equals("All Cameras")) ? "0" : camera;
        this.tag = tag;//(tag == null || tag.equals("")) ? "0" : tag;

    }

    public Filter(Date startDate, Date endDate, String location, String camera, String tag) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.location = (location == null || location.equals("")) ? "0" : location;
        this.camera = (camera == null || camera.equals("") || camera.equals("All Cameras")) ? "0" : camera;
        this.tag = tag;//(tag == null || tag.equals("")) ? "0" : tag;
    }


    public static Date formDate(String day, String time, Date date) {
        if (day != null && !day.equals("")) {
            //	Sun Apr 10 2011 00:00:00 GMT+0300
            //2011-04-12T00:00:00
            try {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                date = formatter.parse(day);
                //logger.info("Received date:" + date);
            } catch (ParseException e) {
                logger.info("Default Received date:" + date);
                logger.error(e);
            }
        }

        //logger.info("Received date:" + date);

        if ((time != null && !time.equals(""))) {
            Pattern pattern = Pattern.compile("(\\d+):(\\d+)\\s+(\\w+)");
            Matcher matcher = pattern.matcher(time);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            //logger.info("Received time:" + cal.getTime());
            if (matcher.find()) {
                Integer hour = Integer.valueOf(matcher.group(1));
                Integer minute = Integer.valueOf(matcher.group(2));
                Integer pm_am = ((matcher.group(3)).equals("AM")) ? Calendar.AM : Calendar.PM;

                cal.set(Calendar.HOUR, hour);
                //MEGA HACK!!!
                cal.getTime();
                cal.set(Calendar.MINUTE, minute);
                cal.set(Calendar.AM_PM, pm_am);
                //logger.info("Converted date :" + cal.getTime());
            } else {
                logger.info("Default Converted date :" + cal.getTime());
            }
            date = cal.getTime();
            //  logger.info("Converted date :" + cal.getTime());
        }
        //logger.info("Converted time :" + date);

        return date;
    }

    public Date getStartDate() {
        return this.startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return this.endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCamera() {
        return camera;
    }

    public void setCamera(String camera) {
        this.camera = camera;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
