package com.scnsoft.fotaflo.webapp.bean;

import java.util.List;
import java.util.Map;

/**
 * Created by Nadezda Drozdova
 * Date: May 20, 2011
 * Time: 11:10:33 AM
 */
public class SendMessagesBean {

    private static int DEFAULT_MAX_ATTACHMENTS = 8;
    List<String> to;
    String from;
    String subject;
    String mailHostServer;
    String port;
    String password;
    String msgText1;
    String ssl;
    String bccEmail;
    String facebookAccessToken;
    String facebookPage;

    /**
     * Max attachments per message
     */
    int maxAttachments;


    Map<String, WatermarkBean> attachments;

    public SendMessagesBean(List<String> to, String from, String subject, String mailHostServer,
                            String port, String password, String msgText1, Map<String,
            WatermarkBean> attachments, String ssl, String bccEmail, String facebookAccessToken, String facebookPage) {
        this.to = to;
        this.from = from;
        this.subject = subject;
        this.mailHostServer = mailHostServer;
        this.port = port;
        this.password = password;
        this.msgText1 = msgText1;
        this.attachments = attachments;
        this.ssl = ssl;
        this.bccEmail = bccEmail;
        setMaxAttachments(DEFAULT_MAX_ATTACHMENTS);
        this.facebookAccessToken = facebookAccessToken;
        this.facebookPage = facebookPage;

    }

    public SendMessagesBean(List<String> to, String from, String subject, String mailHostServer,
                            String port, String password, String msgText1, Map<String,
            WatermarkBean> attachments, String ssl, int maxAttachments, String bccEmail, String facebookPage) {
        this.to = to;
        this.from = from;
        this.subject = subject;
        this.mailHostServer = mailHostServer;
        this.port = port;
        this.password = password;
        this.msgText1 = msgText1;
        this.attachments = attachments;
        this.ssl = ssl;
        this.maxAttachments = maxAttachments;
        this.bccEmail = bccEmail;
        this.facebookAccessToken = "";
        this.facebookPage = facebookPage;
    }

    public List<String> getTo() {
        return to;
    }

    public void setTo(List<String> to) {
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMailHostServer() {
        return mailHostServer;
    }

    public void setMailHostServer(String mailHostServer) {
        this.mailHostServer = mailHostServer;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMsgText1() {
        return msgText1;
    }

    public void setMsgText1(String msgText1) {
        this.msgText1 = msgText1;
    }

    public Map<String, WatermarkBean> getAttachments() {
        return attachments;
    }

    public void setAttachments(Map<String, WatermarkBean> attachments) {
        this.attachments = attachments;
    }

    public String getSsl() {
        return ssl;
    }

    public void setSsl(String ssl) {
        this.ssl = ssl;
    }

    public int getMaxAttachments() {
        return maxAttachments;
    }

    public void setMaxAttachments(int maxAttachments) {
        this.maxAttachments = maxAttachments;
    }

    public String getBccEmail() {
        return bccEmail;
    }

    public void setBccEmail(String bccEmail) {
        this.bccEmail = bccEmail;
    }

    public String getFacebookAccessToken() {
        return facebookAccessToken;
    }

    public void setFacebookAccessToken(String facebookAccessToken) {
        this.facebookAccessToken = facebookAccessToken;
    }

    public String getFacebookPage() {
        return facebookPage;
    }

    public void setFacebookPage(String facebookPage) {
        this.facebookPage = facebookPage;
    }
}
