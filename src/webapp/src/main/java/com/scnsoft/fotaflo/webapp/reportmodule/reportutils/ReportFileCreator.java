package com.scnsoft.fotaflo.webapp.reportmodule.reportutils;

import com.scnsoft.fotaflo.webapp.reportmodule.model.CameraObj;
import com.scnsoft.fotaflo.webapp.reportmodule.model.LocationObj;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 20.08.2012
 * Time: 10:26:01
 * To change this template use File | Settings | File Templates.
 */
public class ReportFileCreator {
    public void writeTofile(String fileName, Map<String, LocationObj> seObjMap, int nullable, List<String> otherPictures) {
        try {
            BufferedWriter out = new BufferedWriter(new java.io.FileWriter(fileName));
            int total = 0;
            int successfull = 0;
            int unsuccessfullFinaly = 0;
            int unsuccessfullTries = 0;
            int maxUploadTime = 0;
            int maxBeforeNumber = 0;
            int rejected = 0;

            for (String key : seObjMap.keySet()) {
                out.write("=====================================================================================================================================");
                out.newLine();
                out.write("LOCATION: " + key);
                out.newLine();
                LocationObj locationObj = seObjMap.get(key);
                out.write("TOTAL: " + locationObj.getTotal());
                out.newLine();
                out.write("SUCCESSFULL UPLOAD TOTAL: " + locationObj.isSuccessfull());
                out.newLine();
                out.write("UNSUCCESSFULL FINALLY UPLOAD TOTAL: " + locationObj.isUnsucessfull());
                out.newLine();
                out.write("UNSUCCESSFULL TRIES: " + locationObj.getCountUnsuccessfull());
                out.newLine();
                out.write("MAX UPLOAD TIME: " + locationObj.getMaxTimeUpload());
                out.newLine();
                out.write("MIN UPLOAD TIME: " + locationObj.getMinTimeUpload());
                out.newLine();
                int locSize = locationObj.getTotals().size();
                int totLoc = 0;
                for (Integer tot : locationObj.getTotals()) {
                    totLoc += tot.intValue();
                }
                out.write("AVERAGE UPLOAD TIME: " + Math.round(totLoc / locSize));
                out.newLine();
                String errorTy = "";
                if (locationObj.getErrorTypes().contains("SocketTimeoutException")) {
                    errorTy = errorTy + "Connection is lost";
                }
                if (locationObj.getErrorTypes().contains("expectedSize != realSize")) {
                    errorTy = ((errorTy.length() == 0) ? "" : errorTy + "; ") + "Picture is not completely uploaded";
                }
                if (errorTy.length() == 0) {
                    errorTy = "NO ERRORS";
                }
                out.write("ERROR TYPES: " + errorTy);
                 /* out.newLine();
                  out.write("MAX BEFORE NUMBER: " + locationObj.getMaxBeforeNumber());*/
                out.newLine();
                 /* out.write("REJECTED QUANTITY AS ALREADY PROCESSING: " + locationObj.getRejected());
                  out.newLine();*/
                //out.write("ALREADY EXIST: " + locationObj.getAlreadyExist());
                total += locationObj.getTotal();
                successfull += locationObj.isSuccessfull();
                unsuccessfullFinaly += locationObj.isUnsucessfull();
                unsuccessfullTries += locationObj.getCountUnsuccessfull();
                if (maxUploadTime < locationObj.getMaxTimeUpload()) {
                    maxUploadTime = locationObj.getMaxTimeUpload();
                }
                if (maxBeforeNumber < locationObj.getMaxBeforeNumber()) {
                    maxBeforeNumber = locationObj.getMaxBeforeNumber();
                }
                rejected += locationObj.getRejected();


                Map<String, CameraObj> cameras = locationObj.getCameras();
                for (String keyCamera : cameras.keySet()) {
                    out.write("--------------------------------");
                    out.newLine();
                    out.write("----CAMERA: " + keyCamera);
                    out.newLine();
                    CameraObj cameraObj = cameras.get(keyCamera);
                    out.write("----TOTAL: " + cameraObj.getTotal());
                    out.newLine();
                    out.write("----SUCCESSFULL UPLOAD TOTAL: " + cameraObj.isSuccessfull());
                    out.newLine();
                    out.write("----UNSUCCESSFULL FINALLY UPLOAD TOTAL: " + cameraObj.isUnsucessfull());
                    out.newLine();
                    out.write("----UNSUCCESSFULL TRIES: " + cameraObj.getCountUnsuccessfull());
                    out.newLine();
                    out.write("----MAX UPLOAD TIME: " + cameraObj.getMaxTimeUpload());
                    out.newLine();
                    out.write("----MIN UPLOAD TIME: " + cameraObj.getMinTimeUpload());
                    out.newLine();

                    int camSize = cameraObj.getTotals().size();
                    int totCam = 0;
                    for (Integer tot : cameraObj.getTotals()) {
                        totCam += tot.intValue();
                    }
                    out.write("----AVERAGE UPLOAD TIME: " + Math.round(totCam / camSize));
                    out.newLine();
                    String errorTyCam = "";
                    if (cameraObj.getErrorTypes().contains("SocketTimeoutException")) {
                        errorTyCam = errorTyCam + "Connection is lost";
                    }
                    if (cameraObj.getErrorTypes().contains("expectedSize != realSize")) {
                        errorTyCam = ((errorTyCam.length() == 0) ? "" : errorTyCam + "; ") + "Picture is not completely uploaded";
                    }
                    if (errorTyCam.length() == 0) {
                        errorTyCam = "NO ERRORS";
                    }
                    out.write("----ERROR TYPES: " + errorTyCam);
                    /*  out.newLine();
                      out.write("----MAX PICTURES UPLOADING AT THE SAME TIME: " + cameraObj.getMaxBeforeNumber());*/
                    out.newLine();
                     /* out.write("----REJECTED QUANTITY AS ALREADY PROCESSING: " + cameraObj.getRejected());
                      out.newLine();*/
                    //out.write("----ALREADY EXIST: " + cameraObj.getAlreadyExist());

                }


            }
            out.write("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
            out.newLine();
            out.write("GENARAL INFO: ");
            out.newLine();
            out.write("TOTAL: " + total);
            out.newLine();
            out.write("SUCCESSFULL UPLOAD TOTAL: " + successfull);
            out.newLine();
            out.write("UNSUCCESSFULL FINALLY UPLOAD TOTAL: " + unsuccessfullFinaly);
            out.newLine();
            out.write("UNSUCCESSFULL TRIES: " + unsuccessfullTries);
            out.newLine();
            out.write("MAX UPLOAD TIME: " + maxUploadTime);
    /*          out.newLine();
              out.write("MAX BEFORE NUMBER: " + maxBeforeNumber);*/
            out.newLine();
              /*out.write("REJECTED QUANTITY AS ALREADY PROCESSING: " + rejected);
              out.newLine();*/
            out.write("0-SIZE PICTURES SENT (they were deleted from the device by user while sending): " + nullable);
            out.newLine();
            out.write("NUMBER OF PICTURES FROM OTHER DAYS: " + otherPictures.size());
            out.newLine();
              /*for(String pict :otherPictures){
                  out.write(pict);
                  out.newLine();
              }*/
            out.write("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
            out.newLine();
            out.close();
        } catch (IOException e) {
        }
    }

}
