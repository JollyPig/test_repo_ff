package com.scnsoft.fotaflo.webapp.common;

import java.util.Collection;

/**
 * Collection response for ExtJS request. Return collection object with type T.
 *
 * @author Anatoly Selitsky
 */
public class CollectionResponse<T> extends Response<Collection<T>>{

    public CollectionResponse() {
    }

    public CollectionResponse(Collection<T> children) {
        super(children);
    }

    public CollectionResponse(Collection<T> children, boolean success) {
        super(children, success);
    }

}
