package com.scnsoft.fotaflo.webapp.service;

import com.scnsoft.fotaflo.webapp.bean.PictureBean;
import com.scnsoft.fotaflo.webapp.bean.WatermarkBean;
import com.scnsoft.fotaflo.webapp.model.Camera;
import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.util.ImageWatermark;
import com.scnsoft.fotaflo.webapp.util.Resolution;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.sanselan.ImageReadException;
import org.apache.sanselan.ImageWriteException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;

@Service
public class ImageService {
    protected static Logger logger = Logger.getLogger(ImageService.class);

    @Autowired
    private PictureService pictureService;

    @Autowired
    private MailService mailService;

    @Autowired
    private CameraService cameraService;

    public String createExportImage(PictureBean bean, Boolean isFullPath)
            throws IOException, ImageWriteException, ImageReadException {
        String picturePath = pictureService.getPicturePath();
        File srcImage = new File(isFullPath ? bean.getUrl() : picturePath + File.separator + bean.getUrl());
        Camera camera = cameraService.getCamera(Integer.valueOf(bean.getCameraId()));
        Location location = camera != null ? camera.getLocation() : null;
        String tmpPath = mailService.getTmpImagePath(bean, false, false, (location == null || location.getResolution() == null || location.getResolution().equals("")) ? Resolution.STANDARD.getCode() : location.getResolution());
        File tmpImage = new File(tmpPath);
        tmpImage.createNewFile();
        if (!srcImage.getAbsolutePath().equals(tmpImage.getAbsolutePath())) {
            FileUtils.copyFile(srcImage, tmpImage);
        }

        ImageWatermark.applyMetadata(tmpImage, camera.getLocation().getImageMetadata());

        return tmpPath;
    }

    public String createPurchaseImage(WatermarkBean bean)
            throws ImageWriteException, IOException, ImageReadException {
        String path = ImageWatermark.addLogoToImage(bean.getRotated(), bean.getImg(), bean.getLogoImg1(), bean.getLogoImg2(), bean.getLogoText(),
                bean.getOutput(), bean.isReduced() ? "reduced" : "high", bean.getWatermark(), bean.getResolution(), bean.getMetadata());

        return path;
    }
}
