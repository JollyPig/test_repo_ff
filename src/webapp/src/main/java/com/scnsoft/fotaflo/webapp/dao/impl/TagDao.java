package com.scnsoft.fotaflo.webapp.dao.impl;

import com.scnsoft.fotaflo.common.util.StringUtils;
import com.scnsoft.fotaflo.webapp.dao.ITagDao;
import com.scnsoft.fotaflo.webapp.model.Camera;
import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.model.Tag;
import com.scnsoft.fotaflo.webapp.model.TagPurchase;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 12.10.12
 * Time: 5:31
 * To change this template use File | Settings | File Templates.
 */
public class TagDao extends BaseDao implements ITagDao {

    public List<Tag> getTagsByLocation(Location location, Date start, Date end) {
        List<Tag> list1 = null;
        StringBuffer query = new StringBuffer();
        query.append("select  tag from Tag as tag where ");
        query.append("tag.creationDate >= :start ");
        query.append("and tag.creationDate <= :end ");
        if (location != null) {
            query.append("and tag.location = :location ");
            list1 = getSession().createQuery(query.toString())
                    .setParameter("start", start)
                    .setParameter("end", end)
                    .setParameter("location", location)
                    .list();
        } else {
            list1 = getSession().createQuery(query.toString())
                    .setParameter("start", start)
                    .setParameter("end", end)
                    .list();
        }

        return list1;
    }

    public List<Tag> getTagsByUser(Integer[] usertagsIds, Date start, Date end) {
        StringBuffer query = new StringBuffer();
        query.append("select tag from Tag as tag where ");
        query.append("tag.creationDate >= :start ");
        //query.append("and tag.creationDate <= :end ");

        if (usertagsIds.length != 0) {
            query.append("and tag.id in ( :usertagsids ) ");
        } else {
            return new ArrayList<Tag>();
        }
        query.append("order by tag.creationDate desc");

        return getSession().createQuery(query.toString())
                .setParameter("start", start)
                        //.setParameter("end", end)
                .setParameterList("usertagsids", usertagsIds).list();

    }

    public Tag getTagsByUserAndName(Integer[] usertagsIds, String name) {
        StringBuffer query = new StringBuffer();
        query.append("select tag from Tag as tag where ");
        query.append("tag.tagName = :name ");
        if (usertagsIds.length != 0) {
            query.append("and tag.id in ( :usertagsids )");
        } else {
            return null;
        }

        List<Tag> tags = getSession().createQuery(query.toString())
                .setParameter("name", name)
                        //.setParameter("end", end)
                .setParameterList("usertagsids", usertagsIds).list();
        if (tags.size() != 0) {
            return tags.get(0);
        }
        return null;

    }

    private String parseList(List list) {
        StringBuffer result = new StringBuffer();
        for (int i = 0; i < list.size(); i++) {
            if (i != 0)
                result.append(", ");
            result.append(((Camera) list.get(i)).getId());
        }
        return result.toString();
    }

    public List<Tag> getTagsByLocation(Location location) {
        List<Tag> list1 = null;
        StringBuffer query = new StringBuffer();
        query.append("select  tag from Tag as tag where ");

        if (location != null) {
            query.append("tag.location = :location ");
            list1 = getSession().createQuery(query.toString())
                    .setParameter("location", location)
                    .list();
        }

        return list1;
    }


    public Tag getTagByNameLocation(String tag, Location location) {

        StringBuffer query = new StringBuffer();
        query.append("from Tag as tag where ");
        query.append("tag.tagName = :tagName ");
        query.append("and tag.location = :location ");

        List result = getSession().createQuery(query.toString())
                .setParameter("location", location)
                .setParameter("tagName", tag)
                .list();

        if (!result.isEmpty()) {
            return (Tag) result.get(0);
        }

        return null;

    }

    public TagPurchase getTagPurchaseByCode(String code) {

        StringBuffer query = new StringBuffer();
        query.append("from TagPurchase as tag where ");
        query.append("tag.generatedCode = :generatedCode ");


        List result = getSession().createQuery(query.toString())
                .setParameter("generatedCode", code)
                .list();

        if (!result.isEmpty()) {
            return (TagPurchase) result.get(0);
        }

        return null;

    }

    public List<TagPurchase> getTagPurchaseByDatesLocation(Date startDate, Date endDate, Location location) {

        StringBuffer query = new StringBuffer();
        query.append("select tagPurchase from TagPurchase as tagPurchase where ");
        query.append("tagPurchase.purchaseDate >= :start ");
        query.append("and tagPurchase.purchaseDate <= :end ");
        query.append("and tagPurchase.locationPurchase = :location ");


        List<TagPurchase> list1 = getSession().createQuery(query.toString())
                .setParameter("start", startDate)
                .setParameter("end", endDate)
                .setParameter("location", location)
                .list();
        return list1;
    }

    public List<TagPurchase> getTagPurchaseByDatesLocationStatus(Date startDate, Date endDate, Location location, int status) {
        StringBuffer query = new StringBuffer();
        query.append("select tagPurchase from TagPurchase as tagPurchase where ");
        query.append("tagPurchase.purchaseDate >= :start ");
        query.append("and tagPurchase.purchaseDate <= :end ");
        query.append("and tagPurchase.locationPurchase = :location ");
        query.append("and tagPurchase.statusCode = :statusCode ");


        List<TagPurchase> list1 = getSession().createQuery(query.toString())
                .setParameter("start", startDate)
                .setParameter("end", endDate)
                .setParameter("location", location)
                .setParameter("statusCode", status)
                .list();
        return list1;

    }

    @SuppressWarnings ("unchecked")
    public List<TagPurchase> getPurchaseByFilter(Location location, Date startDate, Date endDate, String email, String code, int status, String tag) {
        List<TagPurchase> result = null;

        Criteria criteria = getSession().createCriteria(TagPurchase.class);

        Criterion criterion = Restrictions.and(
                Restrictions.eq("statusCode", status),
                Restrictions.and(
                        Restrictions.ge("purchaseDate", startDate),
                        Restrictions.le("purchaseDate", endDate)
                )
        );

        if (location != null) {
            criteria.createCriteria("locationPurchase").add(Restrictions.eq("id", location.getId()));
        }

        if(StringUtils.isNotEmpty (email)){
            criterion = Restrictions.and(
                    criterion,
                    Restrictions.ilike("participantsEmails", '%' + email + '%')
            );
        }

        if(StringUtils.isNotEmpty (code)){
            criterion = Restrictions.and(
                    criterion,
                    Restrictions.like("generatedCode", '%' + code + '%')
            );
        }

        if(StringUtils.isNotEmpty (tag)){
            criteria.createAlias ("tags", "tag");

            criterion = Restrictions.and(
                    criterion,
                    Restrictions.like("tag.tagName", '%' + tag + '%')
            );
        }

        criteria.add(criterion);
        criteria.addOrder(Order.asc("id"));

        return criteria.list ();
    }

}

