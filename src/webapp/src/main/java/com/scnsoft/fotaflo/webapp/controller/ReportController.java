package com.scnsoft.fotaflo.webapp.controller;


import com.scnsoft.fotaflo.common.util.DateFormatUtil;
import com.scnsoft.fotaflo.webapp.service.ReportService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */
@Controller
public class ReportController {
    protected static Logger logger = Logger.getLogger(ReportController.class);

    private final static String successProperty = "success";
    private final static String msgProperty = "msg";

    @Autowired
    private ReportService reportService;

    @RequestMapping("reportPathUpdate.json")
    public
    @ResponseBody
    Map<String, Object> updateReportPath(
            @RequestParam("reportPath") String reportPath) {
        Map<String, Object> modelMap = createModelMap(true);
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        if (new File(reportPath).isDirectory()) {
            reportService.saveReportPath(reportPath);
            modelMap.put(msgProperty, "Report Path was updated successfully");
            logger.info("User [ " + currentUser + " ]" + " saved report path");
        } else {
            modelMap.put(msgProperty, "Report Path was not updated. Please indicate the path correctly Example(D:\\Temp\\)");
            logger.error("User [ " + currentUser + " ]" + " report path was not saved and default will be used (D Temp)");
        }

        return modelMap;
    }

    @RequestMapping("getReportPath.json")
    public
    @ResponseBody
    Map<String, Object> getReportPath() {

        Map<String, Object> modelMap = createModelMap(true);
        String path = reportService.getReportPath();
        modelMap.put("reportPath", path != null ? path : "");
        modelMap.put(msgProperty, "Report path was taken successfully");

        return modelMap;
    }

    @RequestMapping("getReportEmails.json")
    public
    @ResponseBody
    Map<String, Object> getReportEmails(@RequestParam(value = "locationid", required = true) String locationId) {

        Map<String, Object> modelMap = createModelMap(true);
        List<String> mails = reportService.getLocationEmails(Integer.valueOf(locationId));
        for (int i = 1; i < 4; i++) {
            if (i <= mails.size()) {
                modelMap.put("email" + i, mails.get(i - 1));
            } else {
                modelMap.put("email" + i, "");
            }
        }
        List<String> mailStrings = reportService.getLocationStrings(Integer.valueOf(locationId));
        modelMap.put("emailString1", mailStrings.get(0) != null ? mailStrings.get(0) : "");
        modelMap.put("emailString2", mailStrings.get(1) != null ? mailStrings.get(1) : "");
        modelMap.put("emailString3", mailStrings.get(2) != null ? mailStrings.get(2) : "");
        modelMap.put(msgProperty, "Report Location emails were sent successfully");

        return modelMap;
    }

    @RequestMapping("getDailyReportEmails.json")
    public
    @ResponseBody
    Map<String, Object> getDailyReportEmails(@RequestParam(value = "locationid", required = true) String locationId) {

        Map<String, Object> modelMap = createModelMap(true);
        List<String> mails = reportService.getLocationDailyEmails(Integer.valueOf(locationId));
        for (int i = 1; i < 4; i++) {
            if (i <= mails.size()) {
                modelMap.put("email" + i, mails.get(i - 1));
            } else {
                modelMap.put("email" + i, "");
            }
        }
        List<String> mailStrings = reportService.getLocationStrings(Integer.valueOf(locationId));
        modelMap.put("emailString1", mailStrings.get(0) != null ? mailStrings.get(0) : "");
        modelMap.put("emailString2", mailStrings.get(1) != null ? mailStrings.get(1) : "");
        modelMap.put("emailString3", mailStrings.get(2) != null ? mailStrings.get(2) : "");
        modelMap.put(msgProperty, "Report Location emails were sent successfully");

        return modelMap;
    }

    @RequestMapping("getEmailFooter.json")
    public
    @ResponseBody
    Map<String, Object> getEmailFooter(@RequestParam(value = "locationid", required = true) String locationId) {
        Map<String, Object> modelMap = createModelMap(true);
        String area = reportService.getLocationFooter(Integer.valueOf(locationId));
        modelMap.put("area", area);
        modelMap.put(msgProperty, "Email footer was taken for location" + locationId);
        return modelMap;
    }

    @RequestMapping("getPublicLoginEmailContent.json")
    public
    @ResponseBody
    Map<String, Object> getPublicLoginEmailContent(@RequestParam(value = "locationid", required = true) String locationId) {
        Map<String, Object> modelMap = createModelMap(true);
        String area = reportService.getLocationContent(Integer.valueOf(locationId));
        modelMap.put("area", area);
        modelMap.put(msgProperty, "Public login email content was get successfully for location" + locationId);
        return modelMap;
    }

    @RequestMapping("reportSettingsSave.json")
    public @ResponseBody Map<String, Object> saveReportSettings(
            @RequestParam("id") String locationId, @RequestParam("reportEmail1") String reportEmail1,
            @RequestParam("reportEmail2") String reportEmail2, @RequestParam("reportEmail3") String reportEmail3,
            @RequestParam("reportString1") String reportString1, @RequestParam("reportString2") String reportString2,
            @RequestParam("reportString3") String reportString3) {
        Map<String, Object> modelMap = createModelMap(true);
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        if (locationId != null) {
            reportService.saveReportSettings(Integer.valueOf(locationId), reportEmail1, reportEmail2, reportEmail3,
                    reportString1, reportString2, reportString3);
            modelMap.put(msgProperty, "Report Settings were updated successfully");
            logger.info("User [ " + currentUser + " ]" + " saved report settings for location" + locationId);
        } else {
            modelMap.put(msgProperty, "Report Settings were not updated.");
            logger.error("User [ " + currentUser + " ]" + " was not able to saved report settings");
        }
        return modelMap;
    }

    @RequestMapping("reportDailySettingsSave.json")
    public @ResponseBody Map<String, Object> saveDailyReportSettings(
            @RequestParam("id") String locationId, @RequestParam("reportDailyEmail1") String reportEmail1,
            @RequestParam("reportDailyEmail2") String reportEmail2, @RequestParam("reportDailyEmail3") String reportEmail3) {
        Map<String, Object> modelMap = createModelMap(true);
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        if (locationId != null) {
            reportService.saveDailyReportSettings(Integer.valueOf(locationId), reportEmail1, reportEmail2, reportEmail3);
            modelMap.put(msgProperty, "Report Settings were updated successfully");
            logger.info("User [ " + currentUser + " ]" + " saved daily report settings for location " + locationId);
        } else {
            modelMap.put(msgProperty, "Report Settings were not updated.");
            logger.info("User [ " + currentUser + " ]" + " was not able to saved daily report settings");
        }
        return modelMap;
    }

    @RequestMapping("reportEmailSettingsSave.json")
    public @ResponseBody Map<String, Object> reportEmailSettingsSave(
            @RequestParam("smtpServer") String smtpServer,
            @RequestParam("fromEmail") String fromEmail, @RequestParam("subjectText") String subjectText,
            @RequestParam("smtpServerPort") String smtpServerPort, @RequestParam("smtpServerFromPassword") String smtpServerFromPassword,
            @RequestParam(value = "sslcheck", required = false) String sslcheck) {
        Map<String, Object> modelMap = createModelMap(true);
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        reportService.saveEmailReportSettings(smtpServer, smtpServerPort, fromEmail, smtpServerFromPassword, subjectText, sslcheck);
        modelMap.put(msgProperty, "Report Settings were updated successfully");
        logger.info("User [ " + currentUser + " ]" + " saved report email settings");
        return modelMap;
    }

    @RequestMapping("reportEmailSettingsDelete.json")
    public @ResponseBody Map<String, Object> reportEmailSettingsDelete() {
        Map<String, Object> modelMap = createModelMap(true);
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        reportService.deleteEmailReportSettings();
        modelMap.put(msgProperty, "Report Settings were deleted successfully");
        logger.info("User [ " + currentUser + " ]" + "deleted report email settings");
        return modelMap;
    }

    @RequestMapping("getEmailReportSettings.json")
    public @ResponseBody Map<String, Object> getEmailReportSettings() {
        Map<String, Object> sets = reportService.getEmailReportSettings();
        return sets;
    }

    @RequestMapping("emailFooterSave.json")
    public @ResponseBody Map<String, Object> saveEmailFooter(
            @RequestParam("id") String locationId, @RequestParam("area") String area) {
        Map<String, Object> modelMap = createModelMap(true);
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        if (locationId != null) {
            reportService.saveEmailFooter(Integer.valueOf(locationId), area);
            modelMap.put(msgProperty, "Report Email Footer was updated successfully");
            logger.info("User [ " + currentUser + " ]" + "updated report Report Email Footer for location " + locationId);
        } else {
            modelMap.put(msgProperty, "Report Email Footer was not updated.");
            logger.warn("User [ " + currentUser + " ]" + "didn't update report Report Email Footer");

        }
        return modelMap;
    }

    @RequestMapping("publicEmailContentSave.json")
    public @ResponseBody Map<String, Object> saveEmailPublicContent(
            @RequestParam("id") String locationId, @RequestParam("area") String area) {
        Map<String, Object> modelMap = createModelMap(true);
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        if (locationId != null) {
            reportService.saveEmailPublicContent(Integer.valueOf(locationId), area);
            modelMap.put(msgProperty, "Public Email Content was updated successfully");
            logger.info("User [ " + currentUser + " ]" + "updated Public Email Content  for location " + locationId);
        } else {
            modelMap.put(msgProperty, "Public Email Content was not updated.");
            logger.warn("User [ " + currentUser + " ]" + "didn't update Public Email Content");
        }
        return modelMap;
    }

    @RequestMapping(value = "generateReport.json", method = RequestMethod.GET)
    public void generateReport(HttpServletResponse resp, HttpServletRequest request,
            @RequestParam("type") String type,
            @RequestParam("locationId") Integer locationId,
            @RequestParam(value = "fullDate", required = false) @DateTimeFormat(pattern = DateFormatUtil.ISO_DATETIME_FORMAT_PATTERN) Date date){
        if(type.equalsIgnoreCase("public")){
            try {
                resp.sendRedirect("exportPublicReports.json" + "?" + request.getQueryString());
            }catch (IOException e){
                logger.error(e.getMessage());
            }
        }else{
            resp.addHeader("Content-Type", "application/vnd.ms-excel");

            if(date == null){
                date = new Date();
            }

            String reportName = reportService.generateStatisticReportName(date, locationId, type);
            resp.setHeader("Content-Disposition", "attachment; filename=" + reportName);

            try {
                reportService.generateStatisticReport(date, locationId, resp.getOutputStream(), type);
            }catch (IOException e){
                logger.error(e.getMessage());
            }
        }
    }

    private Map<String, Object> createModelMap(Boolean success) {
        Map<String, Object> modelMap = new HashMap<String, Object>();
        modelMap.put(successProperty, success);
        return modelMap;
    }
}