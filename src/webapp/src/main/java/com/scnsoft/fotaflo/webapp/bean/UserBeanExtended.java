package com.scnsoft.fotaflo.webapp.bean;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 02.05.14
 * Time: 18:07
 * To change this template use File | Settings | File Templates.
 */
public class UserBeanExtended extends UserBean {

    String userCurrency;
    String userPrice;
    String userPackagePrice;

    public UserBeanExtended(int id, String loginName, String password, String firstName, String lastName, String locationId, String location, String access, String currency, String price, String packagePrice) {
        this.id = id;
        this.loginName = loginName;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.locationId = locationId;
        this.location = location;
        this.access = access;
        this.userCurrency = currency;
        this.userPrice = price;
        this.userPackagePrice = packagePrice;
    }

    public String getUserCurrency() {
        return userCurrency;
    }

    public void setUserCurrency(String userCurrency) {
        this.userCurrency = userCurrency;
    }

    public String getUserPrice() {
        return userPrice;
    }

    public void setUserPrice(String userPrice) {
        this.userPrice = userPrice;
    }

    public String getUserPackagePrice() {
        return userPackagePrice;
    }

    public void setUserPackagePrice(String userPackagePrice) {
        this.userPackagePrice = userPackagePrice;
    }
}
