package com.scnsoft.fotaflo.webapp.dao.impl;

import com.scnsoft.fotaflo.webapp.dao.ICameraDao;
import com.scnsoft.fotaflo.webapp.model.Camera;
import com.scnsoft.fotaflo.webapp.model.Location;

import java.util.List;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */
public class CameraDao extends BaseDao implements ICameraDao {

    public List<Camera> getCamerasByLocation(Location location) {
        return getHibernateTemplate().find(
                "from Camera camera where camera.location = ?", location);
    }

    public Camera getCamera(Integer cameraId) {
        Camera camera = (Camera) getSession().load(Camera.class, cameraId);
        return camera;

    }
}
