package com.scnsoft.fotaflo.webapp.controller;

import com.scnsoft.fotaflo.common.util.StringUtils;
import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.model.Merchant;
import com.scnsoft.fotaflo.webapp.model.PayPalLocationSettings;
import com.scnsoft.fotaflo.webapp.service.LocationService;
import com.scnsoft.fotaflo.webapp.service.PayPalService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 13.02.2012
 * Time: 17:25:32
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class PayPalGuiController {

    protected static Logger logger = Logger.getLogger(PayPalGuiController.class);
    private final static String successProperty = "success";
    private final static String msgProperty = "msg";

    @Autowired
    private LocationService locationService;

    @Autowired
    private PayPalService payPalService;

    @RequestMapping("getPayPalSettings.json")
    public
    @ResponseBody
    Map<String, Object> getPayPalSettings (@RequestParam ("locationId") Integer locationId) {
        Map<String, Object> modelMap = createModelMap (true);
        if (locationId != null) {
            final Location locationEntity = locationService.getLocation (locationId);

            if (locationEntity != null) {
                final PayPalLocationSettings locationSettingsEntity = locationEntity.getPaypallocationSettings ();

                if (locationSettingsEntity == null) {
                    modelMap.put ("currency", "");
                    modelMap.put ("price", "");
                }
                else {
                    modelMap.put ("currency", StringUtils.nullToNothing (locationSettingsEntity.getCurrency ()));
                    modelMap.put ("price", StringUtils.nullToNothing (locationSettingsEntity.getPrice ()));

                    if (locationEntity.getRfid ()) {
                        modelMap.put ("packagePrice", StringUtils.nullToNothing(locationSettingsEntity.getPackagePrice()));
                    }
                }
                modelMap.put(msgProperty, "Settings was successfully got");
            }
            else {
                modelMap.put (msgProperty, StringUtils.concat ("Location with locationId = ", locationId.toString (), " not found!"));
            }
        }
        else {
            modelMap.put (msgProperty, StringUtils.concat ("LocationId can't be null!"));
        }

        return modelMap;
    }

    @RequestMapping("getPayPalMerchantSettings.json")
    public
    @ResponseBody
    Map<String, Object> getPayPalMerchantSettings() {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        Map<String, Object> modelMap = createModelMap(true);
        Merchant merchant = null;
        try {
            merchant = payPalService.getMerchantAccount();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        if (merchant == null) {
            modelMap.put("account", "");
            modelMap.put("password", "");
            modelMap.put("signature", "");
            modelMap.put("environment", "");
        } else {
            modelMap.put("account", merchant.getAccount() != null ? merchant.getAccount() : "");
            modelMap.put("password", merchant.getPassword() != null ? merchant.getPassword() : "");
            modelMap.put("signature", merchant.getSignature() != null ? merchant.getSignature() : "");
            modelMap.put("environment", merchant.getEnvironment() != null ? merchant.getEnvironment() : "");
        }
        modelMap.put(msgProperty, "Settings was successfully got");
        logger.info("User [ " + currentUser + " ]" + " requested paypal merchant settings");

        return modelMap;
    }

    @RequestMapping("saveMerchantAccountSettings.json")
    public
    @ResponseBody
    Map<String, Object> saveSettings(@RequestParam("account") String account,
                                     @RequestParam("password") String password,
                                     @RequestParam("signature") String signature,
                                     @RequestParam("environment") String environment) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        Map<String, Object> modelMap;
        Merchant merchant = null;
        try {
            merchant = payPalService.saveMerchantAccount(account, password, signature, environment);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        if (merchant != null) {
            modelMap = createModelMap(true);
            modelMap.put(msgProperty, "Settings were successfully updated");
            logger.info("User [ " + currentUser + " ]" + " saved paypal merchant settings");
        } else {
            modelMap = createModelMap(false);
            modelMap.put(msgProperty, "Settings were not successfully updated");
            logger.error("User [ " + currentUser + " ]" + "  paypal merchant settings  were not successfully updated");
        }
        return modelMap;
    }

    @RequestMapping("getMerchantSettings.json")
    public
    @ResponseBody
    Map<String, Object> getMerchantSettings() {

        //Map<String, Object> modelMap = createModelMap(true);
        Map<String, Object> sets = payPalService.getMerchantSettings();
        return sets;
    }

    @RequestMapping("saveAccountSettings.json")
    public
    @ResponseBody
    Map<String, Object> saveAccountSettings(@RequestParam("locationId") String locId,
                                     @RequestParam("currency") String currency,
                                     @RequestParam("price") String price,
                                     @RequestParam(value = "packagePrice", required = false) String packagePrice) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        Map<String, Object> modelMap;
        if (locId != null && !locId.equals("")) {
            final Integer locationId = Integer.valueOf (locId);
            final Location locationEntity = locationService.getLocation (locationId);

            if (locationEntity != null ) {
                if (locationEntity.getRfid () != null && locationEntity.getRfid () && (StringUtils.isEmpty (packagePrice) || ! packagePrice.matches ("^\\+?[\\d]*$"))) {
                    modelMap = createModelMap (false);
                    modelMap.put (msgProperty, "The field \"Package price\" is required and must contain only numbers.");
                }
                else {
                    PayPalLocationSettings payPalLocationSettings = locationEntity.getPaypallocationSettings ();

                    if (payPalLocationSettings == null) {
                        payPalLocationSettings = new PayPalLocationSettings ();
                    }
                    payPalLocationSettings.setPrice(price);
                    payPalLocationSettings.setCurrency(currency);
                    payPalLocationSettings.setPackagePrice (packagePrice);
                    locationService.updateLocationPayPalSettings(locationId, payPalLocationSettings);

                    modelMap = createModelMap (true);
                    modelMap.put (msgProperty, "Settings were successfully updated");
                    logger.info ("User [ " + currentUser + " ]" + " saved account settings");
                }
            }
            else {
                modelMap = createModelMap (false);
                modelMap.put (msgProperty, StringUtils.concat ("Location with id = \"", locId, "\" not found!"));
            }
        }
        else {
            modelMap = createModelMap(false);
            modelMap.put(msgProperty, "Settings were not successfully updated");
            logger.error("User [ " + currentUser + " ]" + "  paypal account settings  were not successfully updated");
        }

        return modelMap;
    }


    private Map<String, Object> createModelMap(Boolean success) {
        Map<String, Object> modelMap = new HashMap<String, Object>();
        modelMap.put(successProperty, success);
        return modelMap;
    }

}
