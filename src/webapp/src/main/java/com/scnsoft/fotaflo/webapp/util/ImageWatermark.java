package com.scnsoft.fotaflo.webapp.util;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */
import com.scnsoft.fotaflo.common.util.StringUtils;
import com.scnsoft.fotaflo.webapp.model.LocationImageMetadata;
import org.apache.log4j.Logger;
import org.apache.sanselan.ImageReadException;
import org.apache.sanselan.ImageWriteException;
import org.apache.sanselan.Sanselan;
import org.apache.sanselan.formats.jpeg.JpegImageMetadata;
import org.apache.sanselan.formats.jpeg.exifRewrite.ExifRewriter;
import org.apache.sanselan.formats.tiff.TiffImageMetadata;
import org.apache.sanselan.formats.tiff.constants.TagInfo;
import org.apache.sanselan.formats.tiff.constants.TiffConstants;
import org.apache.sanselan.formats.tiff.write.TiffOutputDirectory;
import org.apache.sanselan.formats.tiff.write.TiffOutputField;
import org.apache.sanselan.formats.tiff.write.TiffOutputSet;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;

/**
 * The tool to adding watermark to images,recommended markImage () methods to create watermark image.
 */
public class ImageWatermark {
    protected static Logger logger = Logger.getLogger(ImageWatermark.class);

    private static final int OFFSET_X = 10;
    private static final int OFFSET_Y = 10;

    public static final int MARK_LEFT_TOP = 1;
    public static final int MARK_RIGHT_TOP = 2;
    public static final int MARK_CENTER = 3;
    public static final int MARK_LEFT_BOTTOM = 4;
    public static final int MARK_RIGHT_BOTTOM = 5;

    /**
     * A logo picture to picture and watermark, effective this way
     * one instance : http://www.mvgod.com/images/poster/NaNiYaChuanQi2XKaiSiBinWangZi-10285-12811-19978-13383/3.jpg
     *
     * @param srcImg        --
     *                      source image
     * @param logo1         --
     *                      watermark logo image1
     * @param logo1Position --
     *                      watermark logo1 position
     * @param logo2         --
     *                      watermark logo image2
     * @param logo2Position --
     *                      watermark logo2 position
     * @param text          --
     *                      text
     */
    public final static BufferedImage markWithImageAndLogo(boolean rotated, String srcImg, String logo1, int logo1Position, String logo2, int logo2Position, String text, String waterMark, String pictureSize, String resolution) {
        int defaultWidth = rotated ? DefaultValues.pictureHeight : DefaultValues.pictureWidth;
        int defaultHeight = rotated ? DefaultValues.pictureWidth : DefaultValues.pictureHeight;

        int pictureWidth = defaultWidth;
        int pictureHeight = defaultHeight;

        if (resolution != null && !resolution.equals("")) {
            Resolution res = DefaultValues.findByCode(resolution);
            pictureWidth = !rotated ? res.getWidth() : res.getHeight();
            pictureHeight = !rotated ? res.getHeight() : res.getWidth();
        }

        int fontSize = DefaultValues.fontSize;
        float scale = 1;

        if ((waterMark != null && waterMark.length() > 0)) {
            pictureWidth = defaultWidth / 2;
            pictureHeight = defaultHeight / 2;
            fontSize = DefaultValues.fontSize / 2;
            scale = 2;
        } else {
            if (pictureSize != null && pictureSize.equals("reduced") && resolution.equals(Resolution.REDUCED.getCode())) {
                fontSize = DefaultValues.fontSizeReduced;
                scale = 1.33f;
            }
        }
        try {
            File _file = new File(srcImg);
            if (!_file.exists()) return null;

            Image src = ImageIO.read(_file);
            int widthLocal = src.getWidth(null);
            int heightLocal = src.getHeight(null);
            logger.info("widthLocal: " + widthLocal);
            logger.info("heightLocal: " + heightLocal);
            double ratio = (double) widthLocal / (double) heightLocal;

            if (heightLocal > widthLocal) {
                ratio = (double) heightLocal / (double) widthLocal;
            }
            logger.info("ratio: " + ratio);
            if (ratio == (double) 16 / (double) 9) {
                if (rotated) {

                    pictureWidth = Resolution.X16X9.getHeight();
                    pictureHeight = Resolution.X16X9.getWidth();
                    logger.info("rotated!!: " + pictureWidth + "  " + pictureHeight);
                } else {
                    pictureWidth = Resolution.X16X9.getWidth();
                    pictureHeight = Resolution.X16X9.getHeight();
                    logger.info("not rotated!!: " + pictureWidth + "  " + pictureHeight);
                }
                fontSize = DefaultValues.fontSize;
                scale = 1;
            }
            src = src.getScaledInstance(pictureWidth, pictureHeight, 1);
            int width = src.getWidth(null);
            int height = src.getHeight(null);
            BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = image.createGraphics();
            g.drawImage(src, 0, 0, width, height, null);
            int logo1Width = 0;
            int logo2Width = 0;

            // watermark image file logo1
            if (logo1 != null) {
                File markFile1 = new File(logo1);
                if (markFile1.exists() && !markFile1.isDirectory()) {

                    Image mark_img1 = ImageIO.read(markFile1);
                    int mark_img_width1 = Math.round(mark_img1.getWidth(null) / scale);
                    int mark_img_height1 = Math.round(mark_img1.getHeight(null) / scale);

                    logo1Width = mark_img_width1;
                    g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, DefaultValues.opacity));
                    switch (logo1Position) {
                        case ImageWatermark.MARK_LEFT_TOP:
                            g.drawImage(mark_img1, OFFSET_X, OFFSET_Y, mark_img_width1, mark_img_height1, null);
                            break;
                        case ImageWatermark.MARK_LEFT_BOTTOM:
                            g.drawImage(mark_img1, OFFSET_X, (height - mark_img_height1 - OFFSET_Y), mark_img_width1, mark_img_height1, null);
                            break;
                        case ImageWatermark.MARK_CENTER:
                            g.drawImage(mark_img1, (width - mark_img_width1 - OFFSET_X) / 2, (height - mark_img_height1 - OFFSET_Y) / 2,
                                    mark_img_width1, mark_img_height1, null);
                            break;
                        case ImageWatermark.MARK_RIGHT_TOP:
                            g.drawImage(mark_img1, (width - mark_img_width1 - OFFSET_X), OFFSET_Y, mark_img_width1, mark_img_height1, null);
                            break;
                        case ImageWatermark.MARK_RIGHT_BOTTOM:
                        default:
                            g.drawImage(mark_img1, (width - mark_img_width1 - OFFSET_X), (height - mark_img_height1 - OFFSET_Y),
                                    mark_img_width1, mark_img_height1, null);
                    }
                }
            }
            // watermark image file logo1
            File markFile2 = new File(logo2);
            if (markFile2.exists() && !markFile2.isDirectory()) {

                Image mark_img2 = ImageIO.read(markFile2);
                int mark_img_width2 = Math.round(mark_img2.getWidth(null) / scale);
                int mark_img_height2 = Math.round(mark_img2.getHeight(null) / scale);
                logo2Width = mark_img_width2;
                g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, DefaultValues.opacity));
                switch (logo2Position) {
                    case ImageWatermark.MARK_LEFT_TOP:
                        g.drawImage(mark_img2, OFFSET_X, OFFSET_Y, mark_img_width2, mark_img_height2, null);
                        break;
                    case ImageWatermark.MARK_LEFT_BOTTOM:
                        g.drawImage(mark_img2, OFFSET_X, (height - mark_img_height2 - OFFSET_Y), mark_img_width2, mark_img_height2, null);
                        break;
                    case ImageWatermark.MARK_CENTER:
                        g.drawImage(mark_img2, (width - mark_img_width2 - OFFSET_X) / 2, (height - mark_img_height2 - OFFSET_Y) / 2,
                                mark_img_width2, mark_img_height2, null);
                        break;
                    case ImageWatermark.MARK_RIGHT_TOP:
                        g.drawImage(mark_img2, (width - mark_img_width2 - OFFSET_X), OFFSET_Y, mark_img_width2, mark_img_height2, null);
                        break;
                    case ImageWatermark.MARK_RIGHT_BOTTOM:
                    default:
                        g.drawImage(mark_img2, (width - mark_img_width2 - OFFSET_X), (height - mark_img_height2 - OFFSET_Y),
                                mark_img_width2, mark_img_height2, null);
                }
            }
            int watermarkWidth = 0;
            // watermark image file logo1
            if (!StringUtils.isEmpty(waterMark)) {
                File watermarkFile2 = new File(waterMark);
                if (watermarkFile2.exists() && !watermarkFile2.isDirectory()) {

                    Image mark_img3 = ImageIO.read(watermarkFile2);
                    int mark_img_width3 = mark_img3.getWidth(null) * 5;
                    int mark_img_height3 = mark_img3.getHeight(null) * 5;
                    watermarkWidth = mark_img_width3;
                    g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, DefaultValues.opacity / 2));
                    switch (ImageWatermark.MARK_CENTER) {
                        case ImageWatermark.MARK_LEFT_TOP:
                            g.drawImage(mark_img3, OFFSET_X, OFFSET_Y, mark_img_width3, mark_img_height3, null);
                            break;
                        case ImageWatermark.MARK_LEFT_BOTTOM:
                            g.drawImage(mark_img3, OFFSET_X, (height - mark_img_height3 - OFFSET_Y), mark_img_width3, mark_img_height3, null);
                            break;
                        case ImageWatermark.MARK_CENTER:
                            g.drawImage(mark_img3, (width - mark_img_width3 - OFFSET_X) / 2, (height - mark_img_height3 - OFFSET_Y) / 2,
                                    mark_img_width3, mark_img_height3, null);
                            break;
                        case ImageWatermark.MARK_RIGHT_TOP:
                            g.drawImage(mark_img3, (width - mark_img_width3 - OFFSET_X), OFFSET_Y, mark_img_width3, mark_img_height3, null);
                            break;
                        case ImageWatermark.MARK_RIGHT_BOTTOM:
                        default:
                            g.drawImage(mark_img3, (width - mark_img_width3 - OFFSET_X), (height - mark_img_height3 - OFFSET_Y),
                                    mark_img_width3, mark_img_height3, null);
                    }
                }
            }

            if (text != null && text.length() > 0) {
                //drawing logo text
                Font font = new Font("Comic Sans MS", Font.BOLD, fontSize);

                FontMetrics fm = g.getFontMetrics(font);
                int fontHeight = fm.getHeight();
                int leading = fm.getLeading();
                int fontWidth = fm.stringWidth(text);
                java.util.List<String> stringsToWrite = new ArrayList<String>();
                if (fontWidth > (pictureWidth - 60 * pictureWidth / defaultWidth - logo2Width - logo1Width)) {
                    /*if (fontWidth > (pictureWidth - 60 - logo2Width - logo1Width)) {*/
                    String[] strArray = text.split(" ");
                    String toAdd = "";
                    for (int i = 0; i < strArray.length; i++) {
                        String tmp = toAdd + " " + strArray[i];
                        if (fm.stringWidth(tmp) < (pictureWidth - 60 * pictureWidth / defaultWidth - logo2Width - logo1Width)) {
                            /* if (fm.stringWidth(tmp) < (pictureWidth - 60 - logo2Width - logo1Width)) {*/
                            toAdd += " " + strArray[i];
                        } else {
                            toAdd = toAdd.trim();
                            stringsToWrite.add(toAdd);
                            toAdd = strArray[i];
                        }

                        if (i == strArray.length - 1) {
                            toAdd = toAdd.trim();
                            stringsToWrite.add(toAdd);
                        }

                    }
                } else {
                    stringsToWrite.add(text);
                }
                int lineCount = stringsToWrite.size();
                for (int i = 0; i < lineCount; i++) {
                    fontWidth = fm.stringWidth(stringsToWrite.get(i));
                    g.setColor(Color.black);
                    int offset_x = logo1Width + 10 * pictureWidth / defaultWidth + ((pictureWidth - logo1Width - 20 * pictureWidth / defaultWidth - logo2Width - fontWidth) / 2);
                    /*int offset_x = logo1Width + 10 + ((pictureWidth - logo1Width - 20 - logo2Width - fontWidth) / 2);*/
                    int offset_y = 20 + leading * (lineCount - 1 - i) + fontHeight * (lineCount - 1 - i);
                    g.setFont(font);
                    g.drawString(stringsToWrite.get(i), offset_x, height - font.getSize() / 2 - offset_y);
                    g.setColor(Color.white);
                    offset_x = offset_x - 5;
                    offset_y = offset_y + 5;
                    g.drawString(stringsToWrite.get(i), offset_x, height - font.getSize() / 2 - offset_y);
                }
            }
            g.dispose();
            return image;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * A logo picture to picture and watermark, effective this way
     * one instance : http://www.mvgod.com/images/poster/NaNiYaChuanQi2XKaiSiBinWangZi-10285-12811-19978-13383/3.jpg
     *
     * @param srcImg     --
     *                   source image
     * @param markImg1   --
     *                   watermark logo image
     * @param markImg2   --
     *                   watermark logo image
     * @param text       --
     *                   text
     *
     */
    public final static String addLogoToImage(boolean rotated, String srcImg, String markImg1, String markImg2, String text, String output,
                                              String pictureSize, String watrmark, String resolution, LocationImageMetadata locationMetadata) {
        BufferedImage image = markWithImageAndLogo(rotated, srcImg, markImg1, ImageWatermark.MARK_LEFT_BOTTOM, markImg2, ImageWatermark.MARK_RIGHT_BOTTOM, text, watrmark, pictureSize, resolution);

        return getOrSaveConvertedFile(image, output, srcImg, locationMetadata);
    }

    public final synchronized static String getOrSaveConvertedFile(BufferedImage image, String output, String srcImg, LocationImageMetadata locationMetadata) {
        if (new File(output).exists()) {
            logger.info("$$$$$$$$ FILE ALREADY EXIST: TAKEN FROM CACHE $$$$$$$$");
            logger.info("--" + output + "--");
            return output;
        } else {
            if (image == null) return srcImg;
            try {
                logger.info("@@@@@@@@@@@@@@@@@@ FILE CREATED @@@@@@@@@@@@@@@@@@@@");
                logger.info("--" + output + "--");
                OutputStream out = new PrintStream(output);
                ImageIO.write(image, "jpg", out);
                out.close();
                try {
                    applyMetadata(new File(output), locationMetadata);
                } catch (ImageReadException e) {
                    logger.error(e.getMessage());
                } catch (ImageWriteException e) {
                    logger.error(e.getMessage());
                }
                return output;
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }
        return srcImg;
    }

    public static void applyMetadata(File image, LocationImageMetadata locationMetadata)
            throws IOException, ImageReadException, ImageWriteException {
        logger.info("Adding image metadata to image: " + image.getName());
        OutputStream os = null;
        File taggedImage = null;
        try {
            if (locationMetadata != null) {
                JpegImageMetadata metadata = (JpegImageMetadata) Sanselan.getMetadata(image);
                TiffOutputSet outputSet = new TiffOutputSet();
                if (metadata != null) {
                    TiffImageMetadata tiffMetadata = metadata.getExif();
                    if (tiffMetadata != null) {
                        outputSet = tiffMetadata.getOutputSet();
                    }
                }
                TiffOutputDirectory root = outputSet.getOrCreateRootDirectory();
                TiffOutputDirectory exifDirectory = outputSet.getOrCreateExifDirectory();
                addMetadataToImage(root, TiffConstants.EXIF_TAG_IMAGE_DESCRIPTION, locationMetadata.getTitleMetadata());
                addMetadataToImage(root, TiffConstants.EXIF_TAG_COPYRIGHT, locationMetadata.getCopyrightMetadata());
                addMetadataToImage(exifDirectory, TiffConstants.EXIF_TAG_USER_COMMENT, locationMetadata.getCommentMetadata());
                taggedImage = new File(image.getAbsolutePath() + "~");
                taggedImage.createNewFile();
                os = new FileOutputStream(taggedImage);
                new ExifRewriter().updateExifMetadataLossless(image, os, outputSet);
            }
        } finally {
            try {
                if (os != null) {
                    os.close();
                }
                if (taggedImage != null && taggedImage.exists()) {
                    image.delete();
                    taggedImage.renameTo(image);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static void addMetadataToImage(TiffOutputDirectory directory, TagInfo tagInfo, String value) {
        TiffOutputField tag = new TiffOutputField(tagInfo, TiffOutputField.FIELD_TYPE_ASCII, value.length(), value.getBytes());
        directory.removeField(tagInfo);
        directory.add(tag);
    }

    public static int getLogoSize(String logo) {
        int size = 0;
        if (logo == null || logo.length() == 0) return size;

        File markFile2 = new File(logo);
        if (markFile2.exists() && !markFile2.isDirectory()) {

            try {
                Image mark_img2 = ImageIO.read(markFile2);
                size = mark_img2.getWidth(null);
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }
        return size;
    }

}
