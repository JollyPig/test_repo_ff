package com.scnsoft.fotaflo.webapp.model;

import com.scnsoft.fotaflo.common.model.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */

@Entity
@Table(name = "reportSettings")
public class ReportSettings extends AbstractEntity {

    String reportSettings;
    String smtpServer;
    String smtpServerPort;
    String fromEmail;
    String smtpServerFromPassword;
    String subjectText;
    String ssl;

    @Column(name = "report_path", length = 255)
    public String getReportSettings() {
        return reportSettings;
    }

    public void setReportSettings(String reportSettings) {
        this.reportSettings = reportSettings;
    }

    @Column(name = "smtp_server", length = 255)
    public String getSmtpServer() {
        return smtpServer;
    }

    public void setSmtpServer(String smtpServer) {
        this.smtpServer = smtpServer;
    }

    @Column(name = "server_port", length = 255)
    public String getSmtpServerPort() {
        return smtpServerPort;
    }

    public void setSmtpServerPort(String smtpServerPort) {
        this.smtpServerPort = smtpServerPort;
    }

    @Column(name = "from_email", length = 255)
    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    @Column(name = "from_password", length = 255)
    public String getSmtpServerFromPassword() {
        return smtpServerFromPassword;
    }

    public void setSmtpServerFromPassword(String smtpServerFromPassword) {
        this.smtpServerFromPassword = smtpServerFromPassword;
    }

    @Column(name = "report_subject", length = 255)
    public String getSubjectText() {
        return subjectText;
    }

    public void setSubjectText(String subjectText) {
        this.subjectText = subjectText;
    }

    @Column(name = "ssl_check", length = 255)
    public String getSsl() {
        return ssl;
    }

    public void setSsl(String ssl) {
        this.ssl = ssl;
    }
}
