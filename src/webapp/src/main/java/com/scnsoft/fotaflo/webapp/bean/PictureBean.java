package com.scnsoft.fotaflo.webapp.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.File;

/**
 * Created by Paradinets Tatsiana
 * Date: 26.03.2011
 * Time: 11:41:11
 */

//@JsonAutoDetect
public class PictureBean {
    public static final String IMG_DIR = "img" + "/";
    public static final String IMG_SMALL_DIR = "img_small" + "/";

    protected int id;
    String name;
    String url;
    String base64code;
    String cameraId;
    String cameraName;
    String creationDate;
    Boolean selected;
    String logoMainUrl;
    String logoUrl;
    String logoText;
    Boolean rotated;
    String pictureSize;

    String tag;

    public PictureBean() {
    }

    public PictureBean(int id, String name, String url, String cameraId, String cameraName, String creationDate, Boolean selected, Boolean rotated, String pictureSize) {
        this.id = id;
        this.name = name;
        //TO DO
        int sign = url.substring(0, 1).equals(File.separator) ? 1 : 0;
        this.url = url.replace(File.separator, "/").substring(sign);
        this.base64code = this.url.replace(IMG_DIR, IMG_SMALL_DIR);
        this.cameraId = cameraId;
        this.cameraName = cameraName;
        this.creationDate = creationDate;
        this.selected = selected;
        this.logoMainUrl = "";
        this.logoUrl = "";
        this.logoText = "";
        this.rotated = rotated;
        this.pictureSize = pictureSize == null ? "" : pictureSize;
    }

    public PictureBean(int id, String name, String url, String base64code, String cameraId, String cameraName, String creationDate, Boolean selected, Boolean rotated, String pictureSize) {
        this.id = id;
        this.name = name;
        int sign = url.substring(0, 1).equals(File.separator) ? 1 : 0;
        this.url = url.replace(File.separator, "/").substring(sign);
        this.base64code = this.url.replace(IMG_DIR, IMG_SMALL_DIR);
        this.cameraId = cameraId;
        this.cameraName = cameraName;
        this.creationDate = creationDate;
        this.selected = selected;
        this.rotated = rotated;
        this.pictureSize = pictureSize == null ? "" : pictureSize;
    }

    public PictureBean(String name, String url, String base64code, String cameraId, String creationDate, Boolean rotated, String pictureSize) {
        this.name = name;
        int sign = url.substring(0, 1).equals(File.separator) ? 1 : 0;
        this.url = url.replace(File.separator, "/").substring(sign);
        this.base64code = this.url.replace(IMG_DIR, IMG_SMALL_DIR);
        this.base64code = url;
        this.cameraId = cameraId;
        this.creationDate = creationDate;
        this.rotated = rotated;
        this.pictureSize = pictureSize == null ? "" : pictureSize;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPictureSize() {
        return pictureSize;
    }

    public void setPictureSize(String pictureSize) {
        this.pictureSize = pictureSize;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBase64code() {
        return base64code;
    }

    public void setBase64code(String base64code) {
        this.base64code = base64code;
    }

    public String getCameraId() {
        return cameraId;
    }

    public void setCameraId(String cameraId) {
        this.cameraId = cameraId;
    }

    public String getCameraName() {
        return cameraName;
    }

    public void setCameraName(String cameraName) {
        this.cameraName = cameraName;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public Boolean isSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public String getUrl() {
        return getUrl(false);
    }

    public Boolean getRotated() {
        return rotated;
    }

    public void setRotated(Boolean rotated) {
        this.rotated = rotated;
    }

    @JsonIgnore
    public String getUrl(boolean reduced) {
        if (reduced) {
            return url.replace(IMG_DIR, IMG_SMALL_DIR);
        } else {
            return url;
        }
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLogoMainUrl() {
        return logoMainUrl;
    }

    public void setLogoMainUrl(String logoMainUrl) {
        this.logoMainUrl = logoMainUrl;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getLogoText() {
        return logoText;
    }

    public void setLogoText(String logoText) {
        this.logoText = logoText;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
