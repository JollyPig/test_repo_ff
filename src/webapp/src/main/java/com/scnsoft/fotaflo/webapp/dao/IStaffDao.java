package com.scnsoft.fotaflo.webapp.dao;

import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.model.Staff;

import java.util.List;

public interface IStaffDao extends IDao {
    List<Staff> getStaffByLocation(Location location);

    List<Staff> getPagedStaffByLocation(Location location, int start, int limit);

    Integer countStaffForLocation(Location location);

    List getStatisticForStaff();
}
