package com.scnsoft.fotaflo.webapp.util;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.scnsoft.fotaflo.webapp.bean.HelpYourselfSelectedPictureBean;
import com.scnsoft.fotaflo.webapp.bean.PictureBean;
import com.scnsoft.fotaflo.webapp.bean.PurchasePictureBean;
import com.scnsoft.fotaflo.webapp.bean.UserBean;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */

@Component
public class PictureUtil {

    /**
     * Get list of Contacts from request.
     *
     * @param data - json data from request
     * @return list of Contacts
     */
    public List<PictureBean> getPicturesFromRequest(Object data) {

        List<PictureBean> list = null;

        if (data != null) {
            //it is an array - have to cast to array object
            if (data.toString().indexOf('[') > -1) {

                list = getListPicturesFromJSON(data);

            } else { //it is only one object - cast to object/bean

                PictureBean pictureBean = getPictureFromJSON(data);

                list = new ArrayList<PictureBean>();
                list.add(pictureBean);
            }

        }

        return list;
    }

    /**
     * Transform json data format into Contact object
     *
     * @param data - json data from request
     *             <p/>
     *             http://stackoverflow.com/questions/2496494/library-to-encode-decode-from-json-to-java-util-map
     *             http://code.google.com/p/json-simple/wiki/DecodingExamples
     *             http://code.google.com/p/json-simple/wiki/EncodingExamples
     */
    private PictureBean getPictureFromJSON(Object data) {
        Gson gson = new Gson();
        String json = data.toString();
        Map<String, String> pictureMap = gson.fromJson(json, new TypeToken<Map<String, String>>() {
        }.getType());
        return FotafloUtil.createPictureBean(pictureMap);
    }

    /**
     * Transform json data format into list of Contact objects
     *
     * @param data - json data from request
     *             return list object with type PictureBean
     */
    @SuppressWarnings("unchecked")
    private List<PictureBean> getListPicturesFromJSON(Object data) {
        String jsons = data.toString();
        JsonElement json = new JsonParser().parse(jsons);
        JsonArray array = json.getAsJsonArray();
        Iterator iterator = array.iterator();
        List<PictureBean> details = new ArrayList<PictureBean>();
        while (iterator.hasNext()) {
            JsonElement json2 = (JsonElement) iterator.next();
            Gson gson = new Gson();
            Map<String, String> pictureMap = gson.fromJson(json2, new TypeToken<Map<String, String>>() {
            }.getType());
            details.add(FotafloUtil.createPictureBean(pictureMap));
        }
        return details;
    }


    /**
     * Get list of Contacts from request.
     *
     * @param data - json data from request
     * @return list of Contacts
     */
    public List<PurchasePictureBean> getPurchasePicturesFromRequest(Object data) {

        List<PurchasePictureBean> list;

        //it is an array - have to cast to array object
        if (data.toString().indexOf('[') > -1) {

            list = getPurchaseListPicturesFromJSON(data);

        } else { //it is only one object - cast to object/bean

            PurchasePictureBean pictureBean = getPurchasePictureFromJSON(data);

            list = new ArrayList<PurchasePictureBean>();
            if (pictureBean != null) {
                list.add(pictureBean);
            }

        }

        return list;
    }

    static public List<String> getPurchaseEmails(Object data) {

        List<String> list = new ArrayList<String>();

        //it is an array - have to cast to array object
        if (data.toString().indexOf('[') > -1 && data.toString().length() > 2) {

            String json = data.toString();
            json = json.substring(json.indexOf('[') + 2, json.length() - 2);
            list = Arrays.asList(json.split("\",\""));

        } else { //it is only one object - cast to object/bean


            list = new ArrayList<String>();
            if (data != null && !data.equals("")) {
                list.add(data.toString().replaceAll("/", ""));
            }
        }

        return list;
    }

    /**
     * Transform json data format into Contact object
     *
     * @param data - json data from request
     *             <p/>
     *             http://stackoverflow.com/questions/2496494/library-to-encode-decode-from-json-to-java-util-map
     *             http://code.google.com/p/json-simple/wiki/DecodingExamples
     *             http://code.google.com/p/json-simple/wiki/EncodingExamples
     */
    private PurchasePictureBean getPurchasePictureFromJSON(Object data) {
        Gson gson = new Gson();
        String json = data.toString();
        Map<String, String> pictureMap = gson.fromJson(json, new TypeToken<Map<String, String>>() {
        }.getType());
        PurchasePictureBean pictureBean = null;
        if (pictureMap != null && !pictureMap.isEmpty()) {
            pictureBean = FotafloUtil.createPurchasePictureBean(pictureMap);
        }
        return pictureBean;
    }


    /**
     * Transform json data format into list of Contact objects
     *
     * @param data - json data from request
     * @return
     */
    @SuppressWarnings("unchecked")
    private List<PurchasePictureBean> getPurchaseListPicturesFromJSON(Object data) {
        String jsons = data.toString();
        JsonElement json = new JsonParser().parse(jsons);
        JsonArray array = json.getAsJsonArray();
        Iterator iterator = array.iterator();
        List<PurchasePictureBean> details = new ArrayList<PurchasePictureBean>();
        while (iterator.hasNext()) {
            JsonElement json2 = (JsonElement) iterator.next();
            Gson gson = new Gson();
            Map<String, String> pictureMap = gson.fromJson(json2, new TypeToken<Map<String, String>>() {
            }.getType());
            if (pictureMap != null && !pictureMap.isEmpty()) {
                details.add(FotafloUtil.createPurchasePictureBean(pictureMap));
            }
        }
        return details;
    }

    public List<HelpYourselfSelectedPictureBean> getHelpYourselfPicturesFromRequest(Object data) {
        List<HelpYourselfSelectedPictureBean> list;
        //it is an array - have to cast to array object
        if (data.toString().indexOf('[') > -1) {
            list = getHelpYourselfListPicturesFromJSON(data);
        } else { //it is only one object - cast to object/bean
            HelpYourselfSelectedPictureBean pictureBean = getHelpYourselfPictureFromJSON(data);
            list = new ArrayList<HelpYourselfSelectedPictureBean>();
            if (pictureBean != null) {
                list.add(pictureBean);
            }
        }
        return list;
    }

    /**
     * Transform json data format into Contact object
     *
     * @param data - json data from request
     *             <p/>
     *             http://stackoverflow.com/questions/2496494/library-to-encode-decode-from-json-to-java-util-map
     *             http://code.google.com/p/json-simple/wiki/DecodingExamples
     *             http://code.google.com/p/json-simple/wiki/EncodingExamples
     */
    private HelpYourselfSelectedPictureBean getHelpYourselfPictureFromJSON(Object data) {
        Gson gson = new Gson();
        String json = data.toString();
        Map<String, String> pictureMap = gson.fromJson(json, new TypeToken<Map<String, String>>() {
        }.getType());
        HelpYourselfSelectedPictureBean pictureBean = null;
        if (pictureMap != null && !pictureMap.isEmpty()) {
            pictureBean = FotafloUtil.createHelpYourselfSelectedPictureBean(pictureMap);
        }
        return pictureBean;
    }

    /**
     * Transform json data format into list of Contact objects
     *
     * @param data - json data from request
     * @return
     */
    @SuppressWarnings("unchecked")
    private List<HelpYourselfSelectedPictureBean> getHelpYourselfListPicturesFromJSON(Object data) {
        String jsons = data.toString();
        JsonElement json = new JsonParser().parse(jsons);
        JsonArray array = json.getAsJsonArray();
        Iterator iterator = array.iterator();
        List details = new ArrayList();
        while (iterator.hasNext()) {
            JsonElement json2 = (JsonElement) iterator.next();
            Gson gson = new Gson();
            Map<String, String> pictureMap = gson.fromJson(json2, new TypeToken<Map<String, String>>() {
            }.getType());
            if (pictureMap != null && !pictureMap.isEmpty()) {
                details.add(FotafloUtil.createHelpYourselfSelectedPictureBean(pictureMap));
            }
        }
        return details;
    }

}
