package com.scnsoft.fotaflo.webapp.dao.impl;

import com.scnsoft.fotaflo.common.util.StringUtils;
import com.scnsoft.fotaflo.webapp.dao.IPurchaseDao;
import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.model.Purchase;
import com.scnsoft.fotaflo.webapp.model.PurchaseEmail;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 03.12.13
 * Time: 13:52
 * To change this template use File | Settings | File Templates.
 */
public class PurchaseDao extends BaseDao implements IPurchaseDao {

    public List<Purchase> findPurchasesByFilter(Location location, Date startDate, Date endDate, String email, String code, String tag, int status) {
        List<Purchase> result = null;

        Criteria criteria = getSession().createCriteria(Purchase.class);

        Criterion criterion = Restrictions.and(
                Restrictions.eq("statusCode", status),
                Restrictions.and(
                        Restrictions.ge("purchaseDate", startDate),
                        Restrictions.le("purchaseDate", endDate)
                )
        );

        if(location != null){
            criteria.createCriteria("location").add(Restrictions.eq("id", location.getId()));
        }

        if(StringUtils.isNotEmpty (email)){
            criterion = Restrictions.and(
                    criterion,
                    Restrictions.ilike("participantsEmails", '%' + email + '%')
            );
        }

        if(StringUtils.isNotEmpty (code)){
            criterion = Restrictions.and(
                    criterion,
                    Restrictions.like("generatedCode", '%' + code + '%')
            );
        }

        if(StringUtils.isNotEmpty(tag)){
            criterion = Restrictions.and(
                    criterion,
                    Restrictions.like("tags", '%' + tag + '%')
            );
        }

        criteria.add(criterion);
        criteria.addOrder(Order.asc("id"));

        result = criteria.list();

        return result;
    }

    public List<Purchase> getPurchaseByStatusAndLocation(Location location, int status) {
        List<Purchase> list1 = new ArrayList<Purchase>();
        StringBuffer query = new StringBuffer();
        query.append("select  purchase from Purchase as purchase where ");
        query.append("purchase.userLocation = :location ");
        query.append("and purchase.statusCode = :statusCode ");
        if (location != null) {
            list1 = getSession().createQuery(query.toString())
                    .setParameter("location", location)
                    .setParameter("statusCode", status)
                    .list();
        }

        return list1;
    }

    public List<Purchase> getPurchaseByStatus(int status) {
        List<Purchase> list1 = new ArrayList<Purchase>();
        StringBuffer query = new StringBuffer();
        query.append("select purchase from Purchase as purchase where ");
        query.append("purchase.statusCode = :statusCode ");

        list1 = getSession().createQuery(query.toString())
                .setParameter("statusCode", status)
                .list();

        return list1;
    }

    public List<PurchaseEmail> getEmailsByParameters(Location location, Date startDate, Date endDate) {
        List<PurchaseEmail> list1 = new ArrayList<PurchaseEmail>();
        StringBuffer query = new StringBuffer();
        query.append("select  purchaseEmail from PurchaseEmail as purchaseEmail where ");
        query.append("purchaseEmail.date >= :start ");
        query.append("and purchaseEmail.date <= :end ");
        if (location != null) {
            query.append("and purchaseEmail.location = :location");
        }

        if (location != null) {
            list1 = getSession().createQuery(query.toString())
                    .setParameter("location", location)
                    .setParameter("start", startDate)
                    .setParameter("end", endDate)
                    .list();
        } else {
            list1 = getSession().createQuery(query.toString())
                    .setParameter("start", startDate)
                    .setParameter("end", endDate)
                    .list();
        }

        return list1;
    }

    public Purchase getPurchaseByCode(String generatedCode) {
        List<Purchase> list1 = new ArrayList<Purchase>();
        StringBuffer query = new StringBuffer();
        query.append("select  purchase from Purchase as purchase where ");
        query.append("purchase.generatedCode = :generatedCode");


        list1 = getSession().createQuery(query.toString())
                .setParameter("generatedCode", generatedCode)
                .list();

        if (list1.size() == 1) return list1.get(0);
        return null;
    }

}
