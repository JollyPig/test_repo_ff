package com.scnsoft.fotaflo.webapp.ws;

import com.scnsoft.fotaflo.common.util.DateFormatUtil;
import com.scnsoft.fotaflo.webapp.bean.PictureBean;
import com.scnsoft.fotaflo.webapp.controller.MainController;
import com.scnsoft.fotaflo.webapp.model.*;
import com.scnsoft.fotaflo.webapp.service.*;
import com.scnsoft.fotaflo.webapp.util.AccessID;
import com.scnsoft.fotaflo.webapp.util.DateConvertationUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */
@Controller
@RequestMapping("/web/pictures")
public class WebPortalPicturesController {
    protected static Logger logger = Logger.getLogger(MainController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private PurchaseService purchaseService;

    @Autowired
    private PictureService pictureService;

    @Autowired
    private TagPurchaseService tagPurchaseService;

    @Autowired
    private TagService tagService;

    @Autowired
    private PictureSlideShowService picturesSlideShowService;

    @RequestMapping(value = "{name}", method = RequestMethod.GET)
    public
    @ResponseBody
    List<PictureBean> getCodePicturesJSON(@PathVariable String name,
                                          @RequestParam(value = "from", required = false) @DateTimeFormat(pattern = DateFormatUtil.ISO_DATETIME_FORMAT_PATTERN) Date date) {

        List<PictureBean> puPictureBeans = new ArrayList<PictureBean>();
        List<Picture> pictures = new ArrayList<Picture>();
        SystemUser user = userService.getUserByLogin(name);
        if (user == null) {
            return null;
        }
        if (user.getAccess().equals(String.valueOf(AccessID.PUBLICPURCHASEUSER.getValue()))) {
            String name1 = name;
            if (name.length() == 8) {
                name1 = name.substring(0, 7);
            }
            Purchase purchase = purchaseService.getPurchaseByCode(name1);
            if (purchase != null) {
                Set<PurchasePictures> purchasePicturesSet = purchase.getPurchasePictures();
                for (PurchasePictures purchasePictures : purchasePicturesSet) {
                    pictures.add(purchasePictures.getPicture());
                }
            }
        }
        if (user.getAccess().equals(String.valueOf(AccessID.PUBLICEMAILUSER.getValue()))) {
            UserFilter userFilter = pictureService.getUserFilter(name);
            if (userFilter != null) {
                pictures = userFilter.getPictures();
            }
        }
        if (user.getAccess().equals(String.valueOf(AccessID.PUBLICTAGCODEUSER.getValue()))) {
            logger.info("TAGGED CODE USER IS LOOGED IN");
            String nametag = user.getLoginName();
            if (nametag.length() == 8) {
                nametag = nametag.substring(0, 7);
            }
            TagPurchase tagPurchase = tagPurchaseService.getPurchaseTagByCode(nametag);
            pictures.addAll(tagPurchaseService.getPictureForTagPurchase(tagPurchase));
        }
        if (user.getAccess().equals(String.valueOf(AccessID.PUBLICTAGACODE.getValue()))) {
            logger.info("TAG A CODE USER IS LOOGED IN");
            Tag tag = tagService.getTagByNameLocation(user.getLoginName(), user.getLocation());

            if (tag != null) {
                Set<TagPurchase> tagPurchasesSet = tag.getTagPurchases();
                pictures.addAll(tagPurchaseService.getPictureForTagPurchase(tagPurchasesSet));
            }
        }
        if (user.getAccess().equals(String.valueOf(AccessID.PUBLICTAGUSER.getValue()))) {
            logger.info("TAG USER USER IS LOOGED IN");
            Tag tag = tagService.getTagByNameLocation(user.getLoginName(), user.getLocation());

            if (tag != null) {
                logger.info("tag " + tag.getTagName());
                logger.info("Pictures for Tag " + tag.getTagedpictures().size());
                pictures.addAll(tag.getTagedpictures());
            }
        }

        puPictureBeans = picturesSlideShowService.getPicturesFilteredPictureList(name, pictures, date);

        Location location = user.getLocation();
        if (location != null && location.getLocationAddPictures() != null) {
            PictureBean bean;
            for (Picture p : location.getLocationAddPictures()) {
                if(date != null && date.after(p.getCreationDate())){
                    continue;
                }
                bean = new PictureBean(p.getId(), p.getName(), p.getUrl(), null, null, DateConvertationUtils.viewStringDateFormat(p.getCreationDate()), null, null, "");
                bean.setLogoUrl(location.getLocationImageLogoUrl());
                bean.setLogoText(location.getLocationTextLogo());
                bean.setLogoMainUrl(pictureService.getMainLogo(location));
                puPictureBeans.add(bean);
            }
        }

        return puPictureBeans;
    }

}
