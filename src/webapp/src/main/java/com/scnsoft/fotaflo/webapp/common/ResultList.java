package com.scnsoft.fotaflo.webapp.common;

import java.util.List;

/**
 * @author Anatoly Selitsky
 */
public class ResultList<T> {

    private List<T> items;

    private Long total;

    public ResultList() {
        // do nothing
    }

    public ResultList(Long total, List<T> items) {
        this.total = total;
        this.items = items;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

}
