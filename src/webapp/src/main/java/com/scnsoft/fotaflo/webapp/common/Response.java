package com.scnsoft.fotaflo.webapp.common;

/**
 * Response for ExtJS request. Wrap web model has type T.
 *
 * @author Anatoly Selitsky
 */
public class Response<T> extends BaseResponse {

    private T children;

    public Response() {
    }

    /**
     * Create new instance.
     *
     * @param children - web model
     */
    public Response(T children) {
        this.children = children;
    }

    /**
     * Create new instance.
     *
     * @param children - web model
     * @param success - response status
     */
    public Response(T children, boolean success) {
        super(success);
        this.children = children;
    }

    public T getChildren() {
        return children;
    }

    public void setChildren(T children) {
        this.children = children;
    }

}
