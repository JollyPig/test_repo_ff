package com.scnsoft.fotaflo.webapp.reportmodule.reportutils;

import com.scnsoft.fotaflo.webapp.reportmodule.model.CameraObj;
import com.scnsoft.fotaflo.webapp.reportmodule.model.LocationObj;
import com.scnsoft.fotaflo.webapp.reportmodule.model.PictureObj;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 20.08.2012
 * Time: 9:38:50
 * To change this template use File | Settings | File Templates.
 */
public class LogAnalizer {

    Map<String, PictureObj> pictureMap;

    public LogAnalizer(Map<String, PictureObj> pictureMap) {
        this.pictureMap = pictureMap;
    }

    public Map<String, LocationObj> groupByLocationCamera() {
        Map<String, LocationObj> locationObjectsMap = new HashMap<String, LocationObj>();

        for (String key : pictureMap.keySet()) {
            PictureObj picture = pictureMap.get(key);
            String location = picture.getLocationName();

            LocationObj locationObj = locationObjectsMap.get(location);
            if (locationObj == null) {
                locationObj = new LocationObj();
            }
            locationObj.setName(location);
            String camera = picture.getCameraId();
            Map<String, CameraObj> cameraObjMap = locationObj.getCameras();
            if (cameraObjMap == null) {
                cameraObjMap = new HashMap<String, CameraObj>();
            }
            CameraObj cameraObj = cameraObjMap.get(camera);
            if (cameraObj == null) {
                cameraObj = new CameraObj();
                cameraObj.setName(camera);
            }
            //ReJECTED
            int rejCam = cameraObj.getRejected();
            cameraObj.setRejected(rejCam + picture.getRejected());
            int rejLoc = locationObj.getRejected();
            locationObj.setRejected(rejLoc + picture.getRejected());

            //COUNT UNSUCCESSES
            int unsucCam = cameraObj.getCountUnsuccessfull();
            cameraObj.setCountUnsuccessfull(unsucCam + (picture.getCounter() == 0 ? 0 : picture.getCounter() - 1));
            int unsucLoc = locationObj.getCountUnsuccessfull();
            locationObj.setCountUnsuccessfull(unsucLoc + (picture.getCounter() == 0 ? 0 : picture.getCounter() - 1));


            //ALREADY EXIST
            int existCamera = cameraObj.getAlreadyExist();
            cameraObj.setAlreadyExist(existCamera + (picture.getAlreadyExistWarn() ? 1 : 0));
            int existLoc = locationObj.getAlreadyExist();
            locationObj.setAlreadyExist(existLoc + (picture.getAlreadyExistWarn() ? 1 : 0));

            //ERROR TYPES
            String errorTypesCam = cameraObj.getErrorTypes() == null ? "" : cameraObj.getErrorTypes();
            String pictureErr = picture.getErrorType() == null ? "" : picture.getErrorType();
            cameraObj.setErrorTypes(errorTypesCam + pictureErr);
            String errorTypesLoc = locationObj.getErrorTypes() == null ? "" : locationObj.getErrorTypes();
            locationObj.setErrorTypes(errorTypesLoc + pictureErr);

            //MAX BEFORE NUMBER
            int beforeCam = cameraObj.getMaxBeforeNumber();
            int maxBeforeCam = beforeCam > picture.getBeforeNumber() ? beforeCam : picture.getBeforeNumber();
            cameraObj.setMaxBeforeNumber(maxBeforeCam);

            int beforeLoc = locationObj.getMaxBeforeNumber();
            int maxBeforeLoc = beforeLoc > picture.getBeforeNumber() ? beforeLoc : picture.getBeforeNumber();
            locationObj.setMaxBeforeNumber(maxBeforeLoc);

            List<Integer> totals = cameraObj.getTotals();
            if (totals == null) {
                totals = new ArrayList<Integer>();
            }
            totals.add(picture.getTotalTimeUpload());
            cameraObj.setTotals(totals);

            List<Integer> totalsLoc = locationObj.getTotals();
            if (totalsLoc == null) {
                totalsLoc = new ArrayList<Integer>();
            }
            totalsLoc.add(picture.getTotalTimeUpload());
            locationObj.setTotals(totalsLoc);

            //MaxTimeUpload
            int uploadMaxTimeCam = cameraObj.getMaxTimeUpload();
            int maxUploadMaxTimeCam = uploadMaxTimeCam > picture.getTotalTimeUpload() ? uploadMaxTimeCam : picture.getTotalTimeUpload();
            cameraObj.setMaxTimeUpload(maxUploadMaxTimeCam);

            int uploadMaxTimeLoc = locationObj.getMaxTimeUpload();
            int maxUploadMaxTimeLoc = uploadMaxTimeLoc > picture.getTotalTimeUpload() ? uploadMaxTimeLoc : picture.getTotalTimeUpload();
            locationObj.setMaxTimeUpload(maxUploadMaxTimeLoc);

            //MinTimeUpload
            int uploadMinTimeCam = cameraObj.getMinTimeUpload();
            if (uploadMinTimeCam == 0) {
                uploadMinTimeCam = picture.getTotalTimeUpload();
            }
            int minUploadMinTimeCam = uploadMinTimeCam < picture.getTotalTimeUpload() ? uploadMinTimeCam : picture.getTotalTimeUpload();
            cameraObj.setMinTimeUpload(minUploadMinTimeCam);

            int uploadMinTimeLoc = locationObj.getMinTimeUpload();
            if (uploadMinTimeLoc == 0) {
                uploadMinTimeLoc = picture.getTotalTimeUpload();
            }
            int maxUploadMinTimeLoc = uploadMinTimeLoc < picture.getTotalTimeUpload() ? uploadMinTimeLoc : picture.getTotalTimeUpload();
            locationObj.setMinTimeUpload(maxUploadMinTimeLoc);

            //Total
            int totalLoc = locationObj.getTotal() + 1;
            int totalCam = cameraObj.getTotal() + 1;
            locationObj.setTotal(totalLoc);
            cameraObj.setTotal(totalCam);

            //Successfull //Unsuccessfull
            if (picture.isSuccessfull()) {
                int successfullLoc = locationObj.isSuccessfull() + 1;
                int successfullCam = cameraObj.isSuccessfull() + 1;
                locationObj.setSuccessfull(successfullLoc);
                cameraObj.setSuccessfull(successfullCam);
            } else {
                int unsuccessfullLoc = locationObj.isUnsucessfull() + 1;
                int unsuccessfullCam = cameraObj.isUnsucessfull() + 1;
                locationObj.setUnsucessfull(unsuccessfullLoc);
                cameraObj.setUnsucessfull(unsuccessfullCam);
            }


            cameraObjMap.put(camera, cameraObj);
            locationObj.setCameras(cameraObjMap);
            locationObjectsMap.put(location, locationObj);

        }

        return locationObjectsMap;
    }
}
