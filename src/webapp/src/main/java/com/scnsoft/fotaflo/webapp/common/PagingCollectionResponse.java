package com.scnsoft.fotaflo.webapp.common;

import java.util.Collection;

/**
 *
 * @author Anatoly Selitsky
 *
 */
public class PagingCollectionResponse<T> extends Response<Collection<T>> {

    private int total;
    private int pageSize;

    public PagingCollectionResponse(int total, int pageSize) {
        this.total = total;
        this.pageSize = pageSize;
    }

    public PagingCollectionResponse(Collection<T> children, int total, int pageSize) {
        super(children);
        this.total = total;
        this.pageSize = pageSize;
    }

    public PagingCollectionResponse(Collection<T> children, boolean success, int total, int pageSize) {
        super(children, success);
        this.total = total;
        this.pageSize = pageSize;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

}
