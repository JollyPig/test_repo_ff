package com.scnsoft.fotaflo.webapp.controller;

import com.scnsoft.fotaflo.webapp.common.Response;
import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.service.CameraService;
import com.scnsoft.fotaflo.webapp.service.LocationService;
import com.scnsoft.fotaflo.webapp.service.PictureService;
import com.scnsoft.fotaflo.webapp.service.PublicLoginService;
import com.scnsoft.fotaflo.webapp.util.AccessID;
import com.scnsoft.fotaflo.webapp.util.DateConvertationUtils;
import com.scnsoft.fotaflo.webapp.web.model.PublicLoginModel;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 31.01.2012
 * Time: 13:57:01
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class PublicLoginController {

    private final static String successProperty = "success";
    private final static String messageProperty = "msg";
    protected static Logger logger = Logger.getLogger(PublicLoginController.class);

    @Autowired
    private LocationService locationService;

    @Autowired
    private CameraService cameraService;

    @Autowired
    private PictureService pictureService;

    @Autowired
    private PublicLoginService publicLoginService;

    @RequestMapping(value = "generatePublic.json", method = RequestMethod.POST)
    public
    @ResponseBody
    Map<String, ? extends Object> getPublicLogin(
            @RequestParam("iDlocationCombo") Integer locationId, @RequestParam("expDate") String expDate) {

        Date today = new Date();
        Date expirationDate = DateConvertationUtils.getStartDate(-15, today);
        if (expDate != null) {
            try {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                expirationDate = formatter.parse(expDate);
            } catch (ParseException e) {
                logger.error(e);
            }
        }
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();

        HashMap modelMap = new HashMap();
        Location currentLocation = locationService.getLocation(locationId);
        // Date expirationDate = DateConvertationUtils.getStartDate(-15,today);
        Map<String, String> credentials = publicLoginService.createGeneralPublicLogin(currentLocation, today, expirationDate, String.valueOf(AccessID.PUBLICUSER.getValue()), false);

        String result = credentials.get("success");
        if (result != null && result.equals("true")) {
            modelMap.put("login", credentials.get("login"));
            modelMap.put("password", credentials.get("password"));
            modelMap.put("expirationDate", expirationDate);
            modelMap.put(successProperty, "Password was successfully generated");
        } else {
            modelMap.put("error", "Unable to generate public login");
        }

        return modelMap;
    }

    @RequestMapping(value = "savePublicLogin.json", method = RequestMethod.POST)
    public
    @ResponseBody
    Response<PublicLoginModel> savePublicLogin(PublicLoginModel publicLoginModel) {
        if (publicLoginModel == null) {
            throw new IllegalArgumentException("Object can't be must empty");
        }
        boolean status = publicLoginService.updateGeneralPublicLogin(publicLoginModel);
        return new Response<PublicLoginModel>(publicLoginModel, status);
    }

}
