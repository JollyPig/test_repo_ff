package com.scnsoft.fotaflo.webapp.dao;

import com.scnsoft.fotaflo.webapp.model.Camera;
import com.scnsoft.fotaflo.webapp.model.Picture;
import com.scnsoft.fotaflo.webapp.model.SystemUser;
import com.scnsoft.fotaflo.webapp.model.Tag;

import java.util.Date;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 31.03.2012
 * Time: 10:23:42
 * To change this template use File | Settings | File Templates.
 */
public interface IPictureDao {

    public List getPicturesForCameras(Integer[] cameras);

    public List<Picture> findExtended(Date startDate, Date endDate, List<Camera> cameras);

    public List<Picture> findExtendedByTag(Date startDate, Date endDate, List<Camera> cameras, List<Tag> tags);

    public List<Picture> findExtendedWithoutTag(Date startDate, Date endDate, List<Camera> cameras);

    public List<Picture> findExtendedTagOnly(List<Tag> tags);

    public void removePicturesSelectedForUser(SystemUser user);
}
