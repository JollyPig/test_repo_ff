package com.scnsoft.fotaflo.webapp.validators;

import com.scnsoft.fotaflo.common.util.StringUtils;
import com.scnsoft.fotaflo.webapp.exceptions.FormValidationException;

public class SharedValidator {

    private final String parameterName;

    private final String value;

    protected SharedValidator (final String parameterName, final String value) {
        this.parameterName = parameterName;
        this.value = StringUtils.nothingToNull (value);
    }

    public static SharedValidator validate (final String parameterName, final String value) throws FormValidationException{
        return new SharedValidator (parameterName, value);
    }

    public SharedValidator required () throws FormValidationException {
        if (value == null) {
            throw new FormValidationException (StringUtils.concat ("Field \"", parameterName, "\" is required!"));
        }

        return this;
    }

    public SharedValidator minLength (final int length) throws FormValidationException {
        if (value == null || value.length () >= length) {
            return this;
        }
        else {
            throw new FormValidationException ();
        }
    }

    public SharedValidator maxLength (final int length) throws FormValidationException {
        if (value == null || value.length () <= length) {
            return this;
        }
        else {
            throw new FormValidationException ();
        }
    }

    public static String validateQrTagSlideShow (final String value) throws FormValidationException {
        return new QrTagSlideShowFormValidator (value).validate ();
    }

}
