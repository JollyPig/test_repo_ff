package com.scnsoft.fotaflo.webapp.model;

import com.scnsoft.fotaflo.common.model.AbstractEntity;

import javax.persistence.*;

/**
 * Created by Nadezda Drozdova
 * Date Apr 18, 2011
 */

@Entity
@Table(name = "purchase_selected_pictures")
public class PurchaseSelected extends AbstractEntity {
    SystemUser systemUser;
    Picture picture;

    Integer send_to_email_selected;
    Integer number_pict_selected;
    Integer add_to_facebook;
    Integer add_to_flickr;

    @ManyToOne
    /* (cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH} )*/
    @JoinColumn(name = "picture_id")
    public Picture getPicture() {
        return picture;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    @ManyToOne
    /*(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH} )*/
    @JoinColumn(name = "system_user_id")
    public SystemUser getSystemUser() {
        return systemUser;
    }

    public void setSystemUser(SystemUser systemUser) {
        this.systemUser = systemUser;
    }

    @Column(name = "send_to_email_selected")
    public Integer getSend_to_email_selected() {
        return send_to_email_selected;
    }

    public void setSend_to_email_selected(Integer send_to_email_selected) {
        this.send_to_email_selected = send_to_email_selected;
    }

    @Column(name = "number_pict_selected")
    public Integer getNumber_pict_selected() {
        return number_pict_selected;
    }

    public void setNumber_pict_selected(Integer number_pict_selected) {
        this.number_pict_selected = number_pict_selected;
    }

    @Column(name = "add_to_facebook")
    public Integer getAdd_to_facebook() {
        return add_to_facebook;
    }

    public void setAdd_to_facebook(Integer add_to_facebook) {
        this.add_to_facebook = add_to_facebook;
    }

    @Column(name = "add_to_flickr")
    public Integer getAdd_to_flickr() {
        return add_to_flickr;
    }

    public void setAdd_to_flickr(Integer add_to_flickr) {
        this.add_to_flickr = add_to_flickr;
    }
}