package com.scnsoft.fotaflo.webapp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.scnsoft.fotaflo.common.model.AbstractEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */
@Entity
@Table(name = "location")
public class Location extends AbstractEntity {
    private static final long serialVersionUID = 7410674022932886962L;

    String locationName;
    String locationTextLogo;
    String locationImageLogoUrl;

    String locationToFlickrEmail;
    String locationToFacebookEmail;
    String locationSmtpServer;
    String locationFromEmail;
    String locationToEmail;
    String locationToPhotobookEmail;

    String locationSmtpServerPassword;
    Integer locationSmtpServerPort;
    LocationSettings locationSettings;
    LocationImageMetadata imageMetadata;
    String resolution;

    Boolean reduced;
    Boolean tagged;
    Boolean bcconly;
    Boolean giveaway;
    Boolean fwpenable;
    Boolean rfid;
    Boolean camera;
    Boolean photobook;
    Boolean staffsave;
    Boolean slideShowInNewWindow;

    PayPalLocationSettings paypallocationSettings;

    private List<PublicStatistic> publicStatistic = new ArrayList<PublicStatistic>();

    private List<Requests> requests = new ArrayList<Requests>();

    private List<ReportEmails> reportEmails = new ArrayList<ReportEmails>();

    private List<Subscribe> subscribeEmails = new ArrayList<Subscribe>();

    private List<Picture> locationAddPictures = new ArrayList<Picture>();

    String locationSslCheck;

    @Column(name = "location_text_logo", length = 255)
    public String getLocationTextLogo() {
        return locationTextLogo;
    }

    public void setLocationTextLogo(String locationTextLogo) {
        this.locationTextLogo = locationTextLogo;
    }

    @Column(name = "location_image_logo", length = 255)
    public String getLocationImageLogoUrl() {
        return locationImageLogoUrl;
    }

    public void setLocationImageLogoUrl(String locationImageLogoUrl) {
        this.locationImageLogoUrl = locationImageLogoUrl;
    }

    @Column(name = "location_name", length = 64)
    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    @Column(name = "location_to_flickr_email", length = 255)
    public String getLocationToFlickrEmail() {
        return locationToFlickrEmail;
    }

    public void setLocationToFlickrEmail(String locationToFlickrEmail) {
        this.locationToFlickrEmail = locationToFlickrEmail;
    }

    @Column(name = "location_to_facebook_email", length = 255)
    public String getLocationToFacebookEmail() {
        return locationToFacebookEmail;
    }

    public void setLocationToFacebookEmail(String locationToFacebookEmail) {
        this.locationToFacebookEmail = locationToFacebookEmail;
    }

    @Column(name = "location_smtp_server", length = 255)
    public String getLocationSmtpServer() {
        return locationSmtpServer;
    }

    public void setLocationSmtpServer(String locationSmtpServer) {
        this.locationSmtpServer = locationSmtpServer;
    }

    @Column(name = "location_from_email", length = 255)
    public String getLocationFromEmail() {
        return locationFromEmail;
    }

    public void setLocationFromEmail(String locationFromEmail) {
        this.locationFromEmail = locationFromEmail;
    }

    @Column(name = "location_to_email", length = 255)
    public String getLocationToEmail() {
        return locationToEmail;
    }

    public void setLocationToEmail(String locationToEmail) {
        this.locationToEmail = locationToEmail;
    }

    @Column(name = "location_to_photobook_email", length = 255)
    public String getLocationToPhotobookEmail() {
        return locationToPhotobookEmail;
    }

    public void setLocationToPhotobookEmail(String locationToPhotobookEmail) {
        this.locationToPhotobookEmail = locationToPhotobookEmail;
    }

    @Column(name = "location_from_email_password", length = 255)
    public String getLocationSmtpServerPassword() {
        return locationSmtpServerPassword;
    }

    public void setLocationSmtpServerPassword(String locationSmtpServerPassword) {
        this.locationSmtpServerPassword = locationSmtpServerPassword;
    }


    @Column(name = "location_smtp_server_port", length = 255)
    public Integer getLocationSmtpServerPort() {
        return locationSmtpServerPort;
    }

    public void setLocationSmtpServerPort(Integer locationSmtpServerPort) {
        this.locationSmtpServerPort = locationSmtpServerPort;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "locationsettings_id")
    public LocationSettings getLocationSettings() {
        return locationSettings;
    }

    public void setLocationSettings(LocationSettings locationSettings) {
        this.locationSettings = locationSettings;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "paypallocationsettings_id")
    public PayPalLocationSettings getPaypallocationSettings() {
        return paypallocationSettings;
    }

    public void setPaypallocationSettings(PayPalLocationSettings paypallocationSettings) {
        this.paypallocationSettings = paypallocationSettings;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "imagemetadata_id")
    public LocationImageMetadata getImageMetadata() {
        return imageMetadata;
    }

    public void setImageMetadata(final LocationImageMetadata imageMetadata) {
        this.imageMetadata = imageMetadata;
    }

    @Column(name = "ssl_check", length = 255)
    public String getLocationSslCheck() {
        return locationSslCheck;
    }

    public void setLocationSslCheck(String locationSslCheck) {
        this.locationSslCheck = locationSslCheck;
    }

    public Boolean isReduced() {
        return reduced;
    }

    @Column(name = "reduced")
    public void setReduced(Boolean reduced) {
        this.reduced = reduced;
    }

    @Column(name = "resolution")
    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    @JsonIgnore
    @OneToMany(cascade = {CascadeType.REMOVE}, mappedBy = "location")
    public List<PublicStatistic> getPublicStatistic() {
        return publicStatistic;
    }

    public void setPublicStatistic(List<PublicStatistic> publicStatistic) {
        this.publicStatistic = publicStatistic;
    }

    @JsonIgnore
    @OneToMany(cascade = {CascadeType.REMOVE}, mappedBy = "location")
    public List<Requests> getRequests() {
        return requests;
    }

    public void setRequests(List<Requests> requests) {
        this.requests = requests;
    }

    @JsonIgnore
    @OneToMany(cascade = {CascadeType.REMOVE}, mappedBy = "location")
    public List<ReportEmails> getReportEmails() {
        return reportEmails;
    }

    public void setReportEmails(List<ReportEmails> reportEmails) {
        this.reportEmails = reportEmails;
    }

    @JsonIgnore
    @OneToMany(cascade = {CascadeType.ALL}, mappedBy = "location")
    @JoinTable(name = "location_pictures", joinColumns = {@JoinColumn(name = "location_id")}, inverseJoinColumns = {@JoinColumn(name = "picture_id")})
    public List<Picture> getLocationAddPictures() {
        return locationAddPictures;
    }

    public void setLocationAddPictures(List<Picture> locationAddPictures) {
        this.locationAddPictures = locationAddPictures;
    }


    @JsonIgnore
    @OneToMany(cascade = {CascadeType.REMOVE}, mappedBy = "location")
    public List<Subscribe> getSubscribeEmails() {
        return subscribeEmails;
    }

    public void setSubscribeEmails(List<Subscribe> subscribeEmails) {
        this.subscribeEmails = subscribeEmails;
    }

    public Boolean getTagged() {
        return tagged;
    }

    @Column(name = "tagged")
    public void setTagged(final Boolean tagged) {
        this.tagged = tagged;
    }

    public Boolean getBcconly() {
        return bcconly;
    }

    @Column(name = "bcconly")
    public void setBcconly(Boolean bcconly) {
        this.bcconly = bcconly;
    }

    @Column(name = "giveaway")
    public Boolean getGiveaway() {
        return giveaway;
    }

    public void setGiveaway(Boolean giveaway) {
        this.giveaway = giveaway;
    }

    @Column(name = "fwpenable")
    public Boolean getFwpenable() {
        return fwpenable;
    }

    public void setFwpenable(Boolean fwpenable) {
        this.fwpenable = fwpenable;
    }

    @Column(name = "rfid")
    public Boolean getRfid() {
        return rfid;
    }

    public void setRfid(Boolean rfid) {
        this.rfid = rfid;
    }

    @Column(name = "camera")
    public Boolean getCamera() {
        return camera;
    }

    public void setCamera(Boolean camera) {
        this.camera = camera;
    }

    @Column(name = "photobook")
    public Boolean getPhotobook() {
        return photobook;
    }

    public void setPhotobook(Boolean photobook) {
        this.photobook = photobook;
    }

    @Column(name = "savestaff")
    public Boolean getStaffsave() {
        return staffsave;
    }

    public void setStaffsave(Boolean staffsave) {
        this.staffsave = staffsave;
    }

    @Column (name = "slide_show_in_new_window")
    public Boolean getSlideShowInNewWindow () {
        return slideShowInNewWindow;
    }

    public void setSlideShowInNewWindow (Boolean slideShowInNewWindow) {
        this.slideShowInNewWindow = slideShowInNewWindow;
    }

}
