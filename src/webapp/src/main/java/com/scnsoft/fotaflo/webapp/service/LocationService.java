package com.scnsoft.fotaflo.webapp.service;

import com.scnsoft.fotaflo.webapp.bean.LocationBean;
import com.scnsoft.fotaflo.webapp.dao.ILocationDao;
import com.scnsoft.fotaflo.webapp.dao.IMainLogoDao;
import com.scnsoft.fotaflo.webapp.dao.impl.BaseDao;
import com.scnsoft.fotaflo.webapp.model.*;
import com.scnsoft.fotaflo.webapp.util.PictureReseizer;
import com.scnsoft.fotaflo.webapp.util.Resolution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */
@Service(value = "locationService")
@Transactional(propagation = Propagation.REQUIRED)
public class LocationService {

    private static String LOGO_DIR = "logo";
    public static String LOGO_IMAGE_BASE_PATH = PictureService.IMG_DIR + LOGO_DIR;

    @Autowired
    private BaseDao baseDao;

    @Autowired
    private ILocationDao locationDao;

    @Autowired
    private IMainLogoDao mainLogoDao;

    @Autowired
    private UserService userService;

    @Autowired
    private PictureService pictureService;

    @Autowired
    private PackageService packageService;

    public List<LocationBean> getUserLocations(String userlogin) {
        List<LocationBean> list = new ArrayList<LocationBean>();
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        if (systemUser.getAccess().equals("1")) {
            List<Location> result = baseDao.retrieveAll(Location.class);
            if (result != null && !result.isEmpty()) {
                for (Location l : result) {
                    if (l != null) {
                        list.add(new LocationBean(l.getId(), l.getLocationName()));
                    }
                }
            }
            list.add(createAllLocationBean());
        } else {
            if (systemUser.getLocation() != null) {
                list.add(new LocationBean(systemUser.getLocation().getId(), systemUser.getLocation().getLocationName()));
            }
        }
        return list;
    }

    public List<Location> getUserLocationsClean(String userlogin) {
        List<Location> list = new ArrayList<Location>();
        list.addAll(baseDao.retrieveAll(Location.class));
        return list;
    }

    public List<Location> getUserRealLocations(String userlogin, int start, int limit) {
        List result = new ArrayList<Location>();
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        if (systemUser.getAccess().equals("1")) {
            List<Location> items = null;
            if(limit > 0){
                items = locationDao.find(start, limit);
            }else{
                items = locationDao.list();
            }
            if(items != null){
                result = items;
            }
        } else {
            result.add(systemUser.getLocation());
        }
        return result;
    }

    public Long getUserRealLocationsCount(String userlogin) {
        long result = 0;
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        if (systemUser.getAccess().equals("1")) {
            result = locationDao.count();
        } else {
            result = 0;
        }
        return result;
    }

    public List<LocationBean> getActualLocations(String user) {
        List<Location> items = new ArrayList<Location>();
        SystemUser systemUser = userService.getUserByLogin(user);
        if (systemUser.getAccess().equals("1")) {
            items = locationDao.list();
        } else {
            if (systemUser.getLocation() != null) {
                items.add(systemUser.getLocation());
            }
        }

        return getLocationBeans(items);
    }

    public List<LocationBean> getAllLocations() {
        List<Location> items = locationDao.list();

        return getLocationBeans(items);
    }

    protected List<LocationBean> getLocationBeans(List<Location> items){
        List<LocationBean> result = new ArrayList<LocationBean>();

        if(items != null && !items.isEmpty()){
            for (Location location: items){
                if(location != null){
                    result.add(new LocationBean(location.getId(), location.getLocationName()));
                }
            }
        }

        return result;
    }

    public Location getLocationById(String locationId) {
        if (locationId == null || locationId.isEmpty()) {
            return null;
        }
        Location location = (Location) baseDao.retrieve(Location.class, Integer.parseInt(locationId));
        return location;
    }

    public void updateLocationTextLogo(Integer locationId, String locationTextLogo) {
        Location location = getLocation(locationId);
        location.setLocationTextLogo(locationTextLogo);
        baseDao.update(location);
    }

    public void updateLocationImageLogo(Integer locationId, MultipartFile locationImageLogo)
            throws IOException {
        Location location = getLocation(locationId);
        String picturePath = pictureService.getPicturePath();
        String fileName = location.getLocationName() + LOGO_DIR;
        Pattern pattern = Pattern.compile(PictureService.FILENAME_REGEXP);
        Matcher matcher = pattern.matcher(locationImageLogo.getOriginalFilename());
        matcher.find();
        fileName += "." + matcher.group(2);
        location.setLocationImageLogoUrl(LOGO_IMAGE_BASE_PATH + File.separator + fileName);
        baseDao.update(location);
        boolean created = PictureReseizer.createPictureDirectorty(picturePath + LOGO_IMAGE_BASE_PATH +
                File.separator);
        if (created) {
            File logoImage = new File(picturePath + (LOGO_IMAGE_BASE_PATH +
                    File.separator + fileName));
            logoImage.createNewFile();
            locationImageLogo.transferTo(logoImage);
        }
    }

    public void deleteLocationImageLogo(Integer locationId) {
        Location location = getLocation(locationId);
        location.setLocationImageLogoUrl("");
        baseDao.update(location);
    }

    public void updateLocationPayPalSettings(Integer locationId, PayPalLocationSettings payPalLocationSettings) {
        Location location = getLocation(locationId);
        location.setPaypallocationSettings(payPalLocationSettings);
        baseDao.update(location);
    }

    public void updateLocationToFlickrEmail(Integer locationId, String toFlickrEmail) {
        Location location = getLocation(locationId);
        location.setLocationToFlickrEmail(toFlickrEmail);
        baseDao.update(location);
    }

    public void updateLocationToFacebookEmail(Integer locationId, String toFacebookEmail) {
        Location location = getLocation(locationId);
        location.setLocationToFacebookEmail(toFacebookEmail);
        baseDao.update(location);
    }

    public void updateLocationSmtpServer(Integer locationId, String smtpServer) {
        Location location = getLocation(locationId);
        location.setLocationSmtpServer(smtpServer);
        baseDao.update(location);
    }

    public void updateLocationFromEmail(Integer locationId, String fromEmail) {
        Location location = getLocation(locationId);
        location.setLocationFromEmail(fromEmail);
        baseDao.update(location);
    }

    public void updateLocationToEmail(Integer locationId, String toEmail) {
        Location location = getLocation(locationId);
        location.setLocationToEmail(toEmail);
        baseDao.update(location);
    }

    public void updateLocationToPhotobookEmail(Integer locationId, String email) {
        Location location = getLocation(locationId);
        location.setLocationToPhotobookEmail(email);
        baseDao.update(location);
    }

    public void updateLocationFromEmailPassword(Integer locationId, String fromEmailPassword) {
        Location location = getLocation(locationId);
        location.setLocationSmtpServerPassword(fromEmailPassword);
        baseDao.update(location);
    }

    public void updateLocationSmtpServerPort(Integer locationId, Integer smtpServerPort) {
        Location location = getLocation(locationId);
        location.setLocationSmtpServerPort(smtpServerPort);
        baseDao.update(location);
    }

    public void updateLocationSmtpSslCheck(Integer locationId, String sslcheck) {
        Location location = getLocation(locationId);
        String sslString = (sslcheck != null && sslcheck.trim().equals("on")) ? "on" : "off";
        location.setLocationSslCheck(sslString);
        baseDao.update(location);
    }

    public Location getLocation(Integer id) {
        return locationDao.findById(id);
    }

    public void deleteLocations(Integer[] ids) {
        for (Integer id : ids) {
            Location location = (Location) baseDao.retrieve(Location.class, id);
            MainLogo mainLogo = mainLogoDao.getMainLogoByLocation(location);
            if (mainLogo != null) {
                baseDao.delete(mainLogo);
            }
            try {
                pictureService.deleteSlideShowBanner(location, "right");
                pictureService.deleteSlideShowBanner(location, "left");
            } catch (IOException e) {
                e.printStackTrace();
            }
            //Deleting public packages for the location
            packageService.deletePublicPackagesForLocation(location);

            baseDao.delete(Location.class, id);
        }
    }

    public List<Location> saveLocations(List<Location> locations) {
        for (Location location : locations) {
            Integer locationId = location.getId();
            if (locationId != null && locationId != 0) {
                Location loc = (Location) baseDao.retrieve(Location.class, locationId);
                if (loc != null) {
                    loc.setLocationName(location.getLocationName());
                    if (location.getResolution() != null && location.getResolution().equals(Resolution.REDUCED.getCode())) {
                        loc.setReduced(true);
                    } else {
                        loc.setReduced(false);
                    }
                    loc.setResolution(location.getResolution());
                    loc.setTagged(location.getTagged());
                    loc.setBcconly(location.getBcconly() == null ? false : location.getBcconly());
                    loc.setGiveaway(location.getGiveaway() == null ? false : location.getGiveaway());
                    loc.setFwpenable(location.getFwpenable() == null ? false : location.getFwpenable());
                    loc.setRfid(location.getRfid() == null ? false : location.getRfid());
                    loc.setCamera(location.getCamera() == null ? false : location.getCamera());
                    loc.setPhotobook(location.getPhotobook() == null ? false : location.getPhotobook());
                    loc.setStaffsave(location.getStaffsave() == null ? false : location.getStaffsave());
                    loc.setSlideShowInNewWindow (location.getSlideShowInNewWindow () == null ? false : location.getSlideShowInNewWindow ());
                    baseDao.update(loc);

                    if (location.getStaffsave() == false) {
                        String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
                        pictureService.updateStaffs(currentUser, "");
                    }
                }
            } else {
                location.setReduced(false);
                location.setResolution(Resolution.STANDARD.getCode());
                baseDao.create(location);
            }

        }
        return locations;
    }

    public Location saveLocation(Location location) {
        List<Location> locationAsList = Arrays.asList(location);
        saveLocations(locationAsList);
        return location;
    }

    public Location updateImageMetadata(Integer locationId, String title, String comment, String copyright) {
        Location location = getLocation(locationId);
        if (location.getImageMetadata() == null) {
            location.setImageMetadata(new LocationImageMetadata());
        }
        LocationImageMetadata metadata = location.getImageMetadata();
        metadata.setTitleMetadata(title);
        metadata.setCommentMetadata(comment);
        metadata.setCopyrightMetadata(copyright);
        return saveLocation(location);

    }

    public Location createAllLocation() {
        Location location = new Location();
        location.setId(0);
        location.setLocationName("All locations");
        return location;
    }

    public LocationBean createAllLocationBean() {
        Location location = createAllLocation();
        return new LocationBean(location.getId(), location.getLocationName());
    }
}
