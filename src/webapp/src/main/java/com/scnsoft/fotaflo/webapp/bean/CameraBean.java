package com.scnsoft.fotaflo.webapp.bean;

import com.scnsoft.fotaflo.webapp.model.Camera;

/**
 * Created by Paradinets Tatsiana
 * Date: 08.04.2011
 * Time: 14:12:50
 */

public class CameraBean {

    protected int id;
    String cameraName;
    String location;

    public CameraBean(int id, String cameraName, String location) {
        this.id = id;
        this.cameraName = cameraName;
        this.location = location;
    }

    public CameraBean(Camera camera) {
        this.id = camera.getId();
        this.cameraName = camera.getCameraName();
        this.location = (camera.getLocation() != null) ? String.valueOf(camera.getLocation().getId()) : "0";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCameraName() {
        return cameraName;
    }

    public void setCameraName(String cameraName) {
        this.cameraName = cameraName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
