package com.scnsoft.fotaflo.webapp.controller;

import com.scnsoft.fotaflo.webapp.model.RegisteredDevice;
import com.scnsoft.fotaflo.webapp.service.DeviceService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/device")
public class DeviceController {

    @Autowired
    private DeviceService deviceService;

    @RequestMapping(value = "register", method = RequestMethod.POST)
    @ResponseBody
    @Transactional(readOnly = false)
    public ModelMap registerDevice(@RequestParam("registrationID") String registrationID) {
        ModelMap result = new ModelMap();
        RegisteredDevice device = createDevice(registrationID);
        if (device != null) {
            deviceService.save(device);
            result.put("success", true);
        } else {
            result.put("success", false);
            result.put("msg", "Invalid registration ID was provided");
        }

        return result;
    }

    private RegisteredDevice createDevice(String registrationID) {
        if (StringUtils.isEmpty(registrationID)) {
            return null;
        }
        if (deviceService.getByRegisteredId(registrationID) != null) {
            return null;
        }
        RegisteredDevice device = new RegisteredDevice();
        device.setRegisteredID(registrationID);

        return device;
    }
}
