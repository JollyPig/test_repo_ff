package com.scnsoft.fotaflo.webapp.util;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */
public class DefaultValues {

    public final static Long startDate = getStartDate();
    public final static Long endDate = getEndDate();
    public final static String pageSize = "20";
    public final static Integer startTime = 7 * 24 * 60 * 60;
    public final static Integer pictureCount = 4;
    public final static Integer speed = 5;

    public final static int pictureWidth = 1024;
    public final static int pictureHeight = 768;
    public final static int pictureWidthFormat2 = 1024;
    public final static int pictureHeightFormat2 = 576;
    public final static float opacity = 1f;
    public final static int fontSize = 40;
    public final static int fontSizeReduced = 30;
    public static String img_path = "";
    public static String logo_path = "";

    public static Resolution findByCode(String code) {
        if (code == null) return Resolution.STANDARD;
        if (code.equals("0")) {
            return Resolution.REDUCED;
        }
        if (code.equals("1")) {
            return Resolution.STANDARD;
        }
        if (code.equals("2")) {
            return Resolution.MP3;
        }
        if (code.equals("3")) {
            return Resolution.MP5;
        }
        if (code.equals("4")) {
            return Resolution.X16X9;
        }
        return Resolution.STANDARD;
    }

    public static Long getStartDatePublic(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);

        return cal.getTimeInMillis();
    }

    public static Long getEndDatePublic(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 00);

        return cal.getTimeInMillis();
    }


    public static Long getStartDate() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int d = cal.get(Calendar.HOUR_OF_DAY) - 12;
        cal.set(Calendar.HOUR_OF_DAY, d);
        return cal.getTimeInMillis();
    }

    public static Long getEndDate() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int d = cal.get(Calendar.HOUR_OF_DAY) + 12;
        cal.set(Calendar.HOUR_OF_DAY, d);
        return cal.getTimeInMillis();
    }

    public static Long getHistoryStartDate() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int d = cal.get(Calendar.HOUR_OF_DAY) - 24;
        cal.set(Calendar.HOUR_OF_DAY, d);
        return cal.getTimeInMillis();
    }


    public static Date getStartDateDate() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int d = cal.get(Calendar.HOUR_OF_DAY) - 12;
        cal.set(Calendar.HOUR_OF_DAY, d);
        return cal.getTime();
    }

    public static Date getEndDateDate() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int d = cal.get(Calendar.HOUR_OF_DAY) + 12;
        cal.set(Calendar.HOUR_OF_DAY, d);
        return cal.getTime();
    }

}
