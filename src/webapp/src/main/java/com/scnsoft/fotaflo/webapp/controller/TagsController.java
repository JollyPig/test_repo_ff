package com.scnsoft.fotaflo.webapp.controller;

import com.scnsoft.fotaflo.webapp.bean.CodeBean;
import com.scnsoft.fotaflo.webapp.bean.SendMessagesBean;
import com.scnsoft.fotaflo.webapp.bean.TagBean;
import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.model.SystemUser;
import com.scnsoft.fotaflo.webapp.model.Tag;
import com.scnsoft.fotaflo.webapp.model.TagPurchase;
import com.scnsoft.fotaflo.webapp.service.*;
import com.scnsoft.fotaflo.webapp.util.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 03.10.2012
 * Time: 7:48:59
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class TagsController {
    protected static Logger logger = Logger.getLogger(TagsController.class);

    private final static String locationId = "location";

    @Autowired
    TagService tagService;

    @Autowired
    CameraService cameraService;

    @Autowired
    LocationService locationService;

    @Autowired
    UserService userService;

    @Autowired
    TagPurchaseService tagPurchaseService;

    @Autowired
    PublicLoginService publicLoginService;

    @Autowired
    private SendMessagesTask sendMessagesTask;

    @Autowired
    private PictureService pictureService;

    @Autowired
    MailService mailService;

    private final static String successProperty = "success";
    private final static String errorProperty = "error";

    @RequestMapping(value = "getTags.json")
    public
    @ResponseBody
    Map<String, ? extends Object> loadTags(HttpServletRequest httpServletRequest, @RequestParam(value = TagsController.locationId, required = false) String locationId) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        HashMap<String, List<TagBean>> modelMap = new HashMap<String, List<TagBean>>();
        SystemUser systemUser = userService.getUserByLogin(currentUser);
        Location location = null;

        if (systemUser.getAccess().equals(String.valueOf(AccessID.ADMIN.getValue()))) {
            if (locationId != null && !locationId.equals("") && !locationId.equals("0")) {
                location = locationService.getLocation(Integer.valueOf(locationId));
            }
        } else {
            location = systemUser.getLocation();
        }

        String mandatoryTags = httpServletRequest.getParameter("mandatoryTags");

        Filter filter = FotafloUtil.initFilterForCatalog(httpServletRequest);
        String pageLimit = pictureService.getPageSize(currentUser);
        int start = FotafloUtil.parseParameterToInteger(httpServletRequest, "start", "0");
        int limit = FotafloUtil.parseParameterToInteger(httpServletRequest, "limit", pageLimit);

        List<TagBean> result = tagService.getPictureTags(currentUser, filter, location, mandatoryTags, start, limit);

        modelMap.put("tags", result);

        return modelMap;
    }

    @RequestMapping(value = "getExistedTags.json")
    public
    @ResponseBody
    Map<String, ? extends Object> loadExistedTags(@RequestParam(value = TagsController.locationId, required = false) String locationId,
                                                  @RequestParam(value = "startdate", required = false) String startDate,
                                                  @RequestParam(value = "startTime", required = false) String startTime,
                                                  @RequestParam(value = "enddate", required = false) String endDate,
                                                  @RequestParam(value = "endTime", required = false) String endTime) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        HashMap<String, List<TagBean>> modelMap = new HashMap<String, List<TagBean>>();
        SystemUser systemUser = userService.getUserByLogin(currentUser);
        Location location = null;

        if (systemUser.getAccess().equals(String.valueOf(AccessID.ADMIN.getValue()))) {
            if (locationId != null && !locationId.equals("")) {
                location = locationService.getLocation(Integer.valueOf(locationId));
            }
        } else {
            location = systemUser.getLocation();
        }

        List<TagBean> tagbeans = tagService.getExistedTagsByLocationDate(DateConvertationUtils.formDate(startDate, startTime, new Date()),
                DateConvertationUtils.formDate(endDate, endTime, new Date()), location, systemUser);
        modelMap.put("tags", tagbeans);

        return modelMap;
    }

    @RequestMapping(value = "getTagsQuery.json")
    public
    @ResponseBody
    Map<String, ? extends Object> getTagsQuery(HttpServletRequest httpServletRequest) {

        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        String locationID = (httpServletRequest.getParameter("location") != null ? httpServletRequest.getParameter("location") : "0");

        HashMap<String, List<TagBean>> modelMap = new HashMap<String, List<TagBean>>();
        modelMap.put("tagsQuery", tagService.getTagsByLocation(locationService.getLocation(Integer.valueOf(locationID)), userService.getUserByLogin(currentUser)));
        return modelMap;
    }

    @RequestMapping(value = "getTagsByQuery.json")
    public
    @ResponseBody
    Map<String, ? extends Object> loadCamerasQuery(HttpServletRequest httpServletRequest) {
        String query = (httpServletRequest.getParameter("query") != null ? httpServletRequest.getParameter("query") : "0");

        HashMap<String, List<TagBean>> modelMap = new HashMap<String, List<TagBean>>();
        modelMap.put("tagsQuery", tagService.getTagsByQuery(query));
        return modelMap;
    }

    @RequestMapping("checkTagsGrouping.json")
    public
    @ResponseBody
    Map<String, Object> checkTagsGrouping(HttpServletRequest httpServletRequest) {
        String addedTagId = httpServletRequest.getParameter("tag");
        String startDate = httpServletRequest.getParameter("startdate");
        String startTime = httpServletRequest.getParameter("starttime");
        String endDate = httpServletRequest.getParameter("enddate");
        String endTime = httpServletRequest.getParameter("endtime");
        Map<String, Object> sets = tagService.getTagGroups(DateConvertationUtils.formDate(startDate, startTime, new Date()),
                DateConvertationUtils.formDate(endDate, endTime, new Date()), addedTagId);
        return sets;
    }

    @RequestMapping("getChangedTime.json")
    public
    @ResponseBody
    Map<String, Object> getChangedTime(HttpServletRequest httpServletRequest) {

        String addedTagId = httpServletRequest.getParameter("tag");
        String startDate = httpServletRequest.getParameter("startdate");
        String startTime = httpServletRequest.getParameter("starttime");
        String endDate = httpServletRequest.getParameter("enddate");
        String endTime = httpServletRequest.getParameter("endtime");
        Map<String, Object> sets = tagService.getTagNewTime(DateConvertationUtils.formDate(startDate, startTime, new Date()),
                DateConvertationUtils.formDate(endDate, endTime, new Date()), addedTagId);
        return sets;
    }

    @RequestMapping(value = "completeTag.json")
    public
    @ResponseBody
    Map<String, ? extends Object> completeTag(@RequestParam(value = "locationId", required = false) String idLocation,
                                              @RequestParam(value = "package", required = false) String idPackage,
                                              @RequestParam(value = "staff", required = false) String idStaff,
                                              @RequestParam(value = "participant", required = false) Integer participant,
                                              @RequestParam(value = "subject", required = false) String subject,
                                              @RequestParam(value = "strfilename", required = false) String attach_filename,
                                              @RequestParam(value = "emailbody", required = false) String email_body,
                                              @RequestParam(value = "startDate", required = false) String startDate,
                                              @RequestParam(value = "endDate", required = false) String endDate,
                                              @RequestParam(value = "startTime", required = false) String startTime,
                                              @RequestParam(value = "endTime", required = false) String endTime,
                                              @RequestParam(value = "emails", required = false) Object emails,
                                              @RequestParam(value = "tagsToComplete", required = false) Object tagsToComplete,
                                              HttpServletRequest httpServletRequest) {
        //We need to complete the tags or create the tags which did not exist and complete them

        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        SystemUser user = userService.getUserByLogin(currentUser);
        List<String> tagNames = PictureUtil.getPurchaseEmails(tagsToComplete);

        String taglogin = publicLoginService.generateABTPublicLoginCode("T");

        TagPurchase tagPurchase = tagPurchaseService.createTagPurchased(taglogin, user, idLocation, idPackage, participant,
                idStaff, subject, attach_filename, email_body, emails, startDate, endDate, startTime, endTime);

        Set<Tag> tags = new HashSet<Tag>();

        Location location = user.getLocation();

        for (String tagName : tagNames) {
            Tag tag = tagService.getTagByNameLocation(tagName, location);

            //CREATE TAG IF IT DOES NOT EXISTS
            if (tag == null) {
                //Tag does not exist create it
                tag = tagPurchaseService.createTag(user, tagName);
            }
            SystemUser tagUser = userService.getUserByLogin(tagName);

            userService.makeTagAsACode(tagUser);

            tags.add(tag);
            Set<TagPurchase> tagPurchases = tag.getTagPurchases();
            tagPurchases.add(tagPurchase);

            tagService.updateTag(tag);
        }
        tagPurchase.setTags(tags);

        tagPurchaseService.updateTagPurchase(tagPurchase);

        publicLoginService.createTagPublicLogin(taglogin, location, new Date(), String.valueOf(AccessID.PUBLICTAGCODEUSER.getValue()));
        //creating users according to emails

        HashMap<String, Object> modelMap = new HashMap<String, Object>();

        modelMap.put("successProperty", true);
        modelMap.put("code", taglogin);

        pictureService.updateStringFileName(currentUser, attach_filename);
        pictureService.updateMessageBody(currentUser, email_body);

        //TO DO START SENDING THE EMAIL
        //GENERATE USERS ACCORDING TO SAVED PURCHASES
        List<Map<String, String>> toSendEmailsCredentials = new ArrayList<Map<String, String>>();

        int numberOfParticipant = participant == null ? 0 : Integer.valueOf(participant);
        for (int i = 1; i <= numberOfParticipant; i++) {
            String email = httpServletRequest.getParameter("email" + i);
            if (email != null && email.length() != 0 && !email.trim().equals("")) {
                Date expirationDate2 = DateConvertationUtils.getStartDate(-365, new Date());
                Map<String, String> credentials = publicLoginService.generateACodesFromPurchaseCode(taglogin, i, location, new Date(), expirationDate2, String.valueOf(
                        AccessID.PUBLICTAGCODEUSER.getValue()));

                credentials.put("email", email);
                toSendEmailsCredentials.add(credentials);
            }
        }

        List<SendMessagesBean> messagesToSend = new ArrayList<SendMessagesBean>();
        if (toSendEmailsCredentials.size() > 0 && (user.getAccess().equals(String.valueOf(AccessID.ADMIN.getValue())) || user.getAccess().equals(String.valueOf(AccessID.USER.getValue())))) {
            for (Map<String, String> toSendEmailCredentials : toSendEmailsCredentials) {
                SendMessagesBean sendBean = mailService.createACodeEmail(toSendEmailCredentials, subject, email_body, user.getLocation());
                messagesToSend.add(sendBean);
            }
        }
        sendMessagesTask.setMessages(messagesToSend);
        sendMessagesTask.sendMessages();

        boolean staffsaveEnabled = (location != null && location.getStaffsave() != null && location.getStaffsave() == true);
        pictureService.updateStaffs(currentUser, staffsaveEnabled ? idStaff : "");

        return modelMap;
    }

    @RequestMapping(value = "addTag.json")
    public
    @ResponseBody
    Map<String, ? extends Object> addTag(@RequestParam(value = "tag", required = true) String tagName) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        SystemUser user = userService.getUserByLogin(currentUser);
        Tag tag = tagService.getTagByNameLocation(tagName, user.getLocation());

        HashMap<String, Object> modelMap = new HashMap<String, Object>();
        if (tag == null) {
            tag = tagPurchaseService.createTag(user, tagName);
            if (tag != null) {
                modelMap.put(successProperty, true);
            } else {
                modelMap.put(errorProperty, "Error occured suring the tag creation");
            }
        } else {

            modelMap.put(errorProperty, "The tag already exist");
        }
        return modelMap;
    }

    @RequestMapping(value = "refundTag.json")
    public
    @ResponseBody
    Map<String, ? extends Object> refundTag(@RequestParam(value = "tag", required = true) String tagToRefund) {
        //search for tag
        logger.info("TAG TO REFUND " + tagToRefund);
        HashMap<String, Object> modelMap = new HashMap<String, Object>();
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        SystemUser user = userService.getUserByLogin(currentUser);

        tagPurchaseService.refundSystemUser(tagToRefund);
        Tag tag = tagService.getTagByNameLocation(tagToRefund, user.getLocation());

        if (tag != null) {
            Set<TagPurchase> tagPurchasesSet = tag.getTagPurchases();
            logger.info("PUCRAHSES TO REFUND " + tagPurchasesSet.size());
            //we r refunding all tag purchases // set the status of tagPurchase to 2
            if (!tagPurchasesSet.isEmpty()) {
                for (TagPurchase tagPurchase : tagPurchasesSet) {
                    logger.info("REFUNDING PURCHASE: " + tagPurchase.getGeneratedCode());
                    tagPurchaseService.refundTag(tagPurchase);
                }
            }
            modelMap.put(successProperty, true);
        } else {
            modelMap.put(errorProperty, "The tag to refund does not exist");
        }

        return modelMap;
    }

    @RequestMapping(value = "refundCode.json")
    public
    @ResponseBody
    Map<String, ? extends Object> refundCode(@RequestParam(value = "code", required = true) String codeToRefund) {
        HashMap<String, Object> modelMap = new HashMap<String, Object>();
        //search for tag
        logger.info("CODE TO REFUND " + codeToRefund);
        TagPurchase tagPurchase = tagPurchaseService.getPurchaseTagByCode(codeToRefund);
        logger.info("tagPurchase " + tagPurchase);

        if (tagPurchase != null) {
            tagPurchaseService.refundTag(tagPurchase);

            // Refund tags related to tag purchase
            for(Tag tag: tagPurchase.getTags()){
                tagPurchaseService.refundSystemUser(tag.getTagName());
            }
            modelMap.put(successProperty, true);
        } else {
            modelMap.put(errorProperty, "The code can not be refunded as the purchase with this code does not exist");
        }

        return modelMap;
    }

    @RequestMapping(value = "getTagPurchaseCodes.json")
    public
    @ResponseBody
    Map<String, ? extends Object> getTagPurchaseCodes(@RequestParam(value = "location", required = false) String idLocation,
                                                      @RequestParam(value = "startDate", required = false) String startDate,
                                                      @RequestParam(value = "endDate", required = false) String endDate,
                                                      @RequestParam(value = "startTime", required = false) String startTime,
                                                      @RequestParam(value = "endTime", required = false) String endTime) {
        HashMap<String, Object> modelMap = new HashMap<String, Object>();
        //search for tag
        Location location = locationService.getLocation(Integer.parseInt(idLocation));

        List<CodeBean> tagCodes = tagPurchaseService.getPurchaseTagByTimeRangeLocation
                (DateConvertationUtils.formDate(startDate, startTime, new Date()), DateConvertationUtils.formDate(endDate, endTime, new Date()), location);

        modelMap.put("success", true);
        modelMap.put("codes", tagCodes);

        return modelMap;
    }

}
