package com.scnsoft.fotaflo.webapp.service;

import com.scnsoft.fotaflo.webapp.dao.IDeviceDao;
import com.scnsoft.fotaflo.webapp.model.RegisteredDevice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class DeviceService {

    @Autowired
    IDeviceDao deviceDao;

    public List<RegisteredDevice> getAll() {
        return deviceDao.list();
    }

    public void delete(RegisteredDevice device) {
        deviceDao.delete(device);
    }

    public void save(RegisteredDevice device) {
        deviceDao.create(device);
    }

    public RegisteredDevice getByRegisteredId(String registeredID) {
        return deviceDao.getByRegisteredId(registeredID);
    }

}
