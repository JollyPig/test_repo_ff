package com.scnsoft.fotaflo.webapp.service;

import com.scnsoft.fotaflo.webapp.bean.PreviewPrintBean;
import com.scnsoft.fotaflo.webapp.bean.PurchasePictureBean;
import com.scnsoft.fotaflo.webapp.model.Location;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class PrintService {

    protected static Logger logger = Logger.getLogger(MailService.class);

    @Autowired
    private PictureService pictureService;

    public PreviewPrintBean createPreviewPrintBean(PurchasePictureBean pictureBean, Location location, Integer numbersToPrint) {
        String mainLogo = pictureService.getMainLogo(location);
        mainLogo = mainLogo != null ? mainLogo.replace(File.separator, "/") : "";
        String locationImgLogoUrl = (location.getLocationImageLogoUrl() == null) ? null
                : location.getLocationImageLogoUrl().replace("/", File.separator);
        locationImgLogoUrl = locationImgLogoUrl != null ? locationImgLogoUrl.replace(File.separator, "/") : "";
        String locationTextLogo = location.getLocationTextLogo();
        return new PreviewPrintBean(pictureBean.getUrl(), mainLogo, locationImgLogoUrl, locationTextLogo, numbersToPrint);
    }
}
