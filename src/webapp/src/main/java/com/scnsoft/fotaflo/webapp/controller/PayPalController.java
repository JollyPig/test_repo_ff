package com.scnsoft.fotaflo.webapp.controller;

import com.paypal.sdk.exceptions.PayPalException;
import com.paypal.sdk.profiles.APIProfile;
import com.paypal.sdk.profiles.ProfileFactory;
import com.paypal.sdk.services.CallerServices;
import com.paypal.soap.api.*;
import com.scnsoft.fotaflo.common.util.ServletUtil;
import com.scnsoft.fotaflo.webapp.model.*;
import com.scnsoft.fotaflo.webapp.service.PayPalService;
import com.scnsoft.fotaflo.webapp.service.PictureService;
import com.scnsoft.fotaflo.webapp.service.RequestService;
import com.scnsoft.fotaflo.webapp.service.UserService;
import com.scnsoft.fotaflo.webapp.util.AccessID;
import com.scnsoft.fotaflo.webapp.util.DateConvertationUtils;
import com.scnsoft.fotaflo.webapp.util.DefaultValues;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 09.02.2012
 * Time: 17:17:13
 * To change this template use File | Settings | File Templates.
 */

@Controller
public class PayPalController {

    @Autowired
    private PictureService pictureService;

    @Autowired
    private UserService userService;

    @Autowired
    private PayPalService payPalService;

    @Autowired
    private RequestService requestService;

    protected static Logger logger = Logger.getLogger(PayPalController.class);

    private final static String successProperty = "success";

    private final static String msgProperty = "msg";

    private final static String error = "isError";

    private final static String response = "response";

    private final static String statusRegistered = "REGISTERED";

    private final static String statusComplete = "COMPLETE";

    private final static String trueString = "true";

    private final static String falseString = "false";

    private final static String CONFIRM_REDIRECT_URL = "/pictures/main/purchasepayed";

    private final static String CANCEL_REDIRECT_URL = "/pictures/main/purchase";

    @Value("${fotaflo.paypal.url}")
    private String payPalUrl;

    @Value("${facebook.clientId}")
    private String facebookAppId;

    public String getPayPalRedirectUrl() {
        return payPalUrl;
    }


    @RequestMapping(value = "registerPayment.json")
    public
    @ResponseBody
    Map<String, ? extends Object> getRegisterPayment(HttpServletRequest httpServletRequest, String totalPrice, String email)
            throws Exception {

        HashMap<String, Object> modelMap = new HashMap<String, Object>();
        if (totalPrice == null || totalPrice.equals("0")) {
            modelMap.put(successProperty, false);
            modelMap.put(msgProperty, "Total price error. Please contact administrator.");
            logger.warn("Total price error. Please contact administrator.");
            return modelMap;
        }
        Double total = null;
        try {
            total = Double.parseDouble(totalPrice);
        } catch (NumberFormatException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        if (total == null) {
            modelMap.put(successProperty, false);
            modelMap.put(msgProperty, "Total price is incorrect. Please contact administrator.");
            logger.warn("Total price is incorrect. Please contact administrator.");
            return modelMap;
        }
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        SystemUser user = userService.getUserByLogin(currentUser);
        if (user == null) {
            modelMap.put(successProperty, false);
            modelMap.put(msgProperty, "User error. Please contact administrator.");
            logger.warn("User error. Please contact administrator.");
            return modelMap;
        }

        Location location = user.getLocation();
        if (location == null) {
            modelMap.put(successProperty, false);
            modelMap.put(msgProperty, "Location error. Please contact administrator.");
            logger.warn("Location error. Please contact administrator.");
            return modelMap;
        }
        Merchant merchant = payPalService.getMerchantAccount();
        if (merchant == null) {
            modelMap.put(successProperty, false);
            modelMap.put(msgProperty, "Location PayPal merchant settings error. Please contact administrator.");
            logger.warn("Location PayPal merchant settings error. Please contact administrator.");
            return modelMap;
        }
        PayPalLocationSettings payPalLocationSettings = location.getPaypallocationSettings();
        if (payPalLocationSettings == null) {
            modelMap.put(successProperty, false);
            modelMap.put(msgProperty, "Location PayPal settings error. Please contact administrator.");
            logger.warn("Location PayPal settings error. Please contact administrator.");
            return modelMap;
        }
        if (payPalLocationSettings.getCurrency() == null || payPalLocationSettings.getCurrency().trim().equals("")) {
            modelMap.put(successProperty, false);
            modelMap.put(msgProperty, "Currency of the location error. Please contact administrator.");
            logger.warn("Currency of the location error. Please contact administrator.");
            return modelMap;
        }
        if (StringUtils.isEmpty(merchant.getAccount()) || StringUtils.isEmpty(merchant.getPassword()) ||
                StringUtils.isEmpty(merchant.getSignature()) || StringUtils.isEmpty(merchant.getEnvironment())) {
            modelMap.put(successProperty, false);
            modelMap.put(msgProperty, "PayPal merchant account error. Please contact administrator.");
            logger.warn("PayPal merchant account error. Please contact administrator.");
            return modelMap;
        }

        String contextPath = ServletUtil.getContextPath(httpServletRequest);

        Map<String, String> paymentProviderAttribute =
                createPaymentProviderAttribute(merchant.getAccount(), merchant.getPassword(), merchant.getSignature(),
                        merchant.getEnvironment(), contextPath);

        if (paymentProviderAttribute == null) {
            modelMap.put(successProperty, false);
            modelMap.put(msgProperty, "Location PayPal payment provider error. Please contact administrator.");
            logger.warn("Location PayPal payment provider error. Please contact administrator.");
            return modelMap;
        }

        Long invoiceID = new Long(System.currentTimeMillis());

        Map<String, Object> resultHash =
                setExpressChechout(paymentProviderAttribute, total, payPalLocationSettings.getCurrency(), invoiceID);

        if (resultHash.get(error).equals(trueString)) {
            modelMap.put(successProperty, false);
            modelMap.put(msgProperty, resultHash.get(response));

        } else {
            modelMap.put(successProperty, true);
            modelMap.put(msgProperty, resultHash.get(response));
            logger.info("Saved email address during the public purchase " + email);
            String ids = pictureService.getPurchasePicturesSelectionModel(currentUser);
            payPalService.registerPayment(currentUser, new Date(), payPalLocationSettings.getCurrency(), total, invoiceID,
                    statusRegistered, (String) resultHash.get(response), "Success", ids, email);
        }

        return modelMap;
    }


    @RequestMapping(value = "/main/purchasepayed")
    public String getPurchasePaymentPage(ModelMap model, String token, String PayerID, HttpServletRequest httpServletRequest) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        int requestId = Integer.valueOf(
                httpServletRequest.getParameter("requestId") != null ? httpServletRequest.getParameter("requestId") : "-1");
        String fileName = pictureService.getStringFileName(currentUser);
        int access = pictureService.getUserAccess(currentUser);
        SystemUser systemUser = userService.getUserByLogin(currentUser);
        logger.info("User [ " + currentUser + " ] opened a PAYED purchase page");
        Location location1 = userService.getUserLocation(currentUser);
        boolean tagged = (location1 != null && location1.getTagged() != null && location1.getTagged() == true) ? true : false;
        model.put("tagApp", tagged);

        if (access != AccessID.PUBLICEMAILUSER.getValue()) {
            if (access == AccessID.PUBLICUSER.getValue() || access == AccessID.PUBLICTAGUSER.getValue()) {
                Date userDate = systemUser.getCreationDate();
                model.put("startDate", DateConvertationUtils.getStartDate(0, userDate).getTime());
                model.put("endDate", DateConvertationUtils.getEndDate(0, userDate).getTime());

                model.put("startDay", DateConvertationUtils.getDateString(DateConvertationUtils.getStartDate(0, userDate)));
                model.put("endDay", DateConvertationUtils.getDateString(DateConvertationUtils.getEndDate(0, userDate)));
                model.put("startTime", DateConvertationUtils.getTimeString(DateConvertationUtils.getStartDate(0, userDate)));
                model.put("endTime", DateConvertationUtils.getTimeString(DateConvertationUtils.getEndDate(0, userDate)));

                model.put("emails", "");
                model.put("emailCount", -1);
            } else {
                model.put("startDate", DefaultValues.getStartDate());
                model.put("endDate", DefaultValues.getEndDate());
                model.put("startDay", "");
                model.put("endDay", "");
                model.put("startTime", "");
                model.put("endTime", "");

                List<String> emails = requestService.getEmailsFromRequest(requestId);
                int emailCount = emails.size();
                model.put("emails", requestService.getEmailsStrFromRequest(requestId));
                model.put("emailCount", emailCount);
            }
        } else {
            UserFilter userFilter = pictureService.getUserFilter(currentUser);
            model.put("startDate", userFilter.getStartDate().getTime());
            model.put("endDate", userFilter.getEndDate().getTime());

            model.put("startDay", DateConvertationUtils.getDateString(userFilter.getStartDate()));
            model.put("endDay", DateConvertationUtils.getDateString(userFilter.getEndDate()));
            model.put("startTime", DateConvertationUtils.getTimeString(userFilter.getStartDate()));
            model.put("endTime", DateConvertationUtils.getTimeString(userFilter.getEndDate()));

            model.put("emails", "");
            model.put("emailCount", -1);
            model.put("email", "");
        }
        model.put("pageSize", pictureService.getPageSize(currentUser));
        model.put("current", currentUser);
        model.put("userId", access);
        model.put("requestId", requestId);
        model.put("fileName", fileName);
        if (systemUser.getLocation().getPaypallocationSettings() != null &&
                systemUser.getLocation().getPaypallocationSettings().getPrice() != null &&
                !systemUser.getLocation().getPaypallocationSettings().getPrice().equals("")) {
            model.put("price", systemUser.getLocation().getPaypallocationSettings().getPrice());
        } else {
            model.put("price", 0);
        }

        /* Map<String, String> paymentProviderAttribute = createPaymentProviderAttribute("mytain_1328780854_biz_api1.yandex.ru",
       "1328780878","ArOcGAx8uUH0Jon43nj995.VeDCVAekzqr3OOnSYx326WGUe2lM9.prG", "sandbox");*/

        if (systemUser == null) {
            model.put(successProperty, false);
            model.put(msgProperty, "User is null. Please contact administrator.");
            logger.warn("User is null. Please contact administrator.");
            model.put("payed", false);

            return "purchasepage";
        }

        Location location = systemUser.getLocation();
        if (location == null) {
            model.put(successProperty, false);
            model.put(msgProperty, "Location is null. Please contact administrator.");
            logger.warn("Location is null. Please contact administrator.");
            model.put("payed", false);
            return "purchasepage";
        }
        PayPalLocationSettings payPalLocationSettings = location.getPaypallocationSettings();
        if (payPalLocationSettings == null) {
            model.put(successProperty, false);
            model.put(msgProperty, "Location PayPal settings are not set. Please contact administrator.");
            logger.warn("Location PayPal settings are not set. Please contact administrator.");
            model.put("payed", false);
            return "purchasepage";
        }
        Merchant merchant = null;
        try {
            merchant = payPalService.getMerchantAccount();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        if (merchant == null) {
            model.put(successProperty, false);
            model.put(msgProperty, "Location PayPal merchant settings are not set. Please contact administrator.");
            logger.warn("Location PayPal merchant settings are not set. Please contact administrator.");
            model.put("payed", false);
            return "purchasepage";
        }

        String contextPath = ServletUtil.getContextPath(httpServletRequest);

        Map<String, String> paymentProviderAttribute = createPaymentProviderAttribute(merchant.getAccount(), merchant.getPassword(), merchant.getSignature(),
                merchant.getEnvironment(), contextPath);

        if (paymentProviderAttribute == null) {
            model.put(successProperty, false);
            model.put(msgProperty, "Location PayPal settings are not set correctly. Please contact administrator.");
            logger.warn("Location PayPal settings are not set correctly. Please contact administrator.");
            model.put("payed", false);
            return "purchasepage";
        }

        Payment payment = payPalService.getRegisteredPayment(token);
        if (payment == null) {
            model.put(successProperty, false);
            model.put(msgProperty, "Payment was not registered. Please contact administrator.");
            logger.warn("Payment was not registered. Please contact administrator.");
            model.put("payed", false);
            return "purchasepage";
        }

        if (StringUtils.isEmpty(PayerID) || StringUtils.isEmpty(token)) {
            model.put(successProperty, false);
            model.put(msgProperty, "Payment was not registered. Please contact administrator.");
            logger.warn("Payment was not registered. Please contact administrator.");
            model.put("payed", false);
            return "purchasepage";
        }
        model.put("selectedids", payment.getPictureids());
        model.put("email", payment.getEmail());

        Map<String, Object> result =
                doExpressChechout(paymentProviderAttribute, token, PayerID, payment.getPrice(), payment.getCurrency());
        if (result.get(error).equals(trueString)) {
            model.put(successProperty, false);
            model.put(msgProperty, "Payment error. Please contact administrator.");
            logger.warn("Payment error. Please contact administrator.");
            model.put("payed", false);
            return "purchasepage";
        }
        logger.info("Payment RESULT " + result.get(response));

        payment.setStatus(statusComplete);
        payment.setResult((String) result.get(response));
        payment.setPayerID(PayerID);
        payPalService.savePayment(payment);

        model.put("payed", true);
        pictureService.createPurchasePicturesSelectionModel(currentUser, requestId);
        String redirectpaypal = "";
        redirectpaypal = getPayPalRedirectUrl();
        model.put("redirectpaypal", redirectpaypal);
        model.put("facebookPage", systemUser.getLocation().getLocationToFacebookEmail());
        model.put("facebookAppId", facebookAppId);
        return "purchasepage";
    }


    public Map<String, String> createPaymentProviderAttribute(String login, String password, String signature, String environment, String contextPath) {
        if (StringUtils.isEmpty(contextPath)) {
            logger.error("Application context path is empty");
            return null;
        }
        if (StringUtils.isEmpty(login) || StringUtils.isEmpty(password) || StringUtils.isEmpty(signature) ||
                StringUtils.isEmpty(environment)) {
            return null;
        }

        logger.info("Created payment provider attribute");
        Map<String, String> paymentProviderAttribute = new HashMap<String, String>();
        paymentProviderAttribute.put("LOGIN", login);
        paymentProviderAttribute.put("PASSWORD", password);
        paymentProviderAttribute.put("SIGNATURE", signature);
        paymentProviderAttribute.put("ENVIRONMENT", environment);
        paymentProviderAttribute.put("CONFIRMREDIRECTURL", contextPath + CONFIRM_REDIRECT_URL);
        paymentProviderAttribute.put("CANCELREDIRECTURL", contextPath + CANCEL_REDIRECT_URL);

        return paymentProviderAttribute;
    }


    public Map<String, Object> setExpressChechout(Map<String, String> paymentProviderAttribute, Double amount, String currency,
                                                  Long invoiceID) {
        String responseValue = null;
        Map<String, Object> resultHash = new HashMap<String, Object>();
        try {

            SetExpressCheckoutRequestType pprequest = new SetExpressCheckoutRequestType();
            pprequest.setVersion("51.0");

            // Add request-specific fields to the request.
            SetExpressCheckoutRequestDetailsType details = new SetExpressCheckoutRequestDetailsType();
            details.setReturnURL(paymentProviderAttribute.get("CONFIRMREDIRECTURL"));
            details.setCancelURL(paymentProviderAttribute.get("CANCELREDIRECTURL"));
            BasicAmountType orderTotal = new BasicAmountType();
            orderTotal.set_value("" + amount);
            orderTotal.setCurrencyID(CurrencyCodeType.fromString(currency));
            details.setOrderTotal(orderTotal);
            details.setInvoiceID("" + invoiceID);
            details.setPaymentAction(PaymentActionCodeType.Sale);
            pprequest.setSetExpressCheckoutRequestDetails(details);

            // Execute the API operation and obtain the response.
            SetExpressCheckoutResponseType ppresponse =
                    (SetExpressCheckoutResponseType) getCaller(paymentProviderAttribute).call("SetExpressCheckout", pprequest);
            responseValue = ppresponse.getToken();

        } catch (Exception ex) {
            logger.error("Error in setExpressChechout method");
            resultHash.put(error, trueString);
            resultHash.put(response, responseValue);
        }

        logger.info("setExpressChechout method was successful");
        resultHash.put(error, falseString);
        resultHash.put(response, responseValue);
        return resultHash;
    }


    public CallerServices getCaller(Map<String, String> paymentProviderAttribute)
            throws PayPalException {
        //if (caller==null){
        CallerServices caller = new CallerServices();
        APIProfile profile;
        profile = ProfileFactory.createSignatureAPIProfile();
        profile.setAPIUsername(paymentProviderAttribute.get("LOGIN"));
        profile.setAPIPassword(paymentProviderAttribute.get("PASSWORD"));
        profile.setSignature(paymentProviderAttribute.get("SIGNATURE"));
        profile.setEnvironment(paymentProviderAttribute.get("ENVIRONMENT"));
        caller.setAPIProfile(profile);
        logger.info("getCaller  successfully");
        //}
        return caller;
    }

    public Map<String, Object> doExpressChechout(Map<String, String> paymentProviderAttribute, String extTrasnactionInfo, String payerId,
                                                 Double amount, String currency) {
        Map<String, Object> resultHash = new HashMap<String, Object>();
        DoExpressCheckoutPaymentResponseType ppresponse = new DoExpressCheckoutPaymentResponseType();
        try {
            // Create the request object.
            DoExpressCheckoutPaymentRequestType pprequest = new DoExpressCheckoutPaymentRequestType();
            pprequest.setVersion("51.0");

            // Add request-specific fields to the request.

            // Create the request details object.
            DoExpressCheckoutPaymentRequestDetailsType paymentDetailsRequestType = new DoExpressCheckoutPaymentRequestDetailsType();
            //pass the token value by actual value returned in the SetExpressCheckout
            paymentDetailsRequestType.setToken(extTrasnactionInfo);
            paymentDetailsRequestType.setPayerID(payerId);
            paymentDetailsRequestType.setPaymentAction(PaymentActionCodeType.Sale);

            PaymentDetailsType paymentDetails = new PaymentDetailsType();
            BasicAmountType orderTotal = new BasicAmountType();
            orderTotal.set_value("" + amount);
            orderTotal.setCurrencyID(CurrencyCodeType.fromString(currency));
            paymentDetails.setOrderTotal(orderTotal);
            PaymentDetailsType[] paymentDetailsMany = new PaymentDetailsType[1];
            paymentDetailsMany[0] = paymentDetails;
            paymentDetailsRequestType.setPaymentDetails(paymentDetailsMany);
            pprequest.setDoExpressCheckoutPaymentRequestDetails(paymentDetailsRequestType);

            // Execute the API operation and obtain the response.
            ppresponse =
                    (DoExpressCheckoutPaymentResponseType) getCaller(paymentProviderAttribute).call("DoExpressCheckoutPayment", pprequest);

        } catch (Exception ex) {
            resultHash.put(error, trueString);
            resultHash.put(response, ppresponse.getAck().toString());
            return resultHash;
        }
        if (ppresponse.getAck().toString().equalsIgnoreCase("success")) {
            logger.info("doExpressChechout  successfully");
            resultHash.put(error, falseString);
            resultHash.put(response, ppresponse.getAck().toString());
            resultHash.put("transaction", ppresponse.getDoExpressCheckoutPaymentResponseDetails().getPaymentInfo(0).getTransactionID());

            return resultHash;
        } else {
            logger.error("Error in doExpressChechout  method");
            resultHash.put(error, trueString);
            resultHash.put(response, ppresponse.getAck().toString());
            return resultHash;
        }

    }
}
