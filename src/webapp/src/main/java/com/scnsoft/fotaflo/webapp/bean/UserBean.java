package com.scnsoft.fotaflo.webapp.bean;

/**
 * Created Paradinets Tatsiana.
 * Date: 13.05.2011
 * Time: 13:05:00
 */

public class UserBean {

    protected int id;
    String loginName;
    String password;
    String firstName;
    String lastName;
    String locationId;
    String location;
    String access;

    public UserBean() {
    }

    public UserBean(int id, String loginName, String password, String firstName, String lastName, String locationId, String location, String access) {
        this.id = id;
        this.loginName = loginName;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.locationId = locationId;
        this.location = location;
        this.access = access;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    @Override
    public String toString() {
        return "UserBean{" + id + ", " +
                "loginName='" + loginName + '\'' +
                '}';
    }
}
