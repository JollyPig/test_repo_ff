package com.scnsoft.fotaflo.webapp.controller;

import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 19.05.14
 * Time: 14:33
 * To change this template use File | Settings | File Templates.
 */
public class FotafloAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    protected static Logger logger = Logger.getLogger(FotafloAuthenticationSuccessHandler.class);

    @Autowired
    private UserService userService;

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response, Authentication authentication) throws IOException {

        HttpSession session = request.getSession();
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("Authenificated by " + currentUser);
        Location location = userService.getUserLocation(currentUser);
        if (location != null && location.getTagged() != null && location.getTagged() == true) {
            session.setAttribute("tagEnabled", true);
        } else {
            session.setAttribute("tagEnabled", false);
        }
        logger.info("tag enabled: " + session.getAttribute("tagEnabled"));

        redirectStrategy.sendRedirect(request, response, "/pictures/main/common");
    }

}
