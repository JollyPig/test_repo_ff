package com.scnsoft.fotaflo.webapp.model;

import com.scnsoft.fotaflo.common.model.AbstractEntity;
import com.scnsoft.fotaflo.webapp.bean.PictureBean;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */

@Entity
@Table(name = "picture")
public class Picture extends AbstractEntity implements Comparable<Picture> {

    String name;
    String url;
    Date creationDate;
    private Camera camera;
    private Boolean rotated;
    private String pictureSize;
    private Set<Tag> tags = new HashSet<Tag>(0);
    private Set<SystemUser> systemUsers = new HashSet<SystemUser>(0);
    private Set<PurchaseSelected> selectedUsers = new HashSet<PurchaseSelected>(0);
    private Set<HelpYourselfSelected> helpYourselfSelected = new HashSet<HelpYourselfSelected>(0);
    private Set<RequestsSelected> requestSelected = new HashSet<RequestsSelected>(0);
    private Location location;

    private List<UserFilter> userFilterList;

    @Column(name = "picture_name", nullable = false, length = 255)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "picture_url", nullable = false, length = 255)
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl(boolean reduced) {
        if (reduced) {
            return url.replace(PictureBean.IMG_DIR, PictureBean.IMG_SMALL_DIR);
        } else {
            return url;
        }
    }

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, optional = true)
    @JoinColumn(name = "camera_id", nullable = true, updatable = true, insertable = true)
    public Camera getCamera() {
        return camera;
    }

    public void setCamera(Camera camera) {
        this.camera = camera;
    }

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinTable(name = "picture_tags", joinColumns = {@JoinColumn(name = "picture_id")}, inverseJoinColumns = {@JoinColumn(name = "tag_id")})
    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }


    @Column(name = "creation_date")
    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }


    @Column(name = "rotated")
    public Boolean getRotated() {
        return rotated;
    }

    public void setRotated(Boolean rotated) {
        this.rotated = rotated;
    }

    @Column(name = "pictureSize")
    public String getPictureSize() {
        return pictureSize;
    }

    public void setPictureSize(String pictureSize) {
        this.pictureSize = pictureSize;
    }

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinTable(name = "selected_pictures", joinColumns = {@JoinColumn(name = "picture_id")}, inverseJoinColumns = {@JoinColumn(name = "system_user_id")})
    public Set<SystemUser> getSystemUsers() {
        return systemUsers;
    }

    public void setSystemUsers(Set<SystemUser> systemUsers) {
        this.systemUsers = systemUsers;
    }

/*    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, mappedBy="picture")*/

    @OneToMany(cascade = {CascadeType.ALL}, mappedBy = "picture")
    public Set<PurchaseSelected> getSelectedUsers() {
        return selectedUsers;
    }

    public void setSelectedUsers(Set<PurchaseSelected> selectedUsers) {
        this.selectedUsers = selectedUsers;
    }

    /*@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, mappedBy="picture")*/

    @OneToMany(cascade = {CascadeType.ALL}, mappedBy = "picture")
    public Set<RequestsSelected> getRequestSelected() {
        return requestSelected;
    }

    public void setRequestSelected(Set<RequestsSelected> requestSelected) {
        this.requestSelected = requestSelected;
    }


    /*@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, mappedBy="picture")*/

    @OneToMany(cascade = {CascadeType.ALL}, mappedBy = "picture")
    public Set<HelpYourselfSelected> getHelpYourselfSelected() {
        return helpYourselfSelected;
    }

    public void setHelpYourselfSelected(Set<HelpYourselfSelected> helpYourselfSelected) {
        this.helpYourselfSelected = helpYourselfSelected;
    }

    public int compareTo(Picture o) {
        return o.creationDate.compareTo(this.creationDate);
    }

    @ManyToMany(targetEntity = UserFilter.class, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinTable(name = "UserFilter_Picture",
            joinColumns = @JoinColumn(name = "pictures_id"),
            inverseJoinColumns = @JoinColumn(name = "userfilter_id")
    )
    public List<UserFilter> getUserFilterList() {
        return userFilterList;
    }

    public void setUserFilterList(List<UserFilter> userFilterList) {
        this.userFilterList = userFilterList;
    }

    @ManyToOne(/*cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH},*/ optional = true)
    @JoinTable(name = "location_pictures", joinColumns = {@JoinColumn(name = "picture_id")}, inverseJoinColumns = {@JoinColumn(name = "location_id")})
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
