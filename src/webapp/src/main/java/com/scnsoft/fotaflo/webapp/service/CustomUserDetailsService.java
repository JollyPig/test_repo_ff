package com.scnsoft.fotaflo.webapp.service;

import com.scnsoft.fotaflo.webapp.model.SystemUser;
import com.scnsoft.fotaflo.webapp.util.AccessID;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */

/**
 * A custom service for retrieving users from a custom datasource, such as a database.
 * <p/>
 * This custom service must implement Spring's {@link UserDetailsService}
 */
@Transactional(readOnly = false)
public class CustomUserDetailsService implements UserDetailsService {
    protected static Logger logger = Logger.getLogger("service");

    @Autowired
    private UserService userService;

    @Autowired
    private PictureService pictureService;

    /**
     * Retrieves a user record containing the user's credentials and access.
     */
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException, DataAccessException {

        // Declare a null Spring UserSystem
        UserDetails user = null;

        try {
            // Search database for a user that matches the specified username
            // You can provide a custom DAO to access your persistence layer
            // Or use JDBC to access your database
            // DbUser is our custom domain user. This is not the same as Spring's UserSystem
            if (username.contains(";") ||/* username.contains(":") ||*/ username.contains("--") || username.contains("/") || username.contains("*") || username.contains("xp_")) {
                logger.error("Possible SQL injection!!!!!!! login " + username);
                return user;
            }

            SystemUser dbUser = userService.getUserByLogin(username);

            user = new User(
                    dbUser.getLoginName().toLowerCase(),
                    dbUser.getPassword().toLowerCase(),
                    true,
                    true,
                    true,
                    true,
                    getAuthorities(new Integer(dbUser.getAccess())));

            logger.info("User [ " + username + " ]" + " is logged in");
            if (dbUser.getAccess().equals(AccessID.USER.getStringValue()) || dbUser.getAccess().equals(AccessID.ADMIN.getStringValue())) {
                boolean reset = pictureService.resetUserFilter(username);
                if (reset) {
                    logger.info("User [ " + username + " ]" + " cleared filter by logging in");
                } else {
                    logger.error("User [ " + username + " ]" + " was not able to clear it's filter filter by logging in");
                }
            }
        } catch (Exception e) {
            logger.error("Error in retrieving user " + username);
            throw new UsernameNotFoundException("Error in retrieving user" + e);
        }

        // Return user to Spring for processing.
        // Take note we're not the one evaluating whether this user is authenticated or valid
        // We just merely retrieve a user that matches the specified username
        return user;
    }

    /**
     * Retrieves the correct ROLE type depending on the access level, where access level is an Integer.
     * Basically, this interprets the access value whether it's for a regular user or admin.
     *
     * @param access an integer value representing the access of the user
     * @return collection of granted authorities
     */
    public Collection<GrantedAuthority> getAuthorities(Integer access) {
        // Create a list of grants for this user
        List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>(2);

        // All users are granted with ROLE_USER access
        // Therefore this user gets a ROLE_USER by default

        // UserSystem has admin access
        logger.debug("Grant ROLE_PUBLIC to this user");
        authList.add(new GrantedAuthorityImpl("ROLE_PUBLIC"));

        if (access.compareTo(2) <= 0) {
            // UserSystem has admin access
            logger.debug("Grant ROLE_USER to this user");
            authList.add(new GrantedAuthorityImpl("ROLE_USER"));
        }

        // Check if this user has admin access
        // We interpret Integer(1) as an admin user
        if (access.compareTo(1) == 0) {
            // UserSystem has admin access
            logger.debug("Grant ROLE_ADMIN to this user");
            authList.add(new GrantedAuthorityImpl("ROLE_ADMIN"));
        }

        // Return list of granted authorities
        return authList;
    }

}
