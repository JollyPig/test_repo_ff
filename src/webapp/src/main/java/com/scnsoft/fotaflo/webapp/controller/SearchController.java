package com.scnsoft.fotaflo.webapp.controller;

import com.scnsoft.fotaflo.webapp.bean.CameraBean;
import com.scnsoft.fotaflo.webapp.bean.LocationBean;
import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.service.CameraService;
import com.scnsoft.fotaflo.webapp.service.LocationService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */
@Controller
public class SearchController {

    protected static Logger logger = Logger.getLogger(SearchController.class);
    @Autowired
    private LocationService locationService;
    @Autowired
    private CameraService cameraService;

    @RequestMapping(value = "getLocations.json")
    public
    @ResponseBody
    Map<String, ? extends Object> loadLocations() {

        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        HashMap<String, List<LocationBean>> modelMap = new HashMap<String, List<LocationBean>>();
        modelMap.put("locations", locationService.getUserLocations(currentUser));
        return modelMap;
    }

    @RequestMapping(value = "getLocationsClean.json")
    public
    @ResponseBody
    Map<String, ? extends Object> getLocationsClean() {

        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        HashMap modelMap = new HashMap();
        List<Location> locations = locationService.getUserLocationsClean(currentUser);
        modelMap.put("locations", locations);
        return modelMap;
    }

    @RequestMapping(value = "getCameras.json")
    public
    @ResponseBody
    Map<String, ? extends Object> loadCameras(HttpServletRequest httpServletRequest) {

        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        HashMap<String, List<CameraBean>> modelMap = new HashMap<String, List<CameraBean>>();
        int locationId = Integer.valueOf(httpServletRequest.getParameter("locationid") != null ? httpServletRequest.getParameter("locationid") : "0");

        modelMap.put("cameras", cameraService.getCameras(locationId, currentUser));
        return modelMap;
    }


    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }

    public void setCameraService(CameraService cameraService) {
        this.cameraService = cameraService;
    }
}


