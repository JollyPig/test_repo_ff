package com.scnsoft.fotaflo.webapp.model;

import com.scnsoft.fotaflo.common.model.AbstractEntity;

import javax.persistence.*;

/**
 * Created by
 * User: Paradinets
 * Date: 23.12.2011
 * Time: 18:15:39
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "contactInfo")
public class ContactInfo extends AbstractEntity {

    String firstName;
    String lastName;
    String email;
    String question;
    String phone;
    Integer isSuccessfullySent;
    SystemUser systemUser;

    @Column(name = "firstname", length = 100)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "lastname", length = 100)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Column(name = "email", length = 100)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "phone", length = 100)
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Lob
    @Column(name = "question")
    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    @Column(name = "isSuccessfullySent")
    public Integer getSuccessfullySent() {
        return isSuccessfullySent;
    }

    public void setSuccessfullySent(Integer successfullySent) {
        isSuccessfullySent = successfullySent;
    }

    @ManyToOne
    @JoinColumn(name = "system_user_id")
    public SystemUser getSystemUser() {
        return systemUser;
    }

    public void setSystemUser(SystemUser systemUser) {
        this.systemUser = systemUser;
    }
}