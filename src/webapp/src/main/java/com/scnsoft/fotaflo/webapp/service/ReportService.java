package com.scnsoft.fotaflo.webapp.service;

import com.scnsoft.fotaflo.common.util.DateUtil;
import com.scnsoft.fotaflo.common.util.StringUtils;
import com.scnsoft.fotaflo.webapp.bean.StatisticBean;
import com.scnsoft.fotaflo.webapp.dao.IDao;
import com.scnsoft.fotaflo.webapp.dao.IReportDao;
import com.scnsoft.fotaflo.webapp.job.WriteExcel;
import com.scnsoft.fotaflo.webapp.model.*;
import com.scnsoft.fotaflo.webapp.util.DateConvertationUtils;
import com.scnsoft.fotaflo.webapp.util.ExcelBuilder;
import com.scnsoft.fotaflo.webapp.util.MailSenderUtil;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */

@Service
@Transactional(propagation = Propagation.REQUIRED)

public class ReportService {
    protected static Logger logger = Logger.getLogger(ReportService.class);

    @Autowired
    private IReportDao reportDao;

    @Autowired
    private IDao baseDao;

    @Autowired
    private LocationService locationService;

    @Autowired
    private TagPurchaseService tagPurchaseService;

    @Autowired
    private PictureService pictureService;

    public enum ReportNumber {
        FIRST, SECOND, LAST
    }

    public Map<Integer, SortedMap<String, List<StatisticBean>>> getMonthlyStatistic(Date fireTime) {
        SortedMap<String, List<StatisticBean>> map = new TreeMap<String, List<StatisticBean>>();
        HashMap<Integer, SortedMap<String, List<StatisticBean>>> statisticByLocation = new HashMap<Integer, SortedMap<String, List<StatisticBean>>>();
        List locations = reportDao.getLocations();
        Iterator itLocations = locations.iterator();
        Map<Integer, List<Date>> weekDates = DateConvertationUtils.getMonthStartDateMap(fireTime);
        while (itLocations.hasNext()) {
            map = new TreeMap();
            Location location = (Location) itLocations.next();
            for (int i = weekDates.size(); i > 0; i--) {
                List<Date> weekList = weekDates.get(i);
                if (weekList != null && !weekList.isEmpty()) {
                    Date start = weekList.get(0);
                    Date end = weekList.get(weekList.size() - 1);
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(end);
                    cal.set(Calendar.HOUR, 23);
                    cal.set(Calendar.MINUTE, 59);
                    cal.set(Calendar.SECOND, 59);
                    cal.set(Calendar.MILLISECOND, 0);
                    end = cal.getTime();
                    List<StatisticBean> packages = reportDao.findExtendedPackages(start, end, location);

                    Collections.sort(packages, new Comparator<StatisticBean>() {

                        public int compare(StatisticBean o1, StatisticBean o2) {
                            if (o1.getPackages() != null && o2.getPackages() != null && o1.getPackages().getAccess() != null
                                    && o2.getPackages().getAccess() != null) {
                                return o1.getPackages().getAccess().compareTo(o2.getPackages().getAccess());
                            } else {
                                return 0;
                            }
                        }
                    });
                    map.put("Week" + i + " (" + DateConvertationUtils.shortStringDate1(start) + " - " + DateConvertationUtils.shortStringDate1(end) + " )", packages);
                } else {
                    logger.error("The system can not define days of the week " + i);
                }
            }
            statisticByLocation.put(location.getId(), map);
        }

        return statisticByLocation;
    }

    public Map<Integer, List<StatisticBean>> getGeneralMonthlyStatistic(Date fireTime) {
        HashMap<Integer, List<StatisticBean>> statisticByLocationGeneral = new HashMap<Integer, List<StatisticBean>>();
        Map<Integer, List<Date>> weekDates = DateConvertationUtils.getMonthStartDateMap(fireTime);
        List<Date> weekList = weekDates.get(1);
        if (weekList != null && !weekList.isEmpty()) {
            Date start = weekList.get(0);

            List<Date> endList = weekDates.get(weekDates.size());
            if (endList != null && !endList.isEmpty()) {
                Date end = endList.get(endList.size() - 1);

                Calendar cal = Calendar.getInstance();
                cal.setTime(end);
                cal.set(Calendar.HOUR, 23);
                cal.set(Calendar.MINUTE, 59);
                cal.set(Calendar.SECOND, 59);
                cal.set(Calendar.MILLISECOND, 0);
                end = cal.getTime();

                List locations = reportDao.getLocations();
                Iterator itLocations = locations.iterator();
                while (itLocations.hasNext()) {
                    Location location = (Location) itLocations.next();
                    List<StatisticBean> packagesGeneral = reportDao.findExtendedPackages(start, end, location);
                    Collections.sort(packagesGeneral, new Comparator<StatisticBean>() {

                        public int compare(StatisticBean o1, StatisticBean o2) {
                            if (o1.getPackages() != null && o2.getPackages() != null && o1.getPackages().getAccess() != null
                                    && o2.getPackages().getAccess() != null) {
                                return o1.getPackages().getAccess().compareTo(o2.getPackages().getAccess());
                            } else {
                                return 0;
                            }
                        }
                    });

                    statisticByLocationGeneral.put(location.getId(), packagesGeneral);

                }
            } else {
                logger.error("The system can not define the last day of month");
            }
        } else {
            logger.error("The system can not define the first day of month");
        }
        return statisticByLocationGeneral;
    }

    public HashMap getStatistic(Date fireTime) {
        SortedMap map = new TreeMap();
        HashMap<Integer, SortedMap> statisticByLocation = new HashMap<Integer, SortedMap>();
        List locations = reportDao.getLocations();
        Iterator itLocations = locations.iterator();
        while (itLocations.hasNext()) {
            map = new TreeMap();
            Location location = (Location) itLocations.next();
            for (int i = 6; i >= 0; i--) {
                Date start = DateConvertationUtils.getStartDate(i, fireTime);
                Date end = DateConvertationUtils.getEndDate(i, fireTime);

                List packages = reportDao.findExtendedPackages(start, end, location);
                Collections.sort(packages, new Comparator<StatisticBean>() {

                    public int compare(StatisticBean o1, StatisticBean o2) {
                        if (o1.getPackages() != null && o2.getPackages() != null && o1.getPackages().getAccess() != null
                                && o2.getPackages().getAccess() != null) {
                            return o1.getPackages().getAccess().compareTo(o2.getPackages().getAccess());
                        } else {
                            return 0;
                        }
                    }
                });

                map.put(DateConvertationUtils.shortStringDate1(start), packages);
            }

            statisticByLocation.put(location.getId(), map);
        }

        return statisticByLocation;
    }

    public HashMap getGeneralStatistic(Date fireTime) {
        HashMap<Integer, List> statisticByLocationGeneral = new HashMap<Integer, List>();
        //  SortedMap map = new TreeMap();
        Date start = DateConvertationUtils.getStartDate(6, fireTime);
        Date end = DateConvertationUtils.getEndDate(0, fireTime);
        List locations = reportDao.getLocations();
        Iterator itLocations = locations.iterator();
        while (itLocations.hasNext()) {
            Location location = (Location) itLocations.next();
            List packagesGeneral = reportDao.findExtendedPackages(start, end, location);
            Collections.sort(packagesGeneral, new Comparator<StatisticBean>() {
                public int compare(StatisticBean o1, StatisticBean o2) {
                    if (o1.getPackages() != null && o2.getPackages() != null && o1.getPackages().getAccess() != null
                            && o2.getPackages().getAccess() != null) {
                        return o1.getPackages().getAccess().compareTo(o2.getPackages().getAccess());
                    } else {
                        return 0;
                    }
                }
            });

            statisticByLocationGeneral.put(location.getId(), packagesGeneral);
        }

        return statisticByLocationGeneral;
    }

    public HashMap<Integer, List<Statistic>> getNotificationDailyStatistic(Date fireTime) {
        HashMap<Integer, List<Statistic>> statisticByLocation = new HashMap<Integer, List<Statistic>>();
        List locations = reportDao.getLocations();
        Iterator itLocations = locations.iterator();
        while (itLocations.hasNext()) {
            Location location = (Location) itLocations.next();
            Date start = DateConvertationUtils.getStartDate(0, fireTime);
            Date end = DateConvertationUtils.getEndDate(0, fireTime);
            List<Statistic> statistics = reportDao.getStatisticByLocationForThePeriod(location, start, end);

            List<Statistic> statisticApproval = tagPurchaseService.getTagPurchasesForStatistic(location, start, end);

            if (statisticApproval != null && !statisticApproval.isEmpty()) {
                statistics.addAll(statisticApproval);
            }
            statisticByLocation.put(location.getId(), statistics);
        }

        return statisticByLocation;
    }

    public boolean generateAndSendMonthlyStatisticV2(Date fireTime, ReportNumber reportNumber) {
        try {
            logger.info("Start generation monthly report number " + reportNumber);
            Map<Integer, List<StatisticBean>> mapGeneral = getGeneralMonthlyStatistic(fireTime);
            Map<Integer, SortedMap<String, List<StatisticBean>>> map = getMonthlyStatistic(fireTime);
            Iterator itLocations = map.keySet().iterator();
            int sheetNumber = 0;

            logger.info("Start generation monthly report for all the locations ");
            while (itLocations.hasNext()) {
                Integer key = (Integer) itLocations.next();
                logger.info("location:  " + key);
                if (locationService.getLocation(key) == null) {
                    logger.info("No location were found for " + key);
                    continue;
                }

                String keyString = locationService.getLocation(key).getLocationName().trim();
                logger.info("location keyString " + keyString);
                SortedMap reportHash = map.get(key);
                logger.info("location reportHash " + reportHash.size());
                List reportGeneralList = mapGeneral.get(key);
                logger.info("location reportGeneralList " + reportGeneralList.size());

                String reportType = "monthly";
                String reportName = getReportName(reportType, keyString, fireTime);
                String path = pictureService.getPath(File.separator + "reports" + File.separator + "monthly" + File.separator + reportName);

                String reportPath = getReportPath();
                if (new File(reportPath).isDirectory()) {
                    String pathDir = reportPath + (reportPath.endsWith(File.separator) ? "" : File.separator) + "monthly" + File.separator + keyString + File.separator;
                    logger.info("location pathDir " + pathDir);
                    if (!new File(pathDir).isDirectory()) {
                        new File(pathDir).mkdirs();
                    }
                    if (new File(pathDir).isDirectory()) {
                        path = pathDir + reportName;
                        logger.info("location path created " + path);
                    }
                }

                FileOutputStream out = new FileOutputStream(path);

                // create report section
                ExcelBuilder report = new ExcelBuilder(out);
                logger.info("Report is beeing generated for " + mapGeneral.size() + " weeks");

                report.setReportGeneralList(reportGeneralList);
                report.setReportHash(reportHash);

                report.writeMonthlyReport(sheetNumber, keyString, reportHash.size());
                report.closeWorkbook();

                // send mail section
                List<String> to = getLocationEmails(key);
                String msgText1 = "";
                if (reportNumber == ReportNumber.FIRST) {
                    msgText1 = getLocationStrings(key).get(0);
                }
                if (reportNumber == ReportNumber.SECOND) {
                    msgText1 = getLocationStrings(key).get(1);
                }
                if (reportNumber == ReportNumber.LAST) {
                    msgText1 = getLocationStrings(key).get(2);
                }

                sendReport(key, keyString, to, reportName, path, null, msgText1);
            }
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return false;

        } catch (WriteException e) {
            logger.error(e.getMessage(), e);
            return false;
        }
        return true;
    }

    public boolean generateAndSendWeeklyStatisticV2(Date fireTime) {
        Date reportTime = DateConvertationUtils.getStartDate(2, fireTime);
        try {
            HashMap<Integer, List> mapGeneral = getGeneralStatistic(reportTime);
            HashMap<Integer, SortedMap> map = getStatistic(reportTime);
            Iterator itLocations = map.keySet().iterator();
            int sheetNumber = 0;

            while (itLocations.hasNext()) {
                Integer key = (Integer) itLocations.next();

                String keyString = locationService.getLocation(key).getLocationName().trim();
                SortedMap reportHash = map.get(key);
                List reportGeneralList = mapGeneral.get(key);

                String reportType = "weekly";
                String reportName = getReportName(reportType, keyString, fireTime);

                String path = pictureService.getPath(File.separator + "reports" + File.separator + "weekly" + File.separator + reportName);
                String reportPath = getReportPath();
                if (new File(reportPath).isDirectory()) {
                    String pathDir = reportPath + (reportPath.endsWith(File.separator) ? "" : File.separator) + "weekly" + File.separator + keyString + File.separator;
                    if (!new File(pathDir).isDirectory()) {
                        new File(pathDir).mkdirs();
                    }
                    if (new File(pathDir).isDirectory()) {
                        path = pathDir + reportName;
                    }
                }

                FileOutputStream out = new FileOutputStream(path);

                // create report section
                ExcelBuilder report = new ExcelBuilder(out);
                logger.info("Report is beeing generated for " + reportHash.size() + " days");

                report.setReportGeneralList(reportGeneralList);
                report.setReportHash(reportHash);

                report.writeWeeklyReport(sheetNumber, keyString, 7);
                report.closeWorkbook();

                // send mail section
                List<String> to = getLocationDailyEmails(key);
                String msgText1 = "Weekly Statistic report for location " + keyString;
                String fileName = reportName;
                String subject = "Weekly Report";

                sendReport(key, keyString, to, fileName, path, subject, msgText1);
            }
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return false;
        } catch (WriteException e) {
            logger.error(e.getMessage(), e);
            return false;
        }

        return true;
    }

    public boolean generateAndSendDailyStatisticV2(Date fireTime1) {
        Date reportDate = DateConvertationUtils.getStartDate(1, fireTime1);
        try {
            HashMap<Integer, List<Statistic>> map = getNotificationDailyStatistic(reportDate);
            Iterator itLocations = map.keySet().iterator();
            int sheetNumber = 0;

            while (itLocations.hasNext()) {
                Integer key = (Integer) itLocations.next();
                Location location = locationService.getLocation(key);
                String keyString = location.getLocationName().trim();
                logger.info("Start sending Daily email ");
                String reportType = "daily";
                String reportName = getReportName(reportType, keyString, reportDate);

                String path = pictureService.getPath(File.separator + "reports" + File.separator + "daily" + File.separator + reportName);
                String reportPath = getReportPath();
                if (new File(reportPath).isDirectory()) {
                    String pathDir = reportPath + (reportPath.endsWith(File.separator) ? "" : File.separator) + "daily" + File.separator + keyString + File.separator;
                    if (!new File(pathDir).isDirectory()) {
                        new File(pathDir).mkdirs();
                    }
                    if (new File(pathDir).isDirectory()) {
                        path = pathDir + reportName;
                    }
                }

                FileOutputStream out = new FileOutputStream(path);

                // create report section
                ExcelBuilder report = new ExcelBuilder(out);

                List<Statistic> statistics = map.get(key);
                report.setStatisticList(statistics);

                report.writeDailyReport(sheetNumber, keyString, reportDao.getStaffByLocation(location), reportDate);
                report.closeWorkbook();

                // send mail section
                List<String> to = getLocationDailyEmails(key);
                String msgText1 = "Daily Statistic report for location " + keyString;
                String fileName = reportName;
                String subject = " Daily Report";

                sendReport(key, keyString, to, fileName, path, subject, msgText1);
            }
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return false;
        } catch (WriteException e) {
            logger.error(e.getMessage(), e);
            return false;
        }

        return true;
    }

    public String generateStatisticReportName(Date date, Integer locationId, String type){
        if(date != null && locationId != null){
            ReportType rt = ReportType.getType(type);
            Location location = locationService.getLocation(locationId);

            if(rt != null && location != null){
                return getReportName(rt.name().toLowerCase(), location.getLocationName(), date);
            }
        }

        return null;
    }

    public void generateStatisticReport(Date date, Integer locationId, OutputStream out, String type){
        if(out != null){
            ReportType rt = ReportType.getType(type);

            if(rt != null){
                if(date == null){
                    date = new Date();
                }
                switch (rt){
                    case DAILY: generateDailyStatistic(date, locationId, out); break;
                    case WEEKLY: generateWeeklyStatistic(date, locationId, out); break;
                    case MONTHLY: generateMonthlyStatistic(date, locationId, out); break;
                    default: break;
                }
            }
        }
    }

    public boolean generateMonthlyStatistic(Date date, Integer locationId, OutputStream out) {
        // todo change report time logic
        Date reportDate = DateUtils.addMonths(date, 1);
        try {
            logger.info("Start generation monthly report");
            Map<Integer, List<StatisticBean>> mapGeneral = getGeneralMonthlyStatistic(reportDate);
            Map<Integer, SortedMap<String, List<StatisticBean>>> map = getMonthlyStatistic(reportDate);

            int sheetNumber = 0;
            Integer key = locationId;

            if (locationService.getLocation(key) == null) {
                logger.info("No location were found for " + key);
                return false;
            }

            String keyString = locationService.getLocation(key).getLocationName().trim();
            logger.info("Start generation of Monthly report for location " + keyString);
            SortedMap reportHash = map.get(key);
            List reportGeneralList = mapGeneral.get(key);

            // create report section
            ExcelBuilder report = new ExcelBuilder(out);
            logger.info("Report is beeing generated for " + mapGeneral.size() + " weeks");

            report.setReportGeneralList(reportGeneralList);
            report.setReportHash(reportHash);

            report.writeMonthlyReport(sheetNumber, keyString, reportHash.size());
            report.closeWorkbook();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return false;

        } catch (WriteException e) {
            logger.error(e.getMessage(), e);
            return false;
        }
        return true;
    }

    public boolean generateWeeklyStatistic(Date date, Integer locationId, OutputStream out) {
        Date reportTime = DateUtil.lastDayOfWeek(date);     // todo change report time logic
        try {
            HashMap<Integer, List> mapGeneral = getGeneralStatistic(reportTime);
            HashMap<Integer, SortedMap> map = getStatistic(reportTime);

            int sheetNumber = 0;
            Integer key = locationId;

            String keyString = locationService.getLocation(key).getLocationName().trim();
            SortedMap reportHash = map.get(key);
            List reportGeneralList = mapGeneral.get(key);
            logger.info("Start generation of Weekly report ");

            // create report section
            ExcelBuilder report = new ExcelBuilder(out);
            logger.info("Report is beeing generated for " + reportHash.size() + " days");

            report.setReportGeneralList(reportGeneralList);
            report.setReportHash(reportHash);

            report.writeWeeklyReport(sheetNumber, keyString, 7);
            report.closeWorkbook();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return false;
        } catch (WriteException e) {
            logger.error(e.getMessage(), e);
            return false;
        }

        return true;
    }

    public boolean generateDailyStatistic(Date date, Integer locationId, OutputStream out) {
        Date reportDate = DateUtil.emptyTime(date);      // todo change report time logic
        try {
            HashMap<Integer, List<Statistic>> map = getNotificationDailyStatistic(reportDate);
            int sheetNumber = 0;

            Integer key = locationId;
            Location location = locationService.getLocation(key);
            String keyString = location.getLocationName().trim();
            logger.info("Start generation of Daily report ");

            // create report section
            ExcelBuilder report = new ExcelBuilder(out);

            List<Statistic> statistics = map.get(key);
            report.setStatisticList(statistics);

            report.writeDailyReport(sheetNumber, keyString, reportDao.getStaffByLocation(location), reportDate);
            report.closeWorkbook();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return false;
        } catch (WriteException e) {
            logger.error(e.getMessage(), e);
            return false;
        }

        return true;
    }

    protected String getReportName(String type, String location, Date date){
        return StringUtils.concat(type, "_statistic_", location, "_", DateConvertationUtils.shortStringDateFile(date), ".xls");
    }

    protected void sendReport(Integer locationId, String locationName, List<String> to, String filename, String filepath, String subject, String message){   // todo refactor!
        logger.info("Start sending the emails");
        MailSenderUtil mailSenderUtil = new MailSenderUtil();

        Map<String, File> attachment = new HashMap<String, File>();
        File file = new File(filepath);
        attachment.put(filename, file);
        if (to == null || to.isEmpty()) {
            logger.info("No emails were set for the location: " + locationName + " Please set the emails.");
            return;
        }
        if (attachment == null || attachment.size() == 0) {
            logger.info("No reports were attached to the location report: " + locationName);
            return;
        }
        String area = getLocationFooter(locationId);

        message = getHtmlMailBody(message, area);

        Map settings = getEmailReportSettings();
        String mailHostServer = (String) settings.get("smtpServer");
        String port = (String) settings.get("smtpServerPort");
        String from = (String) settings.get("fromEmail");
        String password = (String) settings.get("smtpServerFromPassword");
        String subjectText = (String) settings.get("subjectText");
        String sslcheck = (String) settings.get("sslcheck");

        if(!StringUtils.isEmpty(subject)){
            subjectText = subject;
        }

        boolean isSsl = !StringUtils.isEmpty(sslcheck) && sslcheck.equals("on");

        if (!StringUtils.isEmpty(mailHostServer) && !StringUtils.isEmpty(port) && !StringUtils.isEmpty(from) && !StringUtils.isEmpty(subjectText)) {
            mailSenderUtil.sendMonthlyReports(to, message, attachment, from, subjectText, mailHostServer, port, password, isSsl);
        }
    }

    protected String getHtmlMailBody(String text, String area){
        return StringUtils.concat(
                "<html><head></head><body>",
                "<div>", text, "</div>",
                "<div>", area, "</div>",
                "</body></html>");
    }

    public void saveReportPath(String reportPath) {
        List reportSettings = baseDao.retrieveAll(ReportSettings.class);
        ReportSettings reportSetting = (reportSettings.size() != 0) ? (ReportSettings) reportSettings.get(0) : new ReportSettings();
        reportSetting.setReportSettings(reportPath);
        baseDao.update(reportSetting);
    }

    public String getReportPath() {
        List reportSettings = baseDao.retrieveAll(ReportSettings.class);
        ReportSettings reportSetting = (reportSettings.size() != 0) ? (ReportSettings) reportSettings.get(0) : null;
        if (reportSetting != null) {
            return reportSetting.getReportSettings();
        } else {
            return null;
        }
    }

    public List<String> getLocationEmails(Integer locationId) {
        List emails = new ArrayList();
        if (locationId != null) {
            Location location = (Location) baseDao.retrieve(Location.class, locationId);
            if (location != null) {
                LocationSettings locationSettings = location.getLocationSettings();
                if (locationSettings == null) {
                    locationSettings = new LocationSettings();
                    location.setLocationSettings(locationSettings);
                }
                if (locationSettings.getEmail1() != null && !locationSettings.getEmail1().isEmpty()) {
                    emails.add(locationSettings.getEmail1());
                }
                if (locationSettings.getEmail2() != null && !locationSettings.getEmail2().isEmpty()) {
                    emails.add(locationSettings.getEmail2());
                }
                if (locationSettings.getEmail3() != null && !locationSettings.getEmail3().isEmpty()) {
                    emails.add(locationSettings.getEmail3());
                }
            }
        }
        return emails;
    }

    public List<String> getLocationDailyEmails(Integer locationId) {
        List emails = new ArrayList();
        if (locationId != null) {
            Location location = (Location) baseDao.retrieve(Location.class, locationId);
            if (location != null) {
                LocationSettings locationSettings = location.getLocationSettings();
                if (locationSettings == null) {
                    locationSettings = new LocationSettings();
                    location.setLocationSettings(locationSettings);
                }
                if (locationSettings.getEmailDaily1() != null && !locationSettings.getEmailDaily1().isEmpty()) {
                    emails.add(locationSettings.getEmailDaily1());
                }
                if (locationSettings.getEmailDaily2() != null && !locationSettings.getEmailDaily2().isEmpty()) {
                    emails.add(locationSettings.getEmailDaily2());
                }
                if (locationSettings.getEmailDaily3() != null && !locationSettings.getEmailDaily3().isEmpty()) {
                    emails.add(locationSettings.getEmailDaily3());
                }
            }
        }
        return emails;
    }

    public String getLocationFooter(Location location) {
        if (location != null) {
            LocationSettings locationSettings = location.getLocationSettings();
            if (locationSettings == null) {
                locationSettings = new LocationSettings();
                location.setLocationSettings(locationSettings);
            }
            if (locationSettings.getArea() != null) {
                return locationSettings.getArea();
            }
        }
        return "";
    }

    public String getLocationFooter(Integer locationId) {

        if (locationId != null) {
            Location location = (Location) baseDao.retrieve(Location.class, locationId);
            return getLocationFooter(location);
        }
        return "";
    }

    public String getLocationContent(Integer locationId) {

        if (locationId != null) {
            Location location = (Location) baseDao.retrieve(Location.class, locationId);
            if (location != null) {
                LocationSettings locationSettings = location.getLocationSettings();
                if (locationSettings == null) {
                    locationSettings = new LocationSettings();
                    location.setLocationSettings(locationSettings);
                }
                if (locationSettings.getPublicemalarea() != null) {
                    return locationSettings.getPublicemalarea();
                }
            }

        }
        return "";
    }

    public List<String> getLocationStrings(Integer locationId) {
        List emailStrings = new ArrayList();
        if (locationId != null) {
            Location location = (Location) baseDao.retrieve(Location.class, locationId);
            if (location != null) {
                LocationSettings locationSettings = location.getLocationSettings();
                if (locationSettings == null) {
                    locationSettings = new LocationSettings();
                    location.setLocationSettings(locationSettings);
                }
                emailStrings.add(locationSettings.getEmailString1() != null ? locationSettings.getEmailString1() : "");
                emailStrings.add(locationSettings.getEmailString2() != null ? locationSettings.getEmailString2() : "");
                emailStrings.add(locationSettings.getEmailString3() != null ? locationSettings.getEmailString3() : "");
            }
        }
        return emailStrings;
    }

    public void saveReportSettings(Integer locationId, String email1, String email2, String email3,
                                   String emailString1, String emailString2, String emailString3) {
        if (locationId != null) {
            Location location = (Location) baseDao.retrieve(Location.class, locationId);
            if (location != null) {
                LocationSettings locationSettings = location.getLocationSettings();
                if (locationSettings == null) {
                    locationSettings = new LocationSettings();
                }
                List<String> emails = new ArrayList();
                if (email1 != null && !email1.isEmpty()) {
                    emails.add(email1);
                }
                if (email2 != null && !email2.isEmpty()) {
                    emails.add(email2);
                }
                if (email3 != null && !email3.isEmpty()) {
                    emails.add(email3);
                }
                if (emails.size() == 0) {
                    locationSettings.setEmail1("");
                    locationSettings.setEmail2("");
                    locationSettings.setEmail3("");
                }
                if (emails.size() == 1) {
                    locationSettings.setEmail1(emails.get(0));
                    locationSettings.setEmail2("");
                    locationSettings.setEmail3("");
                }
                if (emails.size() == 2) {
                    locationSettings.setEmail1(emails.get(0));
                    locationSettings.setEmail2(emails.get(1));
                    locationSettings.setEmail3("");
                }
                if (emails.size() == 3) {
                    locationSettings.setEmail1(emails.get(0));
                    locationSettings.setEmail2(emails.get(1));
                    locationSettings.setEmail3(emails.get(2));
                }
                locationSettings.setEmailString1(emailString1);
                locationSettings.setEmailString2(emailString2);
                locationSettings.setEmailString3(emailString3);
                location.setLocationSettings(locationSettings);
                baseDao.create(locationSettings);
                baseDao.create(location);
            }
        }
    }

    public void saveDailyReportSettings(Integer locationId, String email1, String email2, String email3) {
        if (locationId != null) {
            Location location = (Location) baseDao.retrieve(Location.class, locationId);
            if (location != null) {
                LocationSettings locationSettings = location.getLocationSettings();
                if (locationSettings == null) {
                    locationSettings = new LocationSettings();
                }
                List<String> emails = new ArrayList();
                if (email1 != null && !email1.isEmpty()) {
                    emails.add(email1);
                }
                if (email2 != null && !email2.isEmpty()) {
                    emails.add(email2);
                }
                if (email3 != null && !email3.isEmpty()) {
                    emails.add(email3);
                }
                if (emails.size() == 0) {
                    locationSettings.setEmailDaily1("");
                    locationSettings.setEmailDaily2("");
                    locationSettings.setEmailDaily3("");
                }
                if (emails.size() == 1) {
                    locationSettings.setEmailDaily1(emails.get(0));
                    locationSettings.setEmailDaily2("");
                    locationSettings.setEmailDaily3("");
                }
                if (emails.size() == 2) {
                    locationSettings.setEmailDaily1(emails.get(0));
                    locationSettings.setEmailDaily2(emails.get(1));
                    locationSettings.setEmailDaily3("");
                }
                if (emails.size() == 3) {
                    locationSettings.setEmailDaily1(emails.get(0));
                    locationSettings.setEmailDaily2(emails.get(1));
                    locationSettings.setEmailDaily3(emails.get(2));
                }
                location.setLocationSettings(locationSettings);
                baseDao.update(locationSettings);
                baseDao.update(location);
            }
        }
    }

    public void saveEmailReportSettings(String smtpServer, String smtpServerPort, String fromEmail, String smtpServerFromPassword, String subjectText, String sslcheck) {
        List reportSettings = baseDao.retrieveAll(ReportSettings.class);
        ReportSettings reportSetting = (reportSettings.size() != 0) ? (ReportSettings) reportSettings.get(0) : new ReportSettings();
        reportSetting.setSmtpServer(smtpServer.trim());
        reportSetting.setSmtpServerPort(smtpServerPort.trim());
        reportSetting.setFromEmail(fromEmail.trim());
        reportSetting.setSmtpServerFromPassword(smtpServerFromPassword.trim());
        reportSetting.setSubjectText(subjectText.trim());
        String sslString = (sslcheck != null && sslcheck.trim().equals("on")) ? "on" : "off";
        reportSetting.setSsl(sslString);
        baseDao.update(reportSetting);
    }

    public void deleteEmailReportSettings() {
        List reportSettings = baseDao.retrieveAll(ReportSettings.class);
        ReportSettings reportSetting = (reportSettings.size() != 0) ? (ReportSettings) reportSettings.get(0) : new ReportSettings();
        reportSetting.setSmtpServer("");
        reportSetting.setSmtpServerPort("");
        reportSetting.setFromEmail("");
        reportSetting.setSmtpServerFromPassword("");
        reportSetting.setSubjectText("");
        reportSetting.setSsl("off");
        baseDao.update(reportSetting);
    }

    public Map<String, Object> getEmailReportSettings() {
        Map map = new HashMap<String, String>();
        List reportSettings = baseDao.retrieveAll(ReportSettings.class);
        ReportSettings reportSetting = (reportSettings.size() != 0) ? (ReportSettings) reportSettings.get(0) : null;
        if (reportSetting != null) {
            map.put("smtpServer", reportSetting.getSmtpServer());
            map.put("smtpServerPort", reportSetting.getSmtpServerPort());
            map.put("fromEmail", reportSetting.getFromEmail());
            map.put("smtpServerFromPassword", reportSetting.getSmtpServerFromPassword());
            map.put("subjectText", reportSetting.getSubjectText());
            map.put("sslcheck", reportSetting.getSsl() != null ? reportSetting.getSsl() : "off");
            return map;
        } else {
            return null;
        }
    }

    public void saveEmailFooter(Integer locationId, String footer) {
        if (locationId != null) {
            Location location = (Location) baseDao.retrieve(Location.class, locationId);
            if (location != null) {
                LocationSettings locationSettings = location.getLocationSettings();
                if (locationSettings == null) {
                    locationSettings = new LocationSettings();
                }
                locationSettings.setArea(footer);
                location.setLocationSettings(locationSettings);
                baseDao.update(locationSettings);
                baseDao.update(location);
            }
        }
    }

    public void saveEmailPublicContent(Integer locationId, String content) {
        if (locationId != null) {
            Location location = (Location) baseDao.retrieve(Location.class, locationId);
            if (location != null) {
                LocationSettings locationSettings = location.getLocationSettings();
                if (locationSettings == null) {
                    locationSettings = new LocationSettings();
                }
                locationSettings.setPublicemalarea(content);
                location.setLocationSettings(locationSettings);
                baseDao.update(locationSettings);
                baseDao.update(location);
            }
        }
    }

    public void sendDailyUploadReport(List<String> to, String reportName, String newDate) {
        logger.info("Start sending the report");
        MailSenderUtil mailSenderUtil = new MailSenderUtil();
        String msgText1 = "Daily UPLOAD Statistic report for " + newDate;

        Map<String, File> attachment = new HashMap<String, File>();
        File file = new File(reportName);
        attachment.put("uploadReport" + "_" + newDate, file);
        if (to == null || to.isEmpty()) {
            logger.info("No emails were set for the upload report " + " Please set the emails.");
            return;
        }
        if (attachment == null || attachment.size() == 0) {
            logger.info("No reports were attached to the upload report");
            return;
        }
        String area = "";
        String headerMain = "<html><head></head><body>";
        String content = "<div>" + msgText1 + "</div>";
        String contentFooter = "<div>" + area + "</div>";
        String footerMain = "</body></html>";

        msgText1 = headerMain + content + contentFooter + footerMain;
        Map settings = getEmailReportSettings();
        String mailHostServer = (String) settings.get("smtpServer");
        String port = (String) settings.get("smtpServerPort");
        String from = (String) settings.get("fromEmail");
        String password = (String) settings.get("smtpServerFromPassword");
        String subject = "Daily upload report for " + newDate;
        boolean isSsl = ((String) settings.get("sslcheck") != null && ((String) settings.get("sslcheck")).equals("on")) ? true : false;
        if (mailHostServer != null && !mailHostServer.isEmpty() && port != null && !port.isEmpty() && from != null
                && !from.isEmpty() && subject != null && !subject.isEmpty()) {
            mailSenderUtil.sendMonthlyReports(to, msgText1, attachment, from, subject, mailHostServer, port, password, isSsl);
        }
    }

    public void setReportDao(IReportDao reportDao) {
        this.reportDao = reportDao;
    }

    public void setBaseDao(IDao baseDao) {
        this.baseDao = baseDao;
    }

    public enum ReportType{
        DAILY, WEEKLY, MONTHLY;

        public static ReportType getType(String value){
            if(value != null){
                try{
                    return ReportType.valueOf(value.toUpperCase());
                }catch (Exception e){
                }
            }
            return null;
        }
    }
}
