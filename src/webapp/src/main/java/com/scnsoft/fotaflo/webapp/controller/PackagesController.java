package com.scnsoft.fotaflo.webapp.controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.model.Package;
import com.scnsoft.fotaflo.webapp.service.LocationService;
import com.scnsoft.fotaflo.webapp.service.PackageService;
import com.scnsoft.fotaflo.webapp.service.PictureService;
import com.scnsoft.fotaflo.webapp.util.AccessID;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: MilkevichP
 * Date: 25.04.11
 * Time: 17:15
 * To change this template use File | Settings | File Templates.
 */
@Controller()
@RequestMapping("/packages")
public class PackagesController {

    protected static Logger logger = Logger.getLogger(PackagesController.class);
    private final static String rootProperty = "packages";
    private final static String successProperty = "success";
    private final static String locationId = "locationId";
    private PackageService packageService;
    private LocationService locationService;
    @Autowired
    private PictureService pictureService;

    @RequestMapping(value = "getPackages.json"/*, method = RequestMethod.GET*/)
    public
    @ResponseBody
    Map<String, ? extends Object> getPackages(@RequestParam(value = PackagesController.locationId, required = false) Integer locationId, HttpServletRequest httpServletRequest) {

        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        HashMap modelMap = new HashMap();
        int start = Integer.valueOf(httpServletRequest.getParameter("start") != null ? httpServletRequest.getParameter("start") : "0");
        int limit = Integer.valueOf(httpServletRequest.getParameter("limit") != null ? httpServletRequest.getParameter("limit") : "0");
        modelMap.put(rootProperty, packageService.getPackagesByLocation(currentUser, locationId == null ? 0 : locationId, start, limit));
        modelMap.put("total", packageService.getPackagesByLocation(currentUser, locationId == null ? 0 : locationId, 0, 0).size());
        modelMap.put(successProperty, true);
        return modelMap;
    }

    @RequestMapping(value = "getPackagesPurchase.json"/*, method = RequestMethod.GET*/)
    public
    @ResponseBody
    Map<String, ? extends Object> getPackagesPurchase(@RequestParam(value = PackagesController.locationId, required = false) Integer locationId, HttpServletRequest httpServletRequest) {

        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        HashMap modelMap = new HashMap();
        int start = Integer.valueOf(httpServletRequest.getParameter("start") != null ? httpServletRequest.getParameter("start") : "0");
        int limit = Integer.valueOf(httpServletRequest.getParameter("limit") != null ? httpServletRequest.getParameter("limit") : "0");
        int access = pictureService.getUserAccess(currentUser);
        if (access == AccessID.PUBLICUSER.getValue() || access == AccessID.PUBLICEMAILUSER.getValue() || access == AccessID.PUBLICTAGUSER.getValue()) {
            modelMap.put(rootProperty, packageService.getPublicPackagesForUser(currentUser, start, limit));
            modelMap.put("total", packageService.getPublicPackagesForUser(currentUser, 0, 0).size());
        } else {
            modelMap.put(rootProperty, packageService.getPackagesByLocation(currentUser, locationId == null ? 0 : locationId, start, limit));
            modelMap.put("total", packageService.getPackagesByLocation(currentUser, locationId == null ? 0 : locationId, 0, 0).size());
        }
        modelMap.put(successProperty, true);
        return modelMap;
    }

    @RequestMapping(value = "savePackages.json"/*, method = RequestMethod.GET*/)
    public
    @ResponseBody
    Map<String, ? extends Object> savePackages(@RequestParam(value = PackagesController.rootProperty, required = false) String json, @RequestParam(value = PackagesController.locationId, required = false) Integer locationId) {

        HashMap modelMap = new HashMap();
        Gson g = new Gson();
        List ls;

        try {
            Type listType = new TypeToken<List<Package>>() {
            }.getType();
            ls = g.fromJson(json, listType);
        } catch (Exception e) {
            ls = new ArrayList();
            ls.add(g.fromJson(json, Package.class));
        }
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        Location loc = locationService.getLocation(locationId);
        if (loc != null) {
            for (Object l : ls) {
                Package p = (Package) l;
                p.setLocation(loc);
                p.setAccess(Package.PackageAccess.DEFAULT.getId());
                logger.info("User [ " + currentUser + " ]" + " updates package " + p.getPackageName() + "  for location  " + loc.getLocationName());
            }
        }
        packageService.savePackages(ls);
        modelMap.put(rootProperty, ls.size() == 1 ? ls.get(0) : ls);
        modelMap.put(successProperty, true);

        return modelMap;
    }

    @RequestMapping(value = "deletePackages.json"/*, method = RequestMethod.GET*/)
    public
    @ResponseBody
    Map<String, ? extends Object> deletePackages(@RequestParam(value = PackagesController.rootProperty, required = false) Integer[] ids) {

        packageService.deletePackages(ids);
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User [ " + currentUser + " ]" + " deleted " + ids.length + "  packages");
        HashMap<String, Boolean> modelMap = new HashMap<String, Boolean>();
        modelMap.put(successProperty, true);
        return modelMap;
    }

    @Autowired
    public void setPackageService(PackageService packageService) {
        this.packageService = packageService;
    }

    @Autowired
    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }
}
