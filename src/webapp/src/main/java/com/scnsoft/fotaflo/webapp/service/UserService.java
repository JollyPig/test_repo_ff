package com.scnsoft.fotaflo.webapp.service;

import com.scnsoft.fotaflo.webapp.bean.UserBean;
import com.scnsoft.fotaflo.webapp.dao.*;
import com.scnsoft.fotaflo.webapp.model.*;
import com.scnsoft.fotaflo.webapp.util.AccessID;
import com.scnsoft.fotaflo.webapp.util.DefaultValues;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */

@Service
@Transactional
public class UserService {
    protected final static Logger logger = Logger.getLogger(UserService.class);

    @Autowired
    private ISystemUserDao systemUserDao;

    @Autowired
    private IFilterDao filterDao;

    @Autowired
    private IPictureDao pictureDao;

    @Autowired
    private IPurchaseSelectedDao purchaseSelectedDao;

    @Autowired
    private IHelpYourSelfSelctedDao helpYourSelfSelctedDao;

    @Autowired
    private IRequestDao requestDao;

    @Autowired
    private ContactService contactService;

    @Autowired
    private IDao baseDao;

    public List<UserBean> getUsersAll(int start, int limit, String userlogin) {
        List<UserBean> userBeanList = new ArrayList<UserBean>();
        SystemUser systemUser = getUserByLogin(userlogin);
        if (systemUser.getAccess().equals("1")) {

            List<SystemUser> list = systemUserDao.find(start, limit);
            if (list != null && !list.isEmpty()) {
                for (SystemUser user : list) {
                    userBeanList.add(new UserBean(user.getId(), user.getLoginName(), user.getPassword(), user.getFirstName(), user.getLastName(), String.valueOf(user.getLocation().getId()), user.getLocation().getLocationName(), user.getAccess()));
                }
            }
        }

        return userBeanList;
    }

    public Long getUserCount() {
        return systemUserDao.count();
    }

    public List<UserBean> getUsersByName(String name, int start, int limit) {
        List<UserBean> userBeanList = new ArrayList<UserBean>();
        SystemUser systemUser = getUserByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
        if (systemUser.getAccess().equals("1")) {

            List<SystemUser> list = systemUserDao.findByName(name, start, limit);
            if (list != null && !list.isEmpty()) {
                for (SystemUser user : list) {
                    userBeanList.add(new UserBean(user.getId(), user.getLoginName(), user.getPassword(), user.getFirstName(), user.getLastName(), String.valueOf(user.getLocation().getId()), user.getLocation().getLocationName(), user.getAccess()));
                }
            }
        }

        return userBeanList;
    }

    public Long getUserCountByName(String name) {
        return systemUserDao.countByName(name);
    }

    public SystemUser getUserByLogin(String userlogin) {
        return systemUserDao.getUserByLogin(userlogin);
    }

    public SystemUser getCurrentUser() {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        return systemUserDao.getUserByLogin(currentUser);
    }

    public void makeTagAsACode(SystemUser user) {
        if (user != null) {
            if (user.getLocation() != null && user.getLocation().getGiveaway() != null && user.getLocation().getGiveaway() == true) {
                user.setAccess(String.valueOf(AccessID.PUBLICTAGACODE.getValue()));
                baseDao.update(user);
            }
        }

    }

    public boolean createUserBean(UserBean userBean, String userlogin) {
        SystemUser systemUser = getUserByLogin(userlogin);
        if (systemUser.getAccess().equals("1")) {
            if (userBean != null) {
                SystemUser user = new SystemUser();
                if (getUserByLogin(userBean.getLoginName()) != null) {
                    return false;
                }
                user.setLoginName(userBean.getLoginName());
                Location location = (Location) baseDao.retrieve(Location.class, Integer.valueOf(userBean.getLocationId()));
                Md5PasswordEncoder md5PasswordEncoder = new Md5PasswordEncoder();
                user.setPassword(md5PasswordEncoder.encodePassword(userBean.getPassword(), null));
                user.setAccess(userBean.getAccess());
                user.setFirstName(userBean.getFirstName());
                user.setLastName(userBean.getLastName());
                //user.setEmail("Ivanov@.kjl.by");
                user.setLocation(location);
                UserSettings userSettings = new UserSettings();
                userSettings.setPageSize(DefaultValues.pageSize);
                userSettings.setSpeed(DefaultValues.speed);
                userSettings.setStartTime(DefaultValues.startTime);
                userSettings.setPictureCount(DefaultValues.pictureCount);
                user.setUserSettings(userSettings);
                baseDao.update(user);
            }
        }
        return true;
    }

    public boolean checkLogin(String userlogin) {
        if (userlogin == null) return true;
        if (getUserByLogin(userlogin) != null) {
            return false;
        } else {
            return true;
        }
    }

    public void update(UserSettings userSettings) {
        if (userSettings != null) {
            baseDao.update(userSettings);
        }
    }

    public void updateUserBean(UserBean userBean, String userlogin) {
        SystemUser systemUser = getUserByLogin(userlogin);
        if (systemUser.getAccess().equals("1")) {
            if (userBean != null) {
                SystemUser user = (SystemUser) baseDao.retrieve(SystemUser.class, userBean.getId());
                Location location = (Location) baseDao.retrieve(Location.class, Integer.valueOf(userBean.getLocationId()));
                user.setLoginName(userBean.getLoginName());
                if (userBean.getPassword().length() != 0) {
                    Md5PasswordEncoder md5PasswordEncoder = new Md5PasswordEncoder();
                    user.setPassword(md5PasswordEncoder.encodePassword(userBean.getPassword(), null));
                }
                if (systemUser.getId() != user.getId()) {
                    user.setAccess(userBean.getAccess());
                } else {
                    user.setAccess("1");
                }
                user.setFirstName(userBean.getFirstName());
                user.setLastName(userBean.getLastName());
                //user.setEmail("Unknown@unknown.unknown");
                user.setLocation(location);
                baseDao.update(user);
            }
        }
    }

    public void deleteUserBean(Object data, String userlogin) {
        SystemUser systemUser = getUserByLogin(userlogin);
        SystemUser user = (SystemUser) baseDao.retrieve(SystemUser.class, Integer.valueOf((String) data).intValue());
        if (systemUser.getAccess().equals("1") && systemUser.getId() != user.getId()) {
            UserFilter userFilter = filterDao.getFilterByUser(user);
            if (userFilter != null) {
                baseDao.delete(userFilter);
            }
            pictureDao.removePicturesSelectedForUser(user);

            List<PurchaseSelected> purchaseSelecteds = purchaseSelectedDao.getPurchsePictureByUser(user);
            if (purchaseSelecteds != null) {
                for (PurchaseSelected purchaseSelected : purchaseSelecteds) {
                    baseDao.delete(purchaseSelected);
                }
            }

            List<HelpYourselfSelected> helpYourselfSelecteds = helpYourSelfSelctedDao.getHelpYourselfByUser(user);
            if (helpYourselfSelecteds != null) {
                for (HelpYourselfSelected helpYourselfSelected : helpYourselfSelecteds) {
                    baseDao.delete(helpYourselfSelected);
                }
            }

            List<Requests> requestses = requestDao.getRequestsByUser(user);
            if (requestses != null) {
                for (Requests requests : requestses) {
                    requests.setSystemUser(null);
                }
            }

            contactService.deleteContactsForUser(user);

            baseDao.delete(user);
        }
    }

    public void deletePublicLoginExpired(Date start, Date end, String access) {
        List<SystemUser> usersToDelete = systemUserDao.getPublicUsersByDate(start, end, access);
        if (usersToDelete != null && !usersToDelete.isEmpty()) {
            for (SystemUser userToDelete : usersToDelete) {
                if (userToDelete.getAccess().equals(access)) {
                    UserFilter userFilter = filterDao.getFilterByUser(userToDelete);
                    if (userFilter != null) {
                        baseDao.delete(userFilter);
                    }
                    pictureDao.removePicturesSelectedForUser(userToDelete);

                    List<PurchaseSelected> purchaseSelecteds = purchaseSelectedDao.getPurchsePictureByUser(userToDelete);
                    if (purchaseSelecteds != null) {
                        for (PurchaseSelected purchaseSelected : purchaseSelecteds) {
                            baseDao.delete(purchaseSelected);
                        }
                    }

                    List<HelpYourselfSelected> helpYourselfSelecteds = helpYourSelfSelctedDao.getHelpYourselfByUser(userToDelete);
                    if (helpYourselfSelecteds != null) {
                        for (HelpYourselfSelected helpYourselfSelected : helpYourselfSelecteds) {
                            baseDao.delete(helpYourselfSelected);
                        }
                    }

                    List<Requests> requestses = requestDao.getRequestsByUser(userToDelete);
                    if (requestses != null) {
                        for (Requests requests : requestses) {
                            requests.setSystemUser(null);
                        }
                    }
                    contactService.deleteContactsForUser(userToDelete);
                    baseDao.delete(userToDelete);
                }

            }

        }
    }

    public Location getUserLocation(String userLogin) {
        return getUserByLogin(userLogin).getLocation();
    }

    public UserSettings getUserSettings(String userLogin) {
        return getUserByLogin(userLogin).getUserSettings();
    }

    public boolean hasAdminRights(SystemUser user) {
        return SystemUser.ADMIN_ACCESS.equals(user.getAccess());
    }
}
