package com.scnsoft.fotaflo.webapp.service;

import com.scnsoft.fotaflo.webapp.bean.StaffBean;
import com.scnsoft.fotaflo.webapp.dao.IStaffDao;
import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.model.Staff;
import com.scnsoft.fotaflo.webapp.model.Statistic;
import com.scnsoft.fotaflo.webapp.model.SystemUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by Nadezda Drozdova
 * Date Apr 11, 2011
 */

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class StaffService {

    @Autowired
    private IStaffDao staffDao;

    @Autowired
    private UserService userService;

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<StaffBean> getStaffByLocation(String userlogin, int locationId) {
        List<StaffBean> list = new ArrayList<StaffBean>();
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        if (!userService.hasAdminRights(systemUser)) {
            Location location = systemUser.getLocation();
            List<Staff> packages = staffDao.getStaffByLocation(location);
            Iterator iter = packages.iterator();
            while (iter.hasNext()) {
                Staff pack = (Staff) iter.next();
                StaffBean staffBean = new StaffBean(pack.getId(), pack.getStaffName());
                staffBean.setLocationId(String.valueOf(location.getId()));
                staffBean.setLocationName(location.getLocationName());
                list.add(staffBean);
            }
        } else {
            if (locationId != 0) {
                Location location = (Location) staffDao.retrieve(Location.class, locationId);
                List<Staff> staffList = staffDao.getStaffByLocation(location);
                for (Staff staff : staffList) {
                    StaffBean staffBean = new StaffBean(staff.getId(), staff.getStaffName());
                    staffBean.setLocationId(String.valueOf(location.getId()));
                    staffBean.setLocationName(location.getLocationName());
                    list.add(staffBean);
                }
            } else {
                List<Staff> staffList = staffDao.retrieveAll(Staff.class);
                for (Staff staff : staffList) {
                    StaffBean staffBean = new StaffBean(staff.getId(), staff.getStaffName());
                    staffBean.setLocationId(String.valueOf(staff.getLocation().getId()));
                    staffBean.setLocationName(staff.getLocation().getLocationName());
                    list.add(staffBean);
                }
            }
        }

        Collections.sort(list, new Comparator<StaffBean>() {
            public int compare(StaffBean o1, StaffBean o2) {
                if (o1.getStaffName() != null && o2.getStaffName() != null) {
                    return o1.getStaffName().compareTo(o2.getStaffName());
                } else {
                    return 0;
                }
            }
        });

        return list;
    }

    public List<StaffBean> getPagedStaffForLocation(String userlogin, int start, int limit) {
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        List<StaffBean> list = new ArrayList<StaffBean>();
        Location location = null;
        if (!userService.hasAdminRights(systemUser)) {
            location = systemUser.getLocation();
        }
        List<Staff> staffList = staffDao.getPagedStaffByLocation(location, start, limit);
        for (Staff staff : staffList) {
            StaffBean staffBean = new StaffBean(staff.getId(), staff.getStaffName());
            staffBean.setLocationId(String.valueOf(staff.getLocation().getId()));
            staffBean.setLocationName(staff.getLocation().getLocationName());
            list.add(staffBean);
        }
        return list;
    }

    public int countStaffForLocation(String userlogin) {
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        Location location = null;
        if (!userService.hasAdminRights(systemUser)) {
            location = systemUser.getLocation();
        }
        return staffDao.countStaffForLocation(location);
    }

    public Staff getStaffById(Integer staffId) {
        return (Staff) staffDao.retrieve(Staff.class, staffId);
    }

    public Staff updateStaff(Staff staff) {
        staffDao.update(staff);
        return staff;
    }

    public Staff saveStaff(Staff staff) {
        staffDao.create(staff);
        return staff;
    }

    public void deleteStaffById(Integer id) {
        Staff staff = (Staff) staffDao.retrieve(Staff.class, id);
        List<Statistic> statisticList = staffDao.getStatisticForStaff();
        for (Statistic statistic : statisticList) {
            if (statistic.getStaff().contains(staff)) {
                statistic.getStaff().remove(staff);
            }
            staffDao.update(statistic);
        }
        staffDao.delete(staff);
    }
}
