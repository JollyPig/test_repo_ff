package com.scnsoft.fotaflo.webapp.bean;

/**
 * Created by Nadezda Drozdova
 * Date Apr 11, 2011
 */
public class StaffBean {

    protected int id;
    private String staffName;
    private String locationName;
    private String locationId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public StaffBean(int id, String staffName) {
        this.id = id;
        this.staffName = staffName;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }
}
