package com.scnsoft.fotaflo.webapp.web.converter;

/**
 *
 * Convert object from domain model to web model and reverse.
 *
 * @author Anatoly Selitsky
 *
 */
public interface Converter<D, W> {

    W toWeb(D domain);

    D toDomain(W web);

}
