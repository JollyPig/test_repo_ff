package com.scnsoft.fotaflo.webapp.dao.impl;

import com.scnsoft.fotaflo.webapp.dao.IFilterDao;
import com.scnsoft.fotaflo.webapp.model.SystemUser;
import com.scnsoft.fotaflo.webapp.model.UserFilter;

import java.util.List;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */
public class FilterDao extends BaseDao implements IFilterDao {

    public UserFilter getFilterByUser(SystemUser user) {
        List<UserFilter> result = getHibernateTemplate().find(
                "from UserFilter filter where filter.systemUser = ?", user);
        if (result != null && result.size() == 1) {
            return (UserFilter) result.get(0);
        }

        return null;
    }
}
