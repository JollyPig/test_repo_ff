package com.scnsoft.fotaflo.webapp.startupService;

import com.scnsoft.fotaflo.webapp.util.MailSenderUtil;

import java.util.Arrays;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 21.06.13
 * Time: 17:10
 * To change this template use File | Settings | File Templates.
 */

public class StartUpService {

    private String emailTo;
    private String smtp;
    private String port;
    private String login;
    private String password;
    private String ssl;

    private MailSenderUtil mailSender = new MailSenderUtil();

    public void sendStartUpNotification() {
        sendReportAboutCrashUploadPictures();
    }

    public void sendReportAboutCrashUploadPictures() {
        mailSender.sendMailMessage(
                Arrays.asList(emailTo.split(",")),
                login,
                "Server has been restarted",
                "Server has been restarted on " + new Date(System.currentTimeMillis()),
                null,
                smtp,
                port,
                password,
                "on".equals(ssl),
                null
        );
    }


    public String getEmailTo() {
        return emailTo;
    }

    public void setEmailTo(String emailTo) {
        this.emailTo = emailTo;
    }

    public String getSmtp() {
        return smtp;
    }

    public void setSmtp(String smtp) {
        this.smtp = smtp;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSsl() {
        return ssl;
    }

    public void setSsl(String ssl) {
        this.ssl = ssl;
    }
}

