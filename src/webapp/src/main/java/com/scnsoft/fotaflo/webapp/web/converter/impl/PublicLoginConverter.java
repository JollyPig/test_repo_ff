package com.scnsoft.fotaflo.webapp.web.converter.impl;

import com.scnsoft.fotaflo.webapp.model.SystemUser;
import com.scnsoft.fotaflo.webapp.web.converter.Converter;
import com.scnsoft.fotaflo.webapp.web.model.PublicLoginModel;
import org.springframework.stereotype.Component;

/**
 * Convert SystemUser to PublicLoginModel and reverse.
 *
 * @author Anatoly Selitsky
 */
@Component
public class PublicLoginConverter implements Converter<SystemUser, PublicLoginModel> {

    @Override
    public PublicLoginModel toWeb(SystemUser domain) {
        PublicLoginModel web = new PublicLoginModel();
        web.setLogin(domain.getLoginName());
        web.setPassword(domain.getPassword());
        web.setExpirationdate(domain.getExpirationDate().toString());
        return web;
    }

    @Override
    @Deprecated
    public SystemUser toDomain(PublicLoginModel web) {
        throw new UnsupportedOperationException();
    }

}
