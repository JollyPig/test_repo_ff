package com.scnsoft.fotaflo.webapp.service;

import com.scnsoft.fotaflo.common.model.IEntity;
import com.scnsoft.fotaflo.common.util.NumberUtils;
import com.scnsoft.fotaflo.common.util.StringUtils;
import com.scnsoft.fotaflo.webapp.bean.CodeBean;
import com.scnsoft.fotaflo.webapp.bean.TagSavedPurchaseBean;
import com.scnsoft.fotaflo.webapp.dao.ITagDao;
import com.scnsoft.fotaflo.webapp.dao.impl.BaseDao;
import com.scnsoft.fotaflo.webapp.model.*;
import com.scnsoft.fotaflo.webapp.util.AccessID;
import com.scnsoft.fotaflo.webapp.util.DateConvertationUtils;
import com.scnsoft.fotaflo.webapp.util.Filter;
import com.scnsoft.fotaflo.webapp.util.PictureUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 26.05.14
 * Time: 15:14
 * To change this template use File | Settings | File Templates.
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class TagPurchaseService {
    protected static Logger logger = Logger.getLogger(PurchaseService.class);

    @Autowired
    private LocationService locationService;

    @Autowired
    private BaseDao baseDao;

    @Autowired
    private ITagDao tagDao;

    @Autowired
    private PublicLoginService publicLoginService;

    @Autowired
    private TagService tagService;

    @Autowired
    private UserService userService;

    @Autowired
    private StaffService staffService;

    public TagPurchase createTagPurchased(String code, SystemUser user, String idPurchaseLocation, String idPackage,
                                          Integer participant, String idStaff, String subject, String attach_filename,
                                          String email_body, Object emails, String startDay, String endDay, String startTime, String endTime) {
        TagPurchase tagPurchase = new TagPurchase();
        tagPurchase.setStatusCode(TagPurchase.StatusCode.ACTUAL.getValue());
        tagPurchase.setGeneratedCode(code);
        tagPurchase.setCreationDate(new Date());
        tagPurchase.setPurchaseDate(new Date());

        tagPurchase.setLocationPurchase((idPurchaseLocation != null && !idPurchaseLocation.equals("")) ? locationService.getLocation(Integer.valueOf(idPurchaseLocation)) : null);
        tagPurchase.setPurchasePackage((idPackage != null && !idPackage.equals("")) ? (com.scnsoft.fotaflo.webapp.model.Package) baseDao.retrieve(com.scnsoft.fotaflo.webapp.model.Package.class, Integer.valueOf(idPackage)) : null);

        tagPurchase.setStaffs(idStaff);
        tagPurchase.setStaffList(getStaffList(idStaff));

        tagPurchase.setParticipants(participant);

        tagPurchase.setSubject(subject);
        tagPurchase.setSystemUser(user);
        tagPurchase.setEmailBody(email_body);
        tagPurchase.setStringFileName(attach_filename);

        tagPurchase.setStartDate(DateConvertationUtils.formDate(startDay, startTime, new Date()));
        tagPurchase.setEndDate(DateConvertationUtils.formDate(endDay, endTime, new Date()));

        List<String> emailsStringList = emails != null ? PictureUtil.getPurchaseEmails(emails) : new ArrayList<String>();

        String emailsString = "";
        if (!participant.equals("0")) {
            for (String em : emailsStringList) {
                if (em != null && em.length() != 0) {
                    emailsString = emailsString.length() == 0 ? em : emailsString + "," + em;
                }
            }
        }
        String otherEmailsString = "";

        tagPurchase.setParticipantsEmails(emailsString);
        tagPurchase.setParticipantsOtherEmails(otherEmailsString);

        Integer tagPInteger = baseDao.create(tagPurchase);

        return (TagPurchase) baseDao.retrieve(TagPurchase.class, tagPInteger);
    }

    private List<Staff> getStaffList(String ids){
        List<Staff> list = new ArrayList<Staff>();
        if(!StringUtils.isEmpty(ids)){
            Integer i;
            Staff e;
            String[] ida = ids.split(",");
            for(String id: ida){
                i = NumberUtils.getInteger(id);
                if(i != null){
                    e = staffService.getStaffById(i);
                    if(e != null){
                        list.add(e);
                    }
                }
            }
        }

        return list;
    }

    public Integer updateTagPurchase(TagPurchase tagPurchase) {
        if (tagPurchase != null) {
            baseDao.update(tagPurchase);
        }

        return tagPurchase.getId();
    }

    public Tag createTag(SystemUser systemUser, String tagName) {
        //Creating tag code user
        Date today = new Date();
        publicLoginService.createTagPublicLogin(tagName, systemUser.getLocation(), today, String.valueOf(AccessID.PUBLICTAGUSER.getValue()));
        Tag picTag = tagService.getTagByNameLocation(tagName, systemUser.getLocation());
        if (picTag == null) {
            picTag = new Tag(today, tagName, systemUser.getLocation());
            Integer id = baseDao.create(picTag);
            picTag = (Tag) baseDao.retrieve(Tag.class, id);
        }
        return picTag;
    }

    public List<Picture> getPictureForTagPurchase(TagPurchase tagPurchase) {
        logger.info("Getting pictures for tag purchase " + tagPurchase.getGeneratedCode());
        Date startDate = tagPurchase.getStartDate();
        Date endDate = tagPurchase.getEndDate();
        List<Picture> pictures = new ArrayList<Picture>();
        Set<Tag> tags = tagPurchase.getTags();
        logger.info("Corresponding tags " + tags.size());
        if (!tags.isEmpty()) {
            Map<Integer, Picture> pictureMap = new HashMap<Integer, Picture>();
            for (Tag tag : tags) {
                logger.info("Tag: " + tag.getTagName() + " " + tag.getTagedpictures().size());
                Set<Picture> taggedPictures = tag.getTagedpictures();
                if (!taggedPictures.isEmpty()) {
                    for (Picture tPicture : taggedPictures) {
                        if (!pictureMap.containsKey(tPicture.getId())) {
                            if (startDate != null && endDate != null &&
                                    tPicture.getCreationDate().after(startDate) && tPicture.getCreationDate().before(endDate)) {
                                pictureMap.put(tPicture.getId(), tPicture);
                                pictures.add(tPicture);
                            }
                        }
                    }
                }
            }
        }
        logger.info("Total: " + pictures.size());

        return pictures;
    }

    public List<Picture> getPictureForTagPurchase(Set<TagPurchase> tagPurchasesSet) {
        logger.info("Getting pictures for tag purchase SET " + tagPurchasesSet.size());
        List<Picture> pictures = new ArrayList<Picture>();
        Map<Integer, Picture> pictureMap = new HashMap<Integer, Picture>();

        if (!tagPurchasesSet.isEmpty()) {
            for (TagPurchase tagPurchase : tagPurchasesSet) {
                Date startDate = tagPurchase.getStartDate();
                Date endDate = tagPurchase.getEndDate();

                logger.info("Getting pictures for tag purchase " + tagPurchase.getGeneratedCode());
                Set<Tag> tags = tagPurchase.getTags();
                logger.info("Corresponding tags " + tags.size());
                if (!tags.isEmpty()) {
                    for (Tag tag : tags) {
                        if (tag != null) {
                            logger.info("Tag: " + tag.getTagName() + " " + tag.getTagedpictures().size());
                        }
                        Set<Picture> taggedPictures = tag.getTagedpictures();
                        if (!taggedPictures.isEmpty()) {
                            for (Picture tPicture : taggedPictures) {
                                if (!pictureMap.containsKey(tPicture.getId())) {
                                    if (startDate != null && endDate != null &&
                                            tPicture.getCreationDate().after(startDate) && tPicture.getCreationDate().before(endDate)) {
                                        pictureMap.put(tPicture.getId(), tPicture);
                                        pictures.add(tPicture);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        logger.info("Total Pictures in set: " + pictures.size());

        return pictures;
    }

    public void refundSystemUser(String userTag) {
        SystemUser user = userService.getUserByLogin(userTag);
        if (user != null && (user.getAccess().equals(String.valueOf(AccessID.PUBLICTAGCODEUSER.getValue())) || user.getAccess().equals(String.valueOf(AccessID.PUBLICTAGACODE.getValue())))) {
            user.setAccess(String.valueOf(AccessID.PUBLICTAGUSER.getValue()));
            logger.info("Public user is deleted " + user.getLoginName());
            baseDao.update(user);
        }
    }

    public void refundTag(TagPurchase tagPurchase) {
        logger.info("Refunding purcahse " + tagPurchase.getGeneratedCode());
        List<String> alphabet = new ArrayList<String>(Arrays.asList("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "w", "v", "x", "y", "z"));
        if (tagPurchase != null) {
            tagPurchase.setStatusCode(TagPurchase.StatusCode.REFUNDED.getValue());  //REFUNDED
            tagPurchase.setRefundedDate(new Date());
            baseDao.update(tagPurchase);
            //Make the user PUBLICTAGUSER that means it will be able to see pictures with watermarks only
            SystemUser publicUser = userService.getUserByLogin(tagPurchase.getGeneratedCode());
            if (publicUser != null && (publicUser.getAccess().equals(String.valueOf(AccessID.PUBLICTAGCODEUSER.getValue())) || publicUser.getAccess().equals(String.valueOf(AccessID.PUBLICTAGACODE.getValue())))) {
                publicUser.setAccess(String.valueOf(AccessID.PUBLICTAGUSER.getValue()));
                logger.info("Public user is deleted " + publicUser.getLoginName());
                baseDao.update(publicUser);
            }
            //Refund also all created  logins sent to emails
            for (int i = 0; i < 25; i++) {
                String newCode = tagPurchase.getGeneratedCode() + alphabet.get(i);
                SystemUser publicUserChild = userService.getUserByLogin(newCode);
                if (publicUserChild != null && (publicUserChild.getAccess().equals(String.valueOf(AccessID.PUBLICTAGCODEUSER.getValue())) || publicUserChild.getAccess().equals(String.valueOf(AccessID.PUBLICTAGACODE.getValue())))) {
                    publicUserChild.setAccess(String.valueOf(AccessID.PUBLICTAGUSER.getValue()));
                    logger.info("Public produced user is deleted " + publicUserChild.getLoginName());
                    baseDao.update(publicUserChild);
                }
            }
        }
    }

    public TagPurchase getPurchaseTagByCode(String tagCode) {
        if (tagCode == null) return null;
        return tagDao.getTagPurchaseByCode(tagCode);
    }

    public TagPurchase getPurchaseTagById(Integer id) {
        if (id == null) return null;
        return (TagPurchase) baseDao.retrieve(TagPurchase.class, id);
    }

    public boolean removeTagPurchase(Integer id) {
        if (id == null || id == -1) return false;
        TagPurchase tagPurchase = (TagPurchase) baseDao.retrieve(TagPurchase.class, id);
        if (tagPurchase == null) return false;
        tagPurchase.setStatusCode(TagPurchase.StatusCode.REMOVED.getValue());
        baseDao.update(tagPurchase);
        return true;
    }

    public List<CodeBean> getPurchaseTagByTimeRangeLocation(Date start, Date end, Location location) {
        List<CodeBean> codeBeans = new ArrayList<CodeBean>();
        List<TagPurchase> tagPurchases = tagDao.getTagPurchaseByDatesLocation(start, end, location);

        for (TagPurchase tagPurchase : tagPurchases) {
            CodeBean codeBean = new CodeBean(tagPurchase);
            codeBeans.add(codeBean);
        }
        return codeBeans;
    }

    public List<TagSavedPurchaseBean> getRefundedTagPurchasesByParameters(Filter filter, String email, String code) {
        return getTagPurchasesByParameters(filter, email, code, TagPurchase.StatusCode.REFUNDED.getValue(), null);
    }

    public List<TagSavedPurchaseBean> getHistoryTagPurchasesByParameters(Filter filter, String email, String code, String tag) {
        return getTagPurchasesByParameters (filter, email, code, TagPurchase.StatusCode.ACTUAL.getValue(), tag);
    }

    protected Map getMap(List<? extends IEntity> entityList){  // todo make as a part of dao interface
        Map<String, IEntity> map = new HashMap<String, IEntity>();
        for (IEntity entity: entityList) {
            if(entity != null){
                map.put(String.valueOf(entity.getId()), entity);
            }
        }

        return map;
    }

    private String getStaffNames(List<Staff> staffs){
        StringBuilder sb = new StringBuilder();
        if(staffs != null){
            for (Staff staff: staffs){
                sb.append(staff.getStaffName());
                sb.append(",");
            }
        }

        return sb.length() > 0 ? sb.substring(0, sb.length() - 1) : "";
    }

    protected List<TagSavedPurchaseBean> getTagPurchasesByParameters(Filter filter, String email, String code, Integer status, String targetTag) {
        List<TagSavedPurchaseBean> savedPurchaseBeanList = new ArrayList<TagSavedPurchaseBean>();

        Location location = null;

        if (! filter.getLocation ().equals ("0")) {
            location = locationService.getLocation (Integer.valueOf (filter.getLocation ()));
        }

        List<TagPurchase> purchases = tagDao.getPurchaseByFilter(location, filter.getStartDate(), filter.getEndDate(), email, code, status, targetTag);

        List<Camera> camerass = baseDao.retrieveAll(Camera.class);
        Map<String, Camera> cameraMap = getMap(camerass);

        for (TagPurchase purchase : purchases) {
            String staffString = getStaffNames(purchase.getStaffList());

            String camerasString = "";
            if (purchase.getCameras() != null) {
                String[] cameras = purchase.getCameras().split(",");
                for (String cam : cameras) {
                    if (!StringUtils.isEmpty(cam) && !cam.equals("0")) {
                        if (cameraMap.containsKey(cam)) {
                            String camStr = cameraMap.get(cam).getCameraName();
                            camerasString = camerasString.length() == 0 ? camStr : camerasString + "," + camStr;
                        }
                    }
                }
            } else {
                camerasString = "All Cameras";
            }

            Set<Tag> tags = purchase.getTags();

            String tagsString = "";
            for (Tag tag : tags) {
                Integer tagId = tag.getId();
                if (tagId != null && tagId != 0) {
                    tagsString = tagsString.equals("") ? tag.getTagName() : tagsString + ", " + tag.getTagName();
                } else {
                    if (tagId != null && tagId == 0) {
                        tagsString = tagsString.equals("") ? "No tag" : tagsString + ", " + "No Tag";

                    }
                }

                if (purchase.getGeneratedCode() != null && !purchase.getGeneratedCode().equals("")) {
                    TagSavedPurchaseBean tagSavedPurchaseBean = new TagSavedPurchaseBean(purchase.getId(), purchase.getPurchasePackage() != null ? purchase.getPurchasePackage().getPackageName() : "", purchase.getParticipantsEmails().equals("[]") ? "" : purchase.getParticipantsEmails(),
                            staffString, purchase.getSubject(), purchase.getStartDate(), purchase.getEndDate(),
                            purchase.getStartDate(), purchase.getEndDate(), purchase.getLocationPurchase() != null ? purchase.getLocationPurchase().getLocationName() : "", camerasString, purchase.getGeneratedCode(), tagsString, purchase.getFirstPictureTime(), true, "true");

                    savedPurchaseBeanList.add(tagSavedPurchaseBean);
                }
            }
        }

        return savedPurchaseBeanList;
    }

    public List<TagPurchase> getTagPurchaseByDatesLocationStatus(Location location, Date startDate, Date endDate) {
        List<TagPurchase> tagPurchases = new ArrayList<TagPurchase>();
        tagPurchases = tagDao.getTagPurchaseByDatesLocationStatus(startDate, endDate, location, TagPurchase.StatusCode.ACTUAL.getValue());

        return tagPurchases;
    }

    public List<Statistic> getTagPurchasesForStatistic(Location location, Date startDate, Date endDate) {
        List<Statistic> purchaseStatistics = new ArrayList<Statistic>();
        List<TagPurchase> tagPurchases = getTagPurchaseByDatesLocationStatus(location, startDate, endDate);
        if (tagPurchases != null && tagPurchases.size() != 0) {
            for (TagPurchase tagPurchase : tagPurchases) {
                if (tagPurchase == null) continue;
                Statistic statistic = new Statistic();
                statistic.setPackages(tagPurchase.getPurchasePackage());
                if (tagPurchase.getParticipants() != null) {
                    statistic.setNumberOfParticipantAll(tagPurchase.getParticipants());
                } else {
                    statistic.setNumberOfParticipantAll(0);
                }
                statistic.setNumberToPrintAll(0);
                statistic.setCreationDate(tagPurchase.getPurchaseDate());
                statistic.setAnyPictureName("");
                statistic.setEmails(tagPurchase.getParticipantsEmails());
                statistic.setTeasers(tagPurchase.getParticipantsOtherEmails());
                statistic.setEmails("");
                statistic.setEmailSubject(tagPurchase.getSubject());
                statistic.setCameraNames(tagPurchase.getCameras() == null ? "" : tagPurchase.getCameras());
                statistic.setTotalPictureNumber(0);

                Set<Staff> staffSet = new HashSet<Staff>();
                String staffs = tagPurchase.getStaffs();
                if (staffs != null && !staffs.equals("")) {
                    String[] staffIds = staffs.split(",");
                    if (staffIds.length != 0) {
                        for (String st : staffIds) {
                            if (st != null && !st.equals("")) {
                                try {
                                    int staffId = Integer.parseInt(st);
                                    Staff staffff = (Staff) baseDao.retrieve(Staff.class, staffId);
                                    if (staffff != null) {
                                        staffSet.add(staffff);
                                    }
                                } catch (NumberFormatException e) {
                                }
                            }
                        }

                    }
                }
                statistic.setStaff(staffSet);
                purchaseStatistics.add(statistic);
            }
        }
        return purchaseStatistics;
    }

}
