package com.scnsoft.fotaflo.webapp.util;

import javax.crypto.*;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 09.04.2012
 * Time: 5:36:05
 * To change this template use File | Settings | File Templates.
 */
public class PasswordEncryption {

    // Salt
    private static byte[] salt = {(byte) 0xc7, (byte) 0x73, (byte) 0x21,
            (byte) 0x8c, (byte) 0x7e, (byte) 0xc8, (byte) 0xee, (byte) 0x99};

    // Iteration count
    private static int count = 20;

    private static byte[] clearText = "Secret".getBytes();

    public static void main(String[] args) {
        try {
            PasswordEncryption test = new PasswordEncryption();
            /*String passwd = test.encryptPassword("ArOcGAx8uUH0Jon43nj995.VeDCVAekzqr3OOnSYx326WGUe2lM9.prG");
            System.out.println("My encrypted: " + passwd);
            passwd = test.decryptPassword(passwd);*/
           /* System.out.println("My decrypted: " + passwd);*/
            /*test.sample();*/
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private Cipher pbeCipher;
    private PBEParameterSpec pbeParamSpec;

    public PasswordEncryption() throws NoSuchAlgorithmException,
            NoSuchPaddingException {
        // Create PBE parameter set
        pbeParamSpec = new PBEParameterSpec(salt, count);

        // Create PBE Cipher
        pbeCipher = Cipher.getInstance("PBEWithMD5AndDES");
    }

    public byte[] encryptPassword(String password) throws Exception {
        PBEKeySpec pbeKeySpec;
        SecretKeyFactory keyFac;

        String pass = "Password";
        pbeKeySpec = new PBEKeySpec(pass.toCharArray());
        keyFac = SecretKeyFactory.getInstance("PBEWithMD5AndDES");
        SecretKey pbeKey = keyFac.generateSecret(pbeKeySpec);

        byte[] cipherText = encrypt(pbeKey, password.getBytes());

        return cipherText;
    }

    public String decryptPassword(byte[] password) throws Exception {
        PBEKeySpec pbeKeySpec;
        SecretKeyFactory keyFac;

        String pass = "Password";
        pbeKeySpec = new PBEKeySpec(pass.toCharArray());
        keyFac = SecretKeyFactory.getInstance("PBEWithMD5AndDES");
        SecretKey pbeKey = keyFac.generateSecret(pbeKeySpec);

        byte[] cipherText = decrypt(pbeKey, password);

        return new String(cipherText);
    }

    private byte[] encrypt(SecretKey pbeKey, byte[] data)
            throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, InvalidAlgorithmParameterException,
            IllegalBlockSizeException, BadPaddingException {
        // Initialize PBE Cipher with key and parameters
        pbeCipher.init(Cipher.ENCRYPT_MODE, pbeKey, pbeParamSpec);

        // Our cleartext

        // Encrypt the cleartext
        byte[] ciphertext = pbeCipher.doFinal(data);

        return ciphertext;
    }

    private byte[] decrypt(SecretKey pbeKey, byte[] data)
            throws InvalidKeyException, InvalidAlgorithmParameterException,
            IllegalBlockSizeException, BadPaddingException {
        // Initialize PBE Cipher with key and parameters
        pbeCipher.init(Cipher.DECRYPT_MODE, pbeKey, pbeParamSpec);

        // Our cleartext

        // Decrypt the ciphertext
        byte[] clearText = pbeCipher.doFinal(data);

        return clearText;
    }
}
