package com.scnsoft.fotaflo.webapp.util;

import com.scnsoft.fotaflo.common.util.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */

public class DateConvertationUtils {
    protected static Logger logger = Logger.getLogger(DateConvertationUtils.class);

    public static Date getPictureTime(String timeZoneId, long time, String filename) {

        Date date = getDateByPattern(filename, "(\\d+)-(\\d+)-(\\d+)[_\\s](\\d+)[-\\.](\\d+)[-\\.](\\d+)_?([\\d]*)",
                "yyyy-MM-dd_HH-mm-ss_S");

        if(date == null){
            date = getDateByPattern(filename, "(\\d+)[_\\s](\\d+)", "yyyyMMdd_HHmmss");
        }

        if(date == null){
            TimeZone timeZone = null;
            if(!StringUtils.isEmpty(timeZoneId)){
                timeZone = TimeZone.getTimeZone(timeZoneId);
            }
            if(timeZone == null){
                timeZone = TimeZone.getDefault();
                logger.info("Default timezone" + TimeZone.getDefault() + " was set");
            }

            Calendar calTZ = new GregorianCalendar(timeZone);
            logger.info("Timezone " + timeZoneId + " was set");
            calTZ.setTimeInMillis(time);
            date = calTZ.getTime();
        }

        return date;
    }

    protected static Date getDateByPattern(String source, String pattern, String format){
        Date date = null;
        if(!StringUtils.isEmpty(source) && !StringUtils.isEmpty(format)){
            SimpleDateFormat s = new SimpleDateFormat(format, Locale.US);
            int start = 0;
            if(!StringUtils.isEmpty(pattern)){
                Pattern p = Pattern.compile(pattern);
                Matcher m = p.matcher(source);
                if(m.find()){
                    start = m.start();
                }
            }

            String part = source.substring(start);
            try {
                date = s.parse(part);
            } catch (ParseException e) {
            }
        }

        return date;
    }

    public static Date getPictureTimeParsed(String timeZoneId, long time, String filename) {
        Calendar calTZ;
        if (timeZoneId != null) {
            calTZ = new GregorianCalendar(TimeZone.getTimeZone(timeZoneId));
            logger.info("Timezone " + timeZoneId + " was set");
        } else {
            calTZ = new GregorianCalendar(TimeZone.getDefault());
            logger.info("Default timezone" + TimeZone.getDefault() + " was set");
        }
        calTZ.setTimeInMillis(time);
        int year = calTZ.get(Calendar.YEAR);
        int month = calTZ.get(Calendar.MONTH);
        int day = calTZ.get(Calendar.DAY_OF_MONTH);
        int hour = calTZ.get(Calendar.HOUR_OF_DAY);
        int min = calTZ.get(Calendar.MINUTE);
        int sec = calTZ.get(Calendar.SECOND);
        int mili = calTZ.get(Calendar.MILLISECOND);
        Calendar cal = Calendar.getInstance();
        /*Pattern pattern = Pattern.compile("(\\d+)-(\\d+)-(\\d+)_(\\d+)-(\\d+)-(\\d+)_(\\d+)");*/
        Pattern pattern = Pattern.compile("(\\d+)-(\\d+)-(\\d+)[_\\s](\\d+)[-\\.](\\d+)[-\\.](\\d+)_?([\\d]*)");
        Matcher matcher = pattern.matcher(filename);
        if (matcher.find()) {
            try {
                year = Integer.valueOf(matcher.group(1));
                month = Integer.valueOf(matcher.group(2));
                day = Integer.valueOf(matcher.group(3));
                hour = Integer.valueOf(matcher.group(4));
                min = Integer.valueOf(matcher.group(5));
                sec = Integer.valueOf(matcher.group(6));
                String milis = matcher.group(7);
                mili = (milis == null || milis.length() == 0) ? 0 : Integer.valueOf(milis);
                month = month - 1;
            } catch (NumberFormatException e) {
                logger.info("[ " + filename + " ]" + "Error in parsing date is taken from recieved time");
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            logger.info("[ " + filename + " ]" + "Time was parsed and taken from fileName");
        } else {
            logger.info("[ " + filename + " ]" + "Time was not parsed date is taken from recieved time");
        }
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, min);
        cal.set(Calendar.SECOND, sec);
        cal.set(Calendar.MILLISECOND, mili);
        return cal.getTime();
    }

    public static String viewStringDateFormat(Date date) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a");

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return df.format(cal.getTime());
    }

    public static String reportStringDateFormat(Date date) {
        DateFormat df = new SimpleDateFormat("MM.dd.yyyy");

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return df.format(cal.getTime());
    }

    public static String reportStringTimeFormat(Date date) {
        DateFormat df = new SimpleDateFormat("hh:mm a");

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return df.format(cal.getTime());
    }

    public static String shortStringDate(Date date) {
        DateFormat df = new SimpleDateFormat("MM/dd/yy");

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return df.format(cal.getTime());
    }

    public static String shortStringDate1(Date date) {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return df.format(cal.getTime());
    }

    public static String shortStringDateFile(Date date) {
        DateFormat df = new SimpleDateFormat("dd_MM_yy");

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return df.format(cal.getTime());
    }

    public static String shortStringDateFileWithout(Date date) {
        DateFormat df = new SimpleDateFormat("ddMMyy");

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return df.format(cal.getTime());
    }

    public static String shortStringDateFileFolders(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy_MM-dd");

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        String folder = df.format(cal.getTime()).replace("_", File.separator);
        return folder.length() == 0 ? "default" : folder;
    }

    public static Map<Integer, List<Date>> getMonthStartDateMap(Date date) {
        Map<Integer, List<Date>> weekMap = new HashMap();
       /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Switching reports for the previous  or current month*/
        Date newDate = DateUtils.addMonths(date, -1);
        /*Date newDate = DateUtils.addMonths(date, 0);*/
        Calendar cal = Calendar.getInstance();
        cal.setTime(newDate);
        int maximumDays = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        for (int i = 1; i <= maximumDays; i++) {
            cal.set(Calendar.DAY_OF_MONTH, i);
            cal.set(Calendar.AM_PM, 0);
            cal.set(Calendar.HOUR, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);

            int weekOfMonth = cal.get(Calendar.WEEK_OF_MONTH);
            List<Date> dates = weekMap.get(weekOfMonth);
            if (dates == null) {
                dates = new ArrayList<Date>();
            }
            dates.add(cal.getTime());
            weekMap.put(weekOfMonth, dates);
        }
        return weekMap;
    }

    public static Date getEndDatePlusHours(int i, Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR, i);
        return cal.getTime();
    }

    public static Date getStartDate(int i, Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_MONTH, -i);
        cal.set(Calendar.AM_PM, 0);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date getEndDate(int i, Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_MONTH, -i);
        cal.set(Calendar.AM_PM, 0);
        cal.set(Calendar.HOUR, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }


    public static Date formDate(String day, String time, Date date) {
        if (day != null && !day.equals("")) {
            //	Sun Apr 10 2011 00:00:00 GMT+0300
            //2011-04-12T00:00:00
            try {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                date = formatter.parse(day);
                //logger.info("Received date:" + date);
            } catch (ParseException e) {
                logger.info("Default Received date:" + date);
                logger.error(e);
            }
            try {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                date = formatter.parse(day);
                //logger.info("Received date:" + date);
            } catch (ParseException e) {
                logger.info("Default Received date:" + date);
                logger.error(e);
            }
        }

        //logger.info("Received date:" + date);

        if ((time != null && !time.equals(""))) {
            Pattern pattern = Pattern.compile("(\\d+):(\\d+)\\s+(\\w+)");
            Matcher matcher = pattern.matcher(time);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            //logger.info("Received time:" + cal.getTime());
            if (matcher.find()) {
                Integer hour = Integer.valueOf(matcher.group(1));
                Integer minute = Integer.valueOf(matcher.group(2));
                Integer pm_am = ((matcher.group(3)).equals("AM")) ? Calendar.AM : Calendar.PM;

                cal.set(Calendar.HOUR, hour);
                //MEGA HACK!!!
                cal.getTime();
                cal.set(Calendar.MINUTE, minute);
                cal.set(Calendar.AM_PM, pm_am);
                //logger.info("Converted date :" + cal.getTime());
            } else {
                logger.info("Default Converted date :" + cal.getTime());
            }
            date = cal.getTime();
            //  logger.info("Converted date :" + cal.getTime());
        }
        //logger.info("Converted time :" + date);

        return date;
    }

    public static String getDateString(Date date) {
        String stringDate = "";
        if (date != null) {
            //	Sun Apr 10 2011 00:00:00 GMT+0300
            //2011-04-12T00:00:00
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            stringDate = formatter.format(date);
        }
        return stringDate;
    }
    //logger.info("Received date:" + date);

    public static String getTimeString(Date date) {
        String stringTime = "";
        if (date != null) {
            //	Sun Apr 10 2011 00:00:00 GMT+0300
            //2011-04-12T00:00:00
            SimpleDateFormat formatter = new SimpleDateFormat("hh:mm aaa");
            stringTime = formatter.format(date);
        }
        return stringTime;
    }

}