package com.scnsoft.fotaflo.webapp.dao;

import com.scnsoft.fotaflo.webapp.model.BannerSlideShow;
import com.scnsoft.fotaflo.webapp.model.Location;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 31.03.2012
 * Time: 10:06:35
 * To change this template use File | Settings | File Templates.
 */
public interface IBannerDao {

    public List<BannerSlideShow> getBannerSlideShowByLocation(Location location);
}
