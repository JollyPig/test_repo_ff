package com.scnsoft.fotaflo.webapp.service;

import com.scnsoft.fotaflo.webapp.dao.IDao;
import com.scnsoft.fotaflo.webapp.dao.ISystemUserDao;
import com.scnsoft.fotaflo.webapp.model.ContactEmail;
import com.scnsoft.fotaflo.webapp.model.ContactInfo;
import com.scnsoft.fotaflo.webapp.model.ContactSettings;
import com.scnsoft.fotaflo.webapp.model.SystemUser;
import com.scnsoft.fotaflo.webapp.util.MailSenderUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by
 * User: Paradinets
 * Date: 23.12.2011
 * Time: 18:36:56
 * To change this template use File | Settings | File Templates.
 */

@Service
@Transactional
public class ContactService {
    protected static Logger logger = Logger.getLogger(ContactService.class);

    @Autowired
    private ISystemUserDao systemUserDao;

    @Autowired
    private IDao baseDao;

    @Autowired
    private UserService userService;

    public void deleteContactsForUser(SystemUser systemUser) {
        List<ContactInfo> contactInfos = systemUserDao.getContcatInfosForUser(systemUser);
        for (ContactInfo contactInfo : contactInfos) {
            baseDao.delete(contactInfo);
        }
    }

    public void saveContact(String firstName, String lastName, String email, String question, String phone, String userLogin, Integer success) {
        SystemUser systemUser = userService.getUserByLogin(userLogin);
        ContactInfo contactInfo = new ContactInfo();
        contactInfo.setEmail(email);
        contactInfo.setFirstName(firstName);
        contactInfo.setLastName(lastName);
        contactInfo.setQuestion(question);
        contactInfo.setPhone(phone);
        contactInfo.setSystemUser(systemUser);
        contactInfo.setSuccessfullySent(success);
        baseDao.create(contactInfo);
    }

    public String getInfoEmail() {
        List<ContactEmail> contactEmailList = baseDao.retrieveAll(ContactEmail.class);
        String emails = "";
        if (contactEmailList != null && !contactEmailList.isEmpty()) {
            for (ContactEmail contactEmail : contactEmailList) {
                if (!emails.isEmpty()) {
                    emails += ",";
                }
                String em = contactEmail.getEmail();
                if (em != null && !em.isEmpty()) {
                    emails = emails + em;
                }
            }
        }
        return emails;
    }

    public List<String> getInfoEmails() {
        List<ContactEmail> contactEmailList = baseDao.retrieveAll(ContactEmail.class);
        List<String> emails = new ArrayList<String>();
        if (contactEmailList != null && !contactEmailList.isEmpty()) {
            for (ContactEmail contactEmail : contactEmailList) {
                String em = contactEmail.getEmail();
                if (em != null && !em.isEmpty()) {
                    emails.add(em);
                }
            }
        }
        return emails;
    }

    public boolean saveEmail(List<String> emails, String userLogin) {
        SystemUser systemUser = userService.getUserByLogin(userLogin);
        if (systemUser.getAccess().equals("1")) {
            List<ContactEmail> contactEmailList = baseDao.retrieveAll(ContactEmail.class);
            if (contactEmailList != null) {
                baseDao.deleteAll(contactEmailList);
            } else {
                logger.info("No admin emails exist in db to delete");
            }
            contactEmailList = baseDao.retrieveAll(ContactEmail.class);
            if (contactEmailList == null || contactEmailList.isEmpty()) {
                if (emails != null) {
                    for (String email : emails) {
                        if (email != null && !email.isEmpty()) {
                            ContactEmail contactEmailvar = new ContactEmail();
                            contactEmailvar.setEmail(email.trim());
                            baseDao.create(contactEmailvar);
                        } else {
                            logger.info("Email is empty");
                        }
                    }
                    return true;
                } else {
                    logger.info("The admin emails could not be saved because the emails is null");
                    return true;
                }
            } else {
                logger.error("The admin emails could not be cleared that's why new values could not be saved");
                return false;
            }
        }
        logger.error("User don't have permissions to perform the operation (saving  info email)");
        return false;
    }

    public boolean sendEmail(String firstName, String lastName, String email, String question, String userLogin, String phone) {
        SystemUser systemUser = userService.getUserByLogin(userLogin);
        MailSenderUtil mailSenderUtil = new MailSenderUtil();
        StringBuffer messageBody = new StringBuffer();
        messageBody.append("Login:\t").append(systemUser.getLoginName()).append("\n").
                append("First Name:\t").append(firstName).append("\n").
                append("Last Name:\t").append(lastName).append("\n").
                append("Email:\t").append(email).append("\n").
                append("Phone:\t").append(phone).append("\n").
                append("Question:\t").append(question).append("\n").append("\n\n");
        if (email != null && messageBody != null) {
            List<String> emails = getInfoEmails();
            if (emails != null && !emails.isEmpty()) {
                Map map = getEmailContactSettings();
                String mailHostServer = (String) map.get("smtpServer");
                String port = (String) map.get("smtpServerPort");
                String from = (String) map.get("fromEmail");
                String password = (String) map.get("smtpServerFromPassword");
                String subject = (String) map.get("subjectText");
                boolean isSsl = ((String) map.get("sslcheck") != null && ((String) map.get("sslcheck")).equals("on")) ? true : false;
                if (mailHostServer != null && !mailHostServer.isEmpty() && port != null && !port.isEmpty() && from != null
                        && !from.isEmpty() && subject != null && !subject.isEmpty()) {
                    return mailSenderUtil.sendContactQuestions(getInfoEmails(), messageBody.toString(), from, subject, mailHostServer, port, password, isSsl);
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            logger.error("The user email is not specified");
            return false;
        }
    }

    public void saveEmailContactSettings(String smtpServer, String smtpServerPort, String fromEmail, String smtpServerFromPassword, String subjectText, String sslcheck) {
        List contactSettings = baseDao.retrieveAll(ContactSettings.class);
        ContactSettings contactSetting = (contactSettings.size() != 0) ? (ContactSettings) contactSettings.get(0) : new ContactSettings();
        contactSetting.setSmtpServer(smtpServer.trim());
        contactSetting.setSmtpServerPort(smtpServerPort.trim());
        contactSetting.setFromEmail(fromEmail.trim());
        contactSetting.setSmtpServerFromPassword(smtpServerFromPassword.trim());
        contactSetting.setSubjectText(subjectText.trim());
        String sslString = (sslcheck != null && sslcheck.trim().equals("on")) ? "on" : "off";
        contactSetting.setSsl(sslString);
        baseDao.update(contactSetting);
    }

    public void deleteEmailContactSettings() {
        List contactSettings = baseDao.retrieveAll(ContactSettings.class);
        for (Object contactSetting : contactSettings) {
            baseDao.delete((ContactSettings) contactSetting);
        }
    }

    public Map<String, Object> getEmailContactSettings() {
        Map map = new HashMap<String, String>();
        List contactSettings = baseDao.retrieveAll(ContactSettings.class);
        ContactSettings contactSetting = (contactSettings.size() != 0) ? (ContactSettings) contactSettings.get(0) : null;
        if (contactSetting != null) {
            map.put("smtpServer", contactSetting.getSmtpServer());
            map.put("smtpServerPort", contactSetting.getSmtpServerPort());
            map.put("fromEmail", contactSetting.getFromEmail());
            map.put("smtpServerFromPassword", contactSetting.getSmtpServerFromPassword());
            map.put("subjectText", contactSetting.getSubjectText());
            map.put("sslcheck", contactSetting.getSsl() != null ? contactSetting.getSsl() : "off");
        } else {
            map.put("smtpServer", "");
            map.put("smtpServerPort", "");
            map.put("fromEmail", "");
            map.put("smtpServerFromPassword", "");
            map.put("subjectText", "");
            map.put("sslcheck", "off");
        }
        return map;
    }

}
