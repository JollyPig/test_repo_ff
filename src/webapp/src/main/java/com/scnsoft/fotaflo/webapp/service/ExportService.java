package com.scnsoft.fotaflo.webapp.service;

import com.scnsoft.fotaflo.webapp.bean.PictureBean;
import com.scnsoft.fotaflo.webapp.dao.IReportDao;
import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.model.PublicStatistic;
import com.scnsoft.fotaflo.webapp.model.ReportEmails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */
@Service
public class ExportService {

    private static final int BUFFER = 2048;

    @Autowired
    private IReportDao reportDao;

    @Autowired
    private ImageService imageService;

    public byte[] createZipArchive(List<? extends PictureBean> pictures, String fileBaseName, boolean isFullPath) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            ZipOutputStream zipArchive = new ZipOutputStream(baos);
            int pictureNumber = 0;
            for (PictureBean picture : pictures) {
                String fileName = imageService.createExportImage(picture, isFullPath);
                File file = new File(fileName);
                FileInputStream fin = new FileInputStream(file);
                BufferedInputStream origin = new BufferedInputStream(fin, BUFFER);
                StringBuffer entryName = new StringBuffer(fileBaseName);
                entryName.append(" " + picture.getCreationDate());
                entryName.append(" " + pictureNumber);
                entryName.append(".jpg");
                ZipEntry zipEntry = new ZipEntry(entryName.toString());
                zipArchive.putNextEntry(zipEntry);
                byte data[] = new byte[BUFFER];
                int count;
                while ((count = origin.read(data, 0, BUFFER)) != -1) {
                    zipArchive.write(data, 0, count);
                }
                pictureNumber++;
            }
            zipArchive.flush();
            zipArchive.close();
        } catch (Exception e) {
            e.printStackTrace();
            return baos.toByteArray();
        }
        return baos.toByteArray();
    }


    public byte[] createZipArchive(List<? extends PictureBean> pictures, String fileBaseName) {
        return createZipArchive(pictures, fileBaseName, false);
    }


    public byte[] createTextIO(List<PublicStatistic> publicStatistics) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {

            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(baos));
            for (PublicStatistic publicStatistic : publicStatistics) {
                writer.write("Location: ");
                writer.write(' ');
                writer.write(publicStatistic.getLocation() != null ? publicStatistic.getLocation().getLocationName() : "UNKNOWN");
                writer.write(' ');
                writer.write("Package: ");
                writer.write(' ');
                writer.write(publicStatistic.getPackages() != null ? publicStatistic.getPackages().getPackageName() : "UNKNOWN");
                writer.write(' ');
                writer.write("Date: ");
                writer.write(' ');
                writer.write(publicStatistic.getCreationDate() != null ? publicStatistic.getCreationDate().toString() : "UNKNOWN");
                writer.write(' ');
                writer.write("Number of pictures: ");
                writer.write(' ');
                writer.write(String.valueOf(publicStatistic.getNumberOfPicturesSent()));
                writer.write(' ');
                writer.write("Email ");
                writer.write(' ');
                writer.write(publicStatistic.getParticipantEmail() != null ? publicStatistic.getParticipantEmail() : "UNKNOWN");
                writer.write(' ');
                writer.write("User Login:  ");
                writer.write(' ');
                writer.write(publicStatistic.getUserLogin() != null ? publicStatistic.getUserLogin() : "UNKNOWN");

                writer.write('\r');
                writer.write('\n');
            }
            writer.write('\n');
            writer.close();

            return baos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return baos.toByteArray();

    }

    public byte[] createCSV(List<ReportEmails> reports) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {

            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(baos));

            writer.write("email address");
            writer.write(',');
            writer.write("first name");
            writer.write(',');
            writer.write("last name");
            writer.write(',');
            writer.write("city");
            writer.write(',');
            writer.write("state");
            writer.write(',');
            writer.write("gender");
            writer.write(',');
            writer.write("preference");
            writer.write('\n');

            writer.write('\n');
            for (ReportEmails reportEmail : reports) {
                writer.write(reportEmail.getEmail() != null ? reportEmail.getEmail() : "");
                writer.write(',');
                writer.write(reportEmail.getFirstName() != null ? reportEmail.getFirstName() : "");
                writer.write(',');
                writer.write(reportEmail.getLastName() != null ? reportEmail.getLastName() : "");
                writer.write(',');
                writer.write(reportEmail.getCity() != null ? reportEmail.getCity() : "");
                writer.write(',');
                writer.write(reportEmail.getState() != null ? reportEmail.getState() : "");
                writer.write(',');
                writer.write(reportEmail.getGender() != null ? reportEmail.getGender() : "");
                writer.write(',');
                writer.write(reportEmail.getPreference() != null ? reportEmail.getPreference() : "");
                writer.write('\n');
                writer.write('\n');
            }
            writer.close();

            return baos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return baos.toByteArray();
    }

    public List<ReportEmails> getReportEmails(Date start, Date end, Location location) {
        if (start != null && end != null && location != null) {
            return reportDao.getReportEmails(start, end, location);
        } else {
            return new ArrayList<ReportEmails>();
        }

    }
}