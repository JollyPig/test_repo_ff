package com.scnsoft.fotaflo.webapp.bean;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 06.06.14
 * Time: 13:19
 * To change this template use File | Settings | File Templates.
 */
public class TagSavedPurchaseBean extends SavedPurchaseBean {

    public TagSavedPurchaseBean(SavedPurchaseBean savedPurchaseBean) {
        this.id = savedPurchaseBean.getId();
        this.packages = savedPurchaseBean.getPackages();
        this.emails = savedPurchaseBean.getEmails();
        this.staff = savedPurchaseBean.getStaff();
        this.subject = savedPurchaseBean.getSubject();
        this.startDate = savedPurchaseBean.getStartDate();
        this.endDate = savedPurchaseBean.getEndDate();
        this.startTime = savedPurchaseBean.getStartTime();
        this.endTime = savedPurchaseBean.getEndTime();
        this.location = savedPurchaseBean.getLocation();
        this.camera = savedPurchaseBean.getCamera();
        this.code = savedPurchaseBean.getCode();
        this.tag = savedPurchaseBean.getTag();
        this.firstpicturetime = savedPurchaseBean.getFirstpicturetime();
        this.isTagPurchase = false;
        this.tagPurchase = "false";
    }

    public TagSavedPurchaseBean(int id, String packages, String emails, String staff, String subject, Date startDate, Date endDate,
                                Date startTime, Date endTime, String location, String camera, String code, String tag, String firstpicturetime, Boolean isTagPurchase, String tagPurchase) {
        this.id = id;
        this.packages = packages;
        this.emails = emails;
        this.staff = staff;
        this.subject = subject;
        this.startDate = startDate;
        this.endDate = endDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.location = location;
        this.camera = camera;
        this.code = code;
        this.tag = tag;
        this.firstpicturetime = firstpicturetime;
        this.isTagPurchase = isTagPurchase;
        this.tagPurchase = tagPurchase;
    }

    Boolean isTagPurchase;
    String tagPurchase;

    public Boolean getTagPurchase() {
        return isTagPurchase;
    }

    public void setTagPurchase(Boolean tagPurchase) {
        isTagPurchase = tagPurchase;
    }
}
