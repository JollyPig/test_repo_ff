package com.scnsoft.fotaflo.webapp.controller;

import com.scnsoft.fotaflo.webapp.bean.PictureBean;
import com.scnsoft.fotaflo.webapp.common.ResultList;
import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.model.Picture;
import com.scnsoft.fotaflo.webapp.model.SystemUser;
import com.scnsoft.fotaflo.webapp.model.UserSettings;
import com.scnsoft.fotaflo.webapp.service.PictureService;
import com.scnsoft.fotaflo.webapp.service.PictureSlideShowService;
import com.scnsoft.fotaflo.webapp.service.UserService;
import com.scnsoft.fotaflo.webapp.util.AccessID;
import com.scnsoft.fotaflo.webapp.util.Filter;
import com.scnsoft.fotaflo.webapp.util.FotafloUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */
@Controller
public class CatalogController {
    protected static Logger logger = Logger.getLogger(CatalogController.class);
    private final static String successProperty = "success";
    private PictureService pictureService;

    @Autowired
    private PictureSlideShowService pictureSlideShowService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "")
    public
    @ResponseBody
    Map<String, ? extends Object> dispatches(HttpServletRequest httpServletRequest) throws Exception {
        return new ModelMap();
    }

    @RequestMapping(value = "getPicturesForSlideShow.json")
    public
    @ResponseBody
    Map<String, ? extends Object> getPicturesForSlideShow(HttpServletRequest request) {
        Filter filter = FotafloUtil.initFilterForCatalog(request);
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();

        String pageLimit = pictureService.getPageSize(currentUser);
        int start = FotafloUtil.parseParameterToInteger(request, "start", "0");
        HashMap<String, Object> modelMap = new HashMap<String, Object>();

        UserSettings userSettings = userService.getUserSettings(currentUser);
        int pictureCount = 8;
        if (userSettings.getPictureCount() != 1) {
            pictureCount = 24;
        }

        SystemUser systemUser = userService.getUserByLogin (currentUser);

        ResultList<Picture> resultList = pictureSlideShowService.getPicturesFilteredList(currentUser, filter, start, pictureCount);
        List<Picture> pictures = resultList.getItems();
        List<PictureBean> pictureBeans = pictureSlideShowService.getPicturesFilteredPictureList(currentUser, pictures);
        modelMap.put("pictures", pictureBeans != null ? pictureBeans : new ArrayList<PictureBean>());
        modelMap.put("total", resultList.getTotal());
        modelMap.put("current", start);
        modelMap.put("pageSize", pageLimit);
        final Location location = systemUser.getLocation ();
        if (location != null && location.getSlideShowInNewWindow () != null) {
            modelMap.put ("slideShowInNewWindow", location.getSlideShowInNewWindow ());
        }
        else {
            modelMap.put ("slideShowInNewWindow", false);
        }
        FotafloUtil.saveFilter(pictureService, filter, currentUser, request);
        return modelMap;
    }

    @RequestMapping(value = "getPictures.json")
    public
    @ResponseBody
    Map<String, ? extends Object> loadPictures(HttpServletRequest request) {
        Filter filter = FotafloUtil.initFilterForCatalog(request);

        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User [ " + currentUser + " ]" + " requested pictures from " + filter.getStartDate() + " to " + filter.getEndDate());
        String pageLimit = pictureService.getPageSize(currentUser);
        int start = FotafloUtil.parseParameterToInteger(request, "start", "0");
        int limit = FotafloUtil.parseParameterToInteger(request, "limit", pageLimit);
        HashMap modelMap = new HashMap();

        long time1 = System.currentTimeMillis();
        //POSSIBLE PERFORMANCE IMPROVEMENT
        ResultList<Picture> resultList = pictureSlideShowService.getPicturesFilteredList(currentUser, filter, start, limit);
        long time2 = System.currentTimeMillis();
        logger.debug("TIME STEP 1 GET PICTURES FILTERED LIST TOTAL:  " + (time2 - time1) / 1000 + " sec " + (time2 - time1) + " milisec ");
        List<Picture> pictures = resultList.getItems();
        List<PictureBean> pictureBeans = pictureSlideShowService.getPicturesFilteredPictureList(currentUser, pictures);
        long time3 = System.currentTimeMillis();
        logger.debug("TIME STEP 2 GET PICTURES FILTERED LIST:  " + (time3 - time2) / 1000 + " sec " + (time3 - time2) + " milisec ");
        modelMap.put("pictures", pictureBeans != null ? pictureBeans : new ArrayList<PictureBean>());
        modelMap.put("total", resultList.getTotal());
        modelMap.put("pageSize", pageLimit);

        if (String.valueOf(AccessID.PUBLICEMAILUSER.getValue()).equals(userService.getUserByLogin(currentUser).getAccess()) || String.valueOf(AccessID.PUBLICPURCHASEUSER.getValue()).equals(userService.getUserByLogin(currentUser).getAccess())) {
            return modelMap;
        }
        FotafloUtil.saveFilter(pictureService, filter, currentUser, request);
        return modelMap;
    }

    @RequestMapping(value = "getPicturesUpdate.json")
    public
    @ResponseBody
    Map<String, ? extends Object> updateSelectedPictures(@RequestParam(value = "pictures", required = false) Object data, HttpServletRequest httpServletRequest) throws Exception {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        pictureService.updatePictureBeans(data, currentUser);
        return new ModelMap();
    }

    @RequestMapping(value = "getAllPicturesSelected.json")
    public
    @ResponseBody
    Map<String, ? extends Object> getAllPicturesSelected(HttpServletRequest request) throws Exception {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        Filter filter = FotafloUtil.initFilterForCatalog(request);
        HashMap modelMap = new HashMap();
        modelMap.put("pictures", pictureService.getAllPicturesSelected(currentUser, filter));
        /* modelMap.put(successProperty, true);*/
        return modelMap;
    }

    @RequestMapping(value = "getPicturesClearSelections.json"/*, method = RequestMethod.POST*/)
    public
    @ResponseBody
    Map<String, ? extends Object> clearSelections(HttpServletRequest httpServletRequest) throws Exception {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        pictureService.clearAllSelections(currentUser);
        logger.info("User [ " + currentUser + " ]" + " cleared all selections");
        return new ModelMap();
    }

    @RequestMapping(value = "getPicturesDeleted.json")
    public
    @ResponseBody
    Map<String, ? extends Object> getPicturesDeleted(HttpServletRequest httpServletRequest) throws Exception {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        if (httpServletRequest.getParameter("data") != null) {
            String[] arr = ((String[]) httpServletRequest.getParameterMap().get("data"));
            Boolean singledeletion = Boolean.valueOf(httpServletRequest.getParameter("singledeletion"));
            if (arr.length != 0) {
                try {
                    pictureService.deletePictures(arr, currentUser, singledeletion);
                    logger.info("User [ " + currentUser + " ]" + "  successfully deleted the following pictures");
                } catch (Exception en) {
                    logger.error("User [ " + currentUser + " ]" + " was not able to delete pictures " + arr.length + " pictures");
                    logger.error(en.getMessage(), en);
                }
            }
        }
        return new ModelMap();
    }

    @RequestMapping(value = "getPicturesRotate.json")
    public
    @ResponseBody
    Map<String, ? extends Object> getPicturesRotate(HttpServletRequest httpServletRequest) throws Exception {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        if (httpServletRequest.getParameter("data") != null) {
            String pictId = String.valueOf(httpServletRequest.getParameter("data"));
            String direction = String.valueOf(httpServletRequest.getParameter("direction"));
            if (pictId != null) {
                try {
                    pictureService.rotatePictures(pictId, currentUser, direction);
                    logger.info("User [ " + currentUser + " ]" + "  successfully rotated the following picture " + direction + " " + pictId);
                } catch (Exception en) {
                    logger.error("User [ " + currentUser + " ]" + " was not able to rotate picture " + pictId + " direction " + direction);
                    logger.error(en.getMessage(), en);
                }
            }
        }
        return new ModelMap();
    }

    @RequestMapping(value = "getPicturesSelectedDiselected.json"/*, method = RequestMethod.POST*/)
    public
    @ResponseBody
    Map<String, ? extends Object> getPicturesSelectedDiselected(HttpServletRequest httpServletRequest) throws Exception {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        if (httpServletRequest.getParameter("data") != null) {
            String[] arr = ((String[]) httpServletRequest.getParameterMap().get("data"));

            if (arr.length != 0) {
                try {

                    pictureService.selectDeselectPictures(arr, currentUser);
                } catch (Exception en) {
                    logger.error(en.getMessage(), en);
                }
            }
        }
        return new ModelMap();
    }

    @RequestMapping(value = "selectAllSlideshow.json")
    public
    @ResponseBody
    Map<String, ? extends Object> getPicturesIds(HttpServletRequest request) {
        Filter filter = FotafloUtil.initFilterForCatalog(request);
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        HashMap modelMap = new HashMap();
        modelMap.put("ids", pictureService.getPicturesFilteredIds(currentUser, filter));
        modelMap.put(successProperty, true);
        return modelMap;
    }

    @RequestMapping(value = "getPicturesPageSizeUpdate.json")
    public
    @ResponseBody
    Map<String, ? extends Object> updatePageSize(HttpServletRequest httpServletRequest) throws Exception {

        String pageSize = httpServletRequest.getParameter("pageSize") != null ? httpServletRequest.getParameter("pageSize") : "10";
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        pictureService.updatePageSize(currentUser, pageSize);
        return new ModelMap();
    }

    @Autowired
    public void setPictureService(PictureService pictureService) {
        this.pictureService = pictureService;
    }

}
