package com.scnsoft.fotaflo.webapp.service;

import com.scnsoft.fotaflo.common.social.facebook.FacebookTemplate;
import org.apache.log4j.Logger;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShareService {
    private final static Logger logger = Logger.getLogger(ShareService.class);

    public boolean shareImagesViaFacebook(String accessToken, List<Resource> images, String message, String userId){
        FacebookTemplate facebook = new FacebookTemplate(accessToken);

        logger.info("facebook sharing");
        if(facebook != null && facebook.isAuthorized()){
            try{
                facebook.shareImagesToPage(userId, images, message);
            }catch (Exception e){
                logger.error(e.getMessage(), e);
            }
        }

        logger.info("DONE");

        return true;
    }

}
