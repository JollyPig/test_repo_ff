package com.scnsoft.fotaflo.webapp.util;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.io.File;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Nadezda Drozdova
 * Date: Jun 22, 2011
 * Time: 2:05:56 PM
 */
public class ClearingJob extends QuartzJobBean {


    protected static org.apache.log4j.Logger logger = Logger.getLogger("MailSender");

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        Date currentDate = jobExecutionContext.getFireTime();
        long fromDate = getFromDate(1, currentDate);
        logger.info("Deleting objects from date: " + new Date(fromDate));
        String strTempDir = System.getProperty("java.io.tmpdir");
        File tempDir = new File(strTempDir);
        if (tempDir.exists() && tempDir.isDirectory()) {
            logger.info("Get files from directory: " + strTempDir);
            File[] files = tempDir.listFiles(new ImageFileFilter());
            if (files != null) {
                for (File file : files) {
                    if (file.exists() && file.isFile()) {
                        long fileDate = file.lastModified();
                        if (fileDate < fromDate) {
                            logger.info("Deleting file: " + file);
                            file.delete();
                        }
                    }
                }
            }
        }


    }

    public long getFromDate(int i, Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_MONTH, -i);
        cal.set(Calendar.AM_PM, 0);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime().getTime();
    }
}
