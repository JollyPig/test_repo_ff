package com.scnsoft.fotaflo.webapp.job;

import com.scnsoft.fotaflo.webapp.model.PublicStatistic;
import com.scnsoft.fotaflo.webapp.service.LocationService;
import com.scnsoft.fotaflo.webapp.service.PictureService;
import com.scnsoft.fotaflo.webapp.service.ReportService;
import com.scnsoft.fotaflo.webapp.service.StatisticService;
import com.scnsoft.fotaflo.webapp.util.DateConvertationUtils;
import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 07.02.2012
 * Time: 14:42:04
 * To change this template use File | Settings | File Templates.
 */

public class WeeklyPublicReportJob extends QuartzJobBean {

    private ReportService reportService;

    private PictureService pictureService;

    private LocationService locationService;

    private StatisticService statisticService;

    protected static Logger logger = Logger.getLogger("WeeklyPublicReportJob");

    protected void executeInternal(JobExecutionContext ctx) throws JobExecutionException {

        HashMap<Integer, List<PublicStatistic>> mapGeneral = statisticService.getPublicStatistic(ctx.getFireTime());

        Iterator itLocations = mapGeneral.keySet().iterator();

        while (itLocations.hasNext()) {
            Integer key = (Integer) itLocations.next();
            String keyString = locationService.getLocation(key).getLocationName();
            List<PublicStatistic> reportGeneralList = mapGeneral.get(key);

            String path = pictureService.getPath(File.separator + "reports" + File.separator + "statistic_" + "publicReports" + File.separator + keyString + "_" + DateConvertationUtils.shortStringDateFile(ctx.getFireTime()) + ".xls");
            String reportPath = reportService.getReportPath();

            if (new File(reportPath).isDirectory()) {
                String pathDir = reportPath + (reportPath.endsWith(File.separator) ? "" : File.separator) + "publicReports" + File.separator + keyString + File.separator;
                if (!new File(pathDir).isDirectory()) {
                    new File(pathDir).mkdirs();
                }
                if (new File(pathDir).isDirectory()) {
                    path = pathDir + "publicstatistic_" + keyString + "_" + DateConvertationUtils.shortStringDateFile(ctx.getFireTime()) + ".txt";
                }
            }
            File file = new File(path);

            PrintWriter pw = null;

            try {
                pw = new PrintWriter(new FileWriter(file));

                for (PublicStatistic publicStatistic : reportGeneralList) {
                    StringBuffer stringbuffer = new StringBuffer();
                    stringbuffer.append("Location: ");
                    stringbuffer.append(' ');
                    stringbuffer.append(publicStatistic.getLocation() != null ? publicStatistic.getLocation().getLocationName() : "UNKNOWN").append(' ');
                    stringbuffer.append(' ');
                    stringbuffer.append("Package: ");
                    stringbuffer.append(' ');
                    stringbuffer.append(publicStatistic.getPackages() != null ? publicStatistic.getPackages().getPackageName() : "UNKNOWN").append(' ');
                    stringbuffer.append(' ');
                    stringbuffer.append("Date: ");
                    stringbuffer.append(' ');
                    stringbuffer.append(publicStatistic.getCreationDate() != null ? publicStatistic.getCreationDate().toString() : "UNKNOWN").append(' ');
                    stringbuffer.append(' ');
                    stringbuffer.append("Number of pictures: ");
                    stringbuffer.append(' ');
                    stringbuffer.append(publicStatistic.getNumberOfPicturesSent()).append(' ');
                    stringbuffer.append(' ');
                    stringbuffer.append("Email ");
                    stringbuffer.append(' ');
                    stringbuffer.append(publicStatistic.getParticipantEmail() != null ? publicStatistic.getParticipantEmail() : "UNKNOWN").append(' ');
                    stringbuffer.append(' ');
                    stringbuffer.append("User Login:  ");
                    stringbuffer.append(' ');
                    stringbuffer.append(publicStatistic.getUserLogin() != null ? publicStatistic.getUserLogin() : "UNKNOWN");


                    String line = stringbuffer.toString();
                    pw.println(line);
                }
                pw.flush();
                logger.info("Public Report was created: " + path);

            } catch (IOException e) {
                e.printStackTrace();
            } finally {

                //Close the PrintWriter
                if (pw != null)
                    pw.close();

            }

        }
    }

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }

    public void setPictureService(PictureService pictureService) {
        this.pictureService = pictureService;
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }

    public StatisticService getStatisticService() {
        return statisticService;
    }

    public void setStatisticService(StatisticService statisticService) {
        this.statisticService = statisticService;
    }
}