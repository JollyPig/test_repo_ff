package com.scnsoft.fotaflo.webapp.reportmodule.model;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 20.08.2012
 * Time: 9:25:53
 * To change this template use File | Settings | File Templates.
 */
public class PictureObj {


    String fileName;
    String locationName;
    String date;
    String cameraId;
    int totalTimeUpload;
    String lastUploadTime;
    boolean isSuccessfull;
    String errorType;
    int beforeNumber;
    String timeZone;
    String receivedFromPhoneTime;
    String firstUploadTime;
    boolean alreadyExistWarn;
    int counter = 0;
    int rejected = 0;

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCameraId() {
        return cameraId;
    }

    public void setCameraId(String cameraId) {
        this.cameraId = cameraId;
    }

    public int getTotalTimeUpload() {
        return totalTimeUpload;
    }

    public void setTotalTimeUpload(int totalTimeUpload) {
        this.totalTimeUpload = totalTimeUpload;
    }

    public boolean isSuccessfull() {
        return isSuccessfull;
    }

    public void setSuccessfull(boolean successfull) {
        isSuccessfull = successfull;
    }

    public String getErrorType() {
        return errorType;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }

    public int getBeforeNumber() {
        return beforeNumber;
    }

    public void setBeforeNumber(int beforeNumber) {
        this.beforeNumber = beforeNumber;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getReceivedFromPhoneTime() {
        return receivedFromPhoneTime;
    }

    public void setReceivedFromPhoneTime(String receivedFromPhoneTime) {
        this.receivedFromPhoneTime = receivedFromPhoneTime;
    }

    public String getFirstUploadTime() {
        return firstUploadTime;
    }

    public void setFirstUploadTime(String firstUploadTime) {
        this.firstUploadTime = firstUploadTime;
    }

    public boolean getAlreadyExistWarn() {
        return alreadyExistWarn;
    }

    public void setAlreadyExistWarn(boolean alreadyExistWarn) {
        this.alreadyExistWarn = alreadyExistWarn;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public String getLastUploadTime() {
        return lastUploadTime;
    }

    public void setLastUploadTime(String lastUploadTime) {
        this.lastUploadTime = lastUploadTime;
    }

    public int getRejected() {
        return rejected;
    }

    public void setRejected(int rejected) {
        this.rejected = rejected;
    }
}
