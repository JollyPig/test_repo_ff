package com.scnsoft.fotaflo.webapp.controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.scnsoft.fotaflo.webapp.bean.LocationBean;
import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.model.LocationImageMetadata;
import com.scnsoft.fotaflo.webapp.service.LocationService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created MilkevichP
 * Date: 22.04.11
 * Time: 12:23
 */
@Controller()
@RequestMapping("/locations")
public class LocationsController {

    protected static Logger logger = Logger.getLogger(LocationsController.class);

    private final static String rootProperty = "locations";

    private final static String successProperty = "success";

    private LocationService locationService;

    @RequestMapping(value = "getLocations.json"/*, method = RequestMethod.GET*/)
    public
    @ResponseBody
    Map<String, ? extends Object> getLocations() {

        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        HashMap<String, Object> modelMap = new HashMap<String, Object>();
        List<LocationBean> locations = locationService.getUserLocations(currentUser);
        modelMap.put(rootProperty, locations);
        modelMap.put(successProperty, true);
        return modelMap;
    }

    @RequestMapping(value = "getActualLocations.json")
    public
    @ResponseBody
    Map<String, ? extends Object> getActualLocations(HttpServletRequest httpServletRequest) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        HashMap<String, Object> modelMap = new HashMap<String, Object>();
        modelMap.put(rootProperty, locationService.getActualLocations(currentUser));
        modelMap.put(successProperty, true);
        return modelMap;
    }

    @RequestMapping(value = "getRealLocations.json")
    public
    @ResponseBody
    Map<String, ? extends Object> getRealLocations(HttpServletRequest httpServletRequest) {

        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        HashMap<String, Object> modelMap = new HashMap<String, Object>();
        int start =
                Integer.valueOf(httpServletRequest.getParameter("start") != null ? httpServletRequest.getParameter("start") : "0");
        int limit =
                Integer.valueOf(httpServletRequest.getParameter("limit") != null ? httpServletRequest.getParameter("limit") : "0");
        modelMap.put(rootProperty, locationService.getUserRealLocations(currentUser, start, limit));
        modelMap.put("total", locationService.getUserRealLocationsCount(currentUser));
        modelMap.put(successProperty, true);
        return modelMap;
    }

    @RequestMapping(value = "saveLocations.json")
    public
    @ResponseBody
    Map<String, ? extends Object> saveLocations(@RequestParam(value = LocationsController.rootProperty, required = false) String json) {

        HashMap<String, Object> modelMap = new HashMap<String, Object>();
        Gson g = new Gson();
        List ls;

        try {
            Type listType = new TypeToken<List<Location>>() {
            }.getType();
            ls = g.fromJson(json, listType);
        } catch (Exception e) {
            ls = new ArrayList();
            ls.add(g.fromJson(json, Location.class));
        }
        locationService.saveLocations(ls);
        modelMap.put(rootProperty, ls.size() == 1 ? ls.get(0) : ls);
        modelMap.put(successProperty, true);
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User [ " + currentUser + " ]" + " saved locations");
        return modelMap;
    }

    @RequestMapping(value = "deleteLocations.json")
    public
    @ResponseBody
    Map<String, ? extends Object> deleteLocations(@RequestParam(value = LocationsController.rootProperty, required = false) Integer[] ids) {

        locationService.deleteLocations(ids);

        HashMap<String, Boolean> modelMap = new HashMap<String, Boolean>();
        modelMap.put(successProperty, true);
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User [ " + currentUser + " ]" + " deleted locations");
        return modelMap;
    }

    @RequestMapping(value = "updateImageMetadata.json", method = RequestMethod.POST)
    @ResponseBody
    public ModelMap updateImageMetadata(@RequestParam("locationId") Integer locationId, @RequestParam("title") String title,
                                        @RequestParam("comment") String comment, @RequestParam("copyright") String copyright) {
        Location location = locationService.updateImageMetadata(locationId, title, comment, copyright);
        ModelMap result = new ModelMap();
        result.put("success", true);
        result.put("msg", "Image metadata for location: " + location.getLocationName() + " was updated");
        return result;
    }

    @RequestMapping(value = "getImageMetadata.json")
    @ResponseBody
    public ModelMap getImageMetadata(@RequestParam("locationId") Integer locationId) {
        Location location = locationService.getLocation(locationId);
        LocationImageMetadata metadata = location.getImageMetadata() == null ? new LocationImageMetadata() : location.getImageMetadata();
        ModelMap result = new ModelMap();
        result.put("success", true);
        result.put("title", metadata.getTitleMetadata());
        result.put("comment", metadata.getCommentMetadata());
        result.put("copyright", metadata.getCopyrightMetadata());
        return result;
    }


    @Autowired
    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }

}
