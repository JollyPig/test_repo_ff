package com.scnsoft.fotaflo.webapp.dao;

import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.model.Requests;
import com.scnsoft.fotaflo.webapp.model.RequestsSelected;
import com.scnsoft.fotaflo.webapp.model.SystemUser;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 31.03.2012
 * Time: 10:27:32
 * To change this template use File | Settings | File Templates.
 */
public interface IRequestDao {

    public List<Requests> getRequestsByLocation(Location location);

    public List<Requests> getRequestsByUser(SystemUser user);

}
