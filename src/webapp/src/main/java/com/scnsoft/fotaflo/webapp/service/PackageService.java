package com.scnsoft.fotaflo.webapp.service;

import com.scnsoft.fotaflo.webapp.bean.PackageBean;
import com.scnsoft.fotaflo.webapp.dao.IPackageDao;
import com.scnsoft.fotaflo.webapp.dao.impl.BaseDao;
import com.scnsoft.fotaflo.webapp.model.*;
import com.scnsoft.fotaflo.webapp.model.Package;
import com.scnsoft.fotaflo.webapp.util.AccessID;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nadezda Drozdova
 * Date Apr 11, 2011
 */

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class PackageService {
    protected static Logger logger = Logger.getLogger(PackageService.class);

    @Autowired
    private IPackageDao packageDao;

    @Autowired
    private UserService userService;

    @Autowired
    private BaseDao baseDao;

    public List<PackageBean> getPackagesByLocation(String userlogin, int locationId, int start, int limit) {
        List<PackageBean> list = new ArrayList<PackageBean>();
        SystemUser systemUser = userService.getUserByLogin(userlogin);

        if (!systemUser.getAccess().equals("1")) {
            Location location = systemUser.getLocation();
            List<Package> packages = packageDao.getNotPublicPackagesByLocation(location);
            int count = packages.size();
            if (start == 0 && limit == 0)
                limit = count;
            for (int i = start; (i < start + limit) && i < count; i++) {
                Package pack = packages.get(i);
                list.add(new PackageBean(pack.getId(), pack.getPackageName()));
            }
        } else {
            list = getPackagesForAdminByLocation(locationId, start, limit);
        }
        return list;
    }

    public void deletePublicPackagesForLocation(Location location) {
        List<Package> packages = new ArrayList<Package>();
        for(Package.PackageAccess access: Package.PackageAccess.values()){
            if(!Package.PackageAccess.DEFAULT.equals(access)){
                packages.addAll(packageDao.getPublicPackagesByLocation(location, access.getId()));
            }
        }
        Integer[] packIds = new Integer[packages.size()];
        int i = 0;
        for (Package pack : packages) {
            packIds[i++] = pack.getId();
            logger.info("Package " + pack.getPackageName() + " is deleted");
        }
        deletePackages(packIds);
    }

    public Integer createNoteBookPackageForLocationIfNotExist(Location location) {
        return getSystemPackage(location, Package.PackageAccess.PHOTOBOOK);
    }

    public Integer getSinglePhotoPackage(Location location) {
        return getSystemPackage(location, Package.PackageAccess.SINGLE_PHOTO);
    }

    public Integer getGroupPhotoPackage(Location location) {
        return getSystemPackage(location, Package.PackageAccess.GROUP_PHOTO);
    }

    protected Integer getSystemPackage(Location location, Package.PackageAccess access){
        if(location == null || access == null){
            return null;
        }
        Integer id = null;
        List<Package> packages = packageDao.getPublicPackagesByLocation(location, access.getId());
        if (packages == null || packages.isEmpty()) {
            Package packAPackage = new Package(location, access);
            id = baseDao.create(packAPackage);
        }else if (packages != null && packages.size() == 1) {
            Package pack = packages.get(0);
            id = pack.getId();
        }

        return id;
    }

    public List<PackageBean> getPublicPackagesForUser(String userlogin, int start, int limit) {
        List<PackageBean> list = new ArrayList<PackageBean>();
        SystemUser systemUser = userService.getUserByLogin(userlogin);
        List<Package> packages = new ArrayList<Package>();
        if (systemUser.getAccess().equals(String.valueOf(AccessID.PUBLICUSER.getValue()))) {
            Location location = systemUser.getLocation();
            packages = packageDao.getPublicPackagesByLocation(location, Package.PackageAccess.PUBLIC_DAILY.getId());
            if (packages == null || packages.isEmpty()) {
                Package packAPackage = new Package(location, Package.PackageAccess.PUBLIC_DAILY);
                baseDao.create(packAPackage);
                packages.add(packAPackage);
            }
        }
        if (systemUser.getAccess().equals(String.valueOf(AccessID.PUBLICEMAILUSER.getValue()))) {
            Location location = systemUser.getLocation();
            packages = packageDao.getPublicPackagesByLocation(location, Package.PackageAccess.PUBLIC_INDIVIDUAL.getId());
            if (packages == null || packages.isEmpty()) {
                Package packAPackage = new Package(location, Package.PackageAccess.PUBLIC_INDIVIDUAL);
                baseDao.create(packAPackage);
                packages.add(packAPackage);
            }
        }
        if (systemUser.getAccess().equals(String.valueOf(AccessID.PUBLICTAGUSER.getValue()))) {
            Location location = systemUser.getLocation();
            packages = packageDao.getPublicPackagesByLocation(location, Package.PackageAccess.PUBLIC_TAG.getId());
            if (packages == null || packages.isEmpty()) {
                Package packAPackage = new Package(location, Package.PackageAccess.PUBLIC_TAG);
                baseDao.create(packAPackage);
                packages.add(packAPackage);
            }
        }

        int count = packages.size();
        if (start == 0 && limit == 0)
            limit = count;
        for (int i = start; (i < start + limit) && i < count; i++) {
            Package pack = packages.get(i);
            list.add(new PackageBean(pack.getId(), pack.getPackageName()));
        }
        return list;
    }

    public List<PackageBean> getPackagesForAdminByLocation(int locationId, int start, int limit) {
        List<PackageBean> list = new ArrayList();
        //not all locations were selected
        if (locationId != 0) {
            Location location = (Location) baseDao.retrieve(Location.class, locationId);
            List<Package> packageList = packageDao.getNotPublicPackagesByLocation(location);
            int count = packageList.size();
            if (start == 0 && limit == 0)
                limit = count;
            for (int i = start; (i < start + limit) && i < count; i++) {
                Package pack = packageList.get(i);
                list.add(new PackageBean(pack.getId(), pack.getPackageName()));
            }
        } else {
            List<Package> packageList = baseDao.retrieveAll(Package.class);
            int count = packageList.size();
            if (start == 0 && limit == 0)
                limit = count;
            for (int i = start; (i < start + limit) && i < count; i++) {
                Package pack = packageList.get(i);
                list.add(new PackageBean(pack.getId(), pack.getPackageName()));
            }
        }
        return list;
    }

    public void deletePackages(Integer[] ids) {
        for (Integer id : ids) {
            List<Statistic> statisticList = packageDao.getStatisticForPackage(id);
            for (Statistic statistic : statisticList) {
                if (statistic != null && statistic.getPackages().getId() == id) {
                    baseDao.delete(statistic);
                }
            }
            List<PublicStatistic> publicstatisticList = packageDao.getPublicStatisticForPackage(id);
            for (PublicStatistic statistic : publicstatisticList) {
                if (statistic != null && statistic.getPackages().getId() == id) {
                    baseDao.delete(statistic);
                }
            }
            baseDao.delete(Package.class, id);
        }
    }

    public List<Package> savePackages(List<Package> packages) {
        for (Package pkg : packages) {
            baseDao.update(pkg);
        }
        return packages;
    }
}
