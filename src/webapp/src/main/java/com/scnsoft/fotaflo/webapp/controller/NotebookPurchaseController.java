package com.scnsoft.fotaflo.webapp.controller;

import com.scnsoft.fotaflo.webapp.bean.PictureToFTPBean;
import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.model.Picture;
import com.scnsoft.fotaflo.webapp.model.SystemUser;
import com.scnsoft.fotaflo.webapp.service.*;
import com.scnsoft.fotaflo.webapp.util.FTPUploadAndSendMessageTask;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 01.08.14
 * Time: 12:10
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class NotebookPurchaseController {
    protected static Logger logger = Logger.getLogger(NotebookPurchaseController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private FTPUploadAndSendMessageTask ftpUploadAndSendMessageTask;

    @Autowired
    private PictureService pictureService;

    @Autowired
    private StatisticService statisticService;

    @Autowired
    private PackageService packageService;

    @Autowired
    private PurchaseService purchaseService;

    @Value("${fotaflo.ftp.server}")
    private String ftpServer;

    @Value("${fotaflo.ftp.login}")
    private String ftplogin;

    @Value("${fotaflo.ftp.password}")
    private String ftpPassword;

    @RequestMapping(value = "/main/photobook")
    public String getPhotoBookPage(ModelMap model, HttpServletRequest request) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User " + currentUser + " opened a photobook purchase page");

        int access = pictureService.getUserAccess(currentUser);
        SystemUser systemUser = userService.getUserByLogin(currentUser);

        model.put("pageSize", pictureService.getPageSize(currentUser));
        model.put("current", currentUser);
        model.put("userId", access);
        model.put("location", systemUser.getLocation().getId());

        return "photobookpage";
    }

    @RequestMapping(value = "submitNotebookPurchaseFrom.json", method = RequestMethod.POST)
    public
    @ResponseBody
    Map<String, ? extends Object> submitForm(@RequestParam("export") String idPicture,
                                             @RequestParam("photobook") String idPhotobookPicture,
                                             @RequestParam("staff") String idStaff,
                                             @RequestParam("customerName") String customerName,
                                             @RequestParam("email") String[] emails,
                                             HttpServletRequest httpServletRequest) {
        String error = "";
        HashMap<String, Object> modelMap = new HashMap<String, Object>();
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("User " + currentUser + " submitting the  the notebook form ");

        SystemUser currSystemUser = userService.getUserByLogin(currentUser);
        Location location = currSystemUser.getLocation();
        String email_body = "";
        //getEdge email
        String edgeEmail = currSystemUser.getLocation() != null ? currSystemUser.getLocation().getLocationToPhotobookEmail() : "";
        if (edgeEmail == null || edgeEmail.equals("")) {
            error = "Please specify the notobook email for the current location.";
            modelMap.put("error", error);
            return modelMap;
        }
        if (idPicture == null) {
            error = "Please select the picture";
            modelMap.put("error", error);
            return modelMap;
        }
        if (idPhotobookPicture == null) {
            error = "Please select the pictures to ftp server";
            modelMap.put("error", error);
            return modelMap;
        }
        String email_subject = currSystemUser.getLocation().getLocationName() + "_" + "Photo book for " + customerName;


        if (emails.length != 0) {
            for (String email : emails) {
                if (email != null && !email.equals("")) {
                    email_body = email_body.length() == 0 ? email : email_body + ", " + email;
                }
            }
        }
        String firstPictureName = "";
        if (idPhotobookPicture != null) {
            Integer pictureIdentifier = Integer.parseInt(idPhotobookPicture);
            Picture picture = pictureService.getPictureById(pictureIdentifier);
            if (picture != null) {
                String url = picture.getUrl();
                firstPictureName = url;
                Map<String, File> attachment = new HashMap<String, File>();
                String imagePath = pictureService.getPath(
                        File.separator + picture.getUrl().replace("/", File.separator));
                File file = new File(imagePath);
                attachment.put(picture.getName(), file);
                ftpUploadAndSendMessageTask.setFromEmail(location.getLocationFromEmail());
                ftpUploadAndSendMessageTask.setPassword(location.getLocationSmtpServerPassword());
                ftpUploadAndSendMessageTask.setPort(location.getLocationSmtpServerPort().toString());
                ftpUploadAndSendMessageTask.setSmtpServer(location.getLocationSmtpServer());
                ftpUploadAndSendMessageTask.setSsl(location.getLocationSslCheck());
                ftpUploadAndSendMessageTask.setSubject(email_subject);
                ftpUploadAndSendMessageTask.setAttachment(attachment);
                ftpUploadAndSendMessageTask.setEmail_body(email_body);
                ftpUploadAndSendMessageTask.setToSendEmails(Arrays.asList(edgeEmail));
                ftpUploadAndSendMessageTask.sendMessages();
            }

        }

        List<String> picturePaths = new ArrayList<String>();
        List<String> pictureNames = new ArrayList<String>();
        String locationName = "DefaultLocation";

        List<Integer> pictureIdsTpFTP = purchaseService.getIdStaffsFromString(idPicture);
        if (pictureIdsTpFTP != null && pictureIdsTpFTP.size() != 0) {
            for (Integer pictureIdTpFTP : pictureIdsTpFTP) {
                Picture picture = pictureService.getPictureById(pictureIdTpFTP);
                if (picture != null) {
                    picturePaths.add(pictureService.getPath(
                            File.separator + picture.getUrl().replace("/", File.separator)));
                    pictureNames.add(picture.getName());
                    locationName = currSystemUser.getLocation().getLocationName();
                }
            }
        }

        PictureToFTPBean pictureToFTPBean = new PictureToFTPBean(ftpServer, ftplogin, ftpPassword, locationName, picturePaths, pictureNames);

        ftpUploadAndSendMessageTask.setPicturesToFTP(pictureToFTPBean);
        ftpUploadAndSendMessageTask.sendToFTP();


        Integer notebookPackageId = packageService.createNoteBookPackageForLocationIfNotExist(location);
        List<Integer> staffsIds = purchaseService.getIdStaffsFromString(idStaff);

        statisticService.insertStatisticItems(notebookPackageId, staffsIds, 0, new Date(), 0, email_body,
                "", "", picturePaths.size(), firstPictureName, customerName, new ArrayList<String>());

        modelMap.put("error", error);
        return modelMap;
    }


}
