package com.scnsoft.fotaflo.webapp.job;


import com.scnsoft.fotaflo.webapp.service.ReportService;
import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 26.04.2012
 * Time: 9:01:11
 * To change this template use File | Settings | File Templates.
 */
public class DailyReportJob extends QuartzJobBean {

    private ReportService reportService;

    protected static Logger logger = Logger.getLogger(DailyReportJob.class);

    protected void executeInternal(JobExecutionContext ctx) throws JobExecutionException {

        java.util.Date jobTime = ctx.getFireTime();
        boolean result = reportService.generateAndSendDailyStatisticV2(jobTime);

        if (!result) {
            logger.error("The error occurred during DAILY REPORT generation or sending ");
        }
    }

   /* protected void executeInternal(JobExecutionContext ctx) throws JobExecutionException {

        java.util.Date jobTime = ctx.getFireTime();
        boolean result = reportService.generateAndSendDailyStatisticV2(jobTime);

        *//*boolean result1 = reportService.generateAndSendWeeklyStatisticV2(jobTime);
        boolean result2 = reportService.generateAndSendMonthlyStatisticV2(jobTime,ReportService.ReportNumber.FIRST);*//*

        if (!result1) {
            logger.error("The error occurred during WEEKLY REPORT generation or sending ");
        }
        if (!result) {
            logger.error("The error occurred during DAILY REPORT generation or sending ");
        }
        if (!result2) {
            logger.error("The error occurred during MONTHLY REPORT generation or sending ");
        }
    }*/

   /* protected void executeInternal(JobExecutionContext ctx) throws JobExecutionException {
        java.util.Date jobTime = DateConvertationUtils.getStartDate(1, ctx.getFireTime());
        boolean result = reportService.generateAndSendDailyStatistic(jobTime);
        if (!result) {
            logger.error("The error occurred during DAILY REPORT generation or sending ");
        }
        boolean result2 = reportService.generateAndSendDailyStatistic2(jobTime);
        if (!result2) {
            logger.error("The error occurred during DAILY REPORT 2  generation or sending ");
        }
    }*/

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }
}
