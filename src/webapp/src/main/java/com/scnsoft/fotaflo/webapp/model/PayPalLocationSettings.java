package com.scnsoft.fotaflo.webapp.model;

import com.scnsoft.fotaflo.common.model.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 14.02.2012
 * Time: 11:30:42
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "paypallocationsettings")
public class PayPalLocationSettings extends AbstractEntity {

    String account;
    String password;
    String signature;
    String environment;
    String currency;
    String price;
    String packagePrice;

    @Column(name = "account")
    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "signature")
    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    @Column(name = "environment")
    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    @Column(name = "currency")
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Column(name = "price")
    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Column (name = "package_price", nullable = true)
    public String getPackagePrice () {
        return packagePrice;
    }

    public void setPackagePrice (String packagePrice) {
        this.packagePrice = packagePrice;
    }

}
