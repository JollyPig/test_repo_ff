package com.scnsoft.fotaflo.webapp.bean;

import com.scnsoft.fotaflo.webapp.model.Tag;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 09.10.12
 * Time: 10:48
 * To change this template use File | Settings | File Templates.
 */
public class TagBean {

    protected int id;
    String tagIdName;
    String location;
    Date creationDate;

    public TagBean() {
    }

    public TagBean(Tag tag) {
        if(tag != null){
            this.id = tag.getId();
            this.tagIdName = tag.getTagName();
            this.location = tag.getLocation() != null ? tag.getLocation().getLocationName() : null;
            this.creationDate = tag.getCreationDate();
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTagIdName() {
        return tagIdName;
    }

    public void setTagIdName(String tagIdName) {
        this.tagIdName = tagIdName;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
