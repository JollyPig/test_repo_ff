package com.scnsoft.fotaflo.webapp.controller;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 25.06.14
 * Time: 16:09
 * To change this template use File | Settings | File Templates.
 */

import com.scnsoft.fotaflo.common.util.StringUtils;
import com.scnsoft.fotaflo.webapp.service.TagService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/uploadtag")
public class UploadTagController {
    protected static Logger logger = Logger.getLogger(UploadTagController.class);

    static {
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            public void uncaughtException(Thread t, Throwable e) {
                logger.error("thread = " + t.getName() + " e.getMessage = " + e.getMessage());
                e.printStackTrace();
            }
        });
    }

    @Autowired
    private TagService tagService;

    @RequestMapping(value = "{name}", method = RequestMethod.GET)
    public
    @ResponseBody
    boolean uploadTag(@PathVariable String name,
                      @RequestParam(value = "tag", required = false) String tagName,
                      @RequestParam(value = "taglast", required = false) String tagLast,
                      @RequestParam(value = "tagfirst", required = false) String tagFirst){
        if (StringUtils.isEmpty(name)) {
            logger.info("User name is null for tag");
            return false;
        }

        String tagReceived = "";
        if (!StringUtils.isEmpty(tagName)) {
            tagReceived = tagName;
        } else {
            if (!StringUtils.isEmpty(tagLast)) {
                tagReceived = tagLast.substring(tagLast.length() - 7);
            } else {
                if (!StringUtils.isEmpty(tagFirst) && tagFirst.length() >= 7) {
                    tagReceived = tagFirst.substring(0, 7);
                }
            }
        }

        boolean result = tagService.uploadTag(name, tagReceived);

        return result;
    }

}
