package com.scnsoft.fotaflo.webapp.dao.impl;

import com.scnsoft.fotaflo.webapp.dao.IRequestDao;
import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.model.Requests;
import com.scnsoft.fotaflo.webapp.model.SystemUser;

import java.util.List;

/**
 * Created by Nadezda Drozdova
 * Date: Apr 29, 2011
 * Time: 6:20:54 PM
 */
public class RequestDao extends BaseDao implements IRequestDao {

    public List<Requests> getRequestsByLocation(Location location) {
        return getHibernateTemplate().find(
                "from Requests req where req.location = ?", location);
    }

    public List<Requests> getRequestsByUser(SystemUser user) {
        return getHibernateTemplate().find(
                "from Requests req where req.systemUser = ?", user);
    }

}
