package com.scnsoft.fotaflo.webapp.dao.impl;

import com.scnsoft.fotaflo.webapp.dao.ISettingsDao;
import com.scnsoft.fotaflo.webapp.model.SystemUser;
import com.scnsoft.fotaflo.webapp.model.UserSettings;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */

public class SettingsDao extends BaseDao implements ISettingsDao {

    public void updateUserSettings(SystemUser user, Integer pictureCount, Integer speed, Integer startTime, Integer delay) {
        UserSettings settings = user.getUserSettings();
        settings.setPictureCount(pictureCount);
        settings.setSpeed(speed);
        settings.setStartTime(startTime);
        settings.setDelay(delay);
        update(settings);
    }
}
