package com.scnsoft.fotaflo.webapp.service;

import com.scnsoft.fotaflo.common.util.NumberUtils;
import com.scnsoft.fotaflo.webapp.bean.TagBean;
import com.scnsoft.fotaflo.webapp.dao.IDao;
import com.scnsoft.fotaflo.webapp.dao.ITagDao;
import com.scnsoft.fotaflo.webapp.model.*;
import com.scnsoft.fotaflo.webapp.util.AccessID;
import com.scnsoft.fotaflo.webapp.util.DateConvertationUtils;
import com.scnsoft.fotaflo.webapp.util.Filter;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 03.10.2012
 * Time: 7:50:00
 * To change this template use File | Settings | File Templates.
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class TagService {
    protected static Logger logger = Logger.getLogger(TagService.class);

    @Autowired
    private IDao baseDao;

    @Autowired
    private ITagDao tagDao;

    @Autowired
    private UserService userService;

    @Autowired
    private PublicLoginService publicLoginService;

    @Autowired
    private PictureSlideShowService pictureSlideShowService;

    public List<TagBean> getPictureTags(String userLogin, Filter filter, Location location, String mandatoryTagIds, int start, int limit){
        List<TagBean> result = new ArrayList<TagBean>();
        Set<Tag> tags = new HashSet<Tag>();
        List<Picture> pictures = pictureSlideShowService.getPicturesFilteredList(userLogin, filter, start, limit).getItems();
        for(Picture p: pictures){
            if(p.getTags() != null && !p.getTags().isEmpty()){
                tags.addAll(p.getTags());
            }
        }

        Map<Integer, Tag> tagMap = new HashMap<Integer, Tag>();

        for(Tag t: tags){
            result.add(new TagBean(t));
            tagMap.put(t.getId(), t);
        }

        if (!StringUtils.isEmpty(mandatoryTagIds)) {
            String[] additionalTags = mandatoryTagIds.split(",");
            if (additionalTags.length > 0) {
                for (String additionaltag : additionalTags) {
                    Integer addTagId = NumberUtils.getInteger(additionaltag);
                    if(addTagId != null){
                        Tag tag = (Tag) baseDao.retrieve(Tag.class, addTagId);
                        if (tag != null && !tagMap.containsKey(addTagId)) {
                            TagBean tagBean = new TagBean();
                            tagBean.setId(tag.getId());
                            tagBean.setLocation(tag.getLocation() != null ? tag.getLocation().getLocationName() : null);
                            tagBean.setTagIdName(tag.getTagName());
                            result.add(tagBean);
                            tagMap.put(tag.getId(), tag);
                        }
                    }
                }
            }
        }

        Collections.sort(result, new Comparator<TagBean>() {

            public int compare(TagBean o1, TagBean o2) {
                if (o1.getTagIdName().toLowerCase() != null && o2.getTagIdName().toLowerCase() != null) {
                    if (o1.getId() == 0) {
                        return -1;
                    }
                    return o1.getTagIdName().compareTo(o2.getTagIdName());
                } else {
                    return 0;
                }
            }
        });
        result.add(new TagBean(createNoneTag(location)));

        return result;
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<TagBean> getExistedTagsByLocationDate(Date startDate, Date endDate, Location location, SystemUser user) {
        List<TagBean> tagListBean = new ArrayList<TagBean>();
        if (location == null) {
            if (!Integer.valueOf(user.getAccess()).equals(AccessID.ADMIN.getValue())) {
                logger.error("The loctaion could not be defined");
                return (tagListBean);
            }
        }

        List<Tag> tagsforPeriod = tagDao.getTagsByLocation(location, startDate, endDate);

        for (Tag tag : tagsforPeriod) {
            TagBean tagBean = new TagBean();
            tagBean.setId(tag.getId());
            tagBean.setLocation(tag.getLocation() != null ? tag.getLocation().getLocationName() : null);
            tagBean.setTagIdName(tag.getTagName());
            tagListBean.add(tagBean);
        }

        return tagListBean;
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<TagBean> getTagsByLocation(Location location, SystemUser user) {
        List<TagBean> tagListBean = new ArrayList<TagBean>();
        List<Tag> tagsforPeriod = new ArrayList<Tag>();
        if (location == null) {
            if (!Integer.valueOf(user.getAccess()).equals(AccessID.ADMIN.getValue())) {
                tagsforPeriod = tagDao.getTagsByLocation(user.getLocation());
            } else {
                tagsforPeriod = baseDao.retrieveAll(Tag.class);
            }
        } else {
            tagsforPeriod = tagDao.getTagsByLocation(location);
        }

        for (Tag tag : tagsforPeriod) {
            TagBean tagBean = new TagBean();
            tagBean.setId(tag.getId());
            tagBean.setLocation(tag.getLocation() != null ? tag.getLocation().getLocationName() : null);
            tagBean.setTagIdName(tag.getTagName());
            tagListBean.add(tagBean);
        }
        return tagListBean;
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public boolean isSlideShowToBeUpdated(String lastReceivedTag, Date lastReceivedTagTime, SystemUser user) {
        if (user != null && user.getUserSettings() != null) {
            //Last receivedTag time : current Time
            Tag lastTag = user.getUserSettings().getLastTag();
            if (lastTag != null && !lastReceivedTag.equals(lastTag.getTagName()) && lastReceivedTagTime.before(lastTag.getCreationDate())) {
                return true;
            }
        }
        return false;
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public Tag getTagsByUserAndName(String tagName, SystemUser user) {
        if (user == null || user.getUserSettings() == null || user == null) {
            return null;
        }
        Set<Tag> tags = user.getUserSettings().getTags();
        Integer[] ids = new Integer[tags.size()];
        int i = 0;
        for (Tag tag : tags) {
            ids[i] = tag.getId();
            i++;
        }

        Tag tagByUser = tagDao.getTagsByUserAndName(ids, tagName);
        return tagByUser;
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<TagBean> getTagsByUserBetweenDates(Date startDate, Date endDate, SystemUser user) {
        List<TagBean> tagListBean = new ArrayList<TagBean>();
        if (user == null || user.getUserSettings() == null) {
            return (tagListBean);
        }
        Set<Tag> tags = user.getUserSettings().getTags();
        Integer[] ids = new Integer[tags.size()];
        int i = 0;
        for (Tag tag : tags) {
            ids[i] = tag.getId();
            i++;
        }

        List<Tag> tagsByUser = tagDao.getTagsByUser(ids, startDate, endDate);

        for (Tag tag : tagsByUser) {
            TagBean tagBean = new TagBean();
            tagBean.setId(tag.getId());
            tagBean.setLocation(tag.getLocation() != null ? tag.getLocation().getLocationName() : null);
            tagBean.setTagIdName(tag.getTagName());
            tagBean.setCreationDate(tag.getCreationDate());
            tagListBean.add(tagBean);
        }

        return tagListBean;
    }

    public Tag createNoneTag(Location location) {
        Tag tag = new Tag();
        tag.setId(0);
        tag.setTagName("No Tag");
        tag.setLocation(location);
        return tag;
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public Tag getTagByNameLocation(String tagName, Location location) {
        return tagDao.getTagByNameLocation(tagName, location);
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<TagBean> getAllTags() {
        List<TagBean> tagListBean = new ArrayList<TagBean>();
        List<Tag> tagList = baseDao.retrieveAll(Tag.class);
        for (Tag tag : tagList) {
            TagBean tagBean = new TagBean();
            tagBean.setId(tag.getId());
            tagBean.setLocation(tag.getLocation().getLocationName());
            tagBean.setTagIdName(tag.getTagName());
            tagListBean.add(tagBean);
        }
        return tagListBean;
    }

    public Map<String, Object> getTagNewTime(Date startDate, Date endDate, String addedTagId) {
        Map map = new HashMap<String, String>();

        if (!StringUtils.isEmpty(addedTagId)) {
            //We will need to define new timeFrame for the tag
            //Get tag from Db
            Tag newTag = (Tag) baseDao.retrieve(Tag.class, Integer.valueOf(addedTagId));
            if (newTag != null) {
                Date creationDate = newTag.getCreationDate();
                if (creationDate.before(startDate)) {
                    startDate = creationDate;
                    map.put("startDateNew", startDate);
                } else {
                    if (creationDate.after(endDate)) {
                        endDate = DateConvertationUtils.getEndDatePlusHours(1, creationDate);
                        map.put("endDateNew", endDate);
                    }
                }
            }
        }

        return map;
    }

    public Map<String, Object> getTagGroups(Date startDate, Date endDate, String addedTagIdString) {
        Map map = new HashMap<String, String>();
        String tagsSting = "";
        Map<Integer, Tag> tagMap = new HashMap<Integer, Tag>();
        List<Tag> tagsList = new ArrayList<Tag>();
        if (!StringUtils.isEmpty(addedTagIdString)) {
            //We will need to define new timeFrame for the tag
            //Get tag from Db
            List<String> addedTagIds = Arrays.asList(addedTagIdString.split(","));
            for (String addedTagId : addedTagIds) {
                if (addedTagId.equals("0")) {
                    tagsSting = addedTagId;
                } else {
                    Tag newTag = (Tag) baseDao.retrieve(Tag.class, Integer.valueOf(addedTagId));

                    if (newTag != null) {
                        if (!tagMap.containsKey(newTag.getId())) {
                            tagMap.put(newTag.getId(), newTag);
                            tagsList.add(newTag);
                        }
                        Set<TagPurchase> tagPurchasesSet = newTag.getTagPurchases();
                        for (TagPurchase tagPurchase : tagPurchasesSet) {

                            if (tagPurchase != null) {
                                if (!tagPurchase.getEndDate().before(startDate) && !tagPurchase.getStartDate().after(endDate)) {
                                    Set<Tag> tags = tagPurchase.getTags();
                                    for (Tag tag : tags) {
                                        if (!tagMap.containsKey(tag.getId())) {
                                            logger.info("Depending tag to the current " + addedTagId + " " + tag.getId());
                                            tagMap.put(tag.getId(), tag);
                                            tagsList.add(tag);
                                        } else {
                                            System.out.println("Already exist in list " + tag.getId());
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }

            for (Tag tagFromList : tagsList) {
                tagsSting = (tagsSting.length() == 0) ? String.valueOf(tagFromList.getId()) : (tagsSting + "," + tagFromList.getId());
            }

            map.put("tagsToSet", tagsSting);
        }

        return map;
    }

    public List<Tag> getDependantTags(Date startDate, Date endDate, String addedTagIdString) {
        Map<Integer, Tag> tagMap = new HashMap<Integer, Tag>();
        List<Tag> tagsList = new ArrayList<Tag>();
        if (!StringUtils.isEmpty(addedTagIdString)) {
            List<String> addedTagIds = Arrays.asList(addedTagIdString.split(","));
            for (String addedTagId : addedTagIds) {
                if (addedTagId.equals("0")) {
                    // tagsSting=addedTagId;
                } else {
                    Tag newTag = (Tag) baseDao.retrieve(Tag.class, Integer.valueOf(addedTagId));

                    if (newTag != null) {
                        if (!tagMap.containsKey(newTag.getId())) {
                            tagMap.put(newTag.getId(), newTag);
                            tagsList.add(newTag);
                        }
                        Set<TagPurchase> tagPurchasesSet = newTag.getTagPurchases();
                        for (TagPurchase tagPurchase : tagPurchasesSet) {

                            if (tagPurchase != null) {
                                if (tagPurchase.getEndDate() == null || tagPurchase.getStartDate() == null || startDate == null || endDate == null || (!tagPurchase.getEndDate().before(startDate) && !tagPurchase.getStartDate().after(endDate))) {
                                    Set<Tag> tags = tagPurchase.getTags();
                                    for (Tag tag : tags) {
                                        if (!tagMap.containsKey(tag.getId())) {
                                            tagMap.put(tag.getId(), tag);
                                            tagsList.add(tag);
                                        } else {
                                            System.out.println("Depending tag to the current  " + addedTagId + " " + "Already exist in list " + tag.getId());
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }
        }

        return tagsList;
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<TagBean> getTagsByQuery(String query) {
        List<TagBean> tagList = getAllTags();
        List<TagBean> tagListQuired = new ArrayList<TagBean>();
        for (TagBean tag : tagList) {
            if (String.valueOf(tag.getId()).length() >= query.length() && Integer.valueOf(String.valueOf(tag.getId()).substring(0, 2)) == Integer.valueOf(query)) {
                tagListQuired.add(tag);
            }
        }
        return tagListQuired;
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public void createTag(Tag tag) {
        if (tag != null) {
            baseDao.create(tag);
        }
    }

    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public void updateTag(Tag tag) {
        if (tag != null) {
            baseDao.update(tag);
        }
    }

    public boolean uploadTag(String username, String tag){
        if(StringUtils.isEmpty(username)){
            return false;
        }
        SystemUser user = userService.getUserByLogin(username);
        if (user == null) {
            logger.info("User is null for tag");
            return false;
        }

        boolean result = false;

        if (!StringUtils.isEmpty(tag)) {
            result = this.isSlideShowToBeUpdated(tag, getStartTime(), userService.getCurrentUser());

            logger.info("We have got a tag for user " + username + " tag: " + tag);
            Tag picTag = this.getTagByNameLocation(tag, user.getLocation());
            if (picTag == null) {
                Date today = new Date();
                Tag t = new Tag();
                t.setCreationDate(new Date());
                t.setTagName(tag);
                t.setLocation(user.getLocation());
                t.setTagPurchases(new HashSet<TagPurchase>());
                t.setTagedpictures(new HashSet<Picture>());
                Set<UserSettings> usersSets = new HashSet<UserSettings>();
                usersSets.add(user.getUserSettings());
                t.setTagUsers(usersSets);
                this.createTag(t);
                logger.info("Tag was created successfully " + tag + " for user " + username);

                UserSettings userSet = user.getUserSettings();
                userSet.setLastTag(t);
                Set<Tag> userTags = (userSet.getTags() == null) ? new HashSet<Tag>() : userSet.getTags();
                userTags.add(t);
                userSet.setTags(userTags);
                userService.update(userSet);
                publicLoginService.createTagPublicLogin(tag, user.getLocation(), today, String.valueOf(AccessID.PUBLICTAGUSER.getValue()));
            } else {
                picTag.setCreationDate(new Date());
                Tag tagUser = this.getTagsByUserAndName(tag, user);
                UserSettings userSet = user.getUserSettings();
                if (picTag.getTagUsers() != null && picTag.getTagUsers().size() != 0 && tagUser != null && tagUser.getId() == picTag.getId()) {
                    //tat means that user already has this tag
                    this.updateTag(picTag);
                    userSet.setLastTag(picTag);
                    userService.update(userSet);
                } else {
                    Set<UserSettings> usersSets = new HashSet<UserSettings>();
                    usersSets.add(userSet);
                    picTag.setTagUsers(usersSets);
                    this.updateTag(picTag);
                    userSet.setLastTag(picTag);
                    Set<Tag> userTags = (userSet.getTags() == null) ? new HashSet<Tag>() : userSet.getTags();
                    userTags.add(picTag);
                    userSet.setTags(userTags);
                    userService.update(userSet);
                }
            }
        }

        return result;
    }

    private Date getStartTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.clear(Calendar.MILLISECOND);
        calendar.clear(Calendar.SECOND);
        calendar.clear(Calendar.MINUTE);
        if (calendar.get(Calendar.HOUR_OF_DAY) < 5) {
            calendar.add(Calendar.DAY_OF_MONTH, -1);
        }
        calendar.set(Calendar.HOUR_OF_DAY, 5);
        return calendar.getTime();
    }

}
