package com.scnsoft.fotaflo.webapp.bean;

/**
 * Created by Nadezda Drozdova
 * Date Apr 11, 2011
 */
public class PackageBean {

    protected int id;
    private String packageName;

    public PackageBean() {
    }

    public PackageBean(int id, String packageName) {
        this.id = id;
        this.packageName = packageName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
}
