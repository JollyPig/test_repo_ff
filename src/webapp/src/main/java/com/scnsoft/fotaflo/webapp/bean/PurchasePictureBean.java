package com.scnsoft.fotaflo.webapp.bean;

import java.util.Date;

/**
 * Created by Nadezda Drozdova
 * Date Apr 15, 2011
 */
public class PurchasePictureBean extends PictureBean {
    Boolean selectedPurchase;
    Integer toPrintNumber;
    Boolean addToFacebook;
    Boolean addToFlickr;
    Date date; // todo instead of creation date

    public PurchasePictureBean(int id, String name, String url, String cameraId, String cameraName,
                               String creationDate, Boolean selected, Boolean selectedPurchase, Integer toPrintNumber,
                               Boolean addToFacebook, Boolean addToFlickr, Boolean rotated, String pictureSize) {
        super(id, name, url, cameraId, cameraName, creationDate, selected, rotated, pictureSize);
        this.selectedPurchase = selectedPurchase;
        this.toPrintNumber = toPrintNumber;
        this.addToFacebook = addToFacebook;
        this.addToFlickr = addToFlickr;
    }

    public Boolean isSelectedPurchase() {
        return selectedPurchase;
    }

    public void setSelectedPurchase(Boolean selectedPurchase) {
        this.selectedPurchase = selectedPurchase;
    }

    public Integer getToPrintNumber() {
        return toPrintNumber;
    }

    public void setToPrintNumber(Integer toPrintNumber) {
        this.toPrintNumber = toPrintNumber;
    }

    public Boolean getAddToFacebook() {
        return addToFacebook;
    }

    public void setAddToFacebook(Boolean addToFacebook) {
        this.addToFacebook = addToFacebook;
    }

    public Boolean getAddToFlickr() {
        return addToFlickr;
    }

    public void setAddToFlickr(Boolean addToFlickr) {
        this.addToFlickr = addToFlickr;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
