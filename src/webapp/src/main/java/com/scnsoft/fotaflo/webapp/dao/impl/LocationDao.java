package com.scnsoft.fotaflo.webapp.dao.impl;

import com.scnsoft.fotaflo.common.dao.AbstractDao;
import com.scnsoft.fotaflo.webapp.dao.ILocationDao;
import com.scnsoft.fotaflo.webapp.model.Location;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 */
@Repository
public class LocationDao extends AbstractDao<Location, Integer> implements ILocationDao {
    @Override
    protected Class<Location> getDomainClass() {
        return Location.class;
    }

    @Override
    public Location findById(Integer id) {
        List result = getSession().createCriteria(getDomainClass()).add(Restrictions.eq("id", id)).list();
        return (result != null && !result.isEmpty()) ? (Location) result.get(0) : null;
    }

}
