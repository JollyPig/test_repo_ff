package com.scnsoft.fotaflo.webapp.model;

import com.scnsoft.fotaflo.common.model.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 20.12.2011
 * Time: 14:13:31
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "locationsettings")
public class LocationSettings extends AbstractEntity {

    String email1;
    String email2;
    String email3;
    String emailString1;
    String emailString2;
    String emailString3;
    String area;
    String publicemalarea;
    String emailDaily1;
    String emailDaily2;
    String emailDaily3;

    @Column(name = "email1")
    public String getEmail1() {
        return email1;
    }

    public void setEmail1(String email1) {
        this.email1 = email1;
    }

    @Column(name = "email2")
    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    @Column(name = "email3")
    public String getEmail3() {
        return email3;
    }

    public void setEmail3(String email3) {
        this.email3 = email3;
    }

    @Column(name = "emailDaily1")
    public String getEmailDaily1() {
        return emailDaily1;
    }

    public void setEmailDaily1(String emailDaily1) {
        this.emailDaily1 = emailDaily1;
    }

    @Column(name = "emailDaily2")
    public String getEmailDaily2() {
        return emailDaily2;
    }

    public void setEmailDaily2(String emailDaily2) {
        this.emailDaily2 = emailDaily2;
    }

    @Column(name = "emailDaily3")
    public String getEmailDaily3() {
        return emailDaily3;
    }

    public void setEmailDaily3(String emailDaily3) {
        this.emailDaily3 = emailDaily3;
    }

    @Lob
    @Column(name = "emailString1")
    public String getEmailString1() {
        return emailString1;
    }

    public void setEmailString1(String emailString1) {
        this.emailString1 = emailString1;
    }

    @Lob
    @Column(name = "emailString2")
    public String getEmailString2() {
        return emailString2;
    }

    public void setEmailString2(String emailString2) {
        this.emailString2 = emailString2;
    }

    @Lob
    @Column(name = "emailString3")
    public String getEmailString3() {
        return emailString3;
    }

    public void setEmailString3(String emailString3) {
        this.emailString3 = emailString3;
    }

    @Lob
    @Column(name = "footer")
    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    @Lob
    @Column(name = "publicemailarea")
    public String getPublicemalarea() {
        return publicemalarea;
    }

    public void setPublicemalarea(String publicemalarea) {
        this.publicemalarea = publicemalarea;
    }
}
