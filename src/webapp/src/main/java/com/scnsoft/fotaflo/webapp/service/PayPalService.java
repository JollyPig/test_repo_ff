package com.scnsoft.fotaflo.webapp.service;

import com.scnsoft.fotaflo.webapp.dao.IDao;
import com.scnsoft.fotaflo.webapp.dao.IPayPalDao;
import com.scnsoft.fotaflo.webapp.model.Merchant;
import com.scnsoft.fotaflo.webapp.model.Payment;
import com.scnsoft.fotaflo.webapp.util.PasswordEncryption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Paradinets
 * Date: 15.02.2012
 * Time: 17:48:58
 * To change this template use File | Settings | File Templates.
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class PayPalService {

    @Autowired
    private IDao baseDao;

    @Autowired
    private IPayPalDao payPalDao;

    public void registerPayment(String userlogin, Date today, String currency, Double price,
                                Long invoiceId, String status, String transaction, String result, String ids, String email) {

        Payment payment = new Payment();
        payment.setUserLogin(userlogin);
        payment.setInvoiceId(invoiceId);
        payment.setPrice(price);
        payment.setCurrency(currency);
        payment.setStatus(status);
        payment.setToken(transaction);
        payment.setResult(result);
        payment.setPaymentDate(today);
        payment.setPictureids(ids);
        payment.setEmail(email);
        baseDao.create(payment);
    }

    public void savePayment(Payment payment) {
        baseDao.update(payment);
    }

    public Payment getRegisteredPayment(String token) {
        if (token == null) {
            return null;
        }
        List<Payment> payments = payPalDao.getPaymentByTransaction(token);
        if (payments != null && payments.size() == 1) {
            return payments.get(0);
        } else {
            return null;
        }
    }

    public Merchant getMerchantAccount() throws Exception {
        Merchant merchant = null;
        List<Merchant> accounts = baseDao.retrieveAll(Merchant.class);
        if (accounts.size() == 1) {
            merchant = accounts.get(0);
        }
        if (merchant != null) {
            PasswordEncryption passwordEncryption = new PasswordEncryption();
            merchant.setPassword(passwordEncryption.decryptPassword(merchant.getPassCode()));
            merchant.setSignature(passwordEncryption.decryptPassword(merchant.getSignCode()));
        }
        return merchant;
    }

    public Merchant getMerchant() throws Exception {
        Merchant merchant = null;
        List<Merchant> accounts = baseDao.retrieveAll(Merchant.class);
        if (accounts.size() == 1) {
            merchant = accounts.get(0);
        }
        return merchant;
    }

    public Merchant saveMerchantAccount(String account,
                                        String password,
                                        String signature,
                                        String environment) throws Exception {
        Merchant merchant = getMerchant();
        if (merchant != null) {
            PasswordEncryption passwordEncryption = new PasswordEncryption();

            merchant.setAccount(account);
            merchant.setPassword(password);
            merchant.setPassCode(passwordEncryption.encryptPassword(password));
            merchant.setSignature(signature);
            merchant.setSignCode(passwordEncryption.encryptPassword(signature));
            merchant.setEnvironment(environment);
            baseDao.update(merchant);
        } else {
            PasswordEncryption passwordEncryption = new PasswordEncryption();
            merchant = new Merchant();
            merchant.setAccount(account);
            merchant.setPassword(password);
            merchant.setPassCode(passwordEncryption.encryptPassword(password));
            merchant.setSignature(signature);
            merchant.setSignCode(passwordEncryption.encryptPassword(signature));
            merchant.setEnvironment(environment);
            baseDao.create(merchant);
        }
        return merchant;
    }

    public Map<String, Object> getMerchantSettings() {
        Map map = new HashMap<String, String>();
        Merchant merch = null;
        try {
            merch = getMerchantAccount();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (merch != null) {
            map.put("account", merch.getAccount());
            map.put("password", merch.getPassword());
            map.put("signature", merch.getSignature());
            map.put("environment", merch.getEnvironment());
            return map;
        } else {
            return null;
        }
    }

    public void setBaseDao(IDao baseDao) {
        this.baseDao = baseDao;
    }

    public void setPayPalDao(IPayPalDao payPalDao) {
        this.payPalDao = payPalDao;
    }
}
