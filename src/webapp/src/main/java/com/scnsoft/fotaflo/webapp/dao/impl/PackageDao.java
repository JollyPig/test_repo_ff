package com.scnsoft.fotaflo.webapp.dao.impl;

import com.scnsoft.fotaflo.webapp.dao.IPackageDao;
import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.model.Package;

import java.util.List;

/**
 * Created by Drazdova
 */

public class PackageDao extends BaseDao implements IPackageDao {

    public List<Package> getPackagesByLocation(Location location) {
        return getHibernateTemplate().find(
                "from Package pack where pack.location = ?", location);
    }

    public List<Package> getNotPublicPackagesByLocation(Location location) {
        return getHibernateTemplate().find(
                "from Package pack where pack.location = ? and pack.access=1", location);
    }

    public List<Package> getPublicPackagesByLocation(Location location, int access) {
        return getHibernateTemplate().find(
                "from Package pack where pack.location = ? and pack.access=" + access, location);
    }

    public List getStatisticForPackage(Integer packageId) {
        return getHibernateTemplate().find(
                "from Statistic statistic where statistic.packages.id = ?", packageId);
    }

    public List getPublicStatisticForPackage(Integer packageId) {
        return getHibernateTemplate().find(
                "from PublicStatistic publicstatistic where publicstatistic.packages.id = ?", packageId);
    }
}