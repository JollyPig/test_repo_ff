package com.scnsoft.fotaflo.webapp.controller;

import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.service.PictureService;
import com.scnsoft.fotaflo.webapp.service.RequestService;
import com.scnsoft.fotaflo.webapp.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nadezda Drozdova
 * Date: Apr 27, 2011
 * Time: 8:54:31 PM
 */
@Controller
public class RequestController {
    protected static Logger logger = Logger.getLogger(ReportController.class);
    @Autowired
    private PictureService pictureService;
    @Autowired
    private RequestService requestService;

    @Autowired
    private UserService userService;

    private final static String successProperty = "success";

    @RequestMapping(value = "getRequests.json"/*, method = RequestMethod.GET*/)
    public
    @ResponseBody
    Map<String, ? extends Object> loadRequests(HttpServletRequest httpServletRequest) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        HashMap modelMap = new HashMap();
        int locationId = Integer.valueOf(httpServletRequest.getParameter("locationId") != null ? httpServletRequest.getParameter("locationId") : "0");
        modelMap.put("requests", requestService.getRequestBeansAll(currentUser, locationId));
        return modelMap;
    }

    @RequestMapping(value = "getRequestsDeleted.json")
    public
    @ResponseBody
    Map<String, ? extends Object> gettHelpYourSelfPicturesDeleted(HttpServletRequest httpServletRequest) throws Exception {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        if (httpServletRequest.getParameter("requests") != null) {
            String arr = httpServletRequest.getParameter("requests");
            requestService.deleteRequest(arr, currentUser);
        }
        HashMap<String, Boolean> modelMap = new HashMap<String, Boolean>();
        modelMap.put(successProperty, true);
        logger.info("User [ " + currentUser + " ]" + " deleted the requests");
        return modelMap;
    }

    @RequestMapping(value = "applyRequest.json", method = RequestMethod.POST)
    public
    @ResponseBody
    Map<String, ? extends Object> applyRequests(HttpServletRequest httpServletRequest) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        HashMap modelMap = new HashMap();
        int requestId = Integer.valueOf(httpServletRequest.getParameter("requestId") != null ? httpServletRequest.getParameter("requestId") : "-1");
        modelMap.put("requestId", requestId);
        logger.info("User [ " + currentUser + " ]" + " applied the request " + requestId);
        modelMap.put(successProperty, true);
        return modelMap;
    }

    @RequestMapping(value = "/main/requests")
    public String getHelpYourSelfPage(ModelMap model, HttpServletRequest httpServletRequest) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        model.put("current", currentUser);
        model.put("userId", pictureService.getUserAccess(currentUser));
        logger.info("User [ " + currentUser + " ]" + " opened the request page");
        Location location = userService.getUserLocation(currentUser);
        boolean tagged = (location != null && location.getTagged() != null && location.getTagged() == true) ? true : false;
        model.put("tagApp", tagged);
        return "requestpage";
    }

    public void setPictureService(PictureService pictureService) {
        this.pictureService = pictureService;
    }

    public void setRequestService(RequestService requestService) {
        this.requestService = requestService;
    }
}
