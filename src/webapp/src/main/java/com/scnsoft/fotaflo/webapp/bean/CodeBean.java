package com.scnsoft.fotaflo.webapp.bean;


import com.scnsoft.fotaflo.webapp.model.TagPurchase;

public class CodeBean {

    private Integer id;
    private String code;
    private String location;

    public CodeBean() {
    }

    public CodeBean(Integer id, String code, String location) {
        this.id = id;
        this.code = code;
        this.location = location;
    }

    public CodeBean(TagPurchase entity) {
        this.id = entity.getId();
        this.code = entity.getGeneratedCode();
        if (entity.getLocation() != null) {
            this.location = entity.getLocation().getLocationName();
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
