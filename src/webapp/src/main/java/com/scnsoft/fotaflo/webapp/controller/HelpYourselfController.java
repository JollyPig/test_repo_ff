package com.scnsoft.fotaflo.webapp.controller;

import com.scnsoft.fotaflo.webapp.model.Location;
import com.scnsoft.fotaflo.webapp.service.PictureService;
import com.scnsoft.fotaflo.webapp.service.RequestService;
import com.scnsoft.fotaflo.webapp.service.UserService;
import com.scnsoft.fotaflo.webapp.util.DefaultValues;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Nadezda Drozdova
 * Date: Apr 26, 2011
 * Time: 12:46:15 PM
 */

@Controller
public class HelpYourselfController {

    @Autowired
    private PictureService pictureService;

    @Autowired
    private RequestService requestService;

    @Autowired
    private UserService userService;

    protected static Logger logger = Logger.getLogger(HelpYourselfController.class);
    private final static String successProperty = "success";
    private final static String msgProperty = "msg";

    @RequestMapping(value = "getHelpYourSelfPicturesByUser.json")
    public
    @ResponseBody
    Map<String, ? extends Object> loadPictures(HttpServletRequest httpServletRequest) {

        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        String pageLimit = pictureService.getPageSize(currentUser);
        String[] helpselfsIds = httpServletRequest.getParameterValues("helpselfs") != null ? httpServletRequest.getParameterValues("helpselfs") : null;
        int start = Integer.valueOf(httpServletRequest.getParameter("start") != null ? httpServletRequest.getParameter("start") : "0");
        int limit = Integer.valueOf(httpServletRequest.getParameter("limit") != null ? httpServletRequest.getParameter("limit") : pageLimit);
        HashMap modelMap = new HashMap();
        modelMap.put("pictures", requestService.getHelpYourSelfPictureBeansAll(start, limit, currentUser, stringToArray(helpselfsIds)));
        modelMap.put("total", requestService.getHelpYourSelfPictureBeansAll(currentUser, stringToArray(helpselfsIds)).size());
        modelMap.put("pageSize", pageLimit);
        modelMap.put("helpselfs", stringToArray(helpselfsIds));
        return modelMap;
    }

    @RequestMapping(value = "getHelpYourSelfPicturesByUserUpdate.json"/*, method = RequestMethod.POST*/)
    public
    @ResponseBody
    Map<String, ? extends Object> updateSelectedPurchasePictures(@RequestParam(value = "pictures", required = false) Object data, HttpServletRequest httpServletRequest) throws Exception {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        String pageLimit = pictureService.getPageSize(currentUser);
        int start = Integer.valueOf(httpServletRequest.getParameter("start") != null ? httpServletRequest.getParameter("start") : "0");
        int limit = Integer.valueOf(httpServletRequest.getParameter("limit") != null ? httpServletRequest.getParameter("limit") : pageLimit);
        List<Integer> helpselfsIds = requestService.updateHelpYourSelfPictureBeans(data, currentUser);
        HashMap modelMap = new HashMap();
        modelMap.put("pictures", requestService.getHelpYourSelfPictureBeansAll(start, limit, currentUser, helpselfsIds));
        modelMap.put("total", requestService.getHelpYourSelfPictureBeansAll(currentUser, helpselfsIds).size());
        modelMap.put("pageSize", pictureService.getPageSize(currentUser));
        modelMap.put("helpselfs", helpselfsIds);
        return modelMap;

    }

    @RequestMapping(value = "getHelpYourSelfPicturesByUserDelete.json")
    public
    @ResponseBody
    Map<String, ? extends Object> getHelpYourSelfPicturesDeleted(HttpServletRequest httpServletRequest) throws Exception {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        String[] helpselfsIds = httpServletRequest.getParameterValues("helpselfs") != null ? httpServletRequest.getParameterValues("helpselfs") : null;
        List<Integer> helpselfs = new ArrayList<Integer>();
        if (helpselfsIds != null && helpselfsIds.length > 0) {
            helpselfs = stringToArray(helpselfsIds);
        }
        if (httpServletRequest.getParameter("pictures") != null) {
            String arr = httpServletRequest.getParameter("pictures");
            helpselfs = requestService.deleteHelpYourSelfs(arr, currentUser, helpselfs);
        }
        HashMap modelMap = new HashMap();
        modelMap.put("helpselfs", helpselfs);
        modelMap.put(successProperty, true);
        return modelMap;
    }

    @RequestMapping(value = "getHelpYourSelfPicturesPageSizeUpdate.json"/*, method = RequestMethod.POST*/)
    public
    @ResponseBody
    Map<String, ? extends Object> updatePageSize(HttpServletRequest httpServletRequest) throws Exception {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        String pageLimit = pictureService.getPageSize(currentUser);
        String pageSize = httpServletRequest.getParameter("pageSize") != null ? httpServletRequest.getParameter("pageSize") : pageLimit;

        HashMap modelMap = new HashMap();
        modelMap.put("pageSize", pageSize);
        return modelMap;
    }

    @RequestMapping(value = "submitHelpYourSelfForm.json")
    public
    @ResponseBody
    Map<String, ? extends Object> submitForm(HttpServletRequest httpServletRequest) {

        HashMap modelMap = new HashMap();
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        String numberParticipant = httpServletRequest.getParameter("inumberInGroup");
        String otherQuestions = httpServletRequest.getParameter("otherQuestions");
        int numberOfParticipant = numberParticipant == null ? 0 : Integer.valueOf(numberParticipant);
        List<String> emails = new ArrayList<String>();
        for (int i = 1; i <= numberOfParticipant; i++) {
            if (httpServletRequest.getParameter("email" + i) != null)
                emails.add(httpServletRequest.getParameter("email" + i));
        }
        String[] helpselfsIds = httpServletRequest.getParameterValues("helpselfs") != null ? httpServletRequest.getParameterValues("helpselfs") : null;
        List<Integer> helpselfs = new ArrayList<Integer>();
        if (helpselfsIds != null && helpselfsIds.length > 0) {
            helpselfs = stringToArray(helpselfsIds);
        }
        if (!helpselfs.isEmpty()) {
            boolean createReq = requestService.createRequestFromHelpYourSelf(helpselfs, emails, otherQuestions, currentUser);
            if (createReq) {
                requestService.clearHelpYourSelfSelections(helpselfs, currentUser);
                modelMap.put(successProperty, true);
            }
        } else {
            modelMap.put(msgProperty, "Verify the Request Parameters");
            modelMap.put(successProperty, false);
        }
        modelMap.put("current", currentUser);
        modelMap.put("pageSize", pictureService.getPageSize(currentUser));
        modelMap.put("userId", pictureService.getUserAccess(currentUser));
        logger.info("User [ " + currentUser + " ]" + " submitted helpyourself form ?Successfully? " + successProperty);
        return modelMap;
    }

    @RequestMapping(value = "cancelHelpYourselfForm.json")
    public
    @ResponseBody
    String[] cancelForm(ModelMap model, HttpServletRequest httpServletRequest) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        String[] helpselfsIds = httpServletRequest.getParameterValues("helpselfs") != null ? httpServletRequest.getParameterValues("helpselfs") : null;
        String[] pictureIds = null;
        List<Integer> helpselfs = new ArrayList<Integer>();
        if (helpselfsIds != null && helpselfsIds.length > 0) {
            helpselfs = stringToArray(helpselfsIds);
        }
        if (!helpselfs.isEmpty()) {
            pictureIds = requestService.refreshHelpYourSelfPicturesSelectionModel(helpselfs);
            requestService.clearHelpYourSelfSelections(helpselfs, currentUser);
        }
        model.put("current", currentUser);
        model.put("userId", pictureService.getUserAccess(currentUser));
        model.put("pageSize", pictureService.getPageSize(currentUser));

        model.put("startDate", DefaultValues.getStartDate());
        model.put("endDate", DefaultValues.getEndDate());
        model.put("startDay", "");
        model.put("endDay", "");
        model.put("startTime", "");
        model.put("endTime", "");

        logger.info("User [ " + currentUser + " ]" + " cancelled helpyourself form ");
        return pictureIds;
    }

    @RequestMapping(value = "/main/helpyourself")
    public String getHelpYourSelfPage(ModelMap model, HttpServletRequest httpServletRequest) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        String[] params = httpServletRequest.getParameterValues("ids");
        model.put("pageSize", pictureService.getPageSize(currentUser));
        model.put("current", currentUser);
        model.put("userId", pictureService.getUserAccess(currentUser));
        model.put("startDate", DefaultValues.startDate);
        model.put("endDate", DefaultValues.endDate);
        model.put("startDay", "");
        model.put("endDay", "");
        model.put("startTime", "");
        model.put("endTime", "");

        List<Integer> helpselfs = requestService.createHelpYourSelfPicturesSelectionModel(currentUser, params);
        model.put("helpselfs", helpselfs);
        Location location = userService.getUserLocation(currentUser);
        boolean tagged = (location != null && location.getTagged() != null && location.getTagged() == true) ? true : false;
        model.put("tagApp", tagged);
        logger.info("User [ " + currentUser + " ]" + " opened helpyourself form ");
        return "helpyourselfpage";
    }

    private List<Integer> stringToArray(String[] ids) {
        List<Integer> list = new ArrayList<Integer>();
        for (String helpselfsId : ids) {
            if (helpselfsId != null && !helpselfsId.equals("")) {
                Integer idHelp = Integer.valueOf(helpselfsId);
                list.add(idHelp);
            }
        }
        return list;
    }
}
