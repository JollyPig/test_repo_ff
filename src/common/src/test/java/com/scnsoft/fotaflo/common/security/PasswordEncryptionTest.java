package com.scnsoft.fotaflo.common.security;

import org.testng.Assert;
import org.testng.annotations.Test;

public class PasswordEncryptionTest {

    private String encstr = "Some String For Encryption";
    private String encstr2 = "SomePass1";

    @Test
    public void test() throws Exception{
        processString(encstr);
        processString(encstr2);
    }

    private void processString(String str) throws Exception{
        PasswordEncryption pe = new PasswordEncryption();
        byte[] enc = pe.encryptPassword(str);

        String dec = pe.decryptPassword(enc);

        Assert.assertEquals(dec, str);
    }

}
