package com.scnsoft.fotaflo.common.mail;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class MailSenderFactoryTest {

    static JavaMailSender mailSender;

    @BeforeClass
    public void configureSender(){
        SmtpMailConfiguration config = new SmtpMailConfiguration("smtp.gmail.com", 465);
        config.setUsername("scnsofttestmail@gmail.com");
        config.setPassword("ScienceSoft0");
        config.setDebug(true);

        config.setAuth(true);
        config.setSocketFactory(javax.net.ssl.SSLSocketFactory.class);
        config.setSocketFactoryFallback(false);
        config.setStarttls(false);
        config.setQuitwait(false);

        mailSender = new MailSenderFactory().getMailSender(config);
    }

    @Test(priority = 0, enabled = false)
    public void test1(){
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("scnsofttestmail@gmail.com");
        message.setTo("scnsofttestmail@gmail.com");
        message.setSubject("test 1");
        message.setText("test 1 body");
        mailSender.send(message);
    }

    @Test(priority = 1, enabled = false)
    public void test2(){
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("scnsofttestmail@gmail.com");
        message.setTo("scnsofttestmail@gmail.com");
        message.setSubject("test 2");
        message.setText("test 2 body");
        mailSender.send(message);
    }

}
