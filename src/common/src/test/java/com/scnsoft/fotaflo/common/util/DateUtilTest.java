package com.scnsoft.fotaflo.common.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtilTest {

    public static void main(String[] args){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        Date date = DateUtil.emptyTime(new Date());
        System.out.println(format.format(date));

        date = DateUtil.fullTime();
        System.out.println(format.format(date));

        date = DateUtil.firstDayOfWeek();
        System.out.println(format.format(date));

        date = DateUtil.firstDayOfMonth();
        System.out.println(format.format(date));

        date = DateUtil.firstDayOfYear();
        System.out.println(format.format(date));

        date = DateUtil.lastDayOfWeek();
        System.out.println(format.format(date));

        date = DateUtil.lastDayOfMonth();
        System.out.println(format.format(date));

        date = DateUtil.lastDayOfYear();
        System.out.println(format.format(date));
    }

}
