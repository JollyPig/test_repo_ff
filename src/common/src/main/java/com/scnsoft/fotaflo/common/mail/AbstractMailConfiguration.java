package com.scnsoft.fotaflo.common.mail;

import java.util.Properties;

public abstract class AbstractMailConfiguration implements IMailConfiguration {

    private String host;
    private int port = 25;
    private String username;
    private String password;

    private boolean debug = false;

    protected AbstractMailConfiguration(String host) {
        this.host = host;
    }

    protected AbstractMailConfiguration(String host, int port) {
        this(host);
        this.port = port;
    }

    @Override
    public final String getHost() {
        return host;
    }

    @Override
    public final int getPort() {
        return port;
    }

    @Override
    public final String getUsername() {
        return username;
    }

    public final void setUsername(String username) {
        this.username = username;
    }

    @Override
    public final String getPassword() {
        return password;
    }

    public final void setPassword(String password) {
        this.password = password;
    }

    public final boolean isDebug() {
        return debug;
    }

    public final void setDebug(boolean debug) {
        this.debug = debug;
    }

    @Override
    public Properties getJavaMailProperties(){
        Properties properties = getProperties();
        if(properties != null){
            properties.put("mail.debug", debug);
        }

        return properties;
    }

    protected abstract Properties getProperties();

    @Override
    public boolean isValid(){
        return (host != null && !host.isEmpty()) && (port > 0)
                && (username != null && !username.isEmpty())
                && (password != null && !password.isEmpty())
                && (getJavaMailProperties() != null);
    }
}
