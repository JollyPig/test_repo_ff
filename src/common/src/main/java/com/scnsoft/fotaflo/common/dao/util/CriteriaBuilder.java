package com.scnsoft.fotaflo.common.dao.util;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;

public class CriteriaBuilder {

    private Session session;

    private Criteria criteria;

    public CriteriaBuilder(Session session, Class entity) {
        if(session == null){
            throw new IllegalArgumentException("'session' cannot be null");
        }
        if(entity == null){
            throw new IllegalArgumentException("'entity' cannot be null");
        }
        this.session = session;
        this.criteria = session.createCriteria(entity);
    }

    public CriteriaBuilder(Criteria criteria) {
        if(criteria == null){
            throw new IllegalArgumentException("'criteria' cannot be null");
        }
        this.criteria = criteria;
    }

    public Session getSession() {
        return session;
    }

    public Criteria getCriteria() {
        return criteria;
    }

    public Criteria addOrder(String orderBy, boolean asc){
        Order order = asc ? Order.asc(orderBy) : Order.desc(orderBy);
        return getCriteria().addOrder(order);
    }

    public Criteria addLimit(int start, int limit){
        return getCriteria().setFirstResult(start).setMaxResults(limit);
    }

    public Criteria getCount() {
        return getCriteria().setProjection(Projections.rowCount());
    }

}
