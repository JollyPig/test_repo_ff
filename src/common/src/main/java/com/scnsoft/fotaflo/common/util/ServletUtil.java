package com.scnsoft.fotaflo.common.util;

import org.apache.log4j.Logger;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.net.URL;

public class ServletUtil {
    protected final static Logger logger = Logger.getLogger(ServletUtil.class);

    public static String getContextPath(HttpServletRequest request){
        try{
            URL url = new URL(request.getScheme(), request.getServerName(), request.getServerPort(), request.getContextPath());
            return url.toString();
        }catch(MalformedURLException e){
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    public static String getContextPath(){
        HttpServletRequest request = getRequest();
        String path = "";

        if(request != null){
            path = getContextPath(request);
        }
        return path;
    }

    public static String getRequestMapping(){
        HttpServletRequest request = getRequest();
        String mapping = "";

        if(request != null){
            mapping = request.getRequestURI();
        }

        return mapping;
    }

    public static HttpServletRequest getRequest(){
        ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest req = sra.getRequest();

        return req;
    }

}
