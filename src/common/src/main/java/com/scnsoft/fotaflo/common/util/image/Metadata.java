package com.scnsoft.fotaflo.common.util.image;

public class Metadata {

    private String title;
    private String copyright;
    private String comment;

    public Metadata() {
    }

    public Metadata(String title, String copyright, String comment) {
        this.title = title;
        this.copyright = copyright;
        this.comment = comment;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCopyright() {
        return copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
