package com.scnsoft.fotaflo.common.analytics.ga;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

@Component
public class Tracker {
    protected final Logger logger = Logger.getLogger(Tracker.class);

    @Value("${trackingCode}")
    private String trackingId;

    public String getTrackingId() {
        return trackingId;
    }

    public void trackEvent(String clientId, String category, String action, String label, Integer value) {
        EventPayload data = createPayloadData(clientId, category, action, label, value);

        makeRequest(data);
    }

    private EventPayload createPayloadData(String clientId, String category, String action, String label, Integer value) {
        EventPayload data = new EventPayload(getTrackingId(), clientId);
        data.setEvent(category, action, label, value);

        return data;
    }

    public void makeRequest(Payload data){
        if(data == null){
            throw new IllegalArgumentException("'data' cannot be null");
        }
        List<String> urls = data.buildUrls();

        for (String url: urls){
            dispatchRequest(url);
        }
    }

    @Async
    protected void dispatchRequest(String argURL) {
        try {
            URL url = new URL(argURL);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("GET");
            connection.setInstanceFollowRedirects(true);
            connection.connect();
            int responseCode = connection.getResponseCode();
            if (responseCode != HttpURLConnection.HTTP_OK) {
                logger.error("Error requesting url '{" + argURL + "}', received response code {" + responseCode + "}");
            } else {
                if(logger.isTraceEnabled()){
                    logger.trace("Tracking success for url '{" + argURL + "}'");
                }
            }
        } catch (Exception e) {
            logger.error("Error making tracking request:" + e.getMessage());
        }
    }
}
