package com.scnsoft.fotaflo.common.util;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class JsonMapper {
    protected static final Logger logger = Logger.getLogger(JsonMapper.class);

    public static String getJson(Object data){
        String result = "";
        if(data != null){
            try {
                result = new ObjectMapper().writeValueAsString(data);
            }catch (JsonMappingException e){
                logger.error(e.getMessage());
            }catch (IOException e){
                logger.error(e.getMessage());
            }
        }

        return result;
    }

    public static Map fromJson(String data){
        Map result = new HashMap();
        if(data != null){
            try {
                result = new ObjectMapper().readValue(data, Map.class);
            }catch (JsonMappingException e){
                logger.error(e.getMessage());
            }catch (IOException e){
                logger.error(e.getMessage());
            }
        }

        return result;
    }

    public static <T> T fromJson(String data, Class<T> cls){
        T result = null;
        if(data != null && cls != null){
            try {
                result = new ObjectMapper().readValue(data, cls);
            }catch (JsonMappingException e){
                logger.error(e.getMessage());
                System.out.println(e.getMessage());
            }catch (IOException e){
                logger.error(e.getMessage());
                System.out.println(e.getMessage());
            }
        }

        return result;
    }

    public static <T> T fromJson(Object data, Class<T> cls){
        String json = null;

        if(data != null){
            json = data.toString();
        }

        return fromJson(json, cls);
    }

    public static Map fromJsonUrl(URL url){
        Map result = new HashMap();
        if(url != null){
            try {
                result = new ObjectMapper().readValue(url, Map.class);
            }catch (JsonMappingException e){
                logger.error(e.getMessage());
            }catch (IOException e){
                logger.error(e.getMessage());
            }
        }

        return result;
    }

}
