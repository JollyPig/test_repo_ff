package com.scnsoft.fotaflo.common.paypal;

import com.scnsoft.fotaflo.common.util.NumberUtils;
import com.scnsoft.fotaflo.common.util.StringUtils;
import org.apache.log4j.Logger;
import urn.ebay.api.PayPalAPI.*;
import urn.ebay.apis.CoreComponentTypes.BasicAmountType;
import urn.ebay.apis.eBLBaseComponents.*;

import java.util.*;

/**
 *  Template implements PayPal Express Checkout (EC) requests for registering and complete payments. *
 * */
public class PayPalExpressCheckoutTemplate implements PayPal {
    protected final static Logger logger = Logger.getLogger(PayPalExpressCheckoutTemplate.class);

    private PayPalAPIInterfaceServiceService service = null;

    private String redirectUrl = null;

    public PayPalExpressCheckoutTemplate(PayPalAccount account) {
        this(account, null);
    }

    public PayPalExpressCheckoutTemplate(PayPalAccount account, PayPalConfigRepository configRepository) {
        if(account == null){
            throw new IllegalArgumentException("'account' cannot be null");
        }

        if(configRepository == null){
            configRepository = new PayPalConfigRepository();
        }

        service = new PayPalAPIInterfaceServiceService(configRepository.getAccountConfigProperties(account));
        redirectUrl = configRepository.getRedirectUrl(account);
    }

    /**
     *  Register payment.
     *
     *  @param amount total amount for current payment
     *  @param confirmRedirectUrl redirect URL after confirmation payment
     *  @param cancelRedirectUrl redirect URL after cancelling payment
     *
     *  @return PaymentResult
     * */
    @Override
    public PaymentResult requestPayment(PaymentAmount amount, String confirmRedirectUrl, String cancelRedirectUrl){
        PaymentResult result = new PaymentResult();

        if(amount == null){
            throw new IllegalArgumentException("'Amount' cannot be null");
        }

        if(StringUtils.isEmpty(confirmRedirectUrl) || StringUtils.isEmpty(cancelRedirectUrl)){
            throw new IllegalArgumentException("Parameters 'confirmRedirectUrl' and 'cancelRedirectUrl' cannot be empty");
        }

        String invoiceId = generateInvoiceId();

        SetExpressCheckoutResponseType expressCheckoutResponse = sendExpressCheckoutRequest(amount, invoiceId, confirmRedirectUrl, cancelRedirectUrl);

        if (expressCheckoutResponse != null){
            if (isResponseSuccessful(expressCheckoutResponse)) {
                String token = expressCheckoutResponse.getToken();
                String redirectUrl = getPaypalRedirectUrl() + token + "&useraction=commit";
                result.setSuccess(true);
                result.setToken(token);
                result.setInvoiceId(invoiceId);
                result.setRedirectUrl(redirectUrl);
                logger.info("PayPal redirect URL: " + redirectUrl);
            } else {
                throw new PaymentException("PayPal response error:" + getResponseError(expressCheckoutResponse));
            }
        }else{
            throw new PayPalException("Error on getting EC details: 'expressCheckoutResponse' is empty");
        }

        return result;
    }

    /**
     *  Complete payment.
     *
     *  @param token payment token that application gets after redirection
     *
     *  @return PaymentResult
     * */
    @Override
    public PaymentResult completePayment(String token){
        PaymentResult result = new PaymentResult();

        if(StringUtils.isEmpty(token)){
            throw new IllegalArgumentException("'Token' cannot be null");
        }

        GetExpressCheckoutDetailsResponseType detailsResponseType = getExpressCheckoutDetails(token);

        if(detailsResponseType == null){
            throw new PayPalException("Error on getting EC details: 'detailsResponseType' is empty");
        }
        List<PaymentDetailsType> paymentDetailsList = detailsResponseType.getGetExpressCheckoutDetailsResponseDetails().getPaymentDetails();
        if(paymentDetailsList == null || paymentDetailsList.isEmpty()){
            throw new PayPalException("Error on getting EC details: 'paymentDetailsList' is empty");
        }
        PaymentDetailsType paymentDetailsType = paymentDetailsList.get(0);
        BasicAmountType basicAmountType = paymentDetailsType.getOrderTotal();
        if(basicAmountType == null){
            throw new PayPalException("Error on getting EC details: 'basicAmountType' is empty");
        }
        String payerId = detailsResponseType.getGetExpressCheckoutDetailsResponseDetails().getPayerInfo().getPayerID();

        PaymentAmount.Currency currency = PaymentAmount.Currency.valueOf(basicAmountType.getCurrencyID().getValue());
        Double totalAmount = NumberUtils.getDouble(basicAmountType.getValue());
        PaymentAmount amount = new PaymentAmount(totalAmount, currency);

        String invoiceId = paymentDetailsType.getPaymentRequestID();

        DoExpressCheckoutPaymentResponseType expressCheckoutPaymentResponse = doExpressCheckout(amount, invoiceId, token, payerId);

        DoCaptureResponseType captureResponse = null;

        if (isResponseSuccessful(expressCheckoutPaymentResponse)) {

            if (expressCheckoutPaymentResponse
                    .getDoExpressCheckoutPaymentResponseDetails()
                    .getPaymentInfo() != null) {
                List<PaymentInfoType> paymentInfoList = expressCheckoutPaymentResponse
                        .getDoExpressCheckoutPaymentResponseDetails()
                        .getPaymentInfo();

                PaymentInfoType paymentInfoType = null;
                if(paymentInfoList != null && !paymentInfoList.isEmpty()){
                    paymentInfoType = paymentInfoList.get(0);
                }

                if(paymentInfoType != null){
                    String transactionId = paymentInfoType.getTransactionID();
                    captureResponse = doCapture(amount, transactionId);

                    if (isResponseSuccessful(captureResponse)) {

                        // Authorization identification number
                        logger.info("Authorization ID:"
                                + captureResponse.getDoCaptureResponseDetails().getAuthorizationID());

                        result.setSuccess(true);
                        result.setToken(token);
                        result.setPayerId(payerId);
                        result.setTransactionId(transactionId);

                    }else {
                        throw new PaymentException("PayPal response error:" + getResponseError(expressCheckoutPaymentResponse));
                    }
                }else{
                    throw new PayPalException("Error on getting EC details: 'paymentInfoType' is empty");
                }
            }
        }else {
            throw new PaymentException("PayPal response error:" + getResponseError(expressCheckoutPaymentResponse));
        }

        return result;
    }

    /* build amount payment details for EC */
    protected List<PaymentDetailsType> getPaymentDetailsList(List<PaymentAmount> amounts, String invoiceID) {
        // ### Payment Information
        // list of information about the payment
        List<PaymentDetailsType> paymentDetailsList = new ArrayList<PaymentDetailsType>();

        // information about the first payment
        PaymentDetailsType paymentDetails;
        BasicAmountType amountType;
        SellerDetailsType sellerDetails;

        if(amounts != null){
            for(PaymentAmount amount: amounts){
                paymentDetails = new PaymentDetailsType();

                paymentDetails.setOrderTotal(getBasicAmount(amount));
                paymentDetails.setPaymentAction(PaymentActionCodeType.ORDER);

                // Unique identifier for the merchant. For parallel payments, this field
                // is required and must contain the Payer Id or the email address of the
                // merchant.
                sellerDetails = new SellerDetailsType();
                paymentDetails.setSellerDetails(sellerDetails);

                // A unique identifier of the specific payment request, which is
                // required for parallel payments.
                paymentDetails.setPaymentRequestID(invoiceID);

                paymentDetailsList.add(paymentDetails);
            }
        }

        return paymentDetailsList;
    }

    /* get EC payment details */
    protected GetExpressCheckoutDetailsResponseType getExpressCheckoutDetails(String token) {
        // ## GetExpressCheckoutDetailsReq
        GetExpressCheckoutDetailsReq expressCheckoutDetailsReq = new GetExpressCheckoutDetailsReq();

        // A timestamped token, the value of which was returned by
        // `SetExpressCheckout` response.
        GetExpressCheckoutDetailsRequestType expressCheckoutDetailsRequest = new GetExpressCheckoutDetailsRequestType(token);

        expressCheckoutDetailsReq.setGetExpressCheckoutDetailsRequest(expressCheckoutDetailsRequest);

        // ## Creating service wrapper object
        // Creating service wrapper object to make API call and loading
        // configuration file for your credentials and endpoint
        PayPalAPIInterfaceServiceService service = getPayPalAPIService();

        GetExpressCheckoutDetailsResponseType expressCheckoutDetailsResponse = null;
        try {
            // ## Making API call
            // Invoke the appropriate method corresponding to API in service
            // wrapper object
            expressCheckoutDetailsResponse = service.getExpressCheckoutDetails(expressCheckoutDetailsReq);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
        }

        if (isResponseSuccessful(expressCheckoutDetailsResponse)) {
            // Unique PayPal Customer Account identification number. This
            // value will be null unless you authorize the payment by
            // redirecting to PayPal after `SetExpressCheckout` call.
            logger.info("PayerID : "
                    + expressCheckoutDetailsResponse
                    .getGetExpressCheckoutDetailsResponseDetails()
                    .getPayerInfo().getPayerID());

        }else {
            throw new PaymentException("PayPal response error:" + getResponseError(expressCheckoutDetailsResponse));
        }

        return expressCheckoutDetailsResponse;
    }

    /* first stage for EC: register payment and get redirect URL for customer */
    protected SetExpressCheckoutResponseType sendExpressCheckoutRequest(PaymentAmount amount, String invoiceId,
                                                                        String confirmRedirectUrl, String cancelRedirectUrl) {
        // ## SetExpressCheckoutReq
        SetExpressCheckoutRequestDetailsType expressCheckoutRequestDetails = new SetExpressCheckoutRequestDetailsType();

        expressCheckoutRequestDetails.setReturnURL(confirmRedirectUrl);
        expressCheckoutRequestDetails.setCancelURL(cancelRedirectUrl);

        List<PaymentAmount> amounts = new ArrayList<PaymentAmount>();
        amounts.add(amount);

        List<PaymentDetailsType> paymentDetailsList = getPaymentDetailsList(amounts, invoiceId);
        expressCheckoutRequestDetails.setPaymentDetails(paymentDetailsList);

        SetExpressCheckoutReq setExpressCheckoutReq = new SetExpressCheckoutReq();
        SetExpressCheckoutRequestType setExpressCheckoutRequest = new SetExpressCheckoutRequestType(expressCheckoutRequestDetails);

        setExpressCheckoutReq.setSetExpressCheckoutRequest(setExpressCheckoutRequest);

        // ## Creating service wrapper object
        // Creating service wrapper object to make API call and loading
        // configuration file for your credentials and endpoint
        PayPalAPIInterfaceServiceService service = getPayPalAPIService();

        SetExpressCheckoutResponseType setExpressCheckoutResponse = null;
        try {
            // ## Making API call
            // Invoke the appropriate method corresponding to API in service
            // wrapper object
            setExpressCheckoutResponse = service.setExpressCheckout(setExpressCheckoutReq);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return setExpressCheckoutResponse;
    }

    /* second stage for EC: complete the payment */
    protected DoExpressCheckoutPaymentResponseType doExpressCheckout(PaymentAmount amount, String invoiceId, String token, String PayerId) {
        // ## DoExpressCheckoutPaymentReq
        DoExpressCheckoutPaymentReq doExpressCheckoutPaymentReq = new DoExpressCheckoutPaymentReq();

        DoExpressCheckoutPaymentRequestDetailsType doExpressCheckoutPaymentRequestDetails = new DoExpressCheckoutPaymentRequestDetailsType();

        doExpressCheckoutPaymentRequestDetails.setToken(token);

        // Unique paypal buyer account identification number as returned in
        // `GetExpressCheckoutDetails` Response
        doExpressCheckoutPaymentRequestDetails.setPayerID(PayerId);

        List<PaymentAmount> amounts = new ArrayList<PaymentAmount>();
        amounts.add(amount);
        List<PaymentDetailsType> paymentDetailsList = getPaymentDetailsList(amounts, invoiceId);

        doExpressCheckoutPaymentRequestDetails
                .setPaymentDetails(paymentDetailsList);
        DoExpressCheckoutPaymentRequestType doExpressCheckoutPaymentRequest = new DoExpressCheckoutPaymentRequestType(
                doExpressCheckoutPaymentRequestDetails);
        doExpressCheckoutPaymentReq
                .setDoExpressCheckoutPaymentRequest(doExpressCheckoutPaymentRequest);

        PayPalAPIInterfaceServiceService service = getPayPalAPIService();

        DoExpressCheckoutPaymentResponseType doExpressCheckoutPaymentResponse = null;
        try {
            // ## Making API call
            // Invoke the appropriate method corresponding to API in service
            // wrapper object
            doExpressCheckoutPaymentResponse = service
                    .doExpressCheckoutPayment(doExpressCheckoutPaymentReq);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return doExpressCheckoutPaymentResponse;
    }

    /* finish payment forthly */
    protected DoCaptureResponseType doCapture(PaymentAmount amount, String token) {

        // ## DoCaptureReq
        DoCaptureReq doCaptureReq = new DoCaptureReq();

        // `DoCaptureRequest` which takes mandatory params:
        //
        // * `Authorization ID` - Authorization identification number of the
        // payment you want to capture. This is the transaction ID returned from
        // DoExpressCheckoutPayment, DoDirectPayment, or CheckOut. For
        // point-of-sale transactions, this is the transaction ID returned by
        // the CheckOut call when the payment action is Authorization.
        // * `amount` - Amount to capture
        // * `CompleteCode` - Indicates whether or not this is your last capture.
        // It is one of the following values:
        //  * Complete – This is the last capture you intend to make.
        //  * NotComplete – You intend to make additional captures.
        // `Note:
        // If Complete, any remaining amount of the original authorized
        // transaction is automatically voided and all remaining open
        // authorizations are voided.`
        DoCaptureRequestType doCaptureRequest = new DoCaptureRequestType(token, getBasicAmount(amount), CompleteCodeType.COMPLETE);

        doCaptureReq.setDoCaptureRequest(doCaptureRequest);

        // ## Creating service wrapper object
        // Creating service wrapper object to make API call and loading
        // configuration file for your credentials and endpoint
        PayPalAPIInterfaceServiceService service = getPayPalAPIService();

        DoCaptureResponseType doCaptureResponse = null;
        try {
            // ## Making API call
            // Invoke the appropriate method corresponding to API in service
            // wrapper object
            doCaptureResponse = service.doCapture(doCaptureReq);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
        }

        return doCaptureResponse;
    }

    private BasicAmountType getBasicAmount(PaymentAmount amount){
        CurrencyCodeType currencyCodeType = amount.getCurrency().equals(PaymentAmount.Currency.CAD) ? CurrencyCodeType.CAD : CurrencyCodeType.USD;
        BasicAmountType amountType = new BasicAmountType(currencyCodeType, String.valueOf(amount.getAmount()));

        return amountType;
    }

    protected PayPalAPIInterfaceServiceService getPayPalAPIService() {
        return service;
    }

    protected String getPaypalRedirectUrl(){
        return redirectUrl;
    }

    private String getResponseError(AbstractResponseType response){
        StringBuilder sb = new StringBuilder();
        if(response != null){
            for(ErrorType error: response.getErrors()){
                sb.append(error.getLongMessage());
                sb.append("; ");
            }
        }

        return sb.toString();
    }

    private boolean isResponseSuccessful(AbstractResponseType response){
        return response.getAck().getValue().equalsIgnoreCase("success");
    }

    /* generate random string (InvoiceID in payment details) */
    protected String generateInvoiceId(){
        return String.valueOf(System.currentTimeMillis());
    }

}
