package com.scnsoft.fotaflo.common.social.twitter;

import org.apache.log4j.Logger;
import org.springframework.core.io.Resource;
import twitter4j.IDs;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;

import java.io.IOException;
import java.util.List;

public class TwitterTemplate {
    protected static final Logger logger = Logger.getLogger(TwitterTemplate.class);

    private Twitter twitter;

    public TwitterTemplate(Twitter twitter) {
        if(twitter == null){
            throw new IllegalArgumentException("Twitter service cannot be null");
        }
        this.twitter = twitter;
    }

    public boolean isAuthorized(){
        return this.twitter.getAuthorization().isEnabled();
    }

    public void shareImage(String caption, Resource image, List<String> tags, List<String> hashtags, String address){
        try{
            /*ImageUpload upload = new ImageUploadFactory().getInstance(twitter.getAuthorization());
            upload.upload(image.getFile(), caption);*/
            if(tags != null && !tags.isEmpty()){
                for(String h: tags){
                    caption += " @" + h;
                }
            }
            if(hashtags != null && !hashtags.isEmpty()){
                for(String h: hashtags){
                    caption += " #" + h;
                }
            }
            StatusUpdate status = new StatusUpdate(caption);
            status.setMedia(image.getFile());
            status.setPlaceId(this.getPlace(address));
            twitter.tweets().updateStatus(status);
        }catch(IOException e){
            logger.error(e.getMessage());
            throw new RuntimeException(e);
        }catch(TwitterException e){
            logger.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public String getUserName(){
        String screenName = null;
        try {
            screenName = twitter.getAccountSettings().getScreenName();
        }catch (TwitterException e){
            logger.error(e.getMessage());
            throw new RuntimeException(e);
        }
        return screenName;
    }

    public int getFriendAndFollowersCount(){
        int count = 0;
        IDs followers, friends;
        try{
            followers = twitter.friendsFollowers().getFollowersIDs(-1);
            if(followers != null && followers.getIDs() != null){
                logger.info("followers: "+followers.getIDs().length + ", " + followers.hasNext());
                count += followers.getIDs().length;
            }
            friends = twitter.friendsFollowers().getFriendsIDs(-1);
            if(friends != null && friends.getIDs() != null){
                logger.info("friends: "+friends.getIDs().length + ", " + friends.hasNext());
                count += friends.getIDs().length;
            }
        }catch(TwitterException e){
            logger.error(e.getMessage());
            throw new RuntimeException(e);
        }

        return count;
    }

    // TODO доработать поиск мест
    public String getPlace(String place){
        return place;
    }
}
