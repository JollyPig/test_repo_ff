package com.scnsoft.fotaflo.common.analytics.ga;

public class EventPayload extends Payload {
    public final static String HIT_TYPE = "event";

    protected static final String eventCategoryParam    = "ec";
    protected static final String eventActionParam      = "ea";
    protected static final String eventLabelParam       = "el";
    protected static final String eventValueParam       = "ev";

    public EventPayload(String trackingId, String clientId) {
        super(trackingId, clientId, HIT_TYPE);
    }

    public EventPayload(String protocolVersion, String trackingId, String clientId) {
        super(protocolVersion, trackingId, clientId, HIT_TYPE);
    }

    public void setEvent(String category, String action, String label, Integer value) {
        setEventCategory(category);
        setEventAction(action);
        setEventLabel(label);
        setEventValue(value);
    }

    public String getEventCategory() {
        return getParameter(eventCategoryParam);
    }

    public void setEventCategory(String eventCategory) {
        addParameter(eventCategoryParam, eventCategory);
    }

    public String getEventAction() {
        return getParameter(eventActionParam);
    }

    public void setEventAction(String eventAction) {
        addParameter(eventActionParam, eventAction);
    }

    public String getEventLabel() {
        return getParameter(eventLabelParam);
    }

    public void setEventLabel(String eventLabel) {
        addParameter(eventLabelParam, eventLabel);
    }

    public Integer getEventValue() {
        return (Integer)getRawParameter(eventValueParam);
    }

    public void setEventValue(Integer eventValue) {
        addParameter(eventValueParam, eventValue);
    }
}
