package com.scnsoft.fotaflo.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class PropertiesUtil {

    private static Map<String, Properties> propertyStorage = new HashMap<String, Properties>();

    public static Properties loadClasspathProperties(String path) throws IOException {
        InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);

        Properties result = loadProperties(in);

        return result;
    }

    public static Properties getClasspathProperties(String path) throws IOException {
        Properties properties = null;

        if(propertyStorage.containsKey(path)){
            properties = propertyStorage.get(path);
        }else {
            properties = loadClasspathProperties(path);
            if(properties != null){
                propertyStorage.put(path, properties);
            }
        }

        return properties;
    }

    public static Properties loadProperties(String path) throws IOException {
        File file = new File(path);
        if(file.exists()){
             return loadProperties(new FileInputStream(file));
        }

        return null;
    }

    public static Properties loadProperties(InputStream in) throws IOException {
        Properties properties = new Properties();

        properties.load(in);

        in.close();

        return properties;
    }

}
