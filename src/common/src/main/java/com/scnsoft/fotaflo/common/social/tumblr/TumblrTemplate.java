package com.scnsoft.fotaflo.common.social.tumblr;

import org.apache.log4j.Logger;
import org.springframework.core.io.Resource;
import org.springframework.social.tumblr.api.Followers;
import org.springframework.social.tumblr.api.ModifyPhotoPost;
import org.springframework.social.tumblr.api.Tumblr;
import org.springframework.social.tumblr.api.UserInfoBlog;

import java.util.ArrayList;
import java.util.List;

public class TumblrTemplate {
    protected static final Logger logger = Logger.getLogger(TumblrTemplate.class);

    public static final String BLOG_DOMAIN = ".tumblr.com";

    private Tumblr tumblr;

    public TumblrTemplate(Tumblr tumblr) {
        if(tumblr == null){
            throw new IllegalArgumentException("Tumblr service cannot be null");
        }
        this.tumblr = tumblr;
    }

    public boolean isAuthorized(){
        boolean result = false;
        try{
            if(getUserId() != null){
                result = true;
            }
        }catch(Exception e){
        }

        return result;
    }

    public String getUserId(){
        return tumblr.userOperations().info().getName();
    }

    public String getBlogHostName(){
        return this.getUserId() + BLOG_DOMAIN;
    }

    public void shareImage(String caption, Resource image){
        List<Resource> images = new ArrayList<Resource>(1);
        images.add(image);

        this.shareImages(caption, images, null);
    }

    public void shareImages(String caption, List<Resource> images, List<String> tags){
        ModifyPhotoPost post = new ModifyPhotoPost();
        post.setData(images);
        post.setCaption(caption);
        post.setTags(tags);

        String blogHostName = this.getBlogHostName();
        tumblr.blogOperations(blogHostName).blogPostOperations().create(post);
    }

    public int getFollowersCount(){
        String blogHostName = this.getBlogHostName();
        Followers followers = tumblr.blogOperations(blogHostName).followers();
        if(followers != null){
            logger.info(followers.getFollowers());
            return followers.getTotalFollowers();
        }
        return 0;
    }
}
