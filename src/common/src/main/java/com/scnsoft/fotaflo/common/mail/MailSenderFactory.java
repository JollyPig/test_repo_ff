package com.scnsoft.fotaflo.common.mail;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

public class MailSenderFactory {

    public JavaMailSender getMailSender(IMailConfiguration conf){
        if(conf == null || !conf.isValid()){
            throw new IllegalArgumentException("Mail configuration cannot be null or invalid");
        }
        JavaMailSenderImpl sender = new JavaMailSenderImpl();
        sender.setHost(conf.getHost());
        sender.setPort(conf.getPort());
        sender.setUsername(conf.getUsername());
        sender.setPassword(conf.getPassword());

        sender.setJavaMailProperties(conf.getJavaMailProperties());

        return sender;
    }

}
