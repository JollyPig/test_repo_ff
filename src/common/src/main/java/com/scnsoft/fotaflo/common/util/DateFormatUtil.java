package com.scnsoft.fotaflo.common.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class DateFormatUtil {

    public static final String ISO_DATETIME_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";

    public static final DateFormat ISO_DATETIME_FORMAT = new SimpleDateFormat(ISO_DATETIME_FORMAT_PATTERN);

}
