package com.scnsoft.fotaflo.common.util.image;

public enum ImageFormat {

    JPEG,PNG,GIF;

    public static boolean checkFormatSupport(String format){
        if(format != null){
            format = format.toUpperCase();
            for(ImageFormat imageFormat: ImageFormat.values()){
                if(imageFormat.toString().equals(format)){
                    return true;
                }
            }
        }

        return false;
    }

}
