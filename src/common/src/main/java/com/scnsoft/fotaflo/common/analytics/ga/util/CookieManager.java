package com.scnsoft.fotaflo.common.analytics.ga.util;

import javax.servlet.http.Cookie;

public class CookieManager {
    protected final static String clientIdCookie = "_ga";

    public static String getClientId(Cookie[] cookies){
        String[] ga;
        String clientId=null;
        for(Cookie c: cookies){
            if(clientIdCookie.equals(c.getName()) && c.getValue() != null){
                ga = c.getValue().split("[\\.]");
                if(ga.length == 4){
                    clientId = ga[2] + "." + ga[3];
                    break;
                }
            }
        }
        return clientId;
    }

}
