package com.scnsoft.fotaflo.common.util;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadUtil {
    public final static int DEFAULT_THREAD_POOL = 10;

    private ScheduledExecutorService executor = new ScheduledThreadPoolExecutor(DEFAULT_THREAD_POOL);

    public ThreadUtil() {
    }

    public ThreadUtil(ScheduledExecutorService executor) {
        this.executor = executor;
    }

    public void schedule(Runnable task, long delay, long period){
        executor.scheduleAtFixedRate(task, delay, period, TimeUnit.MILLISECONDS);
    }

}
