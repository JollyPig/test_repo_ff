package com.scnsoft.fotaflo.common.util;

import java.lang.reflect.Method;

public class ReflectionUtil {

    public static String getMethodMapping(Method method){
        StringBuilder sb = new StringBuilder();
        if(method != null){
            Class cls = method.getDeclaringClass();
            sb.append(cls.getSimpleName());
            sb.append(".");
            sb.append(method.getName());
            sb.append("(");
            if(method.getParameterTypes() != null){
                int l = method.getParameterTypes().length;
                for(Class p: method.getParameterTypes()){
                    sb.append(p.getSimpleName());
                    if(--l > 0){
                        sb.append(", ");
                    }
                }
            }
            sb.append(")");
        }
        return sb.toString();
    }

}
