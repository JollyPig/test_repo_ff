package com.scnsoft.fotaflo.common.mail;

import java.util.Properties;

/**
 * https://javamail.java.net/nonav/docs/api/com/sun/mail/smtp/package-summary.html
 * */
public class SmtpMailConfiguration extends AbstractMailConfiguration {

    private String from;
    private Class socketFactory;
    private boolean socketFactoryFallback = true;
    private boolean auth = false;
    private boolean starttls = false;
    private boolean quitwait = true;
    private boolean sendpartial = false;

    public SmtpMailConfiguration(String host) {
        super(host);
    }

    public SmtpMailConfiguration(String host, int port) {
        super(host, port);
    }

    @Override
    protected Properties getProperties() {
        Properties prop = new Properties();

        if(from != null){
            prop.put("mail.smtp.from", from);
        }
        prop.put("mail.smtp.auth", auth);
        prop.put("mail.smtp.sendpartial", sendpartial);
        if(socketFactory != null){
            prop.put("mail.smtp.socketFactory.class", socketFactory.getName());
            prop.put("mail.smtp.socketFactory.fallback", socketFactoryFallback);
        }
        prop.put("mail.smtp.starttls.enable", starttls);
        prop.put("mail.smtp.quitwait", quitwait);

        return prop;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public Class getSocketFactory() {
        return socketFactory;
    }

    public void setSocketFactory(Class socketFactory) {
        this.socketFactory = socketFactory;
    }

    public boolean isSocketFactoryFallback() {
        return socketFactoryFallback;
    }

    public void setSocketFactoryFallback(boolean socketFactoryFallback) {
        this.socketFactoryFallback = socketFactoryFallback;
    }

    public boolean isAuth() {
        return auth;
    }

    public void setAuth(boolean auth) {
        this.auth = auth;
    }

    public boolean isStarttls() {
        return starttls;
    }

    public void setStarttls(boolean starttls) {
        this.starttls = starttls;
    }

    public boolean isQuitwait() {
        return quitwait;
    }

    public void setQuitwait(boolean quitwait) {
        this.quitwait = quitwait;
    }

    public boolean isSendpartial() {
        return sendpartial;
    }

    public void setSendpartial(boolean sendpartial) {
        this.sendpartial = sendpartial;
    }
}
