package com.scnsoft.fotaflo.common.social.facebook;

import com.scnsoft.fotaflo.common.util.JsonMapper;
import com.scnsoft.fotaflo.common.util.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.core.io.Resource;
import org.springframework.social.SocialException;
import org.springframework.social.facebook.api.*;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FacebookTemplate {
    protected static final Logger logger = Logger.getLogger(FacebookTemplate.class);

    protected final static String defaultAlbumCaption = "Fotaflo Photo Album";

    protected final static String graphApiResourceUrl = "http://graph.facebook.com/?id=";

    protected final static String publishActionPermission = "publish_actions";

    private Facebook facebook;

    public FacebookTemplate(Facebook facebook) {
        if(facebook == null){
            throw new IllegalArgumentException("Facebook service cannot be null");
        }
        this.facebook = facebook;
    }

    public FacebookTemplate(String accessToken) {
        if(accessToken == null){
            throw new IllegalArgumentException("Access Token cannot be null");
        }
        this.facebook = new org.springframework.social.facebook.api.impl.FacebookTemplate(accessToken);
    }

    public Facebook getApi(){
        return this.facebook;
    }

    public boolean isAuthorized(){
        return this.getApi().isAuthorized();
    }

    public void shareImagesWithFeed(String albumCaption, String albumDescription, List<Resource> images, String message, List<String> tags, List<String> hashtags, String address){
        String user = this.getUserId();

        String albumId = null;
        String albumLink = null;
        Album album = this.getAlbumByUser(user, albumCaption, albumDescription);

        if(album != null){
            albumId = album.getId();
            albumLink = album.getLink();
        }

        this.postPhotos(albumId, images, convertHashtagsToString(hashtags));

        this.postFeed(user, message, albumLink, tags, hashtags, this.getPlace(address), /*Post.Privacy.SELF*/null);
    }

    public void shareImageWithFeed(String caption, Resource image, String message){
        String photoId = this.postPhoto(image, caption);

        Photo photo = this.getPhoto(photoId);

        String user = this.getUserId();

        this.postFeed(user, message, photo.getLink(), null, null, null, null);
    }

    public void shareImages(String albumCaption, String albumDescription, List<Resource> images, String message){
        String user = this.getUserId();

        String albumId = null;
        Album album = this.getAlbumByUser(user, albumCaption, albumDescription);

        if(album != null){
            albumId = album.getId();
        }

        this.postPhotos(albumId, images, message);
    }

    public void shareImagesToPage(String page, List<Resource> images, String message){
        String userId = this.getPageId(page);

        this.publishPhotos(userId, images, message);
    }

    public void shareImage(String caption, Resource image){
        this.postPhoto(image, caption);
    }

    protected Album getAlbumByUser(String user, String albumCaption, String albumDescription){
        Album album = null;

        if(StringUtils.isEmpty(albumCaption)){
            albumCaption = defaultAlbumCaption;
        }
        String albumId = this.getAlbumIdByName(user, albumCaption);
        if(albumId == null){
            try {
                albumId = this.createAlbum(albumCaption, albumDescription);
            }catch (SocialException e){
                logger.error(e.getMessage());
            }
        }

        if(albumId != null){
            album = this.getAlbum(albumId);
        }

        return album;
    }

    public int getFriendsCount(){
        List<String> list = this.getApi().friendOperations().getFriendIds();
        if(list != null){
            return list.size();
        }
        return 0;
    }

    public String getUserId(){
        return this.getApi().userOperations().getUserProfile().getId();
    }

    protected String createAlbum(String name, String description){
        return this.getApi().mediaOperations().createAlbum(name, description);
    }

    protected String getAlbumIdByName(String user, String name){
        if(name != null){
            List<Album> albums = this.getApi().mediaOperations().getAlbums(user);
            for(Album a: albums){
                if(name.equals(a.getName())){
                    return a.getId();
                }
            }
        }
        return null;
    }

    protected Album getAlbum(String id){
        return this.getApi().mediaOperations().getAlbum(id);
    }

    protected Photo getPhoto(String id){
        return this.getApi().mediaOperations().getPhoto(id);
    }

    public String postPhoto(String albumId, Resource image){
        return this.getApi().mediaOperations().postPhoto(albumId, image);
    }

    public String postPhoto(Resource image, String caption){
        return this.getApi().mediaOperations().postPhoto(image, caption);
    }

    public String postPhoto(String albumId, Resource image, String caption){
        String photoId = null;
        if(albumId != null){
            photoId = this.getApi().mediaOperations().postPhoto(albumId, image, caption);
        }else{
            photoId = postPhoto(image, caption);
        }
        return photoId;
    }

    public String publishPhoto(String id, Resource image, String caption){
        PhotoPostData data = new PhotoPostData();
        data.setSource(image);
        data.setCaption(caption);
        data.setNoStory(false);

        String url = getGraphApiUrl();
        if(!StringUtils.isEmpty(id)){
            url += id + "/photos";
        }else{
            throw new FacebookException("Parameter 'id' is empty");
//            url += "me" + "/photos";
        }
        Map<String, Object> response = this.getApi().restOperations().postForObject(url, data.toRequestParameters(), Map.class);
        return (String) response.get("id");
    }

    protected List<String> postPhotos(String albumId, List<Resource> images){
        if(images != null && images.size() > 0){
            List<String> ids = new ArrayList<String>();
            String id = null;
            for(Resource image: images){
                id = postPhoto(albumId, image);
                ids.add(id);
            }
            return ids;
        }
        return null;
    }

    protected List<String> postPhotos(String albumId, List<Resource> images, String caption){
        if(images != null && images.size() > 0){
            List<String> ids = new ArrayList<String>();
            String id = null;
            for(Resource image: images){
                id = postPhoto(albumId, image, caption);
                ids.add(id);
            }
            return ids;
        }
        return null;
    }

    protected List<String> publishPhotos(String albumId, List<Resource> images, String caption){
        if(images != null && images.size() > 0){
            List<String> ids = new ArrayList<String>();
            String id = null;
            for(Resource image: images){
                id = publishPhoto(albumId, image, caption);
                ids.add(id);
            }
            return ids;
        }
        return null;
    }

    public String postFeed(String userId, String message, String link, List<String> tags, List<String> hashtags, String placeId, Post.Privacy privacy){
        ExtendedPostData data = new ExtendedPostData(userId);

        /*if(!StringUtils.isEmpty(link)){
            data.link(link, null, null, null, null);
        }*/

        if(hashtags != null && !hashtags.isEmpty()){
            if(message == null){
                message = "";
            }
            message += convertHashtagsToString(hashtags);
        }

        if(!StringUtils.isEmpty(message)){
            data.message(message);
        }

        tags = getCheckedTags(tags);

        if(tags != null && !tags.isEmpty()){
            String[] t = new String[tags.size()];
            int i = 0;
            for(String tag: tags){
                t[i++] = tag;
            }
            data.tags(t);
        }

        if(!StringUtils.isEmpty(placeId)){
            data.place(placeId);
        }

        if(privacy != null){
            data.privacy(privacy);
        }

        if(data.isEmpty()){
            return null;
        }

        String feedId = null;
        try{
            feedId = this.getApi().feedOperations().post(data);
        }catch(Exception e){
            logger.error("Error posting feed: " + e.getMessage());
        }

        return feedId;
    }

    private String convertHashtagsToString(List<String> hashtags){
        String result = "";
        if(hashtags != null && !hashtags.isEmpty()){
            for(String h: hashtags){
                result += " #" + h;
            }
        }
        return result;
    }

    private List<String> getCheckedTags(List<String> tags){
        List<String> list = new ArrayList<String>();
        String page;
        if(tags != null && !tags.isEmpty()){
            for(String tag: tags){
                page = getPageId(tag);
                if(page != null){
                    list.add(page);
                }
            }
        }

        return list;
    }

    public boolean isTag(String tag){
        if(tag == null){
           return false;
        }
        return getPageId(tag) != null;
    }

    protected String getPageId(String name){
        String id = null;
        try{
            Page page = this.getApi().pageOperations().getPage(name);
            id = page.getId();
        }catch(SocialException e){
            logger.warn(e.getMessage());
        }
        if(id == null){
            id = getPageByGraphId(name);
        }
        return id;
    }

    protected String getPlace(String place){
        if(place == null){
            return null;
        }
        if(place.matches("\\d+")){
            return place;
        }

        // TODO
        /*place = "Canada";

        List<Page> pages = this.getApi().placesOperations().search(place, 49.652, -109.521, 12000);
        System.out.println(pages);
        String o = "";
        for(Page p: pages){
            o += "======================"+'\n';
            o += p.getId() +'\n';
            o += p.getName() +'\n';
            o += p.getLocation().getCountry() +'\n';
            o += p.getLocation().getCity() +'\n';
            o += p.getLocation().getStreet() +'\n';
            o += p.getLocation().getDescription() +'\n';
        }
        System.out.println(o);
        if(pages != null && !pages.isEmpty()){
            return pages.get(0).getId();
        }*/
        return null;
    }

    public boolean isPageExisted(String tag){
        String id = getPageByGraphId(tag);

        return !StringUtils.isEmpty(id);
    }

    protected String getPageByGraphId(String page){
        if(!StringUtils.isEmpty(page)){
            try{
                Map<String, String> result = JsonMapper.fromJsonUrl(new URL(graphApiResourceUrl + page));
                if(result != null && !result.isEmpty()){
                    String id = result.get("id");
                    if(!StringUtils.isEmpty(id)){
                        return id;
                    }
                }
            }catch (IOException e){
                logger.error(e.getMessage());
            }
        }

        return null;
    }

    public void removePermission(String permission){
        String url = getGraphApiUrl() + "me/permissions/" + permission;
        try {
            this.getApi().restOperations().delete(url);
        }catch (Exception e){
            logger.warn(e.getMessage());
        }
    }

    public void removePublishPermission(){
        removePermission(publishActionPermission);
    }

    public void revokeLogin(){
        String url = getGraphApiUrl() + "me/permissions";
        try {
            this.getApi().restOperations().delete(url);
        }catch (Exception e){
            logger.warn(e.getMessage());
        }
    }

    public boolean hasPermission(String permission){
        String name;
        if(!StringUtils.isEmpty(permission)){
            try {
                for (Permission p: getPermissions()){
                    if(p != null && !StringUtils.isEmpty(p.getName())){
                        name = p.getName();
                        if(name.equals(permission) && p.isGranted()){
                            return true;
                        }
                    }
                }
            }catch (Exception e){
                logger.error(e.getMessage());
            }
        }

        return false;
    }

    public boolean hasPublishPermission(){
        return hasPermission(publishActionPermission);
    }

    protected List<Permission> getPermissions(){
        return this.getApi().userOperations().getUserPermissions();
    }

    private String getGraphApiUrl(){
        return GraphApi.GRAPH_API_URL;
    }
}
