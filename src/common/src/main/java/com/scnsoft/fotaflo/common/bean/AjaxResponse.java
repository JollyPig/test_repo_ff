package com.scnsoft.fotaflo.common.bean;

import com.scnsoft.fotaflo.common.util.JsonMapper;

public class AjaxResponse {

    private boolean success;
    private String msg;
    private Object data;
    private Long total;

    public AjaxResponse() {
    }

    public AjaxResponse(String msg) {
        this.msg = msg;
    }

    public AjaxResponse(boolean success, String msg) {
        this.success = success;
        this.msg = msg;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public String toJson(){
        return JsonMapper.getJson(this);
    }
}
