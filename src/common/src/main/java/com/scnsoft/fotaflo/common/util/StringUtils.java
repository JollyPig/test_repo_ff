package com.scnsoft.fotaflo.common.util;

public class StringUtils {

    public static boolean isEmpty (final String value) {
        return value == null || value.trim ().isEmpty ();
    }

    public static boolean isNotEmpty (final String value) {
        return ! isEmpty (value);
    }

    public static String nullToNothing (final String value) {
        if (value == null) {
            return "";
        }

        return value.trim ();
    }

    public static String nothingToNull (final String value) {
        if (value == null || value.trim ().isEmpty ()) {
            return null;
        }

        return value;
    }

    public static String concat (final String... items) {
        final StringBuilder result = new StringBuilder ();

        if(items != null){
            for (final String item : items) {
                if (item != null) {
                    result.append (item);
                }
            }
        }

        return result.toString ();
    }

}
