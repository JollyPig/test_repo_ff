package com.scnsoft.fotaflo.common.util;

import org.apache.log4j.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

public class XmlMapper {
    protected static final Logger logger = Logger.getLogger(XmlMapper.class);

    public static <T> void writeObject(T o, File file){
        if(file != null && o != null){
            try {
                Marshaller jaxbMarshaller = getMarshaller(o.getClass());

                jaxbMarshaller.marshal(o, file);

            } catch (JAXBException e) {
                logger.error(e.getMessage());
            }
        }
    }

    public static <T> void writeObject(T o, OutputStream out){
        if(out != null && o != null){
            try {
                Marshaller jaxbMarshaller = getMarshaller(o.getClass());

                jaxbMarshaller.marshal(o, out);

            } catch (JAXBException e) {
                logger.error(e.getMessage());
            }
        }
    }

    public static <T> T readObject(File file, Class c){
        T result = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(c);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            result = (T)jaxbUnmarshaller.unmarshal(file);
        } catch (JAXBException e) {
            logger.error(e.getMessage());
        }
        return result;
    }

    public static <T> Object readObject(InputStream in, Class c){
        T result = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(c);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            result = (T)jaxbUnmarshaller.unmarshal(in);
        } catch (JAXBException e) {
            logger.error(e.getMessage());
        }
        return result;
    }

    protected static Marshaller getMarshaller(Class c) throws JAXBException{
        JAXBContext jaxbContext = JAXBContext.newInstance(c);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        // output pretty printed
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        return jaxbMarshaller;
    }

}
