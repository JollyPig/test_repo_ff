package com.scnsoft.fotaflo.common.social.facebook;

import com.scnsoft.fotaflo.common.util.JsonMapper;
import com.scnsoft.fotaflo.common.util.StringUtils;
import org.springframework.core.io.Resource;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PhotoPostData {

    private String caption;
    private String url;
    private Resource source;
    private List<String> tags;
    private String place;
    private boolean noStory;

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Resource getSource() {
        return source;
    }

    public void setSource(Resource source) {
        this.source = source;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public boolean isNoStory() {
        return noStory;
    }

    public void setNoStory(boolean noStory) {
        this.noStory = noStory;
    }

    public MultiValueMap<String, Object> toRequestParameters() {
        MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<String, Object>();
        if(!StringUtils.isEmpty(caption)){
            parameters.add("caption", caption);
        }
        if(source != null){
            parameters.add("source", source);
        }else if(!StringUtils.isEmpty(url)){
            parameters.add("url", url);
        }
        if(!StringUtils.isEmpty(place)){
            parameters.add("place", place);
        }
        if(noStory){
            parameters.add("no_story", "true");
        }
        if(tags != null && !tags.isEmpty()){
            List tagList = new ArrayList();
            for(final String tag: tags){
                tagList.add(new HashMap<String, String>(){{
                    put("tag_uid", tag);
                }});
            }
            parameters.add("tags", JsonMapper.getJson(tagList));
        }
        if(!StringUtils.isEmpty(caption)){
            parameters.add("caption", caption);
        }

        return parameters;
    }
}
