package com.scnsoft.fotaflo.common.dao;

import com.scnsoft.fotaflo.common.model.IEntity;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public interface IDao<Entity extends IEntity, Key extends Serializable> {

    /**
     * Creates the entity
     *
     * @param entity the entity to be created
     * @return id
     */
    Key create(Entity entity);

    /**
     * Retrieves the entity by the identifier
     *
     * @param id          the entity identifier
     * @return the entity
     */
    Entity get(Key id);

    /**
     * Get all existed entities from the database
     *
     * @return all the entities existed in the database
     */
    List<Entity> list();

    /**
     * Generic method used to retreive all objects of the specified type in the specified order.
     *
     * @param orderBy     order by property
     * @param asc         asc
     * @return the list of retreived objects
     */
    List<Entity> getAll(String orderBy, boolean asc);

    /**
     * Generic method used to retreive limited count of objects from start row.
     *
     * @param start     first row number
     * @param limit     count of objects
     * @return the list of retreived objects
     */
    List<Entity> find(int start, int limit);

    /**
     * Generic method used to count all objects.
     *
     * @return number of objects
     */
    long count();

    /**
     * Deletes the entity by the identifier
     *
     * @param id          the entity identifier
     */
    void delete(Key id);

    /**
     * Deletes the entity
     *
     * @param entity The entity to be deleted
     */
    void delete(Entity entity);

    /**
     * Deletes all the entitis
     *
     * @param objects The entities to be deleted
     */
    void deleteAll(Collection<Entity> objects);

    /**
     * Updates the entity
     *
     * @param entity the entity to be updated
     */
    void update(Entity entity);
}
