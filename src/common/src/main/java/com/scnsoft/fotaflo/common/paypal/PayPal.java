package com.scnsoft.fotaflo.common.paypal;

/**
 * PayPal Express Checkout (EC) interface for registering and complete payments.
 * */
public interface PayPal {

    /**
     *  Register payment.
     *
     *  @param amount total amount for current payment
     *  @param confirmRedirectUrl redirect URL after confirmation payment
     *  @param cancelRedirectUrl redirect URL after cancelling payment
     *
     *  @return PaymentResult
     * */
    PaymentResult requestPayment(PaymentAmount amount, String confirmRedirectUrl, String cancelRedirectUrl);

    /**
     *  Complete payment.
     *
     *  @param token payment token that application gets after redirection
     *
     *  @return PaymentResult
     * */
    PaymentResult completePayment(String token);

}
