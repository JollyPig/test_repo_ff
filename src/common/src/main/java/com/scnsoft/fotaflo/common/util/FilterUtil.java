package com.scnsoft.fotaflo.common.util;

/**
 *  Common utility for filtering content
 * */
public class FilterUtil {

    /**
     *  Tells whether or not filter string matches the source,
     *  if filter is empty returns true.
     *
     *  @param   filter
     *          filter string
     *
     *  @param   source
     *          source string
     *
     *  @return  <tt>true</tt> if filter is empty string or matches the source string
     *
     * */
    public static boolean contains(String filter, String source){
        if (StringUtils.isEmpty(filter) || (!StringUtils.isEmpty(source)
                && source.contains(filter))) {
            return true;
        }else{
            return false;
        }
    }

}
