package com.scnsoft.fotaflo.common.paypal;

import com.scnsoft.fotaflo.common.util.PropertiesUtil;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Properties;

/**
 * PayPal repository for configuration. Load configuration on creating
 * */
public class PayPalConfigRepository {
    protected final static Logger logger = Logger.getLogger(PayPalConfigRepository.class);

    protected Properties configProperties = loadConfigProperties("paypal/paypal.properties", true);
    protected Properties sandboxConfigProperties = loadConfigProperties("paypal/paypal.sandbox.properties", true);

    protected Properties loadConfigProperties(String path, boolean mandatory){
        Properties properties = null;
        try {
            properties = PropertiesUtil.getClasspathProperties(path);
        }catch(IOException e){
            if(mandatory){
                logger.fatal(e.getMessage(), e);
                throw new RuntimeException("Could not load required config properties. Check file path for classpath:" + path);
            }else{
                logger.error(e.getMessage());
            }
        }

        return properties;
    }

    public Properties getConfigProperties(boolean sandbox){
        if(sandbox){
            return sandboxConfigProperties;
        }else {
            return configProperties;
        }
    }

    public Properties getAccountConfigProperties(PayPalAccount account){
        Properties properties = getConfigProperties(account.isSandbox());

        properties.setProperty("acct1.UserName",    account.getUsername());
        properties.setProperty("acct1.Password",    account.getPassword());
        properties.setProperty("acct1.Signature",   account.getSignature());
        /*properties.setProperty("acct1.AppId", "APP-80W284485P519543T");*/

        return properties;
    }

    public String getRedirectUrl(boolean sandbox){
        Properties properties = getConfigProperties(sandbox);

        return properties.getProperty("paypal.redirect.url");
    }

    public String getRedirectUrl(PayPalAccount account){
        return getRedirectUrl(account.isSandbox());
    }
}
