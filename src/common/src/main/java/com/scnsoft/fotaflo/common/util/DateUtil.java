package com.scnsoft.fotaflo.common.util;

import org.apache.log4j.Logger;

import java.util.Calendar;
import java.util.Date;

public class DateUtil {
    protected final static Logger logger = Logger.getLogger(DateUtil.class);

    public static Date emptyTime(Date date){
        Calendar calendar = Calendar.getInstance();
        if(date != null){
            calendar.setTime(date);
        }

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }

    public static Date emptyTime(){
        return emptyTime(null);
    }

    public static Date fullTime(Date date){
        Calendar calendar = Calendar.getInstance();
        if(date != null){
            calendar.setTime(date);
        }

        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);

        return calendar.getTime();
    }

    public static Date fullTime(){
        return fullTime(null);
    }

    public static Date firstDayOfWeek(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(emptyTime(date));

        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);

        return calendar.getTime();
    }

    public static Date firstDayOfWeek(){
        return firstDayOfWeek(null);
    }

    public static Date lastDayOfWeek(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(emptyTime(date));

        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);

        return calendar.getTime();
    }

    public static Date lastDayOfWeek(){
        return lastDayOfWeek(null);
    }

    public static Date firstDayOfMonth(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(emptyTime(date));

        calendar.set(Calendar.DAY_OF_MONTH, 1);

        return calendar.getTime();
    }

    public static Date firstDayOfMonth(){
        return firstDayOfMonth(null);
    }

    public static Date lastDayOfMonth(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(emptyTime(date));

        calendar.add(Calendar.MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, 0);

        return calendar.getTime();
    }

    public static Date lastDayOfMonth(){
        return lastDayOfMonth(null);
    }

    public static Date firstDayOfYear(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(emptyTime(date));

        calendar.set(Calendar.DAY_OF_YEAR, 1);

        return calendar.getTime();
    }

    public static Date firstDayOfYear(){
        return firstDayOfYear(null);
    }

    public static Date lastDayOfYear(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(emptyTime(date));

        calendar.add(Calendar.YEAR, 1);
        calendar.set(Calendar.DAY_OF_YEAR, 0);

        return calendar.getTime();
    }

    public static Date lastDayOfYear(){
        return lastDayOfYear(null);
    }

}
