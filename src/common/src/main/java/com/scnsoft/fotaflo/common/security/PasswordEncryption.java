package com.scnsoft.fotaflo.common.security;

/**
 * Created by Tayna on 02.05.14.
 */

import javax.crypto.*;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class PasswordEncryption {
    private static final String cipher = "PBEWithMD5AndDES";

    // Salt
    private static byte[] salt = { (byte) 0xc7, (byte) 0x73, (byte) 0x21,
            (byte) 0x8c, (byte) 0x7e, (byte) 0xc8, (byte) 0xee, (byte) 0x99 };

    // Iteration count
    private static int count = 30;

    private static byte[] clearText = "Secret".getBytes();

    private Cipher pbeCipher;
    private PBEParameterSpec pbeParamSpec;

    public PasswordEncryption() throws NoSuchAlgorithmException,
            NoSuchPaddingException {
        // Create PBE parameter set
        pbeParamSpec = new PBEParameterSpec(salt, count);

        // Create PBE Cipher
        pbeCipher = Cipher.getInstance(cipher);
    }

    public byte[] encryptPassword(String password) throws Exception {
        PBEKeySpec pbeKeySpec;
        SecretKeyFactory keyFac;

        String pass = "Password";
        pbeKeySpec = new PBEKeySpec( pass.toCharArray());
        keyFac = SecretKeyFactory.getInstance(cipher);
        SecretKey pbeKey = keyFac.generateSecret(pbeKeySpec);

        byte[] cipherText = encrypt(pbeKey, password.getBytes());

        return cipherText;
    }

    public String decryptPassword(byte[] password) throws Exception {
        PBEKeySpec pbeKeySpec;
        SecretKeyFactory keyFac;

        String pass = "Password";
        pbeKeySpec = new PBEKeySpec( pass.toCharArray());
        keyFac = SecretKeyFactory.getInstance(cipher);
        SecretKey pbeKey = keyFac.generateSecret(pbeKeySpec);

        byte[] cipherText = decrypt(pbeKey, password);

        return new String(cipherText);
    }

    private byte[] encrypt(SecretKey pbeKey, byte[] data)
            throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, InvalidAlgorithmParameterException,
            IllegalBlockSizeException, BadPaddingException {
        // Initialize PBE Cipher with key and parameters
        pbeCipher.init(Cipher.ENCRYPT_MODE, pbeKey, pbeParamSpec);

        // Our cleartext

        // Encrypt the cleartext
        byte[] ciphertext = pbeCipher.doFinal(data);

        return ciphertext;
    }

    private byte[] decrypt(SecretKey pbeKey, byte[] data)
            throws InvalidKeyException, InvalidAlgorithmParameterException,
            IllegalBlockSizeException, BadPaddingException {
        // Initialize PBE Cipher with key and parameters
        pbeCipher.init(Cipher.DECRYPT_MODE, pbeKey, pbeParamSpec);

        // Our cleartext

        // Decrypt the ciphertext
        byte[] clearText = pbeCipher.doFinal(data);

        return clearText;
    }

}
