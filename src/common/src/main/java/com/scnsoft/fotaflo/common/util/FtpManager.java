package com.scnsoft.fotaflo.common.util;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class FtpManager {
    protected final static Logger logger = Logger.getLogger(FtpManager.class);

    private String server;
    private String username;
    private String password;

    public FtpManager(String server, String username, String password) {
        this.server = server;
        this.username = username;
        this.password = password;
    }

    public void upload(String dir, List<Resource> resources) {
        FTPClient ftpClient = new FTPClient();
        try {
            ftpClient.connect(server);
            ftpClient.login(username, password);
            ftpClient.enterLocalPassiveMode();

            if(!StringUtils.isEmpty(dir)){
                boolean success = ftpClient.makeDirectory(dir);
                if(success){
                    ftpClient.changeWorkingDirectory(dir);
                }
            }

            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            if (resources != null && !resources.isEmpty()) {
                String name;
                InputStream inputStream;
                boolean done;
                for (Resource resource: resources) {
                    name = resource.getFilename();
                    inputStream = resource.getInputStream();

                    done = ftpClient.storeFile(name, inputStream);
                    inputStream.close();
                    if (done) {
                        logger.info("The file " + name + "was uploaded successfully.");
                    }
                }
            }
        } catch (IOException ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                logger.error(ex.getMessage(), ex);
            }
        }
    }

}
