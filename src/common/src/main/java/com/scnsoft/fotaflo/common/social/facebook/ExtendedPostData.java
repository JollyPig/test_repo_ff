package com.scnsoft.fotaflo.common.social.facebook;

import com.scnsoft.fotaflo.common.util.JsonMapper;
import org.springframework.social.facebook.api.PostData;
import org.springframework.util.MultiValueMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExtendedPostData extends PostData {

    private Map<String, String> actions;

    public ExtendedPostData(String targetFeedId) {
        super(targetFeedId);
    }

    public Map<String, String> getActions() {
        return actions;
    }

    public void setActions(Map<String, String> actions) {
        this.actions = actions;
    }

    public void addAction(String name, String link) {
        if(actions == null){
            actions = new HashMap<String, String>();
        }

        actions.put(name, link);
    }

    public boolean isEmpty(){
        MultiValueMap<String, Object> parameters = this.toRequestParameters();
        return parameters == null || parameters.isEmpty();
    }

    @Override
    public MultiValueMap<String, Object> toRequestParameters() {
        MultiValueMap<String, Object> parameters = super.toRequestParameters();
        if(actions != null){
            List actionList = new ArrayList();
            for(final String key: actions.keySet()){
                actionList.add(new HashMap<String, String>(){{
                    put("name", key);
                    put("link", actions.get(key));
                }});
            }
            parameters.add("actions", JsonMapper.getJson(actionList));
        }

        return parameters;
    }
}
