package com.scnsoft.fotaflo.common.paypal;

/**
 * Exception on processing payment
 * */
public class PaymentException extends RuntimeException{
    public PaymentException(String message) {
        super(message);
    }

    public PaymentException() {
    }
}
