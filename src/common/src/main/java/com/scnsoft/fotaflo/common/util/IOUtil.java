package com.scnsoft.fotaflo.common.util;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.UUID;

public class IOUtil {
    protected final static Logger logger = Logger.getLogger(IOUtil.class);

    private static final int BUFFER_SIZE = 1024;
    private static final int EOF_MARK = -1;

    protected final static String tmpExtension = ".tmp";

    protected static final String tmpDirPath = System.getProperty("java.io.tmpdir");

    public static File createTemporalFile(){
        return createFile(getRandomFilename());
    }

    public static File createFile(String filepath){
        File file = null;
        try{
            file = new File(filepath);
            if(!file.exists()){
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
        }catch(IOException e){
            logger.error("Error on creating file: "+filepath, e);
            throw new RuntimeException(e);
        }
        return file;
    }

    public static boolean deleteFile(File file){
        boolean success = false;
        if(file != null){
            try{
                success = file.delete();
                if(!success){
                    throw new IOException("File '" + file + "' cannot be deleted");
                }
            }catch(IOException e){
                logger.warn("Error on deleting temporal file: " + file, e);
            }
        }
        return success;
    }

    protected static String getRandomFilename(){
        String name = UUID.randomUUID().toString();
        return tmpDirPath + name + tmpExtension;
    }

    public static int writeFromInputToOutput(InputStream source, OutputStream dest) {
        byte[] buffer = new byte[BUFFER_SIZE];
        int bytesRead = EOF_MARK;
        int count = 0;
        try {
            while ((bytesRead = source.read(buffer)) != EOF_MARK) {
                dest.write(buffer, 0, bytesRead);
                count += bytesRead;
            }
        } catch (IOException e) {
            logger.info(e.getMessage());
            logger.debug(e.getMessage(), e);
        }
        return count;
    }

    public static URI getURI(String uri) throws MalformedURLException, URISyntaxException {
        URL url = new URL(uri);
        return new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
    }

    public static URL getValidURL(URL url) throws MalformedURLException, URISyntaxException {
        return new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef()).toURL();
    }

}
