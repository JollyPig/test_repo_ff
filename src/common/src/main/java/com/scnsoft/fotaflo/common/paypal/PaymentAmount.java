package com.scnsoft.fotaflo.common.paypal;

/**
 * Total payment amount
 * */
public class PaymentAmount {

    private Double amount;
    private Currency currency;

    public PaymentAmount(Double amount, Currency currency) {
        if(amount == null || amount < 0){
            throw new IllegalArgumentException("'Amount' must be correct double value");
        }

        if(currency == null){
            throw new IllegalArgumentException("'Currency' cannot be null");
        }

        this.amount = amount;
        this.currency = currency;
    }

    /**
     * Supported currencies list
     * */
    public enum Currency{
        USD,CAD
    }

    public Double getAmount() {
        return amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    @Override
    public String toString() {
        return "Total amount: {" + currency + " - " +amount + '}';
    }
}
