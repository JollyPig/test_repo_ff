package com.scnsoft.fotaflo.common.mail;

import java.util.Properties;

public interface IMailConfiguration {
    String getHost();

    int getPort();

    String getUsername();

    String getPassword();

    Properties getJavaMailProperties();

    boolean isValid();
}
