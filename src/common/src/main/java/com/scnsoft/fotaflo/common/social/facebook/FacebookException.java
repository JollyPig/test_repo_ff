package com.scnsoft.fotaflo.common.social.facebook;

import org.springframework.social.UncategorizedApiException;

public class FacebookException extends UncategorizedApiException {
    public FacebookException(String message) {
        this(message, null);
    }
    public FacebookException(String message, Throwable cause) {
        super("facebook", message, cause);
    }
}
