package com.scnsoft.fotaflo.common.dao;

import com.scnsoft.fotaflo.common.dao.util.CriteriaBuilder;
import com.scnsoft.fotaflo.common.model.IEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

abstract public class AbstractDao<Entity extends IEntity, Key extends Serializable> implements IDao<Entity, Key> {

    @Resource(name = "sessionFactory")
    protected SessionFactory sessionFactory;

    abstract protected Class<Entity> getDomainClass();

    protected CriteriaBuilder getCriteriaBuilder(){
        return new CriteriaBuilder(getSession(), getDomainClass());
    }

    public void setSessionFactory(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    protected Session getSession(){
        return sessionFactory.getCurrentSession();
    }

    @Override
    public Key create(Entity entity) {
        return (Key) getSession().save(entity);
    }

    @Override
    public Entity get(Key id) {
        return (Entity) getSession().get(getDomainClass(), id);
    }

    @Override
    public List<Entity> list() {
        return getSession().createCriteria(getDomainClass()).list();
    }

    @Override
    public List<Entity> getAll(String orderBy, boolean asc) {
        Order order = asc ? Order.asc(orderBy) : Order.desc(orderBy);
        return getSession().createCriteria(getDomainClass())
                .addOrder(order)
                .list();
    }

    @Override
    public List<Entity> find(int start, int limit) {
        return getSession().createCriteria(getDomainClass())
                .setFirstResult(start).setMaxResults(limit)
                .list();
    }

    @Override
    public long count() {
        Criteria criteria = getSession().createCriteria(getDomainClass());
        return ((Number)criteria.setProjection(Projections.rowCount()).uniqueResult()).longValue();
    }

    @Override
    public void delete(Key id) {
        Object e = get(id);
        if(e != null){
            getSession().delete(e);
        }
    }

    @Override
    public void delete(Entity entity) {
        getSession().delete(entity);
    }

    @Override
    public void deleteAll(Collection<Entity> objects) {
        Session session = getSession();
        if(objects != null && !objects.isEmpty()){
            for (Entity e: objects){
                session.delete(e);
            }
        }
    }

    @Override
    public void update(Entity entity) {
        getSession().saveOrUpdate(entity);
    }
}
