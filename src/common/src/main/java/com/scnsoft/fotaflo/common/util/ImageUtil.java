package com.scnsoft.fotaflo.common.util;

import com.scnsoft.fotaflo.common.util.image.ImageFormat;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.*;

public class ImageUtil {
    protected final static Logger logger = Logger.getLogger(ImageUtil.class);

    public static String getImageFormat(String path){
        ImageFormat format = ImageFormat.JPEG;
        if(path != null){
            path = path.toLowerCase();

            if(path.endsWith(".jpg") || path.endsWith(".jpeg")){
                format = ImageFormat.JPEG;
            }else if(path.endsWith(".png")){
                format = ImageFormat.PNG;
            }else if(path.endsWith(".gif")){
                format = ImageFormat.GIF;
            }
        }

        return format.toString();
    }

    public String encodeImage(BufferedImage _image, String format) {
        if(!ImageFormat.checkFormatSupport(format)){
            throw new UnsupportedOperationException("Unsupported format: " + format);
        }
        String image = "";
        if (_image != null) {
            try {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                ImageIO.write(_image, format, os);
                byte[] data = os.toByteArray();
                image = Base64.encodeBase64String(data);
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }
        return image;
    }

    public static File saveImage(BufferedImage image, String format){
        if(image == null){
            throw new IllegalArgumentException("Field 'image' cannot be null");
        }
        File file = IOUtil.createTemporalFile();
        OutputStream os = null;
        try{
            os = new FileOutputStream(file);
            saveImage(image, format, os);
        }catch(IOException e){
            logger.error(e.getMessage(), e);
            return null;
        }finally {
            if(os != null){
                try{
                    os.close();
                }catch (IOException e){
                    logger.error(e.getMessage(), e);
                }
            }
        }

        return file;
    }

    public static File saveImage(BufferedImage image, String format, String path){
        if(image == null){
            throw new IllegalArgumentException("Field 'image' cannot be null");
        }
        if(path == null || path.trim().isEmpty()){
            throw new IllegalArgumentException("Field 'path' cannot be null or empty");
        }
        File file = IOUtil.createFile(path);
        OutputStream os = null;
        try{
            os = new FileOutputStream(file);
            saveImage(image, format, os);
        }catch(IOException e){
            logger.error(e.getMessage(), e);
            return null;
        }finally {
            if(os != null){
                try{
                    os.close();
                }catch (IOException e){
                    logger.error(e.getMessage(), e);
                }
            }
        }

        return file;
    }

    public static void saveImage(BufferedImage image, String format, OutputStream os) throws IOException{
        if(image == null){
            throw new IllegalArgumentException("Field 'image' cannot be null");
        }
        if(os == null){
            throw new IllegalArgumentException("Output Stream cannot be null");
        }
        if(!ImageFormat.checkFormatSupport(format)){
            throw new UnsupportedOperationException("Unsupported format: " + format);
        }

        ImageIO.write(image, format, os);
    }

    public static BufferedImage rotateImage(BufferedImage image, int angle) {
        double rads = Math.toRadians(angle);
        double sin = Math.abs(Math.sin(rads)), cos = Math.abs(Math.cos(rads));

        int w = image.getWidth(null);
        int h = image.getHeight(null);
        int newWidth = (int) Math.floor(w * cos + h * sin);
        int newHeight = (int) Math.floor(h * cos + w * sin);

        BufferedImage rotated = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = rotated.createGraphics();
        AffineTransform at = new AffineTransform();
        at.translate((newWidth - w) / 2, (newHeight - h) / 2);

        int x = w / 2;
        int y = h / 2;

        at.rotate(Math.toRadians(angle), x, y);
        g2d.setTransform(at);
        g2d.drawImage(image, 0, 0, null);
        g2d.dispose();

        return rotated;
    }

}
