package com.scnsoft.fotaflo.common.model;

import org.hibernate.annotations.GenericGenerator;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.MappedSuperclass;

/**
 * AbstractEntity.
 * Each persistance object shoud inherit AbstractEntity.
 */
@MappedSuperclass
public abstract class AbstractEntity implements IEntity {

    /**
     * The entity unique identifier
     */
    protected int id;

    /**
     * Returns the entity identifier.
     *
     * @return entity identifier
     */
    @Id
    @GeneratedValue(generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    public int getId() {
        return id;
    }

    /**
     * Sets the entity identifier
     *
     * @param id the entity identifier
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId()).toString();
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object obj) {
        if (!(obj instanceof AbstractEntity)) return false;
        AbstractEntity castObject = (AbstractEntity) obj;
        return new EqualsBuilder().append(
                this.getId(), castObject.getId()).isEquals();
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).toHashCode();
    }
}
