package com.scnsoft.fotaflo.common.util.image;

import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.ImageWriteException;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.common.ImageMetadata;
import org.apache.commons.imaging.formats.jpeg.JpegImageMetadata;
import org.apache.commons.imaging.formats.jpeg.exif.ExifRewriter;
import org.apache.commons.imaging.formats.tiff.TiffImageMetadata;
import org.apache.commons.imaging.formats.tiff.constants.ExifTagConstants;
import org.apache.commons.imaging.formats.tiff.constants.TiffTagConstants;
import org.apache.commons.imaging.formats.tiff.fieldtypes.FieldType;
import org.apache.commons.imaging.formats.tiff.taginfos.TagInfo;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputDirectory;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputField;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputSet;
import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

public class ImageMetadataManager {
    protected final static Logger logger = Logger.getLogger(ImageMetadataManager.class);

    public void applyMetadataToImage(BufferedImage image, String format, OutputStream out, Metadata metadata)
            throws IOException, ImageReadException, ImageWriteException {
        if(image == null){
            throw new IllegalArgumentException("'image' cannot be null");
        }
        if(out == null){
            throw new IllegalArgumentException("'out' cannot be null");
        }
        if(!ImageFormat.checkFormatSupport(format)){
            throw new UnsupportedOperationException("Unsupported format: " + format);
        }
        ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
        ImageIO.write(image, format, byteArray);

        applyMetadataToImage(byteArray.toByteArray(), out, metadata);

        byteArray.close();
    }

    public void applyMetadataToImage(byte[] image, OutputStream out, Metadata metadata) throws IOException, ImageReadException, ImageWriteException{
        if(image == null){
            throw new IllegalArgumentException("'image' cannot be null");
        }
        if(out == null){
            throw new IllegalArgumentException("'out' cannot be null");
        }
        ImageMetadata sourceMetadata = Imaging.getMetadata(image);
        TiffOutputSet outputSet = getTiffOutputSet(sourceMetadata);

        addMetadata(outputSet, metadata);

        new ExifRewriter().updateExifMetadataLossless(image, out, outputSet);
    }

    public void applyMetadataToImage(File image, OutputStream out, Metadata metadata) throws IOException, ImageReadException, ImageWriteException{
        if(image == null){
            throw new IllegalArgumentException("'image' cannot be null");
        }
        if(out == null){
            throw new IllegalArgumentException("'out' cannot be null");
        }
        ImageMetadata sourceMetadata = Imaging.getMetadata(image);
        TiffOutputSet outputSet = getTiffOutputSet(sourceMetadata);

        addMetadata(outputSet, metadata);

        new ExifRewriter().updateExifMetadataLossless(image, out, outputSet);
    }

    public ImageMetadata getMetadata(File file) throws IOException, ImageReadException{
        ImageMetadata metadata = Imaging.getMetadata(file);
        return metadata;
    }

    private TiffOutputSet getTiffOutputSet(ImageMetadata metadata){
        TiffOutputSet outputSet = new TiffOutputSet();
        if(metadata != null && (metadata instanceof JpegImageMetadata)){
            JpegImageMetadata jpegMetadata = (JpegImageMetadata) metadata;

            if (jpegMetadata != null) {
                TiffImageMetadata tiffMetadata = jpegMetadata.getExif();
                if (tiffMetadata != null) {
                    try{
                        outputSet = tiffMetadata.getOutputSet();
                    }catch (ImageWriteException e){
                        logger.error(e.getMessage());
                    }
                }
            }
        }

        return outputSet;
    }

    protected void addMetadata(TiffOutputSet outputSet, Metadata metadata) throws ImageWriteException{
        if(outputSet == null){
            throw new IllegalArgumentException("'outputSet' cannot be null");
        }
        TiffOutputDirectory root = outputSet.getOrCreateRootDirectory();
        TiffOutputDirectory exifDirectory = outputSet.getOrCreateExifDirectory();

        if(metadata != null){
            addMetadataTag(root, TiffTagConstants.TIFF_TAG_IMAGE_DESCRIPTION, metadata.getTitle());
            addMetadataTag(root, TiffTagConstants.TIFF_TAG_COPYRIGHT, metadata.getCopyright());
            addMetadataTag(exifDirectory, ExifTagConstants.EXIF_TAG_USER_COMMENT, metadata.getComment());
        }
    }

    private void addMetadataTag(TiffOutputDirectory directory, TagInfo tagInfo, String value) {
        TiffOutputField tag = new TiffOutputField(tagInfo, FieldType.ASCII, value.length(), value.getBytes());
        directory.removeField(tagInfo);
        directory.add(tag);
    }

}
