package com.scnsoft.fotaflo.common.paypal;

/**
 * PayPal SDK Exception
 * */
public class PayPalException extends RuntimeException {
    public PayPalException() {
    }

    public PayPalException(String message) {
        super(message);
    }
}
