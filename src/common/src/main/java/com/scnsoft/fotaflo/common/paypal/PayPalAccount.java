package com.scnsoft.fotaflo.common.paypal;

import com.scnsoft.fotaflo.common.util.StringUtils;

/**
 * PayPal Account data
 * */
public class PayPalAccount {

    private String username;
    private String password;
    private String signature;
    private boolean sandbox;

    public PayPalAccount(String username, String password, String signature, boolean sandbox) {
        if(StringUtils.isEmpty(username)){
            throw new IllegalArgumentException("'username' cannot be empty");
        }
        if(StringUtils.isEmpty(password)){
            throw new IllegalArgumentException("'password' cannot be empty");
        }
        if(StringUtils.isEmpty(signature)){
            throw new IllegalArgumentException("'signature' cannot be empty");
        }

        this.username = username;
        this.password = password;
        this.signature = signature;
        this.sandbox = sandbox;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getSignature() {
        return signature;
    }

    public boolean isSandbox() {
        return sandbox;
    }
}
