package com.scnsoft.fotaflo.common.util;

import org.apache.log4j.Logger;

public class NumberUtils {
    protected final static Logger logger = Logger.getLogger(NumberUtils.class);

    public static Integer getInteger(String value){
        Integer result = null;
        try{
            result = Integer.parseInt(value);
        }catch (NumberFormatException e){
            logger.warn(e.getMessage());
        }

        return result;
    }

    public static Long getLong(String value){
        Long result = null;
        try{
            result = Long.parseLong(value);
        }catch (NumberFormatException e){
            logger.warn(e.getMessage());
        }

        return result;
    }

    public static Float getFloat(String value){
        Float result = null;
        try{
            result = Float.parseFloat(value);
        }catch (NumberFormatException e){
            logger.warn(e.getMessage());
        }

        return result;
    }

    public static Double getDouble(String value){
        Double result = null;
        try{
            result = Double.parseDouble(value);
        }catch (NumberFormatException e){
            logger.warn(e.getMessage());
        }

        return result;
    }

}
