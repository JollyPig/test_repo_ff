package com.scnsoft.fotaflo.webportal.ws.impl;

import com.scnsoft.fotaflo.common.util.StringUtils;
import com.scnsoft.fotaflo.webportal.bean.UserBean;
import com.scnsoft.fotaflo.webportal.config.RemoteServer;
import com.scnsoft.fotaflo.webportal.ws.IUserWebService;
import org.springframework.stereotype.Service;

@Service
public class UserWebService extends AbstractWebService implements IUserWebService {
    @Override
    public String getResourcePath() {
        return null;
    }

    @Override
    public UserBean getUser(String login) {
        UserBean userBean = null;
        if(!StringUtils.isEmpty(login)){
            String path;

            for(RemoteServer server: userContextHolder.getServerList()){
                path = server.getRestPath() + RESOURCE + '/' + login.trim();
                logger.info("Path by " + server.getId() + ": "+path);
                userBean = (UserBean)getJson(client.get(path,
                        server.getPrincipal().getUsername(),
                        server.getPrincipal().getPassword()),
                        UserBean.class);

                if(userBean != null){
                    userBean.setServerNumber(server.getId());
                    break;
                }
            }
        }

        return userBean;
    }
}
