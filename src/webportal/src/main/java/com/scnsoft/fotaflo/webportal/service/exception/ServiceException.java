package com.scnsoft.fotaflo.webportal.service.exception;

public class ServiceException extends RuntimeException {

    private Object data;

    public ServiceException() {
    }

    public ServiceException(String message) {
        super(message);
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
