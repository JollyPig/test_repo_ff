package com.scnsoft.fotaflo.webportal.dao;

import com.scnsoft.fotaflo.webportal.model.SocialNetwork;

import java.util.List;
import com.scnsoft.fotaflo.common.dao.IDao;

public interface ISocialNetworkDao extends IDao<SocialNetwork, Integer> {

    SocialNetwork getByNameAndLocation(String name, Integer location, Integer server);

    List<SocialNetwork> findByLocation(Integer location, Integer server);

}
