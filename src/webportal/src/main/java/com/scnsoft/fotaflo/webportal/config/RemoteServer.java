package com.scnsoft.fotaflo.webportal.config;

import javax.xml.bind.annotation.*;

/** Remote server */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class RemoteServer {

    /** Server ID - is used for retrieving data belonging to the server */
    @XmlAttribute
    private int id;

    /** Credentials for connecting to remote server */
    @XmlElement(name = "principal")
    private Principal principal;

    /** REST WS url base path for retrieving data */
    @XmlElement(name = "rest-path")
    private String restPath;

    /** http or file path location for retrieving photos */
    @XmlElement(name = "picture-path")
    private String picturePath;

    public RemoteServer() {
    }

    public RemoteServer(int id, String restPath, String picturePath) {
        this.id = id;
        this.restPath = restPath;
        this.picturePath = picturePath;
    }

    public RemoteServer(int id, Principal principal, String restPath, String picturePath) {
        this(id, restPath, picturePath);
        this.principal = principal;
    }

    public RemoteServer(int id, String username, String password, String restPath, String picturePath) {
        this(id, new Principal(username, password), restPath, picturePath);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Principal getPrincipal() {
        return principal;
    }

    public void setPrincipal(Principal principal) {
        this.principal = principal;
    }

    public String getRestPath() {
        return restPath;
    }

    public void setRestPath(String restPath) {
        this.restPath = restPath;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }
}
