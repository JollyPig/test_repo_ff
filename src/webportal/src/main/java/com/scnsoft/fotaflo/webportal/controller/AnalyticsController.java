package com.scnsoft.fotaflo.webportal.controller;

import com.scnsoft.fotaflo.webportal.service.impl.AnalyticsService;
import com.scnsoft.fotaflo.webportal.service.IAnalyticsService;
import com.scnsoft.fotaflo.webportal.service.ILocationService;
import com.scnsoft.fotaflo.common.util.IOUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Controller
@RequestMapping("/analytics")
public class AnalyticsController {
    protected static Logger logger = Logger.getLogger( AnalyticsController.class );

    protected final static SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

    @Autowired
    private ILocationService locationService;

    @Autowired
    private IAnalyticsService analyticsService;

    /*--------- Analytics pages -------*/

    @RequestMapping(value = "/common", method = RequestMethod.GET)
    public String overview(HttpServletRequest request, HttpServletResponse response, ModelMap model){
        AnalyticParameters parameters = getAnalyticsParameters(request);

        return "overview";
    }

    @RequestMapping(value = "/device", method = RequestMethod.GET)
    public String deviceType(HttpServletRequest request, HttpServletResponse response, ModelMap model){
        AnalyticParameters parameters = getAnalyticsParameters(request);

        model.put("data", analyticsService.getVisitsByDeviceType(parameters.getStartDate(), parameters.getEndDate(),
                parameters.getLocation()));

        return "deviceType";
    }

    @RequestMapping(value = "/geography", method = RequestMethod.GET)
    public String geography(HttpServletRequest request, HttpServletResponse response, ModelMap model){
        AnalyticParameters parameters = getAnalyticsParameters(request);

        model.put("data", analyticsService.getVisitsByGeography(parameters.getStartDate(), parameters.getEndDate(), parameters.getLocation()));

        return "geography";
    }

    @RequestMapping(value = "/loyality", method = RequestMethod.GET)
    public String loyality(HttpServletRequest request, HttpServletResponse response, ModelMap model){
        AnalyticParameters parameters = getAnalyticsParameters(request);

        return "loyality";
    }

    @RequestMapping(value = "/share", method = RequestMethod.GET)
    public String share(HttpServletRequest request, HttpServletResponse response, ModelMap model){
        AnalyticParameters parameters = getAnalyticsParameters(request);

        model.put("data", analyticsService.getShareCount(parameters.getStartDate(), parameters.getEndDate(), parameters.getLocation()));

        return "sharingOverview";
    }

    @RequestMapping(value = "/purchase", method = RequestMethod.GET)
    public String purchase(HttpServletRequest request, HttpServletResponse response, ModelMap model){
        AnalyticParameters parameters = getAnalyticsParameters(request);

        return "analytics";
    }

    @RequestMapping(value = "/locations", method = RequestMethod.GET)
    public String location(HttpServletRequest request, HttpServletResponse response, ModelMap model){
        AnalyticParameters parameters = getAnalyticsParameters(request);

        return "location";
    }

    @RequestMapping(value = "/goals", method = RequestMethod.GET)
    public String goals(HttpServletRequest request, HttpServletResponse response, ModelMap model){

        return "goals";
    }

    /*------- Ajax (JSON response) ------*/

    @RequestMapping(value = "/share/graph", method = RequestMethod.GET)
    public @ResponseBody Map shareGraph(HttpServletRequest request, HttpServletResponse response, ModelMap model,
                                        @RequestParam(value = "location", required = false) Integer location,
                                        @RequestParam(value = "date", required = false) String dateRange){
        Date startDate = new Date();
        Date endDate = new Date();
        if(dateRange != null){
            String[] dates = dateRange.split(",");
            if(dates.length == 2){
                try{
                    startDate = sdf.parse(dates[0]);
                    endDate = sdf.parse(dates[1]);
                }catch(ParseException e){
                    logger.error(e.getMessage(), e);
                }
            }
        }

        Map result = analyticsService.getShareCount(startDate, endDate, location, AnalyticsService.DateRange.DAY);

        return result;
    }

    @RequestMapping(value = "/download", method = RequestMethod.POST)
    public void download(@RequestParam(value = "loaction", required = false) Integer location, @RequestParam(value = "date", required = false) String date,
                         @RequestParam(value = "range", required = false) Integer rangeType,
                           HttpServletResponse response) throws IOException {
        logger.info("location: "+location + " => " + "date: "+date + " => " + "range: "+rangeType);
        String dateRange = date;
        Date startDate = new Date();
        Date endDate = new Date();
        if(dateRange != null){
            String[] dates = dateRange.split(",");
            if(dates.length == 2){
                try{
                    startDate = sdf.parse(dates[0]);
                    endDate = sdf.parse(dates[1]);
                }catch(ParseException e){
                    logger.error(e.getMessage(), e);
                }
            }
        }

        File file = analyticsService.getShareReport(startDate, endDate, location, AnalyticsService.DateRange.DAY);

        response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
        response.addHeader("Content-Type", "application/xls");
        IOUtil.writeFromInputToOutput(new FileInputStream(file), response.getOutputStream());
        //response.getOutputStream().write(archieve);
        response.getOutputStream().close();
    }

    /*------- Other methods ------*/

    protected AnalyticParameters getAnalyticsParameters(HttpServletRequest request){
        String dateRange = request.getParameter("date");
        String location = request.getParameter("location");
        if(dateRange == null && request.getSession().getAttribute("date") != null){
            dateRange = request.getSession().getAttribute("date").toString();
        }
        Date startDate = new Date();
        Date endDate = new Date();
        if(dateRange != null){
            String[] dates = dateRange.split(",");
            if(dates.length == 2){
                try{
                    startDate = sdf.parse(dates[0]);
                    endDate = sdf.parse(dates[1]);
                }catch(ParseException e){
                    logger.error(e.getMessage(), e);
                }
            }
        }

        logger.info("location" + location);

        if(startDate != null && endDate != null){
            request.getSession().setAttribute("date", sdf.format(startDate)+","+sdf.format(endDate));
        }
        logger.info("date: "+startDate + " " + endDate);
        logger.info(request.getSession().getAttribute("date"));

        Integer locationId = null;

        if(location != null){
            locationId = new Integer(location);
        }

        request.getSession().setAttribute("location", locationId != null ? locationId : 0);

        if(request.getSession().getAttribute("locations") == null){
            request.getSession().setAttribute("locations", locationService.getLocations());
        }

        return new AnalyticParameters(startDate, endDate, locationId);
    }

    class AnalyticParameters{

        private Date startDate;
        private Date endDate;
        private Integer location;

        AnalyticParameters(Date startDate, Date endDate, Integer location) {
            this.startDate = startDate;
            this.endDate = endDate;
            this.location = location;
        }

        Date getStartDate() {
            return startDate;
        }

        void setStartDate(Date startDate) {
            this.startDate = startDate;
        }

        Date getEndDate() {
            return endDate;
        }

        void setEndDate(Date endDate) {
            this.endDate = endDate;
        }

        Integer getLocation() {
            return location;
        }

        void setLocation(Integer location) {
            this.location = location;
        }
    }

}
