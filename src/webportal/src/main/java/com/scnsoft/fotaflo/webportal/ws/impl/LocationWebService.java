package com.scnsoft.fotaflo.webportal.ws.impl;

import com.scnsoft.fotaflo.webportal.bean.LocationBean;
import com.scnsoft.fotaflo.webportal.bean.LocationMetadataBean;
import com.scnsoft.fotaflo.webportal.ws.ILocationWebService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocationWebService extends AbstractWebService implements ILocationWebService {
    @Override
    public String getResourcePath() {
        return getRestPath() + RESOURCE;
    }

    @Override
    public List<LocationBean> getLocations() {
        String url = getResourcePath() + '/' + "all";
        List<LocationBean> result = getJsonList(get(url), LocationBean.class);

        return result;
    }

    @Override
    public LocationBean getLocation(Integer id) {
        if(id == null){
            return null;
        }
        String url = getResourcePath() + '/' + id;
        LocationBean result = (LocationBean)getJson(get(url), LocationBean.class);

        return result;
    }

    @Override
    public LocationMetadataBean getLocationMetadata(Integer id) {
        if(id == null){
            return null;
        }
        String url = getResourcePath() + '/' + id + "/metadata";
        LocationMetadataBean result = (LocationMetadataBean)getJson(get(url), LocationMetadataBean.class);

        return result;
    }
}
