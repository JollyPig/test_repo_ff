package com.scnsoft.fotaflo.webportal.dao.impl;

import com.scnsoft.fotaflo.common.dao.AbstractDao;
import com.scnsoft.fotaflo.webportal.dao.ISystemUserDao;
import com.scnsoft.fotaflo.webportal.model.SystemUser;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */
@Repository
public class SystemUserDao extends AbstractDao<SystemUser, Integer> implements ISystemUserDao {

    @Override
    protected Class getDomainClass() {
        return SystemUser.class;
    }

    public SystemUser getUserByLogin(String login) {
        List result = getSession().createQuery(
                "from SystemUser user where user.loginName = :login")
                .setString("login", login)
                .list();
        if (!result.isEmpty()) {
            return (SystemUser) result.get(0);
        }

        return null;
    }

    public List<SystemUser> getUserByLoginGroup(String loginGroup) {
        List result = getSession().createQuery(
                "from SystemUser user where user.loginGroup = :loginGroup")
                .setString("loginGroup", loginGroup)
                .list();
        if (!result.isEmpty()) {
            return result;
        }

        return null;
    }

}
