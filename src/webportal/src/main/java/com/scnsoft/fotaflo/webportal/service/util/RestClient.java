package com.scnsoft.fotaflo.webportal.service.util;

import com.scnsoft.fotaflo.common.util.DateFormatUtil;
import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.http.client.utils.URIBuilder;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Map;

@Component
public class RestClient {
    protected static Logger logger = Logger.getLogger(RestClient.class);

    DateFormat dtf = DateFormatUtil.ISO_DATETIME_FORMAT;

    public static final int RETRY_COUNT = 3;

    public String get(String url, String username, String password){
        return get(url, username, password, null);
    }

    public String get(String url, String username, String password, Map<String, Object> params){
        return request(RequestType.GET, url, username, password, params);
    }

    public String post(String url, String username, String password, Map<String, Object> params){
        return request(RequestType.POST, url, username, password, params);
    }

    protected String request(RequestType type, String url, String username, String password, Map<String, Object> params){
        String response = null;
        HttpMethodBase method = null;

        if(type == null){
            throw new IllegalArgumentException("'type' cannot be null");
        }

        if(url == null){
            throw new IllegalArgumentException("'Url' cannot be null");
        }

        try {
            HttpClient client = new HttpClient();
            if(username != null && password != null){
                Credentials credentials = new UsernamePasswordCredentials(username, password);
                client.getState().setCredentials(AuthScope.ANY, credentials);
            }

            client.getParams().setAuthenticationPreemptive(true);
            client.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, getRetryHandler());

            switch (type){
                case GET: method = makeGetMethod(url, params); break;
                case POST: method = makePostMethod(url, params); break;
                default: throw new UnsupportedOperationException("Method " + type + "doesn't supported");
            }

            method.setDoAuthentication(true);

            // Send GET request
            int statusCode = client.executeMethod(method);
            logger.info(method.getURI());

            switch (statusCode) {
                case 400: {
                    logger.error("Bad request. The parameters passed to the service did not match as expected. The Message should tell you what was missing or incorrect.");
                    logger.warn("Change the parameter appcd to appid and this error message will go away.");
                    break;
                }
                case 403: {
                    logger.error("Forbidden. You do not have permission to access this resource, or are over your rate limit.");
                    break;
                }
                case 503: {
                    logger.error("Service unavailable. An internal problem prevented us from returning data to you.");
                    break;
                }
                default: {
                    if(statusCode >= 200 && statusCode < 400){
                        logger.info("Response HTTP status: " + statusCode);
                        response = method.getResponseBodyAsString();
                    }else{
                        logger.error("Your call returned an unexpected HTTP status of: " + statusCode);
                    }
                }

            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            // release any connection resources used by the method
            if(method != null){
                method.releaseConnection();
            }
        }

        return response;
    }

    protected HttpMethodBase makeGetMethod(String url, Map<String, Object> params) throws URISyntaxException{
        GetMethod method = null;
        URIBuilder builder = new URIBuilder(url);

        if(params != null && !params.isEmpty()){
            Object value;
            for(String key: params.keySet()){
                value = params.get(key);
                if(value != null){
                    if(value instanceof Date){
                        builder.addParameter(key, dtf.format((Date) value));
                    }else{
                        builder.addParameter(key, value.toString());
                    }
                }
            }
        }

        method = new GetMethod(builder.build().toString());

        return method;
    }

    protected HttpMethodBase makePostMethod(String url, Map<String, Object> params) throws URISyntaxException{
        PostMethod method = new PostMethod(url);

        if(params != null && !params.isEmpty()){
            Object value;
            for(String key: params.keySet()){
                value = params.get(key);
                if(value != null){
                    if(value instanceof Date){
                        method.addParameter(key, dtf.format((Date) value));
                    }else{
                        method.addParameter(key, value.toString());
                    }
                }
            }
        }

        return method;
    }

    protected HttpMethodRetryHandler getRetryHandler(){
        HttpMethodRetryHandler handler = new HttpMethodRetryHandler() {
            public boolean retryMethod(final HttpMethod method, final IOException exception, int executionCount) {
                if (executionCount >= RETRY_COUNT) {
                    // Do not retry if over max retry count
                    return false;
                }
                if (exception instanceof NoHttpResponseException) {
                    // Retry if the server dropped connection on us
                    return true;
                }
                if (!method.isRequestSent()) {
                    // Retry if the request has not been sent fully or
                    // if it's OK to retry methods that have been sent
                    return true;
                }
                // otherwise do not retry
                return false;
            }
        };

        return handler;
    }

    protected enum RequestType{
        GET, POST
    }

}
