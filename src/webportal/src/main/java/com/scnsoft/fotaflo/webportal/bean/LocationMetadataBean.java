package com.scnsoft.fotaflo.webportal.bean;

public class LocationMetadataBean {

    private String title;
    private String comment;
    private String copyright;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCopyright() {
        return copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    @Override
    public String toString() {
        return "LocationMetadataBean{" +
                "title='" + title + '\'' +
                ", comment='" + comment + '\'' +
                ", copyright='" + copyright + '\'' +
                '}';
    }
}
