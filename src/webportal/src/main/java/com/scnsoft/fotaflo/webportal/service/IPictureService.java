package com.scnsoft.fotaflo.webportal.service;

import com.scnsoft.fotaflo.webportal.bean.PictureBean;
import com.scnsoft.fotaflo.webportal.model.Picture;

import java.io.File;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 04.04.14
 * Time: 15:12
 * To change this template use File | Settings | File Templates.
 */
public interface IPictureService {

    List<PictureBean> getPictures(String user);

    List<PictureBean> getPictures(String user, boolean forceReload);

    PictureBean getPicture(int id);

    List<PictureBean> getPicturesByIds(List<Integer> ids);

    File getPictureById (final Integer pictureId) throws Exception;

}
