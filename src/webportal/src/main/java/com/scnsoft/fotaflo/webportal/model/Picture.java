package com.scnsoft.fotaflo.webportal.model;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import com.scnsoft.fotaflo.common.model.AbstractEntity;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 08.04.14
 * Time: 17:02
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "picture")
public class Picture extends AbstractEntity {

    private Integer pictureId;
    private Integer serverId;
    private String name;
    private String url;
    private String small_url;
    private Integer cameraId;
    private String cameraName;
    private Date creationDate;
    private String logoMainUrl;
    private String logoUrl;
    private String logoText;
    private Boolean rotated;
    private Set<SystemUser> systemUsers = new HashSet<SystemUser>();

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinTable(name = "users_pictures", joinColumns = {@JoinColumn(name = "picture_id")}, inverseJoinColumns = {@JoinColumn(name = "system_user_id")})
    public Set<SystemUser> getSystemUsers() {
        return systemUsers;
    }

    public void setSystemUsers(Set<SystemUser> systemUsers) {
        this.systemUsers = systemUsers;
    }

    @Column(name = "external_picture_id")
    public Integer getPictureId() {
        return pictureId;
    }

    public void setPictureId(Integer pictureId) {
        this.pictureId = pictureId;
    }

    @Column(name = "external_server_id")
    public Integer getServerId() {
        return serverId;
    }

    public void setServerId(Integer serverId) {
        this.serverId = serverId;
    }

    @Column(name = "picture_name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "picture_url")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Column(name = "picture_url_small")
    public String getSmall_url() {
        return small_url;
    }

    public void setSmall_url(String small_url) {
        this.small_url = small_url;
    }


    @Column(name = "camera_id")
    public Integer getCameraId() {
        return cameraId;
    }

    public void setCameraId(Integer cameraId) {
        this.cameraId = cameraId;
    }

    @Column(name = "picture_camera_name")
    public String getCameraName() {
        return cameraName;
    }

    public void setCameraName(String cameraName) {
        this.cameraName = cameraName;
    }

    @Column(name = "creation_date")
    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Column(name = "picture_logo_main_url")
    public String getLogoMainUrl() {
        return logoMainUrl;
    }

    public void setLogoMainUrl(String logoMainUrl) {
        this.logoMainUrl = logoMainUrl;
    }

    @Column(name = "picture_logo_url")
    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    @Column(name = "picture_logo_text")
    public String getLogoText() {
        return logoText;
    }

    public void setLogoText(String logoText) {
        this.logoText = logoText;
    }

    @Column(name = "picture_rotated")
    public Boolean getRotated() {
        return rotated;
    }

    public void setRotated(Boolean rotated) {
        this.rotated = rotated;
    }

}
