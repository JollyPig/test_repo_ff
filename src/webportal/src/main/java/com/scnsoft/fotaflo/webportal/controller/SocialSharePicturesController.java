package com.scnsoft.fotaflo.webportal.controller;

import com.scnsoft.fotaflo.common.util.IOUtil;
import com.scnsoft.fotaflo.common.util.ServletUtil;
import com.scnsoft.fotaflo.webportal.bean.PictureBean;
import com.scnsoft.fotaflo.webportal.dto.SocialSharePicturesDto;
import com.scnsoft.fotaflo.webportal.service.IPictureLogoService;
import com.scnsoft.fotaflo.webportal.service.IPictureService;
import com.scnsoft.fotaflo.webportal.service.IShareService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;

/**
 * @author Anatol Sialitski
 */
@Controller
@RequestMapping (value = "/main")
public class SocialSharePicturesController {

    public static final String REDIRECT_URL_PART = "/pictures/auth/autoLogin?username=";

    @Autowired
    private IShareService shareService;

    @Autowired
    private IPictureService pictureService;

    @Autowired
    private IPictureLogoService pictureLogoService;

    @RequestMapping (value = "/generateShareLink", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<String> generateShareLink (final @RequestBody SocialSharePicturesDto dto, final HttpServletRequest request) {
        try {
            Assert.notEmpty (dto.getImageIds ());

            return new ResponseEntity<String> (ServletUtil.getContextPath (request) + REDIRECT_URL_PART + shareService.createUserForSocialShare (dto.getImageIds ()), HttpStatus.OK);
        }
        catch (final Throwable th) {
            return new ResponseEntity<String> (th.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping (value = "/background-image/{code}", method = RequestMethod.GET)
    public void getBackgroundImage (final @PathVariable (value = "code") String code, final HttpServletResponse response) throws Exception {
        if (code != null && ! "".equals (code.trim ())) {
            InputStream in = null;

            final String imgPath = "d:\\tmp\\243266a4-a84a-4b15-8dd4-cf880a1d96f7.jpg"; // TODO find path by code in the database

            try {
                IOUtils.copy (in = new BufferedInputStream (new FileInputStream (imgPath)), response.getOutputStream ());
            }
            finally {
                if (in != null) {
                    in.close ();
                }
            }
        }
    }

    @RequestMapping (value = "/loadPicturePinterest", method = RequestMethod.GET)
    @Transactional
    public void getPictureForPinterest (final @RequestParam (value = "picId", required = true) Integer pictureId, final HttpServletRequest httpRequest, final HttpServletResponse httpResponse) throws IOException, URISyntaxException {
        PictureBean pictureBean = pictureService.getPicture (pictureId);

        httpResponse.addHeader("Content-Type", "image/jpg");
        httpResponse.setHeader("Content-Disposition", "inline;filename=\"" + pictureBean.getName() + "\"");

        File file  = pictureLogoService.getMarkedPictureFile (pictureBean, false);

        IOUtil.writeFromInputToOutput (new BufferedInputStream (new FileInputStream (file)), httpResponse.getOutputStream ());
    }

}
