package com.scnsoft.fotaflo.webportal.analytics.ga;

public enum GaDimension implements IGaField {
    location        ("ga:dimension1", "Location ID"),
    locationName    ("ga:dimension3", "Location Name"),
    username        ("ga:dimension2", "Username"),
    deviceType      ("ga:deviceCategory", "Device Type"),
    socialNetwork   ("ga:socialInteractionNetwork", "Social Network"),
    eventCategory   ("ga:eventCategory", "Event Category"),
    eventAction     ("ga:eventAction", "Event Action"),
    eventLabel      ("ga:eventLabel", "Event Label"),
    socialAccount   ("ga:dimension6", "Social Network Account"),
    date            ("ga:date", "Date"),
    week            ("ga:week", "Week"),
    month           ("ga:month", "Month"),
    year            ("ga:year", "Year"),
    city            ("ga:city", "City");

    private String id;
    private String label;

    private GaDimension(String id, String label) {
        this.id = id;
        this.label = label;
    }

    public String getId(){
        return id;
    }

    public String getLabel(){
        return label;
    }

}
