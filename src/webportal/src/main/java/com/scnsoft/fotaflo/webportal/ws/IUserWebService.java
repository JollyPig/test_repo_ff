package com.scnsoft.fotaflo.webportal.ws;

import com.scnsoft.fotaflo.webportal.bean.UserBean;

public interface IUserWebService {

    String RESOURCE = "/web/users";

    UserBean getUser(String login);

}
