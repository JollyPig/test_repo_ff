package com.scnsoft.fotaflo.webportal.service.impl;

import com.scnsoft.fotaflo.webportal.service.IPictureLoadService;
import com.scnsoft.fotaflo.webportal.service.util.UserContextHolder;
import com.scnsoft.fotaflo.common.util.IOUtil;
import com.scnsoft.fotaflo.webportal.service.util.ImageLoader;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.net.URL;

@Service
public class PictureLoadService implements IPictureLoadService {
    protected final static Logger logger = Logger.getLogger(PictureLoadService.class);

    @Autowired
    private UserContextHolder userContextHolder;

    @Autowired
    private ImageLoader loader;

    @Override
    public BufferedImage loadPicture(String path) throws IOException, URISyntaxException{
        if(path == null || path.trim().isEmpty()){
            throw new IllegalArgumentException("Field 'path' cannot be null or empty");
        }
        BufferedImage bufferedImage;
        URL url = new URL(getFullPath(path));
        if("file".equals(url.getProtocol())){
            bufferedImage = ImageIO.read(url);
        }else{
            bufferedImage = loader.loadImageAsBufferedImage(IOUtil.getValidURL(url), getUsername(), getPassword());
        }
        return bufferedImage;
    }

    @Override
    public void loadPicture(String path, OutputStream out) throws IOException, URISyntaxException{
        if(path == null || path.trim().isEmpty()){
            throw new IllegalArgumentException("Field 'path' cannot be null or empty");
        }
        URL url = new URL(getFullPath(path));
        if("file".equals(url.getProtocol())){
            IOUtil.writeFromInputToOutput(new UrlResource(url).getInputStream(), out);
        }else{
            loader.loadImage(IOUtil.getValidURL(url), getUsername(), getPassword(), out);
        }
    }

    protected String getFullPath(String path){
        path = userContextHolder.getRemotePicturePath() + path;
        return path.replace('\\','/');
    }

    private String getUsername(){
        return userContextHolder.getServer().getPrincipal().getUsername();
    }

    private String getPassword(){
        return userContextHolder.getServer().getPrincipal().getPassword();
    }

}
