package com.scnsoft.fotaflo.webportal.dto;

import java.util.List;

public class PromotionsDto {

    private List<PromotionDto> promotions;

    private List<TextPromotionDto> textPromotions;

    public List<PromotionDto> getPromotions() {
        return promotions;
    }

    public void setPromotions(List<PromotionDto> promotions) {
        this.promotions = promotions;
    }

    public List<TextPromotionDto> getTextPromotions() {
        return textPromotions;
    }

    public void setTextPromotions(List<TextPromotionDto> textPromotions) {
        this.textPromotions = textPromotions;
    }

    @Override
    public String toString() {
        return "PromotionsDto{" +
                "promotions=" + promotions +
                ", textPromotions=" + textPromotions +
                '}';
    }
}
