package com.scnsoft.fotaflo.webportal.service;

public interface IMailService {

    void sendMailFrom(String from, String subject, String message);

    void sendMail(String subject, String message);

    void sendMail(String subject, String message, String to);

    void sendMail(String from, String subject, String message, String to);

}
