package com.scnsoft.fotaflo.webportal.service.impl;

import com.scnsoft.fotaflo.common.util.NumberUtils;
import com.scnsoft.fotaflo.common.util.StringUtils;
import com.scnsoft.fotaflo.webportal.bean.PictureBean;
import com.scnsoft.fotaflo.webportal.dao.IMerchantDao;
import com.scnsoft.fotaflo.webportal.dao.IPaymentDao;
import com.scnsoft.fotaflo.webportal.dto.MerchantAccountDto;
import com.scnsoft.fotaflo.webportal.dto.PaymentDto;
import com.scnsoft.fotaflo.webportal.model.Merchant;
import com.scnsoft.fotaflo.webportal.model.Payment;
import com.scnsoft.fotaflo.webportal.model.Picture;
import com.scnsoft.fotaflo.webportal.model.SystemUser;
import com.scnsoft.fotaflo.webportal.service.IPayPalService;
import com.scnsoft.fotaflo.webportal.service.IPaymentService;
import com.scnsoft.fotaflo.webportal.service.IPictureService;
import com.scnsoft.fotaflo.webportal.service.IUserService;
import com.scnsoft.fotaflo.webportal.service.exception.PaymentValidationException;
import com.scnsoft.fotaflo.webportal.service.exception.ServiceException;
import com.scnsoft.fotaflo.common.security.PasswordEncryption;
import com.scnsoft.fotaflo.webportal.ws.IAnalyticWebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Tayna on 01.05.14.
 */
@Service
@Transactional(readOnly = false)
public class PayPalService implements IPayPalService {

    @Autowired
    private IMerchantDao merchantDao;

    @Autowired
    private IPaymentDao payPalDao;

    @Autowired
    private IUserService userService;

    @Autowired
    private IPictureService pictureService;

    @Autowired
    private IPaymentService paymentService;

    @Autowired
    private IAnalyticWebService analyticWebService;

    public String initiatePayment(PaymentDto dto, String confirmRedirectUrl, String cancelRedirectUrl){
        String redirectUrl = null;

        Payment payment = verifyPayment(dto);

        try{
            redirectUrl = paymentService.requestPayment(payment, confirmRedirectUrl, cancelRedirectUrl);
            payment.setStatus(Payment.StatusCode.REGISTERED.toString());
            payment.setResult("success");
        }catch (ServiceException e){
            payment.setStatus(Payment.StatusCode.ERROR_REGISTERED.toString());
            payment.setResult(e.getMessage());
        }

        this.savePayment(payment);

        return redirectUrl;
    }

    protected Payment verifyPayment(PaymentDto dto){
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        SystemUser user = userService.getUserByLogin(currentUser);

        if (user == null) {
            throw new PaymentValidationException("User error.");
        }

        if(dto == null){
            throw new PaymentValidationException("Payment is empty.");
        }

        String currency = user.getCurrency();
        Integer picturePrice = NumberUtils.getInteger(user.getPrice());
        Integer packagePrice = NumberUtils.getInteger(user.getPackagePrice());
        List<Picture> pictures = user.getPictures();

        List<PictureBean> selected = pictureService.getPicturesByIds(dto.getPictureIds());

        if(selected == null || selected.isEmpty()){
            throw new PaymentValidationException("There are no pictures.");
        }

        if(dto.getPictureIds() != null && dto.getPictureIds().size() != selected.size()){
            throw new PaymentValidationException("Some picture ids doesn't correspond real picture.");
        }

        if (StringUtils.isEmpty(dto.getEmail())) {
            throw new PaymentValidationException("The email was entered incorrectly.");
        }

        if (dto.getPrice() == null || dto.getPrice() <= 0) {
            throw new PaymentValidationException("Total price is incorrect.");
        }

        if (currency == null || picturePrice == null) {
            throw new PaymentValidationException("Wrong currency or price settings for the location.");
        }

        Double calculatedValue = null;
        int num = selected.size();
        boolean group = (num == pictures.size() && packagePrice != null);
        if(group){
            calculatedValue = new Double(packagePrice);
        }else{
            calculatedValue = new Double(num * picturePrice);
        }

        if (calculatedValue == null || !calculatedValue.equals(dto.getPrice())) {
            throw new PaymentValidationException("The total amount of money does not correspond.");
        }

        Payment payment = new Payment();
        payment.setUserLogin(currentUser);
        payment.setPaymentDate(new Date());
        payment.setPrice(dto.getPrice());
        payment.setCurrency(currency);
        payment.setPayerEmail(dto.getEmail());
        payment.setPictureNumber(num);
        payment.setPictureIds(getImageListString(dto.getPictureIds()));       // todo change field type
        if(group){
            payment.setGroupPackage(true);
        }

        return payment;
    }

    private String getImageListString(List<Integer> images) {   // todo remove
        String imageString = "";
        for (Integer image : images) {
            if(imageString.length() != 0){
                imageString += ", ";
            }
            imageString += "id_" + image;
        }
        return imageString;
    }

    @Override
    public String completePayment(String token) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        String newLogin = null;

        Payment payment = this.getRegisteredPayment(token);
        boolean success = false;

        try{
            success = paymentService.completePayment(payment);
        }catch (ServiceException e){
            payment.setStatus(Payment.StatusCode.ERROR_REGISTERED.toString());
            payment.setResult(e.getMessage());
        }

        if(success){
            /// NEW LOGIN GENERATION
            SystemUser user = userService.getUserByLogin(currentUser);
            String userBase = currentUser;
            if (user != null && user.getLoginGroup() != null && !user.getLoginGroup().isEmpty()) {
                userBase = user.getLoginGroup();
            }
            List<String> userLogins = userService.generateNewUserLogins(userBase, 1, payment.getPictureIds());
            newLogin = userLogins.get(0);
            payment.setNewLogin(newLogin);
            payment.setStatus(Payment.StatusCode.COMPLETE.toString());
        }

        this.savePayment(payment);

        this.sendReportData(payment);

        return newLogin;
    }

    @Async
    protected void sendReportData(Payment payment){
        if(payment != null){
            SystemUser user = userService.getUserByLogin(payment.getUserLogin());
            if(user != null){
                analyticWebService.sendReportData(user.getLocationId(), payment.getPaymentDate(), payment.getPictureNumber(), payment.isGroupPackage());
            }
        }
    }

    protected void savePayment(Payment payment) {
        payPalDao.update(payment);
    }

    public Payment getRegisteredPayment(String token) {
        if (token == null) {
            return null;
        }
        List<Payment> payments = payPalDao.getPaymentByTransaction(token);
        if (payments != null && payments.size() == 1) {
            return payments.get(0);
        } else {
            return null;
        }
    }

    public Merchant getMerchantAccount() throws Exception {
        Merchant merchant = null;
        List<Merchant> accounts = merchantDao.list();
        if (accounts.size() == 1) {
            merchant = accounts.get(0);
        }
        if (merchant != null) {
            PasswordEncryption passwordEncryption = new PasswordEncryption();
            merchant.setPassword(passwordEncryption.decryptPassword(merchant.getPassCode()));
            merchant.setSignature(passwordEncryption.decryptPassword(merchant.getSignCode()));
        }
        return merchant;
    }

    public Merchant getMerchantAccountEncrypted() throws Exception {
        Merchant merchant = null;
        List<Merchant> accounts = merchantDao.list();
        if (accounts.size() == 1) {
            merchant = accounts.get(0);
        }
        if(merchant != null){
            merchant.setPassword(merchant.getPassCode().toString());
            merchant.setSignature(merchant.getSignCode().toString());
        }
        return merchant;
    }

    public Merchant getMerchant() throws Exception {
        Merchant merchant = null;
        List<Merchant> accounts = merchantDao.list();
        if (accounts.size() == 1) {
            merchant = accounts.get(0);
        }
        return merchant;
    }

    @Override
    public Merchant saveMerchantAccount (final MerchantAccountDto model) throws Exception {
        Merchant merchant = getMerchant();
        if (merchant == null) {
            merchant = new Merchant();
            fillMerchantFields(merchant,model);
            merchantDao.create(merchant);
        }else{
            fillMerchantFields( merchant, model);
            merchantDao.update(merchant);
        }

        return merchant;
    }

    private Merchant fillMerchantFields (final Merchant merchant, final MerchantAccountDto model) throws Exception{
        PasswordEncryption passwordEncryption = new PasswordEncryption();
        merchant.setAccount(model.getAccount ());
        merchant.setPassword(model.getPassword());
        merchant.setPassCode(passwordEncryption.encryptPassword(model.getPassword()));
        merchant.setSignature(model.getSignature());
        merchant.setSignCode(passwordEncryption.encryptPassword(model.getSignature()));
        merchant.setEnvironment(model.getEnvironment());

        return merchant;
    }

}
