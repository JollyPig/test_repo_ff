package com.scnsoft.fotaflo.webportal.service.util;

import com.scnsoft.fotaflo.webportal.model.Position;
import org.apache.log4j.Logger;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;

/**
 * The tool to adding watermark to images.
 */
@org.springframework.stereotype.Component
public class ImageWatermark {
    protected static Logger logger = Logger.getLogger(ImageWatermark.class);

    private static final int OFFSET_X = 30;
    private static final int OFFSET_Y = 30;
    private static final int OFFSET_SHADOW = 5;

    private static final int FONT_SIZE = 60;

    private static final int DEFAULT_IMAGE_SIZE = 1600;

    /**
     * A logo picture to picture and watermark, effective this way
     *
     * @param image         picture
     * @param logoLeft      watermark logo image #1
     * @param logoRight     watermark logo image #2
     * @param logoText      text
     */
    public BufferedImage addLogosToImage(BufferedImage image, BufferedImage logoLeft, BufferedImage logoRight, String logoText) {
        int fontSize = FONT_SIZE;
        double logoBoxSize = 0.2;

        Graphics g = image.getGraphics();
        int imageWidth = image.getWidth(),
                imageHeight = image.getHeight();

        int imageSize = Math.max(imageWidth, imageHeight);

        int logoLeftWidth = 0, logoLeftHeight = 0;
        int logoRightWidth = 0, logoRightHeight = 0;

        double scale = 1.0;
        double scaleLeft = 1.0,
                scaleRight = 1.0;

        if(logoLeft != null){
            logoLeftWidth = logoLeft.getWidth();
            logoLeftHeight = logoLeft.getHeight();
        }

        if(logoRight != null){
            logoRightWidth = logoRight.getWidth();
            logoRightHeight = logoRight.getHeight();
        }

        if((logoLeftWidth + 2*OFFSET_X) > image.getWidth()*logoBoxSize){
            scaleLeft = ((double)image.getWidth()*logoBoxSize) / (logoLeftWidth + 2*OFFSET_X);
        }

        if((logoRightWidth + 2*OFFSET_X) > image.getWidth()*logoBoxSize){
            scaleRight = ((double)image.getWidth()*logoBoxSize) / (logoRightWidth + 2*OFFSET_X);
        }

        /*if((logoLeftHeight + logoRightHeight + 4*OFFSET_Y)*2*scale > image.getHeight()){
            scale *= image.getHeight() / ((logoLeftHeight + logoRightHeight + 4*OFFSET_Y)*2*scale); // fixme if it's a bug
        }*/

        logoLeftWidth *= scaleLeft;
        logoLeftHeight *= scaleLeft;
        logoRightWidth *= scaleRight;
        logoRightHeight *= scaleRight;

        /*logoMaxWidth *= scale;*/

        /*fontSize *= scale;*/

        if(logoLeft != null){
            markImageByLogoImage(image, logoLeft, Position.LEFT_BOTTOM, scaleLeft);
        }

        if(logoRight != null){
            markImageByLogoImage(image, logoRight, Position.RIGHT_BOTTOM, scaleRight);
        }

        int logoMaxWidth = Math.max(logoLeftWidth, logoRightWidth);

        /*if((2*logoMaxWidth + 4*OFFSET_X)*2 > image.getWidth()){
            scale = (double)image.getWidth() / (double)((2*logoMaxWidth + 4*OFFSET_X)*2);
        }*/

        int logoMaxHeight = Math.max(logoLeftHeight, logoRightHeight);

        scale = ((double)image.getWidth()*logoBoxSize) / (logoMaxWidth + 2*OFFSET_X);

        if (logoText != null && logoText.length() > 0) {
            //drawing logo text
            int logoTextMinWidth = image.getWidth() - 2*(logoMaxWidth + 2*OFFSET_X);
            int offset_x = logoMaxWidth + 2*OFFSET_X;
            int offset_y;

            if(imageSize < DEFAULT_IMAGE_SIZE){
                fontSize *= (double)imageSize/DEFAULT_IMAGE_SIZE;
            }

            Font font = new Font("Comic Sans MS", Font.BOLD, fontSize);
            FontMetrics fm = g.getFontMetrics(font);
            int fontHeight = fm.getHeight();
            int leading = fm.getLeading();
            int textWidth = fm.stringWidth(logoText);

            if (textWidth > logoTextMinWidth) {
                java.util.List<String> stringsToWrite = new ArrayList<String>();
                String[] strArray = logoText.split(" ");
                String toAdd = strArray[0] + " ";
                for (int i = 1; i < strArray.length; i++) {
                    if (fm.stringWidth(toAdd + strArray[i]) < logoTextMinWidth) {
                        toAdd += strArray[i] + " ";
                    }else{
                        stringsToWrite.add(toAdd.trim());
                        toAdd = strArray[i] + " ";
                    }
                }
                stringsToWrite.add(toAdd);

                int lineCount = stringsToWrite.size();
                for (int i = 0; i < lineCount; i++) {
                    textWidth = fm.stringWidth(stringsToWrite.get(i));
                    offset_y = OFFSET_Y + leading * (lineCount - 1 - i) + fontHeight * (lineCount - 1 - i);
                    if(lineCount * fontHeight + leading * (lineCount - 1) < logoMaxHeight){
                        offset_y += (logoMaxHeight - lineCount * fontHeight + leading * (lineCount - 1)) / 2;
                    }
                    addTextToImage(g, stringsToWrite.get(i), font, offset_x + (logoTextMinWidth - textWidth)/2, image.getHeight() - offset_y + (fontHeight / 2), scale);
                }

            }else{
                offset_x += (logoTextMinWidth - textWidth)/2;
                offset_y = image.getHeight() - logoMaxHeight / 2 - OFFSET_Y;
                if(fontHeight < logoMaxHeight){
                    offset_y += fontHeight / 2;
                }
                addTextToImage(g, logoText, font, offset_x, offset_y, scale);
            }
        }

        return image;
    }

    private Graphics addTextToImage(Graphics g, String text, Font font, int offset_x, int offset_y, double scale){
        g.setColor(Color.black);
        g.setFont(font);
        g.drawString(text, offset_x, offset_y);
        g.setColor(Color.white);
        offset_x = offset_x - (int)(OFFSET_SHADOW * scale);
        offset_y = offset_y + (int)(OFFSET_SHADOW * scale);
        g.drawString(text, offset_x, offset_y);
        return g;
    }

    public BufferedImage markImageByLogoImage(BufferedImage image, BufferedImage logo, Position position, double scale){
        Graphics g = image.getGraphics();
        int width = image.getWidth();
        int height = image.getHeight();
        int markWidth = (int)(logo.getWidth() * scale);
        int markHeight = (int)(logo.getHeight() * scale);
        int offsetX = OFFSET_X,
            offsetY = OFFSET_Y;
        int markX,
            markY;

        //g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, DefaultValues.opacity));
        switch (position) {
            case LEFT_TOP:
                markX = offsetX;
                markY = offsetY;
                break;
            case LEFT_MIDDLE:
                markX = offsetX;
                markY = (height - markHeight - offsetY) / 2;
                break;
            case LEFT_BOTTOM:
                markX = offsetX;
                markY = height - markHeight - offsetY;
                break;
            case CENTER_TOP:
                markX = (width - markWidth - offsetX) / 2;
                markY = offsetY;
                break;
            case CENTER:
                markX = (width - markWidth - offsetX) / 2;
                markY = (height - markHeight - offsetY) / 2;
                break;
            case CENTER_BOTTOM:
                markX = (width - markWidth - offsetX) / 2;
                markY = height - markHeight - offsetY;
                break;
            case RIGHT_TOP:
                markX = width - markWidth - offsetX;
                markY = offsetY;
                break;
            case RIGHT_MIDDLE:
                markX = width - markWidth - offsetX;
                markY = (height - markHeight - offsetY) / 2;
                break;
            case RIGHT_BOTTOM:
                markX = width - markWidth - offsetX;
                markY = height - markHeight - offsetY;
                break;
            default:
                markX = width - markWidth - offsetX;
                markY = height - markHeight - offsetY;
        }

        g.drawImage(logo, markX, markY, markWidth, markHeight, null);

        return image;
    }

}
