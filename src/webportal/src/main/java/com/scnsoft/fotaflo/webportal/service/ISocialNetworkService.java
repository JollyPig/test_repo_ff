package com.scnsoft.fotaflo.webportal.service;


import com.scnsoft.fotaflo.webportal.dto.SocialNetworkDto;
import com.scnsoft.fotaflo.webportal.model.SocialNetwork;
import com.scnsoft.fotaflo.webportal.model.SocialNetworkType;

import java.util.List;
import java.util.Map;

public interface ISocialNetworkService {

    SocialNetworkDto getSocialNetwork(Integer id);

    SocialNetworkDto findSocialNetworkDto(String name, Integer location);

    Map<String, SocialNetworkDto> getSocialNetworkMap();

    void updateSocialNetwork(SocialNetworkDto socialNetwork);

    void updateSocialNetworks(List<SocialNetworkDto> socialNetwork);

    List<SocialNetworkType> getSocialNetworkNames();

    Map<String, SocialNetwork> getSocialNetworkSettings();

    List<SocialNetworkDto> getSocialNetworkByLocation ();
}
