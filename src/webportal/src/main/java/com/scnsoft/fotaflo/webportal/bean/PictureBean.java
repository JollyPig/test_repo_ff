package com.scnsoft.fotaflo.webportal.bean;


import java.io.File;
import java.util.Date;
import java.util.HashSet;

import com.google.gson.annotations.SerializedName;
import com.scnsoft.fotaflo.webportal.model.Picture;
import com.scnsoft.fotaflo.webportal.model.SystemUser;

/**
 * Created by Paradinets Tatsiana
 * Date: 26.03.2011
 * Time: 11:41:11
 */

public class PictureBean {
    public static final String IMG_DIR = "img" + "/";
    public static final String IMG_SMALL_DIR = "img_small" + "/";

    @SerializedName("id")
    private Integer id;

    private Integer idInApp;

    @SerializedName("name")
    private String name;

    @SerializedName("url")
    private String url;

    @SerializedName("base64code")
    private String base64code;

    @SerializedName("cameraId")
    private Integer cameraId;

    @SerializedName("cameraName")
    private String cameraName;

    @SerializedName("creationDate")
    private Date creationDate;

    @SerializedName("logoMainUrl")
    private String logoMainUrl;

    @SerializedName("logoUrl")
    private String logoUrl;

    @SerializedName("logoText")
    private String logoText;

    @SerializedName("rotated")
    private Boolean rotated;

    public PictureBean() {
    }

    public PictureBean(Picture picture) {
        this.id = picture.getId();
        this.idInApp = picture.getId();

        this.name = picture.getName();
        this.url = picture.getUrl();
        this.base64code = picture.getSmall_url();

        this.cameraId = picture.getCameraId();
        this.cameraName = picture.getCameraName();
        this.creationDate = picture.getCreationDate();
        this.logoMainUrl = picture.getLogoMainUrl();
        this.logoUrl = picture.getLogoUrl();
        this.logoText = picture.getLogoText();
        this.rotated = picture.getRotated();
    }

    public PictureBean(int id, String name, String url, Integer cameraId, String cameraName, Date creationDate,
                       Boolean rotated, String logoMainUrl,String logoUrl,String logoText) {
        this(id, name, url, null, cameraId, cameraName, creationDate, rotated, logoMainUrl,logoUrl,logoText);
    }

    public PictureBean(int id, String name, String url, String base64code, Integer cameraId, String cameraName, Date creationDate, Boolean rotated, String logoMainUrl,String logoUrl,String logoText) {
        this.id = id;
        this.idInApp = id;
        this.name = name;

        url = getValidUrl(url);
        if(url != null && !url.isEmpty()){  // format url and define small url
            this.url = url;
            this.base64code = url.replaceFirst(IMG_DIR, IMG_SMALL_DIR);
        }
        this.cameraId = cameraId;
        this.cameraName = cameraName;
        this.creationDate = creationDate;
        this.rotated=rotated;
        this.logoMainUrl = getValidUrl(logoMainUrl);
        this.logoUrl = getValidUrl(logoUrl);
        this.logoText = logoText;
    }

    public Picture toEntity(){
        Picture picture = new Picture();

        picture.setName(this.getName());
        picture.setUrl(this.getUrl());
        picture.setSmall_url(this.getBase64code());
        picture.setCameraId(this.getCameraId());
        picture.setCameraName(this.getCameraName());
        picture.setCreationDate(this.getCreationDate());
        picture.setLogoMainUrl(this.getLogoMainUrl());
        picture.setLogoUrl(this.getLogoUrl());
        picture.setLogoText(this.getLogoText());
        picture.setRotated(this.getRotated());

        return picture;
    }

    public Picture toEntity(SystemUser user){
        Picture picture = this.toEntity();

        picture.setServerId(user.getServerNumber());
        picture.setSystemUsers(new HashSet<SystemUser>());
        picture.getSystemUsers().add(user);

        return picture;
    }

    protected String getValidUrl(String url){
        String valid = null;
        if(url != null && !url.isEmpty()){  // format url and define small url
            String u = url.replace(File.separator, "/");
            int sign = u.substring(0, 1).equals("/") ? 1 : 0;
            valid = u.substring(sign);
        }
        return valid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdInApp() {
        return idInApp;
    }

    public void setIdInApp(Integer idInApp) {
        this.idInApp = idInApp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBase64code() {
        return base64code;
    }

    public void setBase64code(String base64code) {
        this.base64code = getValidUrl(base64code);
    }

    public Integer getCameraId() {
        return cameraId;
    }

    public void setCameraId(Integer cameraId) {
        this.cameraId = cameraId;
    }

    public String getCameraName() {
        return cameraName;
    }

    public void setCameraName(String cameraName) {
        this.cameraName = cameraName;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Boolean getRotated() {
        return rotated;
    }

    public void setRotated(Boolean rotated) {
        this.rotated = rotated;
    }

    public String getUrl() {
        return getUrl( false );
    }

    public String getUrl(boolean reduced) {
        if (reduced)
        {
            return url != null ? url.replaceFirst( IMG_DIR, IMG_SMALL_DIR ) : null;
        }
        else
        {
            return url;
        }
    }

    public void setUrl(String url) {
        this.url = getValidUrl(url);
    }

    public String getLogoMainUrl() {
        return logoMainUrl;
    }

    public void setLogoMainUrl(String logoMainUrl) {
        this.logoMainUrl = getValidUrl(logoMainUrl);
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = getValidUrl(logoUrl);
    }

    public String getLogoText() {
        return logoText;
    }

    public void setLogoText(String logoText) {
        this.logoText = logoText;
    }

    @Override
    public String toString() {
        return "PictureBean{" +
                "id=" + id +
                ", idInApp=" + idInApp +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", base64code='" + base64code + '\'' +
                ", cameraId=" + cameraId +
                ", cameraName='" + cameraName + '\'' +
                ", creationDate=" + creationDate +
                ", logoMainUrl='" + logoMainUrl + '\'' +
                ", logoUrl='" + logoUrl + '\'' +
                ", logoText='" + logoText + '\'' +
                ", rotated=" + rotated +
                '}';
    }
}
