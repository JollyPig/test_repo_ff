package com.scnsoft.fotaflo.webportal.controller;



import com.scnsoft.fotaflo.webportal.service.IPromotionService;
import com.scnsoft.fotaflo.webportal.service.ITextPromotionService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */

/**
 * Handles and retrieves the login or denied page depending on the URI template
 */
@Controller
@RequestMapping("/auth")
public class LoginLogoutController {

    protected static Logger logger = Logger.getLogger(LoginLogoutController.class);

  @Autowired
    private IPromotionService promotionService;

    @Autowired
    private ITextPromotionService textPromotionService;

    @Autowired
    @Qualifier("authenticationManager")
    private AuthenticationManager authenticationManager;

    /**
     * Handles and retrieves the login JSP page
     *
     * @return the name of the JSP page
     */
    @RequestMapping(value = "/login", method = {RequestMethod.GET, RequestMethod.POST})
    public String getLoginPage(@RequestParam(value = "error", required = false) boolean error,
                               ModelMap model) {
        logger.info("Received request to show login page");

        // Add an error message to the model if login is unsuccessful
        // The 'error' parameter is set to true based on the when the authentication has failed.
        // We declared this under the authentication-failure-url attribute inside the spring-security.xml

        if (error) {
            // Assign an error message
            logger.warn("UNSUCCESSFUL LOGIN TRIAL");
            model.put("error", "Your code is not activated, please contact the company where you received the code to have the code activated");
          //  model.put("errorLogin", "You have entered an invalid username or password");
        } else {
            model.put("error", "");
          //  model.put("errorLogin", "");
        }

        model.put("promotions", promotionService.getPromotionMap());

        model.put("textPromotions", textPromotionService.getPromotionMap());

        // This will resolve to /WEB-INF/jsp/loginpage.jsp
        return "index";
    }


    @RequestMapping(value = "/autoLogin", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView login(@RequestParam(value = "username", required = false) String username,
                              HttpServletRequest httpServletRequest) {
        try {
            logger.debug("ATOLOGIN NEW!!!");
            Authentication request = new UsernamePasswordAuthenticationToken(username, username);
            Authentication result = authenticationManager.authenticate(request);
            SecurityContextHolder.getContext().setAuthentication(result);
            httpServletRequest.getSession().setAttribute("newSession", true);
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        ModelAndView modelAndView = new ModelAndView("redirect:/pictures/main/common");

        return modelAndView;
    }

    /**
     * Handles and retrieves the denied JSP page. This is shown whenever a regular user
     * tries to access an admin only page.
     *
     * @return the name of the JSP page
     */
    @RequestMapping(value = "/denied", method = RequestMethod.GET)
    public String getDeniedPage(ModelMap model) {
        logger.debug("Received request to show denied page");
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        model.put("current", currentUser);
        logger.warn("User [ "+ currentUser + " ]" + " got a denied page");
        // This will resolve to /WEB-INF/jsp/deniedpage.jsp
        return "denied";
    }


}