package com.scnsoft.fotaflo.webportal.ws;

public interface IBeanWrapper {
    Object prepare(Object bean);
}
