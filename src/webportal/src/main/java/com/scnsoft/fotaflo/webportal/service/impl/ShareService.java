package com.scnsoft.fotaflo.webportal.service.impl;

import com.scnsoft.fotaflo.common.util.StringUtils;
import com.scnsoft.fotaflo.webportal.bean.LocationBean;
import com.scnsoft.fotaflo.webportal.bean.PictureBean;
import com.scnsoft.fotaflo.webportal.bean.ShareStatusBean;
import com.scnsoft.fotaflo.webportal.model.SocialNetwork;
import com.scnsoft.fotaflo.webportal.model.SocialNetworkType;
import com.scnsoft.fotaflo.webportal.model.SystemUser;
import com.scnsoft.fotaflo.webportal.service.*;
import com.scnsoft.fotaflo.webportal.social.Connection;
import com.scnsoft.fotaflo.common.social.facebook.FacebookTemplate;
import com.scnsoft.fotaflo.common.social.tumblr.TumblrTemplate;
import com.scnsoft.fotaflo.common.social.twitter.TwitterTemplate;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.tumblr.api.Tumblr;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import twitter4j.Twitter;

import javax.inject.Inject;
import javax.inject.Provider;
import java.util.*;

@Service
public class ShareService implements IShareService {
    private final static Logger logger = Logger.getLogger(ShareService.class);

    private FacebookTemplate facebook;

    private TwitterTemplate twitter;

    private TumblrTemplate tumblr;

    @Inject
    private Provider<ConnectionRepository> connectionRepositoryProvider;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private IPictureService pictureService;

    @Autowired
    private IPictureLogoService pictureLogoService;

    @Autowired
    private ISocialNetworkService socialNetworkService;

    @Autowired
    private IMailService mailService;

    @Autowired
    private IUserService userService;

    @Autowired
    private ILocationService locationService;

    @Inject
    public ShareService(Facebook facebook, Twitter twitter, Tumblr tumblr) {
        this.facebook = new FacebookTemplate(facebook);
        this.twitter = new TwitterTemplate(twitter);
        this.tumblr = new TumblrTemplate(tumblr);
    }

    private ConnectionRepository getConnectionRepository() {
        return connectionRepositoryProvider.get();
    }

    @Override
    public List<Connection> availableConnections() {
        MultiValueMap<String, org.springframework.social.connect.Connection<?>> conns = getConnectionRepository().findAllConnections();

        List<Connection> connections = new ArrayList<Connection>(conns.size());
        Connection conn;
        for(String key: conns.keySet()){
            if(!conns.get(key).isEmpty()){
                for(int i=0; i<conns.get(key).size(); i++){
                    conn = new Connection(key, conns.get(key).get(i));
                    if(conn.isConnected()){
                        connections.add(conn);
                        break;
                    }else{
                        getConnectionRepository().removeConnection(conns.get(key).get(i).getKey());
                    }
                }
            }
        }
        return connections;
    }

    protected boolean hasPublishPermission(String providerId){
        SocialNetworkType type = getSocialNetworkType(providerId);
        if(type != null){
            switch (type){
                case facebook: return facebook.hasPublishPermission();
                case tumblr: return tumblr.isAuthorized();
                case twitter: return true; // todo
            }
        }

        return false;
    }

    private SocialNetworkType getSocialNetworkType(String providerId){
        SocialNetworkType type = null;
        try{
            type = SocialNetworkType.valueOf(providerId);
        }catch (Exception e){
            logger.error("Wrong SN type: "+e.getMessage());
        }

        return type;
    }

    protected Connection getConnection(String providerId){
        Connection connection = null;
        List<org.springframework.social.connect.Connection<?>> conns = getConnectionRepository().findConnections(providerId);
        if(conns != null && !conns.isEmpty()){
            connection = new Connection(providerId, conns.get(0));
        }

        return connection;
    }

    protected void resetConnection(Connection connection){
        SocialNetworkType type = getSocialNetworkType(connection.getService());
        if(type != null){
            switch (type){
                case facebook: facebook.revokeLogin(); break;
                case tumblr: break;
                case twitter: break;
            }
            getConnectionRepository().removeConnections(type.getLabel().toLowerCase());
        }
    }

    public Connection checkConnectionStatus(String providerId){
        Connection connection = null;
        if(!StringUtils.isEmpty(providerId)){
            providerId = providerId.toLowerCase();
            connection = getConnection(providerId);
            if(connection != null){
                if(!connection.isConnected() || !hasPublishPermission(providerId)){
                    resetConnection(connection);
                    connection.reset();
                }
            }
        }

        return connection;
    }

    @Override
    public List<ShareStatusBean> shareImage(List<SocialNetworkType> socialNetTypes, List<Integer> images, String message, Map<String, String> usertags){
        List<ShareStatusBean> result = new ArrayList<ShareStatusBean>();

        List<PictureBean> pictures = pictureService.getPicturesByIds(images);

        List<Resource> files = new ArrayList<Resource>();

        for(PictureBean picture : pictures){
            try{
                files.add(new FileSystemResource(pictureLogoService.getMarkedPictureFile(picture, false)));
            }catch(Exception e){
                logger.warn(e.getMessage(),e);
            }
        }

        if(socialNetTypes == null || socialNetTypes.size() == 0){
            throw new IllegalArgumentException("Field 'socialNetTypes' cannot be empty");
        }

        Map<String, SocialNetwork> settingsMap = socialNetworkService.getSocialNetworkSettings();
        Map<String, Throwable> errors = new HashMap<String, Throwable>(socialNetTypes.size());
        Map<String, String> accounts = new HashMap<String, String>(socialNetTypes.size());
        Map<String, Integer> friends = new HashMap<String, Integer>(socialNetTypes.size());

        SocialNetwork settings;

        if(socialNetTypes.contains(SocialNetworkType.facebook)){
            settings = settingsMap.get(SocialNetworkType.facebook.name());
            String albumCaption = null, albumDescription = null, address = null;
            List<String> tags = null, hashtags = null;
            if(settings != null){
                albumCaption = settings.getTitle();
                albumDescription = settings.getDescription();
                tags = parseTags(usertags.get(SocialNetworkType.facebook.name()), "@");//settings.getTagList();
                hashtags = parseTags(usertags.get(SocialNetworkType.facebook.name()), "#");//settings.getHashtagList();
                address = settings.getAddress();
            }
            logger.info("facebook sharing");
            if(facebook != null && facebook.isAuthorized()){
                try{
                    facebook.shareImagesWithFeed(albumCaption, albumDescription, files, message, tags, hashtags, address);
                    accounts.put(SocialNetworkType.facebook.name(), facebook.getUserId());
                }catch (Exception e){
                    logger.error(e.getMessage(), e);
                    errors.put(SocialNetworkType.facebook.name(), e);
                }
                /*try{
                    friends.put(SocialNetworkType.facebook.name(), facebook.getFriendsCount());
                }catch (Exception e){
                    logger.warn(e.getMessage(), e);
                }*/
            }
        }

        if(socialNetTypes.contains(SocialNetworkType.twitter)){
            settings = settingsMap.get(SocialNetworkType.twitter.name());
            String address = null;
            List<String> tags = null, hashtags = null;
            if(settings != null){
                tags = parseTags(usertags.get(SocialNetworkType.twitter.name()), "@");//settings.getTagList();
                hashtags = parseTags(usertags.get(SocialNetworkType.twitter.name()), "#");//settings.getHashtagList();
                address = settings.getAddress();
            }
            logger.info("twitter sharing");
            if(twitter != null && twitter.isAuthorized()){
                int count = 1;
                String twit = message;
                try{
                    for(Resource res: files){
                        if(count > 1){
                            twit = message + " [twit "+count+"]";
                        }
                        twitter.shareImage(twit, res, tags, hashtags, address);
                        count++;
                    }
                    accounts.put(SocialNetworkType.twitter.name(), twitter.getUserName());
                }catch (Exception e){
                    logger.error(e.getMessage(), e);
                    errors.put(SocialNetworkType.twitter.name(), e);
                }
                try{
                    friends.put(SocialNetworkType.twitter.name(), twitter.getFriendAndFollowersCount());
                }catch (Exception e){
                    logger.warn(e.getMessage(), e);
                }
            }
        }

        if(socialNetTypes.contains(SocialNetworkType.tumblr)){
            settings = settingsMap.get(SocialNetworkType.tumblr.name());
            List<String> hashtags = null;
            if(settings != null){
                hashtags = parseTags(usertags.get(SocialNetworkType.tumblr.name()), "#");//settings.getHashtagList();
            }
            logger.info("tumblr sharing");
            if(tumblr != null && tumblr.isAuthorized()){
                try{
                    tumblr.shareImages(message, files, hashtags);
                    accounts.put(SocialNetworkType.tumblr.name(), tumblr.getUserId());
                }catch (Exception e){
                    logger.error(e.getMessage(), e);
                    errors.put(SocialNetworkType.tumblr.name(), e);
                }
                try{
                    friends.put(SocialNetworkType.tumblr.name(), tumblr.getFollowersCount());
                }catch (Exception e){
                    logger.warn(e.getMessage(), e);
                }
            }
        }

        logger.info("DONE");

        logger.info("frieds"+ friends);
        ShareStatusBean status;
        for(SocialNetworkType type: socialNetTypes){
            if(errors.containsKey(type.name())){
                status = new ShareStatusBean(type.name(), errors.get(type.name()));
            }else{
                status = new ShareStatusBean(type.name(), accounts.get(type.name()), friends.get(type.name()));
            }
            result.add(status);
        }

        return result;

        /*if(errors.size() > 0){
            RuntimeException e = new ShareImageException(errors);
            logger.error(e.getMessage(), e);
            throw e;
        }*/
    }

    @Override
    public void shareImageViaEmail(String[] emails, List<Integer> images, String message, String redirectUrl) {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getCredentials().toString();
        logger.info("Current User: " + currentUser);
        String userBase = currentUser;

        SystemUser user = userService.getUserByLogin(currentUser);
        if (user != null && user.getLoginGroup() != null && !user.getLoginGroup().isEmpty()) {
            userBase = user.getLoginGroup();
        }

        LocationBean location = locationService.getLocation(user.getLocationId());

        String fromEmail = "";
        if(location != null && location.getFromEmail() != null){
            fromEmail = location.getFromEmail();
        }

        Map<String, Throwable> errors = new HashMap<String, Throwable>(emails.length);

        if (emails != null && emails.length > 0) {
            List<String> userLogins = userService.generateNewUserLogins(userBase, emails.length, getImageListString(images));

            String login, subject, messageBody;
            int j = 0;
            for (String email : emails) {
                login = userLogins.get(j);

                subject = getMessageSubject(location);
                messageBody = getMessageBody(message, redirectUrl, login, location);

                try{
                    mailService.sendMail(fromEmail, subject, messageBody, email);
                    logger.info("Sent to " + email);
                }catch (Exception e){
                    logger.error(e.getMessage(), e);
                    errors.put(email, e);
                }
                j++;
            }
        }

        if(!errors.isEmpty()){
            throw new RuntimeException(errors.toString());
        }
    }

    @Override
    public String createUserForSocialShare (final List<Integer> imageIds) {
        final String currentUser = SecurityContextHolder.getContext ().getAuthentication ().getCredentials ().toString ();

        logger.info ("Current User: " + currentUser);

        String userBase = currentUser;

        SystemUser user = userService.getUserByLogin (currentUser);

        if (user != null && user.getLoginGroup () != null && ! user.getLoginGroup ().isEmpty ()) {
            userBase = user.getLoginGroup ();
        }

        if (imageIds != null && imageIds.size () > 0) {
            final List<String> result = userService.generateNewUserLogins (userBase, 1, getImageListString (imageIds));

            if (! result.isEmpty ()) {
                return result.get (0);
            }
        }

        return null;
    }

    private String getImageListString(List<Integer> images) {   // todo remove
        String imageString = "";
        for (Integer image : images) {
            if(imageString.length() != 0){
                imageString += ", ";
            }
            imageString += "id_" + image;
        }
        return imageString;
    }

    private String getMessageSubject(LocationBean location){
        String name = "";
        if(location != null && location.getName() != null){
            name = location.getName();
        }

        return messageSource.getMessage("mail.subject", new Object[]{ name }, Locale.ROOT);
    }

    private String getMessageBody(String message, String redirectUrl, String login, LocationBean location){
        String footer = "";
        if(location != null && location.getEmailFooter() != null){
            footer = location.getEmailFooter();
        }

        return messageSource.getMessage("mail.template", new Object[]{ message, redirectUrl + login, login, footer }, Locale.ROOT);
    }

    @Override
    public void removeAllConnections(){
        MultiValueMap<String, org.springframework.social.connect.Connection<?>> conns = getConnectionRepository().findAllConnections();

        for(String key: conns.keySet()){
            getConnectionRepository().removeConnections(key);
        }
    }

    @Override
    public boolean checkFacebookTag(String tag) {
        return facebook.isPageExisted(tag);
    }

    private List parseTags(String tagstr, String prefix){
        List<String> tags = new ArrayList<String>();

        if(tagstr != null){
            String[] tokens = tagstr.split("[, ]");
            for(String t: tokens){
                if(!t.isEmpty()){
                    if(t.startsWith(prefix)){
                        tags.add(t.substring(prefix.length()));
                    }
                }
            }
        }
        return tags;
    }

}