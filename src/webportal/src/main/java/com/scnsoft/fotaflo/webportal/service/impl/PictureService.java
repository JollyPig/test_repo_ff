package com.scnsoft.fotaflo.webportal.service.impl;

import java.io.File;
import java.net.URL;
import java.util.*;

import com.scnsoft.fotaflo.webportal.bean.PictureBean;
import com.scnsoft.fotaflo.webportal.dao.IPicturesDao;
import com.scnsoft.fotaflo.webportal.model.Picture;
import com.scnsoft.fotaflo.webportal.model.SystemUser;
import com.scnsoft.fotaflo.webportal.service.IPictureService;
import com.scnsoft.fotaflo.webportal.service.IUserService;
import com.scnsoft.fotaflo.webportal.service.util.UserContextHolder;
import com.scnsoft.fotaflo.webportal.ws.IBeanWrapper;
import com.scnsoft.fotaflo.webportal.ws.IPictureWebService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.UrlResource;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * A custom service for retrieving users from a custom datasource, such as a database.
 * <p/>
 * This custom service must implement Spring's {@link UserDetailsService}
 */
@Service
@Transactional
public class PictureService implements IPictureService {
    protected static Logger logger = Logger.getLogger(PictureService.class);

    @Autowired
    private IPicturesDao pictureDao;

    @Autowired
    private IUserService userService;

    @Autowired
    private IPictureWebService pictureWebService;

    @Autowired
    private UserContextHolder  userContextHolder;

    /**
     * Retrieves user pictures.
     */
    public List<PictureBean> getPictures(String user, boolean forceReload) {
        List<PictureBean> picturesList = new ArrayList<PictureBean>();
        SystemUser systemUser = userService.getUserByLogin(user);

        if(systemUser == null){
            throw new RuntimeException("No user in database with login: " + user);
        }

        if((systemUser.getLastAccessDate() == null || forceReload) && (systemUser.getLoginGroup() == null || systemUser.getLoginGroup().isEmpty())){
            getPicturesFromWebService(systemUser);
            userService.updateUserLastAccessDate(systemUser);
            systemUser = userService.getUserByLogin(systemUser.getLoginName());
        }

        List<Picture> picturesLocal = systemUser.getPictures();

        if(picturesLocal != null) {
            for(Picture picture: picturesLocal){
                if(picture!=null){
                    picturesList.add(new PictureBean(picture));
                }
            }
        }

        return picturesList;
    }

    /**
     * Retrieves user pictures.
     */
    public List<PictureBean> getPictures(String user) {
        return getPictures(user, false);
    }

    protected List<PictureBean> getPictureBeanListFromPicturesSet(List<Picture> pictures) {
        List<PictureBean> pictureBeans = new ArrayList<PictureBean>();
        if(pictures != null) {
            for(Picture picture: pictures){
                if(picture!=null){
                   pictureBeans.add(new PictureBean(picture));
                }
            }
        }

        return pictureBeans;
    }

    @Override
    public PictureBean getPicture(int id) {
        Picture pic = pictureDao.get(id);
        if(pic != null){
            return new PictureBean(pic);
        }
        return null;
    }

    @Override
    public List<PictureBean> getPicturesByIds(List<Integer> ids) {
        List<PictureBean> pictures = new ArrayList<PictureBean>();
        PictureBean pic;
        if(ids != null){
            for(Integer id: ids){
                pic = this.getPicture(id);
                if(pic != null){
                    pictures.add(pic);
                }
            }
        }

        return pictures;
    }

    @Override
    public File getPictureById (final Integer pictureId) throws Exception {
        final Picture entity = pictureDao.get (pictureId);

        if (entity != null) {
            logger.info ("userContextHolder " + userContextHolder);
            logger.info ("server " + entity.getServerId ());
            logger.info ("url " + entity.getSmall_url ());

            if (entity.getServerId () != null) {
                URL url = new URL (userContextHolder.getRemotePicturePath (entity.getServerId ()) + entity.getSmall_url ());

                return new UrlResource (url).getFile ();
            }
        }

        return null;
    }

    protected List<PictureBean> getPicturesFromWebService(final SystemUser user){
        return pictureWebService.getPictures(user.getLoginName(), user.getLastAccessDate(), new IBeanWrapper() {
            @Override
            public Object prepare(Object bean) {
                savePicture((PictureBean)bean, user);
                return bean;
            }
        });
    }

    protected boolean savePicture(PictureBean pictureBean, SystemUser user){
        if(pictureBean == null || user == null){
            return false;
        }
        Picture picture = pictureDao.getPictureByExternalId(pictureBean.getId(), user.getServerNumber());
        if(picture==null){
            picture = pictureBean.toEntity(user);
            picture.setPictureId(pictureBean.getId());
            Integer newId = pictureDao.create(picture);
            pictureBean.setId(newId);
        } else{
            picture.getSystemUsers().add(user);
            picture.setUrl(pictureBean.getUrl());
            picture.setSmall_url(pictureBean.getBase64code());
            picture.setLogoUrl(pictureBean.getLogoUrl());
            picture.setLogoText(pictureBean.getLogoText());
            picture.setLogoMainUrl(pictureBean.getLogoMainUrl());
            pictureDao.update(picture);
            pictureBean.setId(picture.getId());
        }
        return true;
    }
}
