package com.scnsoft.fotaflo.webportal.service.exception;

import java.util.Map;

public class ShareImageException extends ServiceException {

    private Map<String, Throwable> errors;

    public ShareImageException() {
    }

    public ShareImageException(String message) {
        super(message);
    }

    public ShareImageException(String message, Map<String, Throwable> errors) {
        super(message);
        this.errors = errors;
    }

    public ShareImageException(Map<String, Throwable> errors) {
        this.errors = errors;
    }

    public Map<String, Throwable> getErrors() {
        return errors;
    }

    public void setErrors(Map<String, Throwable> errors) {
        this.errors = errors;
    }

    private String getErrorsMessage(Map<String, Throwable> errors){
        String msg = null;
        if(errors != null && !errors.isEmpty()){
            msg = "Errors while sharing image on ";
            for(String key: errors.keySet()){
                msg += key + ", ";
            }
            msg = msg.substring(0, msg.length()-2);
        }
        return msg;
    }

    @Override
    public String getMessage() {
        if(super.getMessage() != null){
            return super.getMessage();
        }else{
            return getErrorsMessage(errors);
        }
    }
}
