package com.scnsoft.fotaflo.webportal.service.converter;

import com.scnsoft.fotaflo.webportal.analytics.AnalyticData;
import com.scnsoft.fotaflo.webportal.report.model.ColumnHeader;
import com.scnsoft.fotaflo.webportal.report.model.ReportData;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DeviceTypeDataConverter extends AnalyticDataConverter {
    @Override
    public ReportData getReportData(AnalyticData data) {
        List<ColumnHeader> columns = new ArrayList<ColumnHeader>();

        columns.add(new ColumnHeader("desktop", "Desktop", ColumnHeader.ColumnType.VALUE, ColumnHeader.ColumnDataType.NUMBER, 15));
        columns.add(new ColumnHeader("mobile", "Mobile", ColumnHeader.ColumnType.VALUE, ColumnHeader.ColumnDataType.NUMBER, 15));
        columns.add(new ColumnHeader("tablet", "Tablet", ColumnHeader.ColumnType.VALUE, ColumnHeader.ColumnDataType.NUMBER, 15));

        return getSimpleReportData(data, columns, "Device_Type");
    }
}
