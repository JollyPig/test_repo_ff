package com.scnsoft.fotaflo.webportal.dao;

import com.scnsoft.fotaflo.common.dao.IDao;
import com.scnsoft.fotaflo.webportal.model.Merchant;

public interface IMerchantDao extends IDao<Merchant, Integer> {

}
