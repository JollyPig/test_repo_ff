package com.scnsoft.fotaflo.webportal.dto;

public class ProgressDto {

    private String id;
    private Float progress;

    public ProgressDto() {
    }

    public ProgressDto(String id, Float progress) {
        this.id = id;
        this.progress = progress;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Float getProgress() {
        return progress;
    }

    public void setProgress(Float progress) {
        this.progress = progress;
    }

    @Override
    public String toString() {
        return "{" +
                id + ": " + progress +
                '}';
    }
}
