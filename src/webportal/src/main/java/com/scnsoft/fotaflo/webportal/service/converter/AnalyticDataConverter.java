package com.scnsoft.fotaflo.webportal.service.converter;

import com.scnsoft.fotaflo.webportal.analytics.AnalyticData;
import com.scnsoft.fotaflo.webportal.bean.LocationBean;
import com.scnsoft.fotaflo.webportal.report.model.ColumnHeader;
import com.scnsoft.fotaflo.webportal.report.model.ReportData;
import com.scnsoft.fotaflo.webportal.service.ILocationService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public abstract class AnalyticDataConverter {
    protected static final Logger logger = Logger.getLogger(AnalyticDataConverter.class);

    protected static final SimpleDateFormat formatter = new SimpleDateFormat("MM.dd.yyyy");

    @Autowired
    ILocationService locationService;

    public abstract ReportData getReportData(AnalyticData data);

    public LocationBean getLocation(Integer id){
        return locationService.getLocation(id);
    }

    protected DateRange getRange(Integer id){
        DateRange range = null;
        if(id != null){
            for(DateRange v: DateRange.values()){
                if(id.equals(v.getId())){
                    range = v;
                    break;
                }
            }
        }
        return range;
    }

    protected ReportData getSimpleReportData(AnalyticData o, List<ColumnHeader> adds, String type){
        Map result = new HashMap();
        if(o == null || o.getStartDate() == null || o.getEndDate() == null || o.getRange() == null){
            throw new IllegalArgumentException("'analyticData' must be defined");
        }
        Date startDate = o.getStartDate(),
            endDate = o.getEndDate();
        DateRange range = getRange(o.getRange());

        if(range == null){
            throw new IllegalArgumentException("'range' is invalid");
        }

        // columns definition
        List<ColumnHeader> columns = new ArrayList<ColumnHeader>();
        int dateFieldCount = addHeaderDateColumns(columns, range);
        columns.addAll(adds);

        if(o.getRows() != null){
            Integer locId = null;
            LocationBean loc;
            Map locData = null, dateData = null;
            String locDate = null, ds;
            Date date = null;
            for(List<String> list: o.getRows()){
                if(!new Integer(list.get(0)).equals(locId)){
                    locId = new Integer(list.get(0));
                    try{
                        loc = getLocation(locId);
                    }catch(Exception e){
                        logger.error(e.getMessage(), e);
                        loc = null;
                    }
                    if(loc == null){
                        logger.warn("No location with id: "+locId);
                        continue;
                    }
                    locData = new TreeMap();
                    result.put(loc.getName(), locData);
                    locDate = null;
                }

                ds = list.get(1);
                for(int i=1; i<dateFieldCount; i++){
                    ds += list.get(i+1);
                }

                if(!ds.equals(locDate)){
                    try{
                        date = new SimpleDateFormat(range.getPattern()).parse(ds);
                    }catch(ParseException e){
                        logger.warn(e.getMessage(), e);
                    }
                    if(date == null){
                        continue;
                    }
                    locDate = ds;

                    dateData = new HashMap();
                    locData.put(date, dateData);
                    addDateColumns(dateData, range, date);
                }

                String key = list.get(dateFieldCount+1);
                int value = new Integer(list.get(dateFieldCount+2));

                dateData.put(key, value);
            }
        }

        ReportData data = new ReportData();
        data.setType(type);
        data.setRange(formatter.format(startDate) + "-" + formatter.format(endDate));
        data.setColumns(columns);
        data.setData(result);

        return data;
    }

    protected int addHeaderDateColumns(List<ColumnHeader> columns, DateRange range){
        if(Arrays.asList(DateRange.YEAR, DateRange.MONTH, DateRange.WEEK).contains(range)){
            columns.add(new ColumnHeader("year", "Year", ColumnHeader.ColumnDataType.NUMBER, 10));
        }
        if(Arrays.asList(DateRange.MONTH,DateRange.WEEK).contains(range)){
            columns.add(new ColumnHeader("month", "Month", ColumnHeader.ColumnDataType.LABEL, 15));
        }
        if(Arrays.asList(DateRange.WEEK).contains(range)){
            columns.add(new ColumnHeader("week", "Week", ColumnHeader.ColumnDataType.NUMBER, 10));
        }
        columns.add(new ColumnHeader("date", "Dates", ColumnHeader.ColumnDataType.LABEL, 20));

        int dateFieldCount;
        switch (range){
            case DAY: dateFieldCount = 1;break;
            case WEEK: dateFieldCount = 2;break;
            case MONTH: dateFieldCount = 2;break;
            case YEAR: dateFieldCount = 1;break;
            default: dateFieldCount = 1;
        }

        return dateFieldCount;
    }

    protected int addDateColumns(Map data, DateRange range, Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        data.put("year", calendar.get(Calendar.YEAR));
        data.put("month", DateFormatSymbols.getInstance().getMonths()[calendar.get(Calendar.MONTH)]);
        data.put("week", calendar.get(Calendar.WEEK_OF_YEAR));

        calendar.roll(range.getId(), true);
        calendar.add(Calendar.DATE, -1);

        if(date.equals(calendar.getTime())){
            data.put("date", formatter.format(date));
        }else{
            data.put("date", formatter.format(date) + "-" +formatter.format(calendar.getTime()));
        }

        return data.values().size();
    }

    public enum DateRange{
        DAY     (Calendar.DATE, "yyyyMMdd"),
        WEEK    (Calendar.WEEK_OF_YEAR, "yyyyww"),
        MONTH   (Calendar.MONTH, "yyyyMM"),
        YEAR    (Calendar.YEAR, "yyyy");

        private int id;
        private String pattern;

        DateRange(int id, String pattern){
            this.id = id;
            this.pattern = pattern;
        }

        int getId() {
            return id;
        }

        String getPattern() {
            return pattern;
        }
    }
}
