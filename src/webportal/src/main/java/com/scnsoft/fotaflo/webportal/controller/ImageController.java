package com.scnsoft.fotaflo.webportal.controller;

import com.scnsoft.fotaflo.common.util.NumberUtils;
import com.scnsoft.fotaflo.webportal.bean.PictureBean;
import com.scnsoft.fotaflo.webportal.service.IPictureLoadService;
import com.scnsoft.fotaflo.webportal.service.IPictureLogoService;
import com.scnsoft.fotaflo.webportal.service.IPictureService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;

@Controller
public class ImageController {
    protected final static Logger logger = Logger.getLogger(ImageController.class);

    @Autowired
    private IPictureLoadService pictureLoadService;

    @Autowired
    private IPictureLogoService pictureLogoService;

    @Autowired
    private IPictureService pictureService;

    @RequestMapping(value = "/img_small/**", method = RequestMethod.GET)
    public void getImage(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        resp.addHeader("Content-Type", "image/jpg");

        try{
            if(pictureLogoService.isImageCached(req.getPathInfo())){
                pictureLogoService.writeImage(req.getPathInfo(), resp.getOutputStream());
            }else{
                PictureBean picture = getPicture(req.getParameter("id"));
                if(picture != null){
                    pictureLogoService.writeMarkedPicture(picture, resp.getOutputStream(), true);
                }else{
                    pictureLoadService.loadPicture(req.getPathInfo(), resp.getOutputStream());
                }
            }
        }catch(FileNotFoundException e){
            logger.error("FileNotFoundException: " + e.getMessage());
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
        resp.getOutputStream().close();
    }

    @RequestMapping(value = "/img/**", method = RequestMethod.GET)
    public void getFullsizeImage(final HttpServletRequest req, final HttpServletResponse resp) throws Exception {
        resp.addHeader("Content-Type", "image/jpg");

        try{
            if(pictureLogoService.isImageCached(req.getPathInfo())){
                pictureLogoService.writeImage(req.getPathInfo(), resp.getOutputStream());
            }else{
                PictureBean picture = getPicture(req.getParameter("id"));
                if(picture != null){
                    pictureLogoService.writeMarkedPicture(picture, resp.getOutputStream(), true);
                }else{
                    pictureLoadService.loadPicture(req.getPathInfo(), resp.getOutputStream());
                }
            }
        }catch(FileNotFoundException e){
            logger.error("FileNotFoundException: " + e.getMessage());
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
        }

        resp.getOutputStream().close();
    }

    private PictureBean getPicture(String param){
        PictureBean picture = null;
        if(param != null && !param.isEmpty()){
            Integer id = NumberUtils.getInteger(param);
            if(id != null){
                picture = pictureService.getPicture(id);
            }
        }

        return picture;
    }

}
