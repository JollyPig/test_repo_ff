package com.scnsoft.fotaflo.webportal.dao;
import com.scnsoft.fotaflo.webportal.model.SystemUser;

import java.util.List;
import com.scnsoft.fotaflo.common.dao.IDao;

/**
 * Created by IntelliJ IDEA.
 * User: Paradinets
 * Date: 30.03.2012
 * Time: 12:21:37
 * To change this template use File | Settings | File Templates.
 */
public interface ISystemUserDao extends IDao<SystemUser, Integer> {
    
    public SystemUser getUserByLogin(String login);

    public List<SystemUser> getUserByLoginGroup(String loginGroup);

}
