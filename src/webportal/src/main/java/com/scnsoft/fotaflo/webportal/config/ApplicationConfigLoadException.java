package com.scnsoft.fotaflo.webportal.config;

import com.scnsoft.fotaflo.webportal.service.exception.ServiceException;

/** Exception that throws when loading application configuration fails */
public class ApplicationConfigLoadException extends ServiceException {
    public ApplicationConfigLoadException() {
    }

    public ApplicationConfigLoadException(String message) {
        super(message);
    }
}
