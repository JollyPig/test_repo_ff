package com.scnsoft.fotaflo.webportal.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.scnsoft.fotaflo.common.model.AbstractEntity;

@Entity
@Table(name = "systemuser")
public class SystemUser extends AbstractEntity {

    String loginName;
    String loginGroup;
    Integer loginGroupNumber;
    String password;
    String access;
    String accessCode;
    String pictureIds;

    Integer locationId;
    String currency;
    String price;
    String packagePrice;
    Integer serverNumber;
    private List<Picture> pictures = new ArrayList<Picture>();

    private Date lastAccessDate;

    @ManyToMany(mappedBy = "systemUsers", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinTable(name = "users_pictures", joinColumns = {@JoinColumn(name = "system_user_id")}, inverseJoinColumns = {@JoinColumn(name = "picture_id")})
    @OrderBy("creationDate DESC")
    public List<Picture> getPictures() {
        return pictures;
    }

    public void setPictures(List<Picture> pictures) {
        this.pictures = pictures;
    }

    /**
     * @return Returns the loginName.
     */
    @Column(name = "login_name", length = 64)
    public String getLoginName() {
        return loginName;
    }

    /**
     * @param loginName The loginName to set.
     */
    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    @Column(name = "login_group", length = 64)
    public String getLoginGroup() {
        return loginGroup;
    }

    public void setLoginGroup(String loginGroup) {
        this.loginGroup = loginGroup;
    }

    @Column(name = "group_number", length = 64)
    public Integer getLoginGroupNumber() {
        return loginGroupNumber;
    }

    public void setLoginGroupNumber(Integer loginGroupNumber) {
        this.loginGroupNumber = loginGroupNumber;
    }

    /**
     * @return Returns the password.
     */
    @Column(name = "password", length = 64)
    public String getPassword() {
        return password;
    }

    /**
     * @param password The password to set.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return Returns the access.
     */
    @Column(name = "access", length = 10)
    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    @Column(name = "picture_ids", length = 1024)
    public String getPictureIds() {
        return pictureIds;
    }

    public void setPictureIds(String pictureIds) {
        this.pictureIds = pictureIds;
    }

    @Column(name = "location_id")
    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    @Column(name = "accessCode", length = 10)
    public String getAccessCode() {
        return accessCode;
    }


    public void setAccessCode(String accessCode) {
        this.accessCode = accessCode;
    }

    @Column(name = "price", length = 10)
    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
    @Column(name = "currency", length = 10)
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Column(name = "serverNumber", length = 10)
    public Integer getServerNumber() {
        return serverNumber;
    }

    public void setServerNumber(Integer serverNumber) {
        this.serverNumber = serverNumber;
    }

    @Column(name = "last_access_date")
    public Date getLastAccessDate() {
        return lastAccessDate;
    }

    public void setLastAccessDate(Date lastAccessDate) {
        this.lastAccessDate = lastAccessDate;
    }

    @Column(name = "packagePrice")
    public String getPackagePrice() {
        return packagePrice;
    }

    public void setPackagePrice(String packagePrice) {
        this.packagePrice = packagePrice;
    }
}
