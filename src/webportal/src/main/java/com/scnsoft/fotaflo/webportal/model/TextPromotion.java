package com.scnsoft.fotaflo.webportal.model;

import javax.persistence.*;
import com.scnsoft.fotaflo.common.model.AbstractEntity;

@Entity
@Table(name = "text_promotion")
public class TextPromotion extends AbstractEntity {

    private Integer number;

    private String caption;

    private String text;

    public TextPromotion() {
    }

    public TextPromotion(Integer number, String caption, String text) {
        this.number = number;
        this.caption = caption;
        this.text = text;
    }

    @Column(name = "number", nullable = false, unique = true)
    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    @Column(name = "caption")
    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    @Lob
    @Column(name = "text", length=10000)
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
