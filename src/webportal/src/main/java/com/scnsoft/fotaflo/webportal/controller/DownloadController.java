package com.scnsoft.fotaflo.webportal.controller;

import com.scnsoft.fotaflo.common.analytics.ga.util.CookieManager;
import com.scnsoft.fotaflo.common.util.StringUtils;
import com.scnsoft.fotaflo.webportal.bean.LocationBean;
import com.scnsoft.fotaflo.webportal.service.IAnalyticsService;
import com.scnsoft.fotaflo.webportal.service.IDownloadService;
import com.scnsoft.fotaflo.webportal.service.ILocationService;
import com.scnsoft.fotaflo.webportal.service.exception.ServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/main/download")
public class DownloadController {
    protected final static Logger logger = Logger.getLogger(DownloadController.class);

    @Autowired
    private IDownloadService downloadService;

    @Autowired
    private ILocationService locationService;

    @Autowired
    private IAnalyticsService analyticsService;

    @RequestMapping(value = "/images", method = RequestMethod.POST)
    public void getImageArchieve(@RequestParam(value = "name", required = false) String name, @RequestParam(value = "images", required = false) String images,
                                 HttpServletResponse response, HttpServletRequest request) throws IOException{
        String filename = "fotaflo";
        List<Integer> imgIds = null;
        logger.info("Name: "+name + " => " + "Images: "+images);

        LocationBean location = locationService.getCurrentUserLocation();
        if(location != null && !StringUtils.isEmpty(location.getName())){
            filename = location.getName();
        }

        /*if(name != null){
            filename = name;
        }*/

        if(images != null){
            imgIds = new ArrayList<Integer>();
            Integer id;
            for(String img: images.split(",")){
                if(img.matches("[\\d]+")){
                    id = new Integer(img);
                    if(id != null){
                        imgIds.add(id);
                    }
                }
            }
        }

        response.setHeader("Content-Disposition", "attachment; filename=" + filename + ".zip");
        response.addHeader("Content-Type", "application/zip");

        String error = null;
        try{
            downloadService.writeZipArchiveByPictureIds(imgIds, filename, response.getOutputStream());

            response.getOutputStream().close();
        }catch (ServiceException e){
            logger.error(e.getMessage());
            error = e.getMessage();
        }

        logger.info("Error: "+error);
        if(error != null){
            analyticsService.trackEvent(CookieManager.getClientId(request.getCookies()), "download", "error", "corrupted", 0, error);
        }else{
            analyticsService.trackEvent(CookieManager.getClientId(request.getCookies()), "download", "success", "downloaded", imgIds.size());
        }
    }

}
