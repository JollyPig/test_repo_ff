package com.scnsoft.fotaflo.webportal.service.pb;

import org.apache.log4j.Logger;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/** Task Progress bar manager */
public class ProgressManager {
    protected static final Logger logger = Logger.getLogger(ProgressManager.class);

    /** Period after that completed tasks can be deleted */
    public final static long EXPIRATION_PERIOD = 2 * 6 * 1000;

    private Map<String, Task> statusMap = new TreeMap<String, Task>();

    /**
     * Change progress for the task by adding some value
     *
     * @param id the id for the task
     * @param progress float value to add
     *
     * @return the id for the task
     * */
    public synchronized String addProgress(String id, float progress){
        if(id != null){
            Task task = statusMap.get(id);
            if(task == null){
                task = new Task(id);
                statusMap.put(id, task);
            }

            task.addStatus(progress);
        }

        return id;
    }

    /**
     * Get progress value for the task
     *
     * @param id the id for the task
     *
     * @return progress of the task
     * */
    public synchronized Float getProgress(String id){
        Float pr = null;
        if(id != null){
            Task task = statusMap.get(id);
            if(task != null){
                pr = task.getStatus();
            }
        }

        return pr;
    }

    /**
     * Get all progresses
     *
     * @return the map of tasks
     * */
    public synchronized Map<String, Float> getProgressMap(){
        Map<String, Float> map = new TreeMap<String, Float>();
        Task s;
        for(String key: statusMap.keySet()){
            s = statusMap.get(key);
            if(s != null){
                map.put(key, s.getStatus());
            }
        }

        return map;
    }

    /**
     * Delete all expired tasks
     * */
    public synchronized void deleteExpiredProgresses(){
        Task s;
        Iterator<String> it = statusMap.keySet().iterator();
        long ct = System.currentTimeMillis();
        while(it.hasNext()){
            String key = it.next();
            s = statusMap.get(key);
            if(s != null && s.getExpired() > 0 && s.getExpired() <= ct){
                it.remove();
                statusMap.remove(key);
            }
        }
    }

    /**
     * Inner class which encapsulates Task entity
     * */
    private class Task implements Comparable<Task>{
        protected final float completedValue = 100.0f;

        private String id;
        private long created = System.currentTimeMillis();
        private boolean completed;
        private float status;
        private long expired;

        Task(String id) {
            this.id = id;
        }

        String getId() {
            return id;
        }

        long getCreated() {
            return created;
        }

        private boolean isCompleted() {
            return completed;
        }

        float getStatus() {
            return status;
        }

        void addStatus(float status) {
            if(!completed){
                this.status += status;
                checkIsCompleted();
            }
        }

        long getExpired() {
            return expired;
        }

        void setExpired(long expired) {
            this.expired = expired;
        }

        private void checkIsCompleted(){
            if(status >= completedValue && !completed){
                completed = true;
                status = completedValue;
                expired = System.currentTimeMillis() + EXPIRATION_PERIOD;
            }
        }

        @Override
        public int compareTo(Task o) {
            if(this == o){
                return 0;
            }
            if(this.id != null && o.id != null && this.id.equals(o.id)){
                return 0;
            }

            return (int)(this.created - o.created);
        }
    }

}
