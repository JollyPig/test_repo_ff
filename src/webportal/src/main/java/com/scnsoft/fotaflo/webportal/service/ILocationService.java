package com.scnsoft.fotaflo.webportal.service;

import com.scnsoft.fotaflo.webportal.bean.LocationBean;

import java.util.List;

public interface ILocationService {

    List<LocationBean> getLocations();

    LocationBean getLocation(int id);

    LocationBean getCurrentUserLocation();

}
