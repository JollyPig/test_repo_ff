package com.scnsoft.fotaflo.webportal.dao.impl;

import com.scnsoft.fotaflo.common.dao.AbstractDao;
import com.scnsoft.fotaflo.webportal.dao.ITextPromotionDao;
import com.scnsoft.fotaflo.webportal.model.TextPromotion;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TextPromotionDao extends AbstractDao<TextPromotion, Integer> implements ITextPromotionDao {

    @Override
    protected Class<TextPromotion> getDomainClass() {
        return TextPromotion.class;
    }

    @Override
    public TextPromotion getByNumber(Integer number){
        List<TextPromotion> list = getSession().createCriteria(getDomainClass())
                .add(Restrictions.eq("number", number))
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .list();
        if(!list.isEmpty()){
            return list.get(0);
        }
        return null;
    }
}
