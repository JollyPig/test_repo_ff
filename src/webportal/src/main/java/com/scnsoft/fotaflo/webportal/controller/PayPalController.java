package com.scnsoft.fotaflo.webportal.controller;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 22.04.14
 * Time: 17:09
 * To change this template use File | Settings | File Templates.
 */

import com.scnsoft.fotaflo.common.analytics.ga.util.CookieManager;
import com.scnsoft.fotaflo.common.bean.AjaxResponse;
import com.scnsoft.fotaflo.common.util.ServletUtil;
import com.scnsoft.fotaflo.common.util.StringUtils;
import com.scnsoft.fotaflo.webportal.bean.LocationBean;
import com.scnsoft.fotaflo.webportal.bean.PictureBean;
import com.scnsoft.fotaflo.webportal.dto.PaymentDto;
import com.scnsoft.fotaflo.webportal.model.Payment;
import com.scnsoft.fotaflo.webportal.model.SystemUser;
import com.scnsoft.fotaflo.webportal.service.*;
import com.scnsoft.fotaflo.webportal.service.exception.PaymentValidationException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Controller
public class PayPalController {
    protected static Logger logger = Logger.getLogger(PayPalController.class);

    @Autowired
    private IUserService userService;

    @Autowired
    private IPayPalService payPalService;

    @Autowired
    private IPictureService pictureService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private IMailService mailService;

    @Autowired
    private IAnalyticsService analyticsService;

    @Autowired
    private ILocationService locationService;

    public final static String successProperty = "success";
    public final static String msgProperty = "msg";
    public final static String payedProperty = "payed";

    private static String CONFIRM_REDIRECT_URL = "/pictures/main/commonPayed";
    private static String CANCEL_REDIRECT_URL = "/pictures/main/commonPayed";

    @RequestMapping(value = "registerPayment.json", method = RequestMethod.POST)
    public
    @ResponseBody
    ResponseEntity<AjaxResponse> registerPayment(HttpServletRequest request, @RequestBody PaymentDto dto) throws Exception {
        AjaxResponse response;

        String contextPath = ServletUtil.getContextPath(request);

        String confirmRedirectUrl = contextPath + CONFIRM_REDIRECT_URL;
        String cancelRedirectUrl = contextPath + CANCEL_REDIRECT_URL;

        try{
            String redirectUrl = payPalService.initiatePayment(dto, confirmRedirectUrl, cancelRedirectUrl);
            if(redirectUrl != null){
                response = new AjaxResponse(true, redirectUrl);
                response.setData(redirectUrl);
                logger.info("PayPal redirect url: " + redirectUrl);
            } else {
                String error = "Error occurs during getting payment token. Please contact the administrator";
                logger.error(error);
                response = new AjaxResponse(false, error);
            }
        }catch (PaymentValidationException e){
            String error = "Payment validation error: " + e.getMessage() + " Please contact the administrator.";
            logger.error(error);
            response = new AjaxResponse(false, error);
        }

        return new ResponseEntity<AjaxResponse>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/main/commonPayed")
    public String getPurchasePaymentPage(ModelMap model, String token, String PayerID, HttpServletRequest request) {
        String contextPath = ServletUtil.getContextPath(request);

        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        SystemUser systemUser = userService.getUserByLogin(currentUser);
        String price = systemUser.getPrice() == null ? "0" : systemUser.getPrice();
        model.put("price", price);
        model.put("packagePrice", systemUser.getPackagePrice());

        logger.info("User [ " + currentUser + " ] opened a PAYED catalog page");

        List<PictureBean> pictures = pictureService.getPictures(currentUser);
        model.addAttribute("pictures", pictures);

        if (StringUtils.isEmpty(PayerID) || StringUtils.isEmpty(token)) {
            logger.warn("Payment was not registered. Please contact administrator.");
            model.put(msgProperty, "Payment was not registered. (PayerId or token is empty). Please contact administrator.");
            model.put(payedProperty, false);
            model.put(successProperty, false);
            return "catalogNotLogin";
        }

        Payment payment = payPalService.getRegisteredPayment(token);
        // todo refactor validation
        if (payment == null) {
            logger.warn("Payment was not registered. Please contact administrator.");
            model.put(msgProperty, "Payment was not registered. Please contact administrator.");
            model.put(payedProperty, false);
            model.put(successProperty, false);
            return "catalogNotLogin";
        }

        if (payment.getTransactionID() != null && payment.getStatus() != null && payment.getStatus().equals(Payment.StatusCode.COMPLETE.toString())) {
            model.put(payedProperty, false);
            model.put(successProperty, true);
            return "catalogNotLogin";
        }

        if (payment.getCurrency() == null || payment.getPrice() == null || payment.getInvoiceId() == null) {
            logger.warn("Payment was not registered. Please contact administrator.");
            model.put(msgProperty, "Payment was not registered. (Price, invoiceId or currency problems) Please contact administrator.");
            model.put(payedProperty, false);
            model.put(successProperty, false);
            return "catalogNotLogin";
        }

        String newLogin = payPalService.completePayment(token);
        if (newLogin == null) {
            logger.warn("Payment was not registered. Please contact administrator.");
            model.put(msgProperty, "Something went wrong. Please contact the administrator.");
            model.put(successProperty, false);
            model.put(payedProperty, false);
        } else {
            LocationBean location = locationService.getLocation(systemUser.getLocationId());       // todo move to service

            String locationName = "", locationFooter = "";

            if (location != null) {
                locationName = location.getName() != null ? location.getName() : "";
                locationFooter = location.getEmailFooter() != null ? location.getEmailFooter() : "";
            }

            model.put(msgProperty, newLogin);
            model.put(successProperty, true);
            model.put(payedProperty, true);
            String email = payment.getPayerEmail(),
                    link, subject, message;
            if (email != null && newLogin != null) {
                link = contextPath + "/pictures/auth/autoLogin?username=" + newLogin;   // todo move to message source
                subject = messageSource.getMessage("mail.subject", new Object[]{locationName}, Locale.ROOT);
                message = messageSource.getMessage("mail.template", new Object[]{"", link, newLogin, locationFooter}, Locale.ROOT);

                mailService.sendMail(subject, message, email);
            }

            analyticsService.trackPayment(CookieManager.getClientId(request.getCookies()), "purchase", "success", "completed purchase", payment.getPictureNumber(), payment.getPrice(), null);
        }

        return "catalogNotLogin";
    }

}
