package com.scnsoft.fotaflo.webportal.service;

import com.scnsoft.fotaflo.webportal.model.Payment;

public interface IPaymentService {
    String requestPayment(Payment payment, String confirmRedirectUrl, String cancelRedirectUrl);

    boolean completePayment(Payment payment);
}
