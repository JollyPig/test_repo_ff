package com.scnsoft.fotaflo.webportal.model;

public enum Position {
    LEFT_TOP        ("tl", "left top"       ),
    CENTER_TOP      ("tc", "center top"     ),
    RIGHT_TOP       ("tr", "right top"      ),
    LEFT_MIDDLE     ("ml", "left middle"    ),
    CENTER          ("mc", "center"         ),
    RIGHT_MIDDLE    ("mr", "right middle"   ),
    LEFT_BOTTOM     ("bl", "left bottom"    ),
    CENTER_BOTTOM   ("bc", "center bottom"  ),
    RIGHT_BOTTOM    ("br", "right bottom"   );

    private final String id;

    private final String alias;

    Position(String id, String alias) {
        this.id = id;
        this.alias = alias;
    }

    public String getId() {
        return this.id;
    }

    public String getAlias() {
        return alias;
    }

    public static Position getById(String id) {
        for (Position c : Position.values()) {
            if (c.getId().equals(id)) {
                return c;
            }
        }
        return null;
    }

    public static boolean contains(String test) {
        for (Position c : Position.values()) {
            if (c.name().equals(test)) {
                return true;
            }
        }
        return false;
    }
}
