package com.scnsoft.fotaflo.webportal.controller;

import com.scnsoft.fotaflo.webportal.bean.PictureBean;
import com.scnsoft.fotaflo.webportal.service.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */
@Controller
public class CatalogController {
    protected static Logger logger = Logger.getLogger(CatalogController.class);

    @Autowired
    private IPictureService pictureService;

    /*@RequestMapping(value = "getPictures.json")
    public
    @ResponseBody ResponseEntity<List<PictureBean>> loadPictures(HttpServletRequest request) {
        String[] ids = request.getQueryString().split(",");
        List<Integer> list = new ArrayList<Integer>();
        for(String id: ids){
            if(!id.trim().isEmpty() && id.matches("[\\d]+")){
                list.add(new Integer(id));
            }
        }
        logger.info("Selected pictures ids: " + list);
        List<PictureBean> result = pictureService.getPicturesByIds(list);
        return new ResponseEntity<List<PictureBean>>(result, HttpStatus.OK);
    }*/

    @RequestMapping(value = "/main/selected")
    public @ResponseBody ResponseEntity<List<PictureBean>> getPicturesById(HttpServletRequest request){
        String[] ids = request.getQueryString().split(",");
        List<Integer> list = new ArrayList<Integer>();
        for(String id: ids){
            if(!id.trim().isEmpty() && id.matches("[\\d]+")){
                list.add(new Integer(id));
            }
        }
        logger.info("Pictures ids: " + list);
        List<PictureBean> result = pictureService.getPicturesByIds(list);
        return new ResponseEntity<List<PictureBean>>(result, HttpStatus.OK);
    }
}
