package com.scnsoft.fotaflo.webportal.dto;

import java.io.Serializable;

public class MerchantAccountDto implements Serializable {

    private String account;
    private String password;
    private String signature;
    private String environment;

    public MerchantAccountDto() {
    }

    public MerchantAccountDto(String account, String password, String signature, String environment) {
        this.account = account;
        this.password = password;
        this.signature = signature;
        this.environment = environment;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

}
