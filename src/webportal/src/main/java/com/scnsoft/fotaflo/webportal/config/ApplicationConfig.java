package com.scnsoft.fotaflo.webportal.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/** Root element for application configuration */
@XmlRootElement(name = "fotaflo-webapp-config")
@XmlAccessorType(XmlAccessType.FIELD)
public class ApplicationConfig {

    @XmlElement(name = "remote-server-list")
    private RemoteServers remoteServerList;

    public RemoteServers getRemoteServerList() {
        return remoteServerList;
    }

    public void setRemoteServerList(RemoteServers remoteServerList) {
        this.remoteServerList = remoteServerList;
    }

    public void addRemoteServer(RemoteServer remoteServer){
        if(remoteServer != null){
            if(remoteServerList == null){
                remoteServerList = new RemoteServers();
            }
            remoteServerList.addRemoteServer(remoteServer);
        }
    }
}
