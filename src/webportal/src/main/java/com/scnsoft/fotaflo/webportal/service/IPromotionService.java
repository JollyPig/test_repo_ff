package com.scnsoft.fotaflo.webportal.service;

import com.scnsoft.fotaflo.webportal.dto.PromotionDto;
import com.scnsoft.fotaflo.webportal.model.Position;

import java.util.List;
import java.util.Map;

public interface IPromotionService {

    PromotionDto getPromotion(Integer id);

    List<PromotionDto> getPromotions();

    Map<Integer, PromotionDto> getPromotionMap();

    Integer createPromotion(PromotionDto promotion);

    void editPromotion(PromotionDto promotion);

    void deletePromotion(PromotionDto promotion);

    Position[] getPromotionPositions();

    void savePromotions(List<PromotionDto> promotions);

}
