package com.scnsoft.fotaflo.webportal.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/** Credentials for connecting to remote server */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Principal {

    @XmlElement(name = "username")
    private String username;

    @XmlElement(name = "password")
    private String password;

    public Principal() {
    }

    public Principal(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
