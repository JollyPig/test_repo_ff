package com.scnsoft.fotaflo.webportal.service;

import com.scnsoft.fotaflo.webportal.model.SystemUser;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 07.04.14
 * Time: 14:31
 * To change this template use File | Settings | File Templates.
 */
public interface IUserService {

    SystemUser getUserByLogin(String userlogin);

    void updateUserLastAccessDate(SystemUser user);

    List<String> generateNewUserLogins(String userloginGroup, int n, String pictureIds);

}
