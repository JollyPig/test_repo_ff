package com.scnsoft.fotaflo.webportal.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/** Shell for list of remote servers */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class RemoteServers {

    @XmlElement(name = "remote-server")
    private List<RemoteServer> remoteServers;

    public List<RemoteServer> getRemoteServers() {
        return remoteServers;
    }

    public void setRemoteServers(List<RemoteServer> remoteServers) {
        this.remoteServers = remoteServers;
    }

    public void addRemoteServer(RemoteServer remoteServer){
        if(remoteServer != null){
            if(remoteServers == null){
                remoteServers = new ArrayList<RemoteServer>();
            }
            remoteServers.add(remoteServer);
        }
    }
}
