package com.scnsoft.fotaflo.webportal.config;

import com.scnsoft.fotaflo.common.util.StringUtils;
import com.scnsoft.fotaflo.common.util.XmlMapper;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

/** Utility that loads application configuration from or saves to .xml file */
public class RemoteServerConfigHolder {
    protected final static Logger logger = Logger.getLogger(RemoteServerConfigHolder.class);

    /* <id> - <remote server> map */
    private Map<Integer, RemoteServer> remoteServers = new HashMap<Integer, RemoteServer>();

    /* File with configuration */
    private final File configFile;

    public RemoteServerConfigHolder(String configPath) {
        if(StringUtils.isEmpty(configPath)){
            throw new IllegalArgumentException("'configPath' cannot be empty");
        }
        this.configFile = new File(configPath);
        if(!this.configFile.exists()){
            throw new IllegalArgumentException("Could not load remote servers configuration. " +
                    "File doesn't exist by path " + this.configFile.getAbsolutePath());
        }

        loadConfig();
    }

    /**
     * Saves remote servers config to file (this method is not used in application)
     * @param path location path for file in which config will be saved
     * */
    public static void saveConfig(String path){
        ApplicationConfig config = new ApplicationConfig();
        // Add remote server configuration
        config.addRemoteServer(new RemoteServer(1, new Principal("tayna", "tayna"), "URL to REST-Service","URL to picture store"));
        /*  // Additional server config
        config.addRemoteServer(new RemoteServer(2, "http://localhost:8081/fotaflo/pictures/","http://localhost:8081/fotaflo/pictures/"));
        * */

        try{
            OutputStream out = new FileOutputStream(path);

            saveConfig(config, out);
        }catch (FileNotFoundException e){
            logger.error(e.getMessage(), e);
        }
    }

    /**
     * Saves remote servers config to output stream (this method is not used in application)
     * @param config application configuration with remote servers
     * @param out output stream
     * */
    public static void saveConfig(ApplicationConfig config, OutputStream out){
        if(config == null){
            throw new IllegalArgumentException("'config' cannot be null");
        }
        XmlMapper.writeObject(config, out);
    }


    /**
     * Loads config data from configFile to this class
     * */
    protected void loadConfig(){
        ApplicationConfig config = XmlMapper.readObject(configFile, ApplicationConfig.class);

        if(config == null || config.getRemoteServerList() == null
                || config.getRemoteServerList().getRemoteServers() == null || config.getRemoteServerList().getRemoteServers().isEmpty()){
            throw new ApplicationConfigLoadException("Application remote server configuration is empty.");
        }

        for(RemoteServer server: config.getRemoteServerList().getRemoteServers()){
            if(server != null){
                remoteServers.put(server.getId(), server);
            }else{
                throw new ApplicationConfigLoadException("Wrong configuration.");
            }
        }

        logger.info("Loaded configuration for " + remoteServers.size() + " remote server(s).");
    }

    /**
     * Returns remote servers mapped by id
     * @return <id> - <remote server> map
     * */
    public Map<Integer, RemoteServer> getRemoteServers() {
        return remoteServers;
    }
}
