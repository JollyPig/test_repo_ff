package com.scnsoft.fotaflo.webportal.service.impl;

import com.scnsoft.fotaflo.common.paypal.*;
import com.scnsoft.fotaflo.common.paypal.PaymentException;
import com.scnsoft.fotaflo.common.util.StringUtils;
import com.scnsoft.fotaflo.webportal.model.Payment;
import com.scnsoft.fotaflo.webportal.service.IPaymentService;
import com.scnsoft.fotaflo.webportal.service.exception.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentService implements IPaymentService {
    protected static final Logger logger = Logger.getLogger(PaymentService.class);

    @Autowired
    private PayPal paypal;

    @Override
    public String requestPayment(Payment payment, String confirmRedirectUrl, String cancelRedirectUrl){
        PaymentResult result = null;
        if(payment == null){
            throw new IllegalArgumentException("'payment' cannot be null");
        }
        Double amount = payment.getPrice();
        String currency = payment.getCurrency();

        if(amount == null || amount <= 0 || StringUtils.isEmpty(currency)){
            throw new IllegalArgumentException("Incorrect values for 'amount' and 'currency'");
        }
        if(StringUtils.isEmpty(confirmRedirectUrl) || StringUtils.isEmpty(cancelRedirectUrl)){
            throw new IllegalArgumentException("'confirmRedirectUrl' and 'cancelRedirectUrl' cannot be empty");
        }

        String error = null;
        String redirectUrl = null;

        try{
            result = paypal.requestPayment(getPaymentAmount(amount, currency), confirmRedirectUrl, cancelRedirectUrl);
        }catch (PayPalException e){
            logger.error(e.getMessage(), e);
            error = e.getMessage();
        }catch (PaymentException e){
            error = e.getMessage();
        }

        if (result != null && result.isSuccess()) {
            logger.debug("EC Token:" + result.getToken());
            payment.setToken(result.getToken());
            payment.setInvoiceId(result.getInvoiceId());
            redirectUrl = result.getRedirectUrl();
        }else {
            new ServiceException(error);
        }

        return redirectUrl;
    }

    @Override
    public boolean completePayment(Payment payment){
        PaymentResult result = null;
        if(payment == null){
            throw new IllegalArgumentException("'payment' cannot be null");
        }
        String token = payment.getToken();

        if(token == null || StringUtils.isEmpty(token)){
            throw new IllegalArgumentException("Incorrect value for 'token'");
        }

        boolean success = false;
        String error = null;

        try{
            result = paypal.completePayment(token);
        }catch (PayPalException e){
            logger.error(e.getMessage(), e);
            error = e.getMessage();
        }catch (PaymentException e){
            error = e.getMessage();
        }

        if (result != null && result.isSuccess()) {
            payment.setPayerID(result.getPayerId());
            payment.setTransactionID(result.getTransactionId());
            success = true;
        }else {
            new ServiceException(error);
        }

        return success;
    }

    protected PaymentAmount getPaymentAmount(Double amount, String currency){
        PaymentAmount.Currency currencyType = currency.equals("CAD") ? PaymentAmount.Currency.CAD : PaymentAmount.Currency.USD;

        return new PaymentAmount(amount, currencyType);
    }

}
