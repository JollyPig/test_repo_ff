package com.scnsoft.fotaflo.webportal.ws;

import java.util.Date;

public interface IAnalyticWebService {

    String RESOURCE = "/web/purchases";

    String getPurchaseEmailCount(Integer locationId, Date startDate, Date endDate);

    String getPurchaseEmails(Integer locationId, Date startDate, Date endDate);

    String sendReportData(Integer locationId, Date date, Integer total, boolean groupPackage);

}
