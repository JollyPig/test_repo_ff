package com.scnsoft.fotaflo.webportal.report;

import com.scnsoft.fotaflo.webportal.report.model.ColumnHeader;
import com.scnsoft.fotaflo.webportal.report.model.ReportData;
import jxl.Workbook;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import jxl.write.Number;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.*;

@Component
public class ExcelReportBuilder extends ReportBuilder {

    public static final String EXTENTION = ".xls";

    private static final String allLetters = getAllLetters();

    private static String getAllLetters(){
        StringBuffer s = new StringBuffer();
        for(char c='A';c<='Z';c++){
            s.append(c);
        }
        return s.toString();
    }

    public File createReport(ReportData data) {
        File out = getOutputFile(getOutputFilename(data));

        WritableWorkbook workbook = null;

        if(data.getData().size() == 0){
            throw new RuntimeException("Report error: no data");
        }
        try{
            workbook = Workbook.createWorkbook(out);

            final CellFormat format = new CellFormat(WritableFont.ARIAL, 10);

            WritableSheet sheet;
            int sheetNum = 0,
                    row, col;
            Label label;
            Number number;
            Formula formula;

            List<Integer> totalColumnIndexes;
            List<Integer> totalRowIndexes;
            Map<Integer, Integer> mergedCells;
            boolean isValueCell;
            int mc;
            for(Object key: data.getData().keySet()){
                row = 0;
                sheet = workbook.createSheet(key.toString(), sheetNum++);

                totalColumnIndexes = new ArrayList<Integer>();
                totalRowIndexes = new ArrayList<Integer>();
                mergedCells = new HashMap<Integer, Integer>();

                // write header
                label = new Label(0, row, "Period:", format.getBold());
                sheet.addCell(label);
                label = new Label(1, row, data.getRange(), format.getBold());
                sheet.mergeCells(1, row, 3, row);
                sheet.addCell(label);

                row++;

                label = new Label(0, row, "Location:", format.getBold());
                sheet.addCell(label);
                label = new Label(1, row, key.toString(), format.getBold());
                sheet.mergeCells(1, row, 3, row);
                sheet.addCell(label);

                row++;
                row++;

                // write column names
                col=0;
                for (ColumnHeader h: data.getColumns()){
                    sheet.setColumnView(col, h.getWidth());
                    label = new Label(col, row, h.getName(), format.getNormalDarkGray());
                    sheet.addCell(label);
                    isValueCell = ColumnHeader.ColumnType.VALUE.equals(h.getType());
                    if(isValueCell){
                        totalColumnIndexes.add(col);
                    }
                    col++;
                }

                // write data
                Map<Date, Map<String, Integer>> sndLvlMap = data.getData().get(key);
                Object value;
                WritableCellFormat cellFormat;
                for(Date date: sndLvlMap.keySet()){
                    row++;

                    col=0;
                    for (ColumnHeader h: data.getColumns()){
                        value = sndLvlMap.get(date).get(h.getId());
                        isValueCell = ColumnHeader.ColumnType.VALUE.equals(h.getType());
                        cellFormat = isValueCell ? format.getNormal() : format.getNormalGray();
                        if(value != null){
                            sheet.addCell(getCellByType(col, row, value, h.getDataType(), cellFormat));

                            if(!isValueCell){
                                if(!mergedCells.containsKey(col)){
                                    mergedCells.put(col, 0);
                                }else{
                                    mc = mergedCells.get(col);
                                    if(value.toString().equals(sheet.getCell(col, row - 1).getContents())){
                                        mergedCells.put(col, mc + 1);
                                    }else{
                                        if(mc > 0){
                                            sheet.mergeCells(col, row-mc, col, row);
                                            mergedCells.put(col, 0);
                                        }
                                    }
                                }
                            }
                        }else if(isValueCell){
                            sheet.addCell(getCellByType(col, row, null, h.getDataType(), cellFormat));
                        }
                        col++;
                    }
                    totalRowIndexes.add(row);
                }

                for(Integer c: mergedCells.keySet()){
                    mc = mergedCells.get(c);
                    if(mc > 0){
                        sheet.mergeCells(c, row-mc, c, row);
                    }
                }

                row++;
                label = new Label(0, row, "Total:", format.getNormalGray());
                sheet.mergeCells(0, row, totalColumnIndexes.get(0)-1, row);
                sheet.addCell(label);
                for(Integer i: totalColumnIndexes){
                    formula = new Formula(i, row,
                            getSumFormula(i, i, totalRowIndexes.get(0),
                                    totalRowIndexes.get(totalRowIndexes.size()-1)), format.getBold());
                    sheet.addCell(formula);
                }

                row++;

                // write footer
                label = new Label(0, row, "Total All:", format.getBold());
                sheet.mergeCells(0, row, data.getColumns().size()-2, row);
                sheet.addCell(label);

                formula = new Formula(data.getColumns().size()-1, row,
                        getSumFormula(totalColumnIndexes.get(0), totalColumnIndexes.get(totalColumnIndexes.size()-1), totalRowIndexes.get(0),
                                totalRowIndexes.get(totalRowIndexes.size()-1)), format.getBold());
                sheet.addCell(formula);
            }

            workbook.write();
            workbook.close();

        }catch (IOException e){
            logger.error(e.getMessage(), e);
            if(workbook != null){
                try {
                    workbook.close();
                }catch (Exception ex){
                    logger.warn(ex.getMessage(), ex);
                }
            }
            return null;
        }catch (WriteException e){
            logger.error(e.getMessage(), e);
            if(workbook != null){
                try {
                    workbook.close();
                }catch (Exception ex){
                    logger.warn(ex.getMessage(), ex);
                }
            }
            return null;
        }
        return out;
    }

    private WritableCell getCellByType(int col, int row, Object data, ColumnHeader.ColumnDataType dataType, WritableCellFormat format){
        switch (dataType){
            case NUMBER: return new jxl.write.Number(col, row, (data != null ? (Integer)data : 0), format);
            case LABEL:
            default: return new Label(col, row, (data != null ? data.toString() : ""), format);
        }
    }

    private String getSumFormula(int c1, int c2, int r1, int r2){
        String formula = "SUM(";
        formula += getColumnIndex(c1) + (r1+1);
        formula += ":";
        formula += getColumnIndex(c2) + (r2+1);
        formula += ")";

        return formula;
    }

    private String getColumnIndex(int index){
        String s = "";
        int r;
        if(index > allLetters.length()-1){
            while (index > allLetters.length()-1){
                r = index % allLetters.length();
                s = allLetters.charAt(r) + s;
                index /= allLetters.length();
            }
            s = allLetters.charAt(index-1) + s;
        }else{
            s += allLetters.charAt(index);
        }

        return s;
    }

    @Override
    protected String getOutputFilename(ReportData data){
        return super.getOutputFilename(data) + EXTENTION;
    }

    protected class CellFormat{

        private WritableFont.FontName font;
        private int size;

        private WritableCellFormat normal;
        private WritableCellFormat bold;

        private WritableCellFormat normalGray;

        private WritableCellFormat normalDarkGray;

        protected CellFormat(WritableFont.FontName font, int size) {
            this.font = font;
            this.size = size;

            initFormat();
        }

        private void initFormat(){
            WritableFont normalFont;
            WritableFont boldFont;

            normalFont = new WritableFont(font, size);
            boldFont = new WritableFont(font, size);
            try{
                boldFont.setBoldStyle(WritableFont.BOLD);
            }catch(WriteException e){
                logger.warn(e.getMessage(), e);
            }

            normal = getCellFormat(normalFont);

            bold = getCellFormat(boldFont);

            normalGray = getCellFormat(normalFont, jxl.format.Colour.GRAY_25);

            normalDarkGray = getCellFormat(normalFont, jxl.format.Colour.GRAY_50);
        }


        protected WritableCellFormat getCellFormat(WritableFont font){
            return getCellFormat(font, null);
        }

        protected WritableCellFormat getCellFormat(WritableFont font, jxl.format.Colour color){
            if(font == null){
                throw new IllegalArgumentException("'font' cannot be null");
            }
            WritableCellFormat format = new WritableCellFormat(font);
            try{
                if(color != null){
                    format.setBackground(color);
                    format.setBorder(Border.ALL, BorderLineStyle.THIN);
                }
                format.setVerticalAlignment(VerticalAlignment.TOP);
            }catch(WriteException e){
                logger.warn(e.getMessage(), e);
            }
            return format;
        }

        public WritableCellFormat getNormal() {
            return normal;
        }

        public WritableCellFormat getBold() {
            return bold;
        }

        public WritableCellFormat getNormalGray() {
            return normalGray;
        }

        public WritableCellFormat getNormalDarkGray() {
            return normalDarkGray;
        }

        public WritableCellFormat getNormal(jxl.format.Colour colour) {
            WritableFont normalFont = new WritableFont(font, size);
            WritableCellFormat format = new WritableCellFormat(normalFont);
            try{
                format.setBackground(colour);
            }catch(WriteException e){
                logger.warn(e.getMessage(), e);
            }
            return format;
        }

        public WritableCellFormat getBold(jxl.format.Colour colour) {
            WritableFont font = new WritableFont(this.font, size);
            try{
                font.setBoldStyle(WritableFont.BOLD);
            }catch(WriteException e){
                logger.warn(e.getMessage(), e);
            }
            WritableCellFormat format = new WritableCellFormat(font);
            try{
                format.setBackground(colour);
            }catch(WriteException e){
                logger.warn(e.getMessage(), e);
            }
            return format;
        }
    }

}
