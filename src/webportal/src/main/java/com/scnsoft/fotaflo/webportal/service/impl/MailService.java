package com.scnsoft.fotaflo.webportal.service.impl;

import com.scnsoft.fotaflo.webportal.service.IMailService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class MailService implements IMailService {

    private static final Logger logger = Logger.getLogger(MailService.class);

    @Autowired
    private JavaMailSender mailSender;

    @Value("${mail.from}")
    private String from;

    @Value("${mail.target}")
    private String target;

    @Override
    public void sendMailFrom(String from, String subject, String message) {
        this.sendMail(from, subject, message, this.target);
    }

    @Override
    public void sendMail(String subject, String message) {
        this.sendMail(this.from, subject, message, this.target);
    }

    @Override
    public void sendMail(String subject, String message, String to) {
        this.sendMail(this.from, subject, message, to);
    }

    public void sendMail(String from, String subject, String message, String to) {
        if(to == null || to.isEmpty()){
            //регExp
            to = target;
        }
        if(from == null || from.trim().isEmpty()){
            throw new IllegalArgumentException("Field 'from' cannot be empty");
        }
        if(subject == null || subject.trim().isEmpty()){
            throw new IllegalArgumentException("Field 'subject' cannot be empty");
        }
        if(message == null || message.trim().isEmpty()){
            throw new IllegalArgumentException("Field 'message' cannot be empty");
        }
        try {
            final SimpleMailMessage template = new SimpleMailMessage();
            template.setFrom(from);
            template.setTo(to);
            template.setSubject(subject);
            template.setText(message);

            mailSender.send(new MimeMessagePreparator() {
                public void prepare(MimeMessage mimeMessage) throws MessagingException {
                    MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
                    message.setFrom(template.getFrom());
                    message.setTo(template.getTo());
                    message.setSubject(template.getSubject());
                    message.setText(template.getText(), true);
                }
            });
        } catch (MailException e) {
            logger.error("Can't send email", e);
            throw new RuntimeException(e);
        }
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }
}
