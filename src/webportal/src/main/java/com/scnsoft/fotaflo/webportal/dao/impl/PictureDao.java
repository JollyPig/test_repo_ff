package com.scnsoft.fotaflo.webportal.dao.impl;

import com.scnsoft.fotaflo.common.dao.AbstractDao;
import com.scnsoft.fotaflo.webportal.dao.IPicturesDao;
import com.scnsoft.fotaflo.webportal.model.Picture;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 08.04.14
 * Time: 17:42
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class PictureDao extends AbstractDao<Picture, Integer> implements IPicturesDao {

    @Override
    protected Class<Picture> getDomainClass() {
        return Picture.class;
    }

    public Picture getPictureByExternalId(int id, int serverId){
        StringBuffer query = new StringBuffer();
        query.append("select picture from Picture as picture where ");
        query.append("picture.pictureId  = :id");
        query.append(" and picture.serverId  = :serverId");

        List<Picture> result = getSession().createQuery(query.toString())
                .setParameter("id", id)
                .setParameter("serverId", serverId)
                .list();
        if (!result.isEmpty()) {
            return result.get(0);
        }
        return null;
    }

}
