package com.scnsoft.fotaflo.webportal.ws;

import com.scnsoft.fotaflo.webportal.bean.PictureBean;

import java.util.Date;
import java.util.List;

public interface IPictureWebService {

    String RESOURCE = "/web/pictures";

    List<PictureBean> getPictures(String login, Date from);

    List<PictureBean> getPictures(String login, Date from, IBeanWrapper preparator);

}
