package com.scnsoft.fotaflo.webportal.dao;

import com.scnsoft.fotaflo.webportal.model.TextPromotion;
import com.scnsoft.fotaflo.common.dao.IDao;

public interface ITextPromotionDao extends IDao<TextPromotion, Integer> {

    TextPromotion getByNumber(Integer number);

}
