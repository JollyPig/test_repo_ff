package com.scnsoft.fotaflo.webportal.analytics.ga.tracking;

public class SocialEventPayload extends ParametrizedEventPayload {

    protected static final String socialAccountParam    = "cd6";
    protected static final String friendsCountParam     = "cm1";

    public SocialEventPayload(String trackingId, String clientId) {
        super(trackingId, clientId);
    }

    public SocialEventPayload(String protocolVersion, String trackingId, String clientId) {
        super(protocolVersion, trackingId, clientId);
    }

    public String getSocialAccount() {
        return getParameter(socialAccountParam);
    }

    public void setSocialAccount(String socialAccount) {
        addParameter(socialAccountParam, socialAccount);
    }

    public Integer getFriendsCount() {
        return (Integer)getRawParameter(friendsCountParam);
    }

    public void setFriendsCount(Integer friendsCount) {
        addParameter(friendsCountParam, friendsCount);
    }
}
