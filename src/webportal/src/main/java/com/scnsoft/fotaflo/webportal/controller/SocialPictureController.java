package com.scnsoft.fotaflo.webportal.controller;

import com.scnsoft.fotaflo.common.util.IOUtil;
import com.scnsoft.fotaflo.webportal.analytics.ga.tracking.GaTracker;
import com.scnsoft.fotaflo.webportal.service.IPictureService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.UUID;

/**
 * @author Anatol Sialitski
 */
@Controller
public class SocialPictureController {

    private final static Logger logger = Logger.getLogger (SocialPictureController.class);

    @Autowired
    private IPictureService pictureService;

    @Autowired
    private GaTracker gaTracker;

    @RequestMapping (value = "/{pictureId}/social", method = RequestMethod.GET)
    public void getPicture (final @PathVariable (value = "pictureId") Integer pictureId, final HttpServletRequest httpRequest, final HttpServletResponse httpResponse) throws Exception {
        final File file = pictureService.getPictureById (pictureId);

        if (file == null) {
            throw new FileNotFoundException ();
        }

        httpResponse.setHeader ("Content-Type", "image/jpg");
        httpResponse.setHeader ("Content-Disposition", "inline;filename=\"" + file.getName () + "\"");

        IOUtil.writeFromInputToOutput (new BufferedInputStream (new FileInputStream (file)), httpResponse.getOutputStream ());

        trackedPinterest (httpRequest, pictureId);
    }

    protected void trackedPinterest (final HttpServletRequest httpRequest, final Integer pictureId) {
        final String userAgentHeader = httpRequest.getHeader ("user-agent");

        logger.info ("userAgentHeader  ---- " + userAgentHeader);

        if (userAgentHeader != null && userAgentHeader.contains ("http://pinterest.com/")) {
            gaTracker.trackSocial(UUID.randomUUID().toString(), "share", "success", "pinterest");
        }
    }

}
