package com.scnsoft.fotaflo.webportal.model;

import javax.persistence.*;
import com.scnsoft.fotaflo.common.model.AbstractEntity;

@Entity
@Table(name = "promotion")
public class Promotion extends AbstractEntity {

    private Integer number;

    private String text;

    private String position;

    private String color;

    private String shadowColor;

    private String path;

    public Promotion() {
    }

    public Promotion(Integer number, String text, String position, String color, String shadowColor, String path) {
        this.number = number;
        this.text = text;
        this.position = position;
        this.color = color;
        this.shadowColor = shadowColor;
        this.path = path;
    }

    @Column(name = "number", nullable = false, unique = true)
    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    @Column(name = "text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Column(name = "text_position")
    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Column(name = "color")
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Column(name = "shadowColor")
    public String getShadowColor() {
        return shadowColor;
    }

    public void setShadowColor(String shadowColor) {
        this.shadowColor = shadowColor;
    }

    @Column(name = "path")
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
