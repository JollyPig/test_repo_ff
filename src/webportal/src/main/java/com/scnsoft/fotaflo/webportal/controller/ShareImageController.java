package com.scnsoft.fotaflo.webportal.controller;

import com.scnsoft.fotaflo.common.analytics.ga.util.CookieManager;
import com.scnsoft.fotaflo.common.bean.AjaxResponse;
import com.scnsoft.fotaflo.webportal.bean.ShareStatusBean;
import com.scnsoft.fotaflo.webportal.model.SocialNetwork;
import com.scnsoft.fotaflo.webportal.model.SocialNetworkType;
import com.scnsoft.fotaflo.webportal.service.IAnalyticsService;
import com.scnsoft.fotaflo.webportal.service.IShareService;
import com.scnsoft.fotaflo.webportal.service.ISocialNetworkService;
import com.scnsoft.fotaflo.webportal.social.Connection;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/main")
public class ShareImageController {
    protected static final Logger logger = Logger.getLogger(ShareImageController.class);

    @Autowired
    private IShareService shareService;

    @Autowired
    private ISocialNetworkService socialNetworkService;

    @Autowired
    private IAnalyticsService analyticsService;

    @Value("${facebook.clientId}")
    private String facebookClientId;

    @RequestMapping(value = "/connections", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<?> availableConnections(){
        List<Connection> list = new ArrayList<Connection>();

        try{
            list = shareService.availableConnections();
        }catch (Exception e){
            logger.error(e.getMessage());
        }

        return new ResponseEntity<List>(list, HttpStatus.OK);
    }

    @RequestMapping(value = "/share", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<?> shareImage(@RequestParam("message") String message,
                                 @RequestParam(value = "services") List<SocialNetworkType> serviceTypes,
                                 @RequestParam(value = "images") List<Integer> images,
                                 @RequestParam(value = "tags", required = false) List<String> tags,
                                 HttpServletRequest request ){
        Map<String, Object> result = new HashMap<String, Object>();

        Map<String, String> tagMap = new HashMap<String, String>();
        if(tags != null){
            String tag;
            int i;
            for(String t: tags){
                i = t.indexOf(':');
                tag = t.substring(0, (i < 0) ? 0 : i).trim();
                if(!tag.isEmpty() && (++i < t.length())){
                    tagMap.put(tag, t.substring(i));
                }
            }
        }

        List<ShareStatusBean> statuses;

        statuses = shareService.shareImage(serviceTypes, images, message, tagMap);

        boolean success = true;
        String errorMessage = "";
        for(ShareStatusBean status: statuses){
            if(status.isSuccess()){
                analyticsService.trackSocial(CookieManager.getClientId(request.getCookies()), "share", "success", status.getType(), images.size(), status.getAccount(), status.getFriends());
            }else{
                success = false;
                errorMessage += status.getType() + ": " + status.getError().getMessage() + ", ";
                analyticsService.trackSocial(CookieManager.getClientId(request.getCookies()), "share", "error", status.getType(), 0, null, 0, status.getError().getMessage());
            }
        }
        result.put("success", success);
        result.put("message", errorMessage);

        return new ResponseEntity<Map>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/share/settings", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<?> shareImage(ModelMap model, HttpServletRequest httpServletRequest ){
        Map<String, SocialNetwork> settings = socialNetworkService.getSocialNetworkSettings();

        if(logger.isDebugEnabled()){
            logger.debug("Settings: "+settings);
        }

        return new ResponseEntity<Map<String, SocialNetwork>>(settings, HttpStatus.OK);
    }

    @RequestMapping(value = "/share/facebook", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<?> getFacebook(ModelMap model, HttpServletRequest httpServletRequest ){
        Map<String, Object> response = new HashMap<String, Object>();

        response.put("appId", facebookClientId);

        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/share/status/{provider}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<?> checkStatus(@PathVariable String provider){
        AjaxResponse response;

        Connection connection = shareService.checkConnectionStatus(provider);
        if(connection != null){
            response = new AjaxResponse();
            response.setSuccess(true);
            response.setData(connection);
        }else{
            response = new AjaxResponse(false, "No connection was found");
        }

        return new ResponseEntity<AjaxResponse>(response, HttpStatus.OK);
    }

}
