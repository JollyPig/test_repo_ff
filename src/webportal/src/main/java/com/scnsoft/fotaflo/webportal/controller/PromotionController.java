package com.scnsoft.fotaflo.webportal.controller;

import com.scnsoft.fotaflo.webportal.dto.PromotionDto;
import com.scnsoft.fotaflo.webportal.dto.PromotionsDto;
import com.scnsoft.fotaflo.webportal.dto.TextPromotionDto;
import com.scnsoft.fotaflo.webportal.model.Position;
import com.scnsoft.fotaflo.webportal.model.SystemUser;
import com.scnsoft.fotaflo.webportal.service.IPromotionService;
import com.scnsoft.fotaflo.webportal.service.ITextPromotionService;
import com.scnsoft.fotaflo.webportal.service.IUserService;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/admin/promotions")
public class PromotionController {
    protected final static Logger logger = Logger.getLogger(PromotionController.class);

    @Autowired
    private IPromotionService promotionService;

    @Autowired
    private IUserService userService;

    @Autowired
    private ITextPromotionService textPromotionService;

    @Value("${fotaflo.promotions_path}")
    private String imagePath;

    @RequestMapping(method = RequestMethod.GET)
    public String list(ModelMap model){
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        SystemUser systemUser = userService.getUserByLogin(currentUser);
        boolean isAdmin = (systemUser!=null&& systemUser.getAccess()!=null && systemUser.getAccess().equals("1"))?true:false;

        Map<Integer, PromotionDto> map = promotionService.getPromotionMap();
        Map<Integer, TextPromotionDto> tmap = textPromotionService.getPromotionMap();
        Position[] pp = promotionService.getPromotionPositions();

        model.addAttribute("promotions", map);
        model.addAttribute("textPromotions", tmap);
        model.addAttribute("positions", pp);
        model.addAttribute("isadmin", isAdmin);

        return "promotions";
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> save(@RequestBody PromotionsDto promotions, HttpServletRequest request){
        logger.debug(promotions);

        promotionService.savePromotions(promotions.getPromotions());
        textPromotionService.savePromotions(promotions.getTextPromotions());

        return new ResponseEntity<Boolean>(true, HttpStatus.OK);
    }

    @RequestMapping(value = "/preview", method = RequestMethod.POST)
    public String getImage(@RequestBody PromotionsDto promotions, ModelMap model){
        Map<Integer, PromotionDto> result = new HashMap<Integer, PromotionDto>();
        for(PromotionDto p: promotions.getPromotions()){
            if(p.getNumber() != null){
                result.put(p.getNumber(), p);
            }
        }
        Map<Integer, TextPromotionDto> textresult = new HashMap<Integer, TextPromotionDto>();
        for(TextPromotionDto p: promotions.getTextPromotions()){
            if(p.getNumber() != null){
                textresult.put(p.getNumber(), p);
            }
        }

        model.addAttribute("promotions", result);
        model.addAttribute("textPromotions", textresult);
        model.addAttribute("preview", true);

        return "index";
    }

    @RequestMapping(value = "/image/{path}.{ext}", method = RequestMethod.GET)
    public void getImage(@PathVariable("path") String path, @PathVariable("ext") String ext, HttpServletRequest req, HttpServletResponse resp) throws IOException{
        String filePath = imagePath + path + '.' + ext;
        logger.debug(filePath);
        try {
            if(filePath == null){
                logger.error("filePath is null");
                resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            }

            File image = new File(filePath);

            FileInputStream in = new FileInputStream(image);
            byte[] imageData = IOUtils.toByteArray(in);

            in.close();

            String contentType = getContentTypeByFormat(ext);
            if(contentType == null){
                contentType = "image/jpg";
            }

            resp.addHeader("Content-Type", contentType);
            resp.getOutputStream().write(imageData);
            resp.getOutputStream().close();

        } catch (FileNotFoundException e) {
            logger.error("FileNotFoundException: " + e.getMessage());
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    @RequestMapping(value = "/image", headers = "content-type=multipart/*", method = RequestMethod.POST)
    public @ResponseBody String uploadImage(@RequestBody MultipartFile file, HttpServletRequest request) throws IOException{
        String url = null;

        String path = request.getRequestURI().substring(request.getContextPath().length());
        logger.info("Path: "+path);

        if (!file.isEmpty()) {
            BufferedImage src = ImageIO.read(new ByteArrayInputStream(file.getBytes()));
            String contentType = file.getContentType();
            String format = getFormatByContentType(contentType);
            if(format == null){
                throw new IllegalArgumentException("unsupported format");
            }

            String filename = file.getOriginalFilename();

            File destination = generateFile(filename);
            boolean uploaded = ImageIO.write(src, format, destination);
            //Save the id you have used to create the file name in the DB. You can retrieve the image in future with the ID.
            if(uploaded){
                url = path + '/' + filename;
            }
        }

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("Content-Type", "text/html; charset=utf-8");

        logger.info("URI: " + url);

        return url;
    }

    private File generateFile(String filename) {
        logger.info("File: " + imagePath + filename);
        File file = new File(imagePath + filename);
        if (!file.isDirectory()) {
            file.mkdirs();
        }

        return file;
    }

    private String getFormatByContentType(String type){
        if(type != null){
            if(type.equalsIgnoreCase("image/jpeg") || type.equalsIgnoreCase("image/pjpeg")){
                return "jpg";
            }
            if(type.equalsIgnoreCase("image/png")){
                return "png";
            }
            if(type.equalsIgnoreCase("image/gif")){
                return "gif";
            }
        }
        return null;
    }

    private String getContentTypeByFormat(String format){
        if(format != null){
            if(format.equalsIgnoreCase("jpg") || format.equalsIgnoreCase("jpg")){
                return "image/jpeg";
            }
            if(format.equalsIgnoreCase("png")){
                return "image/png";
            }
            if(format.equalsIgnoreCase("gif")){
                return "image/gif";
            }
        }
        return null;
    }

}
