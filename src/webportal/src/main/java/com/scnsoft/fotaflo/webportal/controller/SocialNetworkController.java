package com.scnsoft.fotaflo.webportal.controller;

import com.scnsoft.fotaflo.webportal.dto.SocialNetworkDto;
import com.scnsoft.fotaflo.webportal.model.SocialNetworkType;
import com.scnsoft.fotaflo.webportal.model.SystemUser;
import com.scnsoft.fotaflo.webportal.service.ILocationService;
import com.scnsoft.fotaflo.webportal.service.ISocialNetworkService;
import com.scnsoft.fotaflo.webportal.service.IUserService;
import com.scnsoft.fotaflo.webportal.service.exception.ServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin/common")
public class SocialNetworkController {
    protected final static Logger logger = Logger.getLogger(SocialNetworkController.class);

    @Autowired
    private ISocialNetworkService socialNetworkService;

    @Autowired
    ILocationService locationService;

    @Autowired
    private IUserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public String list(ModelMap model){

        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getName();
        SystemUser systemUser = userService.getUserByLogin(currentUser);
        boolean isAdmin = (systemUser!=null&& systemUser.getAccess()!=null && systemUser.getAccess().equals("1"))?true:false;

        //Map<String, SocialNetworkDto> map = socialNetworkService.getSocialNetworkMap();
        List<SocialNetworkType> names = socialNetworkService.getSocialNetworkNames();

        model.addAttribute("locations", locationService.getLocations());
        //model.addAttribute("socialNetwork", map);
        model.addAttribute("socialNetworkNames", names);
        model.addAttribute("isadmin", isAdmin);
        model.addAttribute("userLocation", systemUser.getLocationId());

        return "socialnet";
    }

    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<?> save(@RequestBody(required = false) SocialNetworkDto dto){
        logger.info("Object to save: "+dto);
        Map<String, Object> model = new HashMap<String, Object>();

        try {
            socialNetworkService.updateSocialNetwork(dto);
            model.put("success", true);
            model.put("data", dto);
        }catch(ServiceException e){
            logger.error(e.getMessage());
            model.put("success", false);
            model.put("message", e.getMessage());
            model.put("data", e.getData());
        }

        return new ResponseEntity<Map>(model, HttpStatus.OK);
    }

    @RequestMapping(value = "/record", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<?> get(@RequestParam("socialnet") String socialNetwork, @RequestParam("location") Integer location){

        SocialNetworkDto result = socialNetworkService.findSocialNetworkDto(socialNetwork, location);

        return new ResponseEntity(result, HttpStatus.OK);
    }
}
