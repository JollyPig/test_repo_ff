package com.scnsoft.fotaflo.webportal.analytics.ga;

public interface IGaField {
    String getId();
    String getLabel();
}
