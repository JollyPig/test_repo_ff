package com.scnsoft.fotaflo.webportal.service;

import com.scnsoft.fotaflo.webportal.bean.PictureBean;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;

public interface IPictureLogoService {

    BufferedImage getMarkedPicture(PictureBean picture, boolean thumb) throws IOException, URISyntaxException;

    File getMarkedPictureFile(PictureBean picture, boolean thumb) throws IOException, URISyntaxException;

    void writeMarkedPicture(PictureBean picture, OutputStream out, boolean thumb) throws IOException, URISyntaxException;

    void writeImage(String uri, OutputStream out) throws IOException, URISyntaxException;

    boolean isImageCached(String uri);

}
