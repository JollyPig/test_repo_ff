package com.scnsoft.fotaflo.webportal.service.impl;

import com.scnsoft.fotaflo.webportal.bean.LocationBean;
import com.scnsoft.fotaflo.webportal.model.SystemUser;
import com.scnsoft.fotaflo.webportal.service.ILocationService;
import com.scnsoft.fotaflo.webportal.service.IUserService;
import com.scnsoft.fotaflo.webportal.ws.ILocationWebService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class LocationService implements ILocationService {
    protected static Logger logger = Logger.getLogger(LocationService.class);

    @Autowired
    private ILocationWebService locationWebService;

    @Autowired
    private IUserService userService;

    @Override
    public List<LocationBean> getLocations(){
        List<LocationBean> result = locationWebService.getLocations();

        return result;
    }

    @Override
    public LocationBean getLocation(int id) {
        LocationBean result = locationWebService.getLocation(id);

        return result;
    }

    @Override
    public LocationBean getCurrentUserLocation() {
        final String currentUser = SecurityContextHolder.getContext().getAuthentication().getCredentials().toString();
        SystemUser user = userService.getUserByLogin(currentUser);
        LocationBean location = getLocation(user.getLocationId());

        return location;
    }
}
