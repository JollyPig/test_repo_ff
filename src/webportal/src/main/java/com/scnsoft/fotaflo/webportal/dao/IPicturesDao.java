package com.scnsoft.fotaflo.webportal.dao;

import com.scnsoft.fotaflo.webportal.model.Picture;

import com.scnsoft.fotaflo.common.dao.IDao;

/**
 * Created with IntelliJ IDEA.
 * User: Paradinets
 * Date: 08.04.14
 * Time: 17:54
 * To change this template use File | Settings | File Templates.
 */
public interface IPicturesDao extends IDao<Picture, Integer> {

    Picture getPictureByExternalId(int id, int serverId);

}
