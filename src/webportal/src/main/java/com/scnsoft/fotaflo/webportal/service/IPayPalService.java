package com.scnsoft.fotaflo.webportal.service;

import com.scnsoft.fotaflo.webportal.dto.MerchantAccountDto;
import com.scnsoft.fotaflo.webportal.dto.PaymentDto;
import com.scnsoft.fotaflo.webportal.model.Merchant;
import com.scnsoft.fotaflo.webportal.model.Payment;

import java.util.Date;

/**
 * Created by Tayna on 01.05.14.
 */
public interface IPayPalService {

    String initiatePayment(PaymentDto dto, String confirmRedirectUrl, String cancelRedirectUrl);

    String completePayment(String token);

    public Payment getRegisteredPayment(String token);

    public Merchant saveMerchantAccount (final MerchantAccountDto model) throws Exception;

    public Merchant getMerchantAccount() throws Exception ;

    public Merchant getMerchantAccountEncrypted() throws Exception;


}
