package com.scnsoft.fotaflo.webportal.service;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;

public interface IPictureLoadService {
    BufferedImage loadPicture(String path) throws IOException, URISyntaxException;

    void loadPicture(String path, OutputStream out) throws IOException, URISyntaxException;
}
