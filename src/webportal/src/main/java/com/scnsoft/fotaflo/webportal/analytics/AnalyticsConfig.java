package com.scnsoft.fotaflo.webportal.analytics;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.analytics.Analytics;
import com.google.api.services.analytics.AnalyticsScopes;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;

@Configuration
public class AnalyticsConfig {
    protected static final Logger logger = Logger.getLogger(AnalyticsConfig.class);

    protected static JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();

    protected static HttpTransport httpTransport;

    static{
        try{
            httpTransport = GoogleNetHttpTransport.newTrustedTransport();
        }catch(IOException e){
            logger.fatal(e.getMessage(),e);
            throw new RuntimeException(e);
        }catch(GeneralSecurityException e){
            logger.fatal(e.getMessage(),e);
            throw new RuntimeException(e);
        }
    }

    @Value("${serviceAccountId}")
    private String serviceAccountId;

    @Value("${applicationName}")
    private String applicationName;

    @Bean
    @Scope(value="singleton", proxyMode= ScopedProxyMode.INTERFACES)
    public HttpRequestInitializer httpRequestInitializer() throws IOException,GeneralSecurityException{
        // Build service account credential.
        GoogleCredential credential = new GoogleCredential.Builder().setTransport(httpTransport)
                .setJsonFactory(jsonFactory)
                .setServiceAccountId(serviceAccountId)
                .setServiceAccountScopes(Collections.singleton(AnalyticsScopes.ANALYTICS_READONLY))
                .setServiceAccountPrivateKeyFromP12File(new ClassPathResource("ga-secret-key.p12").getFile())
                .build();

        return credential;
    }

    @Bean
    public Analytics analytics() throws IOException,GeneralSecurityException{
        Analytics analytics = new Analytics.Builder(httpTransport, jsonFactory, httpRequestInitializer())
                .setApplicationName(applicationName)
                .build();

        return analytics;
    }

}
