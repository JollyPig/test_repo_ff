package com.scnsoft.fotaflo.webportal.dao;

import com.scnsoft.fotaflo.webportal.model.Promotion;
import com.scnsoft.fotaflo.common.dao.IDao;

public interface IPromotionDao extends IDao<Promotion, Integer> {

    Promotion getByNumber(Integer number);

}
