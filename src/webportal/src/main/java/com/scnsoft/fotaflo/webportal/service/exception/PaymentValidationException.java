package com.scnsoft.fotaflo.webportal.service.exception;

public class PaymentValidationException extends ServiceException {
    public PaymentValidationException(String message) {
        super(message);
    }
}
