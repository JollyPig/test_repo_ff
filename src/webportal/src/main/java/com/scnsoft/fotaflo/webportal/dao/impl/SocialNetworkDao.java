package com.scnsoft.fotaflo.webportal.dao.impl;

import com.scnsoft.fotaflo.common.dao.AbstractDao;
import com.scnsoft.fotaflo.webportal.dao.ISocialNetworkDao;
import com.scnsoft.fotaflo.webportal.model.SocialNetwork;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SocialNetworkDao extends AbstractDao<SocialNetwork, Integer> implements ISocialNetworkDao {

    @Override
    protected Class<SocialNetwork> getDomainClass() {
        return SocialNetwork.class;
    }

    @Override
    public SocialNetwork getByNameAndLocation(String name, Integer location, Integer server) {
        List<SocialNetwork> list = getSession().createCriteria(getDomainClass())
                .add(Restrictions.eq("name", name))
                .add(Restrictions.eq("location", location))
                .add(Restrictions.eq("serverId", server))
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .list();
        if(!list.isEmpty()){
            return list.get(0);
        }
        return null;
    }

    @Override
    public List<SocialNetwork> findByLocation(Integer location, Integer server) {
        List<SocialNetwork> list = getSession().createCriteria(getDomainClass())
                .add(Restrictions.eq("location", location))
                .add(Restrictions.eq("serverId", server))
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .list();
        return list;
    }
}
