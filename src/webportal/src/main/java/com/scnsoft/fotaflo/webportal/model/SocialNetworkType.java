package com.scnsoft.fotaflo.webportal.model;

import java.util.ArrayList;
import java.util.List;

public enum SocialNetworkType {
    facebook    ("fb", "Facebook"),
    twitter     ("tw", "Twitter"),
    tumblr      ("tb", "Tumblr"),
    pinterest   ("pin", "Pinterest");

    private String id;
    private String label;

    private SocialNetworkType(String id, String label) {
        this.id = id;
        this.label = label;
    }

    public String getId(){
        return id;
    }

    public String getLabel(){
        return label;
    }

    public static List<String> names(){
        List<String> names = new ArrayList<String>();
        for(SocialNetworkType v: values()){
            names.add(v.name());
        }
        return names;
    }
}
