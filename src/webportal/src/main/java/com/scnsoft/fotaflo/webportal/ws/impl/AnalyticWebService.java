package com.scnsoft.fotaflo.webportal.ws.impl;

import com.scnsoft.fotaflo.webportal.ws.IAnalyticWebService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class AnalyticWebService extends AbstractWebService implements IAnalyticWebService {
    public static final String RESOURCE_COUNT = "/count";
    public static final String RESOURCE_REPORT = "/report";

    @Override
    public String getPurchaseEmailCount(Integer locationId, Date startDate, Date endDate) {
        String url = getResourcePath();

        url += RESOURCE_COUNT;

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("start", startDate);
        params.put("end", endDate);
        params.put("id", locationId);

        String response = get(url, params);

        return response;
    }

    @Override
    public String getPurchaseEmails(Integer locationId, Date startDate, Date endDate) {
        String url = getResourcePath();

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("start", startDate);
        params.put("end", endDate);
        params.put("id", locationId);

        String response = get(url, params);

        return response;
    }

    @Override
    public String sendReportData(Integer locationId, Date date, Integer total, boolean groupPackage) {
        String url = getResourcePath();

        url += RESOURCE_REPORT;

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("date", date);
        params.put("locationId", locationId);
        params.put("total", total);
        params.put("groupPackage", groupPackage);

        String response = post(url, params);

        return response;
    }

    @Override
    public String getResourcePath() {
        return getRestPath() + RESOURCE;
    }
}
