package com.scnsoft.fotaflo.webportal.service.converter;

import com.scnsoft.fotaflo.webportal.analytics.AnalyticData;
import com.scnsoft.fotaflo.webportal.report.model.ColumnHeader;
import com.scnsoft.fotaflo.webportal.report.model.ReportData;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ShareDataConverter extends AnalyticDataConverter {
    @Override
    public ReportData getReportData(AnalyticData data) {
        List<ColumnHeader> columns = new ArrayList<ColumnHeader>();

        columns.add(new ColumnHeader("facebook", "Facebook", ColumnHeader.ColumnType.VALUE, ColumnHeader.ColumnDataType.NUMBER, 15));
        columns.add(new ColumnHeader("twitter", "Twitter", ColumnHeader.ColumnType.VALUE, ColumnHeader.ColumnDataType.NUMBER, 15));
        columns.add(new ColumnHeader("tumblr", "Tumblr", ColumnHeader.ColumnType.VALUE, ColumnHeader.ColumnDataType.NUMBER, 15));
        columns.add(new ColumnHeader("mail", "Mail", ColumnHeader.ColumnType.VALUE, ColumnHeader.ColumnDataType.NUMBER, 15));

        return getSimpleReportData(data, columns, "Sharing_Overview");
    }
}
