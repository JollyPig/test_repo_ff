package com.scnsoft.fotaflo.webportal.analytics.ga.tracking;

import com.scnsoft.fotaflo.common.analytics.ga.EventPayload;

public class ParametrizedEventPayload extends EventPayload {

    protected static final String usernameParam         = "cd2";
    protected static final String locationIdParam       = "cd1";
    protected static final String locationNameParam     = "cd3";

    protected static final String eventErrorParam       = "cd5";

    public ParametrizedEventPayload(String trackingId, String clientId) {
        super(trackingId, clientId);
    }

    public ParametrizedEventPayload(String protocolVersion, String trackingId, String clientId) {
        super(protocolVersion, trackingId, clientId);
    }

    public String getUsername() {
        return getParameter(usernameParam);
    }

    public void setUsername(String username) {
        addParameter(usernameParam, username);
    }

    public Integer getLocationId() {
        return (Integer)getRawParameter(locationIdParam);
    }

    public void setLocationId(Integer locationId) {
        addParameter(locationIdParam, locationId);
    }

    public String getLocationName() {
        return getParameter(locationNameParam);
    }

    public void setLocationName(String locationName) {
        addParameter(locationNameParam, locationName);
    }

    public String getEventError() {
        return getParameter(eventErrorParam);
    }

    public void setEventError(String eventError) {
        addParameter(eventErrorParam, eventError);
    }
}
