package com.scnsoft.fotaflo.webportal.dto;

import com.scnsoft.fotaflo.webportal.model.SocialNetwork;

public class SocialNetworkDto {

    private String name;

    private Integer location;

    private String title;

    private String description;

    private String address;

    private String tags;

    private String hashtags;

    public SocialNetworkDto() {
    }

    public SocialNetworkDto(SocialNetwork socialNetwork) {
        if(socialNetwork == null){
            throw new IllegalArgumentException("Entity cannot be null");
        }
        this.name = socialNetwork.getName();
        this.location = socialNetwork.getLocation();
        this.title = socialNetwork.getTitle();
        this.description = socialNetwork.getDescription();
        this.address = socialNetwork.getAddress();
        this.tags = socialNetwork.getTags();
        this.hashtags = socialNetwork.getHashtags();
    }

    public SocialNetwork toEntity(){
        SocialNetwork socialNetwork = new SocialNetwork();
        socialNetwork.setName(this.getName());
        socialNetwork.setLocation(this.getLocation());
        socialNetwork.setTitle(this.getTitle());
        socialNetwork.setDescription(this.getDescription());
        socialNetwork.setAddress(this.getAddress());
        socialNetwork.setTags(this.getTags());
        socialNetwork.setHashtags(this.getHashtags());
        return socialNetwork;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLocation() {
        return location;
    }

    public void setLocation(Integer location) {
        this.location = location;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getHashtags() {
        return hashtags;
    }

    public void setHashtags(String hashtags) {
        this.hashtags = hashtags;
    }

    @Override
    public String toString() {
        return "SocialNetworkDto{" +
                "name='" + name + '\'' +
                ", location=" + location +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", address='" + address + '\'' +
                ", tags='" + tags + '\'' +
                ", hashtags='" + hashtags + '\'' +
                '}';
    }
}
