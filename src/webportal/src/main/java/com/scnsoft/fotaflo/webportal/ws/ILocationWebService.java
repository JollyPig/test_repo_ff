package com.scnsoft.fotaflo.webportal.ws;

import com.scnsoft.fotaflo.webportal.bean.LocationBean;
import com.scnsoft.fotaflo.webportal.bean.LocationMetadataBean;

import java.util.List;

public interface ILocationWebService {

    String RESOURCE = "/web/locations";

    public List<LocationBean> getLocations();

    public LocationBean getLocation(Integer id);

    public LocationMetadataBean getLocationMetadata(Integer id);

}
