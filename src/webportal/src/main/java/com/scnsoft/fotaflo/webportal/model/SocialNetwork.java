package com.scnsoft.fotaflo.webportal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;
import com.scnsoft.fotaflo.common.model.AbstractEntity;

@Entity
@Table(name = "social_network")
public class SocialNetwork extends AbstractEntity {

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Column(name = "location", nullable = false, unique = true)
    private Integer location;

    private Integer serverId;

    @Column
    private String title;

    @Column
    private String description;

    @Column
    private String address;

    private List<String> tags = new ArrayList<String>();

    private List<String> hashtags = new ArrayList<String>();

    @Column
    public String getTags(){
        return listToString(this.tags, "@");
    }

    public void setTags(String tags){
        this.tags = parseStringToList(tags);
    }

    @Column
    public String getHashtags(){
        return listToString(this.hashtags, "#");
    }

    public void setHashtags(String htags){
        this.hashtags = parseStringToList(htags);
    }

    public SocialNetwork() {
    }

    public SocialNetwork(String name, Integer location, String title, String description, String address, String tags, String hashtags) {
        this.name = name;
        this.location = location;
        this.title = title;
        this.description = description;
        this.address = address;
        this.setTags(tags);
        this.setHashtags(hashtags);
    }

    public void addTag(String tag){
        this.tags.add(tag);
    }

    public void addHashtag(String htag){
        this.hashtags.add(htag);
    }

    protected String listToString(List<String> list, String prefix){
        String s = "",
               d = " "; // delimiter
        int i = 0;
        for(String t: list){
            i++;
            if(prefix != null){
                s += prefix;
            }
            s += t;
            if(i < list.size()){
                s += d;
            }
        }
        return s;
    }

    protected List<String> parseStringToList(String s){
        List<String> list =  new ArrayList<String>();
        if(s != null){
            String[] tgs = s.split("[, @#]");
            for(String t: tgs){
                if(!t.isEmpty()){
                    list.add(t);
                }
            }
        }
        return list;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLocation() {
        return location;
    }

    public void setLocation(Integer location) {
        this.location = location;
    }

    @Column(name = "external_server_id")
    public Integer getServerId() {
        return serverId;
    }

    public void setServerId(Integer serverId) {
        this.serverId = serverId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Transient
    public List<String> getTagList(){
        return tags;
    }

    @Transient
    public List<String> getHashtagList(){
        return hashtags;
    }

    @Override
    public String toString() {
        return "SocialNetwork{" +
                "name='" + name + '\'' +
                '}';
    }
}
