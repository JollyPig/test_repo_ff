package com.scnsoft.fotaflo.webportal.dao;

import com.scnsoft.fotaflo.webportal.model.Payment;

import java.util.List;
import com.scnsoft.fotaflo.common.dao.IDao;

/**
 * Created by Tayna on 01.05.14.
 */
public interface IPaymentDao extends IDao<Payment, Integer> {

    public List<Payment> getPaymentByTransaction(String token);
}
