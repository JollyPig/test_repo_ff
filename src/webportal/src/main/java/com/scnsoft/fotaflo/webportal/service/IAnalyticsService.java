package com.scnsoft.fotaflo.webportal.service;

import com.scnsoft.fotaflo.webportal.analytics.AnalyticData;
import com.scnsoft.fotaflo.webportal.service.impl.AnalyticsService;

import java.io.File;
import java.util.Date;
import java.util.Map;

public interface IAnalyticsService {

    AnalyticData getVisitsByDeviceType();

    Map<String, Object> getVisitsByDeviceType(Date startDate, Date endDate, Integer location);

    Map<String, Object> getVisitsByGeography(Date startDate, Date endDate, Integer location);

    Map<String, Object> getShareCount(Date startDate, Date endDate, Integer location);

    Map<String, Object> getShareCount(Date startDate, Date endDate, Integer location, AnalyticsService.DateRange range);

    File getShareReport(Date startDate, Date endDate, Integer locationId, AnalyticsService.DateRange range);

    File getDeviceTypeReport(Date startDate, Date endDate, Integer locationId, AnalyticsService.DateRange range);

    File getVisitorsOverviewReport(Date startDate, Date endDate, Integer locationId, AnalyticsService.DateRange range);

    File getPotentialSocialResearchReport(Date startDate, Date endDate, Integer locationId, AnalyticsService.DateRange range);

    void trackEvent(String clientId, String category, String action, String label, Integer value);

    void trackEvent(String clientId, String category, String action, String label, Integer value, String error);

    void trackSocial(String clientId, String category, String action, String label, Integer value, String accountName, Integer friendsCount);

    void trackSocial(String clientId, String category, String action, String label, Integer value, String accountName, Integer friendsCount, String error);

    void trackPayment(String clientId, String category, String action, String label, Integer value, Double revenue, String error);

}
