package com.scnsoft.fotaflo.webportal.analytics.ga.tracking;

import com.scnsoft.fotaflo.common.analytics.ga.Tracker;
import org.springframework.stereotype.Component;

@Component
public class GaTracker extends Tracker {

    public void trackEvent(String clientId, String username, Integer locationId, String locationName, String category, String action, String label, Integer value, String error) {
        SocialEventPayload data = createPayloadData(clientId, username, locationId, locationName, category, action, label, value);

        data.setEventError(error);

        makeRequest(data);
    }

    public void trackSocial(String clientId, String username, Integer locationId, String locationName, String category, String action, String label, Integer value, String account, Integer friends, String error) {
        SocialEventPayload data = createPayloadData(clientId, username, locationId, locationName, category, action, label, value);

        data.setEventError(error);
        data.setSocialAccount(account);
        data.setFriendsCount(friends);

        makeRequest(data);
    }

    public void trackSocial (String clientId, String category, String action, String label) {
        makeRequest (createPayloadData (clientId, null, null, null, category, action, label, 0));
    }

    private SocialEventPayload createPayloadData(String clientId, String username, Integer locationId, String locationName, String category, String action, String label, Integer value) {
        SocialEventPayload data = new SocialEventPayload(getTrackingId(), clientId);
        data.setEvent(category, action, label, value);

        data.setUsername(username);
        data.setLocationId(locationId);
        data.setLocationName(locationName);

        return data;
    }

    public void trackPayment(String clientId, String username, Integer locationId, String locationName, String category, String action, String label, Integer value, Double revenue, String error) {
        PaymentEventPayload data = new PaymentEventPayload(getTrackingId(), clientId);
        data.setEvent(category, action, label, value);

        data.setUsername(username);
        data.setLocationId(locationId);
        data.setLocationName(locationName);

        data.setRevenue(revenue);

        makeRequest(data);
    }

}
