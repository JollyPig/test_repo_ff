package com.scnsoft.fotaflo.webportal.service;

import com.scnsoft.fotaflo.webportal.bean.ShareStatusBean;
import com.scnsoft.fotaflo.webportal.model.SocialNetworkType;
import com.scnsoft.fotaflo.webportal.social.Connection;

import java.util.List;
import java.util.Map;

public interface IShareService {

    List<Connection> availableConnections();

    Connection checkConnectionStatus(String providerId);

    List<ShareStatusBean> shareImage(List<SocialNetworkType> socialNetTypes, List<Integer> images, String message, Map<String, String> tags);

    void shareImageViaEmail(String[] emails, List<Integer> images, String message, String redirectUrl);

    String createUserForSocialShare (final List<Integer> imageIds);

    void removeAllConnections();

    boolean checkFacebookTag(String tag);

}
