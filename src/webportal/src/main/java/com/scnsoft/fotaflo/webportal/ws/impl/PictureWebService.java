package com.scnsoft.fotaflo.webportal.ws.impl;

import com.scnsoft.fotaflo.webportal.bean.PictureBean;
import com.scnsoft.fotaflo.webportal.ws.IBeanWrapper;
import com.scnsoft.fotaflo.webportal.ws.IPictureWebService;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PictureWebService extends AbstractWebService implements IPictureWebService {
    @Override
    public String getResourcePath() {
        return getRestPath() + RESOURCE;
    }

    @Override
    public List<PictureBean> getPictures(String login, Date from) {
        return getPictures(login, from, null);
    }

    @Override
    public List<PictureBean> getPictures(String login, Date from, IBeanWrapper preparator) {
        if(login == null){
            return new ArrayList<PictureBean>();
        }
        String url = getResourcePath() + '/' + login;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("from", from);
        return getJsonList(get(url, params), PictureBean.class, preparator);
    }
}
