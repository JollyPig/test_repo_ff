package com.scnsoft.fotaflo.webportal.analytics.ga.tracking;

public class PaymentEventPayload extends ParametrizedEventPayload {

    protected static final String revenueParam = "cm2";

    public PaymentEventPayload(String trackingId, String clientId) {
        super(trackingId, clientId);
    }

    public PaymentEventPayload(String protocolVersion, String trackingId, String clientId) {
        super(protocolVersion, trackingId, clientId);
    }

    public Double getRevenue() {
        return (Double)getRawParameter(revenueParam);
    }

    public void setRevenue(Double revenue) {
        addParameter(revenueParam, revenue);
    }
}
