package com.scnsoft.fotaflo.webportal.bean;

import com.google.gson.annotations.SerializedName;

/**
 * User from Fotaflo Application
 */
public class UserBean {

    @SerializedName("id")
    private int id;

    @SerializedName("loginName")
    private String loginName;

    @SerializedName("password")
    private String password;

    @SerializedName("locationId")
    private Integer locationId;

    @SerializedName("access")
    private String access;

    @SerializedName("userCurrency")
    private String currency;

    @SerializedName("userPrice")
    private String price;

    @SerializedName("firstName")
    private String firstName;

    @SerializedName("lastName")
    private String lastName;

    @SerializedName("userPackagePrice")
    private String packagePrice;

    Integer serverNumber;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return Returns the loginName.
     */
    public String getLoginName() {
        return loginName;
    }

    /**
     * @param loginName The loginName to set.
     */
    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    /**
     * @return Returns the password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password The password to set.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return Returns the access.
     */
    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    /**
     * @return Returns the location.
     */
    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getServerNumber() {
        return serverNumber;
    }

    public void setServerNumber(Integer serverNumber) {
        this.serverNumber = serverNumber;
    }

    public String getPackagePrice () {
        return packagePrice;
    }

    public void setPackagePrice (String packagePrice) {
        this.packagePrice = packagePrice;
    }

    @Override
    public String toString() {
        return "UserBean{" +
                "id=" + id +
                ", loginName='" + loginName + '\'' +
                ", password='" + password + '\'' +
                ", locationId='" + locationId + '\'' +
                ", access='" + access + '\'' +
                ", currency='" + currency + '\'' +
                ", price='" + price + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", serverNumber=" + serverNumber +
                '}';
    }
}
