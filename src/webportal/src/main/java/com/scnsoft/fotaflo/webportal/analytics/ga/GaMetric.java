package com.scnsoft.fotaflo.webportal.analytics.ga;

public enum GaMetric implements IGaField {

    session             ("ga:sessions", "Visits"),
    newUsers            ("ga:newUsers", "New Users"),
    socialAction        ("ga:socialInteractions", "Social Actions"),
    eventValue          ("ga:eventValue", "Event Value"),
    totalEvents         ("ga:totalEvents", "Total Events"),
    potentialSocial     ("ga:metric1", "Potential Social Research");

    private String id;
    private String label;

    private GaMetric(String id, String label) {
        this.id = id;
        this.label = label;
    }

    public String getId(){
        return id;
    }

    public String getLabel(){
        return label;
    }

    public static GaMetric getById(String id) {
        for (GaMetric c : GaMetric.values()) {
            if (c.getId().equals(id)) {
                return c;
            }
        }
        return null;
    }
}
