package com.scnsoft.fotaflo.webportal.controller;

import com.scnsoft.fotaflo.common.analytics.ga.util.CookieManager;
import com.scnsoft.fotaflo.common.util.ServletUtil;
import com.scnsoft.fotaflo.common.util.StringUtils;
import com.scnsoft.fotaflo.webportal.service.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/main")
public class MailLinkController {
    protected static Logger logger = Logger.getLogger( MailLinkController.class );

    public static final String REDIRECT_URL_PART = "/pictures/auth/autoLogin?username=";

    @Autowired
    private IShareService shareService;

    @Autowired
    private IAnalyticsService analyticsService;

    @RequestMapping(value = "/mail", method = RequestMethod.POST)
    public
    @ResponseBody
    ResponseEntity<?> shareImage(@RequestParam("message") String message,
                                 @RequestParam("sharemails") String sharemails,
                                 @RequestParam(value = "images") List<Integer> images,
                                 HttpServletRequest request) {
        logger.info("Message: " + message + ", mails: " + sharemails + ", count of images: " + (images != null ? images.size() : 0));
        Map<String, Object> result = new HashMap<String, Object>();

        String contextPath = ServletUtil.getContextPath(request);

        if (!StringUtils.isEmpty(sharemails)) {
            String[] emails = sharemails.split(",");
            try{
                shareService.shareImageViaEmail(emails, images, message, contextPath + REDIRECT_URL_PART);
                result.put("success", true);
            }catch(Exception e){
                logger.error(e.getMessage(), e);
                result.put("success", false);
                result.put("message", e.getMessage());
            }
        }

        analyticsService.trackEvent(CookieManager.getClientId(request.getCookies()), "share", "success", "mail", images.size());
        return new ResponseEntity<Map>(result, HttpStatus.OK);
    }

}
