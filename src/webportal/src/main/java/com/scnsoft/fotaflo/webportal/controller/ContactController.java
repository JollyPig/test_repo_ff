package com.scnsoft.fotaflo.webportal.controller;

import com.scnsoft.fotaflo.webportal.service.IMailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/main")
public class ContactController {
    @Autowired
    private IMailService mailService;

    @RequestMapping(value = "/contactus", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<?> contactUs(@RequestParam("email") String from, @RequestParam("subject") String subject, @RequestParam("message") String message){

        mailService.sendMailFrom(from, subject, message);

        return new ResponseEntity<String>(HttpStatus.OK);
    }
}
