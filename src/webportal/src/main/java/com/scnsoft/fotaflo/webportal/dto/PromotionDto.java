package com.scnsoft.fotaflo.webportal.dto;

import com.scnsoft.fotaflo.webportal.model.Promotion;

public class PromotionDto {

    private Integer id;

    private Integer number;

    private String text;

    private String position;

    private String color;

    private String shadowColor;

    private String path;

    private String imageName;

    public PromotionDto() {
    }

    public PromotionDto(Promotion promotion) {
        if(promotion == null){
            throw new IllegalArgumentException("Entity cannot be null");
        }
        this.id = promotion.getId();
        this.number = promotion.getNumber();
        this.text = promotion.getText();
        this.position = promotion.getPosition();
        this.color = promotion.getColor();
        this.shadowColor = promotion.getShadowColor();
        this.setPath(promotion.getPath());
    }

    public Promotion toEntity(){
        Promotion promotion = new Promotion();
        promotion.setId(this.id);
        promotion.setNumber(this.number);
        promotion.setText(this.text);
        promotion.setPosition(this.position);
        promotion.setColor(this.color);
        promotion.setShadowColor(this.shadowColor);
        promotion.setPath(this.path);
        return promotion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getShadowColor() {
        return shadowColor;
    }

    public void setShadowColor(String shadowColor) {
        this.shadowColor = shadowColor;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
        if(path != null && path.length() > 0){
            int index = path.lastIndexOf('/') + 1;
            if(index < path.length()){
                this.imageName = path.substring(index);
            }
        }
    }

    public String getImageName() {
        return imageName;
    }

    @Override
    public String toString() {
        return "PromotionDto{" +
                "number="+ number +
                ", text='" + text + '\'' +
                ", position=" + position +
                ", color=" + color +
                ", shadowColor=" + shadowColor +
                ", path='" + path + '\'' +
                '}';
    }
}
