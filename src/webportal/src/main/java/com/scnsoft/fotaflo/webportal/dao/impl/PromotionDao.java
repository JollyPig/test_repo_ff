package com.scnsoft.fotaflo.webportal.dao.impl;

import com.scnsoft.fotaflo.common.dao.AbstractDao;
import com.scnsoft.fotaflo.webportal.dao.IPromotionDao;
import com.scnsoft.fotaflo.webportal.model.Promotion;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PromotionDao extends AbstractDao<Promotion, Integer> implements IPromotionDao {

    @Override
    protected Class<Promotion> getDomainClass() {
        return Promotion.class;
    }

    @Override
    public Promotion getByNumber(Integer number){
        List<Promotion> list = getSession().createCriteria(getDomainClass())
                .add(Restrictions.eq("number", number))
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .list();
        if(!list.isEmpty()){
            return list.get(0);
        }
        return null;
    }
}
