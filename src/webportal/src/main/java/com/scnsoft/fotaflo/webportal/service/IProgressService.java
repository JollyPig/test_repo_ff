package com.scnsoft.fotaflo.webportal.service;

import com.scnsoft.fotaflo.webportal.dto.ProgressDto;

import java.util.List;

public interface IProgressService {

    void addProgress(String id, Float progress);

    ProgressDto getProgress(String id);

    List<ProgressDto> getProgresses();

}
