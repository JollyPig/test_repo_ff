package com.scnsoft.fotaflo.webportal.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Anatol Sialitski
 */
public class SocialSharePicturesDto implements Serializable {

    private List<Integer> imageIds = new ArrayList<Integer> ();

    public List<Integer> getImageIds () {
        return imageIds;
    }

    public void setImageIds (List<Integer> imageIds) {
        this.imageIds = imageIds;
    }

}
