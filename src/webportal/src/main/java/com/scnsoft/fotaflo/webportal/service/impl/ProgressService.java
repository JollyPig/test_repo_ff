package com.scnsoft.fotaflo.webportal.service.impl;

import com.scnsoft.fotaflo.common.util.ThreadUtil;
import com.scnsoft.fotaflo.webportal.dto.ProgressDto;
import com.scnsoft.fotaflo.webportal.service.pb.ProgressManager;
import com.scnsoft.fotaflo.webportal.service.IProgressService;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.INTERFACES)
public class ProgressService implements IProgressService {
    protected static final Logger logger = Logger.getLogger(ProgressService.class);

    private ProgressManager progressManager = new ProgressManager();

    public ProgressService() { new ThreadUtil().schedule(new RemoveExpiredProgressesJob(), ProgressManager.EXPIRATION_PERIOD, ProgressManager.EXPIRATION_PERIOD);
        /*Thread job = new Thread(new RemoveExpiredProgressesJob());
        job.setDaemon(true);
        job.start();*/
    }

    @Override
    public void addProgress(String id, Float progress) {
        progressManager.addProgress(id, progress);
    }

    @Override
    public ProgressDto getProgress(String id) {
        Float pr = progressManager.getProgress(id);

        return new ProgressDto(id, pr);
    }

    @Override
    public List<ProgressDto> getProgresses() {
        List<ProgressDto> list = new ArrayList<ProgressDto>();
        Map<String, Float> map = progressManager.getProgressMap();
        for (String key: map.keySet()){
            list.add(new ProgressDto(key, map.get(key)));
        }

        return list;
    }

    private class RemoveExpiredProgressesJob implements Runnable{
        @Override
        public void run() {    progressManager.deleteExpiredProgresses();
            /*try{
                while(true){
                    progressManager.deleteExpiredProgresses();

                    Thread.sleep(ProgressManager.EXPIRATION_PERIOD);
                }
            }catch(InterruptedException e){
                 logger.warn("Thread was interrupted.");
            }*/
        }
    }
}
