package com.scnsoft.fotaflo.webportal.paypal;

import com.scnsoft.fotaflo.common.paypal.PayPal;
import com.scnsoft.fotaflo.common.paypal.PayPalAccount;
import com.scnsoft.fotaflo.common.paypal.PayPalConfigRepository;
import com.scnsoft.fotaflo.common.paypal.PayPalExpressCheckoutTemplate;
import com.scnsoft.fotaflo.webportal.model.Merchant;
import com.scnsoft.fotaflo.webportal.service.IPayPalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

@Configuration
public class PayPalConfig {

    @Autowired
    private IPayPalService payPalService;

	@Bean
	@Scope(value="singleton", proxyMode=ScopedProxyMode.TARGET_CLASS)
	public PayPalConfigRepository configRepository() {
        return new PayPalConfigRepository();
	}

	@Bean
	@Scope(value="session", proxyMode=ScopedProxyMode.INTERFACES)
	public PayPal payPal() {
        PayPal payPal = null;

        try{
            Merchant merchantAccount = payPalService.getMerchantAccount();
            if(merchantAccount != null){
                PayPalAccount account = new PayPalAccount(
                        merchantAccount.getAccount(), merchantAccount.getPassword(), merchantAccount.getSignature(),
                        merchantAccount.isSandboxMode()
                );
                payPal = new PayPalExpressCheckoutTemplate(account, configRepository());
            }
        }catch(Exception e){
            throw new RuntimeException(e);
        }
		return payPal;
	}

}
