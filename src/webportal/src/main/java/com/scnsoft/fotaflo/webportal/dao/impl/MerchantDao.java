package com.scnsoft.fotaflo.webportal.dao.impl;

import com.scnsoft.fotaflo.common.dao.AbstractDao;
import com.scnsoft.fotaflo.webportal.dao.IMerchantDao;
import com.scnsoft.fotaflo.webportal.model.Merchant;
import org.springframework.stereotype.Repository;

@Repository
public class MerchantDao extends AbstractDao<Merchant, Integer> implements IMerchantDao {

    @Override
    protected Class<Merchant> getDomainClass() {
        return Merchant.class;
    }

}
