package com.scnsoft.fotaflo.webportal.service.impl;

import java.util.*;

import com.scnsoft.fotaflo.common.util.StringUtils;
import com.scnsoft.fotaflo.webportal.bean.UserBean;
import com.scnsoft.fotaflo.webportal.dao.ISystemUserDao;
import com.scnsoft.fotaflo.webportal.model.SystemUser;
import com.scnsoft.fotaflo.webportal.security.User;
import com.scnsoft.fotaflo.webportal.ws.IUserWebService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Paradinets Tatsiana.
 * Date: 25.03.2011
 * Time: 19:30:34
 */

/**
 * A custom service for retrieving users from a custom datasource, such as a database.
 * <p/>
 * This custom service must implement Spring's {@link UserDetailsService}
 */
@Transactional(readOnly = false)
public class CustomUserDetailsService implements UserDetailsService {
    protected static Logger logger = Logger.getLogger(CustomUserDetailsService.class);

    @Autowired
    private ISystemUserDao systemUserDao;

    @Autowired
    private IUserWebService userWebService;

    /**
     * Retrieves a user record containing the user's credentials and access.
     */
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException, DataAccessException {

        UserDetails user = null;

        try {

            if (username.contains(";") || username.contains("--") || username.contains("/") || username.contains("*") || username.contains("xp_")) {
                logger.error("Possible SQL injection!!!!!!! login " + username);
                return user;
            }

            String login = "";
            String password = "";
            String access = "0";
            Integer locationId = null;
            Integer serverId = null;
            boolean copy = false;

            SystemUser dbUser = systemUserDao.getUserByLogin(username);
            UserBean userRestUser = userWebService.getUser(username);

            if(userRestUser!=null){
                logger.debug("USER IS OK");
                login = userRestUser.getLoginName().toLowerCase();
                password = userRestUser.getPassword().toLowerCase();
                access = userRestUser.getAccess();
                serverId = userRestUser.getServerNumber();
                locationId = userRestUser.getLocationId();

                copy=true;
            }else{
                if(dbUser!=null && userRestUser==null){
                    //We r working with a child
                    logger.debug("USER HAS MAY BE ALREaDY HAVE BEEN REFUNDED");
                    if(dbUser.getAccess().equals("8") || dbUser.getAccess().equals("7")){
                        if(dbUser.getLoginGroup()!=null && !dbUser.getLoginGroup().equals("")){
                            UserBean userRestUserChild = userWebService.getUser(dbUser.getLoginGroup());
                            if(userRestUserChild!=null && userRestUserChild.getAccess().equals(dbUser.getAccess())){
                                //everything is ok user still have the rights to watch
                                login = dbUser.getLoginName().toLowerCase();
                                password = dbUser.getPassword().toLowerCase();
                                access = dbUser.getAccess();
                                serverId = dbUser.getServerNumber();

                                logger.debug("USER WAS NOT REFUNDED");
                            } else{
                                //definitely refunded tags
                                dbUser.setAccess("5");
                                login = dbUser.getLoginName().toLowerCase();
                                password = dbUser.getPassword().toLowerCase();
                                access = dbUser.getAccess();
                                serverId = dbUser.getServerNumber();
                                dbUser.setAccessCode("B");
                                systemUserDao.update(dbUser);
                                logger.debug("USER WAS  REFUNDED");
                            }
                        }
                    }else{
                        login = dbUser.getLoginName().toLowerCase();
                        password = dbUser.getPassword().toLowerCase();
                        access = dbUser.getAccess();
                        serverId = dbUser.getServerNumber();
                        logger.debug("USER IS OK BUT FOR THE FIRST TIME");
                    }
                }
            }

            user = new User(
                    login,
                    password,
                    serverId,
                    getAuthorities(new Integer(access)));

            logger.info("User [ " + username + " ]" + " is logged in. Access level - " + access);

            if(copy){
                logger.debug("USER IS COPIED! ");
                if (dbUser != null) {
                    dbUser.setAccess(userRestUser.getAccess());
                    dbUser.setAccessCode(getAccessCode(userRestUser.getAccess()));
                    dbUser.setCurrency(userRestUser.getCurrency());
                    dbUser.setPrice(userRestUser.getPrice());
                    dbUser.setPackagePrice (userRestUser.getPackagePrice ());
                    dbUser.setLocationId(locationId);
                    dbUser.setServerNumber(userRestUser.getServerNumber());
                    if(dbUser.getAccess().equals("8")){  // todo
                        dbUser.setLastAccessDate(null);
                    }
                    systemUserDao.update(dbUser);
                } else {
                    dbUser = new SystemUser();
                    dbUser.setLoginName(userRestUser.getLoginName());
                    dbUser.setAccess(userRestUser.getAccess());
                    dbUser.setCurrency(userRestUser.getCurrency());
                    dbUser.setLocationId(locationId);
                    dbUser.setServerNumber(userRestUser.getServerNumber());
                    dbUser.setPrice(userRestUser.getPrice());
                    dbUser.setPackagePrice (userRestUser.getPackagePrice ());
                    dbUser.setAccessCode(getAccessCode(userRestUser.getAccess()));
                    systemUserDao.create(dbUser);
                }
            }
        } catch (Exception e) {
            logger.error("Error in retrieving user " + username, e);
            throw new UsernameNotFoundException("Error in retrieving user", e);
        }

        return user;
    }

    private String getAccessCode(String code){
        String accessCode = null;
        if(!StringUtils.isEmpty(code)) {
            if(code.equals("6") || code.equals("7") || code.equals("8")){
                accessCode = "A";
            }else if(code.equals("4") || code.equals("5")){
                accessCode = "B";
            }
        }
        logger.debug("ACCESS CODE:" + accessCode);

        return accessCode;
    }

    /**
     * Retrieves the correct ROLE type depending on the access level, where access level is an Integer.
     * Basically, this interprets the access value whether it's for a regular user or admin.
     *
     * @param access an integer value representing the access of the user
     * @return collection of granted authorities
     */
    public Collection<GrantedAuthority> getAuthorities(Integer access) {
        List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>(2);

        // UserSystem has admin access
        logger.debug("Grant ROLE_PUBLIC to this user");
        authList.add(new GrantedAuthorityImpl("ROLE_PUBLIC"));


        if (access.compareTo(2) <= 0) {
            // UserSystem has admin access
            logger.debug("Grant ROLE_USER to this user");
            authList.add(new GrantedAuthorityImpl("ROLE_USER"));
        }

        if (access.compareTo(1) == 0) {
            // UserSystem has admin access
            logger.debug("Grant ROLE_ADMIN to this user");
            authList.add(new GrantedAuthorityImpl("ROLE_ADMIN"));
        }
        // Return list of granted authorities
        return authList;
    }

}
