package com.scnsoft.fotaflo.webportal.service.util;

import com.scnsoft.fotaflo.webportal.config.RemoteServer;
import com.scnsoft.fotaflo.webportal.config.RemoteServerConfigHolder;
import com.scnsoft.fotaflo.webportal.security.User;
import com.scnsoft.fotaflo.webportal.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Map;

@Component
public class UserContextHolder {

    @Autowired
    RemoteServerConfigHolder remoteServerConfigHolder;

    public User getPrincipal(){
        User user = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal instanceof User){
            user = (User) principal;
        }

        return user;
    }

    public String getRestPath(int serverId){
        RemoteServer server = getServer(serverId);
        if(server != null){
            return server.getRestPath();
        }

        return null;
    }

    public String getRestPath(){
        User user = getPrincipal();
        if(user == null){
            throw new ServiceException("Method call for getRestPath() is not allowed, because user is not set");
        }

        return getRestPath(user.getServer());
    }

    public String getRemotePicturePath(int serverId){
        RemoteServer server = getServer(serverId);
        if(server != null){
            return server.getPicturePath();
        }

        return null;
    }

    public String getRemotePicturePath(){
        User user = getPrincipal();
        if(user == null){
            throw new ServiceException("Method call for getRemotePicturePath() is not allowed, because user is not set");
        }

        return getRemotePicturePath(user.getServer());
    }

    public RemoteServer getServer(int serverId){
        return getServers().get(serverId);
    }

    public RemoteServer getServer(){
        User user = getPrincipal();
        if(user == null){
            throw new ServiceException("Method call for getRestPath() is not allowed, because user is not set");
        }

        return getServers().get(user.getServer());
    }

    public int getServerCount(){
        return getServers().size();
    }

    public Map<Integer, RemoteServer> getServers(){
        return remoteServerConfigHolder.getRemoteServers();
    }

    public Collection<RemoteServer> getServerList(){
        return getServers().values();
    }

}
