
CREATE DATABASE /*!32312 IF NOT EXISTS*/ `fotaflo_web_db` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `fotaflo_web_db`;


#
# Table structure for table 'merchant'
#

CREATE TABLE /*!32312 IF NOT EXISTS*/ `merchant` (
  `id` int(11) NOT NULL auto_increment,
  `account` varchar(255) default NULL,
  `environment` varchar(255) default NULL,
  `password` blob,
  `signature` blob,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



#
# Dumping data for table 'merchant'
#

/*!40000 ALTER TABLE `merchant` DISABLE KEYS*/;
LOCK TABLES `merchant` WRITE;
INSERT INTO `merchant` (`id`, `account`, `environment`, `password`, `signature`) VALUES (1,'scnsofttestmail-facilitator_api1.gmail.com','sandbox',_cp1251 0x58D77FACED17B224BC0B490F16D1DFE2,_cp1251 0x86249DC9A96E5AACCDA12B9FB052EA033459A7679B46255D13BB9DB81DDD277B7C3240E75372B34EE83A1CDD4379F9079D7171A8008CD090D85C702101921F07);
UNLOCK TABLES;
/*!40000 ALTER TABLE `merchant` ENABLE KEYS*/;