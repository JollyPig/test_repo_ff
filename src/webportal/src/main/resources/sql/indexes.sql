ALTER TABLE `fotaflo_web_db`.`picture`
  ADD  INDEX `EXTERNAL_ID_INDEX` (`external_picture_id`, `external_server_id`);

ALTER TABLE `picture`
  ADD INDEX `PICTURE_URL_INDEX` (`picture_url`);
ALTER TABLE `picture`
  ADD INDEX `PICTURE_URL_SMALL_INDEX` (`picture_url_small`);