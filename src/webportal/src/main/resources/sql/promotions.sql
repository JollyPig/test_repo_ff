/*Data for the table `promotion` */

insert  into `promotion`(`id`,`color`,`number`,`path`,`position`,`shadowColor`,`text`) values (1,'rgb(255, 255, 255)',0,'/images/slider/slide1.jpg','tr','rgba(0, 0, 0, 0)','Happy Customers.'),(2,'rgb(250, 250, 250)',1,'/images/slider/slide2.jpg','tc','rgba(0, 0, 0, 0)','Exciting Adventure.'),(3,'rgb(255, 255, 255)',2,'/images/slider/slide3.jpg','bc','rgba(0, 0, 0, 0)','Sincere Emotions.'),(4,'rgb(255, 102, 0)',3,'/images/promo.jpg','tl','rgba(0, 0, 0, 0)','Incredibly <br> Easy.'),(5,'rgb(255, 255, 255)',4,'/images/promo2.jpg','mc','rgba(0, 0, 0, 0)','Sincerely,<br>Ryan O\'Grady Owner,<br>Fotaflo.');


/*Data for the table `text_promotion` */

insert  into `text_promotion`(`id`,`caption`,`number`,`text`) values (1,'Where your photos  live. And come to life.',3,'We at Fotaflo would like to sincerely thank you for purchasing and sharing your photos from your adventure activity.  By simply sharing your photos you are supporting the tourism business you attended, you are helping the staff of the business including the amazing guides who showed you a great time and you are supporting a local environmental organization supported with a percentage of every photo sold.'),(2,'Share everywhere. Right from here.',4,'By showing your photos and sharing them with friends we hope more people will be active, will get out and enjoy these amazing activities and will support the amazing businesses we work with.  Thank you from everyone at the Fotaflo team for helping your friends get to know our clients.  To find out more about Fotaflo and our culture visit\n<a href=\"http://fotaflo.com/culture\">fotaflo.com/culture</a>.');

