// To avoid java.net.SocketException: Connection reset by peer: socket write error
// http://www.oracle.com/technetwork/java/javase/documentation/tlsreadme2-176330.html
-Dsun.security.ssl.allowUnsafeRenegotiation=true