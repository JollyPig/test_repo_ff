
<div class="share js-modal-window" id="mailForm">
    <div class="share__wrapper">
        <div class="modal-close"></div>

        <h3 class="share__title">Email Photos</h3>
        <form action="<%=request.getContextPath()%>/pictures/main/mail" method="post">

            <label class="share__label">To:</label>
            <input class="share__input" name="sharemails" type="email" required="true" multiple="true"/>
            <label class="share__label">Message:</label>
            <textarea class="share_text" name="message" maxlength="4000" required="true"></textarea>


            <div class="share__clear">
                <div class="share__counter">0</div>
                <ul class="share__select">
                    <li>Selected photos</li>
                </ul>
            </div>

            <div class="share__slider">
                <ul class="share__list">
                </ul>
                <div class="share__slider-prev"></div>
                <div class="share__slider-next"></div>
            </div>

            <input class="share__submit" type="submit" value="Send"/>
            <div class="share__alert" style="display: none;"></div>
        </form>
    </div>
</div>