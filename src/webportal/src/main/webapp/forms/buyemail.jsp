<div class="share js-modal-window" id="buyemailForm">
    <div class="share__wrapper">
        <div class="modal-close"></div>

        <h3 class="share__title">Buy photos</h3>
        <form action="<%=request.getContextPath()%>/pictures/main/contactus" method="post">

            <p style="text-align: left;">Please, enter your valid email. After purchase is completed, the code will be sent to this email.</p>
            <br>
            <label class="share__label">Email</label>
            <input class="share__input" id="buyer_email_input"  name="email" type="email" required="true"/>

            <input id="continue_with_paypal" class="share__submit" type="submit" value="Continue"/>
        </form>
    </div>
</div>