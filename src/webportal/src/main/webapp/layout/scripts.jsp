<%--
  js
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<script type="text/javascript" src="<%= request.getContextPath()%>/js/jquery.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.form.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.cookie.js"></script>

<script type="text/javascript" src="<%= request.getContextPath()%>/js/datepicker.js"></script>
<script type="text/javascript" src="<%= request.getContextPath()%>/js/date/eye.js"></script>
<script type="text/javascript" src="<%= request.getContextPath()%>/js/date/utils.js"></script>
<script type="text/javascript" src="<%= request.getContextPath()%>/js/date/layout.js"></script>

<script type="text/javascript" src="<%= request.getContextPath()%>/js/graphs/amcharts.js"></script>
<script type="text/javascript" src="<%= request.getContextPath()%>/js/graphs/serial.js"></script>
<script type="text/javascript" src="<%= request.getContextPath()%>/js/graphs/light.js"></script>
<%--<script type="text/javascript" src="<%= request.getContextPath()%>/js/graphs/graphs.js"></script>--%>

<script type="text/javascript" src="<%= request.getContextPath()%>/js/caroufredsell.js"></script>

<script type="text/javascript" src="<%= request.getContextPath()%>/js/jquery.easyModal.js"></script>
<script type="text/javascript" src="<%= request.getContextPath()%>/js/jquery.lazyload.js"></script>

<script type="text/javascript" src="<%= request.getContextPath()%>/js/jquery.colorbox-min.js"></script>

<script type="text/javascript" src="<%= request.getContextPath()%>/js/service.js"></script>
<script type="text/javascript" src="<%= request.getContextPath()%>/js/analytics.js"></script>
<script type="text/javascript" src="<%= request.getContextPath()%>/js/main.js"></script>