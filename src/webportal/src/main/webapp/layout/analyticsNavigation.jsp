<%--
  Analytics Navigation Panel
--%>

<nav class="an__nav">
    <ul class="an__nav-list">
        <li class="an__nav-item">
            <a href="<%= request.getContextPath()%>/pictures/analytics/common">Visits</a>
            <ul class="an__nav-sublist">
                <li class="an__nav-subitem"><a href="<%= request.getContextPath()%>/pictures/analytics/device">Device Type</a></li>
                <li class="an__nav-subitem"><a href="<%= request.getContextPath()%>/pictures/analytics/geography">Geography</a></li>
                <li class="an__nav-subitem"><a href="<%= request.getContextPath()%>/pictures/analytics/gender">Gender & Age</a></li>
                <li class="an__nav-subitem"><a href="<%= request.getContextPath()%>/pictures/analytics/loyality">Loyalty</a></li>
                <li class="an__nav-subitem"><a href="<%= request.getContextPath()%>/pictures/analytics/codes">Codes</a></li>
            </ul>
        </li>
        <li class="an__nav-item"><a href="<%= request.getContextPath()%>/pictures/analytics/purchase">Purchases</a></li>
        <li class="an__nav-item"><a href="<%= request.getContextPath()%>/pictures/analytics/share">Sharing</a></li>
        <li class="an__nav-item"><a href="<%= request.getContextPath()%>/pictures/analytics/goals">Goals</a></li>
    </ul>
</nav>