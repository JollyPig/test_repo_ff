<%--
  Created by IntelliJ IDEA.
  User: yparfenchyk
  Date: 17.09.14
  Time: 10:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<nav class="head__nav head__nav_page_inner">
    <ul class="head__list head__list_page_inner">
        <li><a class="head__link" href="common">Home</a></li>
        <li><a class="head__link js-contactlink" href="">Contact us</a></li>
        <li><a class="head__link" href="<%= request.getContextPath()%>/pictures/auth/logout">Sign out</a></li>
    </ul>
    <div class="head__nav-ico"></div>
</nav>