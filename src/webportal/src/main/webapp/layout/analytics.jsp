<%--
    Google Analytics Tracking
--%>

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    var gaSettings = {
        locationId: '<%= session.getAttribute("location")%>',
        locationName: '<%= session.getAttribute("locationName") %>',
        username: '<%= session.getAttribute("username") %>'
    }

    var fotaflo_ga = (function(settings){
        var me = {};
        var trackIds = [];
        var pre = 'tr';

        var init = function(){
            var name;
            for(var i=0;  i<trackIds.length; i++){
                name = pre + i;
                ga(name + '.' + 'require', 'displayfeatures');
            }

            me.set('dimension1', settings.locationId);
            me.set('dimension2', settings.username);
            me.set('dimension3', settings.locationName);
        }

        me.create = function(){
            if(arguments.length > 0){
                for(var i=0;  i<arguments.length; i++){
                    ga('create', arguments[i], 'auto', {'name': 'tr' + i});
                    trackIds.push(arguments[i]);
                }

                init();
            }
        }

        me.set = function(key, value){
            var name;
            for(var i=0;  i<trackIds.length; i++){
                name = pre + i;
                ga(name + '.' + 'set', key, value);
            }
        }

        me.send = function(){
            var name;
            for(var t=0;  t<trackIds.length; t++){
                name = pre + t;
                var params = [name + '.' + 'send'];
                for(var i=0; i<arguments.length; i++){
                    params.push(arguments[i]);
                }

                ga.apply(document, params);
            }
        }

        return me;
    })(gaSettings);

    fotaflo_ga.create('UA-54493509-1', 'UA-54493509-2');
    fotaflo_ga.send('pageview');

</script>