<ul class="social-menu">
  <li><a href="#" class="js-maillink"><img src="<%= request.getContextPath()%>/images/menu/menu-mail-icon.png"></a></li>
  <li><a href="#" class="custom-interactive-link share-pictures share-on-pinterest"><img src="<%= request.getContextPath()%>/images/menu/menu-pinterest-icon.png"></a></li>
  <li><a href="#" class="js-download"><img src="<%= request.getContextPath()%>/images/menu/menu-download-icon.png"></a></li>
  <li>
    <a href="#"
       data-text="${twitter.description}"
       data-hashtags="${twitter.hashtags}"
       data-via="${twitter.tags}" class="custom-interactive-link share-pictures share-on-twitter"><img src="<%= request.getContextPath()%>/images/menu/menu-twitter-icon.png"></a></li>
  <li><a href="#" class="custom-interactive-link share-pictures share-on-facebook"><img src="<%= request.getContextPath()%>/images/menu/menu-facebook-icon.png"></a></li>
</ul>

<div style="display: none;">
  <a id="pinBtn" data-pin-do="buttonPin" data-description="${pinterest.description}"
     href="" target="_blank" data-pin-custom="true"></a>
     <%--href="https://www.pinterest.com/pin/create/button/?url=http://localhost.myziplinephotos.com/pictures/main/common&media=http%3A%2F%2Flocalhost.myziplinephotos.com%2Fimages%2Fhelmet.jpg&description=My description!" data-pin-custom="true"></a>--%>

  <script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>

  <a href="" class="twitter-share-button">Shared to Twitter</a>

  <script>
    window.twttr = (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0],
              t = window.twttr || {};
      if (d.getElementById(id)) return;
      js = d.createElement(s);
      js.id = id;
      js.src = "https://platform.twitter.com/widgets.js";
      fjs.parentNode.insertBefore(js, fjs);

      t._e = [];
      t.ready = function(f) {
        t._e.push(f);
      };

      return t;
    }(document, "script", "twitter-wjs"));

    function tweetIntentToAnalytics (intentEvent) {
      if (!intentEvent) {
        return;
      }

      fotaflo_ga.send('event', 'share', 'success', 'twitter');
    }

    // Wait for the asynchronous resources to load
    twttr.ready(function (twttr) {
      twttr.events.bind('tweet', tweetIntentToAnalytics);
    });

  </script>

  <script>
    window.fbAsyncInit = function() {
      FB.init({
        appId      : '270487023147724',
        xfbml      : true,
        version    : 'v2.5'
      });
    };

    (function(d, s, id){
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {return;}
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  </script>

</div>