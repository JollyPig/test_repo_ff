<%@ page import="org.springframework.social.facebook.api.Page" %>
<%@ page session="true" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<!doctype html>
<!--[if lt IE 8]> <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9 ie8"> <![endif]-->
<!--[if IE 9]>    <html class="ie9""> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->

<head>
    <jsp:include page="layout/head.jsp" />

    <%--<style>


        .catalog__logo {
            position: absolute;
            width: 30px;
            /*height: 30px;*/
            bottom: 10px;
            left: 10px;
            z-index: 2;
            background-size: contain;
        }

        .catalog__mainlogo {
            position: absolute;
            width: 30px;
            /*height: 30px;*/
            right: 10px;
            bottom: 10px;
            z-index: 2;
            background-size: cover;
        }

        .catalog__logotext {
            position: absolute;
            right: 50px;
            left: 50px;
            bottom: 10px;
            z-index: 2;
            color: #ffffff;
            text-shadow: #000000;
            font-size: 12px;
        }
    </style>--%>

</head>
<body>

<jsp:include page="layout/analytics.jsp" />

<div class="page">
    <header class="head head_page_inner">
        <div class="wrapper">
            <nav class="head__nav head__nav_page_inner">
                <ul class="head__list head__list_page_inner">
                    <%--<li><a class="head__link" href="<%= request.getContextPath()%>/pictures/main/common">Home</a></li>--%>
                    <li><a class="head__link js-contactlink" href="">Contact us</a></li>
                    <li><a class="head__link" href="<%= request.getContextPath()%>/pictures/auth/logout">Sign out</a></li>
                </ul>
                <div class="head__nav-ico"></div>
            </nav>

        </div>
    </header>

    <menu class="menu js-sticky">
        <div class="menu__wrapper">
        <ul class="menu__list menu__list_position_left">
            <li><a href="" class="js-select-all">Select all</a></li>
            <li><a href="" class="js-deselect-all">Reset all</a></li>
        </ul>
        <div class="menu__counter">0</div>
        <ul class="menu__list menu__list_position_right">
            <li><span>Selected photos</span></li>
        </ul>

        <ul class="menu__list menu__list_position_center">
            <li><a href="" id = "buy_pictures" class="menu__item menu__item_ico_share menu__item_bay_now" >Buy Now</a></li>
             <li class="menu__money"><span>$</span><span class="js-money">0</span></li>
           <%-- <li class="menu__price"></li>--%>
        </ul>
            <input type=hidden name=menuprice class="menu__price" value="">
            <input type="hidden" name="menu_package_price" class="menu_package_price" value="">
        </div>
    </menu>

    <section class="catalog catalog_login_none">
        <ul id="catalog_list_id" class="catalog__list">
            <c:forEach var="picture" items="${pictures}">
                <c:set var="purl" value="${picture.base64code}?id=${picture.id}" />
                <li class="catalog__item  js-image-item">
                    <%--<div class="catalog__overlay"></div>--%>
                    <div id="${picture.id}" class="catalog__add"></div>
                    <%--<c:if test="${sessionScope.mobile == false}">
                        <c:if test="${not empty picture.logoUrl}">
                            <img class="catalog__logo" src="../${picture.logoUrl}" title = "" alt="">
                        </c:if>
                        <div class="catalog__logotext">${picture.logoText}</div>
                        <c:if test="${not empty picture.logoMainUrl}">
                            <img class="catalog__mainlogo" src="../${picture.logoMainUrl}" title = "" alt="">
                        </c:if>
                    </c:if>--%>
                    <div class="catalog__watermark"></div>
                    <img class="catalog__image" src="../${purl}" alt=""/></li>
                </li>
            </c:forEach>
        </ul>
        <div class="catalog__line">

        </div>
    </section>

    <div class="bottom">
        <div class="bottom__logo">
            <img src="<%=request.getContextPath()%>/images/helmet__logo.jpg" alt=""/>
        </div>
    </div>

</div>

<footer class="footer">
    <span>Copyright � 2014 Fotaflo. All rights reserved.</span>
</footer>

<div id="fotoramaHide" class="fotoramaHide"
     data-auto="false"
     data-allowfullscreen="native"
     data-nav="false"
        >

</div>

<!--MODAL WINDOWS-->
<jsp:include page="forms/contactus.jsp" />
<jsp:include page="forms/buyemail.jsp" />


<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/caroufredsell.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.form.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.cookie.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.easyModal.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.lazyload.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/fotorama/fotorama.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/service.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/main.js"></script>

<script>

     $(function(){
         afterLoad();

         $('#buy_pictures').click(function(e){
             $('#buyemailForm').trigger('openModal');
             e.preventDefault();
             fotaflo_ga.send('event', 'purchase', 'click', 'click buy now');
         });
     })

    function afterLoad(){
         var payed = ${payed};
         var success= ${success};
         var msg = "${msg}";
         var price = "${price}";
         var packagePrice = "${packagePrice}";

        $('.menu__price').val(price);
        $('.menu_package_price').val(packagePrice);
        if(payed==true && success==true){
            fotaflo.showAlert("The payment was successful. Your new code is " + msg + " and it was sent you via email");
        }else{
            if(payed==false && success==false){
                fotaflo.showAlert("Error: " + msg);
            }
        }

        fotaflo.catalog.calculateCostForPaypal();
    }

    function getPictureIds(){
        var data = fotaflo.catalog.getSelectedPictures(),
                images = [];

        $.each(data, function(i, item){
            images.push(item.id);
        })

        return images;
    }

    $('#continue_with_paypal').click(function(e) {
        e.preventDefault();
        $('#buyemailForm').trigger('closeModal');

        payWithPayPal();
        fotaflo_ga.send('event', 'purchase', 'submit', 'click continue');
    });

    function payWithPayPal(){
        var images = getPictureIds();
        var email = $('#buyer_email_input').val() ;
        var price = $('.js-money')[0].innerHTML;
        if(images){
            $.ajax({
                url: "../../registerPayment.json",
                method: 'POST',
                data: JSON.stringify({
                    'price': price,
                    'pictureIds': images,
                    'email': email
                }),
                contentType : 'application/json'

            }).done(function( data ) {
                if(data.success==false){
                    fotaflo.showAlert("Error: " + data.msg);
                }else{
                    fotaflo_ga.send('event', 'purchase', 'redirect', 'redirect to paypal to login and pay');
                    window.location = data.msg;
                }

            })
            .fail(function() {
                 fotaflo.showAlert( "Error!" );
            })
        }else{
            fotaflo.showAlert("Please select pictures.");
        }
    }

</script>

</body>
</html>
