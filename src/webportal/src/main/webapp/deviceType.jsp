<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@ page session="true" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<!doctype html>
<!--[if lt IE 8]> <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9 ie8"> <![endif]-->
<!--[if IE 9]>    <html class="ie9""> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->

<head>
    <jsp:include page="layout/head.jsp" />

</head>
<body>

<div class="page">
    <header class="head head_page_inner">
        <div class="wrapper">
            <jsp:include page="layout/headNavigation.jsp" />

        </div>
    </header>



    <section class="an">
        <jsp:include page="layout/analyticsNavigation.jsp" />
        
        <div class="tabs">
          <div class="tabs__top">
              <jsp:include page="layout/analyticsParameterTab.jsp" />

              <ul class="tabs__head">
                  <li class="thead active">Day</li>
                  <li class="thead">Week</li>
                  <li class="thead">Month</li>
              </ul>
          </div>

            <%
                Map settings = new HashMap(){{
                    put("desktop", new String[]{"Desktop", "adm-type-desktop.png", "or"});
                    put("mobile",  new String[]{"Mobile", "adm-type-mobile.png", "lm"});
                    put("tablet",  new String[]{"Tablet", "adm-type-tablet.png", "gr"});
                    put("other",   new String[]{"Other", "adm-type-other.png", "rd"});
                }};

                request.setAttribute("settings", settings);
            %>

            <div class="adm">
                <div class="adm__block">
                    <c:forEach var="entry" items="${data.records}" >
                        <div class="adm__block">
                            <div class="adm__cell adm__cell_type_w adm__cell_bg_${settings.get(entry.key)[2]}">
                                <img class="adm__type-img" src="<%= request.getContextPath()%>/images/${settings.get(entry.key)[1]}" alt=""/>
                                <span class="adm__type-text">${settings.get(entry.key)[0]}</span>
                            </div>
                            <div class="adm__cell adm__cell_type_p ">
                                <c:if test="${entry.value.get('oldValue') > 0}">
                                    <div class="adm__cell-per adm__cell-per_type_p">
                                        <fmt:formatNumber type="PERCENT" maxFractionDigits="2">${(entry.value.get("value")-entry.value.get("oldValue"))/entry.value.get("oldValue")}</fmt:formatNumber>
                                    </div>
                                </c:if>
                                <div class="adm__cel-numbers">${entry.value.get("value")}</div>
                            </div>
                            <div class="adm__cell adm__cell_type_b">
                        <span class="adm__pers">
                            <fmt:formatNumber type="PERCENT" maxFractionDigits="2">${entry.value.get("value")/data.total}</fmt:formatNumber>
                        </span>
                            </div>
                            <div class="adm__cell adm__cell_type_g">
                                <div id="graphFacebook" class="graph">

                                </div>
                            </div>

                        </div>
                    </c:forEach>
                    <%--<div class="adm__cell adm__cell_type_w">
                        <img class="adm__type-img" src="<%= request.getContextPath()%>/images/adm-type-desktop.png" alt=""/>
                        <span class="adm__type-text">Desktop</span>
                    </div>
                    <div class="adm__cell adm__cell_type_p">
                        <span class="adm__pers">
                            60%
                        </span>
                    </div>
                    <div class="adm__cell adm__cell_type_b">
                        <div class="adm__cell-per adm__cell-per_type_p">15%</div>
                        <div class="adm__cel-numbers">1002</div>
                    </div>
                    <div class="adm__cell adm__cell_type_g">
                        <div id="graphDesktop" class="graph">

                        </div>
                    </div>
                </div>


                <div class="adm__block">
                    <div class="adm__cell adm__cell_type_w">
                        <img class="adm__type-img" src="<%= request.getContextPath()%>/images/adm-type-mobile.png" alt=""/>
                        <span class="adm__type-text">Mobile</span>
                    </div>
                    <div class="adm__cell adm__cell_type_p">
                        <span class="adm__pers">
                            60%
                        </span>
                    </div>
                    <div class="adm__cell adm__cell_type_b">
                        <div class="adm__cell-per adm__cell-per_type_m">15%</div>
                        <div class="adm__cel-numbers">1002</div>
                    </div>
                    <div class="adm__cell adm__cell_type_g">
                        <div id="graphMobile" class="graph">

                        </div>
                    </div>
                </div>


                <div class="adm__block">
                    <div class="adm__cell adm__cell_type_w">
                        <img class="adm__type-img" src="<%= request.getContextPath()%>/images/adm-type-tablet.png" alt=""/>
                        <span class="adm__type-text">Tablet</span>
                    </div>
                    <div class="adm__cell adm__cell_type_p">
                        <span class="adm__pers">
                            60%
                        </span>
                    </div>
                    <div class="adm__cell adm__cell_type_b">
                        <div class="adm__cell-per adm__cell-per_type_m">15%</div>
                        <div class="adm__cel-numbers">1002</div>
                    </div>
                    <div class="adm__cell adm__cell_type_g">
                        <div id="graphTablet" class="graph">

                        </div>
                    </div>
                </div>


                <div class="adm__block">
                    <div class="adm__cell adm__cell_type_w">
                        <img class="adm__type-img" src="<%= request.getContextPath()%>/images/adm-type-tablet.png" alt=""/>
                        <span class="adm__type-text">Other</span>
                    </div>
                    <div class="adm__cell adm__cell_type_p">
                        <span class="adm__pers">
                            60%
                        </span>
                    </div>
                    <div class="adm__cell adm__cell_type_b">
                        <div class="adm__cell-per adm__cell-per_type_m">15%</div>
                        <div class="adm__cel-numbers">1002</div>
                    </div>
                    <div class="adm__cell adm__cell_type_g">
                        <div id="graphOther" class="graph">

                        </div>
                    </div>--%>
                </div>

            </div>

        </div>
        
        
    </section>

    <div class="bottom">
        <div class="bottom__logo">
            <img src="<%= request.getContextPath()%>/images/helmet__logo.jpg" alt=""/>
        </div>
    </div>

</div>

<footer class="footer">
    <span>Copyright � 2014 Fotaflo. All rights reserved.</span>
</footer>

<!--MODAL WINDOWS-->

<jsp:include page="forms/contactus.jsp" />

<jsp:include page="layout/scripts.jsp" />

<script>

    (function($){

    })(jQuery)
</script>
</body>
</html>
