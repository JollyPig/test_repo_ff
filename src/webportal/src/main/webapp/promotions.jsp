<%@ page session="true" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<!doctype html>
<!--[if lt IE 8]> <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9 ie8"> <![endif]-->
<!--[if IE 9]>    <html class="ie9""> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->

<head>
    <jsp:include page="layout/head.jsp" />

</head>
<body>

<div class="page">
    <header class="head head_page_inner">
        <div class="wrapper">
            <nav class="head__nav head__nav_page_inner">
                <ul class="head__list head__list_page_inner">
                    <li><a class="head__link" href="common">Home</a></li>
                    <li><a class="head__link js-contactlink" href="">Contact us</a></li>
                    <li><a class="head__link" href="<%= request.getContextPath()%>/pictures/auth/logout">Sign out</a></li>
                </ul>
                <div class="head__nav-ico"></div>
            </nav>

        </div>
    </header>

    <menu class="menu js-sticky">
        <div class="menu__wrapper">
            <ul class="menu__list menu__list_position_left">
                <li><a href="common" class="js-manage-socialnet">Manage Social Network</a></li>
                <li><a class="menu__item js-manage-promotions" href="promotions">Manage Promotions</a></li>
                <c:if test="${isadmin}">
                    <li><a class="menu__item js-manage-paypal" href="paypal">Manage PayPal Settings</a></li>
                </c:if>

            </ul>

            <ul class="menu__list menu__list_position_center">
            </ul>
        </div>
    </menu>

    <section class="an">
        <h2 class="an__title">Manage Promotions</h2>
        <c:forEach var="i" begin="0" end="4">
            <c:set var="isbox" value="${!(i > 0 && i < 3)}" />
            <c:set var="box" value="${i - 1 <= 1 ? 1 : i - 1 }" />
            <c:set var="promotion" scope="session" value="${promotions.get(i)}"/>
            <c:set var="textPromotion" value="${textPromotions.get(i)}"/>

            <c:if test="${isbox}">

        <table class="mt">

            </c:if>
            <tr>
                    <%--<div>Promotion box <c:out value="${i}"/>: <c:out value="${promotion.text}"/></div>--%>
                <td class="mt__textbox">
                    <input type="hidden" name="number" value="<c:out value="${i}"/>">
                    <c:if test="${isbox}">

                    <label class="mt__label">Text Box <c:out value="${box}"/></label>
                    </c:if>
                    <input class="mt__text" type="text" name="text" value="<c:out value="${promotion.text}"/>"/>
                </td>
                <td class="mt__pos">
                    <select class="mt__select" name="position">
                        <c:forEach var="position" items="${positions}">
                            <option value="<c:out value="${position.getId()}"/>" <c:out value="${promotion.position == position.getId() ? 'selected' : ''}"/>> <c:out value="${position.getAlias()}"/></option>
                        </c:forEach>
                    </select>
                </td>
                <td class="mt__color">
                    <c:if test="${isbox}">
                    <label class="mt__label mt__label_size_small">color</label>
                    </c:if>
                    <div class="mt__colorpick js-color1">
                        <div style="background-color: <c:out value="${promotion.color}"/>;"></div>
                    </div>
                </td>
                <td class="mt__shadow">
                    <c:if test="${isbox}">
                    <label class="mt__label mt__label_size_small">shadow</label>
                    </c:if>
                    <div class="mt__colorpick js-color2">
                        <div style="background-color: <c:out value="${promotion.shadowColor}"/>;"></div>
                    </div>
                </td>
                <td class="mt__empty"></td>
                <td class="mt__image">
                    <c:if test="${isbox}">

                    <label class="mt__label">Image Box <c:out value="${box}"/> <span style="font-size: 10px;">(1920 x ~550)</span></label>
                    </c:if>
                    <input class="mt__text js-image-1-1" type="text" name="image" value="<c:out value="${promotion.imageName}"/>" placeholder="choose image" disabled/>
                    <input type="hidden" name="path" value="<c:out value="${promotion.path}"/>"/>
                </td>
                <td class="mt__submit">
                    <div class="mt__submit-wrap">
                        <div class="mt__fakefile">Replace</div>
                        <input class="mt__file js-file-1-1" type="file" name="file"/>
                    </div>

                </td>
            </tr>

            <c:if test="${box > 1}">
                <div class="mt__center promotion__textbox">
                    <label class="mt__label">Text <c:out value="${box - 1}"/></label>
                    <input type="hidden" name="number" value="<c:out value="${i}"/>">
                    <input class="mt__text" type="text" name="caption" value="<c:out value="${textPromotion.caption}"/>"/>
                    <textarea name="message" class="mt__textarea" cols="30" rows="10"><c:out value="${textPromotion.text}"/></textarea>
                </div>
            </c:if>


            <%--<c:if test="${isbox}">

        </table>

            </c:if>--%>

        </c:forEach>

        </table>

        <div class="mt__button-holder">
            <button class="mt__preview">preview</button>
            <input class="mt__submit-input" type="submit"/>
        </div>


    </section>

    <div class="bottom">
        <div class="bottom__logo">
            <img src="<%=request.getContextPath()%>/images/helmet__logo.jpg" alt=""/>
        </div>
    </div>

</div>

<footer class="footer">
    <span>Copyright � 2014 Fotaflo. All rights reserved.</span>
</footer>

<!--MODAL WINDOWS-->

<jsp:include page="forms/contactus.jsp" />


<script type="text/javascript" src="<%= request.getContextPath()%>/js/jquery.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.form.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.cookie.js"></script>
<script type="text/javascript" src="<%= request.getContextPath()%>/js/caroufredsell.js"></script>

<script type="text/javascript" src="<%= request.getContextPath()%>/js/jquery.easyModal.js"></script>
<script type="text/javascript" src="<%= request.getContextPath()%>/js/jquery.lazyload.js"></script>

<script type="text/javascript" src="<%= request.getContextPath()%>/js/colorpicker.js"></script>
<script type="text/javascript" src="<%= request.getContextPath()%>/js/date/eye.js"></script>



<script type="text/javascript" src="<%= request.getContextPath()%>/js/main.js"></script>
<script>
    (function($){
        $('.js-color1, .js-color2, .js-color3, .js-color4, .js-color5, .js-color6, .js-color7, .js-color8, .js-color9, .js-color10').ColorPicker({
            color: '#0000ff',
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(500);
                return false;
            },
            onSubmit: function (hsb, hex, rgb, el) {
                $(el).find('div').css('backgroundColor', '#' + hex);
                $('.colorpicker').fadeOut(500);
            }
        });

        $('.colorpicker_reset').on('click', function(){
            $('.colorpicker').fadeOut(500);
        })
    })(jQuery)
</script>
</body>
</html>
