<!doctype html>
<!--[if lt IE 8]> <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9 ie8"> <![endif]-->
<!--[if IE 9]>    <html class="ie9""> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->

<head>
    <jsp:include page="layout/head.jsp" />

    <link rel="stylesheet" href="<%= request.getContextPath()%>/css/graps.css">

</head>
<body>

<div class="page">
    <header class="head head_page_inner">
        <div class="wrapper">
            <jsp:include page="layout/headNavigation.jsp" />

        </div>
    </header>



    <section class="goal">
        <jsp:include page="layout/analyticsNavigation.jsp" />
        
        
      <div class="go">
            <div class="go__top">
                <button class="go__tbutton js-goal">Set New Goal</button>
            </div>


          <div class="go__item">
                <div class="go__title">
                    <div class="go__date">
                        <strong>Facebook Shares</strong> (03/03/2014 - 03/31/2014)
                    </div>
                    <div class="go__max">
                        <div class="go__edit"></div>
                        <div class="go__cancel"></div>
                    </div>
                </div>
              <table class="go__content">
                  <tr>
                      <td class="go__column go__column_width_small">
                          <div class="go__logo go__logo_type_fb">

                          </div>
                      </td>
                      <td>
                          <div class="go__line">
                              <div class="go__counter">40%</div>
                              <div class="go__percent go__percent_color_orange" style="width: 60%">
<div class="go__percent-count go__percent-count_color_orange">1400</div>
                              </div>
                              <div class="go__percent-minus"></div>
                          </div>
                      </td>
                  </tr>
              </table>
              <div class="go__bottom">
                  <div class="go__max">
                      1800
                  </div>

              </div>
          </div>


          <div class="go__item">
              <div class="go__title">
                  <div class="go__date">
                      <strong>Emails Send</strong> (03/03/2014 - 03/31/2014)
                  </div>
                  <div class="go__max">
                      <div class="go__edit"></div>
                      <div class="go__cancel"></div>
                  </div>
              </div>
              <table class="go__content">
                  <tr>
                      <td class="go__column go__column_width_small">
                          <div class="go__logo go__logo_type_email">

                          </div>
                      </td>
                      <td>
                          <div class="go__line">
                              <div class="go__counter">40%</div>
                              <div class="go__percent go__percent_color_lime" style="width: 60%">
                                  <div class="go__percent-count go__percent-count_color_lime">1400</div>
                              </div>
                              <div class="go__percent-minus"></div>
                          </div>
                      </td>
                  </tr>
              </table>
              <div class="go__bottom">
                  <div class="go__max">
                      1800
                  </div>

              </div>
          </div>

          <div class="go__item">
              <div class="go__title">
                  <div class="go__date">
                      <strong>Total Shares</strong> (03/03/2014 - 03/31/2014)
                  </div>
                  <div class="go__max">
                      <div class="go__edit"></div>
                      <div class="go__cancel"></div>
                  </div>
              </div>
              <table class="go__content">
                  <tr>
                      <td class="go__column go__column_width_small">
                          <div class="go__logo go__logo_type_totalshare">

                          </div>
                      </td>
                      <td>
                          <div class="go__line">
                              <div class="go__counter">40%</div>
                              <div class="go__percent go__percent_color_green" style="width: 60%">
                                  <div class="go__percent-count go__percent-count_color_green">1400</div>
                              </div>
                              <div class="go__percent-minus"></div>
                          </div>
                      </td>
                  </tr>
              </table>
              <div class="go__bottom">
                  <div class="go__max">1800</div>

              </div>
          </div>


          <div class="go__item">
              <div class="go__title">
                  <div class="go__date">
                      <strong>Total Visits</strong> (03/03/2014 - 03/31/2014)
                  </div>
                  <div class="go__max">
                      <div class="go__edit"></div>
                      <div class="go__cancel"></div>
                  </div>
              </div>
              <table class="go__content">
                  <tr>
                      <td class="go__column go__column_width_small">
                          <div class="go__logo go__logo_type_totalvisits">

                          </div>
                      </td>
                      <td>
                          <div class="go__line">
                              <div class="go__counter">40%</div>
                              <div class="go__percent go__percent_color_red" style="width: 60%">
                                  <div class="go__percent-count go__percent-count_color_red">1400</div>
                              </div>
                              <div class="go__percent-minus"></div>
                          </div>
                      </td>
                  </tr>
              </table>
              <div class="go__bottom">
                  <div class="go__max">
                      1800
                  </div>

              </div>
          </div>

      </div>
        
        
    </section>

    <div class="bottom">
        <div class="bottom__logo">
            <img src="<%= request.getContextPath()%>/images/helmet__logo.jpg" alt=""/>
        </div>
    </div>

</div>

<footer class="footer">
    <span>Copyright © 2014 Fotaflo. All rights reserved.</span>
</footer>

<!--MODAL WINDOWS-->

<jsp:include page="forms/contactus.jsp" />

<jsp:include page="forms/goal.jsp" />


<jsp:include page="layout/scripts.jsp" />

<script>


</script>
</body>
</html>
