<!doctype html>
<!--[if lt IE 8]> <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9 ie8"> <![endif]-->
<!--[if IE 9]>    <html class="ie9""> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->

<head>
    <jsp:include page="layout/head.jsp" />

    <link rel="stylesheet" href="<%= request.getContextPath()%>/css/graps.css">

</head>
<body>

<div class="page">
    <header class="head head_page_inner">
        <div class="wrapper">
            <jsp:include page="layout/headNavigation.jsp" />

        </div>
    </header>



    <section class="an">
        <jsp:include page="layout/analyticsNavigation.jsp" />
        
        
        <div class="tabs">
          <div class="tabs__top">
              <jsp:include page="layout/analyticsParameterTab.jsp" />

              <ul class="tabs__head">
                  <li class="thead active">Day</li>
                  <li class="thead">Week</li>
                  <li class="thead">Month</li>
              </ul>
          </div>

            <div class="loy">
                <div id="loyGraph" class="loygraph">

                </div>
            </div>


            <div class="adm adm_margin_tb">
                <div class="adm__block">
                    <div class="adm__cell adm__cell_type_s adm__cell_bg_or">
                        <span class="adm__type-text">Twitter</span>
                        <span class="adm__pers adm__pers_color_w">40%</span>
                    </div>
                    <div class="adm__cell adm__cell_type_p">
                        <div class="adm__cell-per adm__cell-per_type_m">15%</div>
                        <div class="adm__cel-numbers">1002</div>
                    </div>
                    <div class="adm__cell adm__cell_type_b">
                        <span class="adm__cell-label">Codes</span>
                        <span class="adm__pers">
                            60%
                        </span>
                    </div>
                    <div class="adm__cell adm__cell_type_b">
                        <span class="adm__cell-label">Codes</span>
                        <span class="adm__pers">
                            60%
                        </span>
                    </div>
                    <div class="adm__cell adm__cell_type_b">
                        <span class="adm__cell-label">Codes</span>
                        <span class="adm__pers">
                            60%
                        </span>
                    </div>

                </div>

                <div class="adm__block">
                    <div class="adm__cell adm__cell_type_s adm__cell_bg_lm">
                        <span class="adm__type-text">Twitter</span>
                        <span class="adm__pers adm__pers_color_w">40%</span>
                    </div>
                    <div class="adm__cell adm__cell_type_p">
                        <div class="adm__cell-per adm__cell-per_type_m">15%</div>
                        <div class="adm__cel-numbers">1002</div>
                    </div>
                    <div class="adm__cell adm__cell_type_b">
                        <span class="adm__cell-label">Codes</span>
                        <span class="adm__pers adm__pers_color_lm">
                            60%
                        </span>
                    </div>
                    <div class="adm__cell adm__cell_type_b">
                        <span class="adm__cell-label">Codes</span>
                        <span class="adm__pers adm__pers_color_lm">
                            60%
                        </span>
                    </div>
                    <div class="adm__cell adm__cell_type_b">
                        <span class="adm__cell-label">Codes</span>
                        <span class="adm__pers adm__pers_color_lm">
                            60%
                        </span>
                    </div>

                </div>
            </div>

            <table class="geo__table">
                <thead>
                <tr>
                    <th>City</th>
                    <th>Visits</th>
                    <th>Percent</th>
                </tr>
                </thead>

                <tbody>
                <tr>
                    <td><div class="geo__pad geo__pad_font_small">Toronto</div></td>
                    <td><div class="geo__pad">1002</div></td>
                    <td><div class="geo__pad">70%</div></td>
                </tr>
                <tr>
                    <td><div class="geo__pad geo__pad_font_small">Toronto</div></td>
                    <td><div class="geo__pad">1002</div></td>
                    <td><div class="geo__pad">70%</div></td>
                </tr>
                <tr>
                    <td><div class="geo__pad geo__pad_font_small">Toronto</div></td>
                    <td><div class="geo__pad">1002</div></td>
                    <td><div class="geo__pad">70%</div></td>
                </tr>
                <tr>
                    <td><div class="geo__pad geo__pad_font_small">Toronto</div></td>
                    <td><div class="geo__pad">1002</div></td>
                    <td><div class="geo__pad">70%</div></td>
                </tr>
                <tr>
                    <td><div class="geo__pad geo__pad_font_small">Toronto</div></td>
                    <td><div class="geo__pad">1002</div></td>
                    <td><div class="geo__pad">70%</div></td>
                </tr>
                </tbody>
            </table>

        </div>
        <div class="an__footer">
            <div class="an__icons">
                <div class="an__icon an__icon_type_print"></div>
                <div class="an__icon an__icon_type_download"></div>
                <div class="an__icon an__icon_type_mail"></div>
                <div class="an__icon an__icon_type_blank"></div>
            </div>
        </div>

        <div class="an__total">
            total: <span>5000</span>
        </div>




    </section>

    <div class="bottom">
        <div class="bottom__logo">
            <img src="<%= request.getContextPath()%>/images/helmet__logo.jpg" alt=""/>
        </div>
    </div>

</div>

<footer class="footer">
    <span>Copyright © 2014 Fotaflo. All rights reserved.</span>
</footer>

<!--MODAL WINDOWS-->

<jsp:include page="forms/contactus.jsp" />


<jsp:include page="layout/scripts.jsp" />

<script>

//    calendar
    (function($){
        var initLayout = function() {

            $('#js-calendar').DatePicker({
                format:'m/d/Y',
                date: $('#js-calendar').val(),
                current: $('#js-calendar').val(),
                calendars: 2,
                mode: 'range',
                position: 'bottom',
                starts: 1,

                onBeforeShow: function(){
                    $('#js-calendar').DatePickerSetDate($('#js-calendar').val(), true);
                },
                onChange: function(formated, dates){
                    $('#js-calendar').val(formated);
                }

            });

        };

        EYE.register(initLayout, 'init');
    })(jQuery)


var chart;

var chartData = [
    {
        "year":  "01/03/2014",
        "newV": 100,
        "retV": 0
    },
    {
        "year": "01/04/2014",
        "newV": 200,
        "retV": 160
    },
    {
        "year": "01/05/2014",
        "newV": 300,
        "retV": 300
    },
    {
        "year": "01/06/2014",
        "newV": 400,
        "retV": 800
    },
    {
        "year": "01/07/2014",
        "newV": 500,
        "retV": 300
    }
];


AmCharts.ready(function () {
    // SERIAL CHART
    chart = new AmCharts.AmSerialChart();
    chart.dataProvider = chartData;
    chart.categoryField = "year";
    chart.startDuration = 0;
    chart.dataDateFormat = "DD/MM/YYYY";
    chart.balloon.color = "#000000";

    // AXES
    // category
    var categoryAxis = chart.categoryAxis;
    categoryAxis.fillAlpha = 0;
    categoryAxis.fillColor = "#FAFAFA";
    categoryAxis.gridAlpha = 0;
    categoryAxis.axisAlpha = 0;
    categoryAxis.gridPosition = "start";
    categoryAxis.position = "left";

    // value
    var valueAxis = new AmCharts.ValueAxis();
    valueAxis.dashLength = 0;
    valueAxis.axisAlpha = 0;

    valueAxis.integersOnly = true;
    valueAxis.gridCount = 0;
    chart.addValueAxis(valueAxis);


    var graph = new AmCharts.AmGraph();
    graph.title = "New Visitors";
    graph.valueField = "newV";
    graph.balloonText = "[[category]]: <br /> [[value]]";
    graph.bullet = "round";
    chart.addGraph(graph);


    var graph = new AmCharts.AmGraph();
    graph.title = "Returning Visitors";
    graph.valueField = "retV";
    graph.labelPosition = "left";
    graph.balloonText = "[[category]]: <br /> [[value]]";
    graph.bullet = "round";
    chart.addGraph(graph);


   /* chart.balloon.adjustBorderColor = true;
    chart.balloon.color = "#ffffff";
    chart.balloon.borderThickness = 0;
    chart.balloon.cornerRadius = 0;
    chart.balloon.borderColor = "#ff8600";
    chart.balloon.fillColor = "#ff8600";
    chart.balloon.pointerWidth = 8;
    chart.balloon.horizontalPadding = 10;
    chart.balloon.varticalPadding = 10;
    chart.balloon.textAlign = "middle";*/

    // WRITE
    chart.write("loyGraph");
});

</script>
</body>
</html>
