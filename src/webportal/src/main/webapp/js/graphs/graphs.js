var chartD = AmCharts.makeChart("graphDesktop", {
    "type": "serial",
    "theme": "light",
    "dataDateFormat": "DD/MM/YYYY",
    "pathToImages": "../amcharts/images/",
    "dataProvider": [{
        "date": "01/03/2014",
        "value": 0
    }, {
        "date": "01/04/2014",
        "value": 80
    }, {
        "date": "01/05/2014",
        "value": 70
    }, {
        "date": "01/06/2014",
        "value": 85
    }],
    "valueAxes": [{
        "maximum": 100,
        "minimum": 0,
        "axisAlpha": 0,
        "guides": [{
            "fillAlpha": 1,
            "fillColor": "#FFFFFF",
            "lineAlpha": 0,
            "toValue": 120,
            "value": 0,
            inside: true
        }]
    }],
    "graphs": [{
        "bullet": "round",
        "valueField": "value",
        balloonText: "<span><b>[[value]] Visitors </b> <br />[[date]]</span>"
    }],
    "chartCursor": {
        "cursorAlpha": 0
    },
    "categoryField": "date",
    "categoryAxis": {
        "parseDates": true
    }
});


var chartM = AmCharts.makeChart("graphMobile", {
    "type": "serial",
    "theme": "light",
    "dataDateFormat": "DD/MM/YYYY",
    "pathToImages": "../amcharts/images/",
    "dataProvider": [{
        "date": "01/03/2014",
        "value": 0
    }, {
        "date": "01/04/2014",
        "value": 80
    }],
    "valueAxes": [{
        "maximum": 100,
        "minimum": 0,
        "axisAlpha": 0,
        "guides": [{
            "fillAlpha": 1,
            "fillColor": "#FFFFFF",
            "lineAlpha": 0,
            "toValue": 120,
            "value": 0
        }]
    }],
    "graphs": [{
        "bullet": "round",
        "valueField": "value",
        balloonText: "<span><b>[[value]] Visitors </b> <br />[[date]]</span>"
    }],
    "chartCursor": {
        "cursorAlpha": 0
    },
    "categoryField": "date",
    "categoryAxis": {
        "parseDates": true
    }
});


var balloon = chartD.balloon;
balloon.adjustBorderColor = true;
balloon.color = "#ffffff";
balloon.borderThickness = 0;
balloon.cornerRadius = 0;
balloon.borderColor = "#ff8600";
balloon.fillColor = "#ff8600";
balloon.pointerWidth = 8;
balloon.horizontalPadding = 10;
balloon.varticalPadding = 10;
balloon.textAlign = "middle";

var chart = new AmCharts.AmSerialChart();
var chartCursor = new AmCharts.ChartCursor();
chart.addChartCursor(chartCursor);