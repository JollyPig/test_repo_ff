$(document).ready (function () {

    $('[class*="share-on-"]').click (function(event) {
        var $parent = $(event.target).parent ();

        if ($parent.hasClass('share-on-pinterest')) {
            fotaflo_ga.send('event', 'click', 'success', 'pinterest');
        }
        else if ($parent.hasClass('share-on-twitter')) {
            fotaflo_ga.send('event', 'click', 'success', 'twitter');
        }
        else if ($parent.hasClass('share-on-facebook')) {
            fotaflo_ga.send('event', 'click', 'success', 'facebook');
        }
    });

    $('.custom-interactive-link').click(function(event) {
        event.preventDefault ();

        var $target = $(event.target),
            images = $('.catalog__add_state_selected'),
            imageIds = [],
            fotarama = false;

        if (images.length == 0) {
            images = fotaflo.catalog.getSelectedPictures ();
            fotarama = true
        }



        if (! ($target.hasClass('js-maillink') && $target.hasClass ('js-download'))) {
            $.each(images, function(index, element) {
                if (! fotarama) {
                    imageIds.push ($(element).attr ('id'));
                }
                else {
                    imageIds.push (element.id);
                }
            });

            if (imageIds.length) {
                $.ajax({
                    url: 'generateShareLink',
                    contentType: 'application/json',
                    type: 'POST',
                    data: JSON.stringify({
                        imageIds: imageIds
                    }),
                    success: function (response) {
                        doShare ($target.parent (), response, imageIds[0])
                    }
                });
            }
        }

        return false;
    });

    function doShare ($node, url, pictureId) {
        var encodedUrl = encodeURIComponent (url);
        var imageUrl = window.location.protocol + '//' + window.location.hostname + '/pictures/' + pictureId + '/social';
        var encodedTrackUrl = '';
        var result = '';

        if ($node.hasClass ('share-on-pinterest')) {
            var pinBtn = $('#pinBtn')[0];

            encodedTrackUrl = encodeURIComponent (imageUrl);

            result = 'https://www.pinterest.com/pin/create/button/?url=' + encodedUrl + '&media=' + encodedTrackUrl + '&description=' + encodeURIComponent ($(pinBtn).attr('data-description'));

            window.open(result, "pinterestwindow", "height=450, width=550, toolbar=0, location=0, menubar=0, directories=0, scrollbars=0" );
            $(pinBtn).attr ('href', result);
        }
        else if ($node.hasClass ('share-on-twitter')) {
            var params ='url=' + url;
            params += "&text=" + $node.attr ('data-text');
            params += "&via=" + $node.attr ('data-via');
            params += "&hashtags=" + $node.attr ('data-hashtags');

            window.open( "http://twitter.com/intent/tweet?" + params, "twitterwindow", "height=450, width=550, toolbar=0, location=0, menubar=0, directories=0, scrollbars=0" );
            twttr.events.trigger ("tweet", {});
        }
        else if ($node.hasClass ('share-on-facebook')) {
            $('[property="og:image"]').attr ('content', imageUrl);
            $('[property="og:url"]').attr ('content', url);

            FB.ui({
                method: 'share_open_graph',
                action_type: 'og.shares',
                action_properties: JSON.stringify({
                    object : {
                        'og:url': url,
                        'og:title': $($('meta[property="og:title"]')[0]).attr('content'),
                        'og:description': $($('meta[property="og:description"]')[0]).attr('content'),
                        'og:image': imageUrl
                    }
                })
            }, function(res) {
                if (res.hasOwnProperty ('post_id') && res['post_id'] !== undefined) {
                    fotaflo_ga.send('event', 'share', 'success', 'facebook');
                }
            });

        }
    }

});