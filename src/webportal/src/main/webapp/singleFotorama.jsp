<%@ page session="true" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<!doctype html>
<!--[if lt IE 8]> <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9 ie8"> <![endif]-->
<!--[if IE 9]>    <html class="ie9""> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->

<head>
    <jsp:include page="layout/head.jsp" />
    <meta property="fb:app_id" content="270487023147724" />
    <meta property="og:type"   content="website" />
    <meta property="og:url"    content="" />
    <meta property="og:title"  content="${facebook.title}" />
    <meta property="og:description" content="${facebook.description}" />
    <meta property="og:image"  content="" />
</head>
<body class="body_color_dark body_type_single">

<jsp:include page="layout/analytics.jsp" />

<div>
    <header class="head">
        <div class="wrapper">
            <nav class="head__nav head__nav_page_inner">
                <ul class="head__list head__list_page_inner">
                    <li><a class="head__link" href="<%= request.getContextPath()%>/pictures/main/common">Home</a></li>
                    <li><a class="head__link" href="<%= request.getContextPath()%>/pictures/auth/logout">Sign out</a></li>
                    <li><a class="head__link js-contactlink" href="">Contact us</a></li>
                </ul>
                <div class="head__nav-ico"></div>
            </nav>

        </div>
    </header>

    <menu class="menu fixed-menu">
        <div class="menu__wrapper">

        <a class="arrow_back" onclick="window.location='common'; return false;" href="#">
            <img src="<%= request.getContextPath()%>/images/arrow-copy.png">
        </a>

        <ul class="menu__list menu__list_position_left">
            <li><a href="" class="js-select-all">Select all</a></li>
            <li><a href="" class="js-deselect-all">Reset all</a></li>
        </ul>
        <div class="menu__counter">0</div>
        <ul class="menu__list menu__list_position_right">
            <li><span>Selected photos</span></li>
        </ul>

        <%--<ul class="menu__list menu__list_position_center">
            <li><a class="menu__item menu__item_ico_share js-sharelink" href="">Share</a></li>
            <li><a class="menu__item menu__item_ico_download js-download" href="">Download</a></li>
            <li><a class="menu__item menu__item_ico_mail js-maillink" href="">Email Photos</a></li>
            <li><a class="menu__item menu__item_ico_slide js-slideshow"  id="js-slideshow" href="#">Slideshow</a></li>
        </ul>--%>
        <jsp:include page="socialMenu.jsp"/>
        </div>
    </menu>

    <a class="clideshow__back" onclick="window.location='common'; return false;" href="#">back</a>

    <section class="js-image-list" id="photo" >

        <div class="photo__images" >
            <div class="fotorama photo__slider-list" id="fotorama"
                data-minwidth="100%"
                data-nav="thumbs"
                data-swipe="true"
                data-click="false"
                data-allowfullscreen="native"
                data-height="90%"
                data-auto="false"
                data-thumbmargin="10"
                data-thumbfit="contain"
                data-thumbwidth="175"
                data-thumbheight="120"
                data-arrows="always"
                data-keyboard="true"
                data-hash="true">

                <c:forEach var="picture" items="${pictures}">
                    <c:set var="i" value="${i + 1}" />
                    <c:set var="purl" value="${picture.base64code}?id=${picture.id}" />
                    <img data-pid="${picture.id}" class="fotorama__link" src="../${purl}" data-hash="${i}" alt="" style="visibility: hidden;">
                </c:forEach>

            </div>

        </div>

    </section>

</div>

<footer class="footer">
    <span>Copyright � 2014 Fotaflo. All rights reserved.</span>
</footer>


<!--MODAL WINDOWS-->
<jsp:include page="forms/share.jsp" />
<jsp:include page="forms/maillink.jsp" />
<jsp:include page="forms/contactus.jsp" />


<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.form.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.cookie.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/caroufredsell.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/fotorama/fotorama.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/tappy.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.loadmask.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.easyModal.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.lazyload.js"></script>

<script type="text/javascript" src="<%= request.getContextPath()%>/js/jquery.colorbox-min.js"></script>

<!--<script type="text/javascript" src="<%=request.getContextPath()%>/js/fullscreen.min.js"></script>-->

<script type="text/javascript" src="<%=request.getContextPath()%>/js/service.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/main.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/socialMenu.js?v=2"></script>

</body>
</html>
