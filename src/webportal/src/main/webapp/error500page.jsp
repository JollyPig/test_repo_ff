<%--
  Internal Server Error Page
--%>
<%@ page session="true" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<!doctype html>
<!--[if lt IE 8]> <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9 ie8"> <![endif]-->
<!--[if IE 9]>    <html class="ie9""> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->
<head>
    <title>Server Error</title>
    <jsp:include page="layout/head.jsp" />
</head>
<body>
    <div class="page">
        <header class="head head_page_inner">
            <div class="wrapper">
                <nav class="head__nav head__nav_page_inner">
                    <ul class="head__list head__list_page_inner">
                        <li><a class="head__link" href="<%= request.getContextPath()%>/pictures/main/common">Home</a></li>
                        <li><a class="head__link js-contactlink" href="">Contact us</a></li>
                        <li><a class="head__link" href="<%= request.getContextPath()%>/pictures/auth/logout">Sign out</a></li>
                    </ul>
                    <div class="head__nav-ico"></div>
                </nav>

            </div>
        </header>



        <section style="margin: 10%; text-align: center;">
            <p style="font-size: 28px; font-weight: bold;">Load error, please try again.</p>
            <p>Contact <a style="text-decoration: underline; color: black;" href="mailto:support@fotaflo.com?Subject=Internal%20server%20error">support@fotaflo.com</a> if you continue to experience problems</p>
        </section>

        <div class="bottom">
            <div class="bottom__logo">
                <img src="<%= request.getContextPath()%>/images/helmet__logo.jpg" alt=""/>
            </div>
        </div>

    </div>

    <footer class="footer">
        <span>Copyright � 2014 Fotaflo. All rights reserved.</span>
    </footer>

    <!--MODAL WINDOWS-->

    <jsp:include page="forms/contactus.jsp" />

    <jsp:include page="layout/scripts.jsp" />
</body>
</html>