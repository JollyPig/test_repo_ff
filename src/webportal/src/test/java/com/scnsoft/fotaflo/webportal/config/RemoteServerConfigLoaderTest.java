package com.scnsoft.fotaflo.webportal.config;

import java.util.Map;

public class RemoteServerConfigLoaderTest {

    private final static String filePath = Thread.currentThread().getContextClassLoader().getResource("conf/fotaflo-webapp-servers.xml").getPath();
//    private final static String filePath = "C:\\Program Files\\apache-tomcat-7.0.42\\conf\\fotaflo-webapp-servers.xml";

    public static void main(String[] args){
//        RemoteServerConfigHolder.saveConfig(filePath);
        Map<Integer, RemoteServer> servers = new RemoteServerConfigHolder(filePath).getRemoteServers();
        for (Integer id: servers.keySet()){
            RemoteServer server = servers.get(id);
            System.out.println("Server #" + id + ": rest="+server.getRestPath()+", pic="+server.getPicturePath()
                                    +", user="+server.getPrincipal().getUsername()+":"+server.getPrincipal().getPassword());
        }
    }

}
