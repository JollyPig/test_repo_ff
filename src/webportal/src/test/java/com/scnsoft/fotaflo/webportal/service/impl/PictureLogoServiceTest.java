package com.scnsoft.fotaflo.webportal.service.impl;

import com.scnsoft.fotaflo.webportal.bean.PictureBean;
import com.scnsoft.fotaflo.webportal.service.AbstractServiceTest;
import com.scnsoft.fotaflo.webportal.service.IPictureLogoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;

public class PictureLogoServiceTest extends AbstractServiceTest {

    @Autowired
    IPictureLogoService pictureLogoService;

    @Test(enabled=false)
    public void loadMarkedFullsizePicture() throws Exception{
        PictureBean bean = new PictureBean(1, "pic1","img/MINSK-Testwww/2014/09/19/Vladi/IMG_20140623_123948-19.09.2014 00-00.jpg",
                null,null,new Date(),false, "img/mainlogo/mainlogo_Boler%20Mountain.png", null, "test watermark");

        FileOutputStream fos = new FileOutputStream(new File("c://uploaddir//fotaflo_watermark_test.jpg"));
        Date sd = new Date();

        pictureLogoService.writeMarkedPicture(bean, fos, false);

        log("After save", sd);

        fos.close();
    }

    @Test(enabled=false)
    public void loadMarkedThumbPicture() throws Exception{
        PictureBean bean = new PictureBean(1, "pic1","img_small/MINSK-Testwww/2014/09/19/Vladi/IMG_20140623_123948-19.09.2014 00-00.jpg",
                null,null,new Date(),false, "img/mainlogo/mainlogo_Boler%20Mountain.png", null, "test watermark");

        FileOutputStream fos = new FileOutputStream(new File("c://uploaddir//fotaflo_watermark_test.jpg"));
        Date sd = new Date();

        pictureLogoService.writeMarkedPicture(bean, fos, false);

        log("After save", sd);

        fos.close();
    }

    private void log(String message, Date date){
        logger.info(message + ", " + getDateDiff(date) + "ms");
    }

    private long getDateDiff(Date date){
        return (new Date().getTime() - date.getTime());
    }
}
