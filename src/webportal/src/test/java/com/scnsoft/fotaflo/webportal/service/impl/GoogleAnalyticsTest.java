package com.scnsoft.fotaflo.webportal.service.impl;

import com.scnsoft.fotaflo.webportal.service.AbstractServiceTest;
import com.scnsoft.fotaflo.webportal.service.IAnalyticsService;
import com.scnsoft.fotaflo.webportal.service.impl.AnalyticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import java.util.Calendar;
import java.util.Date;

public class GoogleAnalyticsTest extends AbstractServiceTest {

    @Autowired
    IAnalyticsService analyticsService;

    @Test(enabled = false)
    public void loadDeviceTypeAnalytics(){
        analyticsService.getVisitsByDeviceType();
    }

    @Test(enabled = false)
    public void loadGeographyAnalytics(){
        Date startDate; Date endDate;

        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, -25);

        startDate = c.getTime();
        c.add(Calendar.DATE, 23);
        endDate = c.getTime();

        Integer locationId = null;
        analyticsService.getVisitsByGeography(startDate, endDate, locationId);
    }

    /*@Test
    public void loadShareAnalytics(){
        AnalyticData data = analyticsService.getShareCount();
        ReportData reportData = new ReportData();
        reportData.setColumns(Arrays.asList(new ColumnHeader("")));
        String report = ReportBuilder.csvReports().createReport(data.getColumns(), data.getRows());

        logger.info(report);
    }*/

    @Test(enabled = false)
    public void loadShareAnalyticsForReports(){
        AnalyticsService.DateRange range = AnalyticsService.DateRange.WEEK;

        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, -20);
        Date startDate = c.getTime(), endDate = new Date();
        analyticsService.getShareReport(startDate, endDate, null, range);

        c.add(Calendar.DATE, -10);
        startDate = c.getTime();
        endDate = new Date();
        analyticsService.getShareReport(startDate, endDate, null, range);
        //String report = new ReportBuilder().createCSV(data.getColumns(), data.getRows());

        //logger.info(report);
    }

    @Test(enabled = false)
    public void loadDeviceTypeAnalyticsForReports(){
        AnalyticsService.DateRange range = AnalyticsService.DateRange.WEEK;

        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, -20);
        Date startDate = c.getTime(), endDate = new Date();
        analyticsService.getDeviceTypeReport(startDate, endDate, null, range);

        c.add(Calendar.DATE, -10);
        startDate = c.getTime();
        endDate = new Date();
        analyticsService.getDeviceTypeReport(startDate, endDate, null, range);
        //String report = new ReportBuilder().createCSV(data.getColumns(), data.getRows());

        //logger.info(report);
    }

    @Test(enabled = true)
    public void loadVisitorsAnalyticsForReports(){
        AnalyticsService.DateRange range = AnalyticsService.DateRange.WEEK;

        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, -20);
        Date startDate = c.getTime(), endDate = new Date();
        analyticsService.getVisitorsOverviewReport(startDate, endDate, 1, range);
    }

    @Test(enabled = false)
    public void loadPotentialAnalyticsForReports(){
        AnalyticsService.DateRange range = AnalyticsService.DateRange.WEEK;

        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, -20);
        Date startDate = c.getTime(), endDate = new Date();
        analyticsService.getPotentialSocialResearchReport(startDate, endDate, null, range);
    }
}
