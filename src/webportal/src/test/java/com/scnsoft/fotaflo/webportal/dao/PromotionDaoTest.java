package com.scnsoft.fotaflo.webportal.dao;

import com.scnsoft.fotaflo.webportal.model.Promotion;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PromotionDaoTest extends AbstractDaoTest<Promotion> {

    @Autowired
    IPromotionDao promotionDao;

    @Override
    protected Object[][] getTestData() {
        return new Object[][]{
                {getEntity(1)},
                {getEntity(2)},
                {getEntity(3)},
                {getEntity(4)},
        };
    }

    private Promotion getEntity(int number){
        return new Promotion(number, "text", null, null, null, "path"+number);
    }

    @Override
    protected IPromotionDao getDao() {
        return promotionDao;
    }

    @Override
    protected void modifyEntity(Promotion entity) {
        entity.setText("modified");
    }

    @Override
    protected boolean isEntitiesEqual(Promotion entity1, Promotion entity2) {
        return (entity1.getNumber() != null && entity1.getNumber().equals(entity2.getNumber()))
                && (entity1.getPath() != null && entity1.getPath().equals(entity2.getPath()))
                && (entity1.getText() != null && entity1.getText().equals(entity2.getText()));
    }

    @Test(priority = 1, enabled = true)
    public void getByNumber(){
        Promotion entity;
        for(Promotion e: persistentData.keySet()){
            logger.info("Get by number - "+e.getNumber());
            entity = getDao().getByNumber(e.getNumber());
            Assert.assertNotNull(entity);
            Assert.assertEquals(entity.getId(), (int)persistentData.get(e));
        }
    }
}
