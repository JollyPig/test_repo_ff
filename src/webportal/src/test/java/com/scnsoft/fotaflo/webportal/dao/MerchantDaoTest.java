package com.scnsoft.fotaflo.webportal.dao;

import com.scnsoft.fotaflo.webportal.model.Merchant;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigInteger;
import java.security.SecureRandom;

public class MerchantDaoTest extends AbstractDaoTest<Merchant> {

    @Autowired
    IMerchantDao merchantDao;

    @Override
    protected Object[][] getTestData() {
        return new Object[][]{
                {getEntity()},
                {getEntity()},
                {getEntity()},
                {getEntity()},
                {getEntity()}
        };
    }

    private Merchant getEntity(){
        Merchant entity = new Merchant();
        String code = generateCode(8);
        entity.setAccount(code);
        entity.setEnvironment("env$"+code);
        entity.setPassCode(code.getBytes());
        return entity;
    }

    public String generateCode(int length) {
        String seq = new BigInteger(60, new SecureRandom()).toString(32);
        return seq.substring(0,length);
    }

    @Override
    protected IMerchantDao getDao() {
        return merchantDao;
    }

    @Override
    protected void modifyEntity(Merchant entity) {
        entity.setEnvironment("env$"+generateCode(8));
    }

    @Override
    protected boolean isEntitiesEqual(Merchant entity1, Merchant entity2) {
        logger.info(entity1.getPassCode()+"~"+entity2.getPassCode()+"="+(entity1.getPassCode() != null && entity1.getPassCode().equals(entity2.getPassCode())));
        return (entity1.getAccount() != null && entity1.getAccount().equals(entity2.getAccount()))
                && (entity1.getEnvironment() != null && entity1.getEnvironment().equals(entity2.getEnvironment()));
    }
}
