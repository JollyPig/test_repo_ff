package com.scnsoft.fotaflo.webportal.service.impl;

import com.scnsoft.fotaflo.webportal.service.AbstractServiceTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

public class MailServiceTest extends AbstractServiceTest {

    @Autowired
    MailService mailService;

    @Test(enabled = false)
    public void send(){
        String from = "dontreplyfotaflo@kwic.com";
        String to = "scnsofttestmail@gmail.com";
        mailService.sendMail(from, "test", "Simple test", to);
    }

}
