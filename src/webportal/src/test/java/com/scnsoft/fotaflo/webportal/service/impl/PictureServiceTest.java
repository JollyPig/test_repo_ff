package com.scnsoft.fotaflo.webportal.service.impl;

import com.scnsoft.fotaflo.webportal.bean.PictureBean;
import com.scnsoft.fotaflo.webportal.dao.ISystemUserDao;
import com.scnsoft.fotaflo.webportal.model.SystemUser;
import com.scnsoft.fotaflo.webportal.service.AbstractServiceTest;import com.scnsoft.fotaflo.webportal.service.impl.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.*;

public class PictureServiceTest extends AbstractServiceTest {

    @Autowired
    PictureService pictureService;

    @Autowired
    ISystemUserDao systemUserDao;

    Comparator<PictureBean> comparator = new Comparator<PictureBean>() {

        public int compare(PictureBean o1, PictureBean o2) {
            Date d1 = o1.getCreationDate(), d2 = o2.getCreationDate();
            if(d1 == null){
                return 1;
            }else if(d2 == null){
                return -1;
            }else {
                return d2.compareTo(d1);
            }
        }
    };

    @DataProvider
    public Object[][] getData(){
        return new Object[][]{
                {getEntity(1, "Asrt1in", 6, 1), "few images (less than 10)"},
                {getEntity(2, "Aejp49g", 6, 1), "some images (about 40)"},
                {getEntity(3, "As40vbh", 6, 1), "many images (more than 200)"}
        };
    }

    private SystemUser getEntity(Integer id, String login, Integer access, Integer locationId){
        SystemUser user = new SystemUser();
        user.setId(id);
        user.setLoginName(login);
        user.setAccess(access+"");
        user.setLocationId(locationId);
        user.setServerNumber(0);
        return user;
    }

    @Test(enabled = false)
    public void sortPictures(){
        final Calendar c = Calendar.getInstance();
        List<PictureBean> list = new ArrayList<PictureBean>(){{
            add(new PictureBean(1, "pic1", "/",null,null,null, c.getTime(), false,null,null,null));
            c.add(Calendar.DATE, 15);
            add(new PictureBean(2, "pic2", "/",null,null,null, c.getTime(), false,null,null,null));
            c.add(Calendar.DATE, -30);
            add(new PictureBean(3, "pic3", "/",null,null,null, c.getTime(), false,null,null,null));
            c.add(Calendar.DATE, -30);
            add(new PictureBean(4, "pic4", "/",null,null,null, c.getTime(), false,null,null,null));
            c.add(Calendar.DATE, 70);
            add(new PictureBean(5, "pic5", "/",null,null,null, c.getTime(), false,null,null,null));
            c.add(Calendar.DATE, -25);
            add(new PictureBean(6, "pic6", "/",null,null,null, c.getTime(), false,null,null,null));
        }};

        Collections.sort(list, new Comparator<PictureBean>() {

            public int compare(PictureBean o1, PictureBean o2) {
                Date d1 = o1.getCreationDate(), d2 = o2.getCreationDate();
                if (d1 == null) {
                    return 1;
                } else if (d2 == null) {
                    return -1;
                } else {
                    return d2.compareTo(d1);
                }
            }
        });

        logger.info("list1: "+list);
    }

    @Test(enabled = false, dataProvider = "getData")
    public void getPictures(SystemUser user, String label){
        systemUserDao.create(user);

        logger.info("get web service data");

        long start = System.currentTimeMillis();

        List<PictureBean> rws = pictureService.getPicturesFromWebService(user);

        long end = System.currentTimeMillis();
        Collections.sort(rws, comparator);
        long afterSorting = System.currentTimeMillis();

        logger.info("Results for "+label);
        logger.info("From ws took " + rws.size() + " images, " +
                "load time: " + (end - start) + ", " +
                "sorting time: " + (afterSorting - end));

        logger.info(rws);
    }

}
