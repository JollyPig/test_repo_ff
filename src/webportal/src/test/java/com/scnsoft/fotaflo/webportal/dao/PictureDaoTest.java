package com.scnsoft.fotaflo.webportal.dao;

import com.scnsoft.fotaflo.webportal.model.Picture;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.math.BigInteger;
import java.security.SecureRandom;

public class PictureDaoTest  extends AbstractDaoTest<Picture> {

    @Autowired
    IPicturesDao picturesDao;

    @Override
    protected Object[][] getTestData() {
        return new Object[][]{
                {getEntity()},
                {getEntity()},
                {getEntity()},
                {getEntity()},
                {getEntity()}
        };
    }

    private Picture getEntity(){
        Picture entity = new Picture();
        String code = generateCode(8);
        entity.setName("name$" + code);
        entity.setUrl(code);
        entity.setServerId(0);
        entity.setPictureId(new SecureRandom().nextInt());

        logger.info("Picture: "+entity.getName()+", "+entity.getUrl());
        return entity;
    }

    public String generateCode(int length) {
        String seq = new BigInteger(60, new SecureRandom()).toString(32);
        return seq.substring(0,length);
    }

    @Override
    protected IPicturesDao getDao() {
        return picturesDao;
    }

    @Override
    protected void modifyEntity(Picture entity) {
        entity.setUrl(entity.getUrl()+" modifyed");
    }

    @Override
    protected boolean isEntitiesEqual(Picture entity1, Picture entity2) {
        return (entity1.getName() != null && entity1.getName().equals(entity2.getName()))
                && (entity1.getUrl() != null && entity1.getUrl().equals(entity2.getUrl()));
    }

    @Test(priority = 1, enabled = true)
    public void getPictureByExternalId(){
        Picture result;
        for(Picture e: persistentData.keySet()){
            if(e.getPictureId() != null){
                logger.info("Get pic by external id - "+e.getPictureId());
                result = getDao().getPictureByExternalId(e.getPictureId(), e.getServerId());
                logger.info(result);
                Assert.assertNotNull(result);
            }
        }
    }

}
