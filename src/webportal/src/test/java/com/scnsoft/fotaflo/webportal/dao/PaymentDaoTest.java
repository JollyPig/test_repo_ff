package com.scnsoft.fotaflo.webportal.dao;

import com.scnsoft.fotaflo.webportal.model.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.List;

public class PaymentDaoTest extends AbstractDaoTest<Payment> {

    @Autowired
    IPaymentDao paymentDao;

    @Override
    protected Object[][] getTestData() {
        return new Object[][]{
                {getEntity()},
                {getEntity()},
                {getEntity()},
                {getEntity()},
                {getEntity()}
        };
    }

    private Payment getEntity(){
        Payment entity = new Payment();
        String code = generateCode(8);
        entity.setUserLogin(code);
        entity.setToken("token$"+code);
        entity.setNewLogin("n" + code);
        return entity;
    }

    public String generateCode(int length) {
        String seq = new BigInteger(60, new SecureRandom()).toString(32);
        return seq.substring(0,length);
    }

    @Override
    protected IPaymentDao getDao() {
        return paymentDao;
    }

    @Override
    protected void modifyEntity(Payment entity) {
        entity.setNewLogin(generateCode(8));
    }

    @Override
    protected boolean isEntitiesEqual(Payment entity1, Payment entity2) {
        return (entity1.getUserLogin() != null && entity1.getUserLogin().equals(entity2.getUserLogin()))
                && (entity1.getToken() != null && entity1.getToken().equals(entity2.getToken()))
                && (entity1.getNewLogin() != null && entity1.getNewLogin().equals(entity2.getNewLogin()));
    }

    @Test(priority = 1, enabled = true)
    public void getPaymentByTransaction(){
        List<Payment> result;
        for(Payment e: persistentData.keySet()){
            logger.info("Get by token - "+e.getToken());
            result = getDao().getPaymentByTransaction(e.getToken());
            logger.info(result);
            Assert.assertNotNull(result);
        }
    }
}
