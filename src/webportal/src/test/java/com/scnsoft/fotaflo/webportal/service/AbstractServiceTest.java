package com.scnsoft.fotaflo.webportal.service;

import com.scnsoft.fotaflo.webportal.AbstractTest;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@TransactionConfiguration(defaultRollback = true)
@Transactional
public abstract class AbstractServiceTest extends AbstractTest {
}
