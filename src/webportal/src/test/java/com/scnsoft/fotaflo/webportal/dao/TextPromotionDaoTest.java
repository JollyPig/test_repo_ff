package com.scnsoft.fotaflo.webportal.dao;

import com.scnsoft.fotaflo.webportal.model.TextPromotion;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Random;

public class TextPromotionDaoTest extends AbstractDaoTest<TextPromotion> {

    @Autowired
    ITextPromotionDao textPromotionDao;

    @Override
    protected Object[][] getTestData() {
        return new Object[][]{
                {getEntity(1)},
                {getEntity(2)},
                {getEntity(3)},
                {getEntity(4)},
        };  //To change body of implemented methods use File | Settings | File Templates.
    }

    private TextPromotion getEntity(int number){
        return new TextPromotion(number, "promotion#"+number, "text");
    }

    @Override
    protected ITextPromotionDao getDao() {
        return textPromotionDao;
    }

    @Override
    protected void modifyEntity(TextPromotion entity) {
        entity.setText("modified");
    }

    @Override
    protected boolean isEntitiesEqual(TextPromotion entity1, TextPromotion entity2) {
        return (entity1.getNumber() != null && entity1.getNumber().equals(entity2.getNumber()))
                && (entity1.getCaption() != null && entity1.getCaption().equals(entity2.getCaption()))
                && (entity1.getText() != null && entity1.getText().equals(entity2.getText()));
    }

    @Test(priority = 1, enabled = true)
    public void getByNumber(){
        TextPromotion entity;
        for(TextPromotion e: persistentData.keySet()){
            logger.info("Get by number - "+e.getNumber());
            entity = getDao().getByNumber(e.getNumber());
            Assert.assertNotNull(entity);
            Assert.assertEquals(entity.getId(), (int)persistentData.get(e));
        }
    }

    @Test(priority = 5, enabled = true)
    public void writeBigData(){
        int textSize = 5000;
        Random r = new Random();
        TextPromotion promotion = new TextPromotion();
        promotion.setNumber(10);
        promotion.setCaption("test text");
        StringBuffer s = new StringBuffer("");
        for(int i=0; i<textSize; i++){
            s.append(r.nextInt());
        }
        logger.info("Write: "+s);
        promotion.setText(s.toString());

        Integer id = getDao().create(promotion);

        TextPromotion entity = getDao().get(id);
        logger.info("Read:  "+entity.getText());
        Assert.assertNotNull(entity);
        Assert.assertTrue(isEntitiesEqual(entity, promotion));
    }
}
