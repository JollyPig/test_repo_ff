package com.scnsoft.fotaflo.webportal.ws;

import com.scnsoft.fotaflo.webportal.bean.LocationBean;
import com.scnsoft.fotaflo.webportal.bean.LocationMetadataBean;
import com.scnsoft.fotaflo.webportal.service.AbstractServiceTest;
import junit.framework.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class LocationWebServiceTest extends AbstractServiceTest {

    @Autowired
    ILocationWebService service;

    List<LocationBean> list = new ArrayList<LocationBean>();

    @Test(enabled = true)
    public void list(){
        List<LocationBean> list = service.getLocations();
        Assert.assertNotNull(list);
        logger.info("Clients list size: "+list.size());
        Assert.assertTrue(list.size() > 0);
        this.list = list;
    }

    @Test(enabled = true, dependsOnMethods = "list")
    public void get(){
        LocationBean rc;
        for(LocationBean c: this.list){
            rc = service.getLocation(c.getId());
            Assert.assertNotNull(rc);
            logger.info("Client: "+ rc);
        }
    }

    @Test(enabled = true, dependsOnMethods = "list")
    public void getMeta(){
        LocationMetadataBean rc;
        LocationBean c = this.list.get(0);
        rc = service.getLocationMetadata(c.getId());
        Assert.assertNotNull(rc);
        logger.info("Metadata: "+ rc);
    }

}
