package com.scnsoft.fotaflo.webportal;

import com.scnsoft.fotaflo.webportal.security.User;
import org.apache.log4j.Logger;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.BeforeTest;

import java.util.ArrayList;

@ContextConfiguration(locations = { "classpath*:applicationContext-test.xml" })
public class AbstractTest extends AbstractTransactionalTestNGSpringContextTests {

    protected final Logger logger = Logger.getLogger(this.getClass());

    @BeforeTest
    public void initialize(){
        System.setProperty("config.dir",  Thread.currentThread().getContextClassLoader().getResource("").getPath());
        logger.info(System.getProperty("config.dir"));
        authenification();
    }

    protected void authenification(){
        Authentication authentication = new UsernamePasswordAuthenticationToken(new User("tayna","tayna",2,new ArrayList<GrantedAuthority>(){
            {
                add(new GrantedAuthorityImpl("ROLE_PUBLIC"));
                add(new GrantedAuthorityImpl("ROLE_USER"));
                add(new GrantedAuthorityImpl("ROLE_ADMIN"));
            }
        }), "tayna");
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

}
