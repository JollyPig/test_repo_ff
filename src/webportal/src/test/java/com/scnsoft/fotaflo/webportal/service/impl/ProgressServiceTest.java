package com.scnsoft.fotaflo.webportal.service.impl;

import com.scnsoft.fotaflo.webportal.dto.ProgressDto;
import com.scnsoft.fotaflo.webportal.service.pb.ProgressManager;
import com.scnsoft.fotaflo.webportal.service.IProgressService;
import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import java.util.Random;

public class ProgressServiceTest{
    protected final Logger logger = Logger.getLogger(this.getClass());

    IProgressService progressService = new ProgressService();

    @Test
    public void test() throws InterruptedException{
        new Thread(new ProgressMock("1")).start();
        new Thread(new ProgressMock("2")).start();
        new Thread(new ProgressMock("3")).start();
        new Thread(new ProgressMock("4")).start();

        long time = System.currentTimeMillis();
        while (!progressService.getProgresses().isEmpty()){
            if(System.currentTimeMillis() - time > 1000){
                time = System.currentTimeMillis();
                logger.info("Total progress: "+progressService.getProgresses());
            }
        }

    }

    class ProgressMock implements Runnable{

        String id;
        long timeout = (long)(Math.random() * 10) * 1000 + 1000;
        float progress = (float)Math.random() * 10 + 5;

        ProgressMock(String id) {
            logger.info("add to "+id+": "+progress+" every "+timeout+"ms");
            this.id = id;
        }

        @Override
        public void run() {
            ProgressDto dto;
            try{
                do{
                    progressService.addProgress(id, progress);
                    dto = progressService.getProgress(id);
                    logger.info("progress for "+id+": "+dto.getProgress());

                    Thread.sleep(timeout);
                }while(dto.getProgress() < 100.0f);

            }catch(InterruptedException e){
                logger.warn(e.getMessage());
            }
        }
    }

}
