DELIMITER //

CREATE FUNCTION GET_PICTURE_CR_DATE(s text)
  RETURNS datetime

  BEGIN
    DECLARE d datetime;

    IF STR_TO_DATE( s, '%d/%m/%Y %h:%i %p' ) is not null THEN SET d = STR_TO_DATE( s, '%d/%m/%Y %h:%i %p' );
    ELSEIF STR_TO_DATE( s, '%Y-%m-%d %h:%i:%s' ) is not null THEN SET d=STR_TO_DATE( s, '%Y-%m-%d %h:%i:%s' );
    ELSE SET d = null;
    END IF;

    RETURN d;
  END //

DELIMITER ;

select GET_PICTURE_CR_DATE(`picture_creation_date`) from `picture`;

DELIMITER //

CREATE FUNCTION GET_URL_FROM_PATH(s TEXT)
  RETURNS TEXT

  BEGIN

    IF SUBSTRING( s, 1, 1 ) = '\\' 
    THEN SET s = SUBSTRING( s, 2, LENGTH(s) );
    END IF;
    
    SET s = REPLACE(s, '\\', '/');

    RETURN s;
  END //

DELIMITER ;